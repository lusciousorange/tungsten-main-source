<?php
/**
 * -----------------------
 * DO NOT MODIFY THIS FILE
 * -----------------------
 *
 * This file is the main setup file for the configuration of the site. These values are used throughout the system and
 * you can declare your own and override these as needed.
 *
 * SITE-SPECIFIC CONFIGURATION
 * TC_config.php
 * You can create your own TC_config.php file which can extend and override these defaults as necessary. Do not copy/paste
 * this entire file, instead just set the values that you feel you need to have on any installation of the site. This file
 * is likely committed as part of the repository to ensure it lives with the project.
 *
 * 1. DO not put any passwords or sensitive information in that file, as it ends up in the repository.
 * 2. This file MUST include this file at the start.
 *
 * ENVIRONMENT CONFIGURATION
 * TC_config_env.php (previously TC_config_local.php)
 * This file is where you can put environment specific values such as DB connection values, debug settings, etc
 *
 * This filename is already part of .gitignore in the site root and should never be included as part of the repository.
 * It should be created and managed separately for each installation on each environment/server.
 *
 */


$TC_config = [];
$TC_classes = []; // array to store all the classes that are instantiated. Separate from TC_config to avoid excessive saving within classes
$TC_config['site_tungsten_title'] = 'CMS';

//////////////////////////////////////////////////////
//
// TIMEZONE DEFAULT
//
//////////////////////////////////////////////////////
date_default_timezone_set('America/Winnipeg');
$TC_config['DB_timezone'] = 'America/Winnipeg';

//////////////////////////////////////////////////////
//
// ERROR SETTINGS
//
//////////////////////////////////////////////////////
$TC_config['force_show_errors'] = false;
// WARNING - turn this off for any production site. The system has a mechanism for showing/hiding errors. This should be used in those cases where the error is happening before those settings can be enabled.
$TC_config['show_errors'] = 0; //E_ALL ^ (E_WARNING | E_PARSE | E_STRICT);
$TC_config['use_local_libraries'] = false; // system will load libraries from local rather via remove servers

if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '8888')
{
	$TC_config['is_localhost'] = true;
}

$TC_config['console_saving'] = true;

// Indicate if the console should show the "real memory" which his often higher. Memory is allocated in blocks, so
// enabling this will show those whole number increases in value. False will show the more granular allocated memory, which
// is better for fine-tuning memory issues.
$TC_config['console_show_real_memory'] = false;

//////////////////////////////////////////////////////
//
// TUNGSTEN CORE PATH
//
//////////////////////////////////////////////////////
$TC_config['TCore_path'] = '/admin/TCore';
//$TC_config['Tungsten_class_name'] = 'TMv_OverrideClassName';
//$TC_config['Tungsten_admin_folder_URL'] = 'folder_other_than_admin';

//////////////////////////////////////////////////////
//
// FILE LOCATIONS
//
//////////////////////////////////////////////////////
$TC_config['saved_file_path'] = $_SERVER['DOCUMENT_ROOT'].'/assets/';

//////////////////////////////////////////////////////
//
// DATABASE SETUP
//
// Copy these to your environment config file
//
//////////////////////////////////////////////////////
$TC_config['DB_hostname'] = ''; // likely localhost or the hostname or IP for the DB server
$TC_config['DB_database'] = ''; // the database to be used
$TC_config['DB_username'] = ''; // the username to the database
$TC_config['DB_password'] = ''; // the password to the database
$TC_config['DB_port']     = false; // the port to the database, default is 3306, leave blank to omit
$TC_config['DB_type'] 	  = 'mysql'; // the password to the database
$TC_config['DB_prefix']   = ''; // Will create tables with a prefix and prefix calls will be respected.

//////////////////////////////////////////////////////
//
//  CLASS AUTOLOADERS
//
//////////////////////////////////////////////////////
$TC_config['class_loaders'][] = $TC_config['TCore_path']."/AutoLoading/core_autoloader.php"; // Core Class Loader
$TC_config['class_loaders'][] = "/admin/system/classes/tungsten_autoloader.php"; // Tungsten classes not a part of the core
$TC_config['class_loaders'][] = "/vendor/autoload.php"; // Tungsten classes not a part of the core


//////////////////////////////////////////////////////
//
//  TUNGSTEN SKINNING
//
//////////////////////////////////////////////////////
// Tungsten can be skinned to show a different style other than the
// default, which will work in most cases. Any skin must be placed
// inside the /admin/skins/ folder and the value provided must be the name
// of the folder.
//
// A skin consists must contain a skin.css file which overrides the existing
// styles on the site
//
// If no value is provided or if the skin cannot be found, then the default
// appearance is used.

//$TC_config['skin'] = 'skin_folder_name';


//////////////////////////////////////////////////////
//
//  CLASS OVERRIDES
//
//////////////////////////////////////////////////////
// List of class names that should be used instead of the
// parent class to allow for full customization of existing classes
// This functionality only works when the class is loaded using the Factory ::init() method
// $TC_config['class_overrides']['TMm_ClassCalled'] = 'TMm_ClassUsed';

//////////////////////////////////////////////////////
//
//  LOCALIZATION
//
//////////////////////////////////////////////////////
//include_once(__DIR__.'/admin/localization/config/init.php');

//////////////////////////////////////////////////////
//
//  DISPLAY SETTINGS
//
//////////////////////////////////////////////////////
$TC_config['content_width'] = 1000; // stores the value of the content width, which is used by some rendering tools
//$TC_config['content_column_gap'] = 30; // Width of the column gap, usually $layout-column-gap in SASS
//$TC_config['content_single_column_width'] = 750; // Single column switch, usually $single-column-width in SASS

$TC_config['upload_resize_width'] = 3600; // stores the value that photos should be resized to when uploaded

//////////////////////////////////////////////////////
//
//  FEATURE TOGGLES
//
//////////////////////////////////////////////////////
$TC_config['use_api'] 				= false; // Enables the API functionality for this site to access data
$TC_config['api_cors_origin'] 		= '*'; // The accepted api CORS origin
$TC_config['api_cors_headers'] 		= '*'; // The accepted api CORS permitted headers

$TC_config['use_google_recaptcha'] 	= false; // Enables the functionality related to google recaptchas
$TC_config['use_content_flagging'] 	= false; // Enables content flagging for content
$TC_config['use_workflow'] 			= true; // Enables the workflow setup for content

$TC_config['use_email_verification']= false; // Enables the verification step for emails

$TC_config['use_page_subheadings'] 	= true; // DEPRECATED,  not used for anything

$TC_config['use_git_scanning_email']= false; // Enables the git scanning in conjunction with crons.

$TC_config['use_seo_advanced'] 		= true; // Indicates if the advanced SEO tools are available

$TC_config['use_photo_captions'] 	= true; // Indicates if photo captions are used

$TC_config['use_pages_hero_boxes'] 	= false; // Hero boxes, which aren't always used

// User Impersonation
// The system allows admins to impersonate other users. This isn't always wanted and can be disabled
$TC_config['use_impersonate_user'] 	= true;


$TC_config['use_error_table'] 		= true; // Enables the Error table tracking. Tracks console and fatal errors in a table.

$TC_config['use_stacked_forms'] 	= false; // Indicates if forms are stacked by default, instead of side-by-side

// Indicates if the site uses a single name field for user names
$TC_config['use_single_name_field']	= false;

// Indicates if the benchmarking tools should be used.
$TC_config['use_benchmarking'] 		= false;

// Page builder configuration for what can and cannot happen
$TC_config['page_builder'] = [
	'section_background_images' 	=> false,
	'row_background_images' 		=> false,
	'column_background_images' 		=> false,
	'allow_custom_spacing' 			=> false,// Image filtering options, which can be set to provide an option to
	
	// Background image filters which can be applied on sections if set
	'background_image_filtering' => [
//		'grayscale' => [
//			'name' => 'Grayscale',
//			'function' => 'grayscale',
//			'args' => [],
//		],
//		"darken" => [
//			'name' => "Darken 30%",
//			'function' => "brightness",
//			'args' =>[-30],
//		],
//
	
	],
];

// An option exists to use webp for all the thumbs and images.
// Enabling this will regenerate thumbs
$TC_config['use_webp'] = false;

//////////////////////////////////////////////////////
//
// TUNGSTEN 9
//
// This is an interface update that loads different
// that also has some functionality updates to consider
//
//////////////////////////////////////////////////////
$TC_config['use_tungsten_9'] 		= true;

// Indicates if model history tracking is enabled
// Each model must be configured manually by adding the TSt_ModelWithHistory trait
$TC_config['use_track_model_history'] = false;

$TC_config['use_pages_version_publishing'] = false; // Enables pages tracking on the site

//////////////////////////////////////////////////////
//
// PASSWORDS
//
//////////////////////////////////////////////////////

// Indicate if passwords can be reused by a user
$TC_config['password_prevent_reuse'] = false;

// Indicate if we track consecutive failed logins
$TC_config['password_max_failed_logins'] = 0;


//////////////////////////////////////////////////////
//
// MEMCACHED
//
//////////////////////////////////////////////////////
$TC_config['memcached'] = [
	'enabled' => false, // disabled by default, enable as needed on a site
	'server' => "127.0.0.1",
	'port' => 11211,
	'prefix' => null, // This value must be set in the local environment. Must be unique to avoid conflicts with other sites
];

//////////////////////////////////////////////////////
//
// EMAIL
//
// Replaces the `disable_emails` setting in they system module
//
//////////////////////////////////////////////////////
$TC_config['email'] = [
	'enabled' => true,
];