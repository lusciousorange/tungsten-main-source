<?php
	// Sets the document root to be the directory
	// Always works for cron since it's in the site root
	// Allows it to run on the CLI as well
	$_SERVER['DOCUMENT_ROOT'] = __DIR__;
	require_once($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_config.php");
	
	$cron = TSm_CronManager::init();
	$cron->run();
	
	// For an article on setting up cronjobs
	// http://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/

	// Detects if we've set the keep_session URL Parameter
	// If it's not set, we destroy the session and exit, avoiding buildup or any extra
	// If you want to run the cron manually from the browser, call cron.php?keep_session=1
	// That will avoid destroying the session and logging you out
	if(!isset($_GET['keep_session']))
	{
		session_destroy();
		exit();
	}
	
?>