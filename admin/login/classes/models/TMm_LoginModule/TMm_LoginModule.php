<?php

/**
 * Class TMm_LoginModule
 *
 * An extension of the normal module to add functionality specific to this module.
 */
class TMm_LoginModule extends TSm_Module
{
	
	/**
	 * TSm_Module constructor.
	 * @param array|int $module_id
	 */
	public function __construct($module_id = 'login')
	{
		parent::__construct($module_id);
		
		// ensure this module is always auto-updated
		// since it's core to the operation of the system
		$this->setAsAutoUpdate();
		
		// Detect login swap from pages
		if(!$this->variableExists('show_authentication'))
		{
			$this->updateVariableWithName('show_authentication',
			                              TC_getModuleConfig('pages','show_authentication'));
		}
	}
	
	/**
	 * Indicates the URL that we direct people to if the person is logged in. Every site will have a homepage or
	 * dashboard specific to user logins and this is configured in the module settings.
	 * @return string
	 */
	public function publicLoggedInHomepageURL() : string
	{
		// Default to the homepage
		$url = '/';
		
		// Detect if there's a setting in login module for where to send them if successful
		$public_page_id = TC_getModuleConfig('login','public_login_homepage_id');
		if($public_page_id > 0)
		{
			$success_menu = TMm_PagesMenuItem::init($public_page_id);
			if($success_menu)
			{
				$url = $success_menu->pathToFolder();
			}
		}
		
		return $url;
	
	}
	
	/**
	 * Indicates the URL that contains the page where people see the login screen. Every site
	 * will have a specific page for this. This is configured in the module settings.
	 * @return string
	 */
	public function publicLoginPageURL() : string
	{
		// Default to the homepage
		$url = '/';
		
		// Detect if there's a setting in login module for where to send them if successful
		$public_page_id = TC_getModuleConfig('login','public_login_menu_id');
		if($public_page_id > 0)
		{
			$success_menu = TMm_PagesMenuItem::init($public_page_id);
			if($success_menu)
			{
				$url = $success_menu->pathToFolder();
			}
		}
		
		return $url;
		
	}
	
	
}





