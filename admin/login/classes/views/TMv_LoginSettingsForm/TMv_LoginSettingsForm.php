<?php
class TMv_LoginSettingsForm extends TSv_ModuleSettingsForm
{
	/**
	 * TMv_LoginSettingsForm constructor.
	 * @param string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
		
	}
	
	
	/**
	 * THe configuration settings
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('show_authentication', 'Website user logins ');
		$field->setHelpText('Indicate if the site allows users to login and then include those options in the page settings.');
		$field->addOption('0', 'No');
		$field->addOption('1', 'Yes – Enable login features for this website');
		$field->setDefaultValue('0');
		$this->attachView($field);
		
		$container = new TCv_FormItem_Group('public_login_group','Public login');
		
		$heading = new TCv_FormItem_Heading('public_settings','Public login configuration');
		$container->attachView($heading);
		
		// Only show if the show_authentication field is set
		$container->setVisibilityToFieldValue('show_authentication',1);
		
		// Get menu 0, need to use it more than once
		$menu_0 = TMm_PagesMenuItem::init(0);
		
		
		$field = new TCv_FormItem_Select('public_login_menu_id','Login page ');
		$field->setHelpText('The page that is the public login menu id');
		$field->useFiltering();
		
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$container->attachView($field);
		
		$field = new TCv_FormItem_Select('public_login_homepage_id','Logged in homepage');
		$field->setHelpText('The page that is considered "home" for when a user first logs in.');
		$field->useFiltering();
		
		// Only show if the show_authentication field is set
		$field->setVisibilityToFieldValue('show_authentication',1);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$container->attachView($field);
		
		$field = new TCv_FormItem_TextField('password_code_note_found', 'Password reset code not found');
		$field->setHelpText('The text shown to a user if we cannot find the code they provided for a password reset');
		$field->setDefaultValue('The reset code you provided could not be found. Please try requesting a password reset again.');
		$field->setVisibilityToFieldValue('show_authentication',1);
		$container->attachView($field);
		
		$this->attachView($container);
		
	}
}