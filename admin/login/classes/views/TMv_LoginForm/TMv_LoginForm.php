<?php
/**
 * Class TMv_LoginForm
 *
 * A form that logs a user into the system
 */
class TMv_LoginForm extends TCv_FormWithModel
{
	use TMt_PagesContentView;
	
	protected $user;
	
	protected string $forgot_password_link = '';
	
	protected bool $is_stacked = true;

	/**
	 * TMv_LoginForm constructor.
	 *
	 */
	public function __construct()
	{
		parent::__construct('TMm_User');
		
		$this->setShowID(false);
		$this->setButtonText(TC_localize('sign_in_button_text','Sign in'));
		
		// Disabling form tracking ruins other forms that rely on it in the system
		// Sometimes loaded after the fact
		// Do not turn that back on
		// unset($_SESSION['TCm_FormTracker']);
		
		// For to install if not complete
		if(!TC_website()->installComplete())
		{
			header("Location: /admin/install/");
			exit();
		}
		
		// Handle success URL
		if(TC_isTungstenView())
		{
			$this->setSuccessURL('/admin/login/'); // redirect catches it
		}
		else // public
		{
			$login_module = TMm_LoginModule::init();
			$this->setSuccessURL($login_module->publicLoggedInHomepageURL());
		}
	}

	/**
	 * Sets the forgot password link. If set, it will be added to the form
	 * @param string $url
	 */
	public function setForgotPasswordURL($url)
	{
		$this->forgot_password_link = $url;
	}

	/**
	 * Configures the form elements
	 */
	public function configureFormElements()
	{
		if($this->is_stacked)
		{
			$this->addClass('stacked');
			
		}
		
		if(TC_website())
		{
			$this->user = TC_website()->user();
		}
		
		// Email
		$email = new TCv_FormItem_TextField('email', TC_localize('Email','Email'));
		$email->setIsRequired();
		//$email->setPlaceholderText('Email');
		$email->disableAutoComplete();
		$email->setShowID(false);
		$this->attachView($email);
		
		// Password
		$password = new TCv_FormItem_Password('password', TC_localize('Password','Password'));
		$password->setIsRequired();
		$password->setShowID(false);
		//$password->setPlaceholderText('Password');
		$this->attachView($password);

		// Setting submit button to not have an ID
		$button = new TCv_FormItem_SubmitButton('submit', $this->button_text);
		$button->setShowID(false);
		$this->setSubmitButton($button); // force add now, avoid stacking below other items on extended classes


		// Handle Password Reset
		$forgot_password = new TCv_FormItem_HTML('forgot_password', '');
		
		$forgot_password_link = new TCv_Link('forgot_password_link');
		$forgot_password_link->addText(TC_localize('forgot_your_password?','Forgot your password?'));
		
		// Tungsten login form
		if(TC_isTungstenView())
		{
			$forgot_password_link->setURL('/admin/login/do/forgot-password/');
		}
		else // front-end-routing
		{
			$forgot_password_link->setURL('/w/login/forgot-password/');
		}
		$forgot_password->attachView($forgot_password_link);
		$this->attachView($forgot_password);
		
		
	}
	
	/**
	 * Override to turn off authentication for the form processor
	 * @return bool
	 */
	public static function customFormProcessor_skipAuthentication()
	{
		return true;
	}
	
	
	/**
	 * Performs the primary database action
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return bool
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		return false; // don't do any update function
	}
	
	/**
	 * This function is called after the traditional form processor processFormItems() but before the updateDatabase()
	 * function. A form instance can override this function to do custom form processing that doesn't fit within the
	 * standard operating procedure for many Tungsten forms.
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		TC_messageConditionalTitle(TC_localize('Login Successful','Login successful'), true);
		TC_messageConditionalTitle(TC_localize('Login Unsuccessful', 'Login unsuccessful'), false);

		// process login
		//$validation = new TCm_FormItemValidation('login', 'Tungsten Login');
		if(!TC_website()->processLogin($_POST['email'], $_POST['password']))
		{
			$form_processor->fail();
		}



	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		$field = new TCv_FormItem_Select('is_stacked','Use Stacked Layout');
		$field->addOption(1,'Yes');
		$field->addOption(0,'No');
		
		$form_items[] = $field;
		
		return $form_items;
	}
	
	
	
	public static function pageContent_ViewTitle() : string { return 'User login form'; }
	public static function pageContent_IconCode() : string { return 'fa-user'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A form that allows users to login to the website.';
	}

}
?>