<?php
/**
 * Class TMv_TungstenLoginForm
 *
 * The login form for Tungsten
 */
class TMv_TungstenLoginForm extends TMv_LoginForm
{
	/**
	 * TMv_TungstenLoginForm constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		unset($_SESSION['install_step']); // used to avoid messy install setup conditions.

		$this->setForgotPasswordURL('/admin/login/do/forgot-password/');
			
	}
	
	/**
	 * Configures the form
	 */
	public function configureFormElements()
	{
		if(TC_website()->loggedIn())
		{
			// Update the success url so that it points to the correct folder
			// This happens after login, ensuring they see the correct folder
			$folder = TC_currentUser()->homeModule()->folder();
			
			
			header('Location: /admin/'.$folder.'/');
			exit();
		}
		
		//$this->success_url = '/admin/'.$folder.'/';
		parent::configureFormElements();

	}
	
	/**
	 * Performs the functions that happen after the fact
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return bool
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);
		
		// Update the success url so that it points to the correct folder
		// This happens after login, ensuring they see the correct folder
		$folder = TC_currentUser()->homeModule()->folder();
		
		$success_url = '/admin/'.$folder.'/';
		
		$url_target = TC_currentUser()->homeModuleURLTarget();
		if($url_target != '')
		{
			$success_url .= 'do/'.$url_target.'/';
		}
		$form_processor->setSuccessURL($success_url);
		
		return false; // don't do any update function
	}
	
	
	// Override the TMv_LoginForm option so that it doesn't show up as a content option
	public static function pageContent_IsAddable() : bool { return false; }
	


}
?>