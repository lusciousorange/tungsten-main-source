<?php

class TSc_LoginController extends TSc_ModuleController
{
	/**
	 * TSc_LoginController constructor
	 * @param $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
	}
	
	/**
	 * Defines the URL targets for this controller
	 * @return void
	 */
	public function defineURLTargets()
	{
		$item = TSm_ModuleURLTarget::init( '');
		$item->setViewName('TMv_TungstenLoginForm');
		$item->setAsSkipsAuthentication();
		$item->setTitle('Sign in');
		$this->addModuleURLTarget($item);
	
		$item = TSm_ModuleURLTarget::init( 'forgot-password');
		$item->setViewName('TMv_ForgotPasswordForm');
		$item->setAsSkipsAuthentication();
		$item->setTitle('Forgot password');
		$item->enableFrontEndRouting(); // allow to always happen on front-end
		$this->addModuleURLTarget($item);
	
		$item = TSm_ModuleURLTarget::init( 'reset-password');
		$item->setViewName('TMv_EmailResetPasswordForm');
		$item->setAsSkipsAuthentication();
		$item->setTitle('Reset password');
		$item->enableFrontEndRouting(); // allow to always happen on front-end
		$this->addModuleURLTarget($item);
		
		// ! ----- LOGOUT -----
 		
		$item = TSm_ModuleURLTarget::init( 'logout');
		$item->setModelName('TSv_Tungsten');
		$item->setModelInstanceObjectRequired();
		$item->setModelActionMethod('logout()');
		$item->setNextURLTarget(''); // must be blank to avoid infinite loops
		$item->setTitle('Logout');
		$item->setAsSkipsAuthentication(); // let people run this either way. better for security
		$item->enableFrontEndRouting(); // allow logout to always happen on front-end
		$this->addModuleURLTarget($item);

       $item = TSm_ModuleURLTarget::init( 'logout-to-referrer');
        $item->setModelName('TSv_Tungsten');
        $item->setModelActionMethod('logout()');
        $item->setNextURLTarget('referrer');
        $item->setTitle('Logout');
        $item->setAsSkipsAuthentication();
        $this->addModuleURLTarget($item);


    }
	
		

}
