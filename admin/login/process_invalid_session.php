<?php
	$skip_tungsten_authentication = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 

	$original_page = @$_SESSION['tungsten_login_original_page'];
	
	// Destroy the session.
	session_destroy();
	session_start();
	session_regenerate_id(); // delete the old session id regenerate a new id

	// Recreate Tungsten
	TC_website();
	
	$_SESSION['tungsten_login_original_page'] = $original_page;

	header("Location: /admin/");
	exit();
	
?>
