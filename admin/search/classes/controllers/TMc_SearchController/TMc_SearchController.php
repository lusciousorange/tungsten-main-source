<?php
/**
 * Class TMc_SearchController
 */
class TMc_SearchController extends TSc_ModuleController
{
	/**
	 * TMc_SearchController constructor.
	 * @param $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
	}
		
	/**
	 * Define the URL targets for the search controller
	 */
	public function defineURLTargets()
	{
		/**
		 * The URL target that is called when searched. If customization is required, then the `TMv_SearchResults
		 * class can be extended.
		 */
		$target = TSm_ModuleURLTarget::init('');
		$target->setTitle('Search');
		$target->setViewName('TMv_SearchResultsList');
		$this->addModuleURLTarget($target);
		
		
		
		/**
		 * The URL target that is called when searched. If customization is required, then the `TMv_SearchResults
		 * class can be extended.
		 */
		$target = TSm_ModuleURLTarget::init('search');
		$target->setTitle('Search');
		$target->setViewName('TMv_SearchResults');
		$target->setAsSkipsAuthentication();
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);

		
	}

}
?>