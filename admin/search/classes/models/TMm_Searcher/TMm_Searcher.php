<?php
/**
 * Class TMm_Searcher
 *
 * A class that searches the various models and returns a collection of views.
 */
class TMm_Searcher extends TCm_Model
{
	protected $search_string = '';
	protected $searchable_model_names = false;
	
	protected $use_query_limits = false;
	
	protected static $searchable_class_names;
	
	/**
	 * @var bool
	 * Stored value to indicate if we're using localization It's set once during the process
	 */
	protected $use_localization_non_default = false;
	protected $loaded_language = 'en'; // Dummy value that gets overridden if necessary
	
	/**
	 * @var bool A setting that indicates that the searcher should return results by model, leaving them grouped
	 * together instead of one large single result list
	 */
	protected $group_by_model = false;
	
	/**
	 * TMm_Searcher constructor.
	 */
	public function __construct()
	{
		parent::__construct('searcher');
		
		$this->determineLocalization();
		
		// find the list of searchable classes
		$this->searchable_model_names = static::searchableClassNames();
		
		
	}
	
	/**
	 * Detect if we're using localization AND if we're using non-default languages which then requires some
	 * additional testing to ensure we load localized values
	 */
	public function determineLocalization() : void
	{
		// Deal with localization
		if(TC_getConfig('use_localization'))
		{
			// Get the module
			$localization = TMm_LocalizationModule::init();
			
			// Detect if we're loading the non-default language
			if(!$localization->isLoadingDefaultLanguage())
			{
				$this->use_localization_non_default = true;
			}
			
			$this->loaded_language = $localization->loadedLanguage();
		}
	}
	/**
	 * Turns on the group-by-model functionality which changes how the results are presented. This will insert
	 * heading strings into the search results, which must be addressed by the rendering view.
	 */
	public function enableGroupingByModel() : void
	{
		$this->group_by_model = true;
	}
	
	/**
	 * @return bool|array()
	 */
	public function searchableModelNames() : array
	{
		return $this->searchable_model_names;
	}
	
	/**
	 * This method by default returns the original searchable_model_names,
	 * but can be overridden in order to customize which models are being searched.
	 *
	 * @param string[] $model_names
	 */
	public function setSearchableModelNames($model_names) : void
	{
		$this->searchable_model_names = $model_names;
	}
	
	/**
	 * Function used to filter results when it's called through a TMv_SearchableModelList.
	 * @param $filter_values
	 * @return array
	 */
	public function processFilterSearch($filter_values) : array
	{
		// Turn off the query limits to ensure we find them all
		$this->use_query_limits = false;
		
		// Set the searchable models
		$this->searchable_model_names = [];
		foreach(static::searchableClassNames() as $model_name)
		{
			$index_name = 'search_'.$model_name;
			if(isset($filter_values[$index_name]) && $filter_values[$index_name])
			{
				$this->searchable_model_names[] = $model_name;
			}
		}
		
		// Search, include the counts
		return  $this->search($filter_values['search'], true);
	}
	
	/**
	 * The primary search command which looks for a string.
	 * @param string $string
	 * @return TCm_Model[] The array of models that are found for this search. If grouping-by-model is enabled, there
	 * will also be string names included which are the class_names for the headings.
	 */
	public function search(string $string, bool $include_counts = false) : array
	{
		// Force off the group by model
		if(TC_isTungstenView() && $this->group_by_model)
		{
			$this->group_by_model = false;
		}
		
		$this->processSearchString($string);
		$results = [];
		$counts = [];
		
		// perform the search
		foreach($this->searchableModelNames() as $model_name)
		{
			// Confirm the class is actually searchable
			if(!$model_name::hasTrait('TMt_Searchable'))
			{
				$this->addConsoleError('Attempting to search non-searchable class '.$model_name);
				continue;
			}
			
			// Get the search results for this one model name
			$model_results = $this->searchModelWithName($model_name);
			
			$counts[$model_name] = count($model_results);
			
			// Check if we're grouping by model AND we have results, if so that we can inject the headings
			if($this->group_by_model && count($model_results) > 0)
			{
				$results[] = $model_name;
			}
			
			
			// Add our model results to the growing list of results. Uses the array + command to merge
			$results = array_merge($results , $model_results) ;
			
		}
		
		// Not grouping by model, we should sort them as one giant list
		if(!$this->group_by_model)
		{
			krsort($results);
		}
		
		if($include_counts)
		{
			$results['counts'] = $counts;
		}
		
		return $results;
	}
	
	
	/**
	 * Handles the processing of a search string, which breaks it up by space and removes any blank strings. Values
	 * are saved to the class.
	 * @param string $string
	 */
	public function processSearchString(string $string) : void
	{
		// remove spaces on the ends
		$string = trim($string);
		
		// Replace any multiple-space with a single space
		$string = preg_replace('!\s+!', ' ', $string);
		
		$this->search_string = $string;
		
	}
	
	
	
	/**
	 * This method will perform a search on a given model. It will use the table properties for that model
	 * @param string|TMt_Searchable $model_name The name of the model.
	 * @param bool $rank_models Indicates if these models should be ranked by rating. If false, they are returned in
	 * the search query order
	 * @return TMt_Searchable[]|TCm_Model[] The array models for a given model name.
	 */
	protected function searchModelWithName(string $model_name, bool $rank_models = true) : array
	{
		$this->addConsoleMessage('---- Searching '.$model_name.' ----- Order : '.$model_name::searchOrderIndex());
		if(sizeof($model_name::searchFields()) == 0)
		{
			return array();
		}
		
		$models = array();
		
		$limit = null;
		if($this->use_query_limits)
		{
			$limit = $model_name::searchQueryLimit();
		}
		
		$alternate_language = null;
		if($this->use_localization_non_default)
		{
			$alternate_language = $this->loaded_language;
		}
		
		
		
		return $model_name::search($this->search_string,$limit, $alternate_language);
		
	}
	
	
	
	
	/**
	 * Returns the list of searchable class names. This takes into account the user's current access and if we're in
	 * Tungsten mode or not
	 * @return string[]
	 */
	public static function searchableClassNames() : array
	{
		if(is_null(static::$searchable_class_names))
		{
			$searchable_class_names = [];
			
			$current_user = TC_currentUser();
			$all_model_names = TC_modelsWithTrait('TMt_Searchable');
			foreach($all_model_names as $model_name)
			{
				// Only main classes get shown
				if($model_name == $model_name::classNameForInit())
				{
					if(TC_isTungstenView())
					{
						// Only bother with Tungsten views if the current user is logged in
						if($current_user)
						{
							// Admins always see all. Common shortcut
							if($current_user->isAdmin())
							{
								$searchable_class_names[$model_name] = $model_name;
							}
							elseif(TC_getModuleConfig('search', 'restrict_to_user_modules'))
							{
								$module = TC_moduleForClass($model_name);
								if($module->checkUserPermission($current_user))
								{
									$searchable_class_names[$model_name] = $model_name;
								}
							}
						}
					}
					else // public view, relies on
					{
						if($model_name::isSearchableOnPublicSite())
						{
							$searchable_class_names[$model_name] = $model_name;
						}
						
					}
					
					
				}
			}
			
			$index_groups = [];
			// Order them based on the searchOrderIndex() static function
			// Based on the z-index principle with the default of 0
			foreach($searchable_class_names as $class_name)
			{
				
				$order = $class_name::searchOrderIndex();
				if(!isset($index_groups[$order]))
				{
					$index_groups[$order] = [];
				}
				
				$index_groups[$order][] = $class_name;
				
			}
			ksort($index_groups);
			
			// Break the groups into full order
			$ordered_class_names = [];
			foreach($index_groups as $order => $index_group_class_names)
			{
				$ordered_class_names = array_merge($ordered_class_names, $index_group_class_names);
			}
			
			static::$searchable_class_names = $ordered_class_names;
		}
		
		return static::$searchable_class_names;
	}
	
	
	
	/**
	 * Moves a particular class name to the top of the searchable classes. This must be called AFTER the searchable
	 * classes are processed, which happens in the constructor
	 *
	 * @param string $class_name
	 */
	protected function moveClassToTopOfSearchableModels(string $class_name) : void
	{
		// Get the actual class to call
		$init_class = static::overrideClassName($class_name);
		
		// Only move if it's in the searchable list of models
		// Possibly restricted
		if(isset($this->searchable_model_names[$init_class]))
		{
			// Remove either from the results, just in case
			unset($this->searchable_model_names[$init_class]);
			unset($this->searchable_model_names[$class_name]);
			
			// Append to the start
			$this->searchable_model_names = array($init_class => $init_class) + $this->searchable_model_names;
		}
		
	}
	
	/**
	 * Moves a particular class name to the bottom of the searchable classes. This must be called AFTER the searchable
	 * classes are processed, which happens in the constructor
	 *
	 * @param string $class_name
	 */
	protected function moveClassToBottomOfSearchableModels(string $class_name) : void
	{
		// Get the actual class to call
		$init_class = static::overrideClassName($class_name);
		
		// Only move if it's in the searchable list of models
		// Possibly restricted
		if(isset($this->searchable_model_names[$init_class]))
		{
			
			// Remove either from the results, just in case
			unset($this->searchable_model_names[$init_class]);
			unset($this->searchable_model_names[$class_name]);
			
			// Add to the bottom of results
			$this->searchable_model_names[$init_class] = $init_class;
		}
	}
	
	
	/**
	 * Processes a single module folder
	 * @param TSm_Module $module
	 */
	protected function processModuleForSearchableClasses(TSm_Module $module) : void
	{
		// loop through the models for this module
		
		$view_directory = @dir($_SERVER['DOCUMENT_ROOT'].'/admin/'.$module->folder().'/classes/models/');
		if($view_directory)
		{
			while($view_class_name = $view_directory->read())
			{
				if(class_exists($view_class_name))
				{
					$traits = class_uses($view_class_name);
					if(isset($traits['TMt_Searchable']))
					{
						$this->searchable_model_names[$view_class_name] = $view_class_name;
					}
				}
			}
		}
	}
	
	
}
