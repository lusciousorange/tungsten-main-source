<?php
/**
 * Trait TMt_Searchable
 *
 * Adds teh ability for a class to be searchable in the system.
 */
trait TMt_Searchable
{
	protected static $search_icon_code;
	
	protected static ?array $search_query_fields = null;
	
	// Values related to the current search
	// These values are set in static::search() and are used in subsequent methods
	protected static string $current_search_string = '';
	protected static ?int $current_search_limit = null;
	protected static ?string $current_search_alternative_language = null;
	
	
	
	//protected static string $active_search_query_string
	
	/**
	 * Indicates if this class is publicly searchable. Defaults to false to avoid any accidentally data exposure.
	 * Every publicly available class needs to have this method set.
	 * @return bool
	 */
	public static function isSearchableOnPublicSite() : bool
	{
		return false;
	}
	
	/**
	 * Returns the icon code for this model, which returns the module's code by default
	 * @return string
	 */
	public static function searchIconCode() : string
	{
		if(is_null(static::$search_icon_code))
		{
			$module = TC_moduleForClass(get_called_class());
			static::$search_icon_code = $module->iconCode();
		}
		
		return static::$search_icon_code;
	}
	
	/**
	 * Returns the fields that should be searched in this class. The returned value is an array that either contains
	 * strings OR an array of values for more control.
	 *
	 * Returning a string means that the search will search that field using a wide catch-all "%value%".
	 *
	 * Alternatively, you can choose to return an array instead of a string with 2 values.
	 *
	 * Value 1 : The name of the field, similar to what is returned in the basic version.
	 *
	 * Value 2 : The second value is the `filter` which must be a string that indicates how the search will look with
	 * the word "value" representing the search value.
	 *
	 * For example, you could return array( array('title', 'value%')) which would search the
	 * title field but only if it starts with the search value. Similarly returning a filter of "value" would return
	 * exact matches only.
	 *
	 *
	 * @return string[]|array[]
	 */
	public static function searchFields() : array
	{
		//return array('title');
		return [
			['title', "%value%"]
		];
	}
	
	/**
	 * Returns the field column names, but now defers to searchQueryFields
	 * @deprecated Use static::searchQueryFields
	 */
	public static function searchFieldColumnNames() : void
	{
		TC_triggerError('searchFieldColumnNames() cannot be used and must be updated to use static::searchQueryFields');
	}
	
	
	/**
	 * Returns the title shown in search
	 * @return string
	 */
	public function searchTitle() : string
	{
		return $this->title();
	}
	
	public function searchTitleWithQueryWrap($query_string) : string
	{
		return $this->wrapQueryString($this->searchTitle(), $query_string);
	}
	
	/**
	 * The icon image file for the item when it's being searched. A value of null means it defaults using icons. A
	 * Font-Awesome icon name can also be returned as well.
	 *
	 * @return null|TCm_File|string
	 */
	public function searchIconImageFile()
	{
		$module = $this->moduleForThisClass();
		
		return $module->iconCode();
	}
	
	
	/**
	 * @return null|string
	 */
	public function searchSubtitle() : ?string
	{
		if(method_exists($this, 'subtitle'))
		{
			return $this->subtitle();
		}
		return null;
	}
	
	/**
	 * Indicate if older items of this class should be punished in search results.
	 * @return bool
	 */
	public function punishRankingForAge() : bool
	{
		return false;
	}
	
	/**
	 * The `Select` portion of the query for searching this model.
	 *
	 * @return string
	 */
	public static function searchQuerySelectPortion() : string
	{
		return  "SELECT * FROM `".static::tableName()."`";
	}
	
	/**
	 * The `Where` portion of the query for searching this model. This method should not include the "Where" call
	 * itself, just what comes after it. This method must return an associative array with two indicies, both of
	 * which have arrays within them. The 'where' array return snippets of text to be added in the where clause. The
	 * 'parameters' array is an associative array of parameters to be passed to PDO.
	 * @return array[]
	 */
	public static function searchQueryWherePortion() : array
	{
		$values = array();
		$values['where'] = array();
		$values['parameters'] = array();
		return  $values;
	}
	
	/**
	 * The number of items to be limited in the search
	 * @return null|int
	 */
	public static function searchQueryLimit() : ?int
	{
		return 20;
	}
	
	/**
	 * Used for anything that would come after the `WHERE` portion of the query such as:
	 * ORDER BY
	 * GROUP BY
	 *
	 * @return null|string
	 */
	public static function searchQueryAfterWherePortion() : ?string
	{
		return null;
	}
	
	/**
	 * The url that the search result leads to. If a string is returned then it will open in new tab/window.
	 * If it is an array with the property 'local' set as true then it will open in current tab/window
	 * @return array|string
	 */
	public function searchViewURL()
	{
		if(method_exists($this, 'pageViewURLPath'))
		{
			return $this->pageViewURLPath();
		}
		
		// Must be set manually
		return '#';
	}
	
	/**
	 * Confirms that this item does indeed find items once we don't have html tags involved. This is used to filter
	 * out results after the fact that might have found code that was really inside of an HTML tag.
	 *
	 * This method checks properties of a model internally, so it's worth noting that when a query returns additional
	 * calculated columns, those get set as part of the instantiation if the row is provided.
	 * @param array $results_row The result from the query that was used to instantiate a model
	 * @return bool
	 */
	public static function search_ConfirmResultsIgnoreHTMLTags(array $results_row) : bool
	{
		$search_value = strtolower(static::$current_search_string);
		if($search_value == '')
		{
			return false;
		}
		
		// Loop through each field, processing each search value for that field
		// If we find one, we can return confirming something was found
		foreach(static::searchQueryFields() as $field_values)
		{
			//$this->addConsoleDebug($this->$co);
			$column_name = $field_values['column_name'];
			// Get the text for this field at it's  basic form
			$text = '';
			if(isset($results_row[$column_name]))
			{
				$text = strip_tags(strtolower($results_row[$column_name]));
			}
			
			
			// We find our search value in this item, so get out as fast as we can
			// We just need a single confirmation
			if(strpos($text, $search_value) !== false)
			{
				return true;
			}
			
		}
		
		return false;
	}
	
	/**
	 * Returns an array of strings that are the matching snippets for this model which correspond to the search
	 * fields that are already defined for this model. This method is used in the rendering of search results to
	 * provide context to what is found.
	 * @param string $query_string The string that was being searched for
	 * @return string[] The returned value is an associative array with the index being the field name that the
	 * snippet corresponds to
	 */
	public function searchSnippets(string $query_string) : array
	{
		$snippets = [];
		
		// Need to search individual string pieces, just like search does
		//$search_strings = explode(' ', $query_string);
		
		foreach(static::searchQueryFields() as $field_values)
		{
			$column_name = $field_values['column_name'];
			// Only bother if we have a property value for search field
			if(isset($this->$column_name))
			{
				// Title Check
				// Ignore anything where the value equals the title() since that's already handled
				if($this->$column_name != $this->searchTitle())
				{
					// Get plain text, removing all tags
					$text = strip_tags($this->$column_name);
					
					// Replace any spaces with a regular space
					// We HAD a preg on \s+ but that broke a bunch of stuff
					// @see https://stackoverflow.com/questions/3760816/remove-new-lines-from-string-and-replace-with-one-empty-space
					$text = trim(str_replace(["\n","\r"], ' ', $text));
					
					// Check if the query string actually exists somewhere
					$word_position = stripos($text, $query_string );
					if($word_position !== false)
					{
						// Find the starting point
						// Go back 15 characters from the word, then find the first space and start from there.
						if($word_position > 45)
						{
							// Trim to start further back
							$text = substr($text, $word_position - 45);
							$space_position = stripos($text,' ');
							$text = substr($text, $space_position + 1);
							$snippets[$column_name] = '… '.$text;
						}
						else
						{
							$snippets[$column_name] = $text;
						}
						
					}
					
					
				}
			}
			
		}
		return $snippets;
	}
	
	//////////////////////////////////////////////////////
	//
	// Description string
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Internal function to wrap a provided string with the query string if it's found
	 * @param string $string The main string to try wrapping around
	 * @param string $query_string The query string that is being used
	 * @return string
	 */
	public function wrapQueryString(string $string, string $query_string) : string
	{
		$return_string = $string;
		if(trim($query_string) != '')
		{
			// Replacing them with a code that then swaps
			// Use preg_replace to ensure we respect case
			$return_string = preg_replace('/('.$query_string.')/i','<span class="hl">$0</span>',$return_string);
		}
		
		return $return_string;
	}
	
	
	/**
	 * Returns the string to show for the description for search for this item. This combines the various items that
	 * are searched including descriptions and other fields. It then strips out any tags before wrapping the query
	 * matches in highlighted spans. The various snippets are merged together using ellipses.
	 * @param string $query_string
	 * @return string
	 */
	public function searchDescription(string $query_string) : string
	{
		$description_pieces = [];
		
		// Try and find a description and that's our baseline
		if(method_exists( $this, 'metaDescription'))
		{
			$description_pieces['main'] = strip_tags($this->metaDescription());
		}
		
		// Try and find the search query text the searchable fields
		// Ignore the first field, which is assumed to be the title and already displayed
		// This uses the TMt_Searchable methods since we need access to the properties in the model
		$found_snippets = $this->searchSnippets($query_string);
		
		// If we have found something in the description (commonly used)
		// Then replace the current description with the snippet
		// We search the array keys using substring to account for other ending and localization
		foreach($found_snippets as $column_name => $snippet)
		{
			
			if(substr($column_name,0,11) == 'description' || $column_name == 'meta_description')
			{
				
				$description_pieces['main'] = $snippet;
			}
			else
			{
				
				$description_pieces[$column_name] = $snippet;
			}
		}
		
		//$this->addConsoleDebug($description_pieces);
		
		// Deal with trimming main description when multiple items exist
		$search_description_length = TC_getModuleConfig('search','description_length');
		if($search_description_length == 0)
		{
			$search_description_length = 200;
		}
		//$this->addConsoleDebug('search_description_length '. $search_description_length);
		
		if(count($description_pieces) > 1)
		{
			$search_description_length = round($search_description_length/2);
			
		}
		
		// Loop through each piece, combine them together
		foreach($description_pieces as $index => $piece)
		{
			$new_text = substr($piece,0,$search_description_length);
			
			if(strlen($new_text) < strlen($piece))
			{
				$new_text .= '…';
			}
			
			$description_pieces[$index] = $this->wrapQueryString($new_text, $query_string);
		}
		//$this->addConsoleDebug($description_pieces);
		
		// Combine them all together with
		$full_description_string = implode(' … ', $description_pieces);
		
		// Remove double …. Do it twice, since we can possibly get three in a row
		$full_description_string = str_replace('… …','…',$full_description_string);
		$full_description_string = str_replace('… …','…',$full_description_string);
		
		if($full_description_string == '…')
		{
			return "";
		}
		
		return $full_description_string;
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// SEARCH FUNCTIONALITY
	//
	// The main search functionality exists in this class
	// as it is all static methods
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The search field values for a searchable model name. The returned value is a nested array of values used for
	 * performing searches. The array is indexed by the field name with a value that is an array containing the
	 * search_field and the filter to be used which the word "value" is replaced by the search value
	 * @return array[]
	 */
	public static function searchQueryFields() : array
	{
		if(is_null(static::$search_query_fields))
		{
			static::$search_query_fields = [];
			foreach(static::searchFields() as $field_values)
			{
				// look for table or references such as users.title
				// field_name_search should use users.title
				// field_name is just title
				if(is_string($field_values))
				{
					$field_values = [$field_values,'%value%'];
				}
				
				// Save the values for later use in this function
				if(is_array($field_values))
				{
					$field_name_search = $field_values[0];
					$filter = $field_values[1];
				}
				
				// check if non-simple search fields have a third parameter for selection
				if(strpos($field_name_search, '(') !== false && !isset($field_values[2]))
				{
					TC_triggerError("Search field in " . get_called_class() . " with non-basic search values, require a third parameter to be provided as the column name. ");
				}
				
				
				// They set a column name in the settings
				if(isset($field_values[2]))
				{
					$column_name = $field_values[2];
				}
				// look for a table reference in the fields
				elseif(strpos($field_name_search, '.') !== false)
				{
					$parts = explode('.', $field_name_search);
					$column_name = $parts[1]; // grab teh second half
				}
				
				// Use the same for both, most common option actually
				else
				{
					$column_name = $field_name_search;
				}
				
				
				
				// Localization is involved with the non-default language
				// We need to potentially append some language suffixes
				if(!is_null(static::$current_search_alternative_language))
				{
					
					// Determine if this field is localized in the schema
					$schema = static::schema();
					if(static::schemaFieldIsLocalized($column_name))
					{
						// If this is all true, we need to append the language to the column name
						$column_name .= '_' . static::$current_search_alternative_language;
						$field_name_search .= '_' . static::$current_search_alternative_language;
					}
					
				}
				
				static::$search_query_fields[] = array(
					'field_name_search' => $field_name_search,
					'filter' => $filter,
					'column_name' => $column_name,
				);
				
			}
		}
		
		return static::$search_query_fields;
	}
	
	/**
	 * Returns the search PDF Statement for a current search string and parameters. This method is normally called
	 * from within static::search();
	 * @return PDOStatement
	 */
	protected static function searchPDOStatement() : PDOStatement
	{
		$where_values = static::searchQueryWherePortion();
		$where_snippets = array(); // doesn't include from where since that's "anded" together
		$parameters = $where_values['parameters'];
		$having_snippets = array();
		
		// Parse in any search field aliases
		$select_parts = explode('FROM', static::searchQuerySelectPortion());
		$additional_select_fields = [];
		
		foreach(static::searchFields() as $field_values)
		{
			// Only bother if we have an array and the 3rd parameter is set for the alias
			if(is_array($field_values) && isset($field_values[2]))
			{
				$additional_select_fields[] = $field_values[0].' as '.$field_values[2];
			}
		}
		
		// Rebuild the query, inserting the necessary additional fields if they exist
		$query = $select_parts[0];
		if(count($additional_select_fields) > 0)
		{
			$query .= ', '.implode(', ', $additional_select_fields);
		}
		$query .= " FROM ". $select_parts[1];
		$query .=  " WHERE ";
		
		// Insert alias select fields
		
		
		if(sizeof($where_values['where']) > 0)
		{
			$query .= implode(' AND ', $where_values['where'])." AND ";
			
		}
		
		// Add search values to the query
		$query .= "(";
		$count = 1;
		
		
		
		// Compose the query and acquire the rows from the database field
		$search_query_field_values = static::searchQueryFields();
		foreach($search_query_field_values as $search_field_values)
		{
			$filter = $search_field_values['filter'];
			// Add the full string test, for each field
			$parameters['search_'.$count] = str_replace('value',static::$current_search_string, $filter);
			if($filter == 'value') // exact match
			{
				$where_snippets[] = $search_field_values['field_name_search'].' = :search_'.$count;
			}
			else
			{
				$where_snippets[] = $search_field_values['field_name_search'].' LIKE :search_'.$count;
			}
			$count++;
			
		}
		$query .= implode(' OR ', $where_snippets);
		$query .= " )"; // end the AND ( ) that wraps all the optional searches
		
		if(static::searchQueryAfterWherePortion() !== false)
		{
			$query .= ' '.static::searchQueryAfterWherePortion();
		}
		
		
		if(is_int(static::$current_search_limit))
		{
			$query .= ' LIMIT '.static::$current_search_limit;
			
		}
		
		return static::DB_RunQuery($query, $parameters);
		
	}
	
	/**
	 * Calculates the rating for a result row a search result
	 * @param array $row A single row of results which is rated against the fields
	 * @return float
	 */
	protected static function ratingForSearchResultRow($row) : float
	{
		$rating = 0;
		$search_fields = static::searchQueryFields();
		foreach($search_fields as $search_field_values)
		{
			$field_name = $search_field_values['column_name'];
			
			// Only bother if the field name exists in the responded columns
			if(isset($row[$field_name]))
			{
				$field_name_length = strlen($row[$field_name]);
				
				
				$search_string_length = strlen(static::$current_search_string);
				
				
				$position = stripos($row[$field_name], static::$current_search_string);
				
				// Not found in string. Punish
				if($position === false)
				{
					//	$accuracy -= 0.5;
					//		$this->addConsoleDebug('Not found >> -0.5');
				}
				else // found
				{
					$search_strings_found[static::$current_search_string] = true;
					
					//$difference = $field_name_length - $search_string_length; // difference in number of letters
					if($field_name_length === 0)
					{
						$this_rating = 0;
					}
					else
					{
						$this_rating = ($search_string_length / $field_name_length);
					}
					
					
					
					//		$this->addConsoleDebug('Found >> + '.( ($search_string_length / $field_name_length)) );
					
					// start of a string
					if($position == 0)
					{
						$this_rating += 0.2; // starting means add 10 %
						//				$this->addConsoleDebug('Starts With >> + 0.1');
					}
					
					////if($this_accuracy > $accuracy)
					//{
					$rating += $this_rating;
					//}
					
					
				}
				//$cost = levenshtein($search_string, $row[$field_name], , 1, 3);
				
			}
		}
		
		return $rating;
	}
	
	/**
	 * Returns the order index of this item in relation to other items when searching. In some scenarios, the
	 * order of the models may matter. This value works similar to CSS z-index with a default of 0 and values
	 * less or greater adjusting the order in which they appear.
	 * @return int
	 */
	public static function searchOrderIndex() : int
	{
		return 0;
	}
	
	/**
	 * Generates the query for a given model name and returns the PDO statement that results from that query
	 * @param string $search_string The query string we're using
	 * @param null|int $limit The limit we should use for searching
	 * @param string|null $alternate_language The alternate language to search, which modifies fields that are
	 * searched. The default language never had underscores after table columns. We set this value to explicitly
	 * indicate a non-default language.
	 * @return TCm_Model[]
	 */
	public static function search(string $search_string, ?int $limit = null, ?string $alternate_language = null) : array
	{
		static::$current_search_string = $search_string;
		static::$current_search_limit = $limit;
		static::$current_search_alternative_language = $alternate_language;
		
		$result = static::searchPDOStatement();
		$models = [];
		
		
		while($row = $result->fetch())
		{
			$rating = static::ratingForSearchResultRow($row);
			
			// Double-check that we didn't search inside HTML tags
			// Last chance before we instantiate a model
			if(static::search_ConfirmResultsIgnoreHTMLTags($row))
			{
				
				// ---- After this point, we only concern ourselves with models that are worth saving
				$model = static::init($row);
				
				if($model->userCanView())
				{
					// Handle the Punishing of Old Items
					if($model->punishRankingForAge())
					{
						// reduce the rating by 2% per month
						$num_of_seconds_ago = strtotime('now') - strtotime($model->dateAdded());
						$num_of_months_ago = floor($num_of_seconds_ago / 2592000); // 2,592,000 = 60 * 60 * 25  *30 = num of seconds in amonth
						$rating -= ($num_of_months_ago * 0.01);
					}
					
					$rating += 1; // move it into the positive no matter what
					
					// scale and round to a 3-digit string from 0 to 100
					$rating = str_pad(round($rating * 100), 4, '0', STR_PAD_LEFT);
					
					$models[$rating . '--' . get_called_class() . '--' . $model->id()] = $model;
				}
				else
				{
					//	$model->addConsoleDebugObject('User cannot view ',$model);
				}
			}
			
			
		}
		
		// Sort the models internally for each class
		krsort($models);
		
		return $models;
	}
	
}
