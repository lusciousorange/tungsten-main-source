class TMv_SearchForm extends TCv_Form {


	search_field 				= null;
	target_element				= null;
	timeout						= null;
	more_specific_box			= null;

	fetch_abort_controller		= null;

	options =  {

		use_dynamic_loading			: true,
		view_class_name 			: '',
		update_target 				: '',
		searchable_model_names 		: false,
		more_specific_text			: 'Please be more specific',
		hide_with_off_click 		: true,

	};



	constructor(element, params) {
		super(element, params);

		Object.assign(this.options, params);

		if(this.options.use_dynamic_loading)
		{
			// Target isn't inside the form, must use document selector
			this.target_element = document.querySelector(this.options.update_target);
			this.element.addEventListener('submit', this.formSubmitted.bind(this));


			this.search_field = this.element.querySelector(".q_row input");
			this.search_field.addEventListener('input', this.searchFieldChanged.bind(this));
		}




	}

	/**
	 * Event handler when form submitted
	 * @param {Event} event
	 */
	formSubmitted(event) {
		event.preventDefault();
	}

	/**
	 * Event handler for whenever the search field changes
	 * @param {Event} event
	 */
	searchFieldChanged(event) {
		let search_string = this.search_field.value;

		// Only bother if we have at least three characters
		if (search_string.length >= 3)
		{
			// Hide the box immediately
			this.hideMoreSpecific();

			// 1/2 second pause before triggering the call to deal with typing
			if (this.timeout != null)
			{
				clearTimeout(this.timeout);
			}
			this.timeout = setTimeout(this.updateWithFilters.bind(this), 500);
		}
		else
		{
			this.showMoreSpecific();
			this.element.classList.remove('tungsten_loading');
		}


	};

	showMoreSpecific() {
		if(this.more_specific_box === null)
		{
			this.more_specific_box = document.createElement('div');
			this.more_specific_box.className = 'TMv_SearchResults';
			let inside = document.createElement('div');
			inside.className = 'more_specific';
			inside.textContent = this.options.more_specific_text;
			this.more_specific_box.append(inside);
		}


		this.target_element.innerHTML = '';
		this.target_element.append(this.more_specific_box);

	};

	hideMoreSpecific() {

		if(this.more_specific_box)
		{
			this.more_specific_box.hide();
		}

	}


	/**
	 * Updates the responses by triggering the AJAX call to /admin/search/do/search which then returns the response
	 */
	updateWithFilters() {

		this.element.classList.add('tungsten_loading');

		// Handle abort controller
		if(this.fetch_abort_controller !== null)
		{
			this.fetch_abort_controller.abort();
		}
		this.fetch_abort_controller = new AbortController();

		let params = new URLSearchParams();


		params.append('q', this.search_field.value);
		params.append('view_name',this.options.view_class_name);
		if (this.options.searchable_model_names !== false)
		{
			params.append('searchable_model_names',this.options.searchable_model_names);
		}

		// Generate the URL
		let url = "/admin/search/do/search?" + params;

		fetch(url, {
			method : 'GET',
			signal : this.fetch_abort_controller.signal, // pass the abort controller signal

		}).then(response => response.json())
			.then(response =>
			{
				// Show the results
				this.target_element.show();


				// UPDATE CSS:  the list css if it's set
				processAsyncCSSLinks(response.css);
				this.target_element.innerHTML = response.html;
			})
			.catch((error) =>
			{
				console.log(error);
			})
			.finally(() =>
			{
				this.fetch_abort_controller = null; // disable the abort controller
				this.element.classList.remove('tungsten_loading');
			});



	}

}