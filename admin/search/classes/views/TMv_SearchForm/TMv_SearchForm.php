<?php

/**
 * Class TMv_SearchForm
 *
 * A form that shows the site-wide search
 */
class TMv_SearchForm extends TCv_Form
{
	protected $update_target_id = 'search_form_results';
	protected $hide_with_off_click = true;
	protected $search_icon_code = 'fas fa-search';
	protected $search_field_id = 'q';
	
	protected bool $load_results_dynamically = true;
	
	/**
	 * @var string
	 * The class name for the results given for a single search item
	 */
	protected $result_box_view_name = 'TMv_SearchResultBox';
	protected $searchable_model_names = null;
	
	protected $placeholder_text = 'Search';
	
	/**
	 * TMv_SearchForm constructor.
	 * @param string $id
	 * @param bool $load_form_tracking
	 */
	public function __construct($id = 'search', $load_form_tracking = false)
	{
		parent::__construct($id, $load_form_tracking);
		
		$this->addClassCSSFile('TMv_SearchForm');
		
		$this->setPlaceholderText(TC_localize('search','Search'));
		
		$this->setAttribute('role','search');
		
		$this->setMethod('GET');
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField($this->search_field_id, TC_localize('search','Search'));
		$field->setFieldType('search');
		$field->setIDAttribute($this->id().'_q');
		$field->setShowTitleColumn(false);
		$field->disableAutoComplete();
		$field->setPlaceholderText($this->placeholder_text);
		$this->attachView($field);
	
		$this->loadSearchFormJS();
		
	}
	
	/**
	 * Sets the placeholder text for the search field
	 * @param string $text
	 */
	public function setPlaceholderText(string $text)
	{
		$this->placeholder_text = $text;
	}
	
	/**
	 * Sets the code for the search icon
	 * @param string $code
	 */
	public function setSearchIconCode($code)
	{
		$this->search_icon_code = $code;
	}

	/**
	 * @return TCv_FormItem_SubmitButton
	 */
	public function submitButton()
	{
		$submit = new TCv_FormItem_SubmitButton($this->id().'_submit',
		                                        '<i class="'.$this->search_icon_code.'"></i><span hidden>'
		                                        .TC_localize('search','Search') .'</span>');
		$submit->setUseButtonTag();
		$submit->addFormElementCSSClass('search_submit');
		return $submit;
	}

	/**
	 * Sets the ID of the update target
	 * @param $update_target_id
	 */
	public function setUpdateTargetID($update_target_id)
	{
		$this->update_target_id = $update_target_id;
	}
	
	/**
	 * Indicate the view name that should be loaded for search results. This is view is for an SINGLE search response
	 * and most likely should extend or at least look similar to TMv_SearchResultBox
	 * @param $view_name
	 * @see TMv_SearchResultBox
	 */
	public function setResultViewName($view_name)
	{	
		$this->result_box_view_name = $view_name;
	}

	/**
	 * Indidate the names of models to be used for search results.
	 * @param string[] $model_names
	 */
	public function setModelsToSearch($model_names)
	{
		$this->searchable_model_names = $model_names;
	}
	
	/**
	 * Turns off the functionality where the form hides if we click off the results
	 */
	public function disableHidingWithOffClick() : void
	{
		$this->hide_with_off_click = false;
	}
	
	/**
	 * Turns ON the functionality where the form hides if we click off the results
	 */
	public function enableHidingWithOffClick() : void
	{
		$this->hide_with_off_click = true;
	}
	
	/**
	 * Disables the functionality that shows the results dynamically
	 * @return void
	 */
	public function disableDynamicResults()
	{
		$this->load_results_dynamically = false;
		$this->addJSClassInitValue('use_dynamic_loading',false);
	}
	
	public function loadSearchFormJS()
	{
		//$this->addConsoleDebug('render');
		
		// Load the file no matter what, JS for the form will load regardless
		$this->addClassJSFile('TMv_SearchForm');
		
			
		$this->updateJSClassInitValues([
			'update_target' => '#' . $this->update_target_id,
			'view_class_name' => $this->result_box_view_name,
			'more_specific_text' => TC_localize('search_more_specific', 'Please be more specific'),
			'hide_with_off_click' => $this->hide_with_off_click
		]);
		
		if(is_array($this->searchable_model_names))
		{
			$model_name_string = implode(",", $this->searchable_model_names);
			$this->addJSClassInitValue('searchable_model_names', $model_name_string);
			
		}
			
			
		
		
	}
}