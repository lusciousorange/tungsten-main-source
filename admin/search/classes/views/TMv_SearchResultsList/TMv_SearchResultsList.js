/**
 * Class for search results that extends the default Searchable model list
 */
class TMv_SearchResultsList extends TCv_SearchableModelList {
	constructor(element, params) {
		super(element, params);
	}

	/**
	 * Updates the list with filters
	 * @param clear_pagination
	 */
	updateWithFilters(clear_pagination ) {

		// Clear the counts
		this.element.querySelectorAll('.search_result_count').forEach(el => {
			el.textContent = '–';
		});

		super.updateWithFilters(clear_pagination);


	}

	/**
	 * Extend the hook method so we can deal with the counts
	 * @param response
	 */
	hook_processUpdateResponse(response) {

		for(let class_name in response.counts)
		{
			let num_box = this.element.querySelector('#search_'+class_name+'_container .search_result_count');
			if(num_box)
			{
				num_box.textContent = response.counts[class_name];
			}

		}
	}
}

