<?php

/**
 * Filterable list of itemsthat does a system-wide search
 */
class TMv_SearchResultsList extends TCv_SearchableModelList
{
	use TMt_PagesContentView;
	
	protected array $counts = [];
	
	public function __construct()
	{
		parent::__construct();
		
		$this->defineColumns();
		$this->setPagination(25);
		
		$this->addClassCSSFile();
		$this->addClassJSFile('TMv_SearchResultsList');
		
		$this->setModelListClass('TMm_Searcher', 'processFilterSearch');
		
		
	}
	
	/**
	 * Extend the functionality to set the ID for the row to avoid conflicts with different models of the same ID
	 * @param $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		// Change the ID for the row to avoid conflicts
		$row->setID('row_'.$model->contentCode());
		
		return $row;
	}
	
	public function defineColumns()
	{
		
		
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$column->setWidthAsPercentage(30);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('description');
		$column->setTitle('Description');
		$column->setContentUsingListMethod('descriptionColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('type');
		$column->setTitle('Type');
		$column->setContentUsingListMethod('typeColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(120);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('icon');
		$column->setTitle('');
		$column->setContentUsingListMethod('iconColumn');
		$column->setWidthAsPixels(30);
		$column->setAlignment('right');
		$this->addTCListColumn($column);
		
		// Show the open link if possible
		if(TC_isTungstenView())
		{
			$column = new TCv_ListColumn('open-page');
			$column->setTitle('Page');
			$column->setContentUsingListMethod('openPageColumn');
			$column->setWidthAsPixels(80);
			$column->setAlignment('center');
			$this->addTCListColumn($column);
			
		}
		
		
	}
	
	/**
	 * Returns the query string used for this search results
	 * @return string
	 */
	protected function queryString() : string
	{
		$query_string = '';
		if(isset($_GET['form_filter_search']))
		{
			$query_string = $_GET['form_filter_search'];
		}
		
		return $query_string;
	}
	
	/**
	 * @param TCm_Model|TMt_Searchable $model
	 * @return TMv_SearchResultIcon
	 */
	public function descriptionColumn($model)
	{
		
		return $model->searchDescription($this->queryString());
		//return new TMv_SearchResultIcon($model);
		
	}


	
	
	/**
	 * @param TCm_Model|TMt_Searchable $model
	 * @return TMv_SearchResultIcon
	 */
	public function iconColumn($model)
	{
		return '<i class="fas ' . get_class($model)::searchIconCode() . '"></i>';
		//return new TMv_SearchResultIcon($model);
		
	}
	
	
	/**
	 * @param TCm_Model|TMt_Searchable $model
	 * @return TCv_Link|string
	 */
	public function titleColumn($model)
	{
		$link = null;
		if(TC_isTungstenView())
		{
			$link = new TCv_Link();
			$link->setURL($model->adminEditURL());
			
		}
		elseif(method_exists($model, 'pageViewURLPath'))
		{
			$link = new TCv_Link();
			$link->setURL($model->pageViewURLPath());
			
		}
		
		// If we have a link
		if(!is_null($link))
		{
			$link->addText($model->searchTitleWithQueryWrap($this->queryString()));
		
			$subtitle = $model->searchSubtitle();
			if($subtitle != '')
			{
				$link->addText('<span class="subtitle">'.$subtitle.'</span>');
				
			}
			return $link;
		}
		
		return $model->searchTitle();
		
		
	}
	
	
	
	public function openPageColumn($model)
	{
		if(method_exists($model, 'pageViewURLPath'))
		{
			$link = new TCv_Link();
			$link->setURL($model->pageViewURLPath());
			$link->openInNewWindow();
			$link->setIconClassName('fa-external-link');
			return $link;
		}
		return null;
	}
	
	
	/**
	 * @param TCm_Model|TMt_Searchable $model
	 * @return void
	 */
	public function typeColumn($model)
	{
		return get_class($model)::modelTitleSingular();
	}
	
	
	/**
	 * Function that is called which sets up the list of filters used. By default,it includes the search field and nothing else.
	 *
	 * This method can be overridden to define other filters
	 */
	public function defineFilters()
	{
		parent::defineFilters();

		
		$group = new TCv_FormItem_Group('search_filter_types');
		
		
		
		// Loop through them all, ignore any parent
		$relevant_model_names = TMm_Searcher::searchableClassNames();
		
		foreach($relevant_model_names as $model_name)
		{
			$title = '<i class="fas ' . $model_name::searchIconCode() . '"></i>' . $model_name::modelTitlePlural();
			$title .= '<span class="search_result_count">–</span>';
			$checkbox = new TCv_FormItem_Checkbox('search_' . $model_name, $title);
			$checkbox->useTitleForLabel();
			$checkbox->setDefaultValue(true);
			$group->attachView($checkbox);
		}
		
		$this->addFilterFormItem($group);
		
	}
	
	/**
	 * Extend this functionality to pull out the counts
	 * @param $filter_values
	 * @param $result
	 * @return array
	 */
	public function returnValueForFilterResult($filter_values, $result) : array
	{
		
		// Grab the returned count values, save them and remove them from the results
		if(isset($result['counts']))
		{
			$this->counts = $result['counts'];
			unset($result['counts']);
		}
		return parent::returnValueForFilterResult($filter_values, $result);
	}
	
	/**
	 * Processes the values provided via the ajax searching script and returns the values to be returned
	 * @param TCm_ModelList $model_list
	 * @param array $params
	 *
	 * @return array
	 */
	public function processFilterValues(array $params) : array
	{
		// Add the count values to the return values
		$return_values = parent::processFilterValues($params);
		$return_values['counts'] = $this->counts;
		return $return_values;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The title for this view
	 * @return string
	 */
	public static function pageContent_ViewTitle() : string
	{
		return 'Search Results List';
	}
	
	/**
	 * A description of the view that appears in the interface to add views
	 * @return string
	 */
	public static function pageContent_ViewDescription() : string
	{
		return 'The list of search results that appears on the search page';
	}
	
	/**
	 * Indicates if the preview is shown in the builder. If true, it renders the view, otherwise it shows a grey box
	 * @return bool
	 */
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false;
	}
	
	
}