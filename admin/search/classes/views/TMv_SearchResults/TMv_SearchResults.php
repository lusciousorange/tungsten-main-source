<?php
/**
 * Class TMv_SearchResults
 *
 * A view that shows search results for a provided query value via $_GET['q'].
 */

class TMv_SearchResults extends TCv_View
{
	protected $model = false;
	/**
	 * @var string
	 * The view class name to be used for each search result item
	 */
	protected $view_name = 'TMv_SearchResultBox';

	/** @var bool|string[] $searchable_model_names*/
	protected $searchable_model_names = false;

	public function __construct()
	{
		parent::__construct();
		
		// Determine the view name and if it should be overridden
		if(isset($_GET['view_name']) && $_GET['view_name'] != '')
		{
			$this->view_name = htmlentities($_GET['view_name']);
		}
		if(isset($_GET['searchable_model_names']) && $_GET['searchable_model_names'] != '')
		{
			$this->searchable_model_names = explode(',', $_GET['searchable_model_names']);
		}


		$this->addClassCSSFile('TMv_SearchResults');
	}
	
	/**
	 * Generates the title view used when search groups responses and injects strings in the middle of
	 * @param string|TCm_Model $class_name
	 * @return TCv_View
	 */
	protected function groupTitleView($class_name)
	{
		$view = new TCv_View();
		$view->setTag('h3');
		$view->addClass('group_title_view');
		
		// The
		if(class_exists($class_name))
		{
			$view->addClass('title_'.$class_name);
			$view->addText($class_name::modelTitlePlural());
		}
		else
		{
			$view->addText($class_name);
		}
		return $view;
	}
	
	public function render()
	{
		$searcher = TMm_Searcher::init();

		if ($this->searchable_model_names != false && is_array($this->searchable_model_names))
		{
			$searcher->setSearchableModelNames($this->searchable_model_names);
		}

		$result_block = new TCv_View();

		
		$query = '';
		if(isset($_GET['q']))
		{
			$query = $_GET['q'];
		}
		
		
		// Get the results of the search
		$results = $searcher->search($query);
		if(isset($results['counts']))
		{
			unset($results['counts']);
		}
		$view_class_name = $this->overrideClassName($this->view_name);
	
		// Loop through all the results
		foreach($results as $index => $model)
		{
			// Detect if we've been given a title, at which point we inject a title block, instead of generating a
			// search results for a model that won't exist
			if(is_string($model))
			{
				$result_block->attachView($this->groupTitleView($model));
			}
			else
			{
				// Cannot use ::init() since it will find duplicate IDs that conflict
				$view = new $view_class_name($model);
				if(method_exists($view, 'setSearchQuery'))
				{
					$view->setSearchQuery($query);
				}
				$result_block->attachView($view);
			}
		}

		if(count($results) == 0)
		{
			$result_block->addClass('no_results');
			$result_block->addText(TC_localize('search_no_results','No Results Found'));
		}

		$this->attachView($result_block);

	}
}