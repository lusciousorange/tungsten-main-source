<?php
/**
 * Class TMv_SearchResultBox
 */
class TMv_SearchResultBox extends TCv_Link
{
	protected $model, $query_string;

	/**
	 * TMv_SearchResultBox constructor.
	 * @param TMt_Searchable|TCm_Model $found_model
	 */
	public function __construct($found_model)
	{
		parent::__construct();
		$this->model = $found_model;
		$this->addClassCSSFile('TMv_SearchResultBox');
		$this->addDataValue('class-name', get_class($found_model));
	}

	/**
	 * @return TMt_Searchable|TCm_Model
	 */
	public function model()
	{
		return $this->model;
	}
	
	/**
	 * Sets the query string for this result box, which is used to show and filter content
	 * @param string $query_string
	 */
	public function setSearchQuery(string $query_string)
	{
		$this->query_string = htmlentities(strip_tags($query_string));
	}
	

	
	/**
	 * Returns the icon used in search
	 * @return false|TCm_File|TCv_Image|TCv_View
	 */
	public function searchIconView()
	{
		$icon_image_file =  $this->model()->searchIconImageFile();
		if($icon_image_file instanceof TCm_File)
		{
			if($icon_image_file->isImage())
			{
				$image = new TCv_Image(false, $icon_image_file);
				$image->scaleCenteredInsideBox(50,50);
				return $image;
			}
		}
		elseif($icon_image_file instanceof TCv_View)
		{
			return $icon_image_file;

		}
		elseif(is_string($icon_image_file))
		{
			$string_view = new TCv_View();
			if(substr($icon_image_file, 0,2) == 'fa')
			{
				$string_view->setTag('i');
				$string_view->addClass($icon_image_file);
			}
			else // text, so we'll push it through
			{
				$string_view->addText($string_view);
			}
			return $string_view;
		}

		return false;

	}

	/**
	 * The title that will display for search result
	 * @return string
	 */
	public function titleToDisplay()
	{
		return strip_tags($this->model->searchTitle());
	}
	
	/**
	 * Returns the URL that this result sends the user to
	 * @return string
	 */
	protected function searchResultURL() : string
	{
		$model = $this->model();
		
		$url = $model->searchViewURL();
		if ((gettype($url) == 'string' && $url != '#' ) || (gettype($url) == 'array' && $url['local'] == false))
		{
			return $url;
		}
		elseif(isset($url['url']))
		{
			return $url['url'];
			
		}
		else
		{
			return '';
		}
		
		
	}
	
	
	public function render()
	{
		$this->setURL($this->searchResultURL());
	
		$icon_container = new TCv_View();
		$icon_container->addClass('search_icon_container');


		$icon_container->attachView($this->searchIconView());
		$this->attachView($icon_container);

//		$this->setIconClassName($this->model()->moduleForThisClass()->iconCode());




		$explanation = new TCv_View();
		$explanation->addClass('explanation');

		$explanation->attachView($this->titleView());

		$explanation->attachView($this->subtitleView());
		
		$explanation->attachView($this->descriptionView());
		
		$this->attachView($explanation);


	}
	
	/**
	 * Returns the title box
	 * @return bool|string|TCv_View
	 */
	protected function titleView()
	{
		$title = new TCv_View();
		$title->setTag('span');
		$title->addClass('title');
		$title->addText( $this->model->wrapQueryString($this->titleToDisplay(), $this->query_string));
		
		return $title;
	}
	
	/**
	 * @return TCv_View
	 */
	protected function subtitleView()
	{
		$subtitle = new TCv_View();
		$subtitle->addClass('search_subtitle');
		
		$subtitle_item = $this->model->searchSubtitle();
		
		// Don't bother if we don't have one
		if(is_null($subtitle_item))
		{
			return null;
		}
		
		if($subtitle_item instanceof TCv_View)
		{
			$subtitle->attachView($subtitle_item);
		}
		else
		{
			$subtitle->addText($subtitle_item);
		}
		
		return $subtitle;
	}
	
	/**
	 * Returns the description view for the search results
	 * @return TCv_View
	 */
	protected function descriptionView()
	{
		// The description shows additional details, trimmed to a specific length
		$view = new TCv_View();
		$view->addClass('search_description');
		
		$view->addText($this->model->searchDescription($this->query_string));
		
		return $view;

	}
	
	
}