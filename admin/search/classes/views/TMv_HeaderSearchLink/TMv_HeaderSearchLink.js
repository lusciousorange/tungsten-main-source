class TMv_HeaderSearchLink {
	element = null;
	button = null;
	flyout = null;
	options = {
	};

	constructor(element, params) {
		this.element = element;
		Object.assign(this.options, params);

		// Track the button and flyout elements
		this.button = this.element.querySelector('.header_search_toggle_button');
		this.flyout = this.element.querySelector('.TMv_SearchFlyout');

		// Handle clicks on the button
		this.button.addEventListener('click', this.buttonClick.bind(this));

		// Handle clicks outside this view
		this.addOutsideClickDetection();

	}

	/**
	 * Event handler for any clicks on the button, toggles the style and flyout
	 * @param event
	 */
	buttonClick(event) {
		event.preventDefault();
		this.button.classList.toggle('selected_menu');
		this.flyout.toggle();

		// If we've selected
		if(this.button.classList.contains('selected_menu'))
		{
			this.flyout.querySelector('form input').focus();
		}
	}

	/**
	 * Handles detection of clicks outside this element, hiding the search if that happens
	 */
	addOutsideClickDetection() {

		document.addEventListener('click', (event) =>{

			let search_link = event.target.closest('.header_search_link');
			// We clicked outside of anything that's part of this link
			if(search_link !== this.element)
			{
				this.flyout.hide();
				this.button.classList.remove('selected_menu');
			}

		});

	}
}