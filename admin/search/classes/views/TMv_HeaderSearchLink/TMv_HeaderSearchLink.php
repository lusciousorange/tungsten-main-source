<?php

/**
 * A link added to the header which shows the search flyout
 */
class TMv_HeaderSearchLink extends TCv_View
{
	protected string $search_flyout_class = 'TMv_SearchFlyout';
	protected string $search_url = '/search/';
	
	protected ?string $result_box_view_name = null;
	
	protected ?TCv_Link $search_link = null;
	public function __construct($id = 'header_search_link')
	{
		parent::__construct($id);
		$this->addClassCSSFile('TMv_HeaderSearchLink');
		$this->setTag('span');

		$this->search_link  = new TCv_Link();
		$this->search_link->addClass('sm_link');
		$this->search_link->addClass('header_search_toggle_button');
		$this->search_link->addHiddenTitle('Search');
		$this->search_link->setAttribute('aria-label','Search');
		$this->search_link->setIconClassName('fas fa-search');
		
		
	}
	
	/**
	 * Indicate the view name that should be loaded for search results. This is view is for an SINGLE search response
	 * and most likely should extend or at least look similar to TMv_SearchResultBox
	 * @param string $view_name
	 * @see TMv_SearchResultBox
	 */
	public function setResultViewName(string $view_name) :void
	{
		$this->result_box_view_name = $view_name;
	}
	
	
	/**
	 * Sets the search flyout class name that should be shown
	 * @param string $class_name
	 * @return void
	 */
	public function setFlyoutClassName(string $class_name) : void
	{
		$this->search_flyout_class = $class_name;
	}
	
	/**
	 * Adds a class to the link that shows the search icon
	 * @param string $class
	 * @return void
	 */
	public function addLinkClass(string $class)
	{
		$this->search_link->addClass($class);
	}
	
	public function render()
	{
		$this->search_link->setURL($this->search_url);
		$this->attachView($this->search_link);

		$search_results = ($this->search_flyout_class)::init('header_search_flyout');
		
		// Pass the result box name through
		if(!is_null($this->result_box_view_name))
		{
			$search_results->setResultViewName($this->result_box_view_name);
		}
		$this->attachView($search_results);
		
		$this->addClassJSFile('TMv_HeaderSearchLink');
		$this->addClassJSInit('TMv_HeaderSearchLink');
		
		
		
	}



}