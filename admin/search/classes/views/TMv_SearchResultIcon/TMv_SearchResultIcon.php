<?php
class TMv_SearchResultIcon extends TCv_View
{
	protected $model;
	
	public function __construct($searchable_model)
	{
		parent::__construct();
		
		$this->model = $searchable_model;
	}
	
	
	/**
	 * Renders the view
	 * @return void
	 */
	public function render() : void
	{
		$icon_image_file =  $this->model->searchIconImageFile();
		if($icon_image_file instanceof TCm_File)
		{
			if($icon_image_file->isImage())
			{
				$image = new TCv_Image(false, $icon_image_file);
				$image->scaleCenteredInsideBox(50,50);
				$this->attachView($image);
				return;
			}
		}
		elseif($icon_image_file instanceof TCv_View)
		{
			$this->attachView($icon_image_file);
			return;
			
		}
		elseif(is_string($icon_image_file))
		{
			$string_view = new TCv_View();
			if(substr($icon_image_file, 0,2) == 'fa')
			{
				$string_view->setTag('i');
				$string_view->addClass($icon_image_file);
			}
			else // text, so we'll push it through
			{
				$string_view->addText($icon_image_file);
			}
			$this->attachView($string_view);
			return;
		}
		
		
	}
}
