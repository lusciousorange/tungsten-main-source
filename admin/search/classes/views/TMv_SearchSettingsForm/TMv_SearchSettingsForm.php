<?php
class TMv_SearchSettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('restrict_to_user_modules', 'Restrict to user modules');
		$field->setHelpText('Indicate if search should be restricted to logged in users AND limited to the modules they can see');
		$field->addOption('0', 'No - Search shows all options');
		$field->addOption('1', 'Yes - Search restricted to user modules');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_TextField('description_length', 'Description length');
		$field->setHelpText('The number of characters that the description should be trimmed to');
		$field->setDefaultValue(200);
		$this->attachView($field);




	}
	

}
?>