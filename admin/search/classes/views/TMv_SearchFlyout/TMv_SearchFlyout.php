<?php

/**
 * Class TMv_SearchFlyout
 *
 * A view that contains the flyout box that is commonly used on sites when enabling search from a header or a small
 * icon.
 *
 * Developer is responsible for positioning and sizing of the flyout however the internal operation will handle the
 * rest.
 */
class TMv_SearchFlyout extends TCv_View
{
	protected $update_target_id;
	
	protected ?string $result_box_view_name = null;
	
	/**
	 * TMv_SearchFlyout constructor.
	 * @param $id
	 */
	public function __construct($id)
	{	
		parent::__construct($id);
		$this->addClassCSSFile('TMv_SearchFlyout');
		$this->update_target_id = $this->id().'_search_form_results';
	}
	
	/**
	 * Indicate the view name that should be loaded for search results. This is view is for an SINGLE search response
	 * and most likely should extend or at least look similar to TMv_SearchResultBox
	 * @param string $view_name
	 * @see TMv_SearchResultBox
	 */
	public function setResultViewName(string $view_name) :void
	{
		$this->result_box_view_name = $view_name;
	}
	
	
	/**
	 * Returns the search form that will be rendered in this view
	 * @return TMv_SearchForm
	 */
	public function searchForm() : TMv_SearchForm
	{
		$search_form = TMv_SearchForm::init($this->id.'_search_form');
		$search_form->setUpdateTargetID($this->update_target_id);
		$search_form->disableHidingWithOffClick();
		
		if(!is_null($this->result_box_view_name))
		{
			$search_form->setResultViewName($this->result_box_view_name);
		}
		
		return $search_form;
	}
	
	public function render() : void
	{
		$this->attachView($this->searchForm());

		$update_target = new TCv_View($this->update_target_id);
		$update_target->addClass('search_form_results');
		$this->attachView($update_target);


	}



}