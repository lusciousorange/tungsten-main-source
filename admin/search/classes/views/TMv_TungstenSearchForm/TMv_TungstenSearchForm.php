<?php
class TMv_TungstenSearchForm extends TMv_SearchForm
{
	public function __construct($id = 'search')
	{
		parent::__construct($id, false);
		
		$this->setMethod('GET');
		
		$this->disableDynamicResults();
		// Sets the action to fill in the form filter search
		$this->setAction('/admin/search/?form_filter_search=');
		
		$this->addClassCSSFile('TMv_TungstenSearchForm');
		
	}
	
	public function configureFormElements()
	{
		$this->search_field_id = 'form_filter_search';
		$this->placeholder_text = TC_localize('search','Search');
		
		parent::configureFormElements();
		
		
		
	}
	
	public function html($show_form = true)
	{
		if(!TC_moduleWithNameInstalled('search'))
		{
			return '';
		}
		
		return parent::html();
		
		
	}
}