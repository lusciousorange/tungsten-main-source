<?php

/**
 * Class TMm_DashboardList
 */
class TMm_DashboardList extends TCm_ModelList
{
	/**
	 * TMm_DashboardList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_DashboardItem',true);
		
	}

	/**
	 * Retunrns the list of models
	 * @param $subset_name
	 * @return TMm_DashboardItem[]
	 */
	public function models($subset_name = false)
	{
		return parent::models();
	}

	

}

?>