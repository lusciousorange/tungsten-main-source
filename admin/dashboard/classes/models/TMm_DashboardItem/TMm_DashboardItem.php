<?php

/**
 * Class TMm_DashboardItem
 */
class TMm_DashboardItem extends TMm_PageRendererContent
{
	use TMt_PageRenderItem;

	protected ?string $dashboard_title = null;
	//protected ?string $dashboard_name = null;
	protected ?string $dashboard_color = null;
	
	protected $width;
	protected ?string $view_all_url = null;
	protected ?string $view_all_title = null;
	protected $container_content = false;

	// DATABASE TABLE SETTINGS
	public static $model_title = 'Dashboard item' ;
	public static ?string $mirror_table_suffix = null;
	
	
	/**
	 * TMm_DashboardItem constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		if(!$this->exists) { return null; }
	}

	/**
	 * Returns the upload folder path
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		return TC_getConfig('saved_file_path').'/dashboards/';
	}

	/**
	 * Returns the title
	 * @return string
	 */
	public function title() : string
	{
		return $this->dashboard_title;
	}

	/**
	 * Returns the width of the dashboard item
	 * @return string
	 */
	public function width()
	{
		return $this->width;
	}

	/**
	 * Returns the backgroundColor
	 * @return string
	 */
	public function backgroundColor()
	{
		return $this->dashboard_color;
	}
	
	/**
	 * @return string|null
	 */
	public function viewAllURL() : ? string
	{
		return $this->view_all_url;
	}
	
	/**
	 * @return string|null
	 */
	public function viewAllTitle() : ?string
	{
		return $this->view_all_title;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Confirm that this indicates it's installing in this module
	 * @return string
	 */
	public static function installModuleFolder() : string
	{
		return 'dashboard';
	}
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema() : array
	{
		return parent::schema() + [
				'dashboard_title' => [
					'title'         => 'Dashboard Title',
					'comment'       => 'The title for the dashboard content item',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
//				'dashboard_name' => [
//					'title'         => 'Dashboard Name',
//					'comment'       => 'The name for the dashboard content item',
//					'type'          => 'varchar(255)',
//					'nullable'      => true,
//				],
				'dashboard_color' => [
					'title'         => 'Dashboard color',
					'comment'       => 'The name for the dashboard content item',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'view_all_url' => [
					'title'         => 'View All URL',
					'comment'       => 'The URL for viewing all the items for a dashboard',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'view_all_title' => [
					'title'         => 'View All title',
					'comment'       => 'The title for the view all button ',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'width' => [
					'title'         => 'Width',
					'comment'       => '',
					'type'          => 'smallint(5)',
					'nullable'      => false,
				],
			
			];
	}
	
	/**
	 * An associative array of queries that should be run if we're upgrading from an older version. The indices
	 * should be version number that it should apply to if we're passing that value. So if the index is `2.0.1`
	 * Then if we're installing from `2.0.0` or earlier, this will run AFTER other updates.
	 *
	 * The value for each index should be an array itself since multiple queries might be required.
	 * @return array
	 */
	public static function installUpdateQueries()
	{
		return [
			"2.2.0" => [
				"UPDATE pages_renderer_content SET item_class_name = 'TMm_Dashboard' WHERE item_class_name = 'TMm_DashboardList'"
				]
		];
	}
	
}
