<?php

/**
 * Represents a dashboard in the system. often there is only one, but other dashboards can be setup as needed.
 */
class TMm_Dashboard extends TCm_Model
{
	use TMt_PageRenderer;
	
	protected ?string $model_class_name = null;
	
	/**
	 * TMm_DashboardList constructor.
	 * @param string $id. The unique name for this dashboard. Each dashboard needs a unique name to ensure no
	 * conflicts with the page builder.
	 
	 * Tungsten's primary dashboard which is always the default, which is `main`
	 */
	public function __construct($id = 'main')
	{
		// Convert to new page_renderer_content system
		$this->convertToPageRendererSystem();
		
		parent::__construct($id);
		
		// Get the dashboard values
		$json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/admin/dashboard/config/dashboards.json');
		$values = json_decode($json, true);
		if(isset($values[$id]))
		{
			$this->model_class_name = $values[$id]['model_class_name'];
		}
		
	}
	
	/**
	 * Returns the model class name used for this dashboard.
	 * @return string|null
	 */
	public function modelClassName(): ?string
	{
		return $this->model_class_name;
	}
	
	
	/**
	 * Returns the name of the class that uses the TMt_PageRenderItem
	 * @return string
	 */
	public static function pageRenderItemClassName()
	{
		return 'TMm_DashboardItem';
	}
	
	/**
	 * Returns the title for this model.
	 *
	 * This method is commonly overloaded for each class to return a relevant string for the object.
	 *
	 * @return string
	 */
	public function title()
	{
		return 'Dashboard '.$this->id();
	}
	
	/**
	 * Returns the ID for the renderer.
	 * @return string
	 */
//	public function rendererID() : string
//	{
//		return $this->id();
//	}
	
	/**
	 * Disables the page theme styling on the dashboard
	 * @return bool
	 */
	public function usePageThemeStyling()
	{
		return false;
	}
	
	/**
	 * Returns the name of the form view class that is used to edit/create TMt_PageRenderItem
	 * @return string
	 */
	public function pageRenderItem_ContentFormClassName()
	{
		return 'TMv_DashboardContentForm';
	}
	
	/**
	 * Performs the necessary updates to convert the old dashboard tables into the proper pages_renderer. This is
	 * entirely for legacy support and to transition to the new system without manual MySQL work.
	 */
	public function convertToPageRendererSystem()
	{
		$dashboard_module = TSm_Module::init('dashboard');
		$pages_module = TSm_Module::init('pages');
		
		if(!TC_getModuleConfig('dashboard','upgraded') &&
			$dashboard_module->installed() &&
			$pages_module->installed() &&
			$dashboard_module->version() >= '2.1.0' &&
			$pages_module->version() >= '6.2.4' &&
			TCm_Database::tableInstalled('dashboards') // only bother if it exists
		)
		{
			
			$error_found = false;
			$query = "SELECT * FROM `dashboards`";
			$result = $this->DB_Prep_Exec($query);
			while($row = $result->fetch())
			{
				// Create the new item
				$row['item_id'] = '0';
				$row['item_class_name'] = 'TMm_Dashboard';
				$row['content_id'] = $row['dashboard_id'];
				unset($row['dashboard_id']); // now content_id
				unset($row['display_order']); // legacy, not needed
				$content_item = TMm_PageRendererContent::createWithValues($row);
				if(!$content_item)
				{
					$error_found = true;
				}
			}
			
			if(!$error_found)
			{
				$query = "SELECT * FROM `dashboard_variables`";
				$result = $this->DB_Prep_Exec($query);
				while($row = $result->fetch())
				{
					
					// Create the new item
					$row['variable_id'] = $row['match_id'];
					$row['content_id'] = $row['dashboard_id'];
					unset($row['dashboard_id']); // now content_id
					unset($row['match_id']); // now content_id
					
					$query = "INSERT INTO pages_renderer_content_variables SET ";
					$parts = array();
					foreach($row as $column => $value)
					{
						$parts[] = ' '.$column.' = :'.$column;
					}
					
					$query .= implode(', ', $parts);
					$this->DB_Prep_Exec($query, $row);
				}
			}
			
			if(!$error_found)
			{
				// Delete the tables
				$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 0');
				$this->DB_Prep_Exec("DROP TABLE IF EXISTS dashboard_variables");
				$this->DB_Prep_Exec("DROP TABLE IF EXISTS dashboards");
				$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 1');
				
			}
		}
		
		// Set the module setting, no matter what to avoid future checks
		if(!TC_getModuleConfig('dashboard','upgraded'))
		{
			TC_setModuleConfig('dashboard','upgraded','1');
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PUBLISHING
	//
	// Methods related to publishing
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this particular model uses, publishing, which accounts for the larger flag, but can also be
	 * extended to force a particular model to never use publishing.
	 * @return bool
	 */
	public static function modelUsesPublishing() : bool
	{
		return false;
	}
	

	
}