<?php

/**
 * Class TMm_DashboardModule
 *
 * An extension of the normal module to add functionality specific to this module.
 */
class TMm_DashboardModule extends TSm_Module
{
	protected array $dashboards = [];
	
	/**
	 * Constructor.
	 * @param array|int $module_id
	 */
	public function __construct($module_id = 'dashboard')
	{
		parent::__construct($module_id);
		
		$json = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/admin/dashboard/config/dashboards.json');
		$this->dashboards = json_decode($json, true);
	}
	
	/**
	 * The array of Tungsten dashboards that are available on this site.
	 * @return array[]
	 */
	public function dashboards() : array
	{
		return $this->dashboards;
	}
	
	/**
	 * Returns the values for a given dashboard ID
	 * @param string $dashboard_id
	 * @return array
	 */
	public function dashboardValues(string $dashboard_id) : array
	{
		if(!isset($this->dashboards()[$dashboard_id]))
		{
			$this->triggerMissingDashboardIDError($dashboard_id);
		}
		
		return $this->dashboards()[$dashboard_id];
	}
	
	/**
	 * Returns the dashboard model with the provided ID
	 * @param string $dashboard_id
	 * @return TMm_Dashboard
	 */
	public function dashboardWithID(string $dashboard_id) : TMm_Dashboard
	{
		if(!isset($this->dashboards()[$dashboard_id]))
		{
			$this->triggerMissingDashboardIDError($dashboard_id);
		}
		
		return TMm_Dashboard::init($dashboard_id);
	}
	
	public function triggerMissingDashboardIDError(string $dashboard_id) : void
	{
		TC_triggerError('Dashboard with ID `'. htmlentities($dashboard_id).'` not found. Ensure the dashboard is
		declared in the `dashboards.json` in the config for the dashboards module.');
		
	}
	
	
}





