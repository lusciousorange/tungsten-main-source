<?php

/**
 * A trait that allows a model to expose methods to dashboard views so that those dashboard views can display
 * site data without the user needing to have knowledge about the site structure. A model can override any of
 * the methods of this trait to specify which of its methods to expose for different reasons. For dashboard
 * views that visualize single values, {@see valueMethods()}; for dashboard views that can create graphs or
 * otherwise work with data in series, {@see graphableMethods}.
 *
 * To use this trait:
 *
 * 1. Add the TMt_DashboardModelViewable trait to the model or model list providing data.
 * 2. Override this trait's functions as necessary. The below implementations are templates you can copy to get started.
 */
trait TMt_DashboardModelViewable
{

	/**
	 * Override this method to make methods in a class visible to dashboard views that work with single
	 * values, like {@see TMv_DashboardOneNumber}.
	 *
	 * Returns an associative array containing information about the methods in this model that return numeric
	 * values. Array indices are a name for that number with additional properties for different items.
	 *
	 * 'description' - Required string. A short, user-friendly phrase describing the returned data.
	 * 'method_name' – The name of the method to be called.
	 *
	 * COMPARISONS
	 * These are optional and can be set when there is another value to reference against
	 * `comparison_method_name` - The name of the method to be used for comparisons
	 * `comparison_title` - The title that describes the relationship of the comparison value ("last year", etc)
	 *
	 * OPTIONAL PREFIX or SUFFIX
	 * 'prefix' - Optional string. A prefix to render before the value; for example, a dollar sign.
	 * 'suffix' - Optional string. A suffix to render after the value; for example, a percent sign.
	 *
	 *
	 * @return array
	 */
	public static function valueMethods() : array
	{
		return [
			/*
			'name' => [
				'method_name' => 'valueForThisYear',
				'description' => '...',
			
				// COMPARISONS
				'comparison_method_name' => 'valueForThisYear',
			    'comparison_title' => 'Last Year',
	
				// All of the below properties are optional; remove everything you don't need.
				// See documentation for an explanation of what each of these does.

				// OPTIONAL PREFIX / SUFFIX
				'prefix' => '$',
				'suffix' => '%',
			
			
			],
			// INSERT NEXT METHOD HERE
			*/
		];
	}

	/**
	 * Override this method to make methods in a class visible to dashboard views that work with graphable
	 * data, like {@see TMv_PageGraph}.
	 *
	 * Returns an associative array of information about the graphable methods in the class. Indices are names
	 * of graphable methods, values are small lists properties for each method. The only required property
	 * is the 'description'; all other properties are optional things that help give context to the data so
	 * it's handled correctly.
	 *
	 * A method returns graphable data if it returns a list of series, like so:
	 * 		['series name 1' => [array of data], 'series name 2' => [array of data], ...]
	 *
	 * The simplest format for data is just a regular array of values. Array indices become x values, and array
	 * values become y values. See {@see TCv_Graph.addSeries()} for details on how data is structured.
	 *
	 * Properties available for each method are:
	 *
	 * 'description' - Required String. A short, user-friendly phrase describing the returned data.
	 *
	 * 'is_multiple_series' - Optional Boolean, false if unset. Whether or not the method returns
	 * 			multiple series at once.
	 * 'model_list_method' - Optional String, 'models' if unset. The name of the method in this class' model
	 * 			list class that should be used to generate a list of models for this dataset. Use this to
	 * 			filter the list if a dataset only makes sense for a subset of the available models.
	 * 'x_value_type' - Optional String, {@see TCv_Graph.setXValueType()} for valid values and what they
	 * 			mean. If unset, graphs that have axes will make a reasonable guess.
	 * 'x_axis_title' - Optional String. A default title for the x axis of the graph using the data, if
	 * 			the graph has an x axis.
	 * 'y_axis_title' - Optional String. A default title for the y axis of the graph using the data, if
	 * 			the graph has a y axis.
	 * 'num_dimensions' - Optional Integer. The number of dimensions each datapoint in this dataset has. Used
	 * 			to filter the list of available chart types down to only the types that accept data with that
	 * 			number of dimensions.
	 * 'restrict_chart_type' - Optional String. If used, the only chart type available for this dataset is
	 * 			the one specified; this overrides num_dimensions if both are set. Use one of the class
	 * 			constants from TCv_Graph that end in 'Type' or 'MixedType'.
	 * 'series_types' - Optional Array[String]. For mixed charts; should be used with 'restrict_chart_type'.
	 * 			Specify the type of each series in the graph, in the same order the method returns them in.
	 * 			Use class contants from TCv_Graph that end in 'Type' or 'MixedType'.
	 *
	 * @return array
	 */
	public static function graphableMethods() : array
	{
		return [
			/*
			'method_name' => [
				'description' => '...',

				// All of the below properties are OPTIONAL; remove everything you don't need.
				// See documentation for an explanation of what each of these does.

				'is_multiple_series' => false,

				'model_list_method' => '',

				'x_value_type' => '',

				'x_axis_title' => '',
				'y_axis_title' => '',

				'num_dimensions' => 0,

				'restrict_chart_type' => '',

				'series_types' => []
			],
			'method_name_2' => [
				// ...
			],
			// ...
			*/
		];
	}

}
