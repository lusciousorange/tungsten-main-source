<?php

/**
 * Trait TMt_DashboardView
 *
 * A trait that allows for regular views to be used as a dashboard view that can be added to the dashboard when necessary.
 */
trait TMt_DashboardView
{
	use TMt_PagesContentView;
	protected $dashboard_visible = true;

	/**
	 * Configures the view to be shown on the dashboard. This method is called for all dashboards before being shown
	 * on the dashboard
	 */
	public function configureForDashboard()
	{
		// Do Something
	}
	
	/**
	 * Indicates if this dashboard view is also usable on pages themselves
	 * @return bool
	 */
	public static function isAlsoPageView() : bool
	{
		return false;
	}


	
	/**
	 * Returns the default title for this dashboard
	 * @return string
	 */
	public static function dashboard_DefaultTitle()
	{
		return '';
	}

	public static function dashboard_DefaultViewAllURL()
	{
		return '';
	}
	
	public static function dashboard_DefaultViewAllTitle()
	{
		return 'View All';
	}
	
	
	/**
	 * Returns if the entire dashboard is visible
	 * @return bool
	 */
	public function dashboardVisible() : bool
	{
		return $this->dashboard_visible;
	}

	/**
	 * Hides this dashboard via code
	 */
	public function hideDashboard() : void
	{
		$this->dashboard_visible = false;
	}
	
	//////////////////////////////////////////////////////
	//
	// MODEL METHODS
	//
	// These methods relate to commonly used functionality
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Method to call a another method on a provided class_name, method and potentially ID. This is commonly used by
	 * different dashboards internally when asking for code references to load dashboard data.
	 *
	 * @param string $model_class
	 * @param string $model_id
	 * @param string $method_name
	 * @return mixed|bool
	 */
	protected function callModelMethod($model_class, $model_id, $method_name)
	{
		// CHECK FOR MISSING MODEL CLASS
		if(!class_exists($model_class))
		{
			$this->addText('<br />Class "'.$model_class.'" Does Not Exist');
			$this->addClass("error");
			return false;
		}
		
		$method_name = str_ireplace('()','',$method_name);
		
		try
		{
			$method_checker = new ReflectionMethod($model_class,$method_name);
			
		}
		catch(ReflectionException $e)
		{
			$this->addText('<br />Method "'.$method_name.'" Does Not Exist');
			$this->addClass("error");
			return false;
			
		}
		
		// Check if it's static and don't try to initialize the class
		if($method_checker->isStatic())
		{
			$model = $model_class;
		}
		else
		{
			if($model_id === '0')
			{
				$model = TC_activeModelWithClassName($model_class);
			}
			else
			{
				$model = $model_class::init($model_id);
				
			}
			
			// CHECK MODEL CREATION
			if(!$model)
			{
				$this->addText('<br />Class Creation Failure');
				$this->addClass("error");
				return false;
			}
			
		}
		
		
		
		
		return call_user_func_array(array($model, $method_name), array());
	}

	//////////////////////////////////////////////////////
	//
	// DEPRECATED LEGACY METHODS
	//
	//////////////////////////////////////////////////////


	/**
	 * @param string $title The title to be used.
	 * @return mixed
	 * @deprecated This method is used in the old dashboard settings. New solutions use the static methods to define
	 * them for the class
	 */
	public function setDashboardTitle($title)
	{
		// No action
	}

	/**
	 * Positioning is no longer used in dashboards
	 * @param $position
	 * @deprecated This method is used in the old dashboard settings. New solutions use the static methods to define
	 * them for the class
	 */
	public function setDefaultDashboardPosition($position)
	{
		// No action
	}





}
?>