<?php

class TMc_DashboardController extends TSc_ModuleController
{
	/**
	 * TMc_DashboardController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);


	}
	
	
	public function defineURLTargets()
	{
//		$dashboard = TSm_ModuleURLTarget::init( '');
//		$dashboard->setViewName('TMv_DashboardWrapper');
//		$this->addModuleURLTarget($dashboard);
		
		
		$target = TSm_ModuleURLTarget::init( '');
		$target->setNextURLTarget('main');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('load-iframe');
		$target->setTitle('Load ifrem');
		$target->setViewName('TMv_DashboardWrapper');
		$target->setModelName('TMm_Dashboard');
		$target->setModelInstanceRequired();
		$this->addModuleURLTarget($target);
		
		$module = TMm_DashboardModule::init();
		foreach($module->dashboards() as $dashboard_name => $values)
		{
			if(is_null($values['model_class_name']))
			{
				// Does NOT set the instance required
				// Pulled based on the URL target name
				$target = TSm_ModuleURLTarget::init($dashboard_name);
				$target->setTitle($values['title']);
				$target->setViewName('TMv_DashboardWrapper');
				$target->setModelName('TMm_Dashboard');
				$target->setAsSubsectionParent();
				$this->addModuleURLTarget($target);
			}
		}
		
		
		
		
		$user = TC_currentUser();
		if(!$user)
		{
			header("Location: /admin/");
			exit();
		}

		if($user->isAdmin() && !TC_getConfig('use_tungsten_9'))
		{
			$main_dashboard = $this->URLTargetWithName('main');
			$item = TSm_ModuleURLTarget::init( 'configure');
			$item->setTitle('Configure');
			$item->setParent($main_dashboard);
			$item->setAsRightButton('fa-arrows');
			$this->addModuleURLTarget($item);

			$item = TSm_ModuleURLTarget::init( 'disable-configure');
			$item->setTitle('Done');
			$item->setParent($main_dashboard);
			$item->setAsRightButton('fa-check');
			$this->addModuleURLTarget($item);
		}

		// /admin/dashboard/do/graphable-models-for-method
		// Used in the form for TMv_PageGraph, to return a list of models that can be used to graph any
		// particular dataset - ie a method marked as graphable in any model that uses TMt_DashboardModelViewable.
		$url_target = TSm_ModuleURLTarget::init( 'models-for-graphable-method');
		$url_target->setTitle('Models for graphable method');
		$url_target->setModelName('TMv_PageGraph');
		$url_target->setModelActionMethod('graphableModels');
		$url_target->setReturnAsJSON();
		$url_target->setNextURLTarget(null);
		$url_target->setPassValuesIntoActionMethodType('get','model','method');

		$this->addModuleURLTarget($url_target);

		// /admin/dashboard/do/models-for-value-method
		// Used in the form for TMv_DashboardOneNumber, to return a list of models that numbers can be pulled from.
		$url_target = TSm_ModuleURLTarget::init('models-for-value-method');
		$url_target->setTitle('Models for value method');
		$url_target->setModelName('TMv_DashboardOneNumber');
		$url_target->setModelActionMethod('valueMethodModels');
		$url_target->setReturnAsJSON();
		$url_target->setNextURLTarget(null);
		$url_target->setPassValuesIntoActionMethodType('get','model_class_name');

		$this->addModuleURLTarget($url_target);

	}
	
	

	

}
?>