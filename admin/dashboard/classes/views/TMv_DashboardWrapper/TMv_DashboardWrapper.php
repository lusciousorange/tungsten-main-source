<?php
/**
* Class TMv_DashboardWrapper
*
* A simple wrapper for dashboards that match the existing functionality around page builders that use the wrapper to
 * control functionality.
*/
class TMv_DashboardWrapper extends TMv_PageBuilder_ThemeWrapper
{
	protected string $page_builder_class_name = 'TMv_Dashboard';
	protected TMm_Dashboard $dashboard;
	protected ?TCm_Model $iframe_renderer = null;
	
	/**
	 * The constructor which is based on the URL target name that is being loaded. These are unique indices names for
	 * all the controllers, so it helps sort these out.
	 */
	public function __construct($input_model = null)
	{
		$this->addClass('TMv_DashboardWrapper');
		
		$dashboard_module = TMm_DashboardModule::init();
		
		// Dashboard provided - Internal iframe mode
		// Passes along the ID of the renderer as well
		if($input_model instanceof TMm_Dashboard)
		{
			$this->dashboard = $input_model;
			
			// Detect second model
			$this_dashboard_values = $dashboard_module->dashboardValues($this->dashboard->id());
			$model_class_name = $this_dashboard_values['model_class_name'];
			
			if(is_string($model_class_name))
			{
				$sections = explode("/",$_SERVER['REQUEST_URI']);
				$model_id = $sections[6];
				$renderer_model = $model_class_name::init($model_id);
			}
			
		}
		else
		{
			// Top level dashboards
			// URL TARGET BASED
			$current_url_target = TC_currentURLTarget();
			$this->dashboard = $dashboard_module->dashboardWithID($current_url_target->name());
			
			// We have an input model
			if($input_model instanceof TCm_Model)
			{
				$this->iframe_renderer = $input_model;
			}
			else
			{
				$this->iframe_renderer = $this->dashboard;
			}
			
		}
		parent::__construct($this->dashboard, null);
	
		$this->addClassCSSFile('TMv_DashboardWrapper');
		
		// Default start in preview mode
		$this->removeClass('editor_mode');
		$this->addClass('preview_mode');
		
		
		// Sets the buttons for preview switch mode
		if(!is_null($this->page_builder))
		{
			$this->page_builder->setPreviewButtonText('Exit','Edit dashboard');
		}
		
		
		// Tungsten 9 functionality
		if(TC_getConfig('use_tungsten_9'))
		{
			// Attach the messenger, so that updates get added to the actual view when they happen
			// Otherwise it requires a refresh and that breaks things
			$messenger = TCv_Messenger::instance();;
			if($messenger instanceof TCv_Messenger)
			{
				$this->attachView($messenger);
				
			}
			
		}
		
		
		
	}
	
	/**
	 *
	 */
	protected function loadIFrame()
	{
		$this->setTag('iframe');
		
		$this->setAttribute('src',
		                    '/admin/dashboard/do/load-iframe/'
		                    .$this->dashboard->id().'/'
		                    .$this->iframe_renderer->id().'/'
		                    .'?return_type=fullpage');
		$this->is_iframe = true;
		
	}
	
	
	/**
	 * We don't want to load anything from the theme. Dashboards don't do that
	 * @return void
	 */
	protected function findThemeView()
	{
		// DO NOTHING
	}
}