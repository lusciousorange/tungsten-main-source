<?php

/**
 * Class TMv_DashboardPanel
 *
 * A class that wraps a dashboard item in a panel to be displayed in the dashboard in Tungsten.
 */
class TMv_DashboardPanel extends TCv_View
{
	use TMt_PagesContentView;
	
	/** @var TMm_DashboardItem|TMt_PageRenderItem  */
	protected $dashboard_item;
	protected $background_color = 'FFFFFF';
	
	/** @var bool|TCv_View $panel_view */
	protected $panel_view = false;

	/**
	 *
	 * TMv_DashboardPanel constructor.
	 * @param TMm_DashboardItem|TMt_PageRenderItem|string $dashboard_item If a string is provided, then this indicates a
	 * manual creation of a dashboard item and the provided string is the title
	 */
	public function __construct($dashboard_item)
	{
		if(is_string($dashboard_item))
		{
			$id = preg_replace('/[^\w]/', '', $dashboard_item);
		}
		else
		{
			$id = $dashboard_item->id();
		}
		parent::__construct('dashboard_panel_'.$id);
		$this->dashboard_item = $dashboard_item;

		
		$this->addClassCSSFile('TMv_DashboardPanel');

		$this->prepForRender();
	}

	public function prepForRender()
	{
		if(is_string($this->dashboard_item))
		{
			$title = $this->dashboard_item;
		}
		else
		{
			$title = $this->dashboard_item->title();
			if(method_exists($this->dashboard_item, 'backgroundColor'))
			{
				$this->background_color = $this->dashboard_item->backgroundColor();
			}
			

		}



		if($title != '')
		{
			$heading = new TCv_View();
			$heading->setTag('h3');
			$heading->addClass('dashboard_heading');

			if(($this->dashboard_item instanceof TMm_DashboardItem) && $this->dashboard_item->viewAllURL() != '')
			{
				$link = new TCv_Link();
				$link->addClass('view_all_link');
				$link->setHREF($this->dashboard_item->viewAllURL());
				$link->addText($this->dashboard_item->viewAllTitle());
				$heading->attachView($link);
			}
			$heading->addText($title);
			parent::attachView($heading);
		}
		
		$this->panel_view = new TCv_View();
		$this->panel_view->addClass('panel_view');
		
	}
	
	/**
	 * @param $color
	 * @return void
	 * @deprecated No longer used
	 * @see setPanelColor
	 */
	public function setBackgroundColor($color)
	{
	
	}
	
	/**
	 * Sets the panel color to one of the predefined colours which is styled based on those values. Manually setting
	 * a colour will not work.
	 * @param string $color
	 * @return void
	 */
	public function setPanelColor(string $color)
	{
		$this->background_color = $color;
	}
	
	/**
	 * A special method to deal with attaching the content view to the panel
	 * @param TMt_DashboardView|TMt_PagesContentView|TCv_View $content_view
	 * @deprecated Just use `attachView`
	 */
	public function attachContentView($content_view)
	{
		$this->attachView($content_view);

	}
	
	/**
	 * Pass-through to ensure addText is applied to the panel view
	 * @param string $html
	 * @param bool $disable_template_parsing
	 */
	public function addText($html, $disable_template_parsing = false)
	{
		$this->panel_view->addText($html);
	}
	
	public function attachView($view, $prepend = false)
	{
		$this->addClass('panel_'.get_class($view));
		$this->panel_view->attachView($view, $prepend);
		
		if(method_exists($view,'dashboardVisible') &&  !$view->dashboardVisible())
		{
			$this->addClass('hidden_dashboard');
		}
		
		
	}
	
	public function contentItem()
	{
		return $this->dashboard_item;
	}
	
	/**
	 * Extend functionality to deal with attaching the panel view
	 * @return string
	 */
	public function render()
	{
		$this->addClass('style_'.$this->background_color);
		parent::attachView($this->panel_view);
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// wrapper added for consistency in the page builder
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the view can be added as a content item. You can manually adjust certain items for your site if necessary.
	 * @return bool
	 */
	public static function pageContent_IsAddable() : bool
	{
		return false;
	}
	
}