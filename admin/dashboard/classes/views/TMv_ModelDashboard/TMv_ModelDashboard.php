<?php

/**
 * Class TMv_ModelDashboard
 *
 * A model dashboard is for previewing a single model and showing information related to that model. This class
 * provides a series of methods to generate views that can be put on this dashboard.
 *
 * Each model dashboard should do the following:
 *
 * 1. Extend this class
 * 2. Use standard Page Layout views to create the layout
 * 3. Call the methods in this view to generate consistently styled interfaces
 */
class TMv_ModelDashboard extends TMv_Dashboard
{
	protected $model;
	/**
	 * TMv_ModelDashboard constructor.
	 * @param TCm_Model $model
	 */
	public function __construct($model)
	{
		parent::__construct();
		
		$this->model = $model;
	}
	
	/**
	 * @return TCm_Model
	 */
	public function model()
	{
		return $this->model;
	}
	
	
	/**
	 * No help for model dashboards
	 * @return TCv_View|bool
	 */
	public function helpView()
	{
		return false;
	}
	
	
}