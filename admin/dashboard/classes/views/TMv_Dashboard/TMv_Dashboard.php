<?php

/**
 * Class TMv_Dashboard
 */
class TMv_Dashboard extends TMv_PageBuilder
{
	protected $layout_views = array();
	
	
	/**
	 * TMv_Dashboard constructor.
	 * @param bool|TCm_Model|TMt_PageRenderer $renderer
	 */
	public function __construct($renderer = null)
	{
		if(!$renderer)
		{
			$renderer = TMm_Dashboard::init(0);
		}
	
		parent::__construct($renderer);
		
		$this->enableAllPreviews();

		$this->disableColumnSettings();
		//$this->disableLayoutSettings();
		$this->disableLayoutVisibility();
		$this->disableContentVisibility();
		$this->disableContentWorkflow();
		$this->disableStyles();
		$this->addClassCSSFile('TMv_Dashboard');
		
		if(!TC_getConfig('use_tungsten_9'))
		{
			$this->addClassJSFile('TMv_Dashboard');
			$this->addClassJSInit('TMv_Dashboard');
		}
		$this->setContentClassNames(TCv_Website::classesWithTrait('TMt_DashboardView'));

		
	}
	
	/**
	 * @return TMv_ContentLayout[]
	 */
	public function sectionOptions()
	{
		// Only return one value since we never do the styling on the Dashboard
		return array('TMv_ContentLayout_Section');
	}
	
	protected function processThemeName(): void
	{
		// Override and do nothing
	}
	
	/**
	 * Returns the HTML for the view
	 * @return string
	 */
	public function render()
	{
		if($this->renderer_item->numContentItems() == 0)
		{
			$this->content_container->attachView($this->noContentView());
		}

		$this->attachView($this->addSectionPopup());
		$this->attachView($this->addContentPopup());
		$this->attachView($this->editContentPopup());

		$this->generateViews();

		$this->attachGeneratedViews($this->content_container);
		$this->attachView($this->content_container);
		
		// Only bother for v8
		if(!TC_getConfig('use_tungsten_9'))
		{
			$this->attachView($this->addSectionButton());
		}

		TMv_PageContentRendering::render();
	}

	/**
	 * Generate the view for a single content item. This is a recursive function that deals with the nesting of the content inside of containers
	 * @param TMt_PageRenderItem $content_model
	 * @return bool|TCu_Item
	 */
	public function viewForContentModel($content_model)
	{
		$view = parent::viewForContentModel($content_model);
		if($view)
		{
			// Add full-height for all the rows
			if($view instanceof TMv_ContentLayout)
			{
				$view->setAsEqualHeights();
			}
			
			$traits = class_uses($view);
			if(isset($traits['TMt_DashboardView']))
			{
				/** @var TMt_DashboardView $view */
				$view->configureForDashboard();
				// Wrap the view in a panel
				$panel = new TMv_DashboardPanel($content_model);
				$panel->attachView($view);
				
				// Pass the control panel through to this dashboard panel
				if($view->hasControlPanelView())
				{
					$panel->setControlPanelView($view->controlPanelView());
					
				}
				return $panel;
			}
		}

		return $view;


	}


//	public function helpView()
//	{
//		$help_view = new TCv_View();
			
//			$p = new TCv_View();
//			$p->setTag('h2');
//			$p->addText('The dashboard is the homepage for the Tungsten Admin Interface. ');
//			$help_view->attachView($p);
//
//			$p = new TCv_View();
//			$p->setTag('p');
//			$p->addText('Depending on your website, you will see different dashboards that are useful to you. The dashboard can be configured for each account, so you can customize it to what you prefer. New dashboards must be added by a developer since it involves programming in order to add them.');
//			$help_view->attachView($p);
//
//			$p = new TCv_View();
//			$p->setTag('p');
//			$p->addText('<strong>Configure Your Dashboard</strong>: Click on the ');
//				$link = new TSv_HelpLink('Configure', '.url-target-rearrange');
//				$p->attachView($link);
//			$p->addText(' button. Then drag and drop the  ');
//				$link = new TSv_HelpLink('different dashboards', '.dashboard_container');
//				$p->attachView($link);
//			$p->addText(' into the order you prefer and click the  ');
//				$link = new TSv_HelpLink('Done', '.url-target-disable-rearrange');
//				$p->attachView($link);
//			$p->addText(' button when you are finished.');
//			$help_view->attachView($p);
//
//			$p = new TCv_View();
//			$p->setTag('p');
//			$p->addText('If you don\'t want to see a particular dashboard, then use the configuration to move it to the Disabled section');
//			$help_view->attachView($p);
//
//
//		return $help_view;
//	}
	
}
?>