class TMv_Dashboard {
	element;
	show_button;
	hide_button;

	constructor(element)
	{
		this.element = element;

		this.show_button = document.querySelector('.url-target-configure');
		this.show_button.addEventListener('click', (e) => this.toggleViewMode(e));

		this.hide_button =  document.querySelector('.url-target-disable-configure');
		this.hide_button.addEventListener('click', (e) => this.toggleViewMode(e));
		this.hide_button.hide();

	}

	toggleViewMode(event)
	{
		event.preventDefault();

		let wrapper = this.element.closest('.TMv_DashboardWrapper');
		wrapper.classList.toggle('preview_mode');
		wrapper.classList.toggle('editor_mode');
		this.show_button.toggle();
		this.hide_button.toggle();

	}
}
