class TMv_TabbedDashboardView {
	element;

	constructor(element, params) {
		this.element = element;

		this.element.querySelectorAll('.tab_button').forEach( el => {
			el.addEventListener('click', (e) => {this.tabClicked(e);});
		});

		// Show the first one
		this.showTabWithNumber(1);

	}

	/**
	 *
	 * @param {Event} event
	 */
	tabClicked(event) {
		event.preventDefault();

		let item_number = event.target.getAttribute('data-target-id');
		this.showTabWithNumber(item_number);

	}

	/**
	 * Shows the tab with a specific number in this tabbed view
	 * @param number
	 */
	showTabWithNumber(number) {
		//this.hideAllContainers();

		let showing = this.element.querySelector('.tab_container.showing');
		if(showing)
		{
			showing.classList.remove('showing');
			this.element.querySelector('.tab_button.showing').classList.remove('showing');
		}

		this.element.querySelector('.tab_button_'+number).classList.add('showing');
		this.element.querySelector('.container_'+number).classList.add('showing');


	}


}