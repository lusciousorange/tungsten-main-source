<?php
class TMv_TabbedDashboardView extends TCv_View
{
	protected $tabs = [];
	
	/**
	 * Adds a tab to the view
	 * @param string $title
	 * @param TCv_View $view
	 */
	public function addTabView($title, $view)
	{
		$button = new TCv_Link();
		$button->setURL('#');
		$button->addClass('tab_button');
		$button->addText($title);

		$this->tabs[] = ['button' => $button, 'view' => $view];
		
	}

	/**
	 * Renders the view
	 */
	public function render()
	{
		$this->addClassCSSFile('TMv_TabbedDashboardView');

		$this->addClassJSFile('TMv_TabbedDashboardView');
		$this->addClassJSInit('TMv_TabbedDashboardView');

		$button_box = new TCv_View();
		$button_box->addClass('button_box');
		
		$container_box = new TCv_View();
		$container_box->addClass('container_box');
		
		
		$count = 1;
		foreach($this->tabs as $tab_values)
		{
			/** @var TCv_Link $button */
			$button = $tab_values['button'];
			$button->addDataValue('target-id', $count);
			$button->addClass('tab_button');
			$button->addClass('tab_button_'.$count);
			$button_box->attachView($button);
			
			/** @var TCv_View $view */
			$view = $tab_values['view'];
			$container = new TCv_View();
			$container->addClass('tab_container');
			$container->addClass('container_'.$count);
			$container->attachView($view);
			$container_box->attachView($container);
			$count++;
			
		}

		$this->attachView($button_box);
		
		$this->attachView($container_box);


		
	}

}