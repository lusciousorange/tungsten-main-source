<?php
class TMv_DashboardContentForm extends TMv_PageRendererContentForm
{

	/**
	 * Configuration of the form
	 */
	public function configureFormElements()
	{
		
		/** @var TMt_DashboardView|TCv_View $content_view */
		$content_view = $this->content_view;
		$traits = class_uses($content_view);
		if(isset($traits['TMt_DashboardView']))
		{
			
			
			$heading = new TCv_FormItem_Heading('dashboard_values', 'Dashboard Settings');
			$this->attachView($heading);
			
			$field = new TCv_FormItem_TextField('dashboard_title', 'Dashboard Title');
			$field->setHelpText('The title that appears above the dashboard.');
			$field->setDefaultValue(($content_view)::dashboard_DefaultTitle());
			$this->attachView($field);
			
			
			$field = new TCv_FormItem_Select('dashboard_color', 'Color');
			$field->setHelpText('Choose from a preset color code for this dashboard item.');
			$field->addOption('blue', 'Blue');
			$field->addOption('purple', 'Purple');
			$field->addOption('pink', 'Pink');
			$field->addOption('red', 'Red');
			$field->addOption('orange', 'Orange');
			$field->addOption('yellow', 'Yellow');
			$field->addOption('green', 'Green');
			$field->addOption('grey', 'Grey');
			$this->attachView($field);
			
			
			$field = new TCv_FormItem_TextField('view_all_url', 'View All URL');
			$field->setHelpText("The URL where the View All button will direct people to if clicked. Leave blank to disable");
			$field->setDefaultValue(($content_view)::dashboard_DefaultViewAllURL());
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('view_all_title', 'View All Title');
			$field->setHelpText('The title that is used for the View All link');
			$field->setDefaultValue(($content_view)::dashboard_DefaultViewAllTitle());
			$this->attachView($field);
			
		}
		
		
		
		parent::configureFormElements();

	}
	
	
}