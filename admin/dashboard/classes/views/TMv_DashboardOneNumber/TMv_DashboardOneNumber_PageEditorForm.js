class TMv_DashboardOneNumber_PageEditorForm
{
	/**
	 *
	 * @param {Element} element
	 * @param params
	 */
	constructor(element, params)
	{
		this.element = element;

		this.methods = params.methods;
		this.saved_id = params.saved_id;

		element.querySelector('#data_source').addEventListener('change',this.dataSourceFieldChanged.bind(this));
		this.dataSourceFieldChanged();

		element.querySelector('#widget_format').addEventListener('change',this.widgetFormatFieldChanged.bind(this));
		this.widgetFormatFieldChanged();

	}

	/**
	 * Event handler for when the data source field changes
	 * @param event
	 */
	dataSourceFieldChanged(event) {

		let field = this.element.querySelector('#data_source');
		let model_id_row = this.element.querySelector('#primary_model_id_row');
		let comparison_group = this.element.querySelector('#comparison_group');

		// We have a value
		if(field.value !== '')
		{
			let method_values = this.methods[field.value];

			// Set the visibility of the model row, based on if it requires a model
			if(method_values['requires_model'])
			{
				model_id_row.show();
			}
			else
			{
				model_id_row.hide();
			}





			// Deal with model IDs
			this.updateModelDropDown(method_values.model_class_name);

			if(method_values.comparison_method_name === undefined || method_values.comparison_method_name === '')
			{
				comparison_group.hide();

			}
			else //
			{
				comparison_group.show();

			}

		}
		else // no value, hide the model row, reset the value
		{
			model_id_row.hide();
			comparison_group.hide();
			let model_id_field = this.element.querySelector('#primary_model_id');
			model_id_field.value = '';
			//	model_id_field.chosen().trigger('change');
		}


	}

	widgetFormatFieldChanged(event) {

		let field = this.element.querySelector('#widget_format');

		if(field.value == 'basic')
		{
			// Hide the colour scheme row
			this.element.querySelector('#comparison_colour_row').hidden = true;
		}
		else
		{
			this.element.querySelector('#comparison_colour_row').hidden = false;
		}


	}

	/**
	 * Returns the models provided for a given class name
	 * @param {string} class_name
	 */
	updateModelDropDown(class_name, callback) {

		// Clear model IDs
		let select = this.element.querySelector('#primary_model_id');

		//Clear the select
		select.innerHTML = "";


		fetch('/admin/dashboard/do/models-for-value-method/' + "?" + new URLSearchParams({ model_class_name: class_name }))
			.then(response => response.json())
			.then(response => {

				// Check for errors
				if (response.error !== undefined){
					alert("ERROR: Could not load model list. See developer console for details.");
					return;
				}

				console.log(response);

				// Add the Select box
				response.models.unshift({'id' : '0', 'title' : "Select a model"});
				response.models.push({'id' : 'active_page_model','title' : 'Use active page model'})

				// Add new models to the child dropdown
				for (let item of response.models)
				{
					let option = document.createElement("option");
					option.text = item.title;
					option.value = item.id;

					// Check for the saved value
					if (option.value != 0 && option.value == this.saved_id)
					{
						option.selected = true;
					}
					select.add(option);
				}

				// Update TomSelect to have the latest values
				select.tom_select.sync();

			})
			.catch(error => {
				console.log("ERROR:", error);
				//	alert("ERROR: Could not load model list. See developer console for details.");
			})
			.finally(() => {

			});

	}

}
