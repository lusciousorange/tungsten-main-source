<?php

/**
 * This dashboard item renders a number and possibly a comparison number as either plain text, a gauge, or a
 * progress bar. To add a new number to the list available to this class, you must write a method that returns
 * a number (or an array to count the number of elements of) in some model and then mark it as a value method
 * using the {@see TMt_DashboardModelViewable} trait. Check the trait's documentation for details.
 *
 * @see TMt_DashboardModelViewable
 */
class TMv_DashboardOneNumber extends TCv_View
{
	use TMt_DashboardView;
	use TMt_PagesContentView;
	
	protected $use_comparison = false;
	
	/**
	 * @var string $model_class_name The name of the model that is a TMt_DashboardModelViewable item
	 */
	protected $model_class_name = null;
	
	/**
	 * @var string $data_source_code The code for the value method in valueMethods()
	 */
	protected $data_source_code;
	
	/**
	 * @var array $value_methods_info The array of values that comes from the specific value method
	 */
	protected $value_methods_info;
	
	protected $is_manual = false;
	/**
	 * @var TCm_Model|TMt_DashboardModelViewable $model The actual model that is being used
	 */
	protected $model;
	
	/**
	 * @var int|float  $primary_value The primary value to be shown
	 */
	protected $primary_value = null;
	
	protected $primary_value_method_params = [];
	
	/**
	 * @var int|float|null  $comparison_value The primary value to be shown
	 */
	protected $comparison_value = null;
	
	protected $comparison_value_method_params = [];
	
	protected bool $value_is_bad = false;
	protected bool $value_is_warning = false;
	
	
	// Class constants for each of the formats the widget can display data in. For avoiding typo bugs.
	protected const BasicFormat = 'basic', GaugeFormat = 'gauge', ProgressBarFormat = 'progressbar';
	
	// A set of colours to pick from for the gauge and progress bar formats.
	protected const Colours = [
		'blue' => '#256BB2',
		'purple' => '#7526D6',
		'pink' => '#BF44AF',
		'red' => '#BE4242',
		'orange' => '#CF7200',
		'yellow' => '#B4902D',
		'green' => '#0B8474',
	];
	
	//////////////////////////////////////////////////////
	//
	// SAVED FORM FIELD VALUES
	//
	//////////////////////////////////////////////////////
	
	// The value method(s) to render the results of.
	protected $data_source, $primary_value_method, $primary_model_id, $comparison_value_method, $comparison_model_id;
	
	// String determining what render format to use. Possible values: BasicFormat, GaugeFormat, ProgressBarFormat.
	protected $widget_format = 'basic';
	
	// Various options which affect the presentation of the data based on $widget_format
	protected $abbreviated_numbers, $comparison_title, $comparison_include_percent, $comparison_gauge_style;
	
	protected $comparison_colour = 'blue';
	
	/**
	 * Indicates this is being used manually and not from a data source
	 * @return void
	 */
	public function setAsManual()
	{
		$this->is_manual = true;
	}
	
	public function setWidgetFormat(string $format) : void
	{
		$this->widget_format = $format;
		
		// We require comparison to be true
		if($format == 'gauge' || $format == 'progressbar')
		{
			$this->use_comparison = true;
		}
	}
	
	public function setPrimaryValue($value)
	{
		$this->primary_value = $value;
	}
	
	/**
	 * Returns the primary value and calculates it if it hasn't been yet
	 */
	public function primaryValue()
	{
		if(is_null($this->primary_value))
		{
			$model = $this->model();
			$method_name = $this->primary_value_method;
			$this->primary_value =	call_user_func_array([$model, $method_name], $this->primary_value_method_params);
			
			// If the returned value is an array, we count the number of values in the array
			if(is_array($this->primary_value))
			{
				$this->primary_value = count($this->primary_value);
			}
		}
		
		return $this->primary_value;
	}
	
	
	/**
	 * Sets this view to use the active page model, which is a specific value that tells the component to load the
	 * active model based on the data source
	 * @return void
	 */
	public function setToUseActiveModel()
	{
		$this->primary_model_id = 'active_page_model';
	}
	
	/**
	 * Manually set the data source for this dashboard. Normally used in manual mode to create dashbaords. You can
	 * also pass the name of the function directly and it will bypass the need to set the value method in the class,
	 * which can be an unnecessary step.
	 *
	 * @param string $data_source The name of the data source from the valueMethods() method of the class OR the name
	 * of the method itself.
	 * @return void
	 */
	public function setDataSource(string $data_source)
	{
		$this->data_source = $data_source;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// RENDERING FUNCTIONS
	//
	//////////////////////////////////////////////////////
	
	public function model() : ?TCm_Model
	{
		if(is_null($this->model))
		{
			$this->processDashboardValues();
			if($this->primary_model_id == 'active_page_model')
			{
				$this->model = TC_activeModelWithClassName($this->model_class_name);
			}
			else
			{
				$this->model = ($this->model_class_name)::init($this->primary_model_id);
			}
		}
		
		return $this->model;
	}
	
	public function render()
	{
		$this->addClassCSSFile('TMv_DashboardOneNumber');
		
		try
		{
			if(!$this->is_manual)
			{
				// Before anything else, error check the required primary number, and find its value.
				$this->processDashboardValues();
				
				// DETECT PAGE MODELS
				$model = $this->model();
				
				
				if(!$model)
				{
					if($this->primary_model_id == 'active_page_model')
					{
						$this->addText('Loads active ' . $this->model_class_name::modelTitleSingular());
						return;
						//throw new Exception('No active model for '.$this->model_class_name."");
					}
					else
					{
						throw new Exception($this->model_class_name . " model not found with ID <em>" . $this->primary_model_id . "</em>");
					}
					
				}
				
				// INSTANTIATE AND CALL METHODS
				
				// Generate the primary value
				$this->primaryValue();
				
				if($this->use_comparison && is_null($this->comparison_value))
				{
					$method_name = $this->comparison_value_method;
					$this->comparison_value =
						call_user_func_array([$model, $method_name], $this->comparison_value_method_params);
				}
			}
			
			// And now, rendering.
			
			// Basic format but also failsafe if we ever try to load one with comparisons that just cant
			if($this->widget_format == self::BasicFormat || !$this->use_comparison )
			{
				
				$this->attachView($this->generateBasicView());
			}
			elseif($this->widget_format == self::GaugeFormat)
			{
				$this->attachView($this->generateGaugeView());
			}
			elseif($this->widget_format == self::ProgressBarFormat)
			{
				$this->attachView($this->generateProgressBarView());
			}
			else
			{
				throw new Exception("Invalid widget format.");
			}
			
			if($this->use_comparison)
			{
				$this->attachView($this->comparisionBox());
			}
		}
			// Should anything go wrong, inform the user and note the error in the console.
		catch (Throwable $e)
		{
			$this->addConsoleError($e->getMessage().'<br>at line '.$e->getLine().' in file '.$e->getFile());
			$this->addText('ERROR: '.$e->getMessage());
			$this->addClass('error');
		}
		
		// Add the primary_value data-attribute
		$this->addDataValue('value', $this->primary_value);
	}
	
	/**
	 * Takes a number and formats it according to how the dashboard is configured to render them.
	 * @param array|string|int $value
	 * @return string
	 */
	protected function formatNumber($value)
	{
		if(is_array($value))
		{
			$value = count($value);
		}
		
		// PREFIX
		$string = !empty($this->value_methods_info['prefix']) ? $this->value_methods_info['prefix'] : '';
		
		// NUMBER
		$string .= $this->abbreviated_numbers === '1' ? $this->abbreviateNumber($value) : $value;
		
		// SUFFIX
		$string .= !empty($this->value_methods_info['suffix']) ? $this->value_methods_info['suffix'] : '';
		
		return $string;
	}
	
	/**
	 * Manually sets the prefix for the value
	 * @param string $prefix
	 * @return void
	 */
	public function setValuePrefix(string $prefix) : void
	{
		$this->value_methods_info['prefix'] = $prefix;
	}
	
	/**
	 * Manually sets the suffix for the value
	 * @param string $suffix
	 * @return void
	 */
	public function setValueSuffix(string $suffix) : void
	{
		$this->value_methods_info['suffix'] = $suffix;
	}
	
	/**
	 * A view that shows the main number, formatted consistently
	 * @return TCv_View
	 */
	protected function mainNumberView() : TCv_View
	{
		// Prepare the primary number for display and add it to the view
		$primary_display_number = $this->formatNumber($this->primary_value);
		
		$main_number_div = new TCv_View();
		$main_number_div->addClass('main_number');
		$main_number_div->addText($primary_display_number);
		
		// Check for bad values and flag them
		if($this->value_is_bad && $this->primary_value > 0)
		{
			$this->addClass('value_is_bad');
			
		}
		
		
		return $main_number_div;
		
	}
	
	/**
	 * Generates the basic view which just has a number
	 * @return TCv_View
	 */
	protected function generateBasicView() : TCv_View
	{
		$this->addClass('basic');
		
		$view = new TCv_View();
		$view->addClass('inside');
		$view->attachView($this->mainNumberView());
		
		
		return $view;
	}
	
	public function setAsGauge() : void
	{
		$this->widget_format = self::GaugeFormat;
	}
	
	
	protected function generateGaugeView() : TCv_View
	{
		$view = new TCv_View();
		$view->addClass('inside');
		$this->addClass('gauge');
		
		// Create a radialbar chart, add the number and some custom settings, and attach it
		$graph = new TCv_Graph($this->attributeID().'_graph');
		$graph->setAsRadialBarChart();
		$graph->setSeriesColors([self::Colours[$this->comparison_colour]]);
		
		// Use this special one since radial bars only work with percentages
		$graph->addRadialBarChartSeriesValues($this->comparison_value, [$this->primary_value]);
		$graph->updateConfigFromJSONFile($this->classFolderFromRoot('TMv_DashboardOneNumber').'/gauge_settings.json');
		if ($this->comparison_gauge_style === 'semi')
		{
			$graph->updateConfigFromJSONFile($this->classFolderFromRoot('TMv_DashboardOneNumber').'/gauge_semi_settings.json');
			$this->addClass('semicircle');
		}
		
		
		$view->attachView($graph);
		
		return $view;
		
	}
	
	protected function generateProgressBarView() : TCv_View
	{
		$this->addClass('progress_bar');
		$view = new TCv_View();
		$view->addClass('inside');
		
		$view->attachView($this->mainNumberView());
		
		// PROGRESS BAR
		$percentage = $this->comparisonPercentage();
		
		$progress_bar = new TCv_View();
		$progress_bar->addClass('full_width_bar');
		$progress_bar->addClass('gray');
		
		
		// Make the filled side of the bar the correct colour, and make it fill the right width of the container
		$filled_bar = new TCv_View();
		$filled_bar->addClass('filled_bar');
		$filled_bar->addClass($this->comparison_colour);
		
		// Can't "overflow"
		if($percentage > 100)
		{
			$percentage = 100;
		}
		$filled_bar->setAttribute('style','width:'.$percentage.'%');
//		$filled_bar->addCSSLine(
//			$this->content_item->idAttribute().'_progbar_width',
//			'#'.$this->id().'.'.self::class.'.progress_bar > .filled_bar { width: '.$percent.'%; }'
//		);
		$progress_bar->attachView($filled_bar);
		
		$view->attachView($progress_bar);
		
		return $view;
	}
	
	/**
	 * Converts a numeric value into a precise abbreviation with a suffix and the specified number of decimal
	 * precision. Goes from thousands (k) up to trillions (T).
	 *
	 * @param integer|float $number The number to abbreviate
	 * @param integer $precision OPTIONAL. The number of decimal places to include. Default is 2.
	 * @return string The abbreviated number.
	 */
	protected function abbreviateNumber($number, int $precision = 2) : string
	{
		if ($number < 1000)
		{
			return round($number, $precision).'';
		}
		elseif ($number < 1e6) // 1,000,000
		{
			return round($number/1000, $precision).'k';
		}
		elseif ($number < 1e9) // 1,000,000,000
		{
			return round($number/1e6, $precision).'M';
		}
		elseif ($number < 1e12) // 1,000,000,000,000
		{
			return round($number/1e9, $precision).'B';
		}
		else
		{
			return round($number/1e12, $precision).'T';
		}
	}
	
	/**
	 * Sets the comparison value. This is what's used for a whole bunch of settings, including gagues
	 * @param $value
	 * @return void
	 */
	public function setComparisonValue($value) : void
	{
		$this->comparison_value = $value;
	}
	
	
	/**
	 * Calculations the percentage of the primary value to the comparison value.
	 *
	 * A value of 100 means they are equal.
	 * A value of 200 means the primary value is twice that of the comparison
	 * @return float
	 */
	protected function comparisonPercentage() : float
	{
		// Get the percentage value of the primary vs the comparison
		if($this->comparison_value <= 0)
		{
			return 0.0;
		}
		else
		{
			return round(($this->primary_value / $this->comparison_value * 100), 1);
			
		}
		
	}
	
	protected function comparisionBox()
	{
		$comparison_box = new TCv_View();
		$comparison_box->addClass('comparison_box');
		
		
		if (!empty($this->comparison_title))
		{
			$title = new TCv_View();
			$title->addClass('comparison_title');
			$title->addText($this->comparison_title);
			$title->addText(' : <strong>' . $this->formatNumber($this->comparison_value).'</strong>');
			$comparison_box->attachView($title);
		}
		
		
		if ($this->comparison_include_percent)
		{
			$comp_number_div = new TCv_View();
			$comp_number_div->addClass('comparison_number');
			
			// Take the percentage and substract 100.
			// The percentage value is the ratio between two.
			$percentage = $this->comparisonPercentage() - 100;
			$comparison_text = abs($percentage).'%';
			
			if ($percentage > 0)
			{
				$comparison_text .= ' <i class="fa fa-long-arrow-alt-up"></i>';
				$comp_number_div->addClass('positive_change');
			}
			elseif ($percentage < 0)
			{
				$comparison_text .= ' <i class="fa fa-long-arrow-alt-down"></i>';
				$comp_number_div->addClass('negative_change');
			}
			
			$comp_number_div->addText($comparison_text);
			$comparison_box->attachView($comp_number_div);
		}
		
		return $comparison_box;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// VALIDATION FUNCTIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Validates the properties for the dashboard, saving them to the model for later use
	 *
	 * Throws exceptions if any errors are found
	 *
	 */
	protected function processDashboardValues()
	{
		// Manual processing skips this
		if($this->is_manual || is_string($this->model_class_name))
		{
			return;
		}
		
		$parts = explode('::',$this->data_source);
		if(count($parts) != 2)
		{
			throw new Exception("Primary value is not properly formatted.");
		}
		
		$this->model_class_name = $parts[0];
		$this->data_source_code = $parts[1];
		
		// Does the given class exist, and does it have TMt_DashboardModelViewable ?
		if(! class_exists($this->model_class_name) )
		{
			throw new Exception("Class '{$this->model_class_name}' does not exist.");
		}
		if( !TC_classUsesTrait($this->model_class_name, TMt_DashboardModelViewable::class))
		{
			throw new Exception("Class '{$this->model_class_name}' does not have the 'TMt_DashboardModelViewable' trait.");
		}
		
		// Determine if we were just given a method name of the class
		// This happens in manual loading and saves excess wrapping code
		if(method_exists($this->model_class_name, $this->data_source_code))
		{
			// Manually generate it
			$this->value_methods_info = [
				'method_name' => $this->data_source_code,
				'description' => 'Manual'
			];
		}
		else // pull it from the value methods
		{
			// Process the value method details
			$value_methods = $this->model_class_name::valueMethods();
			if(!array_key_exists($this->data_source_code, $value_methods))
			{
				throw new Exception("Value Method value '{$this->data_source_code}' not found in '{$this->model_class_name}'.");
			}
			
			// Save the value method info
			$this->value_methods_info = $value_methods[$this->data_source_code];
		}
		
		// Does the method exist in the class
		$this->primary_value_method = $this->value_methods_info['method_name'];
		if (!method_exists($this->model_class_name, $this->primary_value_method))
		{
			//	throw new Exception("Method '{$this->model_class_name}->{$this->primary_value_method}()' does not exist.");
		}
		
		
		if(isset($this->value_methods_info['method_params']))
		{
			$this->primary_value_method_params = $this->value_methods_info['method_params'];
			
		}
		
		// Does the comparision method exist in the class
		if(isset($this->value_methods_info['comparison_method_name']) && $this->value_methods_info['comparison_method_name'] != '')
		{
			$this->use_comparison = true;
			$this->comparison_value_method = $this->value_methods_info['comparison_method_name'];
			
			if (!method_exists($this->model_class_name, $this->comparison_value_method))
			{
				//	throw new Exception("Method '{$this->model_class_name}->{$this->comparison_value_method}()' does not
				// exist.");
			}
			
			$this->comparison_title = $this->value_methods_info['comparison_title'];
			
			if(isset($this->value_methods_info['comparison_method_params']))
			{
				$this->comparison_value_method_params = $this->value_methods_info['comparison_method_params'];
				
			}
			
		}
		
		
	}
	
	/**
	 * Manually set this to be a "bad" value that should be flagged
	 * @return void
	 */
	public function setValueAsBad() : void
	{
		$this->value_is_bad = true;
	}
	
	/**
	 * Manually set this value to be a warning
	 * @return void
	 */
	public function setValueAsWarning() : void
	{
		$this->value_is_warning = true;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	public static function pageContent_ViewTitle() : string
	{
		return 'One number';
	}
	
	public static function pageContent_ViewDescription() : string
	{
		return 'A single number that is presented in the dashboard.';
	}
	
	public static function pageContent_IconCode() : string
	{
		return 'fa-hashtag';
	}
	
	/**
	 * Returns the array of data model names to be searched in this dashboard
	 * @return string[]
	 */
	public function dataSourceModelNames() : array
	{
		// Deal with Dashboards that are specific models
		/** @var TMm_Dashboard $active_dashboard */
		$active_dashboard = TC_activeModelWithClassName('TMm_Dashboard');
		$active_dashboard_model_name = $active_dashboard?->modelClassName();
		
		if(!is_null($active_dashboard_model_name))
		{
			$this->setToUseActiveModel();
			return [$active_dashboard_model_name];
		}
		
		
		return TC_modelsWithTrait('TMt_DashboardModelViewable');
	}
	
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		// Extra validation on this dashboard form to ensure non-admins can't alter these values
		if(TC_currentUser()->isAdmin())
		{
			
			$group = new TCv_FormItem_Group('content_settings_group','Content settings');
			
			$field = new TCv_FormItem_HTML('how_to', 'How to use this dashboard');
			$field->addText(
				'This dashboard presents a single number from the system to be displayed to the user. Select a '
				.'number from the list of available numbers on the website. If you don’t see what you need, speak '
				.'to a developer. (Developers: please refer to the DashboardModelViewable trait to add new numbers.)'
			);
			$group->attachView($field);
			
			$dashboard_viewable_models = $this->dataSourceModelNames();
			
			// Choose from the list of available value methods
			$field = new TCv_FormItem_Select('data_source', 'Data source');
			$field->setHelpText('Indicate which data source to use for this dashboard');
			$field->setIsRequired();
			$field->addOption('', 'Select a data source');
			foreach ($dashboard_viewable_models as $model)
			{
				$methods = $model::valueMethods();
				foreach($methods as $name => $method_values)
				{
					$option_val = $model.'::'.$name;
					$field->addOption($option_val, $model::modelTitleSingular().' - '.$method_values['description']);
				}
			}
			//$field->useFiltering();
			$group->attachView($field);
			
			// Some value methods require a model id.
			$field = new TCv_FormItem_Select('primary_model_id', 'Item');
			$field->setHelpText('Some numbers require you to pick a specific model to query.');
			$field->useFiltering();
			$group->attachView($field);

//			// HIDDEN FIELDS TO TRACK THE INDVIDUAL VALUES
//			$field = new TCv_FormItem_Hidden('comparison_include_percent', 'Include Percent Difference');
//			$field->setHelpText('Indicate whether or not a percent difference between the primary and comparison numbers should be shown.');
//			$field->addOption('0','No – Do not include the percentage difference');
//			$field->addOption('1','Yes – Include the percentage difference');
//
//			$group->attachView($field);
//
			
			// Whether or not to abbreviate numbers where possible (ex 1000 becomes 1k).
			$field = new TCv_FormItem_Select('abbreviated_numbers', 'Numbers presentation');
			$field->setHelpText('Indicate if numbers should be abbreviated to the nearest thousand. For example: 1234 becomes 1.2k.');
			$field->setDefaultValue('0');
			$field->addOption('0','Use full values');
			$field->addOption('1','Abbreviate numbers (eg: 5k)');
			$group->attachView($field);
			
			
			$value_is_bad = new TCv_FormItem_Select('value_is_bad', 'Value is bad');
			$value_is_bad->setHelpText('Indicate if this value is bad, and should be highlighted as a problem if not zero.');
			$value_is_bad->addOption('0', 'No');
			$value_is_bad->addOption('1', 'Yes – non-zero values will be indicated as a problem.');
			$group->attachView($value_is_bad);
			
			$form_items['content_settings_group'] = $group;
			
			
			
			// --------------------------------------------------------
			
			
			
			$group = new TCv_FormItem_Group('comparison_group','Comparison group');
			
			$field = new TCv_FormItem_Heading('comparison_heading', 'Comparison');
			$field->setHelpText('Settings related to the comparison number provided.');
			$group->attachView($field);
			
			$field = new TCv_FormItem_Select('widget_format', 'Widget format');
			$field->setHelpText('Indicate which style the number should be visualized in');
			$field->addOption(self::BasicFormat, 'Text – Basic text, nothing fancy');
			$field->addOption(self::GaugeFormat, 'Gauge – A half-circle interface');
			$field->addOption(self::ProgressBarFormat, 'Progress Bar');
			$field->setDefaultValue(self::BasicFormat);
			$group->attachView($field);
			
			// Indicate if a percent difference between the main number and the comparison should be included or not
			$field = new TCv_FormItem_Select('comparison_include_percent', 'Include percent difference');
			$field->setHelpText('Indicate whether or not a percent difference between the primary and comparison numbers should be shown.');
			$field->addOption('0','No – Do not include the percentage difference');
			$field->addOption('1','Yes – Include the percentage difference');
			
			$group->attachView($field);
			
			// Indicate the colour of graphical visualizations
			$field = new TCv_FormItem_Select('comparison_colour', 'Comparison colour');
			$field->setHelpText('Indicate the colour for the graphical elements.');
			foreach (array_keys(self::Colours) as $colour)
			{
				$field->addOption($colour, ucwords($colour));
			}
			$group->attachView($field);
			
			
			$form_items[] = $group;
			
			
			
			// Add the JS class to help manipulate and filter the values
			$params = [
				// Filled in below
				'methods' => [],
				
				//'is_editor' => $this->primar,
				'saved_id' => $this->primary_model_id,
			];
			
			foreach ($dashboard_viewable_models as $model)
			{
				$methods = $model::valueMethods();
				foreach($methods as $name => $method_values)
				{
					$method_values['model_class_name'] = $model;
					$method_values['requires_model'] = !is_subclass_of($model, 'TCm_ModelList') ;
					
					$params['methods'][$model.'::'.$name] = $method_values;
				}
				
			}
			
			$this->addClassJSFile('TMv_DashboardOneNumber_PageEditorForm');
			$this->addClassJSInit('TMv_DashboardOneNumber_PageEditorForm', $params);
		}
		
		return $form_items;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// URL TARGETS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * For a URL target. Returns a list of models that have value methods for the given model class.
	 * @see TMt_DashboardModelViewable
	 * @param string $model_class_name The model class in which the method resides
	 * @return array An array of the id's and human-readable names
	 */
	public static function valueMethodModels(string $model_class_name) : array
	{
		$error_message_for_js = ['error' => "Invalid input; see tungsten console."];
		
		//$this->addConsoleDebug('valueMethodModels');
		// ERROR CHECKING - All done in separate if statements in order to give more detailed error messages,
		// so that it's easier for developers to debug problems that arise here
		
		// Abort if no user logged in
		if (TC_currentUser() === false)
		{
			return [];
		}
		
		// Does the class exist and does it use TMt_DashboardModelViewable
		if (isset($model_class_name) === false || empty($model_class_name))
		{
			//$this->addConsoleError('No class name given.');
			return $error_message_for_js;
		}
		if (class_exists($model_class_name) === false)
		{
			//$this->addConsoleError("Class '{$model_class_name}' does not exist.");
			return $error_message_for_js;
		}
		if (TC_classUsesTrait($model_class_name, TMt_DashboardModelViewable::class) !== true)
		{
			//$this->addConsoleError("Class '{$model_class_name}' does not have the 'TMt_DashboardModelViewable'
			// trait.");
			return $error_message_for_js;
		}
		
		// ACTUAL PROCESSING
		
		$model_list_class = $model_class_name.'List';
		
		// Does this model have an associated list? If no, just return empty; it must not need models.
		if (class_exists($model_list_class) === false)
		{
			//$this->addConsoleMessage("No model list class '{$model_list_class}' found; returning empty model list.");
			return ['models' => []];
		}
		
		// Get the full list of models
		$model_list = $model_list_class::init();
		
		$model_array = $model_list->models();
		
		// Pare down the acquired list to just ids and titles; nothing more is needed.
		$result_array = [];
		foreach ($model_array as $model)
		{
			$result_array[] = [
				'id' => $model->id(),
				'title' => $model->title()
			];
		}
		
		return ['models' => $result_array];
	}
	
	
}
