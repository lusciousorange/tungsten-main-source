<?php
class TMv_DashboardHeading extends TCv_View
{
	use TMt_DashboardView;
	
	protected $heading_text;
	
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->addClassCSSFile('TMv_DashboardHeading');
	}
	
	/**
	 * Adds a string or html to this view.
	 *
	 * @param string $html The text or HTML to be added to this view.
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 * @uses TCu_Text
	 */
	public function addText($html, $disable_template_parsing = false)
	{
		$this->heading_text .= $html;
	}
	
	public static function pageContent_ViewTitle() : string
	{
		return 'Dashboard heading';
	}
	
	public static function pageContent_IconCode() : string
	{
		return 'fa-heading';
	}
	
}