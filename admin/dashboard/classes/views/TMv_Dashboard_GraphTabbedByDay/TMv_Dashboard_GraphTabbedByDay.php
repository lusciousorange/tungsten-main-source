<?php

/**
 * Class TMv_Dashboard_GraphTabbedByDay
 *
 * Shows tabbed dashboard (by day) with an hourly graph
 */
class TMv_Dashboard_GraphTabbedByDay extends TMv_TabbedDashboardView
{
	use TMt_DashboardView;

	protected $model_class, $start_hour, $y_axis_title, $tab_configuration;
	protected $where_clause = false;
	protected $query_params = array();
	protected $date_field = 'date_added';
	protected $granularity = 'hour';

	protected $active_hours = array();
	protected $breakdown_net = array();
	protected $is_cumulative = false;

	/**
	 * Returns the daily Breakdown data which can be extended for custom functionality
	 * @return array[][] 2D array of values with date and times as the indices
	 */
	protected function breakdownByDateAndHour()
	{
		/** @var TCm_ModelList $model_list */
		$model_list = ($this->model_class)::init();

		$breakdown = $model_list->hourlyBreakdown(
										$this->start_hour,
										$this->where_clause,
										$this->query_params,
										$this->date_field);

		$this->addBreakdownToNet($breakdown, 'add');
		print $this->start_hour;
		print '<br />'.($this->is_cumulative ? 'yes' : 'no');

		if($this->is_cumulative)
		{
			return $model_list->hourlyBreakdownIntoDays($this->cumulativeBreakdowns(), $this->start_hour);
		}

		return $model_list->hourlyBreakdownIntoDays($breakdown, $this->start_hour);



	}


	/**
	 * Adds a breakdown to the net count of breakdowns
	 * @param int[] $breakdown
	 * @param string $count_effect
	 */
	protected function addBreakdownToNet($breakdown, $count_effect)
	{
		foreach($breakdown as $index => $count)
		{
			// Track if the count ever changed
			if($count != 0)
			{
				$this->active_hours[$index] = true;
			}
			if(isset($this->breakdown_net[$index]))
			{
				if($count_effect == 'add')
				{
					$this->breakdown_net[$index] += $count;
				}
				elseif($count_effect == 'subtract')
				{
					$this->breakdown_net[$index] -= $count;
				}

			}
			else // doesn't exist yet
			{
				if($count_effect == 'add')
				{
					$this->breakdown_net[$index] = $count;
				}
				elseif($count_effect == 'subtract')
				{
					$this->breakdown_net[$index] = $count * -1;
				}

			}
		}

	}


	protected function breakdownByDateAndTenMinute()
	{
		/** @var TCm_ModelList $model_list */
		$model_list = ($this->model_class)::init();

		$breakdown = $model_list->tenMinuteBreakdown(
			$this->start_hour,
			$this->where_clause,
			$this->query_params,
			$this->date_field);
		return $model_list->tenMinuteBreakdownIntoDays($breakdown, $this->start_hour);

	}


	/**
	 * Returns the cumulative breakdowns for a graph
	 * @return array
	 */
	protected function cumulativeBreakdowns()
	{
		$cumulative_breakdown = array();
		$previous_index = false;
		foreach($this->breakdown_net as $time => $count)
		{
			// First value
			if($previous_index === false)
			{
				$cumulative_breakdown[$time] = $count;
			}
			else
			{
				if(!isset($this->active_hours[$time]))
				{
					$cumulative_breakdown[$time] = 0;
				}
				else
				{
					$cumulative_breakdown[$time] = $cumulative_breakdown[$previous_index] + $count;
				}


			}
			$previous_index = $time;

		}

		return $cumulative_breakdown;
	}


	/**
	 * Returns the appropriate 2D array of values
	 * @return int[][]
	 */
	protected function tabValues()
	{
		if($this->tab_configuration == 'weekday_tabs')
		{
			return $this->valuesForWeekdayTabs();
		}
		elseif($this->tab_configuration == 'weekday_calendar_day')
		{
			return $this->valuesForCalendarTabs();
		}
//		elseif($this->tab_configuration == 'year_tabs')
//		{
//			return $this->valuesForYearTabs();
//		}




		return array();

	}


	/**
	 * Returns the values for the data broken down by "weekday". Grouping items with the same "day of the week" into
	 * the same graph.
	 *
	 * @return int[][]
	 */
	protected function valuesForWeekdayTabs()
	{
		if($this->granularity == 'hour')
		{
			$daily_breakdown = $this->breakdownByDateAndHour();
		}
		elseif($this->granularity == 'ten_minute')
		{
			$daily_breakdown = $this->breakdownByDateAndTenMinute();

		}
		$tab_values = array();

		// Get the list of weekdays shown to auto-fill when necessary
		$weekdays_shown = array();
		//$last_date = false;
		foreach($daily_breakdown as $date => $values)
		{
			$date_time = new DateTime($date);
			$weekday_number = $date_time->format('N');
			$weekdays_shown[$weekday_number] = $date_time->format('l');

			// Init if it does't exist
			if(!isset($tab_values[$weekday_number]))
			{
				$tab_values[$weekday_number] = array();
			}

			// save the values
			$tab_values[$weekday_number][$date] = $values;
			//$last_date = $date;
		}




		// Sort by day of week
		ksort($tab_values);

		$renamed_values = array();
		foreach($tab_values as $weekday => $date_values)
		{
			krsort($date_values);
			$renamed_values[$weekdays_shown[$weekday]] =  $date_values;
		}

		return $renamed_values;
	}

	/**
	 * Returns the values for the data broken down by "weekday". Grouping items with the same "day of the week" into
	 * the same graph.
	 *
	 * @return int[][]
	 */
	protected function valuesForCalendarTabs()
	{
		if($this->granularity == 'hour')
		{
			$daily_breakdown = $this->breakdownByDateAndHour();
		}
		elseif($this->granularity == 'ten_minute')
		{
			$daily_breakdown = $this->breakdownByDateAndTenMinute();

		}
		$tab_values = array();

		foreach($daily_breakdown as $date => $values)
		{
			$date_time = new DateTime($date);
			$month_day = $date_time->format('m-d');

			// Init if it does't exist
			if(!isset($tab_values[$month_day]))
			{
				$tab_values[$month_day] = array();
			}

			// save the values
			$tab_values[$month_day][$date] = $values;
		}

		ksort($tab_values);

		$renamed_values = array();
		foreach($tab_values as $month_day => $date_values)
		{
			$date_time = new DateTime('2000-'.$month_day);
			$renamed_values[$date_time->format('F d')] =  $date_values;
		}

		return $renamed_values;


	}



	/**
	 * Returns the maximum value form all the provided tab values
	 * @param int[][] $tab_values
	 * @return int
	 */
	protected function maxForTabValues($tab_values)
	{
		$max = 0;

		// Loop through each tab, creating graphs for each one
		foreach($tab_values as $tab_name => $tab_data_sets)
		{
			foreach($tab_data_sets as $day => $daily_values)
			{
				$new_max = max($daily_values);
				if($new_max > $max)
				{
					$max = $new_max;
				}

			}
		}
		return $max;
	}



	/**
	 * This is where the views are added to this view
	 */
	public function render()
	{
		
		$model_class = $this->model_class;

		// CHECK FOR MISSING MODEL CLASS
		if(!class_exists($model_class))
		{
			$this->addText('<br />Class "' . $model_class . '" does not exist');
			$this->addClass("error");
			return false;
		}

		$tab_values = $this->tabValues();

		$num = 0;
		$max = $this->maxForTabValues($tab_values);

		$ticks = false;

		if($this->granularity == 'ten_minute')
		{
			$date_object = new DateTime('2000-01-10 '.$this->start_hour.':00:00');
			for($num = 0; $num < 12; $num++)
			{
				$ticks[] = $date_object->format('g:ia');
				$date_object->modify("+2 hour");
			}

		}

		//$ticks = false;



		// Loop through each tab, creating graphs for each one
		foreach($tab_values as $tab_name => $tab_data_sets)
		{
			$num++;

			if(count($tab_data_sets) > 0)
			{
				//print ''

				$graph = new TCv_LineGraph($this->id() . '_' . $num);
				if($this->granularity == 'hour')
				{
					$graph->setXAxisTitle("Hour");
				}
				else
				{
					$graph->setXAxisTitle("Ten Minute Span");

				}

				$graph->setYAxisTitle($this->y_axis_title);
				$graph->showLegend();
				$graph->setYMax($max);
				if($ticks)
				{
					$graph->setTickValues($ticks);
				}


				$max_value = 0;
				$max_location = 0;
				// Loop through the data sets add
				foreach($tab_data_sets as $day => $daily_values)
				{
					$data_set_max = max($daily_values);
					if($data_set_max > $max_value)
					{
						$max_value = $data_set_max;
						$max_location = array_search($max_value, array_values($daily_values));
					}
				
					$date = new DateTime($day);
					$graph->addDataSet($date->format('l, F d Y'), $daily_values);

				}
				
				// Set the vertical marker to the max value found
				$graph->showVerticalMarker($max_location);
				
				
				$this->addTabView($tab_name, $graph);
			}
		}


		parent::render();

	}

	/**
	 * This function is used to set the form items that are shown for this particular dashboard item. The TCv_Form item
	 * that is being shown is passed as a parameter and those values are loaded as properties in your model.
	 *
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		$field = new TCv_FormItem_HTML('how_to', 'How To Use This Dashboard');
		$field->addText('This dashboard pulls a database summary of values from a given Model-List class in the 
		system. You must specify a valid Model Class Name (extends `TCm_ModelList`) and if no override method is provided, 
		then `hourlyBreakdownIntoDays()` will be used.');
		$form_items['how_to'] = $field;


		// Extra validation on this dashboard form to ensure non-admins can't alter these values
		if(TC_currentUser()->isAdmin())
		{
			$field = new TCv_FormItem_TextField('model_class', 'Model List Class Name');
			$field->setHelpText('Indicate the model class name that will be used to find the data. ');
			$field->setIsRequired();
			$form_items['model_class'] = $field;

			$field = new TCv_FormItem_Select('start_hour', 'Start Hour');
			$field->setHelpText('Indicate the "hour of day" which breaks up the days. ');
			for($i=0; $i<24; $i++)
			{
				$field->addOption($i, str_pad($i,2,'0', STR_PAD_LEFT).':00');
			}
			$form_items['start_hour'] = $field;

			$field = new TCv_FormItem_TextField('y_axis_title', 'Y-Axis Title');
			$field->setHelpText('The title for the y-axis ');
			$field->setDefaultValue('Items Per Hour');
			$field->setIsRequired();
			$form_items['y_axis_title'] = $field;

			$field = new TCv_FormItem_Select('tab_configuration', 'Multi-Year Handling');
			$field->setHelpText('Indicate how the graph should handle multi-year values with tabs and graph lines.');
			$field->setIsRequired();
			//$field->addOption('year_tabs','Year Tabs - Multiple days as separate graph lines');
			$field->addOption('weekday_tabs','Weekday Tabs - Grouped by day of week (eg: Friday), each day as separate graph lines');
			$field->addOption('weekday_calendar_day','Calendar Day Tabs - Grouped by calendar day (eg: Dec 24), years as separate graph lines');

			$form_items['y_axis_title'] = $field;

			$field = new TCv_FormItem_Select('granularity', 'Hourly Or 10-Minute');
			$field->setHelpText('Indicate the level of granularity for the graph.');
			$field->setIsRequired();
			//$field->addOption('year_tabs','Year Tabs - Multiple days as separate graph lines');
			$field->addOption('hour','Hourly - Grouped into the hour blocks (eg: 4:00 to 4:59)');
			$field->addOption('ten_minute','10-Minute - Grouped into 10 minute blocks (eg: 4:00 to 4:09)');

			$form_items['granularity'] = $field;

			$field = new TCv_FormItem_Select('is_cumulative', 'Cumulative');
			$field->setHelpText('Indicates if the graph should add values as it goes to the previous.');
			$field->setIsRequired();
			//$field->addOption('year_tabs','Year Tabs - Multiple days as separate graph lines');
			$field->addOption('0','No – Each value is displayed separately from the one before and after');
			$field->addOption('1','Yes - Next values are added/subtracted to the previous');

			$form_items['is_cumulative'] = $field;

		}

		return $form_items;
	}


	public static function pageContent_ViewTitle() : string
	{
		return 'Daily graph by hour';
	}

	public static function pageContent_IconCode() : string
	{
		return 'fa-chart-bar';
	}

}