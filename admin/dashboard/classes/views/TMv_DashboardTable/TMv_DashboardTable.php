<?php
class TMv_DashboardTable extends TCv_HTMLTable
{
	use TMt_DashboardView;
	
	protected $model_class, $model_id, $method_name, $explanation;
	protected $column_table_classes = [];
	
	
	public function render()
	{
		if($this->explanation !='<p></p>')
		{
			$this->addText($this->explanation);
		}
		
		$this->addClassCSSFile('TMv_DashboardTable');
		
		// Common styles that are currently part of the dashboard setup
		$this->addClass('tabular_data');
		$this->addClass('scrollable');
		
		// Loop through the data
		$values = $this->callModelMethod($this->model_class, $this->model_id, $this->method_name);
		
		// Grab the column config if they exist, remove them from the return response
		$column_config = false;
		$use_sum_columns = false;
		if(isset($values['column_config']))
		{
			$column_config = $values['column_config'];
			unset($values['column_config']);
		}
		
		
		// Deal with column config, generate them, prep sum columns as well
		if($column_config)
		{
			$sum_columns_values = [];
		
			$row = new TCv_HTMLTableRow();
			$column_number = 0;
			foreach($column_config as $column_config_value)
			{
				// Set the starting sum for each column to zero
				$sum_columns_values[$column_number] = 0;
				
				// Update the flag that we actually use column sums
				if($column_config_value['sum'])
				{
					$use_sum_columns = true;
				}
				$this->column_table_classes[$column_number] = $column_config_value['class'];
				$row->createHeadingCellWithContent(
					trim($column_config_value['title']),
					$this->classNameForColumnNumber($column_number));
				$column_number++;
			}
			$this->attachHeaderRow($row);
		}
		
		foreach($values as $table_rows)
		{
			$row = new TCv_HTMLTableRow();
			$column_number = 0;
			foreach($table_rows as $cell_value)
			{
				$row->createCellWithContent($cell_value, $this->classNameForColumnNumber($column_number));
				
				// Deal with sum columns, best while processing for the first time, likely refactored at some point
				if($column_config[$column_number]['sum'])
				{
					if(is_numeric($cell_value))
					{
						$sum_columns_values[$column_number] += $cell_value;
					}
				}
				
				$column_number++;
			}
			$this->attachView($row);
			
		}
		
		if($use_sum_columns)
		{
			$row = new TCv_HTMLTableRow();
			foreach($sum_columns_values as $column_number => $sum_value)
			{
				if($column_config[$column_number]['sum'] === true)
				{
					$row->createCellWithContent($sum_value, $this->classNameForColumnNumber($column_number));
				}
				else
				{
					$row->createCellWithContent('', $this->classNameForColumnNumber($column_number));
				}
				
			}
			$this->attachFooterRow($row);
		}
		
	}
	
	/**
	 * Calculates the classname for the column number which computes if it's left, center, right (which returns
	 * column_left, column_center, column_right respectively) otherwise it returns the item as a classname. If
	 * nothing is found, then false is returned.
	 * @param int $number
	 * @return bool|string
	 */
	protected function classNameForColumnNumber($number)
	{
		// Lazy loading and caching to avoid excess work
		if(isset($this->column_table_classes[$number]))
		{
			return $this->column_table_classes[$number];
		}
		
		return false;
	}

	
	/**
	 * Manually set the values for the model call. This is often done in extended views that have a specific call.
	 * @param $model_class
	 * @param $model_id
	 * @param $method_name
	 */
	protected function setModelCallValues($model_class, $model_id, $method_name)
	{
		$this->model_class = $model_class;
		$this->model_id = $model_id;
		$this->method_name = $method_name;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$field = new TCv_FormItem_HTML('how_to', 'How to use this dashboard');
		$field->addText('This dashboard presents a table of data. The numbers are pulled using common programming calls that require some knowledge of how the system is developed. ');
		$form_items[] = $field;
		
		// Extra validation on this dashboard form to ensure non-admins can't alter these values
		if(TC_currentUser()->isAdmin())
		{
			$field = new TCv_FormItem_HTMLEditor('explanation', 'Explanation');
			$field->setHelpText('Provide a brief explanation of the data. ');
			$field->useBasicEditor();
			$form_items[] = $field;
			
			$field = new TCv_FormItem_TextField('model_class', 'Model class name');
			$field->setHelpText('Indicate the model class name that will be used to find this data. ');
			$field->setIsRequired();
			$form_items[] = $field;
			
			$field = new TCv_FormItem_TextField('model_id', 'Model ID');
			$field->setHelpText('Indicate the model id for the class. ');
			$form_items[] = $field;
			
			$field = new TCv_FormItem_TextField('method_name', 'Method mame');
			$field->setHelpText('Indicate the method name on the model. ');
			$field->setIsRequired();
			$form_items[] = $field;
			
			
			
			
		}
		
		return $form_items;
	}
	
	
	
	public static function pageContent_ViewTitle() : string  { return 'Dashboard table'; }
	
	public static function pageContent_ViewDescription() : string
	{
		return 'A table of data that is presented in the dashboard.';
	}
}