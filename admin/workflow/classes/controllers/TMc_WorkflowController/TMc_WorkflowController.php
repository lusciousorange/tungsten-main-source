<?php

/**
 * Class TMc_WorkflowController
 */
class TMc_WorkflowController extends TSc_ModuleController
{
	/**
	 * TSc_PagesController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
//		$this->model = TC_website()->user();

		$this->enableNavigationSubsections();
	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets();

		$this->URLTargetWithName('list')->setValidationClassAndMethod('TMm_WorkflowModule', 'currentUserIsSuper');

//		$target = TSm_ModuleURLTarget::init('dashboard');
//		$target->setTitle('Dashboard');
//		$target->setViewName('TMv_WorkflowDashboard');
//		$target->setAsSubsectionParent();
//		$this->addModuleURLTarget($target);

//		$target = TSm_ModuleURLTarget::init( '');
//		$target->setNextURLTarget('dashboard');
//		$this->addModuleURLTarget($target);
		
		$this->generateDefaultURLTargetsForModelClass(
			'TMm_WorkflowStage',
			'stage-');

		$this->URLTargetWithName('stage-list')->setValidationClassAndMethod('TMm_WorkflowModule', 'currentUserIsSuper');

//		$target = TSm_ModuleURLTarget::init('view');
//		$target->setTitle('Kanban');
//		$target->setViewName('TMv_WorkflowKanban');
//		$target->setModelName('TMm_Workflow');
//		$target->setModelInstanceRequired();
//		$target->setParentURLTargetWithName('edit');
//		$this->addModuleURLTarget($target);

		$item = TSm_ModuleURLTarget::init( 'preview-comment-email');
		$item->setViewName('TMv_WorkflowPeopleEmail');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'preview-stage-email');
		$item->setViewName('TMv_WorkflowStageEmail');
		$item->setModelName('TMm_WorkflowStageModelHistory');
		$item->setModelInstanceRequired();
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'comment-toggle-resolved');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleResolved');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'comment-delete');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('delete');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'add-workflow-to-model');
		$item->setModelName('TMm_Workflow');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('addModel');
		$item->setPassValuesIntoActionMethodType('url',2,3);
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'disable-workflow-item');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('disable');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		//////////////////////////////////////////////////////
		//
		// MOVE STAGES
		// The url targets related to moving a Workflow from
		// one stage to another
		//
		//////////////////////////////////////////////////////

		$item = TSm_ModuleURLTarget::init( 'change-workflow-stage');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('moveToStage');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'move-to-next-stage');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setTitle('Next stage');
		$item->setModelActionMethod('moveToNextStage');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'move-to-previous-stage');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('moveToPreviousStage');
		$item->setTitle('Previous stage');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'workflow-summary');
		$item->setViewName('TMv_WorkflowSummary');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setTitle('Workflow summary');
		$item->setReturnAsJSON();
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'workflow-status-box');
		$item->setViewName('TMv_WorkflowItemStatusBox');
		$item->setModelName('TMm_WorkflowModelMatch');
		$item->setModelInstanceRequired();
		$item->setTitle('Workflow status box');
		$item->setReturnAsJSON();
		$this->addModuleURLTarget($item);
		
		
		
		
		//////////////////////////////////////////////////////
		//
		// SUBMENUS
		//
		//////////////////////////////////////////////////////
		$this->defineSubmenuGroupingWithURLTargets('edit');//,'workflow-stage-list');//,'kanban');

	}
	


}
?>