<?php

/**
 * Class TMm_WorkflowStageModelHistory
 *
 * Represents a single status of a model for a given stage in the history. A model will have multiples of these as it
 * shows the history of it's progression.
 *
 * A model can have multiple workflows and the model progresses through the various stages of each of those workflows.
 * This model represents the history of when that model was in that stage for that workflow. We use the word history,
 * because it stores all the values, not just the current value.
 *
 * In fact the most recent value for a model is the current stage for a workflow. Note that this table doesn't track
 * workflow_ids, so there is some parsing required to ensure you have the most recent stage for the correct workflow.
 *
 * The collection of all these items for a single model_name and model_id represent the complete history of how that
 * model progressed through all the workflows.
 *
 */
class TMm_WorkflowStageModelHistory extends TCm_Model
{
	protected int $match_id;
	protected $stage_id, $workflow_model_match_id, $user_id, $is_current_stage;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_stages'; // [string] = The table for this class
	public static $model_title = 'Stage';
	public static $primary_table_sort = 'date_added DESC';
	
	public function __construct($id)
	{
		parent::__construct($id);
		
		// Validate that the stage still exists
		if(!$this->stage())
		{
			// No stage, this is an orphaned model stage, just delete it
			$this->delete();
			
			// Ensure the constructor fails
			$this->destroy();
			return false;
		}
	}

	/**
	 * Returns the stage for this item
	 * @return bool|TMm_WorkflowStage
	 */
	public function stage()
	{
		return TMm_WorkflowStage::init($this->stage_id);
	}

	/**
	 * Returns the user for this item
	 * @return bool|TMm_User
	 */
	public function user()
	{
		return TMm_User::init($this->user_id);
	}


//	/**
//	 * Move this item to a new stage, which means creating a new item with similar properties.
//	 * @param int $id Not used. The old stage id
//	 * @param int $id_2 The new stage id
//	 * @return TMm_WorkflowStageModelHistory The new history stage ID that was created
//	 */
//	public function moveToStage($id, $id_2)
//	{
//		// Create a new stage with the same values but
//		$values = array();
//		$values['stage_id'] = $id_2;
//		$values['user_id'] = TC_currentUser()->id();
//		$values['workflow_model_match_id'] = $this->workflow_model_match_id;
//
//		$history_stage = TMm_WorkflowStageModelHistory::createWithValues($values);
//		$old_stage = TMm_WorkflowStageModelHistory::init($id);
//		$old_stage->updateDatabaseValue('is_current_stage', 0);
//
//		return $history_stage;
//
//	}

	/**
	 * Returns the model for this workflow stage
	 * @return TCm_Model|TCu_Item|TMt_WorkflowItem
	 *
	 */
	public function model()
	{
		return $this->modelMatch()->model();
	}

	/**
	 * Returns the WorkflowModelMatch for this model
	 * @return bool|TMm_WorkflowModelMatch
	 */
	public function modelMatch()
	{
		return TMm_WorkflowModelMatch::init($this->workflow_model_match_id);
	}
	
	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model
	 * @return bool|TCm_Model
	 */
	public static function createWithValues ($values, $bind_param_values = array(), $return_new_model = true)
	{
		/** @var TMm_WorkflowStageModelHistory $new_model_stage */
		$new_model_stage = parent::createWithValues($values, $bind_param_values,$return_new_model);

		// If it's the very first stage, don't send any notifications
		// Just get out
//		if(sizeof($new_model_stage->stages()) <= 1)
//		{
//			return $new_model_stage;
//		}

//		// Loop through and find out if user wants to receive the email
//		foreach($new_model_stage->people() as $user)
//		{
//			if($user->valueForProperty('workflow_email_stages'))
//			{
//				$email = new TMv_WorkflowstageEmail($new_model_stage, $user);
//				$email->send();
//			}
//
//		}


//		// HANDLE SLACK CALLS
//		// If Slack is installed
//		if(
//			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
//			class_exists('TMm_SlackClient') && // We also need the proper class
//			TC_getModuleConfig('workflow','slack_for_stages') // Respect system pref for Slack and Workflow
//			)
//		{
//			/** @var TMm_SlackClient $slack_client */
//			$slack_client = TMm_SlackClient::init();
//
//			$model_title = $new_model_stage->model()->title();
//			$stage_title = $new_model_stage->stage()->title();
//
//
//			$title_text = $slack_client->cleanTextForSlack(TC_currentUser()->fullName()). " changed the status for " . $new_model_stage->typeTitle() ." : <".
//				$slack_client->fullDomainName().$new_model_stage->model()->adminEditURL()
//				."|".$slack_client->cleanTextForSlack($model_title).'>';
//
//			$slack_client->addText($title_text);
//
//			// Find list of people
//			$people_names = array();
//			foreach($new_model_stage->people() as $person)
//			{
//				$people_names[] = $slack_client->slackCodeForUser($person);
//			}
//
//			$attachment = array(
//				"fallback" => "",
//				"color" => '#'.$new_model_stage->stage()->color(),
//				"fields" => array(
//					array(
//						"title" => "Stage",
//						"value" => $slack_client->cleanTextForSlack($stage_title)
//					),
//					array(
//						"title" => "People",
//						"value" => implode(', ',$people_names)
//					)
//
//				)
//			);
//			$slack_client->addAttachment($attachment);
//			$slack_client->sendIncomingWebhook();
//
//
//		}
		return $new_model_stage;
	}


	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				'stage_id' => [
					'title'         => 'Stage ID',
					'comment'       => 'The ID of the stage',
					'type'          => 'TMm_WorkflowStage',
					'nullable'      => false,
				],
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The ID of the stage',
					'type'          => 'TMm_User',
					'nullable'      => false,
				],
				'workflow_model_match_id' => [
					'title'         => 'Workflow Model Match ID',
					'comment'       => 'The Workflow model ID for this match',
					'type'          => 'TMm_WorkflowModelMatch',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_WorkflowModelMatch',
						'delete'        => 'CASCADE'
					],
				],
				'is_current_stage' => [
					'title'         => 'Is current stage',
					'comment'       => 'Tracks whether or not this is the current stage of the model workflow match',
					'type'          => 'tinyint(1) unsigned NOT NULL DEFAULT 1',
					'nullable'      => false,
				],
				'model_id' => [
					'delete' => true,
				],
				'model_name' => [
					'delete' => true,
				]
			];
	}

}
?>