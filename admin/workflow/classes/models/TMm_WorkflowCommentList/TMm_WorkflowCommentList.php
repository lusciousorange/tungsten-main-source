<?php

/**
 * Class TMm_WorkflowCommentList
 */
class TMm_WorkflowCommentList extends TCm_ModelList
{
	/**
	 * TMm_UserGroupList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_WorkflowComment',false);
	}

	/**
	 * Returns the most recent X items
	 * @param int $max (Optional) Default 20.
	 * @return TMm_WorkflowComment[]
	 */
	public function mostRecent($max = 20)
	{
		$models = array();
		$query = "SELECT * FROM `workflow_model_comments` ORDER BY date_added DESC LIMIT ".$max;
		$result = $this->DB_Prep_Exec($query);
		while($row = $result->fetch())
		{
			/** @var TMm_WorkflowComment $comment */
			$comment = TMm_WorkflowComment::init($row);
			$models[$comment->dateAdded().'-'.$comment->contentCode()] = $comment;

		}

		return $models;
	}
	
	
}

?>