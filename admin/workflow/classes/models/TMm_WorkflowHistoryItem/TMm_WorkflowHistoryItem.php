<?php

/**
 * Class TMm_WorkflowHistoryItem
 *
 * This is a base class that is extended by other models in the module. The different types of items all have similar
 * setups, so this provides a central class.
 *
 * The naming is someone awkward, but it's accurate.
 */
class TMm_WorkflowHistoryItem extends TCm_Model
{
	protected $user_id, $model_name, $model_id;
	protected $people = false;
	protected $stages = false;


	// DATABASE TABLE SETTINGS
	public function __construct($id)
	{
		parent::__construct($id);
	}


	/**
	 * Returns the user for this item
	 * @return bool|TMm_User
	 */
	public function user()
	{
		return TMm_User::init( $this->user_id);
	}


	/**
	 * Returns the model name for this item
	 * @return string
	 */
	public function modelName()
	{
		return $this->model_name;
	}

	/**
	 * Returns the model ID for this history item
	 * @return integer
	 */
	public function modelID()
	{
		return $this->model_id;
	}

	/**
	 * Returns the model for this item
	 * @return bool|TCm_Model
	 */
	public function model()
	{
		return ($this->modelName())::init($this->modelID());
	}
	
	/**
	 * Returns if this item uses a model provided
	 * @param TCm_Model|TMt_WorkflowItem $model
	 * @return bool
	 */
	public function usesModel($model)
	{
		return $model->id() == $this->modelID() && $model->baseClassName() == $this->modelName();
	}
	
	
	
	/**
	 * Returns the title for the type of item being shown
	 * @return string
	 */
	public function typeTitle()
	{
		$model_name = $this->modelName();
		return strtolower($model_name::$model_title);
	}

	/**
	 * Returns the list of users (people) who are associated with this item
	 * @return TMm_User[]
	 */
	public function people()
	{
		if($this->people === false)
		{
			$this->people = array();
			$query = "SELECT u.* FROM workflow_model_users  INNER JOIN users u USING(user_id) WHERE model_name = :model_name and model_id = :model_id";
			$result = $this->DB_Prep_Exec($query, array('model_name' => $this->modelName(), 'model_id' => $this->modelID()));
			while($row = $result->fetch())
			{
				$this->people[] = TMm_User::init( $row);
			}
		}

		return $this->people;
	}

	/**
	 * Returns the list of previous stages
	 * @return TMm_WorkflowStageModelHistory[]
	 */
	public function stages()
	{
		if($this->stages === false)
		{
			$this->stages = array();
			$query = "SELECT * FROM `workflow_model_stages` WHERE model_name = :model_name and model_id = :model_id";
			$result = $this->DB_Prep_Exec($query, array('model_name' => $this->modelName(), 'model_id' => $this->modelID()));
			while($row = $result->fetch())
			{
				$this->stages[] = TMm_WorkflowStageModelHistory::init($row);
			}
		}

		return $this->stages;
	}


}
?>