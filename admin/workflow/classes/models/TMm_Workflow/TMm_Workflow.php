<?php
/**
 * Class TMm_Workflow
 *
 * This represents one "workflow" which is a customizable series of steps that must be taken to consider something
 * complete. A workflow has multiple stages that it goes through, which are often the responsibility of one of us or
 * a client.
 *
 * The workflow is the definition of those steps, then the workflow is applied to items (models). When we apply a
 * workflow to an item, we're saying "you need to follow these steps" and we're going to tell you what the next step
 * is.
 *
 * For example, every page might use a workflow for written content, but then some pages have an additional workflow
 * for animation, or programming review, or translation. We break up all those processes into separate workflows so
 * we can avoid confusion. We can "finish" the writing and leave the translation until later since they are two
 * workflows applied to the same page.
 */
class TMm_Workflow extends TCm_Model
{
	protected int $workflow_id;
	protected $title, $description, $code, $model_class_names, $model_classes, $color, $category;
	
	protected $comments;
	
	protected string $icon_code = 'fas fa-circle';
	
	protected $color_light;
	
	protected $stages = null;
	protected $stage_matches = null;
	protected $stage_history;
	protected $assigned_users;
	
	protected $models_in_workflow;

	protected $default_model_names = false;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'workflow_id'; // [string] = The id column for this class
	public static $table_name = 'workflows'; // [string] = The table for this class
	public static $model_title = 'Workflow';
	public static $primary_table_sort = 'title ASC';

	public static $workflow_categories = [
		"writing" => "English Copy",
		"visuals" => "Visuals and Layout",
		"translation" => "Translation",
		"programming" => "Data-Driven Pages"
	];
	
	// A set of colours to pick from for the gauge and progress bar formats.
	// @see https://sashamaps.net/docs/resources/20-colors/
	// @see https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=6A1B9A
	// based on the 800 numbers
	public static array $colors = [
		
		// Colors are organized into three rows of 7 to make a color circle
		// ROW 1
		'lavender' => ['code' => '#9c27b0', 'text' => '#FFFFFF'],
		'pink' => ['code' => '#ad1457', 'text' => '#FFFFFF'],
		'red' => ['code' => '#c62828', 'text' => '#FFFFFF'],
		'deep_orange' => ['code' => '#d84315', 'text' => '#FFFFFF'],
		'orange' => ['code' => '#ef6c00', 'text' => '#FFFFFF'],
		'amber' => ['code' => '#ff8f00', 'text' => '#492900'],
		'medium_brown' => ['code' => '#BE9818', 'text' => '#FFFFFF'],
		'lime' => ['code' => '#9E9D24', 'text' => '#FFFFFF'],
		
		// ROW 2
		'purple' => ['code' => '#6a1b9a', 'text' => '#FFFFFF'],
		'maroon' => ['code' => '#550000', 'text' => '#FFFFFF'],
		'brown' => ['code' => '#4e342e', 'text' => '#FFFFFF'],
		'charcoal' => ['code' => '#333333', 'text' => '#FFFFFF'],
		'grey' => ['code' => '#666666', 'text' => '#FFFFFF'],
		'light_grey' => ['code' => '#999999', 'text' => '#111111'],
		'dark_green' => ['code' => '#00544A', 'text' => '#FFFFFF'],
		'olive' => ['code' => '#558b2f', 'text' => '#FFFFFF'],
		
		// ROW 3
		'deep_purple' => ['code' => '#4527a0', 'text' => '#FFFFFF'],
		'dark_navy' => ['code' => '#182058', 'text' => '#FFFFFF'],
		'navy' => ['code' => '#17438C', 'text' => '#FFFFFF'],
		'blue' => ['code' => '#1565c0', 'text' => '#FFFFFF'],
		'light_blue' => ['code' => '#0277bd', 'text' => '#FFFFFF'],
		'cyan' => ['code' => '#00838f', 'text' => '#FFFFFF'],
		'teal' => ['code' => '#00695c', 'text' => '#FFFFFF'],
		'green' => ['code' => '#2e7d32', 'text' => '#FFFFFF'],
		
		
		//	'yellow' =>     ['code' => '#f9a825', 'text' => '#47300B'],
	
	
	];
	
	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	public function title()
	{
		return $this->title;
	}
	
	public function code()
	{
		return strtoupper($this->code);
	}
	
	public function iconCode() : string
	{
		return $this->icon_code;
	}
	
	//////////////////////////////////////////////////////
	//
	// COLORS
	//
	//////////////////////////////////////////////////////
	
	
	public function color()
	{
		return TMm_Workflow::getColorCode($this->color);
	}
	
	/**
	 * Static function to process colors
	 * @param string $value
	 * @return string
	 */
	public static function getColorCode(string $value) : string
	{
		// Determine if we're using a named color
		if(isset(TMm_Workflow::$colors[$value]))
		{
			return str_replace('#','', TMm_Workflow::$colors[$value]['code']);
		}
		
		return strtoupper($value);
	}
	
	/**
	 * Returns the color hex code for this stage
	 * @return string
	 */
	public function colorName()
	{
		return TMm_Workflow::getColorName($this->color);
	}
	
	/**
	 * Returns the readable color based on the overall contrast of the main color chosen
	 * @return string
	 */
	public function readableColor()
	{
		return TMm_Workflow::getColorTextShade($this->color);
	}
	
	/**
	 * Static function to process colors
	 * @param string $value
	 * @return string
	 */
	public static function getColorName(string $value) : string
	{
		// Determine if we're using a named color
		if(isset(TMm_Workflow::$colors[$value]))
		{
			// Return uppercase letters with spaces
			return strtoupper(str_replace('_',' ',$value));
		}
		
		return '#'.strtoupper($value);
	}
	
	public static function getColorTextShade(string $value) : string
	{
		// Using named colors, just return the text value
		if(isset(TMm_Workflow::$colors[$value]))
		{
			return str_replace('#','', TMm_Workflow::$colors[$value]['text']);
		}
		
		// Calculate it instead
		$r = hexdec(substr($value, 0, 2));
		$g = hexdec(substr($value, 2, 2));
		$b = hexdec(substr($value, 4, 2));
		
		$squared_contrast = (
			$r * $r * .299 +
			$g * $g * .587 +
			$b * $b * .114
		);
		
		if($squared_contrast > pow(150, 2))
		{
			return '000000';
		}
		else
		{
		    return 'FFFFFF';
		}
	}
	
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorLight()
	{
		// Using named colors, just return the text value
		if(isset(TMm_Workflow::$colors[$this->color]))
		{
			return str_replace('#','', TMm_Workflow::$colors[$this->color]['text']);
		}
		
		if($this->color_light == null)
		{
			$this->color_light = $this->shadeHexColor($this->color,-85);
		}
		return $this->color_light;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// COLORS
	//
	//////////////////////////////////////////////////////
	
	public function category()
	{
		return $this->category;
	}
	
	public function titleWithCode()
	{
		return $this->code.' : '.$this->title();
	}
	
	/**
	 * Returns the array of model classes for this workflow
	 * @return string[]
	 */
	public function modelClassNames()
	{
		if($this->model_classes == null)
		{
			$this->model_classes = array();
			$classes =  explode(',', $this->model_class_names);
			foreach($classes as $class)
			{
				$class = trim($class);
				if($class != '')
				{
					$this->model_classes[] = $class;
				}
				
			}
		}
		
		return $this->model_classes;
	}
	
	/**
	 * @param TCm_Model|string $model
	 * @return bool
	 */
	public function usesModel($model)
	{
		if($model instanceof TCm_Model)
		{
			return in_array($model->baseClassName(),  $this->modelClassNames());
		}
		elseif(is_string($model))
		{
			return in_array($model::baseClass(), $this->modelClassNames());
		}
		
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// Stages
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns all the stage matches for this workflow
	 * @return TMm_WorkflowStageMatch[]
	 */
	public function stageMatches()
	{
		if($this->stage_matches == null)
		{
			$this->stage_matches = [];
			$query = "SELECT m.* FROM `workflow_stage_matches` m INNER JOIN `workflow_stages` s USING(stage_id) WHERE m.workflow_id = :id ORDER BY s.display_order ASC";
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			while($row = $result->fetch())
			{
				$stage_match = TMm_WorkflowStageMatch::init($row);
				$this->stage_matches[] = $stage_match;
			}
		}
		
		return $this->stage_matches;
	}

	/**
	 *
	 * Returns the actual stages
	 * @return TMm_WorkflowStage[]
	 */
	public function stages()
	{
		if($this->stages == null)
		{
			$this->stages = [];
			
			foreach($this->stageMatches() as $stage_match)
			{
				$this->stages[$stage_match->stage()->id()] = $stage_match->stage();
			}
		}
		return $this->stages;
	}
	
	/**
	 * Creates the first stage if it doesn't exist
	 */
	public function createDefaultStageMatches()
	{
		if ($this->numStages() == 0)
		{
			$values = array();
			$values['workflow_id'] = $this->id();
			$values['stage_id'] = 1;
			TMm_WorkflowStageMatch::createWithValues($values);

			$values = array();
			$values['workflow_id'] = $this->id();
			$values['stage_id'] = 2;
			TMm_WorkflowStageMatch::createWithValues($values);
		}
	}
	
	/**
	 * Returns the number of stages
	 * @return int
	 */
	public function numStages()
	{
		return count($this->stages());
	}
	
	/**
	 * Returns the first stage for this workflow
	 * @return TMm_WorkflowStage|bool
	 */
	public function firstStage()
	{
		$stages = array_values($this->stages());
		if(count($stages) > 0)
		{
			return $stages[0];
		}
		else
		{
			$this->createDefaultStageMatches();
			return TMm_WorkflowStage::init(1);
		}
	}

	/**
	 * Checks whether a certain stage has a match with this workflow
	 * @param TMm_WorkflowStage $stage
	 * @return bool|TMm_WorkflowStageMatch
	 */
	public function findStageMatch(TMm_WorkflowStage $stage)
	{
		$stage_matches = $this->stageMatches();
		foreach($stage_matches as $match)
		{
			if($match->stage()->id() == $stage->id())
			{
				return $match;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns if this stage is currently NOT used by the workflow
	 * @param TMm_WorkflowStage|int $stage The stage that is being tested
	 * @return bool
	 */
	public function stageIsNotUsed($stage)
	{
		if(!$stage instanceof TMm_WorkflowStage)
		{
			$stage = TMm_WorkflowStage::init($stage);
		}
		$stages = $this->stages();
		return !isset($stages[$stage->id()]);
	}
	
	
	
	/**
	 * Update the stages matches in this workflow
	 * @param int[] $stage_ids
	 */
	public function updateWorkflowStages(array $stage_ids) : void
	{
		$stage_list = TMm_WorkflowStageList::init();

		foreach ($stage_list->models() as $stage)
		{
			$is_set = in_array($stage->id(), $stage_ids);
			$this->updateStageMatch($stage, $is_set);
		}
	}
	
	/**
	 * Deals with the Workflow-stage match for a single stage
	 * @param TMm_WorkflowStage $stage
	 * @param bool $is_set
	 */
	public function updateStageMatch($stage, $is_set)
	{
		$match = $this->findStageMatch($stage);

		// if stage is set and there is no match yet, create one
		if($is_set && $match == false)
		{
			$values = array(
				'stage_id' => $stage->id(),
				'workflow_id' => $this->id(),
			);

			TMm_WorkflowStageMatch::createWithValues($values);

		}
		// If stage is not set and match exists, remove it
		elseif(!$is_set && $match instanceof TMm_WorkflowStageMatch)
		{
			$match->delete();
		}
	}
	
	/**
	 * Returns all the stage history items for this workflow
	 * @return TMm_WorkflowStageModelHistory[]
	 */
	public function stageHistory()
	{
		if($this->stage_history == null)
		{
			$this->stage_history = [];
			$query = "SELECT s.* FROM `workflow_model_stages` s
				INNER JOIN `workflow_stages_matches` USING(stage_id)
				WHERE workflow_id = :id 
				ORDER BY s.date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			while($row = $result->fetch())
			{
				$stage_history = TMm_WorkflowStageModelHistory::init($row);
				$this->stage_history[] = $stage_history;
			}
			
		}
		
		return $this->stage_history;
	}

	/**
	 * Returns all the items using this workflow
	 * @return TMt_WorkflowItem[]
	 */
	public function modelsInWorkflow()
	{
		if($this->models_in_workflow == null)
		{
			$this->models_in_workflow = [];
			
			// Loop through the full stage history, which will give you all the models
			foreach($this->stageHistory() as $stage_history)
			{
				$model = $stage_history->model();
				if($model)
				{
					$this->models_in_workflow[$model->id()] = $model;
				}
				
			}
			
		}
		
		return $this->models_in_workflow;
	}

	/**
	 * An array of model names that are using this as default
	 * @return TMm_WorkflowModelDefault[]
	 */
	public function defaultModels()
	{
		if ($this->default_model_names === false)
		{
			$this->default_model_names = array();
			$query = "SELECT * FROM `workflow_model_defaults` WHERE workflow_id = :workflow_id ";
			$result = $this->DB_Prep_Exec($query, array('workflow_id' => $this->id()));
			while($row = $result->fetch())
			{
				$this->default_model_names[$row['model_name']] = TMm_WorkflowModelDefault::init($row);
			}
		}

		return $this->default_model_names;
	}

	/**
	 * Whether this workflow is a default for given model
	 * @param string $model_name
	 * @return bool
	 */
	public function isDefaultForModel($model_name)
	{
		$this->defaultModels(); // Load the models
		return isset($this->default_model_names[$model_name]);
	}

	/**
	 * Tries to get a model default match for model name
	 * @param $model_name
	 * @return bool|TMm_WorkflowModelDefault
	 */
	public function getModelDefaultMatch($model_name)
	{
		if($this->isDefaultForModel($model_name))
		{
			return $this->default_model_names[$model_name];
		}

		return false;
	}

	/**
	 * Update the stages matches in this workflow
	 * @param string[] $model_names
	 */
	public function updateDefaultModels($model_names)
	{
		$models_list = TC_modelsWithTrait('TMt_WorkflowItem');
		
		foreach ($models_list as $name)
		{
			if(is_subclass_of($name,'TMm_PagesMenuItem'))
			{
				$name = 'TMm_PagesMenuItem';
			}
			$is_set = in_array($name, $model_names);
			$this->updateDefaultModel($name, $is_set);
		}
	}

	/**
	 * Deals with the Workflow-stage match for a single stage
	 * @param string $model_name
	 * @param bool $is_set
	 */
	public function updateDefaultModel($model_name, $is_set)
	{
		$match = $this->getModelDefaultMatch($model_name);

		// if stage is set and there is no match yet, create one
		if($is_set && $match == false)
		{
			$values = array(
				'model_name' => $model_name,
				'workflow_id' => $this->id(),
			);

			TMm_WorkflowModelDefault::createWithValues($values);

		}
		// If stage is not set and match exists, remove it
		elseif(!$is_set && $match instanceof TMm_WorkflowModelDefault)
		{
			$match->delete();
		}
	}

	/**
	 * Returns all the comments for this workflow
	 * @return TMm_WorkflowComment[]
	 */
	public function comments()
	{
		if($this->comments == null)
		{
			$this->comments = [];
			$query = "SELECT * FROM `workflow_model_comments` WHERE workflow_id = :workflow_id ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('workflow_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$workflow_comment = TMm_WorkflowComment::init($row);
				if($workflow_comment)
				{
					$this->comments[] = $workflow_comment;
				}
				
				
			}
		}
		
		return $this->comments;
	}
	
	/**
	 * Returns ALL the assigned users for any item in this workflow
	 * @return TMm_WorkflowAssignedUser[]
	 */
	public function assignedUsers()
	{
		if($this->assigned_users == null)
		{
			$this->assigned_users = [];
			$query = "SELECT * FROM `workflow_model_users` WHERE workflow_id = :workflow_id ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('workflow_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$workflow_user = TMm_WorkflowAssignedUser::init($row);
				if($workflow_user)
				{
					$this->assigned_users[] = $workflow_user;
				}
				
				
			}
		}
		
		return $this->assigned_users;
	}
	
	/**
	 * @param string $class_name
	 * @param int $class_id
	 */
	public function addModel($class_name, $class_id)
	{
		/** @var TCm_Model|TMt_WorkflowItem $model */
		$model = $class_name::init($class_id);
		
		if($model)
		{
			if($workflow_item = $model->workflowMatch($this))
			{
				// Existing workflow item
				$workflow_item->enable();
			}
			else
			{
				// No workflow item, then we create one
				$model->createWorkflowMatch($this);
			}
		}
		else
		{
			TC_message('Model with class "'.$class_name.'" not found');
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		$cat_options = '';
		$first = true;
		foreach (TMm_Workflow::$workflow_categories as $id => $value)
		{
			if (!$first)
			{
				$cat_options .= ',';
			}
			$cat_options .= "'".$id."'";
			$first = false;
		}

		return parent::schema() + [

				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title of the workflow',
					'type'          => ' varchar(64)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true
					],
				],
				'icon_code' => [
					'type'          => ' varchar(32)',
					'nullable'      => false,
				],
				'description' => [
					'title'         => 'Description',
					'comment'       => 'Extra info that can be added if a title is not enough',
					'type'          => ' varchar(64)',
					'nullable'      => false,
				],
				'color' => [
					'title'         => 'Hex Color',
					'comment'       => 'The name of the hex color for the workflow',
					'type'          => 'varchar(24)',
					'nullable'      => false,
//					'validations'   => [
//						'pattern'   => '/^[0-9A-F]{6}$/i'
//					],
				],
				'category' => [
					'title'         => 'Category',
					'comment'       => 'Workflows can be sorted into categories',
					'type'          => "ENUM(".$cat_options.") ",
					'nullable'      => true,
				],
				
				// Old values that ends up in DBs
				'user_id' => [
					'delete' => true,
				],
				'is_user_restricted' => [
					'delete' => true,
				],
			
			];
	}
	
}