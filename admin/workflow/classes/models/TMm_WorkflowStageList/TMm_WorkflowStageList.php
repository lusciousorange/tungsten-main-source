<?php
class TMm_WorkflowStageList extends TCm_ModelList
{

	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_WorkflowStage',$init_model_list);

		$this->createDefaultStages();
	}

	public function createDefaultStages()
	{
		if ($this->numStages() == 0)
		{
			$start_values = array(
				'title' => 'Start',
				'color' => '000000',
				'display_order' => 1,
			);
			TMm_WorkflowStage::createWithValues($start_values);

			$done_values = array(
				'title' => 'Done',
				'color' => '009900',
				'display_order' => 2,
				'is_done' => 1
			);
			TMm_WorkflowStage::createWithValues($done_values);
		}
	}

	public function numStages()
	{
		return sizeof($this->models());
	}

	/**
	 * Returns the first stage available
	 * @return bool | TMm_WorkflowStage
	 */
	public function firstStage()
	{
		foreach($this->models() as $model)
		{
			return $model;
		}

		return false;
	}


}

?>