<?php

/**
 * Class TMm_WorkflowModelDefault
 *
 * This class represents the pairing of models with default workflows
 *
 * @author Katie Overwater
 */
class TMm_WorkflowModelDefault extends TCm_Model
{
	protected $workflow_id, $model_name;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'workflow_model_default_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_defaults'; // [string] = The table for this class
	public static $model_title = 'Workflow model default';
	public static $primary_table_sort = 'model_name';

	public function __construct($id, $load_from_table = true)
	{
		parent::__construct($id, $load_from_table);
	}

	/**
	 * Returns the Workflow ID number
	 * @return mixed
	 */
	public function workflowID()
	{
		return $this->workflow_id;
	}

	/**
	 * Returns the Model ID number
	 * @return mixed
	 */
	public function modelName()
	{
		return $this->model_name;
	}

	/**
	 * Returns the Workflow that is used for this match
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the page menu item to add additional fields
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				'model_name' => [
					'title'         => 'Model Name',
					'comment'       => 'The class name of the model in this match',
					'type'          => 'varchar(25)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'workflow_id' => [
					'title'         => 'Workflow ID',
					'comment'       => 'The Workflow ID for this match',
					'type'          => 'TMm_Workflow',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_Workflow',
						'delete'        => 'CASCADE'
					],
				],
			];
	}
}