<?php

/**
 * Class TMm_WorkflowStageModelHistoryList
 */
class TMm_WorkflowStageModelHistoryList extends TCm_ModelList
{
	/**
	 * TMm_WorkflowStageModelHistoryList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = false)
	{
		parent::__construct('TMm_WorkflowStageModelHistory',$init_model_list);
	}

	/**
	 * Returns the most recent X items
	 * @param int $max (Optional) Default 20.
	 * @return TMm_WorkflowStageModelHistory[]
	 */
	public function mostRecent($max = 20)
	{
		$models = array();
		$query = "SELECT * FROM `workflow_model_stages` ORDER BY date_added DESC LIMIT ".$max;
		$result = $this->DB_Prep_Exec($query);
		while($row = $result->fetch())
		{
			/** @var TMm_WorkflowComment $comment */
			$stage = TMm_WorkflowStageModelHistory::init($row);
			$models[$stage->dateAdded().'-'.$stage->contentCode()] = $stage;

		}

		return $models;
	}

//	/**
//	 * @param $model_name
//	 * @return TMm_WorkflowStage[] An array of stages with the indices being the IDs of the models that were asked
//	 * for. If there are N models with that model name, then it will return rows.
//	 */
//	public function currentStagesForModelName($model_name)
//	{
//		$query = "SELECT s.* FROM workflow_model_stages s
//
//		INNER JOIN (
//			SELECT max(date_added) max_post_date, model_id FROM workflow_model_stages WHERE model_name = :model_name GROUP BY model_id
//		) s2
//		ON s.date_added = s2.max_post_date AND s.model_id = s2.model_id
//
//		INNER JOIN ".$model_name::tableName()." m ON s.model_id = m.".$model_name::$table_id_column."
//
//		WHERE s.model_name = :model_name ORDER BY s.date_added ASC
//		";
//
//		$values = array();
//		$result = $this->DB_Prep_Exec($query, array('model_name' => $model_name));
//		while($row = $result->fetch() )
//		{
//			$values[$row['model_id']] = TMm_WorkflowStage::init($row['stage_id']);
//		}
//
//		return $values;
//
//	}
		
}

?>