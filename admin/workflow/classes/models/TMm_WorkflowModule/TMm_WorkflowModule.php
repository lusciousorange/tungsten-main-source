<?php

/**
 * Class TMm_WorkflowModule
 *
 * This is the module for the workflow. Tungsten allows us to customize the module class itself to store module-wide
 * functionality. Earlier iterations of this module relied heavily on this class, however later implementations moved
 * the functionality to the appropriate classes and traits.
 *
 * The class can still be used for module-wide functionality that is not specific to a single model.
 */
class TMm_WorkflowModule extends TSm_Module
{
	protected $workflow_model;
	protected $model_users = array();
	protected $stages = array();
	protected $models_with_workflow;
	
	protected $permitted_users, $user_groups;
	
	protected $workflows;
	
	/**
	 * TMm_WorkflowModule constructor.
	 * @param array|int|bool $module_id
	 */
	public function __construct($module_id = 'workflow')
	{
		parent::__construct($module_id);
		
		$this->setAsAutoUpdate();
	}
	
	/**
	 * @return ?TCm_Model
	 */
	public function currentModel() : ?TCm_Model
	{
		if($this->workflow_model == null)
		{
			$this->workflow_model = $this->determineWorkflowModel();
		}
		return $this->workflow_model;
	}
	
	/**
	 * Returns the array of model names that use workflows. The models must have the `TMt_WorkflowItem` trait.
	 * @return string[]
	 */
	public function modelsWithWorkflow()
	{
		if($this->models_with_workflow == null)
		{
			$this->models_with_workflow = TC_modelsWithTrait('TMt_WorkflowItem');
		}
		
		return $this->models_with_workflow;
		
	}
	
	/**
	 * Determines the workflow model that is being viewed
	 * @return null|TMt_WorkflowItem|TCm_Model
	 */
	protected function determineWorkflowModel() : ?TCm_Model
	{
		// Loop through the active models which indicate all the possible items that might be being viewed
		foreach (TC_activeModels() as $active_model)
		{
			foreach($this->modelsWithWorkflow() as $model_name)
			{
				if($active_model instanceof $model_name)
				{
					return $active_model;
				}
			}
		}
		
		return null;
		
	}

	/**
	 * Returns all the workflows on the website
	 * @return TMm_Workflow[]
	 */
	public function workflows()
	{
		if($this->workflows == null)
		{
			$this->workflows = [];
			$query = "SELECT * FROM `workflows` ORDER BY workflow_id ASC ";
			$result = $this->DB_Prep_Exec($query);
			while($row = $result->fetch())
			{
				$this->workflows[] = TMm_Workflow::init($row);
			}
		}
		return $this->workflows;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// USERS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of user groups who have access to the workflow
	 * @return TMm_UserGroup[]
	 */
	public function userGroups()
	{
		if($this->user_groups === null)
		{
			$this->user_groups = [];
			$this->user_groups[] = TMm_UserGroup::init(1); // add administrators
			
			$group_ids = explode(',', TC_getModuleConfig('workflow','permitted_user_groups'));
			foreach($group_ids as $user_group_id)
			{
				if(trim($user_group_id) != '')
				{
					$user_group = TMm_UserGroup::init($user_group_id);
					if($user_group)
					{
						$this->user_groups[] = $user_group;
					}
				}
			}
		}
		
		return $this->user_groups;
	}
	
	/**
	 * Returns the users who have access to interact with workflow
	 * @return TMm_User[]
	 */
	public function permittedUsers()
	{
		if($this->permitted_users === null)
		{
			$this->permitted_users = [];
			
			foreach($this->userGroups() as $user_group)
			{
				foreach($user_group->users() as $user)
				{
					$this->permitted_users[$user->id()] = $user;
				}
			}
			
		}
		
		return $this->permitted_users;
	}


	/**
	 * Returns if the current user is a super user. This function checks if they are an admin in the system. 
	 * @return bool
	 */
	public function currentUserIsSuper()
	{
		return TC_currentUser()->isAdmin();
	}
	
	//////////////////////////////////////////////////////
	//
	// INSTALL SETUP
	//
	// Workflows have a feature toggle, which should hide
	// them in all forms
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Extend functionality to check if the feature is set
	 * @return bool
	 */
	public function installable()
	{
		return TC_getConfig('use_workflow');
	}
	
	/**
	 * Returns if the module is installed
	 * @return bool
	 */
	public function installed()
	{
		if(!TC_getConfig('use_workflow'))
		{
			return false;
		}
		
		return parent::installed();
	}
	
}