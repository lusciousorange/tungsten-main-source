<?php
class TMm_WorkflowList extends TCm_ModelList
{

	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_Workflow',$init_model_list);
		
	}

	/**
	 * @param bool $subset_name
	 * @return TMm_Workflow[]
	 */
	public function models($subset_name = false)
	{
		return parent::models($subset_name);
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}


}

?>