<?php

/**
 * Class TMm_WorkflowStageMatch
 *
 * This class represents the pairing of one workflow with one stage.
 *
 * @author Katie Overwater
 */
class TMm_WorkflowStageMatch extends TCm_Model
{
	protected int $workflow_stage_match_id;
	protected int $workflow_id;
	protected int $stage_id;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'workflow_stage_match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_stage_matches'; // [string] = The table for this class
	public static $model_title = 'Workflow stage match';
	public static $primary_table_sort = 'workflow_id';

	public function __construct($id, $load_from_table = true)
	{
		parent::__construct($id, $load_from_table);
	}

	/**
	 * Returns the Workflow ID number
	 * @return mixed
	 */
	public function workflowID()
	{
		return $this->workflow_id;
	}

	/**
	 * Returns the Model ID number
	 * @return mixed
	 */
	public function stageID()
	{
		return $this->stage_id;
	}

	/**
	 * Returns the Stage used in this match
	 * @return TMm_WorkflowStage|bool
	 */
	public function stage()
	{
		return TMm_WorkflowStage::init($this->stage_id);
	}

	/**
	 * Returns the Workflow that is used for this match
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}

	/**
	 * Display order
	 * @return mixed
	 */
	public function displayOrder()
	{
		return $this->stage()->displayOrder();
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the page menu item to add additional fields
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				'stage_id' => [
					'title'         => 'Model ID',
					'comment'       => 'The model ID for this match',
					'type'          => 'TMm_WorkflowStage',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_WorkflowStage',
						'delete'        => 'CASCADE'
					],
				],
				'workflow_id' => [
					'title'         => 'Workflow ID',
					'comment'       => 'The Workflow ID for this match',
					'type'          => 'TMm_Workflow',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
						'methods'       => [
							[
								'method_name' => 'stageIsNotUsed',
								'mode' => 'create',
								'class_field_name' => 'workflow_id',
								'parameter_field_names' => ['stage_id'],
								'error_message' => 'Stage already applied to workflow'
							]
						],
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_Workflow',
						'delete'        => 'CASCADE'
					],
				],
//				'display_order' => [
//					'title'         => 'Display order',
//					'comment'       => 'The order of this item, in relation to items with the same parent',
//					'type'          => 'smallint(5) unsigned',
//					'nullable'      => false,
//				],
			];
	}
}