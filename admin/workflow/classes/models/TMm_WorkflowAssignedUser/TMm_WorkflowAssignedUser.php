<?php

/**
 * Class TMm_WorkflowAssignedUser
 *
 * This model represents a match table connecting a workflow model with user in the system. This lets us say that a
 * user is responsible for a particular workflow for a particular model. This takes into account the fact that a
 * model might have multiple workflows associated with it, so the user might only be responsible for one of those
 * workflows.
 *
 * This is why we have a `model_name` and `model_id` which represent the model, but we also have a `workflow_id`
 * since there can be multiples and then there's a `user_id` because it's a specific user.
 *
 */
class TMm_WorkflowAssignedUser extends TMm_WorkflowHistoryItem
{
	protected $workflow_model_match_id, $user_id;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_users'; // [string] = The table for this class
	public static $model_title = 'User';
	public static $primary_table_sort = 'date_added DESC';

	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	/**
	 * Returns the workflow for this item
	 * @return bool|TMm_WorkflowModelMatch
	 */
	public function workflowModelMatch()
	{
		return TMm_WorkflowModelMatch::init($this->workflow_model_match_id);
	}
	
	
	
	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model (Optional) Default true. Indicates if the newly created object should be
	 * instantiated and returned. This is a heuristic option to speed up complex calls where the model isn't needed
	 * @return bool|TCm_Model
	 */
	public static function createWithValues($values, $bind_param_values = array(), $return_new_model = true)
	{

		$new_user_match = parent::createWithValues($values, $bind_param_values);

//		// Loop through and find out if user wants to receive the email
//		$user = $new_user_match->user();
//		if($user->valueForProperty('workflow_email_assigned'))
//		{
//			$email = new TMv_WorkflowPeopleEmail($new_user_match, $user);
//			$email->send();
//		}
//
//
//
//		// HANDLE SLACK CALLS
//
//		// If Slack is installed
//		if(
//			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
//			class_exists('TMm_SlackClient') && // We also need the proper class
//			TC_getModuleConfig('workflow','slack_for_user_assignment') // Respect system pref for Slack and Workflow
//		)
//		{
//			$slack_client = TMm_SlackClient::init();
//
//			$model_title = $new_user_match->model()->title();
//
//
//			$title_text = $slack_client->slackCodeForUser($new_user_match->user()). " has been added to the " . $new_user_match->typeTitle() ." : <".
//				$slack_client->fullDomainName().$new_user_match->model()->adminEditURL()
//				."|".$slack_client->cleanTextForSlack($model_title).'>';
//
//			$slack_client->addText($title_text);
//
//
//			$slack_client->sendIncomingWebhook();
//
//
//		}
		return $new_user_match;
	}
	
	
	
	
	public static function numModelsWithNameForUser($workflow_model_match_id, $user)
	{
		$model = new TCm_Model(false);
		$query = "SELECT * FROM `workflow_model_users` WHERE user_id = :user_id AND workflow_model_match_id = :workflow_model_match_id";
		$result = $model->DB_Prep_Exec($query, array('user_id' => $user->id(), 'workflow_model_match_id' => $workflow_model_match_id));
		return $result->rowCount();
	}


	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the page menu item to add additional fields
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The user who submitted the comment',
					'type'          => 'TMm_User',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'CASCADE'
					],
				],
				'workflow_model_match_id' => [
					'title'         => 'Workflow Model Match ID',
					'comment'       => 'The Workflow model ID for this match',
					'type'          => 'TMm_WorkflowModelMatch',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_WorkflowModelMatch',
						'delete'        => 'CASCADE'
					],
				],
			];
	}
}
?>