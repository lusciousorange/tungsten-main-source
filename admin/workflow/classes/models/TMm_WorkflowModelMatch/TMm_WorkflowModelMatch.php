<?php

/**
 * Class TMm_WorkflowModelMatch
 *
 * This class represents the pairing of one workflow with one model. A workflow can be applied to multiple models and
 * a model can have multiple workflows.
 *
 * In the old setup, this was a `TMm_WorkflowItem` but that name has been used for other purposes. This represents
 * the models in a match table which stores the pairs of workflows and models.
 *
 * @author Katie Overwater
 */
class TMm_WorkflowModelMatch extends TCm_Model
{
	protected int $workflow_model_match_id;
	protected $workflow_id, $model_name, $model_id, $is_enabled;

//	protected $stage_models;
	protected $workflow = false;
	protected $model = false;
	protected $users = false;
	protected $comments;
	protected $stages;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'workflow_model_match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_matches'; // [string] = The table for this class
	public static $model_title = 'Workflow model match';
	public static $primary_table_sort = 'model_name, model_id';

	public function __construct($id, $load_from_table = true)
	{
		parent::__construct($id, $load_from_table);

		// Validate that the stage still exists
		if($this->modelName() != '' && $this->modelID() != '' && !$this->model())
		{
			// No stage, this is an orphaned workflow item, just delete it
			$this->delete();
			// Ensure the constructor fails
			$this->destroy();
			return false;
		}
	}

	/**
	 * Returns the Workflow ID number
	 * @return mixed
	 */
	public function workflowID()
	{
		return $this->workflow_id;
	}

	/**
	 * Returns the Model ID number
	 * @return mixed
	 */
	public function modelID()
	{
		return $this->model_id;
	}

	/**
	 * Returns the model name for this item
	 * @return string
	 */
	public function modelName()
	{
		return $this->model_name;
	}

	/**
	 * Returns the Model used in this match
	 * @return TCm_Model|bool
	 */
	public function model()
	{
		if ($this->model === false)
		{
			if(class_exists($this->model_name))
			{
				$this->model = ($this->model_name)::init($this->model_id);
			}
		}

		return $this->model;

	}

	/**
	 * Returns the Workflow that is used for this match
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		if ($this->workflow === false)
		{
			$this->workflow = TMm_Workflow::init($this->workflow_id);
		}

		return $this->workflow;
	}

	/**
	 * Returns if the item is enabled
	 * @return bool
	 */
	public function isEnabled()
	{
		return $this->is_enabled;
	}

	/**
	 * Enables this workflow item
	 */
	public function enable()
	{
		$this->updateDatabaseValue('is_enabled', 1);
	}

	/**
	 * Disables this workflow item
	 */
	public function disable()
	{
		$this->updateDatabaseValue('is_enabled', 0);
	}

	/**
	 * Returns all the comments for this workflow
	 * @return TMm_WorkflowComment[]
	 */
	public function comments()
	{
		if($this->comments == null)
		{
			$this->comments = [];
			$query = "SELECT * FROM `workflow_model_comments` WHERE workflow_model_match_id = :workflow_model_match_id ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('workflow_model_match_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$workflow_comment = TMm_WorkflowComment::init($row);
				if($workflow_comment)
				{
					$this->comments[] = $workflow_comment;
				}


			}
		}

		return $this->comments;
	}


	/**
	 * @return TMm_WorkflowStageModelHistory[]
	 */
	public function stageHistory()
	{
		if($this->stages == null)
		{
			$this->stages = [];
			$query = "SELECT * FROM `workflow_model_stages` WHERE workflow_model_match_id = :workflow_model_match_id ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('workflow_model_match_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$stage = TMm_WorkflowStageModelHistory::init($row);
				if($stage)
				{
					$this->stages[] = $stage;

				}
			}

			if (empty($this->stages))
			{
				$this->stages[] = $this->createFirstStage();
			}
		}

		return $this->stages;
	}

	/**
	 * Creates the first item for the given workflow .
	 *
	 * @param TMm_WorkflowModelMatch $workflow
	 * @return TMm_WorkflowStageModelHistory|bool
	 */
	public function createFirstStage()
	{
		// Generate the first stage if missing
		$stage = $this->workflow()->firstStage();

		// Create the first stage
		$values = array();
		$values['stage_id'] = $stage->id();
		$values['workflow_model_match_id'] = $this->id();
		$values['user_id'] = TC_currentUser()->id();

		$model_stage = TMm_WorkflowStageModelHistory::createWithValues($values);

		return $model_stage;
	}

	/**
	 * Returns the current stage that this model match is at.
	 * @return TMm_WorkflowStage|false
	 */
	public function currentStage()
	{
		$history_stage = $this->currentStageHistory();
		if($history_stage instanceof TMm_WorkflowStageModelHistory)
		{
			return $history_stage->stage();
		}
		return false;
	}
	
	/**
	 * Returns the current stage history for this model match
	 * @return TMm_WorkflowStageModelHistory|false
	 */
	public function currentStageHistory()
	{
		$history_stages = array_values($this->stageHistory());
		if(count($history_stages) > 0)
		{
			return $history_stages[0];
		}
		return false;
	}
	
	/**
	 * Returns the next stage in the order
	 * @return TMm_WorkflowStage|bool
	 */
	public function nextStage()
	{
		// Get all the stages for the workflow
		$stages = $this->workflow()->stages();
		$current_stage_found = false;
		$current_stage_id = $this->currentStage()->id();
		
		
		// Loop through the stages and find the current stage, then return the NEXT stage in the list
		foreach ($stages as $stage)
		{
			// The value is set, so the previous iteration found a match
			if ($current_stage_found)
			{
				return $stage;
			}
			
			// Check if this is the current stage, set the flag
			if ($stage->id() == $current_stage_id)
			{
				$current_stage_found = true;
			}
		}
		
		// Not found, so we're in the last stage, return false
		return false;
	}

	/**
	 * Returns the previous stage in the order
	 * @return TMm_WorkflowStage|bool
	 */
	public function previousStage()
	{
		// Get all the stages for the workflow
		$stages = $this->workflow()->stages();
		$current_stage_id = $this->currentStage()->id();

		$previous_stage = false;


		// Loop through the stages and find the current stage, then return the previous stage
		foreach ($stages as $stage)
		{

			// Check if this is the current stage, if so return the previous stage
			if ($stage->id() == $current_stage_id)
			{
				return $previous_stage;
			}

			$previous_stage = $stage;
		}

		// Not found, so we're in the last stage, return false
		return false;
	}

	/**
	 * Moves this model workflow on to the next stage
	 * @return bool|TMm_WorkflowStageModelHistory
	 */
	public function moveToNextStage()
	{
		return $this->moveToStage($this->id(), $this->nextStage()->id());
	}

	/**
	 * Moves this model workflow back to the previous stage
	 * @return bool|TMm_WorkflowStageModelHistory
	 */
	public function moveToPreviousStage()
	{
		return $this->moveToStage($this->id(), $this->previousStage()->id());
	}

	/**
	 * Move this item to a new stage, which means creating a new item with similar properties.
	 * @param int $this_id Not used.
	 * @param int $new_stage_id The new stage id
	 * @return bool|TMm_WorkflowStageModelHistory The new history stage ID that was created
	 */
	public function moveToStage($this_id, $new_stage_id)
	{
		$current_stage = $this->currentStageHistory();

		// Create a new stage with the same values but
		$values = array();
		$values['stage_id'] = $new_stage_id;
		$values['user_id'] = TC_currentUser()->id();
		$values['workflow_model_match_id'] = $this->id();
		$new_stage_history = TMm_WorkflowStageModelHistory::createWithValues($values);

		$current_stage->updateDatabaseValue('is_current_stage', 0);

		return $new_stage_history;

	}


	//////////////////////////////////////////////////////
	//
	// USERS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns all the users assigned to this item for a given workflow
	 * @return TMm_User[]
	 */
//	public function workflowAssignedUsers(TMm_Workflow $workflow)
	public function assignedUsers()
	{
		if ($this->users === false)
		{
			$this->users = [];

			$query = "SELECT * FROM `workflow_model_users`
				WHERE workflow_model_match_id = :workflow_model_match_id";
			$result = $this->DB_Prep_Exec($query, ['workflow_model_match_id' => $this->id()]);

			while($row = $result->fetch())
			{
				$this->users[$row['user_id']] = TMm_User::init($row['user_id']);// TMm_WorkflowAssignedUser::init($row);
			}
		}

		return $this->users;
	}

	/**
	 * Returns if the provided user is assigned to the workflow
	 * @param TMm_Workflow $workflow
	 * @return bool
	 */
	public function userIsAssigned($user)
	{
		$users = $this->assignedUsers();
		return isset($users[$user->id()]);
	}

	/**
	 * Adds a user from the item for a specific workflow
	 * @param TMm_Workflow $workflow
	 * @param TMm_User $user
	 */
	public function addUser($user)
	{
		$values = array();

		$values['workflow_model_match_id'] = $this->id();
		$values['user_id'] = $user->id();
		$this->users[$user->id()] = $user;
		TMm_WorkflowAssignedUser::createWithValues($values);
	}

	/**
	 * Removes a user from the item for a specific workflow
	 * @param TMm_User $user
	 */
	public function removeUser($user)
	{
		$query = "DELETE FROM workflow_model_users
				WHERE user_id = :user_id AND
				      workflow_model_match_id = :workflow_model_match_id";
		$this->DB_Prep_Exec($query, [
			'user_id' => $user->id(),
			'workflow_model_match_id' => $this->id(),
		]);
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the page menu item to add additional fields
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				'model_name' => [
					'title'         => 'Model Name',
					'comment'       => 'The class name of the model in this match',
					'type'          => 'varchar(25)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'model_id' => [
					'title'         => 'Model ID',
					'comment'       => 'The model ID for this match',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'workflow_id' => [
					'title'         => 'Workflow ID',
					'comment'       => 'The Workflow ID for this match',
					'type'          => 'TMm_Workflow',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_Workflow',
						'delete'        => 'CASCADE'
					],
				],
				'is_enabled' => [
					'title'         => 'Workflow ID',
					'comment'       => 'The Workflow ID for this match',
					'type'          => 'tinyint(1) unsigned DEFAULT 1',
					'nullable'      => false,
				],
		];
	}
}