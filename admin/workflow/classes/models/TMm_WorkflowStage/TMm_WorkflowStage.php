<?php

/**
 * Class TMm_WorkflowStage
 *
 * These are the ordered stages in the system that can be used with a Workflow to show progress
 *
 * The stages exist even if they are not connected to a Workflow
 *
 * If you're looking for the model that represents the current status of single model when it was at a given stage
 * for a given workflow:
 * @see TMm_WorkflowStageModelHistory
 */
class TMm_WorkflowStage extends TCm_Model
{
	protected int $stage_id;
	
	protected $title, $color, $display_order, $readable_color, $is_done;
	
	protected $color_light, $color_dark;
	protected $user_groups = false;
	protected $items_in_stage = false;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'stage_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_stages'; // [string] = The table for this class
	public static $model_title = 'Stage';
	public static $primary_table_sort = 'display_order ASC';
	
	public function __construct($id)
	{
		parent::__construct($id);
	}


	/**
	 * Returns the title for this stage
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}

	/**
	 * Display order
	 * @return mixed
	 */
	public function displayOrder()
	{
		return $this->display_order;
	}

	//////////////////////////////////////////////////////
	//
	// USERS
	//
	// Regarding the users who have access to this
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the list of user groups that are associated with this stage
	 * @return TMm_UserGroup[]
	 */
	public function userGroups()
	{
		if($this->user_groups === false)
		{
			$this->user_groups = array();

			// CHECK FOR USER GROUP MATCHES
			if($this->id() > 0)
			{
				$query = "SELECT * FROM `workflow_stage_user_group_matches` m INNER JOIN user_groups g USING(group_id) WHERE m.stage_id = :stage_id";
				$result = $this->DB_Prep_Exec($query, array('stage_id' => $this->id()));
				while($row = $result->fetch())
				{
					$group = TMm_UserGroup::init($row);
					$this->user_groups[$group->id()] = $group;
				}
			}

		}

		return $this->user_groups;
	}

	/**
	 * Returns if this user has permission to move this stage
	 * @param bool|TMm_User $user
	 * @return bool
	 */
	public function userCanMoveStage($user = false)
	{
		if($user === false)
		{
			$user = TC_currentUser();
		}

		// Admins can always move a stage
		if($user->isAdmin())
		{
			return true;
		}

		foreach (TMm_WorkflowModule::init()->userGroups() as $user_group)
		{
			if($user->inGroup($user_group))
			{
				return true;
			}
		}

		return false;
	}

//	/**
//	 * Updates a single group value to indicate if the user is a part of this group or not. This function co-incides
//	 * with the required function for the TCv_FormItem_CheckboxList class.
//	 * @param TMm_UserGroup $user_group
//	 * @param bool $is_member
//	 */
//	public function updateGroupSetting($user_group, $is_member)
//	{
//		$current_member = $this->groupHasAccess($user_group);
//
//		// Current Member that shouldn't be
//		if($current_member && !$is_member)
//		{
//			$query = "DELETE FROM workflow_stage_user_group_matches WHERE stage_id = :stage_id AND group_id = :group_id";
//			$result = $this->DB_Prep_Exec($query, array('group_id'=> $user_group->id(),':stage_id' => $this->id()));
//			unset($this->user_groups[$user_group->id()]);
//		}
//
//		// NOT A MEMBER
//		elseif(!$current_member && $is_member)
//		{
//			$query = "INSERT INTO workflow_stage_user_group_matches SET stage_id = :stage_id , group_id = :group_id, date_added = now()";
//			$result = $this->DB_Prep_Exec($query, array('group_id'=> $user_group->id(),'stage_id' => $this->id()));
//			$this->user_groups[$user_group->id()] = $user_group;
//
//		}
//	}

//	/**
//	 * Updates the list of user groups based on an associative array of values
//	 * @param $values
//	 */
//	public function updateUserGroups($values)
//	{
//		$user_group_list = TMm_UserGroupList::init();
//		foreach($user_group_list->groups() as $user_group)
//		{
//			$in_group = in_array($user_group->id(), $values);
//			$this->updateGroupSetting($user_group, $in_group);
//		}
//	}

	/**
	 * Returns if the provided user group is associated with this stage
	 * @param TMm_UserGroup $user_group
	 * @return bool
	 */
	public function groupHasAccess($user_group)
	{
		$this->userGroups();
		return isset($this->user_groups[$user_group->id()]);
	}

	/**
	 * @return bool
	 */
	public function isDoneStage()
	{
		return $this->is_done;
	}

	/**
	 * @return bool
	 */
	public function isStartStage()
	{
		return $this->id() == 1;
	}

	//////////////////////////////////////////////////////
	//
	// MODELS IN STAGES
	//
	// Functions related to the models that belong in this stage.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns all models that have this set as current stage
	 */
	public function modelsCurrentlyInStage()
	{
		if($this->items_in_stage == null)
		{
			$this->items_in_stage = [];
			$query = "SELECT * FROM `workflow_model_stages` WHERE stage_id = :stage_id AND is_current_stage = 1";
			$result = $this->DB_Prep_Exec($query, array('stage_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$match = TMm_WorkflowModelMatch::init($row['workflow_model_match_id']);
				$this->items_in_stage[] = $match->model();
			}
		}

		return $this->items_in_stage;
	}




	//////////////////////////////////////////////////////
	//
	// COLORS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the color hex code for this stage
	 * @return string
	 */
	public function color()
	{
		return TMm_Workflow::getColorCode($this->color);
	}
	
	/**
	 * Returns the color hex code for this stage
	 * @return string
	 */
	public function colorName()
	{
		return TMm_Workflow::getColorName($this->color);
	}
	
	
	/**
	 * Returns the readable color based on the overall contrast of the main color chosen
	 * @return string
	 */
	public function readableColor()
	{
		return TMm_Workflow::getColorTextShade($this->color);
	}
	
	function shadeColor2($color, $percent)
	{
		$color = str_replace("#", "", $color);
		$t = $percent < 0 ? 0 : 255;
		$p = $percent < 0 ? $percent * -1 : $percent;
		$RGB = str_split($color, 2);
		$R = hexdec($RGB[0]);
		$G = hexdec($RGB[1]);
		$B = hexdec($RGB[2]);
		return '#' . substr(dechex(0x1000000 + (round(($t - $R) * $p) + $R) * 0x10000 + (round(($t - $G) * $p) + $G) * 0x100 + (round(($t - $B) * $p) + $B)), 1);
	}
	
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorLight()
	{
		if($this->color_light == null)
		{
			$this->color_light = $this->shadeHexColor($this->color,-93);
		}

		return $this->color_light;
	}
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorDark()
	{
		if ($this->color_dark == null)
		{
			$this->color_dark = $this->shadeHexColor($this->color, 75);
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema() + [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title of the stage',
					'type'          => ' varchar(64)',
					'nullable'      => false,
				],
				'color' => [
					'title'         => 'Hex Color',
					'comment'       => 'The 6-character hex color for the stage',
					'type'          => 'varchar(24)',
					'nullable'      => false,
//					'validations'   => [
//						'pattern'   => '/^[0-9A-F]{6}$/i'
//					],
				],

				'display_order' => [
					'title'         => 'Display order',
					'comment'       => 'The order of this item, in relation to items with the same parent',
					'type'          => 'smallint(5) unsigned',
					'nullable'      => false,
				],

				'is_done' => [
					'title'         => 'Done',
					'comment'       => 'Indicates if this flags  workflow as Done ',
					'type'          => 'tinyint(1) unsigned DEFAULT 0',
					'nullable'      => false,
				],
				'workflow_id' => [
					'delete'        => true,
				],
				'description' => [
					'delete'        => true,
				],
			
			
			
			
			];
	}
}
?>