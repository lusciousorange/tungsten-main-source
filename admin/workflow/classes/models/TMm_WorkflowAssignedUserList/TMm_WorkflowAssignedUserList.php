<?php

/**
 * Class TMm_WorkflowAssignedUserList
 */
class TMm_WorkflowAssignedUserList extends TCm_ModelList
{
	/**
	 * TMm_UserGroupList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_WorkflowAssignedUser',false);
	}


	/**
	 * @param $model_name
	 * @return TMm_WorkflowAssignedUser[] An array of stages with the indices being the IDs of the models that were asked
	 * for. If there are N models with that model name, then it will return rows.
	 */
	public function currentUsersForModelName($model_name)
	{
		$query = "SELECT * FROM `workflow_model_users` WHERE model_name = :model_name ORDER BY date_added ASC
		";

		$values = array();
		$result = $this->DB_Prep_Exec($query, array('model_name' => $model_name));
		while($row = $result->fetch() )
		{
			$values[$row['model_id']][$row['user_id']] = TMm_WorkflowAssignedUser::init($row);
		}

		return $values;

	}
		
}

?>