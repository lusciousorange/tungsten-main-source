<?php

/**
 * Class TMm_WorkflowComment
 *
 * This represents a single comment in the system. Comments are always created by a user in the system (`user_id`)
 * but then it's associated with a particular workflow for a particular model. There can be multiple workflows for a
 * model, so they have entirely separate comment sections.
 *
 * The content for the comment is fairly basic HTML.
 *
 * Comments can also be "resolved", which means whatever was mentioned is dealt with. This helps track if things are
 * still unfinished. Without this option, comments would build up but nothing would get done about them.
 *
 * Comments can be associated with a particular content_id. This is baked into the Page Builder and allows people to
 * point ot a specific content item for their comment, saving some confusion on complex pages.
 */
class TMm_WorkflowComment extends TCm_Model
{
	protected int $comment_id;
	protected $comment, $is_resolved, $content_id, $workflow_model_match_id, $user_id;

	protected string $model_name;
	protected ?int $model_id = null;
	protected ?int $workflow_id = null;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'comment_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_comments'; // [string] = The table for this class
	public static $model_title = 'Comment';
	public static $primary_table_sort = 'date_added DESC';

	public function __construct($id)
	{
		parent::__construct($id);
	}


	public function content()
	{
		return $this->comment;
	}

	public function comment()
	{
		return $this->comment;
	}

	public function user()
	{
		return TMm_User::init( $this->user_id);
	}

	public function toggleResolved()
	{
		$this->updateWithValues(array('is_resolved' => !$this->is_resolved));
		$this->is_resolved = !$this->is_resolved;
	}

	public function isResolved()
	{
		return $this->is_resolved;
	}

	public function workflowModelMatch()
	{
		return TMm_WorkflowModelMatch::init($this->workflow_model_match_id);
	}

	/**
	 * Returns the workflow for this item
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return $this->workflowModelMatch()->workflow();
	}

	/**
	 * Returns if this item is linked to a content item
	 * @return bool
	 */
	public function isLinked()
	{
		return $this->content_id > 0;
	}

	public function linkTargetID()
	{
		if($this->content_id > 0)
		{
			return $this->content_id;
		}
	}


	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model
	 * @return bool|TCm_Model
	 */
	public static function createWithValues ($values, $bind_param_values = array(), $return_new_model = true)
	{
		$model = new TCm_Model(false);

		if(trim($values['comment']) == '')
		{
			return false;
		}

		/** @var TMm_WorkflowComment $new_comment */
		$new_comment = parent::createWithValues($values, $bind_param_values, $return_new_model);

//		// Loop through and find out if user wants to receive the email
//		foreach($new_comment->people() as $user)
//		{
//			if($user->valueForProperty('workflow_email_comments'))
//			{
//				$email = new TMv_WorkflowPeopleEmail($new_comment, $user);
//				$email->send();
//			}
//
//		}
//
//		// HANDLE SLACK CALLS
//
//		// If Slack is installed
//		if(
//			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
//			class_exists('TMm_SlackClient') && // We also need the proper class
//			TC_getModuleConfig('workflow','slack_for_comments') // Respect system pref for Slack and Workflow
//		)
//		{
//
//			$slack_client = TMm_SlackClient::init();
//
//			$model_title = $new_comment->model()->title();
//			$comment_text = $new_comment->comment();
//
//
//			$title_text = $slack_client->cleanTextForSlack(TC_currentUser()->fullName()). " added a comment to " . $new_comment->typeTitle() ." : <".
//				$slack_client->fullDomainName().$new_comment->model()->adminEditURL()
//				."|".$slack_client->cleanTextForSlack($model_title).'>';
//
//			$slack_client->addText($title_text);
//
//			// Find list of people
//			$people_names = array();
//			foreach($new_comment->people() as $person)
//			{
//				$people_names[] = $slack_client->slackCodeForUser($person);
//			}
//
//			$attachment = array(
//				"fallback" => "",
//				"color" => '#640064', // Workflow Purple
//				"fields" => array(
//					array(
//						"title" => "Comment",
//						"value" => $slack_client->cleanTextForSlack($comment_text)
//					),
//					array(
//						"title" => "People",
//						"value" => implode(', ',$people_names)
//					)
//
//				)
//			);
//			$slack_client->addAttachment($attachment);
//			$slack_client->sendIncomingWebhook();
//
//
//		}
		return $new_comment;
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the page menu item to add additional fields
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				'comment' => [
					'title'         => 'Comment',
					'comment'       => 'The contents of the comment made',
					'type'          => 'text',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The user who submitted the comment',
					'type'          => 'TMm_User',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'CASCADE'
					],
				],
				'model_name' => [
					'title'         => 'Model Name',
					'type'          => 'varchar(128)',
					'nullable'      => true,
				
				],
				'model_id' => [
					'title'         => 'Model ID',
					'type'          => 'int(10) unsigned',
					'nullable'      => true,
					
				],
				'content_id' => [
					'title'         => 'Content ID',
					'comment'       => 'The content item related to this comment',
					'type'          => 'TMm_PagesContent',
					'nullable'      => false,
					'validations'   => [
						'required'      => false, // Explicitly must be set to false or will try to validate the ID
					],
				
				],
				'workflow_id' => [
					'title'         => 'Workflow ID',
					'comment'       => 'The Workflow  ID for this match',
					'type'          => 'TMm_Workflow',
					'nullable'      => true,
					'foreign_key'   => [
						'model_name'    => 'TMm_Workflow',
						'delete'        => 'SET NULL'
					],
				],
				'workflow_model_match_id' => [
					'title'         => 'Workflow Model Match ID',
					'comment'       => 'The Workflow model ID for this match',
					'type'          => 'TMm_WorkflowModelMatch',
					'nullable'      => true,
					'foreign_key'   => [
						'model_name'    => 'TMm_WorkflowModelMatch',
						'delete'        => 'SET NULL'
					],
				],
				'is_resolved' => [
					'title'         => 'Is Resolved',
					'comment'       => 'Whether or not this comment was resolved',
					'type'          => 'tinyint(1) unsigned DEFAULT 0',
					'nullable'      => false,
				],
			];
	}
}
?>