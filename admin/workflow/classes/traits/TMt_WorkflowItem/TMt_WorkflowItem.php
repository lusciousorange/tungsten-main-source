<?php

/**
 * Trait TMt_WorkflowItem
 *
 * A trait that must be added to each model which can have the workflow applied to it.
 *
 */
trait TMt_WorkflowItem
{
	//protected $workflow_module
	//public function init
	protected ?array $workflow_comments = null;
	protected ? array $workflow_model_matches = null;
	protected ?TMm_WorkflowModule $workflow_module = null;

	public static $all_workflow_model_matches = null;
	public static $workflow_model_matches_by_category = null;
	public static $workflow_model_matches_without_category = null;

	//////////////////////////////////////////////////////
	//
	// WORKFLOWS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the workflow module
	 * @return bool|TMm_WorkflowModule
	 */
	public function workflowModule()
	{
		// Lazy loaded
		if($this->workflow_module == null)
		{
			$this->workflow_module = TMm_WorkflowModule::init();
		}
		
		return $this->workflow_module;
	}

	public function createWorkflowMatch($workflow)
	{
		$match = false;
		$values = array();

		if (is_numeric($workflow))
		{
			$workflow = TMm_Workflow::init($workflow);
		}

		if ($workflow instanceof TMm_Workflow)
		{
			if ($this->usesWorkflow($workflow))
			{
				$match = $this->workflowMatch($workflow);
				$match->enable();
			}
			else
			{
				$this->addConsoleDebug('No match exists. Creating Match');
				$values['workflow_id'] = $workflow->id();
				$values['model_id'] = $this->id();
				$values['model_name'] = $this->baseClassName();

				$match = TMm_WorkflowModelMatch::createWithValues($values);

			}
		}

		return $match;
	}
	
	
	/**
	 * Finds all the workflow model matches for this workflow item. This is commonly used to reduce DB calls in lists
	 * of items and instead acquire them all and reference it if it exists.
	 *
	 * @return array[] Returns a 2D array first indexed by model_id, then with an array of `TMm_WorkflowModelMatch`
	 * items for that model_id.
	 */
	public static function allWorkflowModelMatches()
	{
		if(static::$all_workflow_model_matches == null)
		{
			static::$all_workflow_model_matches = [];
			
			// Get ALL the matches for this model, regardless of ID
			$model = TCm_Model::init(0);
			$query = "SELECT * FROM `workflow_model_matches` WHERE model_name = :model_name";
			$result = $model->DB_Prep_Exec($query, ["model_name" => static::baseClass()]);
			while($row = $result->fetch())
			{
				// If we haven't set the value yet, init the array
				if(!isset(static::$all_workflow_model_matches[$row['model_id']]))
				{
					static::$all_workflow_model_matches[$row['model_id']] = [];
				}
				
				// Save the match, indexed by model_id
				static::$all_workflow_model_matches[$row['model_id']][] = TMm_WorkflowModelMatch::init($row);
			}
		}
		
		return static::$all_workflow_model_matches;
	}

	/**
	 * Returns an array of TMm_WorkflowModelMatch, indexed by workflow category with all
	 * matches for this class
	 *
	 * @return array[]
	 */
	public static function workflowModelMatchesByCategory()
	{
		if(static::$workflow_model_matches_by_category == null)
		{
			$all_matches = static::allWorkflowModelMatches();
			$categories = TMm_Workflow::$workflow_categories;

			// Create initial array sorted by categories
			static::$workflow_model_matches_by_category = [];
			foreach ($categories as $key => $value)
			{
				static::$workflow_model_matches_by_category[$key] =[];
			}

			// Divide all matches into categories array
			foreach ($all_matches as $matches)
			{
				foreach ($matches as $match)
				{

					if (!empty($match->workflow()->category()))
					{
						/** @var TMm_WorkflowModelMatch $match */
						static::$workflow_model_matches_by_category[$match->workflow()->category()][] = $match;
					}
					else
					{
						static::$workflow_model_matches_without_category[$match->workflow()->id()][] = $match;
					}
				}
			}
		}

		return static::$workflow_model_matches_by_category;
	}

	/**
	 * Returns an array of matches without a category sorted by workflow ID
	 * @return array[]|null
	 */
	public static function workflowModelMatchesWithoutCategory()
	{
		static::workflowModelMatchesByCategory();

		return static::$workflow_model_matches_without_category;
	}

	/**
	 * Returns percentages of stages for a workflow
	 * @param $workflow_id
	 * @return array
	 */
	public static function stagePercentageForWorkflow($workflow_id)
	{
		$percentages = [];
		$total_matches = 0;
		$matches_by_workflow = static::workflowModelMatchesWithoutCategory();

		// Start count array with stage keys
		$stage_counts = [];
		foreach (TMm_WorkflowStageList::init()->models() as $stage)
		{
			$stage_counts[$stage->id()] = 0;
		}

		// Go through each match and add to counts
		foreach ($matches_by_workflow[$workflow_id] as $match)
		{
			/** @var TMm_WorkflowModelMatch $match */
			$stage_counts[$match->currentStage()->id()] ++;
			$total_matches++;
		}

		if ($total_matches > 0)
		{
			// Go through counts and calculate percentages
			foreach ($stage_counts as $id => $count)
			{
				$num = ($count / $total_matches) * 100;
				$percentages[$id] = round($num,2);
			}
		}

		return $percentages;
	}



	/**
	 * Returns the percentages of each stage for all models that belong to a certain category
	 * @param $category
	 * @return array
	 */
	public static function percentagePerStageForCategory($category)
	{
		$matches_by_category = static::workflowModelMatchesByCategory();

		$percentages = [];
		$total_matches = 0;

		// Start count array with stage keys
		$stage_counts = [];
		foreach (TMm_WorkflowStageList::init()->models() as $stage)
		{
			$stage_counts[$stage->id()] = 0;
		}

		// Go through each match and add to counts
		foreach ($matches_by_category[$category] as $match)
		{
			/** @var TMm_WorkflowModelMatch $match */
			$stage_counts[$match->currentStage()->id()] ++;
			$total_matches++;
		}

		if ($total_matches > 0)
		{
			// Go through counts and calculate percentages
			foreach ($stage_counts as $id => $count)
			{
				$num = ($count / $total_matches) * 100;
				$percentages[$id] = round($num,2);
			}
		}

		return $percentages;
	}
	
	/**
	 * Sets the workflow model matches for this item. This is commonly used as part of the list loading to avoid
	 * multiple calls.
	 * @param TMm_WorkflowModelMatch[] $matches
	 */
	public function setWorkflowModelMatches($matches)
	{
		$this->workflow_model_matches = $matches;
	}
	
	/**
	 * Grabs all current Workflow matches for this model
	 *
	 * @author Katie Overwater
	 *
	 * @return TMm_WorkflowModelMatch[]
	 */
	public function workflowModelMatches()
	{
		if($this->workflow_model_matches == null)
		{
			// Detect if we've loaded ALL the matches via the static method. If that value exists, we don't need to
			// call the DB, we just need to get those values
			if(isset(static::$all_workflow_model_matches))
			{
				if(isset(static::$all_workflow_model_matches[$this->id()]))
				{
					$this->workflow_model_matches = static::$all_workflow_model_matches[$this->id()];
				}
				else
				{
					$this->workflow_model_matches = [];
				}
				
			}
			else // Static not set, likely viewing an individual item, either way, go get the values for just this model
			{
				$this->workflow_model_matches = array();
				
				$query = "SELECT * FROM `workflow_model_matches` WHERE model_id = :model_id AND model_name = :model_name";
				$result = $this->DB_Prep_Exec($query, array("model_id" => $this->id(), "model_name" => $this->baseClassName()));
				while($row = $result->fetch())
				{
					$this->workflow_model_matches[] = TMm_WorkflowModelMatch::init( $row);
				}
			}
			
		}
		return $this->workflow_model_matches;
	}

	/**
	 * Checks if this particular model uses this workflow.
	 * @author Katie Overwater
	 *
	 * @return bool
	 */
	public function usesWorkflow(TMm_Workflow $workflow)
	{

		foreach ($this->workflowModelMatches() as $match)
		{
			if ($workflow->id() == $match->workflowID())
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns the specific match for a workflow
	 *
	 * @author Katie Overwater
	 *
	 * @param TMm_Workflow $workflow
	 * @return bool|TMm_WorkflowModelMatch
	 */
	public function workflowMatch(TMm_Workflow $workflow)
	{
		if ($this->usesWorkflow($workflow))
		{
			$workflow_matches = $this->workflowModelMatches();
			foreach ($workflow_matches as $match)
			{
				if ($match->workflowID() == $workflow->id())
				{
					return $match;
				}
			}
		}

		return false;
	}
	
	
	/**
	 * The array of workflows that are applied to this item by default.
	 * @return TMm_Workflow[]
	 *
	 * @author Katie Overwater
	 */
	public function defaultWorkflows()
	{
		$workflows = array();
		$query = "SELECT * FROM `workflow_model_defaults` WHERE model_name = :model_name ";
		$result = $this->DB_Prep_Exec($query, array('model_name' => $this->baseClassName()));
		while($row = $result->fetch())
		{
			$workflows[] = TMm_Workflow::init($row['workflow_id']);
		}

		return $workflows;
	}
	
	/**
	 * Generates the default workflow matches for this model.
	 */
	public function generateDefaultWorkflows()
	{
		foreach($this->defaultWorkflows() as $workflow)
		{
			$values = array(
				'workflow_id' => $workflow->id(),
				'model_name' => $this->baseClassName(),
				'model_id' => $this->id()
			);
			
			TMm_WorkflowModelMatch::createWithValues($values);
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// STAGES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of stages that comprise the history for this model for a workflow
	 * @param TMm_WorkflowModelMatch $workflow
	 * @return TMm_WorkflowStageModelHistory[]
	 */
//	public function workflowStageHistory($workflow)
//	{
//		return $workflow->stageHistory();
//	}
//
//	/**
//	 * Returns the current stage history item for a given workflow
//	 * @param TMm_WorkflowModelMatch $workflow
//	 * @return TMm_WorkflowStageModelHistory
//	 */
//	public function workflowCurrentStageHistory(TMm_Workflow $workflow)
//	{
//		// Return the first item in the list of the history
//		return $this->workflowStageHistory($workflow)[0];
//	}
//
//	/**
//	 * Returns the current stage for a given workflow
//	 * @param TMm_WorkflowModelMatch $workflow
//	 * @return TMm_WorkflowStage
//	 */
//	public function workflowCurrentStage($workflow)
//	{
//		return $this->workflowCurrentStageHistory($workflow)->stage();
//	}


	//////////////////////////////////////////////////////
	//
	// COMMENTS
	//
	//////////////////////////////////////////////////////
	/**
	 * Returns all the comments for this model
	 *
	 * @return TMm_WorkflowModelMatch[]
	 */
	public function workflowComments() : array
	{
		if(is_null($this->workflow_comments))
		{
			$this->workflow_comments = [];
			
			$query = "SELECT * FROM `workflow_model_comments` WHERE model_id = :model_id AND model_name = :model_name ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array("model_id" => $this->id(), "model_name" => $this->baseClassName()));
			while($row = $result->fetch())
			{
				$this->workflow_comments[] = TMm_WorkflowComment::init( $row);
			}
			
			
		}
		return $this->workflow_comments;
	}
	
	
}