<?php

/**
 * Class TMv_WorkflowProgressSummary
 */
class TMv_WorkflowProgressSummary extends TCv_View
{
	use TMt_DashboardView;

	/**
	 * TMv_WorkflowControlBar constructor.
	 * @param bool $id
	 */
	public function __construct ($id = false)
	{
		parent::__construct($id);

		$this->addClassCSSFile();

	}

	public function html ()
	{
		///** @var TMm_WorkflowStageList $stage_list */
		//$stage_list = TMm_WorkflowStageList::init();
		
		$workflow = TMm_WorkflowModule::workflows();
//
//
//		$workflow_models = explode(',', TC_getModuleConfig('workflow', 'model_class_names'));
//		foreach($workflow_models as $model_name)
//		{
//			$model_name = trim($model_name);
//			$module = $model_name::moduleForClassName();
//			$folder = $module->folder();
//
//
//			$container = new TCv_View();
//			$container->addClass('model_block');
//
//			$stage_values = array();
//
//			$total = 0;
//
//			foreach($stage_list->models() as $stage)
//			{
//				//$this->addText('<br />'.$stage->title());
//				$num_models_in_stage = $stage->numItemsWithModelName($model_name);
//				$total += $num_models_in_stage;
//
//				//$this->addText(' - '.$num_models_in_stage);
//
//				$stage_values[] = array('stage' => $stage, 'quantity' => $num_models_in_stage);
//
//			}
//
//			$heading = new TCv_View();
//			$heading->setTag('h3');
//				$total_view = new TCv_View();
//				$total_view->addClass('total_view');
//				$total_view->addText($total.' Items');
//				$heading->attachView($total_view);
//
//			$heading->addText($model_name::modelTitlePlural());
//			$container->attachView($heading);
//
//			$container->attachView($this->progressBarForStageNumbers($stage_values, $total, $model_name));
//
//			// Assigned to You
//
//			$num_assigned = TMm_WorkflowAssignedUser::numModelsWithNameForUser($model_name, TC_currentUser());
//
//			$view = new TCv_Link();
//			$view->setIconClassName('fa-user');
//			$view->addText($num_assigned.' Item'.($num_assigned == 1 ? '': 's' ).' assigned to you');
//			$view->addClass('progress_link');
//
//			if($folder == 'pages')
//			{
//				$url = '/admin/pages/do/manage/';
//			}
//			else
//			{
//				$url = '/admin/'.$folder.'/do/list/?workflow_user_id=' .TC_currentUser()->id();
//
//			}
//			$view->setURL($url);
//
//
//
//			$container->attachView($view);
//			$this->attachView($container);
//		}


		return parent::html();
	}


	/**
	 * Returns a progress bar for the entire model
	 * @param array $stage_values
	 * @param int $total
	 * @param string $model_name
	 * @return TCv_View
	 */
	private function progressBarForStageNumbers($stage_values, $total, $model_name)
	{
		$view = new TCv_View();
		$view->addClass('summary_progress_bar');

		if($total == 0)
		{
			return $view;
		}

		// CREATE BARS
		foreach($stage_values as $values)
		{
			$percent = round($values['quantity'] / $total*100,1);

			if($percent > 0)
			{

				/** @var TMm_WorkflowStage $stage */
				$stage = $values['stage'];

				$bar = new TCv_Link();
				$bar->setTitle("");
				$bar->addClass('progress_bar_portion');
				$module = $model_name::moduleForClassName();
				$folder = $module->folder();

				if($folder == 'pages')
				{
					$url = '/admin/pages/do/manage/';
				}
				else
				{
					$url = '/admin/'.$folder.'/do/list/?workflow_stage_id=' .$stage->id();

				}
				$bar->setURL($url);
				//$bar->addText($percent.'%');
				$bar->addText($values['quantity']);
				$bar->setAttribute('style', 'background:#' . $stage->color() . '; width: ' . $percent . '%;');

				$hover = new TCv_View();
				$hover->addClass('hover_box');
				$hover->addText($stage->title());
				//$hover->addText('<br />'.$values['quantity'].' / '.$total);
				$hover->addText('<br />' . $percent . '%');
				$bar->attachView($hover);
				$view->attachView($bar);
			}
		}

		return $view;
	}

	/**
	 * Returns the default title for this dashboard
	 * @return string
	 */
	public static function dashboard_DefaultTitle() : string
	{
		return 'Workflow progress';
	}

	public static function pageContent_ViewTitle() : string
	{
		return 'Workflow progress';
	}
}