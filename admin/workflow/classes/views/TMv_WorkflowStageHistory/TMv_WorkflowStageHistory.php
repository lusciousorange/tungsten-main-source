<?php

/**
 * Class TMv_WorkflowStageHistory
 */
class TMv_WorkflowStageHistory extends TMv_WorkflowHistoryItem
{
	protected $model_stage = false;
	/**
	 * TMv_WorkflowStageHistory constructor.
	 * @param TMm_WorkflowStageModelHistory $model_stage
	 */
	public function __construct ($model_stage)
	{

		parent::__construct($model_stage);

		$this->model_stage = $model_stage;

		$this->addClassCSSFile('TMv_WorkflowStageHistory');

	}

	public function html ()
	{
//		$user = $this->model_stage->user();
		$stage = $this->model_stage->stage();

		$button = new TCv_View();
		$button->setTag('span');
		$button->addClass('workflow_stage_color');
		$button->setAttribute('style','background:#'.$stage->color().';color:#'
		                             .$this->model_stage->stage()->readableColor().';');
		$button->addText($this->model_stage->stage()->title());
		$this->attachView($button);

		$this->setAttribute('style','border-color:#'.$stage->color().';');


		$stage_box = new TCv_View();
		$stage_box->addClass('history_user');
		$stage_box->addText($this->model_stage->user()->fullName());
		$this->attachView($stage_box);

		$stage_box = new TCv_View();
		$stage_box->addClass('history_date');
		$stage_box->addText($this->model_stage ->dateAddedFormatted('F j, Y \a\t g:ia'));
		$this->attachView($stage_box);



		return parent::html();
	}

}