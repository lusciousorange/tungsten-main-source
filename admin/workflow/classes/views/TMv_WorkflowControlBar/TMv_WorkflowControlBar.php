<?php

/**
 * Class TMv_WorkflowControlBar
 */
class TMv_WorkflowControlBar extends TCv_View
{
	protected bool $load_js_and_css = true;
	protected bool $allow_sliding = true;
	
	/**
	 * TMv_WorkflowControlBar constructor.
	 * @param bool $id
	 */
	public function __construct ($id = false)
	{
		parent::__construct($id);

	}
	
	/**
	 * Disables the loading of JS or CSS, allowing the site to do it's own. used in Tungsten 9 to undo all the
	 * styling and custom stuff for panels.
	 * @return void
	 */
	public function disableJSandCSS() : void
	{
		$this->load_js_and_css = false;
	}
	
	/**
	 * Disables sliding
	 * @return void
	 */
	public function disableSliding() : void
	{
		$this->allow_sliding = false;
	}

	public function render ()
	{
		if(!TC_getConfig('use_workflow'))
		{
			return '';
		}
		
		$module = TMm_WorkflowModule::init();
		
		$params = array();
		if(isset($_GET['workflow-comments']))
		{
			$params['start_section'] = 'comments';
		}
		elseif(isset($_GET['workflow-people']))
		{
			$params['start_section'] = 'people';
		}
		elseif(isset($_GET['workflow-stages']))
		{
			$params['start_section'] = 'status';
		}
		
		if($this->load_js_and_css)
		{
			$this->addClassCSSFile('TMv_WorkflowControlBar');
			$this->addClassJSFile('TMv_WorkflowControlBar');
			$this->addClassJSInit('TMv_WorkflowControlBar', $params);
		}
		
		/** @var TMt_WorkflowItem|TCm_Model $model */
		$model = $module->currentModel();
		
		$button_bar = new TCv_View();
		$button_bar->addClass('button_bar');
		
		$num_enabled_workflow_items = 0;
		if($model)
		{
			foreach($model->workflowModelMatches() as $match)
			{
				if($match->isEnabled())
				{
					$workflow = $match->workflow();

					$workflow_button = new TCv_Link('workflow_switch_button_' . $workflow->id());
					$workflow_button->setURL('#');
					$workflow_button->addText($workflow->code());
					$workflow_button->addClass('workflow_switch_button');
					$workflow_button->addDataValue('id', $workflow->id());
					$workflow_button->setTitle('Switch to ' . $workflow->title());
					$workflow_button->setAttribute('style', 'background-color:#' . $workflow->color() . ';');
					$workflow_button->addDataValue('code', $workflow->code());
					$workflow_button->addDataValue('title', $workflow->title());
					$workflow_button->addDataValue('color', $workflow->color());
					$workflow_button->addDataValue('color-light', $workflow->colorLight());
					$button_bar->attachView($workflow_button);
					
					$num_enabled_workflow_items++;
				}
			}
		}
		
		// If we have at least one workflow item, then we want to show the buttons to see the modes
		if($num_enabled_workflow_items > 0)
		{
			
			$comment_button = new TCv_Link('workflow_control_status');
			//$comment_button->addText('Comments');
			$comment_button->setIconClassName('fa-list-ol');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'status');
			$comment_button->setURL('#');
			$button_bar->attachView($comment_button);
			
			$comment_button = new TCv_Link('workflow_control_comments');
			//$comment_button->addText('Comments');
			$comment_button->setIconClassName('fa-comments');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'comments');
			$comment_button->setURL('#');
			$button_bar->attachView($comment_button);
			
			$comment_button = new TCv_Link('workflow_control_people');
			//$comment_button->addText('People');
			$comment_button->setIconClassName('fa-users');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'people');
			$comment_button->setURL('#');
			$button_bar->attachView($comment_button);
		}
		if($module->currentUserIsSuper())
		{
			$comment_button = new TCv_Link('workflow_control_settings');
			$comment_button->addClass('control_button');
			$comment_button->setIconClassName('fa-cog');
			$comment_button->addDataValue('section', 'settings');
			$comment_button->addClass('');
			$comment_button->setURL('#');
			$button_bar->attachView($comment_button);
		}
		
		// No workflows and not an admin, we need to hide this entire interface
		if(!$module->currentUserIsSuper() && $num_enabled_workflow_items == 0)
		{
			$this->clear();
			return;
		}
		
		if($this->allow_sliding)
		{
			$comment_button = new TCv_Link('workflow_control_toggle');
			$comment_button->addText(' ');
			$comment_button->setIconClassName('fa-chevron-left');
			$comment_button->setURL('#');
			$button_bar->attachView($comment_button);
		}
		$this->attachView($button_bar);
		
		$model = $module->currentModel();
		if($model)
		{
			$workflow_editor = TMv_WorkflowEditor::init($model);
			$this->attachView($workflow_editor);
			
		}
		
		
	}

}