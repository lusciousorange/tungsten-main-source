class TMv_WorkflowControlBar {
	element;
	workflow_editor;
	toggle_view_button;
	options = {
		model_name : false,
		scroll_timer : false,
		start_section : null
	};


	constructor(element, params) {
		this.element = element;

		this.workflow_editor = document.querySelector('.TMv_WorkflowEditor');
		this.toggle_view_button = this.element.querySelector('#workflow_control_toggle');

		// Update options
		Object.assign(this.options, params);

		// Deal with identifying the first workflow
		document.body.classList.add('workflow_init');

		// Stored in session storage to avoid maintaining across multilpe windows to different interfaces
		// Refreshes each new tab
		let workflow_id = sessionStorage.getItem('workflow_id');

		// Check for saved workflow that's been disabled
		if(workflow_id)
		{
			// find the button for that switch
			let button = this.element.querySelector('#workflow_switch_button_'+workflow_id);
			if(button === null)
			{
				workflow_id = null;
			}
		}

		// No default workflow, pick the first one
		if(workflow_id === null)
		{
			let button = this.element.querySelector('.workflow_switch_button');
			if(button !== null)
			{
				workflow_id = button.getAttribute('data-id');
			}

		}

		// NO workflow found, show the settings
		if(workflow_id === null)
		{
			this.showSection('settings');
		}
		else // workflow found, show that
		{
			// Ensure color settings are set
			this.switchToWorkflow(workflow_id);

		}

		// add listeners
		this.toggle_view_button.addEventListener('click', (e) => {this.toggleClicked(e);});

		this.element.querySelectorAll('.control_button').forEach(el => {
			el.addEventListener('click', (e) => {this.controlClicked(e); });
		});

		this.element.querySelectorAll('.workflow_switch_button').forEach(el => {
			el.addEventListener('click', (e) => {this.workflowButtonClicked(e); });
		});

		

		// No section set, default to "status"
		if(this.options.start_section === null)
		{
			this.options.start_section = 'status';
			let existing_value = sessionStorage.getItem('workflow_section_showing');
			if(existing_value == null)
			{
				this.options.start_section = 'status';

			}
			else
			{
				this.options.start_section = existing_value;

			}
		}

		this.showSection(this.options.start_section);


		// Deal with showing the workflow at all
		let show_workflow = JSON.parse(sessionStorage.getItem('workflow_show'));
		if(show_workflow === null)
		{
			sessionStorage.setItem('workflow_show', 'true');
			show_workflow = true;
		}

		if(show_workflow)
		{
			document.body.classList.add('showing_workflow');
			this.workflow_editor.classList.add('showing');
		}
		else
		{
			document.body.classList.remove('showing_workflow');
			this.workflow_editor.classList.remove('showing');
		}

		window.addEventListener('scroll', this.screenScroll.bind(this));

		// Wait a sec for page to load
		setTimeout(this.screenScroll.bind(this),1000);

		// // Wait 2 seconds, disable the init, which enables animations
		setTimeout(() => document.body.classList.remove('workflow_init'),1000);
	}

	/**
	 * Switches the panel to a given workflow
	 */
	switchToWorkflow(workflow_id) {
		sessionStorage.setItem('workflow_id', workflow_id);

		let button = this.element.querySelector('#workflow_switch_button_' + workflow_id);

		let data_color = button.getAttribute('data-color');
		let data_color_light = button.getAttribute('data-color-light');

		// Update the selected class
		this.element.querySelectorAll('.workflow_switch_button').forEach(el => {
			el.classList.remove('selected');
		});
		button.classList.add('selected');

		// Change title color, which is part of the editor
		this.workflow_editor.querySelectorAll('.workflow_section_box h2 ').forEach(h2 => {
			h2.style.color = '#' + data_color;
			h2.style.backgroundColor = '#' + data_color_light;
		});


		// Update the selected class
		this.element.querySelectorAll('.wf_button').forEach(el => {
			el.style.color = null;
			el.style.backgroundColor = null;
		});

		let showing_button = this.element.querySelector('.wf_button.showing');
		if(showing_button)
		{
			showing_button.style.color = '#' + data_color;
			showing_button.style.backgroundColor = '#' + data_color_light;
		}


	}

	showSection(section_name) {
		// Hide all the boxes
		this.element.querySelectorAll('.workflow_section_box').forEach(el => {
			el.hide();
		});

		this.element.querySelectorAll('.control_button').forEach(el => {
			el.classList.remove('showing');
		});

		this.element.querySelector('#workflow_control_' + section_name).classList.add('showing');

		sessionStorage.setItem('workflow_section_showing',section_name);


		if(section_name === 'settings')
		{
			this.workflow_editor.querySelector('#workflow_' + section_name + '_box').show();


		}
		else // workflow chosen
		{
			let workflow_id = sessionStorage.getItem('workflow_id');

			// show the specific workflow related one
			this.workflow_editor.querySelector('#workflow_' + section_name + '_box'+ '_' + workflow_id).show();

			// Ensure styling updates match
			this.switchToWorkflow(workflow_id);
		}

	}


	/**
	 * Event handler when one of the control buttons is clicked to switch modes inside of a workflow
	 * @param event
	 */
	controlClicked(event)
	{
		event.preventDefault();

		let section_name = event.target.closest('.control_button').getAttribute('data-section');
		this.showSection(section_name);
		this.showPanel();
	}

	/**
	 * Forces the workflow panel to show
	 */
	showPanel() {
		document.body.classList.add('showing_workflow');
		this.toggle_view_button.classList.remove('hiding_workflow');
		this.workflow_editor.classList.add('showing');

		sessionStorage.setItem('workflow_show', 'true');

	}

	workflowButtonClicked(event) {
		event.preventDefault();

		let button = event.target.closest('.workflow_switch_button');
		let workflow_id = button.getAttribute('data-id');
		this.switchToWorkflow(workflow_id);
		let current_section = sessionStorage.getItem('workflow_section_showing');
		this.showSection(current_section);



	}

	//////////////////////////////////////////////////////
	//
	// TOGGLING SHOWING THE PANEL
	//
	//////////////////////////////////////////////////////

	/**
	 * Event handler for if the button is clicked
	 * @param event
	 */
	toggleClicked(event) {
		if(typeof(event) === 'object' )
		{
			event.preventDefault();
		}

		this.toggleShowing();
		this.repositionEditor();
		sessionStorage.setItem('workflow_show', !JSON.parse(sessionStorage.getItem('workflow_show')));

	}

	/**
	 * Toggles if the panel is actually showing
	 */
	toggleShowing() {
		document.body.classList.toggle('showing_workflow');

		this.toggle_view_button.classList.toggle('hiding_workflow');
		this.workflow_editor.classList.toggle('showing');
	}


	//////////////////////////////////////////////////////
	//
	// SCREEN SCROLLING AND FULL HEIGHT LAYOUT
	//
	//////////////////////////////////////////////////////

	screenScroll() {
		// Make sure we're referencing something we aren't moving
		let first_submenu_bar = document.querySelector('.TSv_ModuleNavigation_SubmenuBar');

		if(first_submenu_bar && first_submenu_bar.offsetTop - window.scrollY < 0)
		{
			this.element.classList.add('pinned');
		}
		else
		{
			this.element.classList.remove('pinned');
		}
		this.repositionEditor();

	}

	/**
	 * Determines the appropriate height for the editor within the control panel and sets the height
	 */
	repositionEditor() {
		if(document.body.classList.contains('showing_workflow'))
		{
			// Pinned is handled by css, remove any calculations
			if(this.element.classList.contains('pinned'))
			{
				this.element.style.height = null;
			}
			else
			{
				let new_height = window.innerHeight - this.element.getBoundingClientRect().y;
				this.element.style.height = new_height+'px';
			}
		}

	}


}