<?php
class TMv_WorkflowItemCommentBox extends TCv_View
{
	protected TMm_WorkflowModule $workflow_module;
	protected ?TMm_WorkflowModelMatch $match = null;
	protected ?TMm_Workflow $workflow = null;
	protected TCm_Model $model;
	
	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param ?TMm_WorkflowModelMatch $match The match to be used. If none is provided, then all the comments for the
	 * current model are found and shown
	 */
	public function __construct (?TMm_WorkflowModelMatch $match = null)
	{
		$this->match = $match;
		
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->model = $this->workflow_module->currentModel();
		
		if($match instanceof TMm_WorkflowModelMatch)
		{
			// ID uses main workflow for matching
			
			$this->workflow = $this->match->workflow();
			parent::__construct('workflow_comments_box_'.$this->workflow->id());
			$this->addDataValue('match-id', $this->match->id());
		}
		else // Tungsten 9, comments aren't tied to workflows
		{
			parent::__construct();
			
			// Find a workflow to use, and rely on that for everything. It doesn't really matter.
			
		}
		
		$this->addClass('workflow_section_box');

		
		$this->addClassCSSFile('TMv_WorkflowItemCommentBox');
		$this->addClassJSFile('TMv_WorkflowItemCommentBox');
		$this->addClassJSInit('TMv_WorkflowItemCommentBox');
	}

	public function modelLink()
	{
		if($this->workflow)
		{
			$link = new TCv_Link();
			$link->addClass('workflow_model_link');
			$link->addText($this->workflow->title());
			$link->setIconClassName($this->model->moduleForThisClass()->iconCode());
			$link->setURL($this->model->adminEditURL());
			return $link;
		}
		return null;
	}

	public function render ()
	{
	//	$this->addConsoleDebug('Rendering Comment box');
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Comments');
		$this->attachView($heading);
		
		
		// ATTACH THE FORM
		$this->attachView($this->commentForm());

		$comment_page_title = new TCv_View();
		$comment_page_title->addClass('comment_page_title');
		$comment_page_title->addText('Related Comments');
		$this->attachView($comment_page_title);
		
		$match_id = null;
		if($this->match)
		{
			$match_id = $this->match->id();
		}
		$box = new TCv_View('workflow_model_comments_box_' . $match_id);
		$box->addClass('comments_container');
		if($this->workflow)
		{
			foreach($this->match->comments() as $comment)
			{
				$box->attachView(new TMv_WorkflowComment($comment));
			}
		}
		else
		{
			foreach($this->model->workflowComments() as $comment)
			{
				$box->attachView(new TMv_WorkflowComment($comment));
			}
			
		}
		$this->attachView($box);
		
	}
	
	protected function commentForm(): TCv_Form
	{
		// ID must be based on the workflow ID, not the match ID because other interacting systems only know about
		// the workflow, not the specific match
		if($this->workflow)
		{
			$workflow_id = $this->workflow->id();
		}
		else
		{
			$workflow_id = null;
		}
		$comment_form = new TCv_Form('workflow_comment_form_'.$workflow_id);
		$comment_form->addClass('workflow_comment_form');
		$comment_form->addDataValue('workflow-id', $workflow_id);
		
		if($this->match)
		{
			$comment_form->addDataValue('match-id', $this->match->id());
		}
		else
		{
			$comment_form->addDataValue('match-id', null);
		}
		
		$comment_form->setButtonText('Add Comment');
		
		$field = new TCv_FormItem_HTMLEditor('workflow_comment_'.$workflow_id,'Comment');
		$field->addClass('workflow_comment_box');
		$field->useSuperBasicEditor();
		$field ->setShowTitleColumn(false);
		
		$link_button = new TCv_Link('comment_link_button_'.$workflow_id);
		$link_button->addClass('comment_link_button');
		$link_button->setIconClassName('fa-link');
		$link_button->addText('Comment Linked');
		$link_button->addClass('has_content_link');
		$link_button->setURL('#');
		$link_button->setTitle('Remove Link');
		$field->attachViewAfter($link_button);
		$comment_form->attachView($field);
		
		$field = new TCv_FormItem_Hidden('content_id_'.$workflow_id,'');
		$comment_form->attachView($field);
		
		//$this->addConsoleDebugObject($this->model->title(), $this->model);
		
		$field = new TCv_FormItem_Hidden('model_id','Model ID');
		$field->setValue($this->model->id());
		$comment_form->attachView($field);
		
		$field = new TCv_FormItem_Hidden('model_name','Model Name');
		$field->setValue($this->model->baseClassName());
		$comment_form->attachView($field);
		
		return $comment_form;
	}
}