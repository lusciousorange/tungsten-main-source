<?php
	require($_SERVER['DOCUMENT_ROOT'] . "/admin/system/headers/tungsten_header.php");


	$values = array();
	$values['comment'] = $_POST['comment'];
	$values['model_name'] = $_POST['model_name'];
	$values['model_id'] = $_POST['model_id'];

	if($_POST['workflow_id'] != '')
	{
		$values['workflow_id'] = $_POST['workflow_id'];
		
	}

	if($_POST['workflow_model_match_id'] != '')
	{
		$values['workflow_model_match_id'] = $_POST['workflow_model_match_id'];
		
	}
	
	$values['user_id'] = TC_currentUser()->id();
	if($_POST['content_id'] != '')
	{
		$values['content_id'] = $_POST['content_id'];
	}

	$comment = TMm_WorkflowComment::createWithValues($values);

	if($comment)
	{
		$comment_view = new TMv_WorkflowComment($comment);
		print  $comment_view->html();

	}


?>