class TMv_WorkflowItemCommentBox {
	element;

	constructor(element) {
		this.element = element;

		this.element.querySelector('.workflow_comment_form').addEventListener('submit', (e) => this.commentSubmitted(e));

		this.element.addEventListener('click', this.delegateEventListener.bind(this));

		// Hide all the comment link buttons
		this.element.querySelectorAll(".comment_link_button").forEach(el => el.hide());



	}

	delegateEventListener(event) {
		if(event.target.closest('.comment_delete'))
		{
			this.deleteCommentClicked(event);
		}
		else if(event.target.closest('.comment_toggle_resolved'))
		{
			this.resolveCommentClicked(event);
		}
		else if(event.target.closest('.comment_link_button'))
		{
			this.removeCommentLinkClicked(event);
		}
	}


	deleteCommentClicked(event) {
		event.preventDefault();

		let params = new URLSearchParams();
		params.append('cancel_next_action', '1');

		let comment_id = event.target.closest('.comment_delete').getAttribute('data-comment-id');

		fetch("/admin/workflow/do/comment-delete/"+ encodeURI(comment_id) +'?'+ params,{
			method : 'GET'
		})
			.then(response => {
				this.element.querySelector('#TMm_WorkflowComment--' + comment_id).remove();
			});

	}

	/**
	 * Handles when the resolved button for a comment is clicked
	 * @param event
	 */
	resolveCommentClicked(event) {
		event.preventDefault();

		let button = event.target.closest('.comment_toggle_resolved');
		console.log(button);

		let comment_id = button.getAttribute('data-comment-id');

		let params = new URLSearchParams();
		params.append('cancel_next_action', '1');

		fetch("/admin/workflow/do/comment-toggle-resolved/"+ encodeURI(comment_id) + '?'+params,{
			method : 'GET'
		})
			.then(response => {
				this.element.querySelector('#TMm_WorkflowComment--' + comment_id).classList.toggle('resolved');
			});




	}

	removeCommentLinkClicked(event) {
		event.preventDefault();

		this.disableCommentLinkButton();

	}

	/**
	 * Disables the comment link button
	 */
	disableCommentLinkButton() {
		this.element.querySelectorAll('.comment_link_button').forEach(el => {
			el.setAttribute('data-highlight-target','');
			el.slideUp();
		});
	}


	/**
	 * Triggered when the comment is submitted which
	 * @param event
	 */
	commentSubmitted(event)  {
		console.log('commentSubmitted');
		event.preventDefault();


		let match_id = event.target.getAttribute('data-match-id');
		let workflow_id = event.target.getAttribute('data-workflow-id');

		let content_id_field = event.target.querySelector('#content_id_'+workflow_id);

		let content_id = content_id_field.value;
		let params = new FormData();


		params.append('comment', window.tinymce.get('workflow_comment_'+workflow_id).getContent() );
		params.append('content_id', content_id);
		params.append('workflow_id', workflow_id);
		params.append('workflow_model_match_id', match_id);
		params.append('model_name', event.target.querySelector('#model_name').value);
		params.append('model_id', event.target.querySelector('#model_id').value);

		fetch("/admin/workflow/classes/views/TMv_WorkflowItemCommentBox/ajax_add_workflow_comment.php",{
			method : 'POST',
			body : params,
		})
			.then(response => response.text())
			.then(response => {

				// Add the content
				this.element.querySelector('.comments_container').insertAdjacentHTML('afterbegin', response);

				// Reset the Form
				content_id_field.value = '';
				this.disableCommentLinkButton();

				// Empty the editor
				window.tinymce.get('workflow_comment_'+workflow_id).setContent('');

				// Remove loading from the form
				event.target.classList.remove('tungsten_loading');

			});





	}
}