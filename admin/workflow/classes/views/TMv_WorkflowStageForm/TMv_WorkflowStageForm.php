<?php
class TMv_WorkflowStageForm extends TCv_FormWithModel
{
	/**
	 * TMv_WorkflowStageForm constructor.
	 * @param string|TMm_WorkflowStage $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setSuccessURL('stage-list');
		
	}

	/**
	 * @return TMm_WorkflowStage
	 */
	public function model ()
	{
		return parent::model();
	}

	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TMv_FormItem_WorkflowColorPicker('color', 'Color');
		$this->attachView($field);
		
		
//		$field = new TCv_FormItem_TextField('color', 'Color');
//		$field->setIsRequired();
//		$field->setHelpText('The 6-digit hex color for this stage');
//		$this->attachView($field);

//		$user_group_list = new TMm_UserGroupList();
//		if(sizeof($user_group_list->models()) > 1)
//		{
//			$user_groups = new TCv_FormItem_CheckboxList('user_groups', 'Exit Control Groups');
//			$user_groups->setHelpText("Indicate the groups who can move this item to a different stage in the workflow. Only users in this user group can move this item to another status. ");
//			$user_groups->setSaveToDatabase(false);
//
//			$all_user_groups = $user_group_list->groups();
//			unset($all_user_groups[1]); // remove system admin
//			$selected_groups = array();
//			if($this->isEditor())
//			{
//				$selected_groups = $this->model()->userGroups();
//			}
//			$user_groups->setValuesFromObjectArrays($all_user_groups, $selected_groups);
//			$user_groups->addText('<p>Note: System Administrators have full workflow control.</p>');
//			$this->attachView($user_groups);
//		}

		$field = new TCv_FormItem_Select('is_done','Mark as done');
		$field->setHelpText("Indicates that this stage considers the item 'done'");
		$field->addOption('0','No');
		$field->addOption('1','Yes – Moving to this stage completes the workflow process');
		$this->attachView($field);
		
		
	}


	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$hex = $form_processor->formValue('color');
		//$hex = strtoupper(str_ireplace('#', '', $hex));
		
		// Don't worry about uppercase, we'll deal with mostly names now
		$hex = str_ireplace('#', '', $hex);
		$form_processor->addDatabaseValue('color', $hex );

//		if(!preg_match('/^[0-9A-F]{6}$/i',$hex))
//		{
//			$form_processor->failFormItemWithID('color', 'Color must be exactly 6 HEX digits such as A5B2C3');
//		}

		if($form_processor->isCreator())
		{
			$stage_list = TMm_WorkflowStageList::init();
			$num_stages = $stage_list->numStages();
			$form_processor->addDatabaseValue('display_order', $num_stages+1);
		}


	}

	/**
	 * This function is called after the traditional form processor updateDatabase() function. A form instance can override
	 * this function to do custom form processing that doesn't fit within the standard operating proceedure for many Tungsten forms.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
//		// Only bother if the user groups were actually passed in
//		if($form_processor->fieldIsSet('user_groups'))
//		{
//
//			// Update the user groups
//			/** @var TMm_WorkflowStage $stage */
//			$stage = $form_processor->model();
//			$user_group_list = TMm_UserGroupList::init();
//			$user_group_values = array();
//			foreach ($user_group_list->groups() as $group)
//			{
//				if ($form_processor->formValue('user_groups_' . $group->id()) == 1)
//				{
//					$user_group_values[] = $group->id();
//				}
//			}
//			$stage->updateUserGroups($user_group_values);
//		}

//		// Only bother if the user groups were actually passed in
//		if($form_processor->fieldIsSet('next_stages'))
//		{
//			// Update the user groups
//			/** @var TMm_WorkflowStage $stage */
//			$stage = $form_processor->model();
//			/** @var TMm_WorkflowStageList $stage_list */
//			$stage_list = TMm_WorkflowStageList::init();
//			$next_stage_values = array();
//			foreach ($stage_list->models() as $next_stage)
//			{
//				if ($form_processor->formValue('next_stages_' . $next_stage->id()) == 1)
//				{
//					$next_stage_values[] = $next_stage->id();
//				}
//			}
//			$stage->updateNextStages($next_stage_values);
//		}
	}


}