<?php

/**
 * Class TMv_WorkflowRecentHistory
 */
class TMv_WorkflowRecentHistory extends TCv_View
{
	use TMt_DashboardView;

	/**
	 * TMv_WorkflowControlBar constructor.
	 * @param bool $id
	 */
	public function __construct ($id = false)
	{
		parent::__construct($id);

	}

	public function html ()
	{
		$comment_history_list = new TMm_WorkflowCommentList();
		$models = $comment_history_list->mostRecent(20);

		$stage_history_list = new TMm_WorkflowStageModelHistoryList();
		$models = $models + $stage_history_list->mostRecent(20);

		krsort($models);
		$relevant_models = array_slice($models, 0,10);

		foreach($relevant_models as $model)
		{
			if($model->model())
			{
				if($model instanceof TMm_WorkflowComment)
				{
					$view = new TMv_WorkflowComment($model);
					$view->setShowItemTitle();
					$this->attachView($view);
				}
				elseif($model instanceof TMm_WorkflowStageModelHistory)
				{
					$view = new TMv_WorkflowStageHistory($model);
					$view->setShowItemTitle();
					$this->attachView($view);
				}
			}
		}


		return parent::html();
	}

	/**
	 * Returns the default title for this dashboard
	 * @return string
	 */
	public static function dashboard_DefaultTitle() : string
	{
		return 'Workflow history';
	}

	public static function pageContent_ViewTitle() : string
	{
		return 'Workflow History';
	}

}