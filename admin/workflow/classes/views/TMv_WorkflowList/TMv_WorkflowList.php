<?php
class TMv_WorkflowList extends TCv_ModelList
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_Workflow');
		$this->defineColumns();
		
		$this->addClassCSSFile();
	}
	
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('icon_code');
		$column->setTitle('Icon');
		$column->setContentUsingListMethod('iconColumn');
		$column->setAlignment('center');
		$column->setWidthAsPixels(120);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('category');
		$column->setTitle('Category');
		$column->setContentUsingListMethod('categoryColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('color');
		$column->setTitle('Color');
		$column->setContentUsingListMethod('colorColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('stages');
		$column->setTitle('Stages');
		$column->setContentUsingListMethod('stagesColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('models');
		$column->setTitle('Models');
		$column->setContentUsingListMethod('modelsColumn');
		$this->addTCListColumn($column);

		$this->addDefaultEditIconColumn();
		$this->addDefaultDeleteIconColumn();
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'edit');
		$link->addText($model->title());
		return $link;
	}
	
	/**
	 * @param TMm_Workflow $model
	 * @return string
	 */
	public function iconColumn($model)
	{
		return '<i class="fas ' . $model->iconCode() . '"></i>';
		//return new TMv_SearchResultIcon($model);
		
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function colorColumn($model)
	{
		$view = new TCv_View();
		$view->setTag('span');
		$view->addClass('color_box');
		$view->setAttribute('style','background-color:#'.$model->color().';color:#'.$model->readableColor().';  ');
		
		$view->addText($model->colorName());
		
		return $view;
		
		
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function categoryColumn(TMm_Workflow $model)
	{
		// Checks if the category is set
		if($model->category() != '')
		{
			return TMm_Workflow::$workflow_categories[$model->category()];
		}
		
		return '';
		

	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function stagesColumn($model)
	{
		$column_view = new TCv_View();
		$column_view->addClass('stage_list_container');
		
		foreach($model->stages() as $stage)
		{
			
			$view = new TCv_View();
			$view->setTag('span');
			$view->addClass('color_box');
			$view->setAttribute('style','background-color:#'.$stage->color().';color:#'.$stage->readableColor().';  ');
			
			$view->addText($stage->title());
			
			$column_view->attachView($view);
			
			
			
		//	$titles[] = $stage->title();
		}
		
		return $column_view;
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function modelsColumn($model)
	{
		$titles = [];

		foreach($model->defaultModels() as $model_name => $match)
		{
			$titles[] = $model_name::modelTitlePlural();
		}

		return implode('<br />',$titles);
	}
	
}