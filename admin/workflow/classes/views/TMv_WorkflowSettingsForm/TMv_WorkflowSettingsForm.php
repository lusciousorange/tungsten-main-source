<?php
class TMv_WorkflowSettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{

		$field = new TCv_FormItem_Select('send_emails', 'Send emails');
		$field->setHelpText("Indicate if emails should be sent for notifications.");
		$field->addOption('1', "YES – Emails are sent to users");
		$field->addOption('0', "NO – Emails are NOT sent to users");
		$this->attachView($field);
		
		$user_list = TMm_UserList::init();
		$field->useFiltering($user_list,'namesAndEmailsForSearchString', 'TMm_User');
		
		$this->attachView($field);

		$field = new TCv_FormItem_Select('permitted_user_groups', 'Permitted user groups');
		$field->setHelpText("Indicate the user groups that can interact in Workflow. Administrators always have access.");
		$user_group_list = TMm_UserGroupList::init();
		foreach($user_group_list->models() as $user_group)
		{
			if($user_group->id() != 1)
			{
				$field->addOption($user_group->id(), $user_group->title());
			}
			
		}
		$field->setUseMultiple(',');
		$field->useFiltering();
		$this->attachView($field);


		// If Slack is installed
		if (TC_moduleWithNameInstalled('slack') && class_exists('TMm_SlackClient'))
		{
			$field = new TCv_FormItem_Heading('slack_heading', 'Slack settings');
			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_comments', 'Slack for comments');
			$field->setHelpText("Indicate if comments should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for comments");
			$field->addOption('0', "NO – Do NOT send Slack notifications for comments");
			$this->attachView($field);

//			$field = new TCv_FormItem_Select('slack_for_comment_resolutions', 'Slack for Comment Resolutions');
//			$field->setHelpText("Indicate if comments being marked as resolved, should trigger Slack notifications.");
//			$field->addOption('1', "YES – Send Slack notifications for comment resolutions");
//			$field->addOption('0', "NO – Do NOT send Slack notifications for comment resolutions");
//			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_stages', 'Slack for stage changes');
			$field->setHelpText("Indicate if stage changes should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for stage changes");
			$field->addOption('0', "NO – Do NOT send Slack notifications for stage changes");
			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_user_assignment', 'Slack for user assignment');
			$field->setHelpText("Indicate if user assignments should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for user assignments");
			$field->addOption('0', "NO – Do NOT send Slack notifications for user assignments");
			$this->attachView($field);


		}

	}
	
}
?>