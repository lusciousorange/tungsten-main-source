<?php
class TMv_WorkflowSummary extends TCv_View
{
	//protected $workflow_config = false;
	protected $model = false;
	protected ?TMm_WorkflowModule $workflow_module;
	protected ?TMm_Workflow $workflow;
	protected ?TMm_WorkflowModelMatch $match;

	protected $edit_url = '';
	
	
	/**
	 * TMv_WorkflowSummaryColumn constructor.
	 * @param TMm_WorkflowModelMatch $workflow_match
	 */
	public function __construct($workflow_match)
	{
		//$this->workflow_item = $workflow_item;
		$this->model = $workflow_match->model();
		$this->match = $workflow_match;
		$this->workflow = $workflow_match->workflow();

		parent::__construct();
		$this->addClass('workflow_summary_box');
		
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->edit_url = $this->model->adminEditURL();

		//$this->addClassCSSFile('TMv_WorkflowSummary');

	}
	
	


	public function render()
	{
		if(!$this->workflow_module)
		{
			return '';
		}

		// Get the history item, then the stage for that history item
		$history_stage = $this->match->currentStageHistory();
		$stage = $this->match->currentStage();
		
		// ---- STAGE LINE -----
		$stage_color = new TCv_Link();
		$stage_color->addClass('workflow_stage_color');

		$workflow_tag = new TCv_View();
		$workflow_tag->setTag('span');
		$workflow_tag->addClass('workflow_tag');
		$workflow_tag->setAttribute('style','background-color:#'.$this->workflow->color().';');

		

		$workflow_tag->addText('<i class="'.$this->workflow->iconCode().'"></i>');
		$stage_color->attachView($workflow_tag);
		
		$stage_color->setAttribute('style', 'background:#' . $stage->color() . ';color:#'.$stage->readableColor().';');

		$title = new TCv_View();
		$title->setTag('span');
		$title->addText($stage->title());
		$title->addClass('title');
		
		$stage_color->attachView($title);

		$stage_color->setTitle($this->model->title());
		$stage_color->setURL('#');
		$stage_color->setAttribute('onclick',
		                           "this.nextElementSibling.slideToggle(); return false;");
		$this->attachView($stage_color);


		if($stage->userCanMoveStage())
		{
			$next_stage_box = new TCv_View('next_stage_box_' . $this->model->contentCode());
			$next_stage_box->addClass('next_stage_box');
			$next_stage_box->setAttribute('style', 'border-color:#' . $stage->color());

			$next_previous = new TMv_WorkflowNextPrevious($this->match);
			$next_stage_box->attachView($next_previous);

			$this->attachView($next_stage_box);

		}

		if(!$stage->isDoneStage())
		{
			$workflow_details_box = new TCv_View();
			$workflow_details_box->setTag('span');
			$workflow_details_box->addClass('workflow_details');


//			// ---- USERS -----
//			$users = $this->model->workflowAssignedUsers($this->workflow);
//
//			foreach($users as $user)
//			{
//				$box = new TCv_Link();
//				//$box->setTag('span');
//				$box->setURL($this->edit_url . '?workflow-people');
//				$box->addClass('detail_box');
//				$box->addClass('user_box');
//				$box->addText($user->initials());
//				if($user->id() == TC_currentUser()->id())
//				{
//					$box->addClass('active_user');
//				}
//				$workflow_details_box->attachView($box);
//			}

//			// ---- COMMENTS -----
//			// Tungsten 9 disconnects comments from the workflows directly
//			if(!TC_getConfig('use_tungsten_9'))
//			{
//				$comments = $this->match->comments();
//				$num_comments = sizeof($comments);
//
//				$num_unresolved_comments = 0;
//				foreach($comments as $comment)
//				{
//					if(!$comment->isResolved())
//					{
//						$num_unresolved_comments++;
//					}
//				}
//
//				if($num_comments > 0)
//				{
//					$box = new TCv_Link();
//					$box->setURL($this->edit_url . '?workflow-comments');
//					$box->addClass('detail_box');
//					$box->addClass('comment_box');
//					$box->setIconClassName('fa-comments');
//
//
//					if($num_unresolved_comments == 0)
//					{
//						$box->addText('All');
//					}
//					else
//					{
//						$box->addText(($num_comments - $num_unresolved_comments) . ' of ' . $num_comments);
//						$box->addClass('comment_action_required');
//					}
//
//					$box->addText(' Resolved');
//					$workflow_details_box->attachView($box);
//				}
//			}
			
			$this->attachView($workflow_details_box);
		}
	}


	/**
	 * @param string $url_target_name
	 * @param string $title
	 * @param string $icon_name
	 * @param bool|string $target_id
	 */
	public function addError($url_target_name, $title, $icon_name, $target_id = false)
	{
		$module = $this->model->moduleForThisClass();

		// We have a successful link to create
		$link = new TCv_Link();
		$link->setTitle( $title );
		$link->addText('<span>'.$title.'</span>');
		$link->setURL('/admin/'.$module->folder().'/do/'.$url_target_name.'/'
					  .$this->model->id());


		$link->setIconClassName($icon_name);
		if($target_id)
		{
			$link->appendToURL('#'.$target_id);
		}
		$link->addClass('workflow_error');
		$this->attachView($link);
	}


}
?>