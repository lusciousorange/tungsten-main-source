<?php

/**
 * Class TMv_WorkflowHistoryItem
 */
class TMv_WorkflowHistoryItem extends TCv_View
{
	protected $model = false;
	protected $show_item_title = false;

	/**
	 * TMv_WorkflowHistoryItem constructor.
	 * @param TCm_Model $history_item
	 */
	public function __construct ($history_item)
	{
		$this->model = $history_item;
		parent::__construct($history_item->contentCode());

			$this->addClassCSSFile('TMv_WorkflowHistoryItem');



	}

	public function setShowItemTitle()
	{
		$this->show_item_title = true;
	}

	public function showItemTitle()
	{
		return $this->show_item_title ;
	}

	public function item()
	{
		return $this->model;
	}

	public function modelLink()
	{

		$link = new TCv_Link();
		$link->addClass('workflow_model_link');
		$link->addText($this->model->title());
		$link->setIconClassName($this->model->moduleForThisClass()->iconCode());
		$link->setURL($this->model->adminEditURL());
		return $link;
	}

	public function html ()
	{

		return parent::html();
	}

}