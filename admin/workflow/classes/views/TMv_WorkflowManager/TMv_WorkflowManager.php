<?php
class TMv_WorkflowManager extends TCv_View
{
	public function __construct($id = false)
	{
		parent::__construct('workflow_manager');
	}
	
	public function render()
	{
		$workflow_list = TMm_WorkflowList::init();
		foreach($workflow_list->models() as $workflow)
		{
			$this->attachView(new TMv_WorkflowKanban($workflow));
		}
	}
}