<?php

/**
 * An email sent for a particular person being added to an item
 *
 * Class TMv_WorkflowPeopleEmail
 */
class TMv_WorkflowPeopleEmail extends TMv_WorkflowEmail
{
	/**
	 * TMv_WorkflowEmail constructor.
	 * @param TMm_WorkflowAssignedUser $new_user_match
	 * @param TMm_User|bool $user (Optional) Default false
	 */
	public function __construct ($new_user_match, $user = false)
	{
		parent::__construct($new_user_match, $user);
		$this->setURLControlCode('workflow-people');
		$this->setSubject('Workflow Assignment : '.$this->workflow_item->typeTitle().' – ' . $this->workflow_item->model()->title());
	}


	/**
	 * A method to be overwritten to handle the specific content for this email. This content gets wrapped in the
	 * existing framework for all Workflow emails.
	 */
	public function attachMessageContent()
	{
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('You have been assigned to the '.$this->workflow_item->typeTitle().' ');
			$link = new TCv_Link();
			$link->setURL($this->fullDomainName().$this->workflow_item->model()->adminEditURL().$this->url_control_code);
			$link->addText($this->workflow_item->model()->title());
			$p->attachView($link);
		$this->attachView($p);


	}


}

?>