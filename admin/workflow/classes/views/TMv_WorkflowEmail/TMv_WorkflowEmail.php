<?php

/**
 * An email sent for anything related to workflow
 *
 * Class TMv_WorkflowEmail
 */
class TMv_WorkflowEmail extends TCv_Email
{
	protected $workflow_item = false;
	protected $url_control_code = '';
	/**
	 * TMv_WorkflowEmail constructor.
	 * @param TMm_WorkflowHistoryItem $workflow_item
	 * @param TMm_User|bool $user (Optional) Default false
	 */
	public function __construct ($workflow_item, $user = false)
	{
		if($user === false)
		{
			$user = TC_currentUser();
		}
		parent::__construct($user);
		$this->workflow_item = $workflow_item;
	//	$this->setEmbedCSSFiles(false);
		$this->addClassCSSFile('TMv_WorkflowEmail');

	}

	/**
	 * Sets the control code for url redirects when viewing the item
	 * @param string $code
	 */
	public function setURLControlCode($code)
	{
		$this->url_control_code = '?'.$code;
	}


	/**
	 * A method to be overwritten to handle the specific content for this email. This content gets wrapped in the
	 * existing framework for all Workflow emails.
	 */
	public function attachMessageContent()
	{

	}

	/**
	 * The html for the email
	 * @return bool|string
	 */
	public function html ()
	{
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText($this->user->firstName().',');
		$this->attachView($p);

		$this->attachMessageContent();

		$p = new TCv_View();
		$p->setTag('p');
			$link = new TCv_Link();
			$link->addClass('big_button');
			$link->setURL($this->fullDomainName().$this->workflow_item->model()->adminEditURL().$this->url_control_code);
			$link->addText('View '.$this->workflow_item->typeTitle());
		$p->attachView($link);
		$this->attachView($p);

		$disclaimer = new TCv_View();
		$disclaimer->addClass('disclaimer');
		$disclaimer->addText("You received this email because your account on <a href=\"".$this->fullDomainName()."\">".$this->fullDomainName()."</a> is configured to send these emails.
		To change your settings, edit your <a href=\"".$this->fullDomainName().'/admin/profile/do/edit-workflow/'."\">Profile Settings for Workflow</a>.
 		");

		$this->attachView($disclaimer);




		return parent::html();
	}

	public function send ($force_send = false)
	{

		if(!TC_getModuleConfig('workflow','send_emails'))
		{
			return false;
		}
		// Avoid sending to the same user who created the item. No need.
		if($this->workflow_item->user()->id() == $this->user->id())
		{
			return false;
		}
		parent::send();

	}
}

?>