<?php

/**
 * An email sent for a particular stage is changed
 *
 * Class TMv_WorkflowStageEmail
 */
class TMv_WorkflowStageEmail extends TMv_WorkflowEmail
{
	/**
	 * TMv_WorkflowEmail constructor.
	 * @param TMm_WorkflowStageModelHistory $model_stage
	 * @param TMm_User|bool $user (Optional) Default false
	 */
	public function __construct ($model_stage, $user = false)
	{
		parent::__construct($model_stage, $user);
		$this->setURLControlCode('workflow-stages');
		$this->setSubject('Workflow Stage : '.$this->workflow_item->typeTitle().' – ' . $this->workflow_item->model()->title());
	}

	/**
	 * A method to be overwritten to handle the specific content for this email. This content gets wrapped in the
	 * existing framework for all Workflow emails.
	 */
	public function attachMessageContent()
	{
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText($this->workflow_item->user()->fullName().' has change the stage for the '.$this->workflow_item->typeTitle().' ');
		$link = new TCv_Link();
		$link->setURL($this->fullDomainName().$this->workflow_item->model()->adminEditURL());
		$link->addText($this->workflow_item->model()->title());
		$p->attachView($link);

		$p->addText(' to <strong>'.$this->workflow_item->stage()->title().'</strong>');


		$this->attachView($p);

	}
}

?>