class TMv_WorkflowEditor {
	element;
	default_stage_text = {};
	default_stage_style = {};

	constructor(element, params) {
		this.element = element;

		this.element.querySelectorAll('.stage_dot').forEach(dot => {
			dot.addEventListener('mouseenter', (e) => { this.stageDotMouseOver(e); });
			dot.addEventListener('mouseleave', (e) => { this.stageDotMouseOut(e); });

		});

		this.element.querySelectorAll('.stage_box').forEach(box => {
			let match_id = box.getAttribute('data-match-id');
			this.default_stage_text[match_id] = box.textContent;
			this.default_stage_style[match_id] = box.getAttribute('style');

		});

	}



	stageDotMouseOver(event) {
		// Get the dot that was clicked
		let dot = event.target.closest('.stage_dot');

		// find the corresponding stage box for this dot
		let stage_box = dot.closest('.workflow_section_box').querySelector('.stage_box');

		stage_box.textContent = dot.getAttribute('data-title');
		stage_box.setAttribute('style', dot.getAttribute('style'));

	}

	/**
	 * Resets the style to the previous setting
	 * @param event
	 */
	stageDotMouseOut(event) {
		// Get the dot that was clicked
		let dot = event.target.closest('.stage_dot');

		// find the corresponding stage box for this dot
		let stage_box = dot.closest('.workflow_section_box').querySelector('.stage_box');

		let section_box = event.target.closest('.workflow_section_box');
		let match_id = section_box.getAttribute('data-match-id');

		stage_box.textContent = this.default_stage_text[match_id];
		stage_box.setAttribute('style', this.default_stage_style[match_id]);
	}
}