<?php

/**
 * Class TMv_WorkflowEditor
 */
class TMv_WorkflowEditor extends TCv_View
{
	
	/** @var bool|TMm_WorkflowModule  */
	protected $workflow_module = false;
	
	/** @var TCm_Model|TMt_WorkflowItem $model */
	protected $model;
	
	public function __construct ($model)
	{
		parent::__construct('workflow_editor');
		
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->model = $model;
	}
	

	public function render ()
	{
		if(!TC_getConfig('use_workflow'))
		{
			return '';
		}

		// Add for comments so we can just pull them in
		$this->addClassCSSFile('TMv_WorkflowStageHistory');
		$this->addClassCSSFile('TMv_WorkflowHistoryItem');
		$this->addClassCSSFile('TMv_WorkflowComment');
		
		$this->addClassCSSFile('TMv_WorkflowEditor');
		$this->addClassJSFile('TMv_WorkflowEditor');
		$this->addClassJSInit('TMv_WorkflowEditor');


		$stage_box = new TCv_View();
		$stage_box->addClass('workflow_stage_box');

		// Not Setup Yet, probably no stages, avoid hard fail
		foreach($this->model->workflowModelMatches() as $workflow_match)
		{
			if($workflow_match->currentStage() && $workflow_match->isEnabled() )
			{
				$this->attachView(new TMv_WorkflowItemCommentBox($workflow_match));
				$this->attachView(new TMv_WorkflowItemStatusBox($workflow_match));
				$this->attachView(new TMv_WorkflowItemPeopleBox($workflow_match));
			}
		}
		
		if($this->workflow_module->currentUserIsSuper())
		{

			$this->attachView($this->settingsBox());
		}
	}



	
	/**
	 * The box for settings
	 * @return TCv_View
	 */
	public function settingsBox()
	{
		$settings = TMv_WorkflowSettingsBox::init($this->model);


//		// Add the heading for the view
//		$heading = new TCv_View();
//		$heading->setTag('h2');
//		$heading->addText('Workflow Settings');
//		$settings->attachView($heading, true);
		
		return $settings;
//
	}

}