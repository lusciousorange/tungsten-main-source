<?php

/**
 * Class TMv_WorkflowNextPrevious
 *
 * A 2-button layout that will take a workflow item and move it to the next stage or the previous
 *
 * @author Katie Overwater
 */
class TMv_WorkflowNextPrevious extends TCv_View
{

	protected $match = false;

	/**
	 * TMv_WorkflowNextPrevious constructor.
	 * @param TMm_WorkflowModelMatch $match
	 */
	public function __construct ($match)
	{
		parent::__construct();

		$this->match = $match;

		$this->addClassCSSFile();
		
		// File added in TSv_Tungsten, so it always loads
		//$this->addJSFile('workflow_next_prev_handler',$this->classFolderFromRoot().'/workflow_next_prev_handler.js');
		
	}

	public function render ()
	{
		$next_stage = $this->match->nextStage();

		$module = TMm_WorkflowModule::init();

		if (!$this->match->currentStage()->isStartStage())
		{
			$this->addClass('has_prev');
			$button = new TCv_Link();
			$button->addClass('previous_button');
			$button->addText('<i class="fas fa-chevron-left"></i>Previous');
			$button->setURL('/admin/workflow/do/move-to-previous-stage/' . $this->match->id());
			$this->attachView($button);
		}
		if (!$this->match->currentStage()->isDoneStage() && ($module->currentUserIsSuper() || !$next_stage->isDoneStage()))
		{
			$this->addClass('has_next');
			$button = new TCv_Link();
			$button->addClass('next_button');
			$button->addText('Next<i class="fas fa-chevron-right"></i>');
			$button->setURL('/admin/workflow/do/move-to-next-stage/' . $this->match->id());
			$this->attachView($button);
		}

	}

}