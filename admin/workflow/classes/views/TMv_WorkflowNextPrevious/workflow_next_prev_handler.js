document.body.addEventListener('click', workflowClickHandler);

function workflowClickHandler(event) {

	let workflow_button = event.target.closest('.next_button, .previous_button');
	if(workflow_button)
	{
		let status_box = workflow_button?.closest('.TMv_WorkflowItemStatusBox');
		let summary_parent = workflow_button?.closest('.workflow_summary_box');
		let url = null;

		let update_target = null;
		// We're inside  a workflow summary
		if(summary_parent)
		{
			url = workflow_button.getAttribute('href');
			url += '?next_url_target_name=workflow-summary';
			update_target = summary_parent;

		}
		else if (status_box)
		{
			url = workflow_button.getAttribute('href');
			url += '?next_url_target_name=workflow-status-box';
			update_target = status_box;
		}



		if(url && update_target)
		{
			update_target.classList.add('tungsten_loading');

			event.preventDefault();

			fetch(url, {
				method : 'GET',
			}).then(response => response.json())
				.then(response =>
				{
					if(response.html)
					{
						let parent = update_target.parentElement;
						let id = update_target.getAttribute('id');
						update_target.outerHTML = response.html;


						status_box = parent.querySelector('#' + id);
						if(status_box)
						{
							status_box.show();
						}

					}

				})
				.catch((error) =>
				{
					console.log(error);
				})
				.finally(() =>
				{
					this.fetch_abort_controller = null; // disable the abort controller
				});
		}

	}

}