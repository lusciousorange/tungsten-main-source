<?php
class TMv_WorkflowStageList extends TCv_RearrangableModelList
{
	/**
	 * TMv_WorkflowStageList constructor.
	 * @param bool|TMm_Workflow $workflow
	 */
	public function __construct($workflow = false)
	{
		parent::__construct();
		
		$this->setModelClass('TMm_WorkflowStage');
		$this->defineColumns();

		$this->addClassCSSFile();
	}

	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('color');
		$column->setTitle('Color');
		$column->setContentUsingListMethod('colorColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('done');
		$column->setTitle('Done stage');
		$column->setContentUsingListMethod('doneStageColumn');
		$column->setAlignment('center');
		$this->addTCListColumn($column);

		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
		
	}


	/**
	 * Returns the column value for the provided model
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'stage-edit');
		$link->addText($model->title());
		return $link;
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function colorColumn($model)
	{
		$view = new TCv_View();
		$view->setTag('span');
		$view->addClass('color_box');
		$view->setAttribute('style','background-color:#'.$model->color().';color:#'.$model->readableColor().';');
		
		$view->addText($model->colorName());

		return $view;

	}
	
	/**
	 *
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function doneStageColumn($model)
	{
		if($model->isDoneStage())
		{
			return $this->controlIcon('fa-check');
			
		}
	}
	
		
}
?>