<?php
class TMv_WorkflowItemPeopleBox extends TCv_View
{
	/** @var bool|TMm_WorkflowModule $workflow_module  */
	protected $workflow_module;
	
	protected $match;
	protected $model;
	
	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param TMm_Workflow $match
	 */
	public function __construct (TMm_WorkflowModelMatch $match)
	{
		$this->match = $match;
		$this->model = $match->model();
	
		$this->workflow_module = TMm_WorkflowModule::init();

		// ID uses main workflow for matching
		parent::__construct('workflow_people_box_'.$this->match->workflow()->id());
		$this->addClass('workflow_section_box');
		$this->addDataValue('match-id',$this->match->id());
		

		$this->addClassCSSFile('TMv_WorkflowItemPeopleBox');
		$this->addClassJSFile('TMv_WorkflowItemPeopleBox');
		$this->addClassJSInit('TMv_WorkflowItemPeopleBox');


	}

	public function render ()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('People');
		$this->attachView($heading);


		$form = new TCv_Form('workflow_people_form');
		$form->hideSubmitButton();
		$form->addDataValue('match-id', $this->match->id());
		
		$all_workflow_users = $this->workflow_module->permittedUsers();

		foreach($all_workflow_users as $user)
		{
		//	$this->addConsoleDebugObject($user->title(), $user);
			$field = new TCv_FormItem_Checkbox($this->match->id().'_users_'.$user->id(),'');
			$field->setLabel($user->fullName());
			$field->setFormValue($user->id());
			
			$field->setShowTitleColumn(false);
			
			if($this->match->userIsAssigned($user))
			{
			//	$this->addConsoleDebug('user was assigned');
				$field->setDefaultValue(true);
			}
		
			$form->attachView($field);
		}
		
		$this->attachView($form);
	}

}