class TMv_WorkflowItemPeopleBox {
	element;
	constructor(element, params) {
		this.element = element;

		this.element.querySelectorAll('form input[type="checkbox"]').forEach(el => {
			el.addEventListener('change', (e) => this.personClicked(e));
		});

	}


	personClicked(event) {
		event.preventDefault();

		let checkbox = event.target;

		let params = new FormData();
		params.append('user_id', checkbox.value);
		params.append('is_checked',  (checkbox.checked ? '1' : '0'));

		let match_id = this.element.querySelector('form').getAttribute('data-match-id');
		params.append('workflow_model_match_id', match_id);

		fetch("/admin/workflow/classes/views/TMv_WorkflowItemPeopleBox/ajax_workflow_update_person.php",{
			method : 'POST',
			body : params
		})
			.then(response => {

			});

	}


}