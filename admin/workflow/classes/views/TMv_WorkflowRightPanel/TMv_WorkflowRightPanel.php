<?php

/**
 * The class used in Tungsten 9 to manage workflows
 */
class TMv_WorkflowRightPanel extends TCv_View
{
	protected TMm_WorkflowModule $module;
	protected TCm_Model|TMt_WorkflowItem $model;
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TMv_WorkflowRightPanel');
		$this->addClassJSFile('TMv_WorkflowRightPanel');
		$this->addClassJSInit('TMv_WorkflowRightPanel');
		$this->module = TMm_WorkflowModule::init();
		$this->model = $this->module->currentModel();
		
		
	}
	
	public function render()
	{
		// No workflows and not an admin, we need to hide this entire interface
		if($this->module->currentUserIsSuper())
		{
			$settings_button = new TCv_Link('workflow_control_settings');
			$settings_button->addClass('control_button');
			$settings_button->setIconClassName('fa-cog');
			$settings_button->addDataValue('section', 'settings');
			
			// Control section tracking for show/hide
			$settings_button->addClass('control_section_settings');
			$settings_button->setURL('#');
			$this->attachView($settings_button);
		}
		
		// Attach the control bar
		$this->attachView($this->controlBar());
		
		$this->attachSubpanels();
		
		// Attach the editor
//		$workflow_editor = TMv_WorkflowEditor::init($this->module->currentModel());
//		$this->attachView($workflow_editor);
//
		
		
	}
	
	public function controlBar () : TCv_View
	{
		$control_bar = new TCv_View();
		$control_bar->addClass('workflow_control_bar');
		
//		$params = array();
//		if(isset($_GET['workflow-comments']))
//		{
//			$params['start_section'] = 'comments';
//		}
//		elseif(isset($_GET['workflow-people']))
//		{
//			$params['start_section'] = 'people';
//		}
//		elseif(isset($_GET['workflow-stages']))
//		{
//			$params['start_section'] = 'status';
//		}
		
	
		
		
		
		$num_enabled_workflow_items = 0;
		if($this->model)
		{
			foreach($this->model->workflowModelMatches() as $match)
			{
				if($match->isEnabled())
				{
					$workflow = $match->workflow();
					
					$workflow_button = new TCv_Link('workflow_switch_button_' . $workflow->id());
					$workflow_button->setURL('#');
					//$workflow_button->addText($workflow->title());
					$workflow_button->setIconClassName($workflow->iconCode());
					$workflow_button->addClass('control_button');
					$workflow_button->addDataValue('section', $workflow->id());
					$workflow_button->setTitle('Switch to ' . $workflow->title());
					$workflow_button->setAttribute('style',
					                               '
					                               border-color:#' . $workflow->color() . ';
					                               background-color:#' . $workflow->color() . ';
					                               color:#' . $workflow->readableColor() . ';
					                               
					                               '
					);
//					$workflow_button->addDataValue('code', $workflow->code());
//					$workflow_button->addDataValue('title', $workflow->title());
//					$workflow_button->addDataValue('color', $workflow->color());
//					$workflow_button->addDataValue('color-light', $workflow->colorLight());
					
					// Control section tracking for show/hide
					$workflow_button->addClass('control_section_'.$workflow->id());
					$control_bar->attachView($workflow_button);
					
				}
			}
		}
		
		
		
		return $control_bar;
		
	}
	
	public function attachSubpanels() : void
	{
		// Add for convenience
		$this->addClassCSSFile('TMv_WorkflowStageHistory');
		$this->addClassCSSFile('TMv_WorkflowHistoryItem');
		$this->addClassCSSFile('TMv_WorkflowComment');
		
		// TOP-LEVEL SETTINGS FOR THIS ITEM
		$settings_box = TMv_WorkflowSettingsBox::init();
		$settings_box->addClass('control_section_settings');
		$this->attachView($settings_box);
		
		
		// Add status for each workflow
		foreach($this->model->workflowModelMatches() as $workflow_match)
		{
			if($workflow_match->currentStage() && $workflow_match->isEnabled() )
			{
				$workflow = $workflow_match->workflow();
				$status_box =new TMv_WorkflowItemStatusBox($workflow_match);
				$status_box->setAttribute('style', 'border-color:#' . $workflow->color() . ';');
				
				// Control section tracking for show/hide
				$status_box->addClass('control_section_'.$workflow->id());
				$this->attachView($status_box);
			}
		}
		
		
	}
}