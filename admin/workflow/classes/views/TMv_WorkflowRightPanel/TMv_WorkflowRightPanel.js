class TMv_WorkflowRightPanel {
	element;
	workflow_editor;

	options = {
		model_name : false,
		scroll_timer : false,
		start_section : null
	};

	constructor(element, params) {
		this.element = element;
		this.workflow_editor = this.element.querySelector('.TMv_WorkflowEditor');

		// Update options
		Object.assign(this.options, params);

		// Try and get the first ID
		let workflow_view = sessionStorage.getItem('workflow_view');


		// Check for saved workflow that's been disabled
		if(workflow_view)
		{

			// Ensure that workflow shows try to find the button for that switch
			let button = this.element.querySelector('#workflow_switch_button_'+workflow_view);
			if(button === null)
			{
				workflow_view = null;
			}


		}

		// No default workflow, pick the first one
		if(workflow_view === null)
		{
			let button = this.element.querySelector('.workflow_control_bar .control_button');
			if(button !== null)
			{
				workflow_view = button.getAttribute('data-section');
			}

		}

		// SHOW THE CORRECT PANEL

		// NO workflow found, show the settings
		if(workflow_view === null)
		{
			this.showSection('settings');
		}
		else // workflow found, show that
		{
			// Ensure color settings are set
			this.showSection(workflow_view);

		}

		// add listeners

		this.element.querySelectorAll('.control_button').forEach(el => {
			el.addEventListener('click', (e) => {this.controlClicked(e); });
		});

		// this.element.querySelectorAll('.workflow_switch_button').forEach(el => {
		// 	el.addEventListener('click', (e) => {this.workflowButtonClicked(e); });
		// });



		// // No section set, default to "status"
		// if(this.options.start_section === null)
		// {
		// 	this.options.start_section = 'status';
		// 	let existing_value = sessionStorage.getItem('workflow_section_showing');
		// 	if(existing_value == null)
		// 	{
		// 		this.options.start_section = 'status';
		//
		// 	}
		// 	else
		// 	{
		// 		this.options.start_section = existing_value;
		//
		// 	}
		// }
		//
		// this.showSection(this.options.start_section);
		//
		//
		// // Deal with showing the workflow at all
		// let show_workflow = JSON.parse(sessionStorage.getItem('workflow_show'));
		// if(show_workflow === null)
		// {
		// 	sessionStorage.setItem('workflow_show', 'true');
		// 	show_workflow = true;
		// }
		//
		// if(show_workflow)
		// {
		// 	document.body.classList.add('showing_workflow');
		// 	this.workflow_editor.classList.add('showing');
		// }
		// else
		// {
		// 	document.body.classList.remove('showing_workflow');
		// 	this.workflow_editor.classList.remove('showing');
		// }



	}

	showSection(workflow_view) {

	//	console.log('show', workflow_view);
		// Save the value
		sessionStorage.setItem('workflow_view', workflow_view);

		// Turn it all off
		this.element.querySelectorAll('.control_button, .workflow_section_box').forEach(el => {
			el.classList.remove('showing');
		});

		this.element.querySelectorAll('.control_section_'+workflow_view).forEach(el => {
			el.classList.add('showing');
		});

	}


	/**
	 * Event handler when one of the control buttons is clicked to switch modes inside of a workflow
	 * @param event
	 */
	controlClicked(event)
	{
		event.preventDefault();

		let section_name = event.target.closest('.control_button').getAttribute('data-section');
		this.showSection(section_name);
	}

}