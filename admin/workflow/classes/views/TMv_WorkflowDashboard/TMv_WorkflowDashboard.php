<?php

/**
 * Class TMv_WorkflowDashboard
 *
 * A dashboard view that will be shown in Workflow module. It will basically function as a container/layout
 * to display multiple "workflow widgets" that can be viewed to get a summary of workflow progress.
 *
 * @author Katie Overwater
 */
class TMv_WorkflowDashboard extends TCv_View
{
	/**
	 * TMv_WorkflowDashboard constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addClassCSSFile('TMv_WorkflowDashboard');

	}

	/**
	 * Renders the view content
	 */
	public function render()
	{
		$class_names = TC_modelsWithTrait('TMt_WorkflowItem');

		foreach ($class_names as $class_name)
		{
			$this->attachWidget(new TMv_WorkflowDashboardWidget($class_name));
		}
	}

	/**
	 * Attaches a specified widget to the dashboard.
	 * Basic functionality is just extending `attachView()` function, but
	 * can be expanded if needed
	 *
	 * @param TCv_View|TCv_Link|TMv_WorkflowDashboardWidget $widget
	 */
	public function attachWidget($widget)
	{
		$this->attachView($widget);
	}

}