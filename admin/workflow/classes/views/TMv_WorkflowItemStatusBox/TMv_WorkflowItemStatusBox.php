<?php
class TMv_WorkflowItemStatusBox extends TCv_View
{
	protected $match;
	protected $model;
	
	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param TMm_WorkflowModelMatch $match
	 */
	public function __construct (TMm_WorkflowModelMatch $match)
	{
		$this->match = $match;
		$this->model = $match->model();
		
		// ID uses main workflow for matching
		parent::__construct('workflow_status_box_'.$this->match->workflow()->id());
		$this->addClass('workflow_section_box');
		$this->addDataValue('match-id',$this->match->id());
		
		$this->addClassCSSFile('TMv_WorkflowItemStatusBox');
		$this->addClassJSFile('TMv_WorkflowItemStatusBox');
		$this->addClassJSInit('TMv_WorkflowItemStatusBox');
		
		
	}

	public function render ()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText($this->match->workflow()->title());
		$this->attachView($heading);

		$stage = $this->match->currentStage();

		$stage_box = new TCv_View('stage_box_'.$this->match->id());
		$stage_box->addClass('stage_box');
		$stage_box->addDataValue('match-id', $this->match->id());
		$stage_box->setAttribute('style','background-color:#'.$stage->color().'; color:#'.$stage->readableColor().';');
		$stage_box->addText($stage->title());
		$this->attachView($stage_box);

		$this->attachView(new TMv_WorkflowStageProgressBar($this->match));

		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText('Move To Stage');
		$heading->addClass('move_to_stage_heading');
		$this->attachView($heading);

		// Attach the move-stage buttons
		$next_previous = new TMv_WorkflowNextPrevious($this->match);
		$this->attachView($next_previous);


		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText('History');
		$this->attachView($heading);

		foreach($this->match->stageHistory() as $model_stage)
		{
			$history_view = new TMv_WorkflowStageHistory($model_stage);
			$this->attachView($history_view);

		}
	}

}