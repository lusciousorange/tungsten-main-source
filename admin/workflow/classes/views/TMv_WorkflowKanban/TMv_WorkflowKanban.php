<?php
class TMv_WorkflowKanban extends TCv_View
{
	protected $workflow;
	protected $workflow_module;
	
	/**
	 * TMv_WorkflowKanban constructor.
	 * @param TMm_Workflow $workflow
	 */
	public function __construct($workflow)
	{
		parent::__construct();
		
		$this->addClassCSSFile('TMv_WorkflowKanban');
		
		$this->workflow = $workflow;
		$this->workflow_module = TMm_WorkflowModule::init();
		
		// Init the models for faster loading
		foreach($this->workflow->modelClassNames() as $class_name)
		{
			$class_name::allModels();
		}
		
		
		
	}
	
	public function render()
	{
		
		
		$inner_container = new TCv_View();
		$inner_container->addClass('inner_container');
		foreach($this->workflow->stages() as $stage)
		{
			$inner_container->attachView($this->stageView($stage));
		}
		
		$outer_container = new TCv_View();
		$outer_container->addClass('outer_container');
		$outer_container->attachView($inner_container);
		
		$this->attachView($outer_container);
	}
	
	/**
	 * @param TMm_WorkflowStage $stage
	 * @return TCv_View
	 */
	public function stageView(TMm_WorkflowStage $stage)
	{
		$stage_box = new TCv_View();
		$stage_box->addClass('stage_box');
		$stage_box->setAttribute('style','background-color:#'.$stage->colorLight().';');
		$stage_box->addDataValue('id',$stage->id());
		// Deal with identifying the next stages
		$next_stage_ids = [];
		
		// Super users can move from anywhere
		if($this->workflow_module->currentUserIsSuper())
		{
			$next_stage_ids[] = '*';
		}
		else
		{
			foreach($stage->nextStages() as $next_stage)
			{
				$next_stage_ids[] = $next_stage->id();
			}
		}
		$stage_box->addDataValue('next-stages', implode(',', $next_stage_ids));
		
		$stage_title_box = new TCv_View();
		$stage_title_box->addClass('stage_title_box');
		$stage_title_box->addText('<h2>'.$stage->title().'</h2>');
		$stage_title_box->setAttribute('style','background-color:#'.$stage->color().';color:#'.$stage->readableColor().';');
		
		$stage_box->attachView($stage_title_box);
		
		$stage_items = new TCv_View();
		$stage_items->addClass('stage_items');
		
		foreach($stage->modelsInStage() as $model)
		{
			$stage_items->attachView($this->workflowItemView($this->workflow, $model));
		}
		$stage_box->attachView($stage_items);
		
		return $stage_box;
	}

	/**
	 * @param TMm_Workflow $workflow
	 * @param TCm_Model|TMt_WorkflowItem $model
	 * @return bool|TSv_ModuleURLTargetLink
	 */
	public function workflowItemView(TMm_Workflow $workflow, $model)
	{
		$link = new TCv_Link();
		$link->setURL($model->adminEditURL());
		$link->setIconClassName($model->moduleForThisClass()->iconCode());
		$link->addClass('workflow_item_box');

		$link->addDataValue('current-stage-history', $model->workflowCurrentStageHistory($workflow)->id());
		
		$card_title = new TCv_View();
		$card_title->setTag('h3');
		$card_title->addText($model->title());
		$link->attachView($card_title);
		
		
		// ---- COMMENTS -----
		
		
		$comments = $model->workflowComments($this->workflow);
		$num_comments = sizeof($comments);
		
		
		$num_unresolved_comments = 0;
		foreach($comments as $comment)
		{
			if(!$comment->isResolved())
			{
				$num_unresolved_comments++;
			}
		}
		
		if($num_comments > 0)
		{
//
//			$box = new TCv_Link();
//			$box->setURL($this->edit_url . '?workflow-comments');
//			$box->addClass('detail_box');
//			$box->addClass('comment_box');
//			$box->setIconClassName('fa-comments');
//
//
//			if($num_unresolved_comments == 0)
//			{
//				$box->addText('All');
//			}
//			else
//			{
//				$box->addText(($num_comments - $num_unresolved_comments) . ' of ' . $num_comments);
//				$box->addClass('comment_action_required');
//			}
//
//			$box->addText(' Resolved');
			$icon_box = $this->iconBoxWithCount(
				'fa-comments',($num_comments - $num_unresolved_comments) .' / '.$num_comments);
			
			if($num_unresolved_comments > 0)
			{
				$icon_box->addClass('error');
			}
			
			$link->attachView($icon_box);
			
		}
		
		
		//$this->attachView($workflow_details_box);

		return $link;
	}
	
	public function iconBoxWithCount($icon_code, $count)
	{
		$box = new TCv_View();
		$box->addClass('icon_box');
		
		$box->addText('<i class="fas '.$icon_code.'"></i>');
		$box->addText('<span>'.$count.'</span>');
		
		return $box;
	}
}