<?php

/**
 * An email sent for a particular comment when it is added
 *
 * Class TMv_WorkflowCommentEmail
 */
class TMv_WorkflowCommentEmail extends TMv_WorkflowEmail
{
	/**
	 * TMv_WorkflowEmail constructor.
	 * @param TMm_WorkflowComment $comment
	 * @param TMm_User|bool $user (Optional) Default false
	 */
	public function __construct ($comment, $user = false)
	{
		parent::__construct($comment, $user);
		$this->setURLControlCode('workflow-comments');
		$this->setSubject('Workflow Comment : '.$this->workflow_item->typeTitle().' – ' . $this->workflow_item->model()->title());
	}


	/**
	 * A method to be overwritten to handle the specific content for this email. This content gets wrapped in the
	 * existing framework for all Workflow emails.
	 */
	public function attachMessageContent()
	{
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText($this->workflow_item->user()->fullName().' has added a new comment to the '.$this->workflow_item->typeTitle().' ');
			$link = new TCv_Link();
			$link->setURL($this->fullDomainName().$this->workflow_item->model()->adminEditURL().$this->url_control_code);
			$link->addText($this->workflow_item->model()->title());
			$p->attachView($link);
		$this->attachView($p);


		$p = new TCv_View();
		$p->setTag('p');
		$p->addClass('highlight_block');
		$p->addText('"'.$this->workflow_item->comment(). '"');
		$this->attachView($p);

	}


}

?>