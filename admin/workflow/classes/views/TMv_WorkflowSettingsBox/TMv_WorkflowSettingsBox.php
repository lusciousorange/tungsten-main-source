<?php
class TMv_WorkflowSettingsBox extends TCv_View
{
	protected TMm_WorkflowModule $workflow_module;
	
	/** @var TCm_Model|TMt_WorkflowItem $model */
	protected $model;
	
	public function __construct()
	{
		parent::__construct('workflow_settings_box');
		$this->addClass('workflow_section_box');
		
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->model = $this->workflow_module->currentModel();
		
		$this->addClassCSSFile('TMv_WorkflowSettingsBox');
	}
	
	public function render()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Workflow settings');
		$this->attachView($heading);
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText("Select which workflows apply to this item.");
		$this->attachView($explanation);
		
		foreach($this->workflow_module->workflows() as $workflow)
		{
			// Sets the background color on the whole link, then we can override as we need to
			$toggle_button_style = 'background-color:#'.$workflow->color().';';
			
			$workflow_toggle_button = new TCv_Link();
			$workflow_toggle_button->addClass('workflow_toggle_button');
			
			// Enabled the icon wrapping so we can style properly
			$workflow_toggle_button->wrapIconInContainer();
			
			// Determine if this workflow is enabled for this item
			$enabled = false;
			$match = $this->model->workflowMatch($workflow);
			if($match && $match->isEnabled())
			{
				$enabled = true;
			}
			
			// Create the title box which is always included
			$title_box = new TCv_View();
			$title_box->setTag('span');
			$title_box->addClass('title_box');
			$title_box->addText($workflow->title());
			$title_box->setAttribute('style','color:#'.$workflow->color().';');
			
			
			if($enabled)
			{
				$workflow_toggle_button->addClass('workflow_enabled');
				
				// Workflow specific styling
				$toggle_button_style .= 'border-color:#'.$workflow->color().';';
				
				// Set the URL to disable
				$workflow_toggle_button->setURL('/admin/workflow/do/disable-workflow-item/'.$match->id());
				
				// Set the icon
				$workflow_toggle_button->setIconClassName('fa-check');


//				$workflow_heading = new TCv_Link();
//				$workflow_heading->addClass('workflow_box_heading');
//				$workflow_heading->addText($workflow->title());
//				$workflow_heading->setAttribute('style','color:#'.$workflow->color().';');
//				$workflow_toggle_button->attachView($workflow_heading);

//				$disable_button = new TCv_Link();
//				$disable_button->addClass('disable_workflow_button');
//				$disable_button->addClass('workflow_setting_button');
//
//			//	$disable_button->setURL('/admin/workflow/do/disable-workflow-item/'.$match->id());
//				$workflow_box->attachView($disable_button);
			}
			else
			{
				$workflow_toggle_button->addClass('workflow_disabled');
				
				// Set the URL to enable
				$workflow_toggle_button->setURL(
					'/admin/workflow/do/add-workflow-to-model/'.
					$workflow->id().'/'.$this->model->baseClassName().'/'.$this->model->id());
				
				// Set the icon
				$workflow_toggle_button->setIconClassName('fa-plus');


//				$workflow_heading = new TCv_Link();
//				$workflow_heading->addClass('workflow_box_heading');
//				$workflow_heading->addText($workflow->title());
				//$workflow_box->attachView($workflow_heading);

//				$enable_button = new TCv_Link();
//				$enable_button->setIconClassName('fa-plus');
//				$enable_button->addClass('enable_workflow_button');
//				$enable_button->addClass('workflow_setting_button');
//				$enable_button->setURL(
//					'/admin/workflow/do/add-workflow-to-model/'.
//					$workflow->id().'/'.$this->model->baseClassName().'/'.$this->model->id());
//				$workflow_box->attachView($enable_button);
			}
			
			// Attach the title box
			$workflow_toggle_button->attachView($title_box);
			
			// Apply the style
			$workflow_toggle_button->setAttribute('style',$toggle_button_style);
			
			
			$this->attachView($workflow_toggle_button);
		}
		
	}
}