<?php
class TMv_FormItem_WorkflowColorPicker extends TCv_FormItem_RadioButtons
{
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->addClassCSSFile('TMv_FormItem_WorkflowColorPicker');
		
		$this->setHelpText("Select a color");
		
		$first = true;
		foreach(TMm_Workflow::$colors as $name => $code_values)
		{
			if($first)
			{
				$this->setDefaultValue($name);
				$first = false;
			}
			$this->addOption($name, ucfirst($name));
			$this->addOptionAttribute($name, 'style',
			                           'color:'.$code_values['text'].';'.
			                           ' background-color:'.$code_values['code']);

		}
		
		
	}
	
	
}