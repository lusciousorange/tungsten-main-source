<?php

/**
 * Class TMv_WorkflowDashboardWidget
 *
 * A single "widget" that can be displayed on the Workflow Dashboard. It is meant to contain a
 * visual representation of some Workflow information.
 *
 * This is the skeleton class, or template, that can be extended when creating widgets to keep the
 * layout and general features consistent across all workflow widgets.
 *
 * @author Katie Overwater
 */
class TMv_WorkflowDashboardWidget extends TCv_View
{
	protected $model_list = false;
	protected $class_name = false;

	/**
	 * TMv_WorkflowDashboardWidget constructor.
	 */
	public function __construct($class_name)
	{
		parent::__construct();

		$this->addClassCSSFile('TMv_WorkflowDashboardWidget');
		$this->class_name = $class_name;

		$title = new TCv_View();
		$title->addText($class_name::modelTitlePlural());
		$title->setTag('h2');
		$this->attachView($title);
	}

	/**
	 * Renders the view content
	 */
	public function render()
	{
		($this->class_name)::workflowModelMatchesByCategory();
		$without_category = ($this->class_name)::workflowModelMatchesWithoutCategory();

		// Need to show model name
		// Need to show workflow categories
			// Workflow categories need to have a progress bar with percentages per stage
		foreach (TMm_Workflow::$workflow_categories as $id => $name)
		{
			$percentages = ($this->class_name)::percentagePerStageForCategory($id);
			$this->createCategoryBox($percentages, $name);
		}

		foreach ($without_category as $id => $matches)
		{
			$workflow = TMm_Workflow::init($id);
			$percentages = ($this->class_name)::stagePercentageForWorkflow($id);
			$this->createCategoryBox($percentages, $workflow->title());
		}
	}

	public function createCategoryBox($percentages, $name)
	{

		if (count($percentages) > 0)
		{
			$cat_container = new TCv_View();
			$cat_container->addClass('cat_container');

			$title = new TCv_View();
			$title->addClass('cat_title');
			$title->setTag('h3');
			$title->addText($name);
			$cat_container->attachView($title);

			$status_box = new TCv_View();
			$status_box->addClass('status_box');

			foreach ($percentages as $stage_id => $percentage)
			{
				if ($percentage > 0)
				{
					$stage = TMm_WorkflowStage::init($stage_id);

					$percent_box = new TCv_View();
					$percent_box->addClass('percent_box');
					$percent_box->setAttribute('style', 'width:'.$percentage.'%;');

					$bar = new TCv_View();
					$bar->addClass('status_bar');
					$bar->setAttribute('style', 'background-color:#'.$stage->color().';');
					$percent_box->attachView($bar);

					$text = new TCv_View();
					$text->addClass('stage_name');
					$text->addText($stage->title());
					$percent_box->attachView($text);

					$text = new TCv_View();
					$text->addClass('stage_percent');
					$text->addText($percentage.'%');
					$percent_box->attachView($text);

					$status_box->attachView($percent_box);
				}
			}
			$cat_container->attachView($status_box);
			$this->attachView($cat_container);

		}
	}


}