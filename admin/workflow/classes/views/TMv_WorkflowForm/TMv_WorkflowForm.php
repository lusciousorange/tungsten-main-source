<?php
class TMv_WorkflowForm extends TCv_FormWithModel
{
	protected $workflow = false;

	/**
	 * TMv_WorkflowStageForm constructor.
	 * @param string|TMm_Workflow $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setSuccessURL('list');

		if ($this->isEditor())
		{
			$this->workflow = $model;
		}
		
		$this->addClassCSSFile();
		
	}

	

	public function configureFormElements()
	{

		
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TSv_FontAwesome_FormItem_SelectIcon('icon_code','Icon');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TMv_FormItem_WorkflowColorPicker('color', 'Color');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_TextBox('description', 'Description');
		$this->attachView($field);

		$field = new TCv_FormItem_Select('category', 'Category');
		$field->setHelpText("Helps with sorting the Workflows on the dashboard");
		$field->addOption('', 'No category');
		foreach (TMm_Workflow::$workflow_categories as $id => $value)
		{
			$field->addOption($id, $value);
		}
		$this->attachView($field);

		$stage_list = TMm_WorkflowStageList::init();
		$all_stages = $stage_list->models();
		$stages_checkbox_list = new TCv_FormItem_CheckboxList('stages', 'Stages');
		$stages_checkbox_list->setHelpText("Indicate the stages that will be used in this workflow.");
		$stages_checkbox_list->setSaveToDatabase(false);
		
		
		$selected_stage_ids = [];
		if($this->isEditor())
		{
			foreach($this->workflow->stages() as $selected_stage)
			{
				$selected_stage_ids[] = $selected_stage->id();
			}
		}
		
		foreach($all_stages as $stage)
		{
			$stage_view = new TCv_View();
			$stage_view->setTag('span');
			$stage_view->addClass('stage_color_box');
			$stage_view->setAttribute('style','background-color:#'.$stage->color().';color:#'.$stage->readableColor().';  ');
			$stage_view->addText($stage->title());
			
			$stages_checkbox_list->addCheckbox($stage->id(),
			                                   $stage_view,
			                                   in_array($stage->id(), $selected_stage_ids));
		}
		
		//$stages_checkbox_list->setValuesFromObjectArrays($all_stages, $selected_stages);
		$this->attachView($stages_checkbox_list);

		$all_models = TC_modelsWithTrait('TMt_WorkflowItem');
		$models = new TCv_FormItem_CheckboxList('models', 'Models');
		$models->setHelpText("Indicate the models that will use this workflow by default.");
		$models->setSaveToDatabase(false);
		
		// Store them in an array to avoid duplicates on extended classes
		$model_values = [];
		foreach ($all_models as $model)
		{
			$is_checked = false;
			if ($this->isEditor())
			{
				$is_checked = $this->workflow->isDefaultForModel($model::baseClass());
			}
			
			$model_values[$model::baseClass()] = [
				'title' => $model::modelTitlePlural(),
				'is_checked' => $is_checked];
		}
		
		// Add the checkboxes
		foreach($model_values as $model_name => $values)
		{
			$models->addCheckbox($model_name, $values['title'],$values['is_checked']);
		}
		$this->attachView($models);
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$hex = $form_processor->formValue('color');
		
		// Don't worry about uppercase, we'll deal with mostly names now
		$hex = str_ireplace('#', '', $hex);
		
		$form_processor->addDatabaseValue('color', $hex );
		
//		if(!preg_match('/^[0-9A-F]{6}$/i',$hex))
//		{
//			$form_processor->failFormItemWithID('color', 'Color must be exactly 6 HEX digits such as A5B2C3');
//		}
	}


	/**
	 * This function is called after the traditional form processor updateDatabase() function. A form instance can override
	 * this function to do custom form processing that doesn't fit within the standard operating proceedure for many Tungsten forms.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{

		/** @var TMm_Workflow $workflow */
		$workflow = $form_processor->model();

		if($form_processor->fieldIsSet('stages'))
		{
			$workflow->updateWorkflowStages($form_processor->formValue('stages'));
			
		}

		if($form_processor->fieldIsSet('models'))
		{
			$workflow->updateDefaultModels( $form_processor->formValue('models'));

		}

		// Detect if we're creating a workflow, at which point we create the default stages for every workflow
		if ($form_processor->isCreator())
		{
			$workflow->createDefaultStageMatches();
		}
	}


}