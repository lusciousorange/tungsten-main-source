<?php
class TMv_WorkflowStageProgressBar extends TCv_View
{
//	protected $workflow_module;
	
//	/** @var TMm_WorkflowStage $workflow_stage  */
//	protected $workflow_stage;
	protected $match;

	protected $check_svg = '';
	
	/**
	 * TMv_WorkflowStageProgressBar constructor.
	 * @param TMm_WorkflowModelMatch $match
	 */
	public function __construct(TMm_WorkflowModelMatch $match)
	{
//		$this->workflow_module = TMm_WorkflowModule::init();
		
		$this->match = $match;
		parent::__construct('workflow_progress_bar'.$match->id());
		
		$this->addClassCSSFile('TMv_WorkflowStageProgressBar');
		
		
		$svg_code = file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->classFolderFromRoot().'/check.svg');
		$this->check_svg = $svg_code;
		
	}
	
	public function render()
	{
		$done = true;
		foreach ($this->match->workflow()->stages() as $stage)
		{
			$dot = new TCv_View();
		//	$dot->setAttribute('title',$stage->title());
			$dot->addClass('stage_dot');
			$dot->setAttribute('style',
			                   'background-color:#'.$stage->color().';'.
			                         'color:#'.$stage->readableColor().';'.
			                         'border-color:#'.$stage->color().';');
			
			if($stage->id() == $this->match->currentStage()->id())
			{
				$dot->addClass('current_stage');
				$done = false; // those after this are not done
			}
			
			$inside = new TCv_View();
			$inside->addClass('inside');
			$inside->setAttribute('style','background-color:#'.$stage->color().';');
			$dot->attachView($inside);
			
			
			
			if($done)
			{
				$dot->addText($this->check_svg);
				$dot->addClass('done');
			}
			
			$dot->addDataValue('title',$stage->title());
			
			$this->attachView($dot);
		}
	}
}