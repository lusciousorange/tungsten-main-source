<?php

/**
 * A summary box that is used in model lists to show the status of the workflow for that item
 */
class TMv_WorkflowItemSummaryListColumn extends TCv_View
{
	/** @var TCm_Model|TMt_WorkflowItem $model  */
	protected TCm_Model $model;
	
	protected ?TMm_WorkflowModule $workflow_module;
	protected ?string $edit_url;
	
	public function __construct(TCm_Model $model)
	{
		parent::__construct();
		$this->addClass('workflow_container');
		
		$this->model = $model;
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->edit_url = $this->model->adminEditURL();
		
		$this->addClassCSSFile('TMv_WorkflowItemSummaryListColumn');
		
		
	}
	
	public function render()
	{
		// Don't allow them to modify workflows if they can't edit them
		if($this->model->userCanEdit())
		{
			
			// Loop through each workflow match to show the progress
			foreach($this->model->workflowModelMatches() as $match)
			{
				if($match->isEnabled())
				{
					//	$workflow_box = new TMv_WorkflowSummary($match);
					//	$this->attachView($workflow_box);
					
					$this->attachView($this->workflowMatchRow($match));
				}
			}
			
			$this->attachView($this->commentsRow());
		}
		
	}
	
	/**
	 * The row for a single workflow match
	 * @param TMm_WorkflowModelMatch $match
	 * @return TCv_View|null
	 */
	protected function workflowMatchRow(TMm_WorkflowModelMatch $workflow_match) : ?TCv_View
	{
		$summary = TMv_WorkflowSummary::init($workflow_match);
		return $summary;
		
	}
	
	/**
	 * The row for the comments
	 * @return TCv_View|null
	 */
	protected function commentsRow() : ?TCv_View
	{
		$comments = $this->model->workflowComments();
		$num_comments = sizeof($comments);
		
		if($num_comments > 0)
		{
			
			$num_unresolved_comments = 0;
			foreach($comments as $comment)
			{
				if(!$comment->isResolved())
				{
					$num_unresolved_comments++;
				}
			}
			
			$box = new TCv_Link();
			
			$box->setURL($this->edit_url . '?workflow-comments');
			$box->addClass('detail_box');
			$box->addClass('comment_box');
			$box->setIconClassName('fa-comments');
			
			$message = new TCv_View();
			$message->setTag('span');
			$message->addClass('title');
			
			$message->addText(($num_comments - $num_unresolved_comments) . ' of ' . $num_comments);
			$message->addText(' Resolved');
			
			if($num_unresolved_comments > 0)
			{
				$box->addClass('comment_action_required');
			}
			
			$box->attachView($message);
			$this->attachView($box);
			
		}
		
		return null;
	}
}