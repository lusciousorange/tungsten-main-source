<?php
$skip_tungsten_authentication = true; // api calls require their own auth
require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

//  CORS Header
header("Access-Control-Allow-Methods: POST,GET,DELETE,PATCH,OPTIONS");
header("Access-Control-Allow-Origin: ". TC_getConfig('api_cors_origin'));
header('Access-Control-Expose-Headers:'.TC_getConfig('api_cors_headers'));
header("Access-Control-Allow-Headers:".TC_getConfig('api_cors_headers'));
header("Access-Control-Max-Age: 86400");

// Ignore actual API call, for CORS preflight
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{
	exit();
}


// Clear process errors just in case a previous call didn't work
// Sessions should be cleared, but if actual PHP errors are hit, then they are not
TC_clearProcessErrors();

// Call the API
$controller = TSc_APIController::init();
$controller->parseAPICall();
