<?php
	/**********************************************
	 
	 This is a general process script that only deals with 
	 process scripts that may or may not be loaded through tungsten or hte pages side.
	 It resolves any confusion based on the referrer and loads the appropriate 
	 header script to ensure consistency.
	 
	 **********************************************/
	require_once($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_config.php");


    /**********************************************/
	/*										   	  */
	/*          CHECK FOR AUTH SKIPPING           */
	/*										   	  */
	/**********************************************/
	// Only needs to happen when processing a form
	// Only triggers when using the built in form processing system
	if(isset($_POST['tracker_form_class']))
	{
		$tracker_form_class = $_POST['tracker_form_class'];
		if($tracker_form_class != '')
		{
			if($tracker_form_class::customFormProcessor_skipAuthentication())
			{
				$skip_tungsten_authentication = true;
			}
		}
	}

	$referrer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
	$sections = explode("/",$referrer); // split the requested url

	if(TC_isTungstenView())
	{
        require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");
	}
	else
	{
		$skip_tungsten_authentication = true;
		require($_SERVER['DOCUMENT_ROOT']."/admin/pages/headers/pages_header.php");
	
	}


	
	
	
?>