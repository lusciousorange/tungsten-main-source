<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_config.php");
	
	//////////////////////////////////////////////////////
	//
	// TUNGSTEN INSTANCE
	//
	//////////////////////////////////////////////////////
	
	$tungsten_class_name = TSv_Tungsten::tungstenClassName();
	$website = $tungsten_class_name::website();
	
	
	if(!@$skip_tungsten_connection)
	{
		if($website->urlSection(1) == 'install')
		{
			$skip_tungsten_authentication = !isset($_SESSION['tungsten_user_id']);
			
		}
		$website->setSkipAuthentication(@$skip_tungsten_authentication);
		//$website->setCompressCSS();
		//$website->setCompressJS();
		
		//////////////////////////////////////////////////////
		//
		// AUTHENTICATE USER GROUP
		// authenticate to ensure they can view this page
		//
		//////////////////////////////////////////////////////
		$website->authenticateSession();
		
		//////////////////////////////////////////////////////
		//
		// AUTHENTICATE USER GROUP
		//
		//////////////////////////////////////////////////////
		$website->checkModuleAccess();
		
		//////////////////////////////////////////////////////
		//
		// MODULE CSS
		//
		//////////////////////////////////////////////////////
		$website->addModuleCSS();
		
	}
	
	// backwards compatibility
	$tungsten = $website;
