<?php 
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	/** @var TSv_Tungsten $website */

	/** @var TSm_Module $module */
	$module = TC_website()->currentModule();
	if($module)
	{
		$controller = $module->controller();
	
		$views = $controller->views();
		$headers = $views['headers'];
		$main_content = $views['main_content'];
		
		if(TC_getConfig('use_tungsten_9'))
		{
			$website->setMainContentView($main_content);
		}
		else
		{
			foreach($headers as $view)
			{
				$website->attachToSubNavigationView($view);
			}
			foreach($main_content as $view)
			{
				$website->attachToMainContentView($view);
			}
		}
		
		
		$html = $website->html();
		
		echo $html;
		
		TMm_BenchmarkEntry::instance()?->trackPrintTimes();
		
	}
	
?>
