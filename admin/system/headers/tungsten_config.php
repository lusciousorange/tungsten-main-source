<?php
	$start_microtime = microtime(true);

	// Trim the trailing slash from the document root. Just in case
	$_SERVER['DOCUMENT_ROOT'] = rtrim($_SERVER['DOCUMENT_ROOT'],'/');

	// CLI – Command line scenarios where server values aren't set
	if(!isset($_SERVER['HTTP_USER_AGENT']))
	{
		$_SERVER['HTTP_USER_AGENT'] = 'cli';
	}

	// CLI – Request URL set to the file path
	if(!isset($_SERVER['REQUEST_URI']))
	{
		$_SERVER['REQUEST_URI'] = __FILE__;
	}


	include_once($_SERVER['DOCUMENT_ROOT']."/TC_config.php"); // Grab the config file. Everything start there.
	include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/config/module_init_config.php"); // Grab the config file. Everything start there.
	include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/config/staging_config.php"); // Grab the config file. Everything start there.

	$TC_config['timer_start'] =  $start_microtime;
	$benchmark_start_time = $start_microtime;
	
	//////////////////////////////////////////////////////
	//
	// DEAL WITH DISABLED
	//
	//////////////////////////////////////////////////////
	if(@$TC_config['website_disabled'] === true)
	{
		print 'Website Disabled';
		exit();
	}



	//////////////////////////////////////////////////////
	//
	// SAVE TO GLOBALS VARIABLE
	//
	//////////////////////////////////////////////////////

	$GLOBALS['TC_config'] = $TC_config;
	$GLOBALS['TC_classes'] = array();
	$GLOBALS['TC_module_configs'] = array();

	if($GLOBALS['TC_config']['show_errors'])
	{
		$error_levels = $GLOBALS['TC_config']['show_errors'];
		
		// Detect legacy value of boolean true
		if($error_levels === true || $error_levels === 1)
		{
			// Disable notices which can be triggered in lots of ways
			// via how Tungsten loads values in classes
			$error_levels = E_ALL &  ~ E_NOTICE;;
		}
		ini_set('display_errors','1'); 
		error_reporting($error_levels);
	}
	else
	{
		ini_set('display_errors','0'); 
		error_reporting(0);
		
	}

	//////////////////////////////////////////////////////
	//
	// AUTOLOAD CLASSES
	//
	// Various classes are loaded automatically via
	// $GLOBALS['TC_config']['class_loaders']
	//
	// More can be added if necessary
	//
	//////////////////////////////////////////////////////

	require_once($_SERVER['DOCUMENT_ROOT'].$GLOBALS['TC_config']['TCore_path']."/AutoLoading/autoload_classes.php"); // Various classes are

	//////////////////////////////////////////////////////
	//
	// STAT SESSION
	//
	//////////////////////////////////////////////////////
	session_start();

	//////////////////////////////////////////////////////
	//
	// BASIC TUNGSTEN CORE FUNCTIONS
	//
	//////////////////////////////////////////////////////
	require_once($_SERVER['DOCUMENT_ROOT'].$GLOBALS['TC_config']['TCore_path']."/System/basic_TC_functions.php");



	//////////////////////////////////////////////////////
	//
	// BASIC TUNGSTEN CORE FUNCTIONS
	//
	//////////////////////////////////////////////////////
	TMm_BenchmarkEntry::instance([
		'url' => $_SERVER['REQUEST_URI'], // if not set, use the file path
		'start_microtime' => $benchmark_start_time*1000,
		'session_start_microtime' => microtime(true)*1000,
	                             ]);

	//////////////////////////////////////////////////////
	//
	// CHECK FOR AN UPGRADE
	//
	//////////////////////////////////////////////////////
	//TCv_Website::checkForUpgrade();
	// Disabled for speed concerns

	//////////////////////////////////////////////////////
	//
	// CHECK USER-SPECIFIC ERROR REPORTING
	//
	//////////////////////////////////////////////////////
	if($current_user = TC_currentUser())
	{
		$current_user->configureErrorReporting();
	}
	
	//////////////////////////////////////////////////////
	//
	// START THE CONSOLE
	//
	//////////////////////////////////////////////////////
	$console_page_start = new TSm_ConsoleItem('Page Start', TSm_ConsolePageStart);
	$console_page_start->commit();
	//*/

	//////////////////////////////////////////////////////
	//
	// SESSION TRACKING
	// Uniquely generated based on the microtime and a
	// random collection of letters/numbers
	// required for histories
	//
	//////////////////////////////////////////////////////
	if(TC_getConfig('use_track_model_history'))
	{
		if($current_user = TC_currentUser())
		{
			$current_user->setAsDatabaseSessionID();
		}
		
		$session_version = TCu_Text::generatePassword(6) . '.' . round($start_microtime * 10000);
		$model = new TCm_Model(0);
		$model->DB_Prep_Exec('SET @session_version = "'.$session_version.'"');
	}

?>