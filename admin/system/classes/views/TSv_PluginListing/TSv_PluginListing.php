<?php
/**
 * Class TSv_PluginListing
 */
class TSv_PluginListing extends TCv_RearrangableModelList
{
	protected $plugin_list = false;
	protected $plugin_views = array();
	
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TSm_Plugin');

		$plugin_list = TSm_PluginList::init();
		$this->setModels($plugin_list->allPlugins());
//		$this->setModelListClassMethodName('allPlugins');
		$this->defineColumns();
		$this->addClassCSSFile('TSv_PluginListing');

		$this->attachStagingNotice();
	}

	/**
	 * Attaches the notice that shows the staging setup
	 */
	private function attachStagingNotice()
	{
		$explanation = new TCv_View();
		$explanation->addClass('beta_explanation');


		if(TC_getConfig('is_localhost'))
		{
			$explanation->addText('<strong>Localhost:</strong> Testing on a localhost, defaults to Staging settings. ');

		}
		elseif(TC_getConfig('staging_exists'))
		{
			if(TC_getConfig('is_staging_domain'))
			{
				$explanation->addText('<strong>Staging Website:</strong> enabled for the domain currently being viewed : ');
			}
			else
			{
				$explanation->addText('<strong>Live Website:</strong> enabled for the domain. Staging domain set to ');

			}
			$explanation->addText('<em>'.TC_getConfig('staging_domain').'</em>');
		}
		else
		{
			$explanation->addText('<strong>Live Website:</strong> No staging domain has been set. ');

		}

		if(TC_getConfig('is_staging_domain'))
		{
			$this->addClass('staging_domain_highlight');
		}
		else
		{
			$this->addClass('live_domain_highlight');
		}


		$this->attachView($explanation);
	}


	/**
	 *
	 */
	public function defineColumns()
	{
		$icon = new TCv_ListColumn('icon');
		$icon->setWidthAsPixels(40);
		$icon->setTitle('');
		$icon->setContentUsingListMethod('pluginIconColumn');
		$icon->setAlignment('center');
		$this->addTCListColumn($icon);
		
		$title = new TCv_ListColumn('title');
		//$title->setWidthAsPercentage(25);
		$title->setTitle('');
		$title->setContentUsingListMethod('pluginTitleColumn');
		$this->addTCListColumn($title);
		
		$item = new TCv_ListColumn('wait_for_rendering');
		$item->setWidthAsPixels(110);
		$item->setTitle('Wait For Render');
		$item->setAlignment('center');
		$item->setContentUsingListMethod('pluginWaitForRenderingColumn');
		$this->addTCListColumn($item);
		
		$item = new TCv_ListColumn('public');
		$item->setWidthAsPixels(70);
		$item->setTitle('Public');
		$item->setAlignment('center');
		$item->setContentUsingListMethod('pluginPublicColumn');
		$this->addTCListColumn($item);
		
		$item = new TCv_ListColumn('admin');
		$item->setWidthAsPixels(70);
		$item->setTitle('Admin');
		$item->setAlignment('center');
		$item->setContentUsingListMethod('pluginAdminColumn');

		$this->addTCListColumn($item);
		
		$item = new TCv_ListColumn('live');
		$item->setWidthAsPixels(70);
		$item->setTitle('Live');
		$item->setAlignment('center');
		$item->setContentUsingListMethod('pluginLiveSiteColumn');
		$this->addTCListColumn($item);
		
		$item = new TCv_ListColumn('beta');
		$item->setWidthAsPixels(70);
		$item->setTitle('Staging');
		$item->setAlignment('center');
		$item->setContentUsingListMethod('pluginBetaSiteColumn');
		$this->addTCListColumn($item);
		
		
		$edit_button = new TCv_ListColumn('edit_button');
		$edit_button->setVerticalAlignment('middle');
		$edit_button->setAlignment('center');
		$edit_button->setContentUsingListMethod('editIconColumn');
		$edit_button->setWidthAsPixels(50);
		$this->addTCListColumn($edit_button);
	
		
	}
	
	/**
	 * @param TSm_Plugin $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		if($model->isInstalled())
		{
			$row->addClass('installed');
		}
		else
		{
			$row->addClass('not_installed');
		}
		
		return $row;
			
	}
	
	
	/**
	 * @param TSm_Plugin $model
	 * @return TCv_View
	 */
	public function viewForModel($model)
	{
		if($model instanceof TSm_Plugin)
		{	
			if(!isset($this->plugin_views[$model->className()]))
			{
				$this->plugin_views[$model->className()] = ($model->className())::init($model);
			}
			
			return $this->plugin_views[$model->className()];
		}
		return new TCv_View();
	}
	
	/**
	 * @param bool $is_on
	 * @return TCv_Link
	 */
	public function onOffIndicatorLink($is_on)
	{
		$indicator = new TCv_Link();
		$indicator->addClass('fa-fw');
		$indicator->addClass('list_control_button');
		if($is_on)
		{
			$indicator->setIconClassName('fas fa-check-circle');
		}
		else
		{
			$indicator->setIconClassName('fas fa-circle');
		}
		
		return $indicator;
	}
	
	/**
	 * @return TCv_View
	 */
	public function requiredIndicator()
	{
		$indicator = new TCv_View();
		$indicator->addClass('list_control_button');
		$indicator->addClass('fa-check-circle');
		$indicator->addClass('required');
		return $indicator;
	}
	
	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginIconColumn($model)
	{
		$link = new TCv_View();
		$link->setTag('i');
		$link->addClass($this->viewForModel($model)->pluginIconName());
		$link->addClass('list_control_button');
		
		return $link;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginTitleColumn($model)
	{
		$title_box = new TCv_View();
		$title_box->setTag('span');
			
			if($model->isInstalled())
			{
				$link = new TCv_Link();
				$link->setURL('/admin/system/do/plugin-edit/'.$model->id());
			}
			else
			{
				$link = new TCv_View();
				
			}
			$link->addText('<strong>'.$this->viewForModel($model)->pluginTitle().'</strong>');
			$title_box->attachView($link);
			
			$description = new TCv_View();
			$description->setTag('span');
			$description->addClass('plugin_description');
			$description->addText($this->viewForModel($model)->pluginDescription());
			$title_box->attachView($description);
			
			
		return $title_box;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginWaitForRenderingColumn($model)
	{
		if(!$model->isInstalled())
		{
			return new TCv_View();
		}
		$link = $this->onOffIndicatorLink($model->waitsForRendering());
		$link->setURL('/admin/system/do/plugin-toggle-wait-for-rendering/'.$model->id());
		return $link;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginPublicColumn($model)
	{
		if(!$model->isInstalled())
		{
			return new TCv_View();
		}
		if($model->viewClass()->publicRequired())
		{
			$link = $this->requiredIndicator();
		}
		else
		{
			$link = $this->onOffIndicatorLink($model->enabledOnPublic());
			$link->setURL('/admin/system/do/plugin-toggle-enabled-on-public/'.$model->id());
			
		}
		return $link;
	}





	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginAdminColumn($model)
	{
		if(!$model->isInstalled())
		{
			return new TCv_View();
		}
		if($model->viewClass()->adminRequired())
		{
			$link = $this->requiredIndicator();
		}
		else
		{
			$link = $this->onOffIndicatorLink($model->enabledOnAdmin());
			$link->setURL('/admin/system/do/plugin-toggle-enabled-on-admin/'.$model->id());
		}
		return $link;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginLiveSiteColumn($model)
	{
		if(!$model->isInstalled())
		{
			return new TCv_View();
		}
		if($model->viewClass()->liveRequired())
		{
			$link = $this->requiredIndicator();
		}
		else
		{
			$link = $this->onOffIndicatorLink($model->enabledOnLiveSite());
			$link->setURL('/admin/system/do/plugin-toggle-enabled-on-live-site/'.$model->id());
		}
		return $link;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function pluginBetaSiteColumn($model)
	{
		if(!$model->isInstalled())
		{
			return new TCv_View();
		}
		if($model->viewClass()->betaRequired())
		{
			$link = $this->requiredIndicator();
		}
		else
		{
			$link = $this->onOffIndicatorLink($model->enabledOnBetaSite());
			$link->setURL('/admin/system/do/plugin-toggle-enabled-on-beta-site/'.$model->id());
		}
		return $link;
	}

	/**
	 * @param TSm_Plugin $model
	 * @return string|TCv_View
	 */
	public function editIconColumn($model)
	{
		$link = new TCv_Link();
		$link->addClass('list_control_button');
		if($model->isInstalled())
		{
			if($model->viewClass()->deletable())
			{
				$link->setTitle('Un-Install this plugin');
				$link->setIconClassName('fa-times');
				$link->setURL('/admin/system/do/plugin-uninstall/'.$model->className());
			}
			
		}
		else
		{
			$link->setTitle('Install this plugin');
			$link->setIconClassName('fa-plus');
			$link->setURL('/admin/system/do/plugin-install/'.$model->className());
		}
		return $link;
	}
	
	/**
	 * @return TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('The plugins available for this website. ');
			$help_view->attachView($p);
			
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('Plugins are pieces of code that are loaded <strong>site-wide</strong> instead of on a particular page. They primarily exist for loading 3rd-party website integration or code libraries.  ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Public and Admin</strong>: Plugins can be configured to load on either the ');
				$link = new TSv_HelpLink('public website', '.TCv_ListColumn_public .list_control_button');
				$p->attachView($link);
			$p->addText(', the ');
				$link = new TSv_HelpLink('admin side', '.TCv_ListColumn_admin .list_control_button');
				$p->attachView($link);
			$p->addText(' or both. The "public" website is considered anything that isn\'t this admin interface you\'re using right now.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Live and Beta</strong>: The website determines automatically if a website is ');
				$link = new TSv_HelpLink('live', '.TCv_ListColumn_live .list_control_button');
				$p->attachView($link);
			$p->addText(' or ');
				$link = new TSv_HelpLink('beta', '.TCv_ListColumn_beta .list_control_button');
				$p->attachView($link);
			$p->addText(' based on the ');
				$link = new TSv_HelpLink('System Settings\'', '.url-target-settings');
				$p->attachView($link);
			$p->addText(' beta URL. ');
			$help_view->attachView($p);
					
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Wait For Render</strong>: Click the ');
				$link = new TSv_HelpLink('Wait For Render', '.TCv_ListColumn_wait_for_rendering .list_control_button');
				$p->attachView($link);
			$p->addText(' button if the plugin should only be loaded after the website has finished loading. ');
			$help_view->attachView($p);
					



			return $help_view;
	}
	
	
		
}
?>