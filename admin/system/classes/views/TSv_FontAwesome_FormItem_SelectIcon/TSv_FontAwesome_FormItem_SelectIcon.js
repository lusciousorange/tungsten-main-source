class TSv_FontAwesome_FormItem_SelectIcon {
	element;

	constructor(element) {
		this.element = element;

		this.element.addEventListener('change', this.fieldChanged.bind(this));
		this.fieldChanged();

	}

	/**
	 * Triggered when a search field is changed. handle keystrokes and potential ajax call on timeout
	 */
	fieldChanged() {

		let container = this.element.closest('.form_item_container').querySelector('.fa_preview_container');
		container.innerHTML = '';
		if(this.element.value !== '')
		{
			let icon = document.createElement('i');
			icon.className = this.element.value;
			container.append(icon);

		}
	};
}