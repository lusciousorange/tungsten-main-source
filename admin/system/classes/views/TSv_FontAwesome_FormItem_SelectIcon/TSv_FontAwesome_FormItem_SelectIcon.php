<?php

/**
 * Class TSv_FontAwesome_FormItem_SelectIcon
 *
 * A specialized Select field that shows the icons for FontAwesome when selected
 */
class TSv_FontAwesome_FormItem_SelectIcon extends TCv_FormItem_Select
{
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		// Add CSS and JS
		$this->addClassCSSFile('TSv_FontAwesome_FormItem_SelectIcon');
		
		$this->addClassJSFile('TSv_FontAwesome_FormItem_SelectIcon');
		$this->addClassJSInit('TSv_FontAwesome_FormItem_SelectIcon');
		
		// Enable filtering since there are always hundreds
		$this->useFiltering();
		
		// Add the blank field
		$this->setEmptyTitle('Select an icon');
		
		// Loop through and find the icons available on this server
		$directory = dir($_SERVER['DOCUMENT_ROOT'].$this->classFolderFromRoot('TSv_FontAwesome').
		                 '/'.TSv_FontAwesome::$library_folder.'/svgs/solid/');
		while($file = $directory->read())
		{
			if(strpos($file, '.svg') > 0)
			{
				$name = str_replace('.svg', '', $file);
				
				$readable = ucwords(str_replace('-', ' ', $name));
				
				$this->addOption('fas fa-'.$name, $readable);
			}
		}
		
		// Find the brand icons
		$directory = dir($_SERVER['DOCUMENT_ROOT'].$this->classFolderFromRoot('TSv_FontAwesome').
		                 '/'.TSv_FontAwesome::$library_folder.'/svgs/brands/');
		while($file = $directory->read())
		{
			if(strpos($file, '.svg') > 0)
			{
				$name = str_replace('.svg', '', $file);
				
				$readable = ucwords(str_replace('-', ' ', $name));
				
				$this->addOption('fab fa-'.$name, $readable);
			}
		}
		
		// Add the preview container
		$icon_container = new TCv_View();
		$icon_container->addClass('fa_preview_container');
		
		$this->attachViewAfter($icon_container);
		
		
	}
	
	public function setEmptyTitle($title)
	{
		$this->addOption('',$title);
		
	}
}