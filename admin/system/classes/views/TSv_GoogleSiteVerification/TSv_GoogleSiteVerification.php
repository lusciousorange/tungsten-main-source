<?php
/**
 * Class TSv_GoogleSiteVerification
 *
 * Adds Google Site Verification to the entire site.
 */
class TSv_GoogleSiteVerification extends TSv_Plugin
{
	/**
	 * TSv_GoogleSiteVerification constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Google Site Verification');
		$this->setPluginDescription('A plugin that adds the google-site-verification metatag that lets google know you have access.');
		$this->setPluginIconName('fab fa-google');
		$this->enableVariableForNum(1, 'Verification Number','');
		

		
	}
	
	/**
	 *
	 */
	public function html()
	{
		if($this->pluginVariable(1) != '')
		{
			$this->addMetaTag('google_site_verification', array('name' => 'google-site-verification', 'content' => $this->pluginVariable(1)));
	
		}		

	}
	
	
}
?>