<?php
/**
 * Class TSv_ModuleSettingsForm
 *
 * A class for modifying the settings for a that module
 */
class TSv_ModuleSettingsForm extends TCv_FormWithModel
{
	/**
	 * TSv_ModuleSettingsForm constructor.
	 *
	 * @param string|TSm_Module $module
	 */
	public function __construct($module)
	{	
		if(!($module instanceof TSm_Module))
		{
			$module = TC_website()->currentModule();
		}
		
		// DETECT TUNGSTEN 9 redirect
		// Loading this page on its own, needs to redirect to the settings list
		// This is done by looking for the return_type of JSON, if it's missing, it's trying to load the page
		if(TC_getConfig('use_tungsten_9'))
		{
			if(!isset($_GET['return_type']) || $_GET['return_type'] != 'json')
			{
				header("Location: /admin/system/do/settings-list/#".$module->folder()); exit();
			}
		}
		
		
		parent::__construct($module);
		
		$this->setButtonText('Update settings');
		$this->setIsEditor(true);
		
		// In tungsten 9, submit should always be attached to the root
		if(TC_getConfig('use_tungsten_9'))
		{
			$this->setSubmitToAttachToRoot();
			
			// Add the module title
			
			$heading = new TCv_View('module_heading_title');
			$heading->addText('<i class="fas '.$module->iconCode().'"></i>');
			
			$heading->addText($module->title());
			$this->attachToFormRoot($heading);
			
		}
	
	}
	
	/**
	 * Extend the view for the title column to add in the developer button
	 * @param TCv_FormItem $form_item
	 * @return TCv_View|null
	 */
	public function titleColumnView($form_item)
	{
		$view = parent::titleColumnView($form_item);
		if(TC_currentUserIsDeveloper())
		{
			$view->attachView($this->developerCodeTextView($form_item));
		}
		return $view;
		
	}
	
	/**
	 * Extend the help text view to add the developer values
	 * @param $form_item
	 * @return TCv_View|null
	 */
	public function developerCodeTextView($form_item)
	{
		$container = new TCv_View();
		$container->addClass('help_text_container');
		$container->addClass('developer_code_container');
		
		$link = new TCv_Link();
		$link->addClass('help_icon_button');
		$link->setAttribute('tabindex', '-1'); // avoid tabbing on help icons
		$link->setURL('#');
		$link->setIconClassName('fas fa-code');
		$link->setTitle('Code details');
		
		$container->attachView($link);
		
		$text = new TCv_View();
		$text->addClass('form_info_text ');
		$text->addText($form_item->id());
		$container->attachView($text);
		
		return $container;
	}
	

	/**
	 * Extend this function to add the other values that can be called from the form
	 */
	public function formStartup()
	{
		
		// This shows a lot of developer related items that just get in the way
		if(TC_currentUserIsDeveloper())
		{
			// Get the settings for the module we're looking at
			$module_settings = $this->model()->moduleSettings();
			$settings_class_name = get_class($module_settings);
			
			// Check if we have a custom module setting
			if($settings_class_name != 'TSm_ModuleSettings')
			{
				$all_methods = get_class_methods($settings_class_name);
				
				$parent_class = get_parent_class($settings_class_name);
				
				// Check if we're extending one level
				if($parent_class != 'TSm_ModuleSettings')
				{
					$all_methods = array_unique(array_merge($all_methods, get_class_methods($parent_class)));
					$parent_class = get_parent_class($parent_class); // Go one more level deep
				}
				
				if($parent_class)
				{
					$parent_methods = get_class_methods($parent_class);
					$relevant_methods = array_diff($all_methods, $parent_methods);
				}
				else
				{
					$relevant_methods = $all_methods;
				}
				
				if(count($relevant_methods) > 0)
				{
					// Add a Heading
					$field = new TCv_FormItem_Heading('methods_heading', 'Calculated content codes');
					$field->addHelpText('These are calculated codes that can be used on the website. They are based
				on other settings in the system and are defined by a programmer for public use. 
				<br /><br/>Usage: {{ <i>module_folder.code</i> }} where the module_folder is what you see in the URL after "admin"
				 (eg: /admin/module_folder/do/) 
				and the code is the light grey text below each item. ');
					
					parent::attachView($field);
					
					foreach($relevant_methods as $method_name)
					{
						$title = preg_replace('/(?<!\ )[A-Z]/', ' $0', ucwords($method_name));
						
						$field = new TCv_FormItem_HTML($method_name.'()', $title);
						$field->preventLayoutStacking();
						$value = TC_getModuleConfig($this->moduleForThisClass()
						                                 ->folder(), $method_name);
						if(is_bool($value))
						{
							$value = $value ? 'True' : 'False';
						}
						$field->addText($value);
//						$code_view = new TCv_View();
//						$code_view->addClass('setting_code');
//						$code_view->addText('<i class="fal fa-code"></i> ' . $method_name . '');
//						$field->attachViewAfter($code_view);
//
						parent::attachView($field);
					}
				}
				
			}
		}
		
		parent::formStartup();
		
	}



	/**
	 * Extend functionality of the attachView method to set the default value to pull from the module values
	 * @param bool|TCv_FormItem|TCv_View $form_item
	 * @param bool $prepend
	 */
	public function attachView($form_item, $prepend = false)
	{
		// Don't attach for non-form items or items that don't have fields
		if(
			$form_item instanceof TCv_FormItem &&
			!($form_item instanceof TCv_FormItem_Hidden) &&
			!($form_item instanceof TCv_FormItem_Heading) &&
			!($form_item instanceof TCv_FormItem_HTML)

		)
		{
			
			$form_item->setEditorValue($this->model()->variable($form_item->id()));
		}
		
		// Deal with groups having values that require editor settings
		elseif($form_item instanceof TCv_FormItem_Group)
		{
			foreach($form_item->formItems() as $group_form_item)
			{
				$group_form_item->setEditorValue($this->model()->variable($group_form_item->id()));
			}
		}
		parent::attachView($form_item, $prepend);
		$this->appendLocalizedFields($form_item, $prepend);
		
	}
	
	/**
	 * This method detects if we're using localization and appends the additional fields for the given form item.
	 * Module settings are defined in the localization.php file in the config for the module.
	 * @param TCv_FormItem $form_item
	 * @param bool $prepend
	 */
	protected function appendLocalizedFields($form_item, $prepend = false)
	{
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			$module_folder = $this->model()->moduleForThisClass()->folder();
			$localization = TMm_LocalizationModule::init();
			
			if($form_item instanceof TCv_FormItem_Group)
			{
				foreach($form_item->formItems() as $position => $group_form_item)
				{
					if($localization->isModuleSettingLocalized($module_folder, $group_form_item->id()))
					{
						foreach($localization->nonDefaultLanguages() as $language => $language_settings)
						{
							$lang_field = $this->localizedFormItemForLanguage($group_form_item, $language_settings);
							
							// Attach the item to the group
							$form_item->attachView($lang_field);
							
						}
					}
				}
			}
			else // Normal field
			{
				if($localization->isModuleSettingLocalized($module_folder, $form_item->id()))
				{
					foreach($localization->nonDefaultLanguages() as $language => $language_settings)
					{
						$lang_field = $this->localizedFormItemForLanguage($form_item, $language_settings);
						
						parent::attachView($lang_field, $prepend);
						
					}
				}
			}
			
		}
	}
	
	/**
	 * @param TCv_FormItem $form_item The form item to be localized
	 * @param array $language_settings The language settings to localize
	 * @return TCv_FormItem
	 */
	protected function localizedFormItemForLanguage($form_item, $language_settings)
	{
		$language = $language_settings['code'];
		$form_item_language = clone $form_item;
		$form_item_language->setID($form_item_language->id().'_'.$language);
		$form_item_language->setTitle($form_item_language->title().' ('.$language_settings['title'].')');
		$form_item_language->addClass('localization');
		$form_item_language->addClass('localization_'.$language);
		$form_item_language->addClass($form_item->id().'_row');
		if($default_value_localized = $form_item->defaultValueLocalized($language))
		{
			$form_item_language->setDefaultValue($default_value_localized);
		}
		
		// values aren't saved in normal space, so we need to pull them differently
		// uses saved value to override the editor value coming from the $form_item
		$form_item_language->setSavedValue( $this->model()->variable($form_item_language->id()) );
		
		// Attach the code
		$form_item_language->clearViewsAfter();
		//$this->appendCodeToFormItem($form_item_language);
		
		return $form_item_language;
	}
	
	/**
	 * Pulls the values to be install for the variables
	 * @return array
	 */
	public function installValues()
	{
		$values = array();
		$this->configureFormElements();
		foreach($this->formItems() as $form_item)
		{
			if(!$form_item instanceof TCv_FormItem_Hidden && $form_item instanceof TCv_FormItem)
			{
				$values[$form_item->id()] = $form_item->defaultValue();
			}
		}
		
		return $values;
	}
	
	/**
	 * Extend the primary DB action to disable it
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return bool
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		return false;
	}
	
	
	/**
	 * This function is called after the traditional form processor processFormItems() but before the updateDatabase() function. A form instance can override this function to do custom form processing that doesn't fit within the standard operating proceedure for many Tungsten forms.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		TC_messageConditionalTitle('The settings where updated.', true);
		TC_messageConditionalTitle('The settings could not be updated.', false);
	
		/** @var TSm_Module $module */
		$module = $form_processor->model();
		$module->updateVariables($form_processor->submittedSavableValues());
	}
	
	/**
	 * Extend the classname function to determine the module, if one exists
	 * @param ...$args
	 * @return string
	 */
	public static function classNameForInit(...$args)
	{
		$module = TC_activeModelWithClassName('TSm_Module');
		
		// If we don't have a module AND we're NOT in tungsten 9, load the v8 current module
		// These are loading in the module, not in system
		if(!$module && !TC_getConfig('use_tungsten_9'))
		{
			$module = TC_website()->currentModule();
		}
		
		// Try and find the variable class form name
		if($module && $module->variableFormClassName())
		{
			$form_name = $module->variableFormClassName();
			
			// Check for overrides
			return static::overrideClassName($form_name);
		}
		
		return 'TSv_ModuleSettingsForm';
		
	}
	
	
}
?>