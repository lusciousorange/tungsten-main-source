<?php

/**
 * Class TSv_License
 */
class TSv_License extends TCv_View
{
	/**
	 * TSv_License constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * @return string
	 */
	public function html()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('The MIT License (MIT)');
		$this->attachView($heading);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Copyright (c) '.date('Y').' Luscious Orange');
		$this->attachView($p);
		
	
	
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:');
	$this->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.');
		$this->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.');
		$this->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->setAttribute('style', 'border-top:1px solid #666;padding-top:10px;');
		$p->addText('The above copyright ONLY includes to the core Tungsten infrastructure which consists of the following folders contained within the <em>admin</em> folder: <br />dashboard, images, login, pages, profile, system, TCore, users.');
		$this->attachView($p);
		

		return parent::html();
	}
}
?>