<?php

/**
 * Class TMv_ContentFlagButton
 *
 * A button for content flags which indicate the number of flags and a buttoon to learn more
 */
class TMv_ContentFlagButton extends TCv_Link
{
	protected $model;
	
	/**
	 * TMv_ContentFlagButton constructor.
	 * @param TMt_ContentFlaggable|TCm_Model $model
	 */
	public function __construct($model)
	{
		parent::__construct();
		$this->model = $model;
		
		$this->addClassCSSFile('TMv_ContentFlagButton');
	}
	
	/**
	 * Render the button
	 */
	public function render()
	{
		// Not a model, get out
		if(!$this->model instanceof TCm_Model)
		{
			return;
		}
		
		if($this->model->hasContentFlags())
		{
			
			$this->setURL('#');
			$this->setTitle('Flags');
			$this->setIconClassName('fa-triangle');
			
			$grouped_by_type = [];
			foreach($this->model->contentFlags() as $content_flag)
			{
				$grouped_by_type[$content_flag->type()][] = $content_flag->message();
				
			}
			
			
			// Generate the messages, including grouping multiples together for convenience
			$flag_strings = [];
			foreach($grouped_by_type as $type => $messages)
			{
				$main_message = $messages[0];
				// Multiples
				if(count($grouped_by_type[$type]) > 1)
				{
					$flag_strings[] = $main_message.' (x'.count($grouped_by_type[$type]).')';
				}
				else
				{
					$flag_strings[] = $main_message;
				}
			}
			
			// Add the dialog popup
			$this->useDialog(implode('<br />', $flag_strings),'fa-exclamation-triangle', '#d21696');
			$this->setDialogCancelText(''); // hide the cancel button
			// Add the count
			$flag_count = new TCv_View();
			$flag_count->setTag('i'); // use i to avoid span conflicts
			$flag_count->addClass('number');
			$flag_count->addText($this->model->numContentFlags());
			$this->attachView($flag_count);
		}
		else
		{
			$this->setTag('span');
		}
		
		$this->addClass('flags_button');
	}
	
}