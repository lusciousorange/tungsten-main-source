<?php
class TSv_PopupContainer extends TCv_View
{
	protected $popup_inside = false;
	
	public function __construct($id = false)
	{	
		parent::__construct($id);
		
		$this->addClassCSSFile('TSv_PopupContainer');
		
		$this->popup_inside = new TCv_View();
		$this->popup_inside->addClass('popup_inside');
	}
	
	public function attachView($view, $prepend = false)
	{
		$this->popup_inside->attachView($view, $prepend);
	}

	public function html()
	{
		parent::attachView($this->popup_inside);
		
		return parent::html();
	}
	
}
?>