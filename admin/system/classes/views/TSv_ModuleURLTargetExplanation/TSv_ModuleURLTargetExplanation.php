<?php
/**
 * Class TSv_ModuleURLTargetExplanation
 */
class TSv_ModuleURLTargetExplanation extends TCv_View
{
	/** @var bool|TSm_ModuleURLTarget $url_target */
	protected $url_target = false;

	/**
	 * TSv_ModuleURLTargetExplanation constructor.
	 *
	 * @param bool $url_target
	 */
	public function __construct($url_target)
	{
		parent::__construct();
		$this->url_target = $url_target;
		
		$this->addClassCSSFile();
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		$p = new TCv_View();
		$p->setTag('h2');
		$p->addText('URL Target: '.$this->url_target->pathWithExplanation());
		$this->attachView($p);
		
		
		if($this->url_target->modelInstanceRequired())
		{
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('This <em>URL Target</em> requires a <strong>'.$this->url_target->variableNameForID().'</strong> to be provided through the URL in order to properly load.');
			$this->attachView($p);
		
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('The provided '.$this->url_target->variableNameForID().' will be used to instantiate a <strong>'.$this->url_target->modelName().'</strong> <em>$model</em>.');
			$this->attachView($p);
		
			if($this->url_target->isView())
			{
				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('That model will be used to create the view <strong>'.$this->url_target->viewName().'(</strong><em>$model</em><strong>)</strong>.');
				$this->attachView($p);

				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('That view will be shown on this page.');
				$this->attachView($p);

			}
			elseif($this->url_target->modelActionMethod())
			{
				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('The system will then call <strong>$model->'.$this->url_target->modelActionMethod().'()</strong>.');
				$this->attachView($p);
			}
		
		}
		else
		{
			if($this->url_target->isView())
			{
				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('The view <strong>'.$this->url_target->viewName().'()</strong> will be created and shown on this page. It requires no input values.');
				$this->attachView($p);

			}
			
		}
		
		// Handle Rediret
		if($this->url_target->modelActionMethod())
		{
			$p = new TCv_View();
			$p->setTag('p');
			//$p->addText('After the action is completed, the browser will be redirected to the URL Target with the name <strong>'.$this->url_target->nextURLTarget().'</strong>. ');
			$this->attachView($p);
		}
		
		$p = new TCv_View();
		$p->setTag('h2');
		$p->addText('Validation');
		$this->attachView($p);
		
		foreach($this->url_target->validationCalls() as $call_values)
		{
			$validation_model_name = $call_values['model_name'];
			$validation_method_name = $call_values['method_name'];
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('This URL target will only show views or perform actions if the method <em>'.$validation_model_name.'->'.$validation_method_name.'()</em> returns true.');
			$this->attachView($p);
			
//			$p = new TCv_View();
//			$p->setTag('p');
//			$p->addText('For navigation items that are being displayed on other URL targets, the system will check if that URL target has an instance of <em>'.$validation_model_name.'</em> that exists. If so, it will call <em>'.$validation_model_name.'()</em> to check for validation. Any other scenario will mark this URL Target as invalid and it will not appear in the navigation.');
//			$this->attachView($p);
			
		}
		
	
		
			
		if($parent_url = $this->url_target->parentURLTarget())
		{
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('Parent Details');
			$this->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('Parent URL Targets are used to create relationships between URL Targets. It also controls the navigation breadcrumbs and buttons.');
			$this->attachView($p);
			
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('This URL Target has a parent with the name <strong>'.$parent_url->name().'</strong> and URL of <strong>'.$parent_url->pathWithExplanation().'</strong>. ');
			$this->attachView($p);
			
			if($parent_url->modelInstanceRequired() && $parent_url->modelName() != $this->url_target->modelName())
			{
				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('The parent requires a model (<strong>'.$parent_url->modelName().'</strong>) that is different from the one needed for this URL Target (<strong>'.$this->url_target->modelName().'</strong>).  ');
				$this->attachView($p);
				
				$p = new TCv_View();
				$p->setTag('p');
				$p->addText('You have indicated that the method <strong>'.$this->url_target->parentModelMethodName().'()</strong> exists in the class <strong>'.$this->url_target->modelName().'</strong> AND that it returns a single <strong>'.$parent_url->modelName().'</strong> model. This ensures that this URL Target has a way to reference the parent model. ');
				$this->attachView($p);
				
			}
		
		
		}		
		
		return parent::html();
	}
}
?>