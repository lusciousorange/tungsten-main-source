<?php
/**
 * Class TSv_AccessDenied
 * A view that shows that the page the person is tried to view is not allowed.
 */
class TSv_AccessDenied extends TCv_View
{
	protected $main_title = 'Access Denied'; // [string] = The main title for this content
	protected $explanation = 'You are not permitted to view this page. Please use the back button or the available navigation to return to a section of the website for which you have access.';
	protected $additional_content = false;
	
	/**
	 * TSv_AccessDenied constructor.
	 *
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->additional_content = new TCv_View();
		$this->additional_content->addClass('additional_content');
		$this->addConsoleWarning('Access Denied');
	}
	
	/**
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return void
	 */
	public function attachView($view, $prepend = false)
	{
		$this->additional_content->attachView($view,$prepend);
	}
	
	/**
	 * Sets the main title
	 * @param string $title
	 */
	public function setMainTitle($title)
	{
		$this->main_title = $title;
	}
	
	/**
	 * Sets the explanation
	 * @param string $explanation
	 */
	public function setExplanation($explanation)
	{
		$this->explanation = $explanation;
	}

	/**
	 * @return string
	 */
	public function html()
	{
		$this->addClassCSSFile('TSv_AccessDenied');
	
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addClass('main_title');
		$heading->addText($this->main_title);
		parent::attachView($heading);
		
		$explanation = new TCv_View();
		$explanation->addClass('explanation');
		$explanation->addText($this->explanation);
		parent::attachView($explanation);
		
		parent::attachView($this->additional_content);
		return parent::html();
	}

	public function setAsBlue()
	{
		$this->addClass('blue');
	}
	
}	
?>