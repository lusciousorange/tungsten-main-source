<?php
class TSv_ComponentHelpBox extends TCv_View
{
	protected ?TSm_ComponentHelp $help_component = null;
	
	public function __construct(TSm_ComponentHelp $help_component)
	{
		parent::__construct();
		
		$this->addClassCSSFile('TSv_ComponentHelpBox');
		$this->help_component = $help_component;
		
	}
	
	public function rightPanelUtilityView() : TCv_View
	{
		$link = new TCv_ToggleTargetLink();
		$link->addText('Edit');
		$link->setURL('#');
		$link->addClassToggle('#help_box_container','showing');
		$link->addClassToggle('.TSv_ComponentHelpForm','showing');
		$link->addClassToggle('#right_panel_help','editor_mode');
		return $link;
		
	}
	
	public function render()
	{
		parent::render();
		
		$help_container = new TCv_View('help_box_container');
		$help_container->addClass('showing');
		$help_container->addText($this->help_component->helpHTML());
		$this->attachView($help_container);
		
		if(TC_currentUser()->isAdmin())
		{
			$help_form = new TSv_ComponentHelpForm($this->help_component);
			$this->attachView($help_form);
		}
	}
	
}