<?php
use League\CommonMark\CommonMarkConverter;

class TSv_Tungsten9Panels extends TCv_View
{
	protected $url_target;
	
	protected ?TCv_View $left_panel, $center_panel, $right_panel;
	
	protected ?TCv_View $right_panels_container, $right_panel_icon_bar;
	protected int $num_right_panels = 0;
	
	public function __construct($id = false)
	{
		parent::__construct('tungsten_9_panels');
		
		// variables to pass in
		$this->url_target = TC_currentURLTarget();
		$init_values = [
			'url_target_name' => $this->url_target->name(),
			'module_name' => $this->url_target->module()->folder()
		];
		$this->addClassCSSFile('TSv_Tungsten9Panels');
		
		$this->addClassJSFile('TSv_Tungsten9Panels');
		$this->addClassJSInit('TSv_Tungsten9Panels',$init_values);
		
		// Track right panels, since they might load a few different ways
		$this->right_panels_container = new TCv_View('panel_container');
		
		$this->right_panel_icon_bar = new TCv_View('panel_icons');
		
		
	}
	
	/**
	 * Sets the left panel view
	 * @param TCv_View $view
	 */
	public function setLeftPanelView($view)
	{
		$view = $this->configureFormView($view);
		
		$this->left_panel = $view;
	}
	
	/**
	 * Sets the left panel view
	 * @param TCv_View $view
	 */
	public function setCenterPanelView($view)
	{
		$view->addClass('primary_content');
		
		$view = $this->configureFormView($view);
		$view = $this->configureListView($view);
		
		$this->center_panel = $view;
		
		
	}
	
	/**
	 * Sets the right panel view
	 
	 */
	public function configureRightPanelView()
	{
		$this->right_panel = $this->rightPanelView();
	}
	
	/**
	 * Configures and generates the right panels based on the current URL target values
	 * @return TCv_View
	 */
	public function rightPanelView()
	{
		$module = TC_currentModule();
		$controller = $module->controller();
		
		// Generate a fake right panel
		$right_panel = new TCv_View('tungsten_right_panel');
		
		// Add right panels
		foreach($this->url_target->rightPanelURLTargets() as $right_panel_target)
		{
			// Validate access
			// We should only show right panels that can be shown
			if($right_panel_target->validateAccess())
			{
				// Generate the view
				$model = TC_activeModelWithClassName($right_panel_target->modelName());
				$view = $controller->viewForURLTarget($right_panel_target, $model);
				$view = $this->configureFormView($view);
				
				$this->addRightPanel($right_panel_target->title(true),
									 $right_panel_target->rightPanelIcon(),
									 $right_panel_target->name(), $view);
			}
			
		}
		
		$this->detectWorkflowRightPanel();
		$this->detectHelpRightPanel();
		
		
		
		// Handle localization switching buttons
//
//		if(TC_getConfig('use_localization'))
//		{
//			// Only bother for page content views, regular Admin views don't need it
//
//			if(TC_currentUser() && TC_classUsesTrait($url_target->viewName(),'TMt_PagesContentView'))
//			{
//				// @TODO : test this
//				$language_selector = TMv_Localization_TungstenLanguageSelector::init($url_target);
//				$icon_bar->attachView($language_selector);
//			}
//
//		}
		
		$right_panel->attachView($this->right_panel_icon_bar);
		$right_panel->attachView($this->right_panels_container);
		
		if($this->num_right_panels > 0)
		{
			return $right_panel;
		}
		
		return null;
		
	}
	
	protected function addRightPanel(string $title,
	                                 string $icon,
	                                 string $code,
	                                 TCv_View $panel_view) : void
	{
		// Add the icon to the icon bar
		$button = new TCv_Link('right_panel_button_'.$code);
		$button->setTitle($title);
		$button->setIconClassName($icon);
		$button->setHREF('#');
		$button->addDataValue('target',$code);
		$button->addClass('panel_button');
		$this->right_panel_icon_bar->attachView($button);
		
		// Create a panel for this URL target
		$right_panel_view = new TCv_View('right_panel_'.$code);
		$right_panel_view->addClass('right_panel_view');
		
		$heading_container = new TCv_View();
		$heading_container->addClass('right_panel_heading_container');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addClass("right_panel_title");
		$heading->addText($title);
		$heading_container->attachView($heading);
		
		// Detect and show the optional utility view for right panels
		if(method_exists($panel_view,'rightPanelUtilityView'))
		{
			$utility_view = $panel_view->rightPanelUtilityView();
			if($utility_view instanceof TCv_View)
			{
				$utility_view->addClass('right_panel_utility_view');
				$heading_container->attachView($utility_view);
			}
		}
		$right_panel_view->attachView($heading_container);
		
		// Attach the panel view itself
		$right_panel_view->attachView($panel_view);
		
		// Attach to the container
		$this->right_panels_container->attachView($right_panel_view);
		
		// Track the number of panels
		$this->num_right_panels++;
		
	}
	
	/**
	 * Detects if help text exists and adds it to the right panel
	 * @return void
	 */
	protected function detectHelpRightPanel() : void
	{
		$component_help = TSm_ComponentHelp::findHelpForViewName($this->url_target->viewName());
		
		if($component_help instanceof TSm_ComponentHelp)
		{
			$help_view = new TSv_ComponentHelpBox($component_help);
		

			$this->addRightPanel(TC_localize('help','Help'),
			                     'fa-question',
			                     'help',
			                     $help_view);
		}
		
		
	}
	
	/**
	 * Detects if help text exists and adds it to the right panel
	 * @return void
	 */
	protected function detectWorkflowRightPanel() : void
	{
		if(TC_moduleWithNameInstalled('workflow'))
		{
			$workflow_module = TMm_WorkflowModule::init();
			if(TC_getConfig('use_workflow') && $workflow_module->currentModel())
			{
				$workflow_view = new TMv_WorkflowRightPanel();
				
				
				$this->addRightPanel(TC_localize('workflows','Workflows'),
				                     'fa-project-diagram',
				                     'workflows',
				                     $workflow_view);
				
				$this->addRightPanel(TC_localize('comments','Comments'),
				                     'fa-comments',
				                     'comments',
				                     TMv_WorkflowItemCommentBox::init());
			}
			
			
			
			
		}
		
		
		
	}
	
	/**
	 * Renders the view
	 */
	public function render()
	{
		// LEFT PANEL
		$left_panel = new TCv_View();
		$left_panel->addClass('left_panel');
		$left_panel->attachView($this->left_panel);
		
		// Add expand/collapse controls
		$control_bar = new TCv_Link();
		$control_bar->addClass('expand_button');
		$control_bar->setIconClassName('fal fa-arrow-left');
		$control_bar->setURL('#');
		$control_bar->setTitle('Expand left panel');
		$left_panel->attachView($control_bar);
		
		// ------------------------------------
		
		// CENTER PANEL
		$center_panel = new TCv_View('tungsten_main_content');
		$center_panel->setTag('main');
		$center_panel->addClass('center_panel');
		$center_panel->addClass('tungsten_main_content');
		
		// Add views to center panel
		
		// Adding the main header
		$container = new TCv_View('main_header');
		$container->attachView(TSv_Tungsten9_BreadcrumbBar::init());
		$container->attachView(TSv_Tungsten9_SubmenuBar::init());
		$center_panel->attachView($container);
		
		// Adding messenger
		$center_panel->attachView(TCv_Messenger::instance());
		
		// Adding center panel content
		$center_panel->attachView($this->center_panel);
		
		
		// ------------------------------------
		
		// RIGHT PANEL
		if($this->right_panel)
		{
			// LEFT PANEL
			$right_panel = new TCv_View();
			$right_panel->addClass('right_panel');
			$right_panel->attachView($this->right_panel);
			
			$control_bar = new TCv_Link();
			$control_bar->addClass('expand_button');
			$control_bar->setIconClassName('fal fa-arrow-right');
			$control_bar->setTitle('Expand right panel');
			$control_bar->setURL('#');
			$right_panel->attachView($control_bar);
		}
		
		// ------------------------------------
		
		// ATTACH
		
		if(TC_currentUser())
		{
			// Attach the three panels
			$this->attachView($left_panel);
		}
		
		$this->attachView($center_panel);
		
		// RIGHT PANEL, only visible if logged in
		if(TC_currentUser() && isset($right_panel))
		{
			$this->attachView($right_panel);
		}
		
		
	}
	
	/**
	 * Configures a view to be properly setup for being in Tungsten.
	 * @param TCv_View $view
	 * @return TCv_View
	 */
	protected function configureFormView($view) : TCv_View
	{
		
		// Detect if we're setting a view and configure the submit to render outside the form table
		// Necessary for styling the view in Tungsten's panels
		if($view instanceof TCv_Form)
		{
			if($view->submitButtonIconCode() === null)
			{
				$view->setSubmitButtonIconCode('fa-check');
			}
			
			$view->setSubmitToAttachToRoot();
		}
		
		return $view;
	}
	
	
	/**
	 * Configures a view to be properly setup for being in Tungsten.
	 * @param TCv_View $view
	 * @return TCv_View
	 */
	protected function configureListView($view) : TCv_View
	{
		
		// Detect if we're setting a view and it's a searchable model lists
		// The scrolling keeps the pagination, so we always want to turn off the footer pagination
		if($view instanceof TCv_SearchableModelList)
		{
			$view->hideFooterPagination();
		}
		
		return $view;
	}
	
}