/**
 * JS Class that corresponds to the Tungsten panels view. Used to manage collapsing of side panels in the interface.
 */
class TSv_Tungsten9Panels
{

	constructor(element, init_values)
	{
		this.element = element;
		this.url_target_name = init_values['url_target_name'];
		this.module_name = init_values['module_name'];
		this.center_panel = this.element.querySelector(".center_panel");
		this.right_panel = this.element.querySelector(".right_panel");

		// A name used to easily reference a value in local storage
		this.panel_storage_name = 'tungsten_right_panel_selected_' + this.module_name + '_' + this.url_target_name;

		this.initLeftPanel();
		this.initRightPanel();


	}

	/**
	 * Initializes the left panel
	 */
	initLeftPanel()
	{
		this.left_panel = this.element.querySelector(".left_panel");

		if(this.left_panel)
		{
			this.left_panel.classList.add('init');
			this.left_panel.querySelector('.expand_button').addEventListener('click',this.toggleLeftPanel.bind(this));

			// Check if it's loading as collapsed
			let is_collapsed = localStorage.getItem('tungsten_left_panel_collapsed') === 'true';
			if(is_collapsed)
			{
				this.left_panel.classList.add('collapsed');
			}
		}
	}

	/**
	 * Toggles the left panel with an click event
	 * @param event
	 */
	toggleLeftPanel(event) {

		// Toggle if it's collapsed
		this.left_panel.classList.remove('init');
		this.left_panel.classList.toggle('collapsed');

		// Save the value to local storage
		localStorage.setItem('tungsten_left_panel_collapsed', this.left_panel.classList.contains('collapsed'));
	}

	/**
	 * Initializes the left panel
	 */
	initRightPanel()
	{
		this.right_panel = this.element.querySelector(".right_panel");

		if(this.right_panel)
		{
			this.right_panel.classList.add('init');
			this.right_panel.querySelector('.expand_button').addEventListener('click',this.toggleRightPanel.bind(this));

			// Check if it's loading as collapsed
			let is_collapsed = localStorage.getItem('tungsten_right_panel_collapsed') === 'true';
			if(is_collapsed)
			{
				this.right_panel.classList.add('collapsed');
			}

			// Add the listener for buttons
			let buttons = this.element.querySelectorAll('.right_panel .panel_button');

			for(let button of buttons)
			{
				button.addEventListener('click',this.changeRightPanel.bind(this));

			}

			let first_button = buttons[0];

			let saved_panel_name = localStorage.getItem(this.panel_storage_name);

			// NO saved panel name OR we can't find the panel, use the first one
			if(!saved_panel_name || !this.element.querySelector('#right_panel_' + saved_panel_name))
			{
				localStorage.setItem(this.panel_storage_name, first_button.getAttribute('data-target'));
			}

			// Load the relevant panel no matter what, don't force the expansion
			this.selectRightPanel(localStorage.getItem(this.panel_storage_name), false);
		}
	}

	/**
	 * Toggles the right panel with an click event
	 * @param event
	 */
	toggleRightPanel(event)
	{

		// Toggle if it's collapsed
		this.right_panel.classList.remove('init');
		this.right_panel.classList.toggle('collapsed');

		// Save the value to local storage
		localStorage.setItem('tungsten_right_panel_collapsed', this.right_panel.classList.contains('collapsed'));
	}

	/**
	 * Forces the right panel to be shown
	 */
	showRightPanel()
	{
		// Toggle if it's collapsed
		this.right_panel.classList.remove('init');
		this.right_panel.classList.remove('collapsed');

		// Save the value to local storage
		localStorage.setItem('tungsten_right_panel_collapsed', false);
	}

	/**
	 * Triggered when someone clicks on a right button
	 * @param {Event} event
	 */
	changeRightPanel(event) {
		let button = event.currentTarget;

		let panel_name = button.getAttribute('data-target');

		this.selectRightPanel(panel_name);

		event.preventDefault();

	}

	/**
	 * Selects a panel with a given name
	 * @param {string} panel_name
	 * @param {boolean} expand_right_panel
	 */
	selectRightPanel(panel_name, expand_right_panel = true)
	{
		this.right_panel.querySelectorAll('.panel_button').forEach(el => {
			el.classList.remove('selected');
		});

		this.right_panel.querySelector('#right_panel_button_' + panel_name).classList.add('selected');

		this.right_panel.querySelectorAll('.right_panel_view').forEach(el => {
			el.hide();
		});

		this.right_panel.querySelector('#right_panel_' + panel_name).show('block');

		// save to local storage
		localStorage.setItem(this.panel_storage_name, panel_name);

		// Show the panel, just in case it's collapsed
		if(expand_right_panel)
		{
			this.showRightPanel();
		}

	}

	/**
	 * Returns the array of buttons
	 * @return {NodeListOf<Element>}
	 */
	panelButtons() {
		return this.right_panel.querySelectorAll('.panel_button');
	}


}