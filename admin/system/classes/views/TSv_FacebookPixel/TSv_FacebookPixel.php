<?php

/**
 * The facebook pixel
 */
class TSv_FacebookPixel extends TSv_TrackingPixel
{
	protected static $pixel_name = 'Facebook';
	protected static $pixel_icon = 'fab fa-facebook';
	protected static $track_function_name = 'fbq';
	
	
	protected static $standard_events = ['purchase'];
	
	/**
	 * TSv_FacebookPixel constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Facebook Pixel');
		$this->setPluginDescription('Installs a Facebook pixel on the site for tracking and analysis.');
		$this->setPluginIconName('fab fa-facebook');
		$this->enableVariableForNum(1, 'Tracking ID','');
		$this->setDefaultInstallValue('enable_admin', 0);
		$this->setDefaultInstallValue('enable_beta_site', 1);
		
		// Correct if it's set to not wait for rendering, needs it in case a view or theme sets a gtag value
		if($plugin_model && !$plugin_model->waitsForRendering())
		{
			$plugin_model->updateWithValues(['wait_to_load_on_rendering' => 1]);
		}
	}
	
	/**
	 *
	 */
	public function html()
	{
		$this->addMetaTag('fb_preconnect','<link rel="preconnect" href="https://connect.facebook.net">');
		
		// STEP 1 : Load the fb tag function
		$this->addJSLine('fbtag',
		                 "\n// FACEBOOK PIXEL \n!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');", false);
		
		
		
		// Only pass user data if it exists
		$user_data_string = '';
		$user_data = $this->getUserData();
		if(count($user_data) > 0)
		{
			$user_data_string = ",".json_encode($user_data);
		}
		
		// STEP 2: Trigger events
		$this->addJSLine('fbtag_init',
		                 "fbq('init','" . $this->pluginVariable(1)."'" .$user_data_string.");", false);
		$this->addJSLine('fbtag_track_PageView',
		                 "fbq('track','PageView');", false);
		
		
		// STEP 3 : User properties as events
		if($user = TC_currentUser())
		{
			$values = $user->googleAnalyticsUserProperties();
			foreach($values as $code => $value)
			{
				if($value == 'Yes')
				{
					static::trackEvent($code);
				}
			}
			
		}
		
		$this->addEventTrackingCalls();
		
		
	}
	
	protected function getUserData()
	{
		// @see https://developers.facebook.com/docs/meta-pixel/advanced/advanced-matching
		// Build user data
		$values = [];
		if($user = TC_currentUser())
		{
			$sha256_email = hash('sha256', strtolower($user->email()));
			$values['em'] = $sha256_email;
			
			if(method_exists($user,'gender'))
			{
				$gender = strtolower(substr($user->gender(),0,1));
				
				// Sometimes stored as numbers
				if($gender === '1')
				{
					$gender = 'm';
				}
				elseif($gender === '0')
				{
					$gender = 'f';
				}
				$values['ge'] = $gender;
			}
			
//			if(method_exists($user,'birthday') && $user->birthday() != '')
//			{
//				$values['db'] = str_replace('-','',$user->birthday());
//
//			}
			
			if(method_exists($user,'postalCode') && $user->postalCode() != '')
			{
				$values['zp'] = strtolower(str_replace(['-', ' '], '', $user->postalCode()));
			}
			
//			if(method_exists($user,'city') && $user->city() != '')
//			{
//				$values['ct'] = strtolower(str_replace(['-', ' '], '', $user->city()));
//			}
//			if(method_exists($user,'province') && $user->province() != '')
//			{
//				$values['st'] = strtolower($user->province());
//			}
//			if(method_exists($user,'country') && $user->country() != '')
//			{
//				$values['st'] = strtolower($user->country());
//			}
		}
		
		return $values;
	}
	
	
	/**
	 * Extend the event-call function to determine the method names
	 * Returns the JS for the event call for the pixel. This is a single line of JS that is run for each event.
	 * @param string $event_name
	 * @param array $values
	 * @param string $method_name
	 * @return string
	 */
	public static function eventCall($event_name, $values = [], $method_name = 'track' )
	{
		// Determine if we're tracking a custom event or a standard one
		if(!in_array(strtolower($event_name), static::$standard_events))
		{
			$method_name = 'trackCustom';
		}
		
		return parent::eventCall($event_name, $values, $method_name);
	}
	
	/**
	 * Adds a value to track for the fbtag for a "event" command. Each event will be a separate fbtag with the
	 * values for that event call. If multiple tags are set for the same event, it overwrites the previous event call.
	 *
	 * This method can be triggered in the middle of script and the next time it loads, it will add that to the fbtag
	 * for the page.
	 *
	 * @param string $event_name The name of the event being called
	 * @param array|string $values The values to be passed.If it's an array, it will merge it with other values to
	 * create a unified object. If it's a string, it will append to JSON for output
	 * @param bool $return_as_script_tag Indicates if the values should be immediately returned as a script tag
	 *
	 * @return null|string
	 */
//	public static function trackEvent($event_name, $values = [], $return_as_script_tag = false )
//	{
//
//		// Determine if we're tracking a custom event or a standard one
//		$track_type = 'trackCustom';
//		if(in_array(strtolower($event_name), static::$standard_events))
//		{
//			$track_type = 'track';
//		}
//
//		// Check if the session value exists
//		if(!isset($_SESSION['fbtag']))
//		{
//			$_SESSION['fbtag'] =[];
//		}
//		// Check if the $type array
//		if(!isset($_SESSION['fbtag']['event']))
//		{
//			$_SESSION['fbtag']['event'] = [];
//		}
//
//		if($return_as_script_tag)
//		{
//			return '<script>fbq("'.$track_type.'","'.$event_name.'",'.json_encode($values).' )</script>';
//		}
//		else
//		{
//			$_SESSION['fbtag']['event'][$event_name] = $values;
//		}
//
//	}
	
	
}