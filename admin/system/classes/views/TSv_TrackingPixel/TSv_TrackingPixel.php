<?php
class TSv_TrackingPixel extends TSv_Plugin
{
	protected static $pixel_name = '';
	protected static $pixel_icon = 'fa-square';
	protected static $track_function_name = '';
	
	
	/**
	 * TSv_TikTokPixel constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle(static::$pixel_name.' Pixel');
		$this->setPluginDescription('Installs the pixel on the site for tracking and analysis.');
		$this->setPluginIconName(static::$pixel_icon);
		$this->enableVariableForNum(1, 'Tracking ID', '');
		$this->setDefaultInstallValue('enable_admin', 0);
		$this->setDefaultInstallValue('enable_beta_site', 1);
		
	}
	
	/**
	 * Adds the tracking lines to JS after the pixel is loaded
	 * @return void
	 */
	public function addEventTrackingCalls()
	{
		$session_name = get_called_class().'_tag';
		
		// STEP 2 : Add site-specific tag `event`
		if(isset($_SESSION[$session_name]))
		{
			// EVENT Commands
			if(isset($_SESSION[$session_name]['event']))
			{
				$num_events = 1;
				foreach($_SESSION[$session_name]['event'] as $event_name => $event_values)
				{
					$this->addJSLine(
						$session_name.'_'.$num_events,
						static::eventCall($event_name, $event_values));
					$num_events++;
				}
				
			}
			
			
			// Clear the values in the session
			$_SESSION[$session_name] = [];
		}
	}
	
	
	
	/**
	 * Adds a value to track for the tiktok-tag for a "event" command. Each event will be a separate TikTok tag with the
	 * values for that event call. If multiple tags are set for the same event, it overwrites the previous event call.
	 *
	 * This method can be triggered in the middle of script and the next time it loads, it will add that to the tag
	 * for the page.
	 *
	 * @param string $event_name The name of the event being called
	 * @param array|string $values The values to be passed.If it's an array, it will merge it with other values to
	 * create a unified object. If it's a string, it will append to JSON for output
	 * @param bool $return_as_script_tag Indicates if the values should be immediately returned as a script tag
	 *
	 * @return null|string
	 */
	public static function trackEvent($event_name, $values = [], $return_as_script_tag = false )
	{
//		// Ignore if plugin not installed
//		if(!TC_website()->pluginInstalled(get_called_class()))
//		{
//			return null;
//		}
		
		$session_name = get_called_class().'_tag';
		
		
		// Check if the session value exists
		if(!isset($_SESSION[$session_name]))
		{
			$_SESSION[$session_name] =[];
		}
		// Check if the $type array
		if(!isset($_SESSION[$session_name]['event']))
		{
			$_SESSION[$session_name]['event'] = [];
		}
		
		if($return_as_script_tag)
		{
			return '<script>'.static::eventCall($event_name, $values).'</script>';
		}
		else
		{
			$_SESSION[$session_name]['event'][$event_name] = $values;
		}
		
	}
	
	/**
	 * Returns the JS for the event call for the pixel. This is a single line of JS that is run for each event.
	 * @param string $event_name
	 * @param array $values
	 * @param string $method_name
	 * @return string
	 */
	public static function eventCall($event_name, $values = [], $method_name = 'track' )
	{
		return (static::$track_function_name).'("'.$method_name.'","'.$event_name.'"'
			.(count($values) > 0 ? ',' . json_encode($values) : '').');';
	}
	
	
	/**
	 * Returns if this plugin is usable. Various reasons to disable a plugin on a site.
	 * @return bool
	 */
	public static function isUsable()
	{
		// This one is only usable if extended
		return get_called_class() != 'TSv_TrackingPixel';
	}
	
}