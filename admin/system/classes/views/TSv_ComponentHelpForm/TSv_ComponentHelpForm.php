<?php
class TSv_ComponentHelpForm extends TCv_FormWithModel
{
	/**
	 * TMv_LoginForm constructor.
	 * @param TSm_ComponentHelp $help_component
	 */
	public function __construct(TSm_ComponentHelp $help_component)
	{
		parent::__construct($help_component);
		
		$this->setButtonText('Update help');
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_HTMLEditor('help_html', 'Help HTML');
		$field->setTinyMCEConfig('plugins',"paste fullscreen anchor code charmap link lists");
		$field->setTinyMCEToolbar("formatselect | bold italic underline
		| bullist numlist | link code fullscreen ");
		$this->attachView($field);
	}
}