<?php
/**
 * The form to show the settings for a single plugin in the system
 */
class TSv_PluginForm extends TCv_FormWithModel
{
	/**
	 * The plugin form which shows a single plugin
	 * @param TSm_Plugin $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
		$this->setSuccessURL('/admin/system/do/plugins/');
		
	}
	
	/**
	 * Configures the form
	 * @return void
	 */
	public function configureFormElements()
	{
		$checkbox = new TCv_FormItem_Checkbox('enable_public', 'Enable On Public');
		$checkbox->setHelpText('Enable this plugin on the public website.');
		$checkbox->preventLayoutStacking();
		$this->attachView($checkbox);
		
		$checkbox = new TCv_FormItem_Checkbox('enable_admin', 'Enable On Admin');
		$checkbox->setHelpText('Enable this plugin on the admin side of this website.');
		$checkbox->preventLayoutStacking();
		$this->attachView($checkbox);
		
		$checkbox = new TCv_FormItem_Checkbox('enable_live_site', 'Enable On Live Site');
		$checkbox->setHelpText('Enable this plugin on the live/production site. ');
		$checkbox->preventLayoutStacking();
		$this->attachView($checkbox);
		
		$checkbox = new TCv_FormItem_Checkbox('enable_beta_site', 'Enable On Beta Site');
		$checkbox->setHelpText('Enable this plugin on the beta/testing site. ');
		$checkbox->preventLayoutStacking();
		$this->attachView($checkbox);
		
		$view_class = ($this->model()->className())::init($this->model());
		
		for($num = 1; $num <= 8; $num++)
		{
			if($variable_name = $view_class->pluginVariableNameForNum($num))
			{
				$name = new TCv_FormItem_TextField('variable_'.$num, $variable_name);
				$name->preventLayoutStacking();
				$this->attachView($name);
			}
		}

		
		
	}
	

	
}
?>