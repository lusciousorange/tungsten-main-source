<?php
/**
 * Class TSv_GoogleAnalytics
 */
class TSv_GoogleAnalytics extends TSv_TrackingPixel
{
	protected static $pixel_name = 'GoogleAnalytics';
	protected static $pixel_icon = 'fab fa-google';
	protected static $track_function_name = 'gtag';
	
	/**
	 * TSv_GoogleAnalytics constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		
		// Add multiple code slots
		$this->enableVariableForNum(1, 'Code 1', '');
		$this->enableVariableForNum(2, 'Code 2', '');
		$this->enableVariableForNum(3, 'Code 3', '');
		$this->enableVariableForNum(4, 'Code 4', '');
		$this->enableVariableForNum(5, 'Code 5', '');
		
		// Correct if it's set to not wait for rendering, needs it in case a view or theme sets a gtag value
		if($plugin_model && !$plugin_model->waitsForRendering())
		{
			$plugin_model->updateWithValues(['wait_to_load_on_rendering' => 1]);
		}
		
	}
	
	/**
	 *
	 */
	public function html()
	{
		// Add preconnect to sites
		$this->addMetaTag('gtm_preconnect','<link rel="preconnect" href="https://www.googletagmanager.com">');
		$this->addMetaTag('ga_preconnect','<link rel="preconnect" href="https://www.google-analytics.com">');
		
		
		// Find the active tags
		// Front-loading Google Tag Manager GTM codes
		$gtm_tags = [];
		$tags = [];
		for($num = 1; $num <= 5; $num++)
		{
			$code = trim($this->pluginVariable($num));
			if($code != '')
			{
				if(substr($code, 0, 3) == 'GTM')
				{
					$gtm_tags[] = $code;
				}
				else
				{
					$tags[] = $code;
				}
			}
			
		}
		
		$tags = array_merge($gtm_tags, $tags);
		
		if(count($tags) > 0)
		{
			// STEP 1 : Add scripts for the codes
			$count = 1;
			foreach($tags as $index => $tag_code)
			{
				
				$this->addJSFile('googletagmanager_' . $count,
								 '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $tag_code . '"></script>');
				
				$count++;
			}
			
			// STEP 2 : Load the gtag init
			$this->addJSLine('gtag',
							 "\n\t// GOOGLE TAG \n\twindow.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());", false);
			
			// LOAD CONSENT
			// @see https://developers.google.com/tag-platform/security/guides/consent?consentmode=advanced
			$this->addJSLine('gtag_consent',
							 "
	gtag('consent', 'default', {
	  'ad_storage': 'denied',
	  'ad_user_data': 'denied',
	  'ad_personalization': 'denied',
	  'analytics_storage': 'granted'
	});", false);
			
			
			// STEP 3 : Add tags for the codes
			foreach($tags as $index => $tag_code)
			{
				// Enable debug mode if necessary
				if(TC_getConfig('is_staging_domain') && substr($tag_code, 0, 2) == 'G-')
				{
					$this->addJSLine('gtag_' . $index,
									 "gtag('config', '" . $tag_code . "',{'debug_mode' : 'true'});", false);
					
				}
				else
				{
					$this->addJSLine('gtag_' . $index,
									 "gtag('config', '" . $tag_code . "');", false);
					
				}
				
				
			}
			
			
			// STEP 4 : Add site-specific gtag values for `set`
			if(isset($_SESSION['gtag']))
			{
				// SET Commands
				if(isset($_SESSION['gtag']['set']))
				{
					$line = "gtag('set', " . json_encode($_SESSION['gtag']['set']) . ")";
					$this->addJSLine('gtag_set', $line);
					
				}
			}
			
			
			// STEP 5 : Add site-specific gtag values for  `event`
			$this->addEventTrackingCalls();
			
			
		}
		
		
	}
	
	
	/**
	 * Adds a value to track for the gtag for a "set" command. Multiple calls to this method will consolidate all the
	 * values into a single call.
	 *
	 * This method can be triggered in the middle of script and the next time it loads, it will add that to the gtag
	 * for the page.
	 *
	 * @param array|string $values The values to be passed.If it's an array, it will merge it with other values to
	 * create a unified object. If it's a string, it will append
	 * to JSON for output
	 *
	 * @see https://developers.google.com/tag-platform/gtagjs/reference#set
	 * @return void
	 */
	public static function addSetGTag($values)
	{
		// Check if the session value exists
		if(!isset($_SESSION['gtag']))
		{
			$_SESSION['gtag'] = [];
		}
		// Check if the $type array
		if(!isset($_SESSION['gtag']['set']))
		{
			$_SESSION['gtag']['set'] = [];
		}
		
		
		foreach($values as $name => $value)
		{
			$_SESSION['gtag']['set'][$name] = $value;
		}
		
		
	}
	
	/**
	 * Extend the event-call function to determine the method names
	 * Returns the JS for the event call for the pixel. This is a single line of JS that is run for each event.
	 * @param string $event_name
	 * @param array $values
	 * @param string $method_name
	 * @return string
	 */
	public static function eventCall($event_name, $values = [], $method_name = 'track')
	{
		if($method_name === 'track')
		{
			$method_name = 'event';
		}
		
		return parent::eventCall($event_name, $values, $method_name);
	}
	
	/**
	 * Generates the values for a cartable model
	 * @param TCm_Model $model
	 * @return array
	 */
	public static function itemValuesForModel($model)
	{
		$values = [
			'item_id' => $model->contentCode(),
			'item_name' => $model->title(),
			'quantity' => 1,
			'price' => 0,
		];
		
		if(class_exists('TMm_ShoppingCartItem') && class_exists('TMm_PurchaseItem'))
		{
			if($model instanceof TMm_ShoppingCartItem || $model instanceof TMm_PurchaseItem)
			{
				$values = [
					'item_id' => $model->item()->contentCode(),
					'item_name' => $model->item()->title(),
					'price' => floatval($model->pricePerItem()),
					'discount' => floatval($model->discountPerItem()),
					'quantity' => $model->quantity(),
				
				];
				
			}
			else
			{
				$values = [
					'price' => floatval($model->price()),
					'discount' => floatval($model->discountPerItem()),
				
				];
				
			}
		}
		
		
		return $values;
	}
	
	/**
	 * Backwards compatibility.
	 * @param $event_name
	 * @param $values
	 * @param $return_as_script_tag
	 * @return void
	 */
	public static function addEventGTag($event_name, $values = [], $return_as_script_tag = false)
	{
		static::trackEvent($event_name, $event_name, $return_as_script_tag);
	}
	
	
}