<?php
class TSv_ModuleSettingsList extends TCv_ModelList
{
	public function __construct()
	{
		// Always clear the cache with this list
		TC_Memcached::delete(TSm_Module::cacheAllKeyName());
		
		parent::__construct('module_settings_list');
		$this->setModelClass('TSm_Module');
		$this->defineColumns();
		
		// Override default list to only acquire those
		$this->addModels($this->modelList()->availableModules());
		
		$this->addClassCSSFile('TSv_ModuleSettingsList');
	}
	
	/**
	 * @param TSm_Module $module
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($module)
	{
		if(!$module->installable())
		{
			return null;
		}
		
		$module->clearCache();

		return parent::rowForModel($module);
		
	}
	
	public function defineColumns()
	{
		$column = new TCv_ListColumn('icon');
		$column->setWidthAsPixels(32);
	//	$column->setTitle('Module');
		$column->setContentUsingListMethod('moduleIconColumn');
		$column->setAlignment('center');
		//$column->setHeadingColumnSpan(2);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('title');
	//	$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
	//	$column->setWidthAsPercentage(30);
		$this->addTCListColumn($column);
		
		if(TC_Memcached::enabled())
		{
			$column = new TCv_ListColumn('cache');
			$column->setTitle('Cache');
			$column->setContentUsingListMethod('cacheColumn');
			$column->setWidthAsPixels(80);
			$this->addTCListColumn($column);
			
		}
		
		
		$column = new TCv_ListColumn('install_upgrade');
		$column->setVerticalAlignment('middle');
		$column->setContentUsingListMethod('installUpgradeColumn');
		$column->setTitle('Version');
		//$column->setWidthAsPercentage(30);
		$column->setWidthAsPixels(110);
		$this->addTCListColumn($column);
		
		if(TC_currentUserIsDeveloper())
		{
			$column = new TCv_ListColumn('force_install');
			$column->setVerticalAlignment('middle');
			$column->setContentUsingListMethod('forceInstallColumn');
			$column->setTitle('');
			$column->setWidthAsPixels(30);
			$this->addTCListColumn($column);
		}

//		$column = new TCv_ListColumn('settings_arrow');
//		$column->setAlignment('right');
//		$column->setVerticalAlignment('middle');
//		$column->setContentUsingListMethod('settingsArrowColumn');
////		//$column->setWidthAsPixels(50);
////		//$column->setWidthAsPercentage(40);
////		//$column->setTitle('Version');
//		$this->addTCListColumn($column);
		
		
		
	}
	
	/**
	 * The icon column row
	 * @param TSm_Module $module The module being shown
	 * @return bool|TCv_View
	 */
	public function moduleIconColumn(TSm_Module $module)
	{
		if($module->hasVariables())
		{
			$link = new TCv_Link();
			$link->setURL('/admin/system/do/edit-settings/'.$module->folder());
			$link->setIconClassName($module->iconCode());
			$link->addClass('settings_link');
			$link->addDataValue('module-id', $module->id());
			
			return $link;
			
		}
		else
		{
			$icon = new TCv_View();
			$icon->addClass('fas');
			$icon->addClass($module->iconCode());
			return $icon;
		}
		
	}
	
	/**
	 * The column for the title
	 * @param TSm_Module $model The item being provided
	 * @return TCv_View|string
	 */
	public function titleColumn(TSm_Module $module)
	{
		if($module->hasVariables())
		{
			$link = new TCv_Link();
			$link->setURL('/admin/system/do/edit-settings/'.$module->folder());
			$link->addText($module->title());
			$link->addClass('settings_link');
			$link->addClass('module_button_'.$module->folder());
		//	$link->addDataValue('module-id', $module->id());
			return $link;
		}
		
		return $module->title();
	}
	
	/**
	 * Column to clear memcached if necessary
	 * @param TSm_Module $module
	 * @return TCv_Link|null
	 */
	public function cacheColumn(TSm_Module $module)
	{
		if($module->installed())
		{
			$link = new TCv_Link();
			$link->setURL('/admin/system/do/clear-module-memcached/' . $module->folder());
			$link->addText('Clear');
			//	$link->addDataValue('module-id', $module->id());
			return $link;
		}
		return null;
	}
	
	/**
	 * The column for the title
	 * @param TSm_Module $model The item being provided
	 * @return TCv_View|string
	 */
	public function settingsArrowColumn(TSm_Module $module)
	{
//		if($module->hasVariables() && $module->installed())
//		{
//			$link = new TCv_Link();
//			$link->setURL('/admin/system/do/edit-settings/'.$module->folder());
//			$link->addClass('settings_link');
//			$link->setIconClassName('fa-arrow-right');
//		//	$link->addDataValue('module-id', $module->id());
//			return $link;
//		}
		
		return null;
	}


	/**
	 * The install upgrade column
	 * @param TSm_Module $module The module being shown
	 * @return bool|TCv_View
	 */
	public function installUpgradeColumn($module)
	{
		// Not installable
		if(!$module->installable())
		{
			return;
		}
		$upgrade_button = new TCv_Link();
		$upgrade_button->addClass('module_server_version');
		$upgrade_button->addText($module->version());
		
		if($module->installed())
		{
			if($module->requiresUpgrade())
			{
				$upgrade_button->addClass('upgrade_ready');
				$upgrade_button->setIconClassName('fas fa-arrow-up-from-bracket');
				$upgrade_button->setURL('/admin/system/do/module-install/'.$module->folder());
				$upgrade_button->setTitle('Upgrade');
			}
			else
			{
				$upgrade_button->addClass('installed');
				$upgrade_button->setIconClassName('fas fa-check');
				$upgrade_button->setTag('span');
				
			}
		}
		else // Not installed
		{
			$upgrade_button->addClass('not_installed');
			$upgrade_button->setIconClassName('fa-plus');
			$upgrade_button->setURL('/admin/system/do/module-install/'.$module->folder());
			$upgrade_button->setTitle('Install');
		}
		
		
		return $upgrade_button;
		
		
	}
	
	public function forceInstallColumn($module)
	{
		if($module->installed())
		{
			$upgrade_button = new TCv_Link();
			$upgrade_button->setIconClassName('fa-redo');
			$upgrade_button->setURL('/admin/system/do/module-install/' . $module->folder());
			$upgrade_button->setTitle('Reinstall');
			return $upgrade_button;
		}
	}
}