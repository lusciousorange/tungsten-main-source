<?php
/**
 * Class TSv_FacebookDomainVerification
 *
 * Adds Facebook Domain Verification to the entire site.
 */
class TSv_FacebookDomainVerification extends TSv_Plugin
{
	/**
	 * TSv_FacebookDomainVerification constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Facebook Domain Verification');
		$this->setPluginDescription('A plugin that adds the facebook-domain-verification metatag that lets google know you have access.');
		$this->setPluginIconName('fab fa-facebook');
		$this->enableVariableForNum(1, 'Verification Code','');
		

		
	}
	
	/**
	 *
	 */
	public function html()
	{
		if($this->pluginVariable(1) != '')
		{
			$this->addMetaTag('facebook-domain-verification', [
				'name' => 'facebook-domain-verification',
				'content' => $this->pluginVariable(1)]);
	
		}		

	}
	
	
}
?>