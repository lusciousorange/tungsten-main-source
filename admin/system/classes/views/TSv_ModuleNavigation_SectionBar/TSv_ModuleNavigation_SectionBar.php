<?php
/**
 * Class TSv_ModuleNavigation_SectionBar
 */
class TSv_ModuleNavigation_SectionBar extends TCv_Menu
{
	/**
	 * TSv_ModuleNavigation_SectionBar constructor.
	 *
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct('module_navigation_section_bar');
		
		$this->addClassCSSFile('TSv_ModuleNavigation_SectionBar');
		
	}
	
	
	
}	
?>