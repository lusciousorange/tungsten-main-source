<?php

/**
 * Class TSv_TungstenVersion
 */
class TSv_TungstenVersion extends TCv_View
{
	protected string $branch_name = 'Unknown';
	
	/**
	 * TSv_TungstenVersion constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->addClassCSSFile('TSv_TungstenVersion');
	
	}


	/**
	 * @return string
	 */
	public function html()
	{
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Versions');
		$this->attachView($heading);
		
		$table = new TCv_HTMLTable();
		
		
		include($_SERVER['DOCUMENT_ROOT'].'/admin/system/config/system_version.php');
		
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Tungsten');
		$row->createCellWithContent($admin_version);
		$table->attachView($row);
		
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('PHP');
		$row->createCellWithContent(phpversion());
		$table->attachView($row);
		
		
		$this->attachView($table);
		
		
		
		if(TC_configIsSet('cloudways') && class_exists('TMv_CloudwaysStatusSummary'))
		{
			$this->attachView(TMv_CloudwaysStatusSummary::init());
		}
		else
		{
			$this->attachView($this->activeGitRepoFolderStatus());
		}
		
		
		
		
		
		return parent::html();
	}
	
	protected function activeGitRepoFolderStatus() : TCv_View
	{
		$status = new TCv_View();
		$git_installed = true;
		
		$rev = TCv_Website::gitVersion();
		if($rev == '')
		{
			$git_installed = false;
			$rev = 'Not found';
		}
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('GIT Folder');
		$status->attachView($heading);
		
		$table = new TCv_HTMLTable();
		
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('GIT version');
		$row->createCellWithContent($rev);
		$table->attachView($row);
		
		
		
		
		if($git_installed)
		{
			$row = new TCv_HTMLTableRow();
			$row->createCellWithContent('GIT branch');
			
			
			if(function_exists('exec'))
			{
				$branches = exec('git branch', $lines);
				foreach($lines as $line)
				{
					if(substr($line, 0, 1) == '*')
					{
						$line = trim(substr($line, 1));
						$this->branch_name = $line;
					}
				}
			}
			$row->createCellWithContent($this->branch_name);
			$table->attachView($row);
			
			
		}
		
		$status->attachView($table);
		
		return $status;
	}
	
}
?>