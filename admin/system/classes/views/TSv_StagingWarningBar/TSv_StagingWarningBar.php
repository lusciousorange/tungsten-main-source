<?php

/**
 * A class to show the warning bar for staging websites which includes the console button
 */
class TSv_StagingWarningBar extends TCv_View
{
	public function render()
	{
		$this->addClassCSSFile('TSv_StagingWarningBar');
		
		// Add the warning
		$warning = new TCv_View();
		$warning->addClass('message');
		//$warning->addText('<i class="fa fa-exclamation-triangle"></i> ');
		$show_message = false;
		if(TC_getConfig('is_localhost'))
		{
			$this->addClass('localhost_message');
			$warning->addText('<strong>Localhost</strong> – You are currently viewing a localhost domain, not the live website.');
			$show_message = true;
		}
		elseif(TC_getConfig('is_staging_domain'))
		{
			$warning->addText('<strong>Staging</strong> – You are currently viewing a staging domain, not the live website.');
			$show_message = true;
		}
		if($show_message)
		{
			$this->attachView($warning);
			
			
			// Hide if we aren't saving to the console
			if(TC_getConfig('console_saving'))
			{
				// Confirm console exists
				if($console_plugin = TSm_Plugin::init('TSv_Console'))
				{
					if($console_plugin->determineIfShown(
						TC_getConfig('is_staging_domain'),
						TC_isTungstenView()) )
					{
						$console_button = new TCv_Link();
						$console_button->setTitle('Console');
						$console_button->addText('Console');
						$console_button->addClass('TSv_ConsoleButton_show');
						$console_button->addClass('console_button');
						$console_button->setURL('#');
						$this->attachView($console_button);
						
					}
				}
			}
		}
		
		
	}
}