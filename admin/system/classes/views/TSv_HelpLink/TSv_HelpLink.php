<?php

/**
 * Class TSv_HelpLink
 *
 * A link used in help to highlight items in the interface.
 */
class TSv_HelpLink extends TCv_Link
{
	/**
	 * TSv_HelpLink constructor.
	 *
	 * @param bool|string $title
	 * @param string $css_identifier
	 */
	public function __construct($title, $css_identifier)
	{
		parent::__construct(false);
		$this->setURL('#');
		$this->setAttribute('data-highlight', $css_identifier);
		$this->addText($title);

	}
	
}
?>