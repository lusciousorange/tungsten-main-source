<?php
class TSv_FontAwesomeSymbol extends TCv_View
{
	protected $symbol_id;
	protected $code;

	/**
	 * TSv_FontAwesomeSymbol constructor.
	 * @param string $symbol_id The ID for the symbol
	 * @param string $code The code for the actual FontAwesome classes
	 */
	public function __construct($symbol_id, $code)
	{
		parent::__construct();
		$this->symbol_id = $symbol_id;
		$this->code = $code;
		$this->setTag('svg');

	}

	public function render()
	{
		$this->addFontAwesomeClassAsSymbol($this->symbol_id, $this->code);
		$this->addClass('svg-inline--fa fa-fw');
		$this->addClass('symbol_'.$this->symbol_id);
		$this->addText('<use xlink:href="#'.$this->symbol_id.'"></use>');

		// The symbols cannot have these otherwise they don't work
		// They might get added in other ways, but always need to remove them
		$this->removeClass('fa-brands');
		$this->removeClass('fa-solid');
	}

}