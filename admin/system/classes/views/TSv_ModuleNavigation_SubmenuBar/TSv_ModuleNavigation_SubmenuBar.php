<?php
/**
 * Class TSv_ModuleNavigation_SubmenuBar
 *
 * A Navigation bar used to show submenus in Tungsten
 */
class TSv_ModuleNavigation_SubmenuBar extends TCv_Menu
{
	/**
	 * TSv_ModuleNavigation_SubmenuBar constructor.
	 *
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->addClassCSSFile('TSv_ModuleNavigation_SubmenuBar');
		
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		return parent::html();
	}
	
	/**
	 * Adds an item to the submenu
	 * @param string $title
	 * @param string $link
	 */
	public function addItem($title, $link)
	{
		$button = new TCv_Link();
		$button->setTitle($title);
		$button->addText($title);
		$button->setURL($link);
	}
	
	/**
	 * @param $menu_item
	 * @deprecated No longer necessary
	 * @see TCv_View::attachView()
	 */
	public function attachMenuItem($menu_item)
	{
		$this->attachView($menu_item);
	}
	
	
}	
?>