<?php
/**
 * Class TSv_AdobeTypeKit
 */
class TSv_AdobeTypeKit extends TSv_Plugin
{
	/**
	 * TSv_AdobeTypeKit constructor.
	 *
	 * @param bool $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Adobe TypeKit');
		$this->setPluginDescription('This requires an Adobe TypeKit account configured to provide fonts of this website.');
		$this->setPluginIconName('fa-bold');
		$this->enableVariableForNum(1, 'TypeKit URL','');
		$this->setDefaultInstallValue('enable_admin', 0);		
		

		
	}
	
	
	/**
	 *
	 */
	public function html()
	{
		if($this->pluginVariable(1) != '')
		{
			$this->addJSFile('adobe_typekit', $this->pluginVariable(1));
		//	$this->addMetaTag('adobe_typekit', '<script src="'.$this->pluginVariable(1).'"></script>');
		//	$this->addMetaTag('adobe_typekit_load', '<script>try{Typekit.load();}catch(e){}</script>');
			$this->addJSLine('adobe_typekit_load', 'try{Typekit.load();}catch(e){}', true);
	
		}
	}
	
}
?>