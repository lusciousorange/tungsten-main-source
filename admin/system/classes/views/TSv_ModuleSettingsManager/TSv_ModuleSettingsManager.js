class TSv_ModuleSettingsManager {

	element;
	form_container;
	constructor(element, params) {

		this.element = element;
		this.form_container = element.querySelector('#module_settings_form_container');

		element.querySelectorAll('.settings_link').forEach(el =>
			el.addEventListener('click',this.settingsClickHandler.bind(this))
		);

		// DETECT HASH, which comes from the form being loaded independantly
		if(window.location.hash) {
			// Fragment exists
			let module_name = window.location.hash.replace('#','');

			this.element.querySelector('.module_button_'+module_name)?.click();

		}
	}

	settingsClickHandler(event) {
		event.preventDefault();

		let link_element = event.currentTarget;

		// Add Tungsten loading to the list
		this.element.classList.add('tungsten_loading');

		// Remove any row that has the showing class, then add it to the parent
		this.element.querySelector('.TCv_ListRow.showing')?.classList.remove('showing');
		link_element.closest('.TCv_ListRow').classList.add('showing');


		// Get the URL and load it with fetch
		let url = link_element.getAttribute('href');
		history.pushState('','',url);
		url = url +'/?return_type=json';

		fetch(url,{
			method : 'GET'
		})	.then(response => response.json())
			.then(response => {

				this.element.classList.remove('tungsten_loading');

				this.form_container.innerHTML = response.html;

				if(response.js)
				{
					processAsyncJSLinks(response.js);
				}
				if(response.css)
				{
					processAsyncCSSLinks(response.css);
				}


			});

	}

}