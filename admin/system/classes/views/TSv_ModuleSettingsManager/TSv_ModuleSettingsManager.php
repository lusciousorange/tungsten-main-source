<?php

/**
 * This is a manager that combines the
 */
class TSv_ModuleSettingsManager extends TCv_View
{
	/**
	 * The setting manager
	 * @param $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TSv_ModuleSettingsManager');
		$this->addClassJSFile('TSv_ModuleSettingsManager');
		$this->addClassJSInit('TSv_ModuleSettingsManager');
		
	}
	
	public function render()
	{
		$list = TSv_ModuleSettingsList::init();
		$this->attachView($list);
		
		$form_container = new TCv_View('module_settings_form_container');
		$this->attachView($form_container);
	}
}