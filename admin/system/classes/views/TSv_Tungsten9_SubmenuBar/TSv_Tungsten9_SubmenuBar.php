<?php
/**
 * Class TSv_Tungsten9_SubmenuBar
 *
 * A Navigation bar used to show submenus in Tungsten
 */
class TSv_Tungsten9_SubmenuBar extends TCv_Menu
{
	/**
	 * TSv_Tungsten9_SubmenuBar constructor.
	 *
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->addClassCSSFile('TSv_Tungsten9_SubmenuBar');
		
		$this->generateForCurrentURLTarget();
		
	}
	
	/**
	 * Auto generates the menu items for the current URL target. This looks in the controller for the module and
	 * finds a subgrouping that contains the current URL target, then adds those as menu items.
	 */
	public function generateForCurrentURLTarget()
	{
		$current_url_target = TC_currentURLTarget();
		$next_url_target = $current_url_target;
		$url_targets = [];
		// Loop until we find one that works, or until we no longer have a parent
		while($next_url_target instanceof TSm_ModuleURLTarget && count($url_targets) == 0)
		{
			// Get all submenus
			$url_targets = $next_url_target->module()->controller()->submenuGroupingForURLTarget($next_url_target);
			
			// Go up one level
			$next_url_target = $next_url_target->parentURLTarget();
		}
		
		
		$this->addClass('menu_count_'.count($url_targets));
		
		// Determine if any of these url_targets are the current one. If so, we can just label that one as current
		// Saves the problem with labeling using parent/child for things next ot each other
		$has_current_menu = false;
		foreach($url_targets as $name => $url_target)
		{
			if($url_target->name() == $current_url_target->name())
			{
				$has_current_menu = true;
			}
		}
		
		// SORT URL TARGETS BY submenu order, which may or may not be set
		// Loop through each one, splitting them into two groups, sorted and not
		$sortable = [];
		$unsorted = [];
		foreach($url_targets as $name => $url_target)
		{
			
			$submenu_order =$url_target->submenuOrder();
			if(is_null($submenu_order))
			{
				$unsorted[] = $url_target;
			}
			else // sortable
			{
				$num = 0;
				do
				{
					// Try decimals to differentiate, takes the number and divides by ten
					// So we're using X.1, X.2, etc
					$submenu_order += $num/10;
					$num++;
				}
				while(isset($sortable[$submenu_order]));
				
				// Made it through, add it t
				$sortable[$submenu_order] = $url_target;
			}
		}
		
		// rearrange the sortable ones by their unique inidces
		ksort($sortable);
		
		// Combine the sortable and unsorted
		$url_targets = array_merge($sortable, $unsorted);
		
		foreach($url_targets as $url_target)
		{
			$link = TSv_ModuleURLTargetLink::init($url_target);
			
			// If it has the current menu, then we only worry about that one
			if($has_current_menu)
			{
				if($url_target->name() == $current_url_target->name())
				{
					$link->addRelatedMenuClasses();
				}
			}
			else
			{
				$link->addRelatedMenuClasses();
				
			}
			$link->useGenericTitle();
			
			$icon_code = $url_target->iconCode();
			if($icon_code != '')
			{
				// Ensure we're using solid icons
				if(strpos($icon_code, 'fab ') === false)
				{
					$icon_code = 'fas '.$icon_code;
				}
				$link->setIconClassName($icon_code);
			}
//			if($current_url_target->name() == $name)
//			{
//				$link->addClass('active_menu');
//			}
			$this->attachView($link);
		}
	}
	
	
}	
?>