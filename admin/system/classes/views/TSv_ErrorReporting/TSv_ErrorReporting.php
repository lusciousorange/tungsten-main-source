<?php
/**
 * Class TSv_ErrorReporting
 */
class TSv_ErrorReporting extends TSv_Plugin
{
	
	/**
	 * TSv_ErrorReporting constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Error Reporting ');
		$this->setPluginDescription('Indicate if error reporting is shown for a given part of the system.');
		$this->setPluginIconName('fa-bomb');
		
		$this->setAsNotDeletable();
	
		
	}

	/**
	 * no action html call
	 */
	public function html()
	{
	}

	/**
	 * Process when not loaded
	 */
	public function processNotLoaded()
	{
	}
	
	
}
?>