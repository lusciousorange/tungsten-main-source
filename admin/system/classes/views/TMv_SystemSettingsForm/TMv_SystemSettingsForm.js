class TMv_SystemSettingsForm extends TCv_Form {

	use_smtp_field = null;
	smtp_auth_type_field = null;

	constructor(element, params) {
		super(element, params);

		// get both fields first
		this.use_smtp_field = this.element.querySelector('#use_smtp');
		this.smtp_auth_type_field = this.element.querySelector('#smtp_auth_type');

		if(this.use_smtp_field)
		{
			this.use_smtp_field.addEventListener('change', e => { this.useSMTPChanged(e)});
			this.useSMTPChanged();

		}


		if(this.smtp_auth_type_field)
		{
			this.smtp_auth_type_field.addEventListener('change', e => { this.authTypeChanged(e)});
			this.authTypeChanged();

		}

	}

	useSMTPChanged(event) {
		if(this.use_smtp_field.value === '1')
		{
			this.showFieldRows('.smtp_value');
			this.authTypeChanged();
		}
		else
		{
			this.hideFieldRows('.smtp_value');
		}

	}

	authTypeChanged(event) {
		//console.log('authTypeChanged');
		if(this.use_smtp_field.value === '1')
		{
			if(this.smtp_auth_type_field.value === 'name_pass')
			{
				this.showFieldRows('.smtp_auth_name_pass');
				this.hideFieldRows('.smtp_auth_oauth');
			}
			else
			{
				this.hideFieldRows('.smtp_auth_name_pass');
				this.showFieldRows('.smtp_auth_oauth');

			}
		}


	}
}