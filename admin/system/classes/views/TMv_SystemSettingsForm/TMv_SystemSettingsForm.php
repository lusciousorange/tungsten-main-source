<?php
/**
 * Class TMv_SystemSettingsForm
 *
 * The form associated with the settings for the system
 */
class TMv_SystemSettingsForm extends TSv_ModuleSettingsForm
{
	/**
	 * TMv_SystemSettingsForm constructor.
	 *
	 * @param string|TCm_Model $module
	 */
	public function __construct($module)
	{	
		parent::__construct($module);
		$this->addClassJSFile('TMv_SystemSettingsForm');
		$this->addClassJSInit('TMv_SystemSettingsForm');

		$this->addClassCSSFile('TMv_SystemSettingsForm');

	}
	
	/**
	 * Configures the form elements for creating or editing a product
	 */
	public function configureFormElements()
	{
		if(TC_currentUserIsDeveloper())
		{
			$console_hour_limit = new TCv_FormItem_TextField('console_hour_limit', 'Console memory hours');
			$console_hour_limit->setDefaultValue('12');
			$console_hour_limit->setHelpText('The number of hours that the developer console will retain a memory. The longer the memory, the slower the console. ');
			$this->attachView($console_hour_limit);
		}
		$field = new TCv_FormItem_Heading('email_settings', 'System email settings');
		$this->attachView($field);

		$email_name = new TCv_FormItem_TextField('password_reset_name', 'Email name');
		$email_name->setHelpText('The name that should be shown for emails coming from the system.');
		$this->attachView($email_name);

		$email_address = new TCv_FormItem_TextField('password_reset_email', 'Email address');
		$email_address->setHelpText('The email address shown for emails coming from the system.');
		$this->attachView($email_address);

		$email_name = new TCv_FormItem_TextField('email_reply_to_address', 'Reply-to email address');
		$email_name->setHelpText('The email address that should be set as the reply-to when attempting to respond to an email.');
		$this->attachView($email_name);

		$email_frame_view = new TCv_FormItem_TextField('email_frame_view', 'Email frame view');
		$email_frame_view->setHelpText('The name of the view uses as a frame for emails. This is a programmer setting that determines how your emails look.');
		$this->attachView($email_frame_view);

		$email_frame_view = new TCv_FormItem_TextBox('user_created_email_explanation', 'Email new user explanation');
		$email_frame_view->setHelpText('The text shown to a new user when they create an account.');
		$email_frame_view->setDefaultValue('Thank you for signing up for our website. Your account has been created and you can log in using the link below.');
		$this->attachView($email_frame_view);

		if(TC_currentUserIsDeveloper())
		{
			
			
			$setting = new TCv_FormItem_Select('use_smtp', 'Use SMTP');
			$setting->setHelpText('Indicate if emails should use an SMTP Server instead of the default system email functionality. ');
			$setting->addOption('0', 'No – Use built-in mail() functionality');
			$setting->addOption('1', 'Yes – Use SMTP');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_Select('smtp_connection_mode', 'Connection mode');
			$setting->setHelpText('Indicate the secure connection mode. TLS is recommended.');
			$setting->addClass('smtp_value');
			$setting->addOption('tls', 'TLS');
			$setting->addOption('ssl', 'SSL');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_port', 'SMTP port');
			$setting->setHelpText('If you select SSL, use 465 but check with email provided.');
			$setting->addClass('smtp_value');
			$setting->setDefaultValue('587');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_host', 'SMTP host');
			$setting->setHelpText('List of SMTP servers, separated by semi-colon.');
			$setting->addClass('smtp_value');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_Select('smtp_auth_type', 'Password or OAuth');
			$setting->setHelpText('Indicate if you want to connect via a username/password or via Oauth2. If possible, we recommend using OAuth2. ');
			$setting->addClass('smtp_value');
			$setting->addOption('name_pass', 'Username (email) and password');
			$setting->addOption('oauth2_google', 'OAuth2 - Google/Gmail');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_username', 'SMTP email');
			$setting->setHelpText('The email SMTP Username. Usually the same that you use to log into email.');
			$setting->addClass('smtp_value');
			$this->attachView($setting);
			
			
			$setting = new TCv_FormItem_TextField('smtp_password', 'SMTP password');
			$setting->setHelpText('The email SMTP Password. ');
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_name_pass');
			$setting->setSavedValue(TC_getModuleConfig('system', 'smtp_password'));
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_HTML('smtp_instructions_google', 'Google SMTP OAuth Instructions');
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_oauth');
			
			$toggle_button = new TCv_ToggleTargetLink();
			$toggle_button->addText("View instructions");
			$toggle_button->addVisibilityTarget('#google_oauth_instructions');
			$setting->attachView($toggle_button);
			
			
			$instructions = new TCv_View('google_oauth_instructions');
			$instructions->setTag('ol');
			$instructions->addText('<li>Visit the <a href="https://console.developers.google.com/apis/" target="_blank">APIs section</a> in the <strong>Google Developer Console</strong>. It must be for the domain being used.</li>');
			$instructions->addText('<li>Create a <strong>new project</strong> with an appropriate name and select that project.</li>');
			$instructions->addText('<li>Configure the consent screen (if not configured) in <strong>OAuth consent screen</strong></li>');
			$instructions->addText('<li>Click on <strong>Create Credentials</strong> OAuth client ID</li>');
			$instructions->addText('<li>Configure as "External" which avoids verification requirements. </li>');
			$instructions->addText('<li>Fill in the required information. Ignore optional fields. Default scopes. </li>');
			$instructions->addText('<li>Click on <strong>Credentials</strong></li>');
			$instructions->addText('<li>Click on <strong>Create Credentials</strong> > <strong>OAuth client ID</strong></li>');
			$instructions->addText('<li>Application Type: Web application</li>');
			$instructions->addText('<li><strong>Authorized redirect URI:</strong> The URL found in this form. Must be publicly loadable, not localhost</li>');
			$instructions->addText('<li>Copy/paste the <strong>Client ID</strong> AND the <strong>Client Secret</strong> into this form</li>');
			$instructions->addText('<li>Submit this form, to save those values in the database</li>');
			$instructions->addText('<li>Copy/paste the OAuth URL from this form and load it in a browser window</li>');
			$instructions->addText('<li>Follow the steps to authenticate the app</li>');
			$instructions->addText('<li>Copy/paste the <strong>Refresh Token</strong> from the browser into the field on this form</li>');
			$instructions->addText('<li>Submit this form, to save those values in the database</li>');
			$instructions->addText('<li>Use the forgot password form to confirm emails are sending</li>');
			$instructions->addText('<li><a href="https://stackoverflow.com/questions/46553579/unknown-error-when-creating-a-new-project-in-google-api" target="_blank">Dealing with Unknown Error</a></li>');
			$setting->attachView($instructions);
			
			$this->attachView($setting);
			
			
			$setting = new TCv_FormItem_TextField('smtp_oauth_client_id', 'SMTP OAuth client ID');
			$setting->setHelpText('The client ID for OAuth2.');
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_oauth');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_oauth_client_secret', 'SMTP OAuth client secret');
			$setting->setHelpText('The client secret for OAuth2. ');
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_oauth');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_oauth_url', 'SMTP OAuth URL');
			$setting->setHelpText('The URL that points to the oauth validator which must be visible to the service provider. ');
			$setting->setDefaultValue((isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/admin/TCore/Content/TCv_Email/TC_get_mailer_oauth_token.php');
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_oauth');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('smtp_oauth_refresh_token', 'SMTP refresh token');
			$setting->setHelpText("The refresh token provided by the script above");
			$setting->addClass('smtp_value');
			$setting->addClass('smtp_auth_oauth');
			$this->attachView($setting);
		}
		
		if(TC_currentUserIsDeveloper())
		{
			$field = new TCv_FormItem_Heading('cron_heading', 'Cron settings');
			$this->attachView($field);
		
			$setting = new TCv_FormItem_TextField('cron_domain', 'Cron domain');
			$setting->setHelpText('The domain (optional protocol) for cron calls. ');
			$setting->setDefaultValue($_SERVER['HTTP_HOST']);
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_Select('cron_tracking', 'Cron tracking');
			$setting->setHelpText('Indicate if cron calls should be tracked for debugging purposes. ');
			$setting->addOption('0', 'No – Do not track cron calls');
			$setting->addOption('1', 'Yes – Track cron calls');
			$this->attachView($setting);
		}


		$this->addGoogleReCaptchaFields();

	}

	/**
	 * Set the required keys needed for using the Google ReCaptcha form field
	 */
	public function addGoogleReCaptchaFields()
	{
		if(TC_getConfig('use_google_recaptcha'))
		{
			$field = new TCv_FormItem_Heading('recaptcha_heading', 'Google recaptcha settings');
			$field->addHelpText('Keys required for using the Google ReCaptcha form item. Keys can be generated by creating a profile for this site on the Google reCaptcha admin page.');
			$this->attachView($field);
			
			$setting = new TCv_FormItem_TextField('recaptcha_site_key', 'ReCaptcha site key');
			$this->attachView($setting);
			
			$setting = new TCv_FormItem_TextField('recaptcha_secret_key', 'ReCaptcha secret key');
			$this->attachView($setting);
		}

	}
	

	
}
?>