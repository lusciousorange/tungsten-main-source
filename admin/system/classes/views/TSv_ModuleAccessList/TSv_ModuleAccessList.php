<?php
/**
 * Class TSv_ModuleAccessList
 */
class TSv_ModuleAccessList extends TCv_ModelList
{
	/**
	 * TSv_ModuleList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->setModelClass('TSm_Module');
		
		// Override default list to only acquire those 
		$this->addModels($this->modelList()->installedModules());
		
		
		$this->defineColumns();
		//$this->processListItems();
		$this->addClassCSSFile('TSv_ModuleAccessList');
	}
	
	/**
	 * Defines the columns for hte list
	 */
	public function defineColumns()
	{
		$icon = new TCv_ListColumn('icon');
		$icon->setWidthAsPixels(55);
		$icon->setTitle('Module');
		$icon->setContentUsingListMethod('moduleIconColumn');
		$icon->setAlignment('center');
		$this->addTCListColumn($icon);
		
		$title = new TCv_ListColumn('title');
		$title->setTitle('');
		$title->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($title);
		
		$groups = new TCv_ListColumn('user_groups');
		$groups->setWidthAsPercentage(30);
		$groups->setTitle('Access Groups');
		$groups->setContentUsingListMethod('groupsColumn');
		$this->addTCListColumn($groups);
		
		$show_in_menu = new TCv_ListColumn('show_in_menu');
		$show_in_menu->setVerticalAlignment('middle');
		$show_in_menu->setWidthAsPercentage(10);
		$show_in_menu->setAlignment('center');
		$show_in_menu->setTitle('Visibility');
		$show_in_menu->setContentUsingListMethod('showInMenuColumn');
		$this->addTCListColumn($show_in_menu);
		
		$login_default = new TCv_ListColumn('login_default');
		$login_default->setVerticalAlignment('middle');
		$login_default->setWidthAsPercentage(10);
		$login_default->setAlignment('center');
		$login_default->setTitle('Home');
		$login_default->setContentUsingListMethod('startPageColumn');
		$this->addTCListColumn($login_default);
		
		
//		$settings_button = $this->controlButtonColumnWithListMethod('moduleSettingsIconColumn');
//		$this->addTCListColumn($settings_button);
//
//		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
//		$this->addTCListColumn($edit_button);
//
//		$delete_button = $this->controlButtonColumnWithListMethod('updateIconColumn');
//		$this->addTCListColumn($delete_button);
		
		
	}
	
	/**
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		// In Tungsten 9, we don't show user groups at all
		if($model->folder() == 'user_groups')
		{
			return null;
		}
		
		$row = parent::rowForModel($model);
	
	
		
		return $row;
	
		
		
	}
	
	/**
	 * The icon column row
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function moduleIconColumn($model)
	{
		$link = new TCv_Link();
		$link->setIconClassName($model->iconCode());
		
		if($model->installed())
		{
			//$link->setURL('/admin/system/do/edit/'.$model->id());
			$link->setURL('/admin/'.$model->folder().'/');
		}
		return $link;
		
	}
	
	
	/**
	 * The title column
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function titleColumn($model)
	{
		$title_view = new TCv_View();
			
		if($model->installed())
		{
			$link = new TCv_Link();
			$link->addClass('module_title');
			//$link->setURL('/admin/'.$model->folder().'/');
			$link->setURL('/admin/system/do/edit/'.$model->id());
			$link->addText($model->title());
		}
		else
		{
			$link = new TCv_View();
			$link->setTag('span');
			$link->addClass('module_title');
			$link->addText($model->title());
		
		}
		$title_view->attachView($link);
		
	
		return $title_view;
	}

	/**
	 * The groups column row
	 * @param TSm_Module $module The module being shown
	 * @return bool|TCv_View
	 */
	public function groupsColumn($module)
	{
		$view = new TCv_View();
		
		if($module->installed())
		{
			if($module->folder() == 'system')
			{	
				$view->addText('<em>System Administrator</em>');
			}
			elseif($module->isAllAccess())
			{	
				$view->addText('<em>All access</em>');
			}
			else
			{
				$group_titles = array();
				foreach($module->permittedUserGroups() as $code => $group)
				{
					$group_link = new TCv_Link();
					$group_link->addText($group->title());
					$group_link->setURL('/admin/system/do/user-group-edit/'.$group->id());
					$view->attachView($group_link);
					
				}
			}
			
		}
		
		return $view;
	}
	
	/**
	 * The show in menu column
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function showInMenuColumn($model)
	{
		$view = new TCv_View();
		
		if($model->installed())
		{
			// If there is a hard-override, then that's what we show
			if(is_bool($model->showInLeftMenu()))
			{
				$icon = new TCv_View();
				$icon->addClass('list_control_button');
				$icon->addClass('immutable');
				if($model->showInLeftMenu())
				{
					$icon->addClass('fas fa-eye');
				}
				else
				{
					$icon->addClass('fas fa-eye-slash');
					
				}
				$view->attachView($icon);
			}
			else // editable
			{
				
				$visible_link = new TCv_Link();
				$visible_link->setTitle('Toggle visibility');
				$visible_link->addClass('list_control_button');
				if($model->showInMenu())
				{
					$visible_link->setIconClassName('fa fa-eye');
				}
				else
				{
					$visible_link->setIconClassName('fa fa-eye-slash');
				}
				$visible_link->setURL('/admin/system/do/module-toggle-show-in-menu/' . $model->id());
				$view->attachView($visible_link);
			}
		}
	
		return $view;
	}


	
	/**
	 * The start page column
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function startPageColumn($model)
	{
		if($model->installed() && $model->folder() != 'login')
		{
			$button = $this->listControlButton($model, 'module-set-as-start-page', 'fa-home');
			
			if(!$model->isLoginStartPage())
			{
				$button->addClass('faded');
			}
			
			return $button;
		}
		else
		{
			return new TCv_View();
		}
	}
	
	
	/**
	 * The edit column
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function editIconColumn($model)
	{
		if($model->installed())
		{
			return $this->listControlButton($model, 'edit', 'fa-pencil');
		}
		else
		{
			return new TCv_View();
		}
	}
	
	
	/**
	 * The update column
	 * @param TSm_Module $model The module being shown
	 * @return bool|TCv_View
	 */
	public function updateIconColumn($model)
	{
		if($model->installable())
		{
			$install_link = new TCv_Link();
			$install_link->setURL('/admin/system/do/module-install/'.$model->folder());
			$install_link->addClass('list_control_button');
			$install_link->setIconClassName('fa fa-download');
			
			return $install_link;
		
		}	
	}

	/**
	 * The help view
	 *
	 * @return bool|TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('The list of modules installed on this website. ');
			$help_view->attachView($p);
			
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Managing Access</strong>: Every module can be restricted to only appear to certain user groups. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Visibility</strong>: A module may be used without being shown in the main menus. To toggle the visibility of a button, use the  ');
				$link = new TSv_HelpLink('', '.view_url_target_link');
				$link->setIconClassName('fa-eye');
				$p->attachView($link);
			$p->addText(' link to switch if it can be seen.');
			$help_view->attachView($p);
			
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>URL Targets</strong>: Tungsten uses Module URL Targets to control which view is shown for which menu within the Tungsten Admin. You can use the ');
				$link = new TSv_HelpLink(' URL Targets', '.view_url_target_link');
				$link->setIconClassName('fa-external-link-square');
				$p->attachView($link);
			$p->addText(' link to see the list of defined URL targets. URL targets are defined in the module controller.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Admin Homepage</strong>: Use the  ');
				$link = new TSv_HelpLink('', '.TCv_ListColumn_login_default a');
				$link->setIconClassName('fa-home');
				$p->attachView($link);
			$p->addText(' button to specify which module should be shown whenever someone logs into Tungsten. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Updates</strong>: If an update is available for update, click on the upgrade button to run the installer. ');
			$help_view->attachView($p);
					


			return $help_view;
	}
		
}
?>