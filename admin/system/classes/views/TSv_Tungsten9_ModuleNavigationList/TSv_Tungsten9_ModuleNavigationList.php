<?php

/**
 * The navigation for the main module list in Tungsten.
 */
class TSv_Tungsten9_ModuleNavigationList extends TCv_View
{
	protected $module_button_list;
	
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->module_button_list = new TCv_View();
		$this->module_button_list->setTag('ul');
		$this->module_button_list->addClass('module_button_list');
	}
	
	/**
	 * Renders the list of module buttons
	 */
	public function render()
	{
		$this->addClassCSSFile('TSv_Tungsten9_ModuleNavigationList');
		
		$current_user = TC_currentUser();
		$current_module_folder = TC_currentModule()->folder();
		$is_installing = $current_module_folder == 'install';
		
		// Return empty if we're installing OR not logged in OR they don't have tungsten access
		if($is_installing || !$current_user || !$current_user->hasTungstenAccess())
		{
			return;
		}
		
		$module_list = TSm_ModuleList::init();
		$installed_modules = $module_list->installedModulesForUser($current_user);
		
		// Move system to the end, if it exists
		if(isset($installed_modules['system']))
		{
			$system_module = $installed_modules['system'];
			$system_module->setToShowInMenu(); // force it to show, only true in Tungsten 9
			unset($installed_modules['system']);
			$installed_modules['system'] = $system_module;
		}
		
		foreach($installed_modules as $module)
		{
			if($module->showInMenu() && $module->folderExists())
			{
				$is_current = $module->folder() == $current_module_folder;
				$li = new TCv_View();
				$li->setTag('li');
				
				$link = new TCv_Link('tungsten_menu_'.$module->folder());
				$link->addClass('module_button');
				$link->setURL('/admin/'.$module->folder().'/');
				$icon_view = new TCv_View();
				$icon_view->setTag('i');
				$icon_view->addClass('fal '.$module->iconCode());
				$link->attachView($icon_view);
				
				$title_box = new TCv_View();
				$title_box->setTag('span');
				$title_box->addClass('title_box');
				$title_box->addText($module->title());
				$link->attachView($title_box);
				
				$link->setTitle($module->title());
				if($is_current)
				{
					$link->addClass("current");
				}
				
				// @TODO: Deal with "notifications" of actions
				if($module->numNotifications() > 0)
				{
					$notification_icon = new TCv_View();
					$notification_icon->addClass('notification_icon');
					$notification_icon->addText($module->numNotifications());
					$link->attachView($notification_icon);
				}
				
				$li->attachView($link);
				
				if($is_current)
				{
					$li->attachView($this->sectionMenuForModule($module));
				}
				
				
				if($module->buttonColor())
				{
					$css = 'a#tungsten_menu_'.$module->folder().' { color:'.$module->buttonColor().' !important; } ';
					$this->addCSSLine('button_tungsten_menu_'.$module->folder(),$css);
					
				}
				
				
				// Attach the list item
				$this->module_button_list->attachView($li);
				
			}
			
		}
		
		$this->attachView($this->module_button_list);
	}
	
	public function attachView($view, $prepend = false)
	{
		// Detect if we're attaching a list item, if so, attach it to the module button list
		if($view->tag() == 'li')
		{
			$this->module_button_list->attachView($view, $prepend);
		}
		else
		{
			parent::attachView($view, $prepend);
		}

	}
	
	/**
	 * Configures the breadcrumb bar based on the current URL target
	 * @param TSm_Module $module
	 * @return TCv_Menu|null
	 */
	public function sectionMenuForModule(TSm_Module $module)
	{
		$current_url_target = TC_currentURLTarget();
		if(!$current_url_target)
		{
			return null ;
		}
		
		
		$menu = new TCv_Menu();
		
		foreach($module->urlTargetsByParent() as $url_target_name => $url_target)
		{
			if($url_target->isSubsectionParent() && $url_target->userHasAccess())
			{
				$menu->addClass('module_sections');
				$link = TSv_ModuleURLTargetLink::init($url_target);
				
				// Check if the currently viewed URL target is related to this top-level target
				if($current_url_target->relatedToName($url_target_name))
				{
					$link->addClass('active_menu');
				}
				$menu->attachView($link);
				
			}
		}
		
		if($menu->numItems() > 1)
		{
			return $menu;
		}
		
		return null;
		
	}
}