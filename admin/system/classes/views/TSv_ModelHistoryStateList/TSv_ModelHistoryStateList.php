<?php

/**
 * A list of history changes for a model. This list is very generalized, accepting any type of model and showing the
 * changes for that model.
 */
class TSv_ModelHistoryStateList extends TCv_ModelList
{
	protected ?TCm_Model $primary_model = null;
	protected array $model_schema;
	
	/**
	 * TSv_ModelHistoryStateList constructor.
	 * @param TCm_Model|TSt_ModelWithHistory|null $primary_model
	 */
	public function __construct($primary_model = null)
	{
		parent::__construct();
		
		$this->primary_model = $primary_model;
		$this->setModelClass('TSm_ModelHistoryState');
		$this->addModels($primary_model->historyStates());
		
		// Disabled so that it expands the center row always
		//$this->setAsHorizontalScrolling();
		$this->defineColumns();
		
		$this->addClassCSSFile('TSv_ModelHistoryStateList');
		
		$this->model_schema = $primary_model::schema();
		
	}
	
	/**
	 * @param TSm_ModelHistoryState $history_state
	 */
	public function rowForModel($history_state)
	{
		$row = parent::rowForModel($history_state);
		
		$row->addClass('action_'.$history_state->action());
		
		// DOn't bother with rows that have no changes
		$changes = $history_state->changesFlattened();
		if(count($changes) == 0)
		{
			return null;
		}
		
		return $row;
	}
	
	/**
	 *
	 */
	public function defineColumns()
	{
		$title = new TCv_ListColumn('date');
		$title->setWidthAsPixels(110);
		$title->setTitle('Date');
		$title->setContentUsingListMethod('dateColumn');
		$this->addTCListColumn($title);
		
		$title = new TCv_ListColumn('changes');
	//	$title->setWidthAsPixels(600);
		$title->setTitle('Changes');
		$title->setContentUsingListMethod('titleColumn');
		$title->setRightPadding(8);
		$this->addTCListColumn($title);
		
		$title = new TCv_ListColumn('user');
		$title->setWidthAsPixels(150);
		$title->setTitle('User');
		$title->setContentUsingListMethod('userColumn');
		$title->setAlignment('right');
		$this->addTCListColumn($title);
		
//		$title = new TCv_ListColumn('version');
//		$title->setWidthAsPixels(90);
//		$title->setTitle('Version');
//		$title->setContentUsingModelMethod('id');
//		$title->setAlignment('right');
//		$this->addTCListColumn($title);
	}
	
	/**
	 * @param TSm_ModelHistoryState $history_state
	 * @return mixed
	 */
	public function titleColumn(TSm_ModelHistoryState $history_state) : TCv_View
	{
		$column_container = new TCv_View();
		
		// Shortcut for the creation row
		if($history_state->action() == 'insert')
		{
			$column_container->setIDAttribute('created_changes_table');
			$created_box = new TCv_ToggleTargetLink();
			$created_box->addClass('created_title_box');
			$created_box->setIconClassName('fa-arrow-down');
			$created_box->addText("<span>Created</span>");
			$created_box->addClassToggle('#created_changes_table','showing');
			$column_container->attachView($created_box);
			
			// Detect if we have a partial history and note it
			if($history_state->version() == 'retroactive' &&  $history_state->className() == $this->primary_model?->baseClassName())
			{
				$incomplete_history_box = new TCv_View();
				$incomplete_history_box->addClass('incomplete_history');
				$incomplete_history_box->setTag('p');
				$incomplete_history_box->addText('<i class="fas fa-exclamation-circle"></i>');
				$incomplete_history_box->addText('This history is incomplete. Tracking started after the creation of this item.');
				$column_container->attachView($incomplete_history_box);
			}

//
//			return $column_container;
		}
		
		$column_container->addClass('changes_table');
		foreach($history_state->changesFlattened() as $history_change)
		{
			$column = $history_change->columnName();
			$action = $history_change->action();
			
			// Make the container
			$container = new TCv_View();
			$container->addClass('changed_value_box');
			$container->addClass('change_action_'.$action);
			
			if($history_change->isRelatedEntry())
			{
				$container->addClass('related');
			}
			
			if($history_change->isRelatedHeading())
			{
				$container->addClass('related_heading');
			}
			
			$container->addText('<div class="change_column_name">'
			                    . $history_change->columnDisplayTitle()
			                    . '</div>');
			
			$old = new TCv_View();
			$old->addClass('old');
			$old->addText($history_change->oldHTML());
			$container->attachView($old);
			
			$arrow = new TCv_View();
			$arrow->addClass('action_icon');
			$arrow->addClass($history_change->displayIconName());
			$container->attachView($arrow);

			$new = new TCv_View();
			$new->addClass('new');
			$new->addText($history_change->newHTML());
			$container->attachView($new);
			
			
			$revert = new TCv_View();
			$revert->addClass('revert_button');
			
			if(!is_null($history_change->oldHistoryID()) // The old ID needs to be set, since that's what we
				// reference for reversions
				&& TC_currentUser()->isAdmin()
				&& $history_state->userCanEdit()
				&& !$history_change->isInsert()
				&& !$history_change->isRelatedHeading())
			{
			
				// Add the reverse button
				$revert = new TCv_Link();
				$revert->addClass('revert_button');
				$revert->setTitle('Revert');
				$revert->setIconClassName('fa-history');
				$revert->setURL('/admin/system/do/history_revert/'.$history_change->className().'/'
				                .$history_change->oldHistoryID().'/'.$column);
				
			}
			$container->attachView($revert);
			
			
			$column_container->attachView($container);
		}
		
		
		return $column_container;
	}
	
	/**
	 * @param TSm_ModelHistoryState $model
	 * @return mixed
	 */
	public function dateColumn(TSm_ModelHistoryState $model)
	{
		return $model->dateFormatted('M j, Y \<b\r\/>g:ia');
	}
	
	/**
	 * @param TSm_ModelHistoryState $model
	 * @return mixed
	 */
	public function userColumn(TSm_ModelHistoryState $model)
	{
		$user = $model->user();
		if($user)
		{
			return $user->title();
		}
		
		return '–';
	}
	
	
	/**
	 * Help html
	 * @return string|null
	 */
	public static function defaultHelpHTML(): ?string
	{
		return '';
	}
	
}