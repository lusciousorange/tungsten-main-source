<?php
class TSv_APIBrowser extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $path_start = '/t-api';
	
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TSv_APIBrowser');
		
		$this->addClassJSFile('TSv_APIBrowser');
		$this->addClassJSInit('TSv_APIBrowser');
	}
	
	public function render()
	{
		// the root of the admin
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			if($module->usesAPI())
			{
				$this->attachView($this->moduleView($module));
				// Process the pages content folder for any content views that should be loaded
				
			}
			
		}
	}
	
	/**
	 * @param TSm_Module $module
	 * @return TCv_View
	 */
	protected function moduleView(TSm_Module $module)
	{
		$path_start = $this->path_start.'/'.$module->folder();
		
		$settings = $module->apiSettings();
		$view = new TCv_View('api_module_'.$module->folder());
		$view->addClass('api_module');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText($module->title());
		$view->attachView($heading);
		
		$this->addConsoleDebug($settings);
		
		foreach($settings as $values)
		{
			$setting_block = new TCv_View();
			$setting_block->addClass('setting_block');
			
			$full_path = $path_start.'/'.$values['path'];
			$full_path = str_ireplace('//','/', $full_path);
			
			$call = new TCv_Link();
			$call->setURL('#');
			$call->addClass('call_details');
			$call->addText('<span class="expand">'.TC_localize('details','Details').'</span>');
			$call->addText('<span class="method">'.$values['http_method'].'</span>');
			$call->addText('<span class="path">'.$full_path .'</span>');
			
			$setting_block->attachView($call);
			
			if(isset($values['description']))
			{
				$description = new TCv_View();
				$description->setTag('p');
				$description->addClass('description');
				$description->addText($values['description']);
				$setting_block->attachView($description);
			}
			
			$setting_block->attachView($this->parametersView($module, $values));
			$setting_block->attachView($this->returnView($module, $values));
			
			
			$view->attachView($setting_block);
		}
		
		return $view;
	}
	
	/**
	 * @param TSm_Module $module The module that is being requested
	 * @param array $api_values The values for the api
	 * @return TCv_View
	 */
	protected function parametersView($module, $api_values)
	{
		$view = new TCv_View();
		$view->addClass('parameter_values');
		$view->addClass('details_block');
		
		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText(TC_localize('parameters','Parameters'));
		$view->attachView($heading);
		
		if($api_values['http_method'] == 'POST' || $api_values['http_method'] == 'PATCH')
		{
			$view->attachView($this->schemaView($module, $api_values));
			
		}
		else
		{
			$view->addText('<p>'.TC_localize('no_parameters','No Parameters').'</p>');
			
		}
		
		return $view;
	}
	
	protected function generateReturnViewForArray($return_values)
	{
		$view = new TCv_View();
		$view->addClass('return_view');
		$view->addText("{<br />");
		
		foreach($return_values as $index => $explanation)
		{
			$view->addText('&nbsp;&nbsp;"'.$index.'" : ');
			
			// Nested values
			if(is_array($explanation))
			{
				$view->addText('<br />');
				$view->attachView($this->generateReturnViewForArray($explanation));
			}
			else
			{
				$view->addText($explanation);
				$view->addText('<br />');
			}
			
		}
		$view->addText("}<br />");
		
		return $view;
	}
	
	/**
	 * @param TSm_Module $module The module that is being requested
	 * @param array $api_values The values for the api
	 * @return TCv_View
	 */
	protected function returnView($module, $api_values)
	{
		$view = new TCv_View();
		$view->addClass('return_values');
		$view->addClass('details_block');
		
		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText(TC_localize('returns', 'Returns'));
		$view->attachView($heading);
		
		$p = new TCv_View();
		$model_name = $api_values['model'];
		
		if($model_name === 'current-user')
		{
			$model_name = TMm_User::classNameForInit();
		}
		
		$p->setTag('p');
		
		
		if(isset($api_values['returns']))
		{
			// array return values indicates the
			$return_value = $api_values['returns'];
			if(is_array($return_value))
			{
				$p->setTag('div');
				$p->attachView($this->generateReturnViewForArray($return_value));
				
			}
			else
			{
				$p->addText($api_values['returns']);
			}
			
		}
		
		// POST
		elseif($api_values['http_method'] == 'POST')
		{
			$p->addText(TC_localize('json_newly_created', "JSON for the newly created ")
			            .strtolower($model_name::$model_title) );
		}
		elseif($api_values['http_method'] == 'PATCH')
		{
			$p->addText(TC_localize('json_updated', "JSON for the updated ")
			            .strtolower($model_name::$model_title) );
		}
		elseif($api_values['http_method'] == 'GET')
		{
			$p->addText(TC_localize('json_for', "JSON for the  ")
			            .strtolower($model_name::$model_title) );
		}
		elseif($api_values['http_method'] == 'DELETE')
		{
			$return_value = [
				'deleted' => 'boolean – indicates if it was deleted',
				$model_name::$table_id_column => 'int - The ID of the item that was deleted'
			];
			// RETURN RESULTS ALWAYS THE SAME
			$p->setTag('div');
			$p->attachView($this->generateReturnViewForArray($return_value));
		}
		
		$view->attachView($p);
		
		return $view;
	}
	
	/**
	 * @param TSm_Module $module The module that is being requested
	 * @param array $api_values The values for the api
	 * @return TCv_View
	 */
	protected function schemaView($module, $api_values)
	{
		$view = new TCv_View();
		$schema = ($api_values['model'])::schema();
		
		foreach($schema as $field_id => $schema_field_values)
		{
			// Don't show deleted columns
			if(isset($schema_field_values['delete']))
			{
				continue;
			}
			// Don't show readonly fields
			if(isset($schema_field_values['readonly']) && $schema_field_values['readonly'] === true)
			{
				continue;
			}
			
			// Don't show values that are set as non-api-parameters
			if(isset($schema_field_values['is_api_param']) && !$schema_field_values['is_api_param'])
			{
				continue;
			}
				
			$block = new TCv_View();
			$block->addClass('field_block');
			
			$field_id_block = new TCv_View();
			$field_id_block->addClass('field_title');
			
			
			$id_box  = new TCv_View();
			$id_box->addClass("field_id");
			$id_box->addText($field_id);
			$field_id_block->attachView($id_box);
			
			$type_string = $this->typeStringForSchemaType($schema_field_values['type']);
			$type_box = new TCv_View();
			$type_box->addText('<span class="type_title">'.TC_localize('type','Type').'</span>');
			$type_box->addText('<span class="type_label">'.$type_string.'</span>');
			$field_id_block->attachView($type_box);
			
			
			
			$block->attachView($field_id_block);
		
			if(isset($schema_field_values['comment']))
			{
				$field_id_block = new TCv_View();
				$field_id_block->setTag('p');
				$field_id_block->addText($schema_field_values['comment']);
				$block->attachView($field_id_block);
				
			}
			$view->attachView($block);
			
			
			
		}
		
		return $view;
	}
	
	/**
	 * Determines the type string to show for a given type value
	 * @param string $type
	 * @return string
	 */
	public function typeStringForSchemaType($type)
	{
		if(substr($type,0,3) == 'TMm')
		{
			return TC_localize('integer','Integer');
		}
		
		if($type == 'text' || $type == 'datetime' || $type == 'date')
		{
			return 'string';
		}
		return $type;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		return [];
	}
	
	public static function pageContent_ViewTitle() : string  { return 'API browser'; }
	public static function pageContent_IconCode() : string  { return 'fa-code'; }
	public static function pageContent_IsAddable() : bool { return TC_getConfig('use_api'); }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public static function pageContent_ViewDescription() : string
	{
		return 'A browser for viewing the available APIs enabled on the website ';
	}
}