<?php
/**
 * Class TSv_GoogleMaterialIcons
 */
class TSv_GoogleMaterialIcons extends TSv_Plugin
{
	
	/**
	 * TSv_GoogleMaterialIcons constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Google Material Icons');
		$this->setPluginDescription('Loads the <a href="https://design.google.com/icons/" target="_blank" >Google Material Icon Font</a> which can be used throughout the system. ');
		$this->setPluginIconName('fa-font');
		$this->enableVariableForNum(1, 'Version', '4.5.0');
	}
	
	
	/**
	 *
	 */
	public function html()
	{
		$this->addCSSFile('google-material', 'https://fonts.googleapis.com/icon?family=Material+Icons');
	}
	
	
	
}
?>