<?php
/**
 * A view that adds site-wide functionality to the main TCv_Website view. Plugins are managed through the System module
 * and can be configured to load only on the public site or the admin, as well as only on the live or beta websites if
 * that's applicable.
 */
class TSv_Plugin extends TCv_View
{
	protected $plugin_model = false;
	
	protected $plugin_title = 'Plugin Title';
	protected $plugin_icon_name = 'plug';
	protected $plugin_description = 'Plugin Description';
	
	protected $public_required = false;
	protected $admin_required = false;
	protected $live_required = false;
	protected $beta_required = false;
	protected $deletable = true;
	
	protected $plugin_variables = array(
		1 => array('name' => false, 'value' => ''),
		2 => array('name' => false, 'value' => ''),
		3 => array('name' => false, 'value' => ''),
		4 => array('name' => false, 'value' => ''),
		5 => array('name' => false, 'value' => ''),
		6 => array('name' => false, 'value' => ''),
		7 => array('name' => false, 'value' => ''),
		8 => array('name' => false, 'value' => ''),
		
	);
	
	protected $plugin_default_install_values = array(
		'wait_to_load_on_rendering' => 0,
		'enable_public' => 1,
		'enable_admin' => 1,
		'enable_live_site' => 1,
		'enable_beta_site' => 1,
		'variable_1' => '',
		'variable_2' => '',
		'variable_3' => '',
		'variable_4' => '',
		'variable_5' => '',
		'variable_6' => '',
		'variable_7' => '',
		'variable_8' => '',
		
		
		);
	
	/**
	 * TSv_Console constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct();
		

		if($plugin_model instanceof TSm_Plugin)
		{
			$this->plugin_model = $plugin_model;
			// initialize values from the model
			for($i = 1; $i <= 8; $i++)
			{
				$this->plugin_variables[$i]['value'] = $plugin_model->variableValueForNum($i);
			}
			
			
		}
		
	}
	
	/**
	 * Calculates if the plugin should be shown with the provided settings for beta and admin
	 * @param bool $is_beta
	 * @param bool $is_admin
	 * @return bool
	 */
	public function determineIfShown($is_beta, $is_admin)
	{
		if(
			($is_beta && $this->betaRequired()) ||
			(!$is_beta && $this->liveRequired()) ||
			($is_admin && $this->adminRequired()) ||
			(!$is_admin && $this->publicRequired())
			
		)
		{
			return true;
		}
		
		return $this->plugin_model->determineIfShown($is_beta, $is_admin);
	}

	/**
	 * Sets the plugin title
	 * @param string $title
	 */
	public function setPluginTitle($title)
	{
		$this->plugin_title = $title;
	}
	
	/**
	 * A function that can be extended which is run when a plugin is not loaded.
	 */
	public function processNotLoaded()
	{
		
	}
	
	/**
	 * Returns the title for the plugin
	 * @return string
	 */
	public function pluginTitle()
	{
		return $this->plugin_title;
	}
	
	/**
	 * Sets the plugin description
	 * @param string $title
	 */
	public function setPluginDescription($title)
	{
		$this->plugin_description = $title;
	}
	
	/**
	 * Returns the plugin description
	 * @return string
	 */
	public function pluginDescription()
	{
		return $this->plugin_description;
	}
	
	/**
	 * Sets the plugin icon name
	 * @param string $name
	 */
	public function setPluginIconName($name)
	{
		$this->plugin_icon_name = $name;
	}
	
	/**
	 * returns the plugin icon name
	 * @return string
	 */
	public  function pluginIconName()
	{
		return $this->plugin_icon_name ;
	}

	//////////////////////////////////////////////////////
	//
	// REQUIREMENT FUNCTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the plugin as not deletable
	 */
	public  function setAsNotDeletable()
	{
		 $this->deletable = false;
	}
	
	/**
	 * Indicates that this plugin is required for the public side
	 */
	public  function setPublicAsRequired()
	{
		 $this->public_required = true;
	}
	
	/**
	 * Indicates that this plugin is required for the admin side
	 */
	public  function setAdminAsRequired()
	{
		 $this->admin_required = true;
	}
	
	/**
	 * Indicates that this plugin is required for the live site
	 */
	public  function setLiveAsRequired()
	{
		 $this->live_required = true;
	}
	
	/**
	 * Indicates that this plugin is required for the beta site
	 */
	public  function setBetaAsRequired()
	{
		 $this->beta_required = true;
	}
	
	/**
	 * Returns if it's required for the public
	 * @return bool
	 */
	public  function publicRequired()
	{
		return $this->public_required;
	}
	
	/**
	 * Returns if it's required for the admin
	 * @return bool
	 */
	public  function adminRequired()
	{
		return $this->admin_required ;
	}
	
	/**
	 * Returns if it's required for the live site
	 * @return bool
	 */
	public  function liveRequired()
	{
		return $this->live_required;
	}
	
	/**
	 * Returns if it's required for the beta
	 * @return bool
	 */
	public function betaRequired()
	{
		return $this->beta_required;
	}
	
	/**
 	 * Returns if it's deletable
	 * @return bool
	 */
	public  function deletable()
	{
		return $this->deletable;
	}
	
	/**
	 * Returns if this plugin is usable. Various reasons to disable a plugin on a site.
	 * @return bool
	 */
	public static function isUsable()
	{
		return true;
	}

	//////////////////////////////////////////////////////
	//
	// VARIABLES FUNCTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Set the values for one of the 8 plugin variables.
	 *
	 * @param int $num
	 * @param string $title
	 * @param string $default_value
	 */
	public  function enableVariableForNum($num, $title, $default_value = '')
	{
		$this->plugin_variables[$num]['name'] = $title;
		
		if($this->plugin_variables[$num]['value'] == '')
		{
			$this->plugin_variables[$num]['value'] = $default_value;
		}

		$this->plugin_default_install_values['variable_'.$num] = $default_value;
	}
	
	/**
	 * Returns the value for one of the plugin variables.
	 * @param int $num
	 * @return bool
	 */
	public  function pluginVariable($num)
	{
		if($this->plugin_variables[$num]['name'] !== false)
		{
			return $this->plugin_variables[$num]['value'];
		}
		return false;
	}
	
	/**
	 * Returns the name for one of the plugin variables.
	 *
	 * @param $num
	 * @return mixed
	 */
	public  function pluginVariableNameForNum($num)
	{
		return $this->plugin_variables[$num]['name'];
	}
	
	/**
	 * Sets the default install value
	 * @param string $variable_name
	 * @param string $value
	 */
	public function setDefaultInstallValue($variable_name, $value)
	{
		// only allow existing values to be set
		if(isset($this->plugin_default_install_values[$variable_name]))
		{
			$this->plugin_default_install_values[$variable_name] = $value;
		}
	}
	
	/**
	 * Returns the default install values
	 * @return array
	 */
	public function defaultInstallValues()
	{
		return $this->plugin_default_install_values;
	}

}
?>