<?php

/**
 *
 */
class TSv_ModuleURLTargetList extends TCv_ModelList
{
	/**
	 * @param $module
	 */
	public function __construct($module)
	{
		parent::__construct();
		
		$this->setModelClass('TSm_ModuleURLTarget');
		
		// Override default list to only acquire those 
		$this->addModels($module->urlTargetsByParent());
		
		$this->defineColumns();
	
	}
	
	/**
	 * @return void
	 */
	public function defineColumns()
	{
		
		$code = new TCv_ListColumn('code');
		$code->setTitle('Code');
		$code->setContentUsingListMethod('codeColumn');
		$this->addTCListColumn($code);
		
		$code = new TCv_ListColumn('path');
		$code->setWidthAsPercentage(20);
		$code->setTitle('Path');
		$code->setContentUsingListMethod('pathColumn');
		$this->addTCListColumn($code);
		
		$code = new TCv_ListColumn('model');
		$code->setTitle('Model (Instance)');
		$code->setWidthAsPixels(150);
		$code->setContentUsingListMethod('modelNameColumn');
		$this->addTCListColumn($code);
			
		$code = new TCv_ListColumn('view');
		$code->setWidthAsPixels(150);
		$code->setTitle('View');
		$code->setContentUsingListMethod('viewNameColumn');
		$this->addTCListColumn($code);
	
		$code = new TCv_ListColumn('action');
		$code->setTitle('Action Method');
		$code->setWidthAsPercentage(10);
		$code->setContentUsingListMethod('modelActionMethodColumn');
		$this->addTCListColumn($code);
		
		$code = new TCv_ListColumn('validation');
		$code->setTitle('Validation');
		$code->setWidthAsPercentage(15);
		$code->setContentUsingListMethod('validationColumn');
		$this->addTCListColumn($code);
		
		$column = $this->controlButtonColumnWithListMethod('moreInfoColumn');
		$this->addTCListColumn($column);
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View
	 */
	public function codeColumn($url_target)
	{
		$view = new TCv_View();
		
		// Handle Parent Indenting First
		$num_parent_levels = $url_target->numParentLevels();
		if($num_parent_levels > 0)
		{
			for($i = $num_parent_levels; $i--; $i > 0)
			{
				$view->addText('&nbsp;&nbsp;&nbsp;');
			}	
		}
		
		$name = $url_target->name();
		if($name == '')
		{
			$name = '<em>blank</em>';
		}
		$view->addText($name);
		
		
		return $view;
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View
	 */
	public function pathColumn($url_target)
	{
		$view = new TCv_View();
		$view->addText($url_target->pathWithExplanation());
		return $view;
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View
	 */
	public function modelNameColumn($url_target)
	{
		$view = new TCv_View();
		$view->addText($url_target->modelName());
		if($url_target->modelInstanceRequired())
		{
			$view->addText(' ( • )');
		}
		return $view;
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View
	 */
	public function viewNameColumn($url_target)
	{
		$view = new TCv_View();
		if($url_target->viewName() != '')
		{
			$view->addText($url_target->viewName());
		}
		return $view;
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View
	 */
	public function modelActionMethodColumn($url_target)
	{
		$view = new TCv_View();
		if($url_target->modelActionMethod() != '')
		{
			$view->addText(str_ireplace('()','', $url_target->modelActionMethod()).'()');
			$view->addText(' <br />>> '. $url_target->nextURLTargetName());
		}
		
		
		return $view;
	
	}
	
	/**
	 * @param $url_target
	 * @return TCv_View|null
	 */
	public function validationColumn($url_target)
	{
		$strings = [];
		foreach($url_target->validationCalls() as $values)
		{
			$method_name = $values['method_name'];
			$model_name = $values['model_name'];
			
			$function_call = $method_name;
			if(substr($function_call, 0, 2) != '::')
			{
				$function_call = '->'.$function_call;
			}
			$strings[] = $model_name.$function_call.'()';
			
		}
		
		if(count($strings) > 0)
		{
			$view = new TCv_View();
			$view->addText(implode('<br />', $strings));
			return $view;
		}
		
		return null;
	
	
	}
	
	
	/**
	 * @param $model
	 * @return TCv_Link
	 */
	public function moreInfoColumn($model)
	{
		
		$link = new TCv_Link();
		$link->setTitle('title');
		$link->setURL('#');
		
		$explanation = TSv_ModuleURLTargetExplanation::init($model);
		
		$link->addClassCSSFile('TSv_ModuleURLTargetExplanation');
		
		$link->useDialog($explanation->html(), 'fa-info-circle', '#333', true); // use fullscreen
		
		$link->addClass('list_control_button');
		
		$link->addClass('fa-info-circle');
			
		
		//$link = $this->listControlButton($model, '', 'fa-info', true);
		return $link;
	}
	
	
	

		
}
?>