<?php
/**
 * Class TSv_JQuery
 */
class TSv_JQuery extends TSv_Plugin
{
	/**
	 * TSv_JQuery constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('jQuery');
		$this->setPluginDescription('Loads the <a href="https://jquery.com/" target="_blank" >jQuery JavaScript Library</a> ('.$this->pluginVariable(1).') and the accompanying UI Library ('.$this->pluginVariable(2).'). Both libraries are loaded through the Google Hosted API system.');
		$this->setPluginIconName('fa-code');

		// Replaced with hard-coded so we can control this on sites with updates. Otherwise it's a weird setting that
		// gets lost in transition
		// $this->enableVariableForNum(1, 'Remote jQuery Version', '3.3.1');
		// $this->enableVariableForNum(2, 'Remote jQuery UI Version', '1.12.1');

//		$this->setPublicAsRequired();
//		$this->setAdminAsRequired();
//		$this->setLiveAsRequired();
//		$this->setBetaAsRequired();
	//	$this->setAsNotDeletable();
	}
	
	
	/**
	 * Adds the files
	 */
	public function render()
	{
		$this->addJSFile('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js');
		$this->addJSFile('jquery_ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js');
		$this->addCSSFile('jquery_ui_css', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/themes/base/jquery-ui.min.css');
	}
	
	
	
}
?>