<?php
class TSv_UserImpersonationForm extends TCv_Form
{
	public function __construct()
	{
		parent::__construct('impersonation_form');

		$this->setButtonText('Impersonate User');

	}

	public function configureFormElements()
	{
		if(isset($_SESSION['impersonation_id']))
		{
			/** @var TMm_User $impersonated_user */
			$impersonated_user = TMm_User::init($_SESSION['impersonation_id']);

			$field = new TCv_FormItem_HTML('current_impersonation','Public Impersonating');
			$field->addText($impersonated_user->fullName().' '.$impersonated_user->email());
			$field->addText(' – ');

			$cancel_button = new TCv_Link();
			$cancel_button->addText("Cancel");
			$cancel_button->setURL('/admin/system/do/cancel-impersonation/');

			$field->attachView($cancel_button);
			$this->attachView($field);
		}
		else
		{
			$field = new TCv_FormItem_Select('public_site_user_id', 'User');
			$field->setIsRequired();
			$field->addOption('','Enter the name of a user that you want to impersonate');

			$user_list = TMm_UserList::init();
			$field->useFiltering($user_list,'namesAndEmailsForSearchString', 'TMm_User');



			$this->attachView($field);

		}


	}

	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		$new_public_user_id = $form_processor->formValue('public_site_user_id');
		if($new_public_user_id !== TC_currentUser()->id())
		{
			$_SESSION['impersonation_id'] = $new_public_user_id;
		}
		
		header("Location: /"); exit();

	}


}