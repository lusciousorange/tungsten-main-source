
document.addEventListener('DOMContentLoaded',() => {

	// Detect any click and determine if it happened on a ModuleURLTarget that's set to
	// load the view in a dialog
	let main = document.querySelector('main');
	if(main) {
		main.addEventListener('click', (event) => {
			let clicked_link = event.target.closest('.TSv_ModuleURLTargetLink.load_view_in_dialog');
			if(clicked_link) {
				event.preventDefault();
				loadModuleURLTargetLink_ViewInDialogClick(clicked_link);
			}
		});
	}
});


/**
 * Loads the view for a given Module
 * @param button
 */
function loadModuleURLTargetLink_ViewInDialogClick(button) {

	let title = button.getAttribute('title');

	let loading_view = document.querySelector(button.getAttribute('data-loading-view'));

	if(loading_view)
	{
		loading_view.classList.add('tungsten_loading');
	}

	let url = button.getAttribute('href')+'/?return_type=json';


	fetch(url,{
		method : 'GET'
	})	.then(response => response.json())
		.then(response => {

			// Show the title based on the title of the link
			if(title !== null && typeof title == "string")
			{
				response.html = '<h2>'+ title + '</h2>' + response.html;
			}

			window.W.TCv_Dialog.setMessage(response.html);

			if(response.js)
			{
				processAsyncJSLinks(response.js);
			}
			if(response.css)
			{
				processAsyncCSSLinks(response.css);
			}

			window.W.TCv_Dialog.setFullScreen(true);
			window.W.TCv_Dialog.setCancelText('');
			window.W.TCv_Dialog.show();


			if(loading_view)
			{
				loading_view.classList.remove('tungsten_loading');
			}
	});


}