<?php
/**
 * Class TSv_ModuleURLTargetLink
 *
 * A link that is created using a Module URL Target in the system. This class serves as a top-level control that will
 * only generate relevant values if the current user has sufficient privileges to see it. If the URL Target can't be
 * generated, then the class returns false to avoid any HTML being generated.
 */
class TSv_ModuleURLTargetLink extends TCv_Link
{
	protected $url_target = false;
	protected $model = null;
	protected $use_generic_title = false;
	protected $text_override = false;
	protected $is_valid = true;
	protected $basic_text_committed = false;
	
	protected $url_additional_values = array();
	
	public static bool $disable_tungsten_caching = true; // turn this value on in a subclass to ensure it never gets cached. This may lead to more DB queries.
	
	/**
	 * TSv_ModuleURLTargetLink constructor.
	 * @param string|TSm_ModuleURLTarget $url_target
	 */
	public function __construct($url_target)
	{
		parent::__construct();
		
		$controller = TC_currentModuleController();
			
		if($url_target instanceof TSm_ModuleURLTarget)
		{
			$this->url_target = $url_target;
		}
		else // string, need to instantiate
		{
			if($controller)
			{
				$this->url_target = $controller->URLTargetWithName($url_target);
				
			}
	
		}
		
		if(!$this->url_target)
		{
			return false;
		}
		// return false if the URL Target can't be validated
		if(!$this->url_target->validateAccess())
		{
			$this->is_valid = false;

		}
		$this->setTitle($this->url_target->title());
		
		$this->addClass('url-target-'.$this->url_target->name());

		if($this->url_target->openInNewWindow())
		{
			$this->openInNewWindow();
		}

	}
	
	/**
	 * Sets the link to use the generic title from the URL Target
	 */
	public function useGenericTitle()
	{
		$this->use_generic_title  = true;
		$this->setTitle($this->url_target->title(true));
		
	}
	

	/**
	 * Adds text which overrides the default text which is normally the title being derived from the URL Target
	 * model. WHen using this method, only the text provided will be shown and not the default auto-generated text
	 *
	 * @param string $text
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.

	 */
	public function addOverrideText($text, $disable_template_parsing = false)
	{
		$this->text_override = true;
		parent::addText($text, $disable_template_parsing);
	}
	
	/**
	 * Compares the URL target to the currently viewed URL target and adds CSS classes to indicate if the link is a
	 * parent, active, or current URL target.
	 */
	public function addRelatedMenuClasses()
	{
		// this is probably one of the few "cheats" left in this entire setup. 
		// fixing it is wide-spread
		$current_url_target = TC_website()->currentModule()->controller()->currentURLTarget();
		
		if($current_url_target->module()->folder() == $this->url_target->module()->folder())
		{
			if($current_url_target->isParentURLTarget($this->url_target))
			{
				$this->addClass('parent_menu');
			}
			
			if($current_url_target->relatedToName($this->url_target->name()))
			{
				$this->addClass('active_menu');
			}
			
			if($current_url_target->name() == $this->url_target->name())
			{
				$this->addClass('current_menu');
			
			}	
		}
		
	}	
	
	/**
	 * Adds an additional value that will be appended to the end of the url. These are manually added when
	 * instantiating a URL target.
	 * @param string $additional_value
	 */
	public function addURLAdditionalValue($additional_value)
	{
		$this->url_additional_values[] = $additional_value;
	}
	
	/**
	 * Appends to the URL
	 * @param string $append
	 */
	public function appendToURL($append)
	{
		$append = explode('/', $append);
		foreach($append as $append_string)
		{
			if(trim($append_string) != '')
			{
				$this->url_additional_values[] = $append_string;
			}
		}
	}
	
	
	/**
	 * Extend the attachView functionality to force the existing text to be added. Ensures proper stacking.
	 *
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return bool|string|TCv_View|void
	 */
	public function attachView($view, $prepend = false)
	{
		$this->commitBasicText();
		parent::attachView($view, $prepend);
	}
					
	/**
	 * Extend the addText functionality to force the existing text to be added. Ensures proper stacking.
	 *
	 * @param string $text
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.

	 */
	public function addText($text, $disable_template_parsing = false)
	{
		$this->commitBasicText();
		parent::addText($text, $disable_template_parsing);
	}
					
	/**
	 * @return bool
	 */
	public function isValid()
	{
		return $this->is_valid;
	}
	
	/**
	 * Adds the basic text to the view
	 */
	public function commitBasicText()
	{
		if(!$this->basic_text_committed)
		{
			$this->basic_text_committed = true;
			if(!$this->text_override)
			{
				if($this->url_target)
				{
					parent::addText($this->url_target->title($this->use_generic_title));
				}
				
			}
			
		}
	}
	
	/**
	 * Extend function to properly clear out any previous content
	 */
	public function detachAllViews()
	{
		$this->basic_text_committed = true;
		parent::detachAllViews();
	}
	
	public function url()
	{
		$url = $this->url_target->url();
		if(sizeof($this->url_additional_values) > 0)
		{
			$url .= '/'.implode('/', $this->url_additional_values);
		}
		
		return $url;
	}
	
	
	/**
	 * @return string
	 */
	public function html()
	{
		if(!$this->is_valid)
		{
			return '';
		}
		
		if($this->url_target)
		{
			$this->commitBasicText();
			
			$this->setURL($this->url());
			
		}
		
		
		
		return parent::html();		
	}
	
	/**
	 * @param string $loading_view
	 */
	public function loadViewInDialog($loading_view = '')
	{
		$this->addClass('load_view_in_dialog');
		if($loading_view != '')
		{
			$this->addDataValue('loading-view', $loading_view);
		}
		
		// NOTE: JS loaded once on the site and listener setup to detect any click
	}
	
	
}
?>