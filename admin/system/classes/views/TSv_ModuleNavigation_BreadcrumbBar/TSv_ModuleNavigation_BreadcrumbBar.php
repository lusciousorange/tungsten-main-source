<?php
/**
 * Class TSv_ModuleNavigation_BreadcrumbBar
 *
 * The breadcrumbs in Tungsten
 */
class TSv_ModuleNavigation_BreadcrumbBar extends TCv_Menu
{
	/** @var array  */
	protected $right_menus = array(); // [array]
	protected $breadcrumbs = array();
	
	/**
	 * TSv_ModuleNavigation_BreadcrumbBar constructor.
	 * @param bool $id (Optional) Default false.
	 */
	public function __construct($id = false)
	{
		parent::__construct('breadcrumb_menus');
		
		$this->addClassCSSFile('TSv_ModuleNavigation_BreadcrumbBar');
		
	}
	
	/**
	 * Extends the attachView method to add the view to the breadcrumbs
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 */
	public function attachView($view, $prepend = false)
	{
		$this->attachBreadcrumbView($view);
	}
	
	/**
	 * @param TCv_View $view
	 */
	public function attachBreadcrumbView($view)
	{
		$this->breadcrumbs[] = $view;
	
	}
	
	/**
	 * @param string $title
	 * @param bool|string $link
	 */
	public function addBreadcrumb($title, $link = false)
	{
		if($link)
		{
			$view = new TCv_Link();
			$view->setTitle($title);
			$view->setURL($link);
		}
		else
		{
			$view = new TCv_View();
			$view->setTag('span');
		}
		
		$view->addText($title);
		$this->attachBreadcrumbView($view);
		
	}
	
	/**
	 * Adds a menu item, link or view to the right side
	 * @param TCv_View $view
	 */
	public function addRightView($view)
	{
		if($view instanceof TCv_Link)
		{
			if($view->contentIsOnlyIcon())
			{
				$view->addClass('icon_only');
			}
		}
		
		$this->right_menus[] = $view;
	}
	
	/**
	 * Removes the last breadcrumb from the list
	 */
	public function removeLastBreadcrumb()
	{
		array_pop($this->breadcrumbs);
	}
	
	/**
	 * Returns the list of breadcrumbs
	 * @return TCv_View[]
	 */
	public function breadcrumbViews()
	{
		return $this->breadcrumbs;
	}

	/**
	 * Returns the right views
	 * @return TCv_View[]
	 */
	public function rightView()
	{
		return $this->right_menus;
	}

	
	/**
	 * @return string
	 */
	public function html()
	{
		$num_right_menus = 0;
		$icons_buttons_only = true;
		$li = new TCv_View();
		$li->setTag('li');
		$li->addClass('right_menu_container');
		
		foreach($this->right_menus as $right_menu_view)
		{
			// Check validity for Module URL links
			$is_valid = true;
			if( $right_menu_view instanceof TSv_ModuleURLTargetLink && !$right_menu_view->isValid())
			{
				$is_valid = false;
			}
				// Only bother if it is a valid view
			if($is_valid)
			{
				$container = new TCv_View();
				$container->addClass('right_menu');
				$container->attachView($right_menu_view);
				$li->attachView($container);
				$num_right_menus++;
				
				if(!$right_menu_view->hasClass('icon_only'))
				{
					$icons_buttons_only = false;
				}
			}
			
			
		}
		
		if($num_right_menus > 0)
		{
			$this->addClass('menu_count_right_'.$num_right_menus);
			if(!$icons_buttons_only)
			{
				$li->addClass('full_width');
			}
			parent::attachView($li);
					
		}
		
		
		
		
		$count = 1;
		$page_title_parts = array();
		$last_page_title_part = 'no title';
		foreach($this->breadcrumbs as $breadcrumb_view)
		{
			$title = false;
			if(method_exists($breadcrumb_view, 'title'))
			{
				$title = $breadcrumb_view->title();
			}
			
			$li = new TCv_View();
			$li->setTag('li');
			$li->addClass('breadcrumb');
			if($count == 1)
			{
				$li->addClass('first');
			}
			$li->attachView($breadcrumb_view);
			$count++;
			
			// Avoid double titles, which happen and looks awkward
			if($title != $last_page_title_part || $title === false)
			{
				$page_title_parts[] = $title;
				parent::attachView($li);
			}
			$last_page_title_part = $title;
		}
		
		// Set the title of the page then lock it.
		// lock avoids content items setting the page title in the pages module
		$this->setBrowserPageTitle(implode(' – ', $page_title_parts), true); 



		return parent::html();
	}
	
}	
?>