<?php
	$skip_tungsten_authentication = true; // need it to work for all pages
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 
	
	$console = new TSv_Console(false);
	
	$console->buildConsole();
		
	print $console->consoleHeader()->html();
	print $console->consoleListingTable()->html();
	
?>