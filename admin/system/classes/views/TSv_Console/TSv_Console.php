<?php
/**
 * Class TSv_Console
 *
 * The console that is shown on the website when a page is loaded. The purpose of the console is to consolidate all the
 * errors and debuggging information from a site into a single viewer. This class is normally already instantiated and
 * handled at the highest level in the TC_config.php file. This is to ensure that all files for the site will always
 * have access to the console. Many classes including TCu_Item have hooks to use the console seamlessly.
 *
 */
class TSv_Console extends TSv_Plugin
{
	protected $num_to_load = 30;
	
	
	protected $active_page = ''; // [string] = The string identifier for the active page
	protected $is_visible = true;
	
	protected $last_memory_count = 0;
	protected $last_time_count = 0;
	protected $page_time_start = 0;

	protected $console_table = false;
	protected $console_table_rows = array();
	protected $num_columns = 6;
	protected $type_names_used = array();
	
	protected $type_names_safe = array();
	protected $row_count = 0;
	
	protected $page_start_time = 0;
	protected $last_console_time = 0;
	
	/**
	 * TSv_Console constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{	
		parent::__construct($plugin_model);
		
		// PLUGIN SETTINGS
		$this->setPluginTitle('Developer Console');
		$this->setPluginDescription('Installs a developer console. ');
		$this->setPluginIconName('fa-bug');
		
		$this->setDefaultInstallValue('enable_live_site', 1);		
		$this->setDefaultInstallValue('wait_to_load_on_rendering', 1);	
			
		$this->setAsNotDeletable();
	

	}
	
	/**
	 * Processes the types for a console item
	 * @param TSm_ConsoleItem $console_item
	 */
	protected function processTypes(&$console_item)
	{
		if(isset($this->type_names_safe[$console_item->typeName()]))
		{
			$type_name_safe = $this->type_names_safe[$console_item->typeName()];
		}
		else
		{
			$type_name_safe = strtolower(str_ireplace(' ', '_', $console_item->typeName()));
			$this->type_names_safe[$console_item->typeName()] = $type_name_safe;
		}
		
		
		$this->type_names_used[$type_name_safe] = $console_item->typeName();
		
		// ERROR COUNT
		if($console_item->isError())
		{
			$this->type_names_used['error'] = 'Error';
		}
	}
	
	/**
	 * Processes the page value to indicate if a new page has been triggered
	 *
	 * @param TSm_ConsoleItem $console_item
	 * @return TCv_HTMLTableRow
	 */
	protected function pageRow(TSm_ConsoleItem $console_item) : TCv_HTMLTableRow
	{
		$this->last_console_time = $console_item->timeProcessed();
			
		$page_row = new TCv_HTMLTableRow();
		$page_row->addClass('console_page_title console_item_row');
		$page_row->addDataValue('row-id',$console_item->id());
		
			$cell = new TCv_HTMLTableCell();
			$cell->setColumnSpan($this->num_columns - 1);
			$cell->addText('Page: '. str_ireplace('&',"&\n", $console_item->page()));
			$page_row->attachView($cell);
			
			
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_options');
			
	
	
			if($console_item->details())
			{
				$link = new TCv_Link();
				$link->addClass('details_button');
				$link->setURL('#');
				$link->addText('Details');
				$cell->attachView($link);
			}
//			if($console_item->hasBacktrace())
//			{
//
//				$link = new TCv_Link();
//				$link->addClass('backtrace_button');
//				$link->setURL('#');
//				$link->addText('Trace');
//				$cell->attachView($link);
//			}
			$page_row->attachView($cell);
		
		return $page_row;
	}
	
	/**
	 * Builds the main console row for console item
	 *
	 * @param TSm_ConsoleItem $console_item
	 * @return TCv_HTMLTableRow
	 */
	protected function mainConsoleRow($console_item)
	{
		// COUNT NUMER
		$this->row_count++;
		
		$console_row = new TCv_HTMLTableRow();
		$console_row->addClass('console_item_row');
		$console_row->addClass('console_'.$this->type_names_safe[$console_item->typeName()]);
		$console_row->addDataValue('row-id',$console_item->id());
		
		if($console_item->isError())
		{ 
			$console_row->addClass('console_error');
		}
		elseif($console_item->isSkipped())
		{ 
			$console_row->addClass('console_skipped');
		}
		if($console_item->dbTransactionID())
		{ 
			$console_row->addClass('transaction transaction_'.$console_item->dbTransactionID());
		}
			

			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_count');
			$cell->addText($this->row_count);
			$console_row->attachView($cell);
				
			// MAIN MESSAGE
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_message');
			

			if($console_item->isMultiColumn())
			{
				$column_number_word = 'three';
				if($console_item->message3() == '')
				{
					$column_number_word = 'two';
				}
				$message = '';
				for($i = 1; $i <= 3; $i++)
				{
					$var_name = 'message'.$i;
					
					$content = trim($console_item->$var_name());
					if($content == '')
					{
						// Do nothing$content = '&nbsp;';
					}
					else
					{
						$message .= '<span class="console_'.$column_number_word.'_column_'.$i.'">'.$content.'</span>';	
					}
					
				}
					
			}
			else
			{
				$message = $console_item->message();	
			}
			//$message .= ' – '.$console_item->pagePositionCount();
			
			if($console_item->details())
			{
				$link = new TCv_Link();
				$link->setURL('#');
				$link->addClass('expand_button');
				$link->addText($message, true);
				$cell->attachView($link);
			}
			else
			{
				$cell->addText($message, true);
			}
			$console_row->attachView($cell);
			
			
			
			
			// TIME SPENT
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_time_spent');
			$text = '–';
			if($console_item->timeSpent() > 0)
			{
				$text = number_format($console_item->timeSpent()*1000,3).'ms';
			}
			
			//$this->last_console_time = $console_item->timeProcessed();
			$cell->addText($text);//number_format($time_difference,2).'s');
			
			$console_row->attachView($cell);
			
			// TYPE NAME
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_type');
			$cell->addText($console_item->typeName());
			$console_row->attachView($cell);
			
			// CLASS NAME
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_class');
			$cell->addText($console_item->className());
			$console_row->attachView($cell);
			
			// OPTIONS
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('console_column_options');
			
			if($console_item->details())
			{
				$link = new TCv_Link();
				$link->addClass('details_button');
				$link->setURL('#');
				$link->addText('Details');
				$cell->attachView($link);
			}
			if($console_item->hasBacktrace())
			{
		
				$link = new TCv_Link();
				$link->addClass('backtrace_button');
				$link->setURL('#');
				$link->addText('Trace');
				$cell->attachView($link);
			}	
			$console_row->attachView($cell);
			
		return $console_row;
			
				
					
	}		
			
	/**
	 * Builds the details row for the console item
	 * @param TSm_ConsoleItem $console_item
	 * @return bool|TCv_HTMLTableRow
	 */
	protected function detailsRow(&$console_item)
	{
		if($console_item->details())
		{
			$id = 'console_desc_'.$console_item->id();
		
			$console_row = new TCv_HTMLTableRow($id);
			$console_row->addClass('console_description');
			$console_row->addClass('console_description_'.$this->type_names_safe[$console_item->typeName()]);
			$console_row->addClass('console_'.$this->type_names_safe[$console_item->typeName()]);
			$console_row->addDataValue('row-id',$console_item->id());
			if($console_item->isError())
			{ 
				$console_row->addClass('console_error');
			}
			$console_row->setAttribute('style', "display:none;");
			
				$cell = new TCv_HTMLTableCell();
				$cell->addClass('console_column_count');
				$console_row->attachView($cell);
			
				$cell = new TCv_HTMLTableCell();
				$cell->setColumnSpan($this->num_columns-1);
				$cell->addText($console_item->details());
				$console_row->attachView($cell);
			
			return $console_row;
		}
		return false;	
	}
	
	/**
	 * Builds the trace row for console item
	 *
	 * @param TSm_ConsoleItem $console_item
	 * @return bool|TCv_HTMLTableRow
	 */
	protected function traceRow(&$console_item)
	{
		if($console_item->hasBacktrace())
		{
			$id = 'console_backtrace_'.$console_item->id();
			

			$console_row = new TCv_HTMLTableRow($id);
			$console_row->addClass('console_backtrace');
			$console_row->addClass('console_backtrace_'.$this->type_names_safe[$console_item->typeName()]);
			$console_row->addClass('console_'.$this->type_names_safe[$console_item->typeName()]);
			$console_row->addDataValue('row-id', $console_item->id());
			if($console_item->isError())
			{ 
				$console_row->addClass('console_error');
			}
			$console_row->setAttribute('style', "display:none;");
			
				$cell = new TCv_HTMLTableCell();
				$cell->addClass('console_column_count');
				$console_row->attachView($cell);
			
				$cell = new TCv_HTMLTableCell();
				$cell->setColumnSpan($this->num_columns-1);
				
				$cell->attachView($this->createTableForBacktraceJSON($console_item->backtrace()));
				//$cell->addText($console_item->backtrace());
				$console_row->attachView($cell);
			
			return $console_row;
		}
		
		return false;
			
	}
	
	/**
	 * Converts a json string of the backtrace into an HTML table to be shown on the screen
	 * @param string $backtrace_json
	 * @return TCv_HTMLTable
	 */
	protected function createTableForBacktraceJSON(string $backtrace_json) : TCv_HTMLTable
	{
		$table = new TCv_HTMLTable();
		$table->addClass('console_backtrace_table');
		
		// Header row
		$row = new TCv_HTMLTableRow();
		$row->createHeadingCellWithContent('#','num_col');
		$row->createHeadingCellWithContent('File','file_col');
		$row->createHeadingCellWithContent('Line','line_col');
		$row->createHeadingCellWithContent('Call','call_col');
		$table->attachView($row);
		
		$backtrace = json_decode($backtrace_json, true);
		$number = 1;
		foreach($backtrace as $backtrace_row)
		{
			foreach($backtrace_row as $index => $value)
			{
			//	print '** '.$index.' === '.$value;
			}
			
			$row = new TCv_HTMLTableRow();
			$row->createCellWithContent($number);
			$row->createCellWithContent($backtrace_row['f']);
			$row->createCellWithContent($backtrace_row['l']);
			$row->createCellWithContent($backtrace_row['d']);
			$table->attachView($row);
			$number++;
		}
		
		return $table;
	}
	
	/**
	 * Builds the console header
	 * @return TCv_View
	 */
	public function consoleHeader()
	{
		
		$header = new TCv_View('TSv_Console_Header');
		
		$filter_bar = new TCv_View('TSv_Console_filterBar');
		
		// Create the filter options
		$filter_bar->addText('<span class="console_title">Developer Console</span>');
		$filter_bar->addText('<a href="#" class="TSv_ConsoleButton_hide fas fa-times"></a>');
		
		$header->attachView($filter_bar);
		
		return $header;
		
	}
		
	/**
	 * Builds the console listing table
	 * @return TCv_View
	 */
	public function consoleListingTable()
	{
		
		$this->console_table = new TCv_View('console_table');
		$this->console_table->setTag('table');
		foreach($this->console_table_rows as $row_html)
		{
			$this->console_table->addText($row_html);
		}
		
		return $this->console_table;
		
		
	}
	
	/**
	 * Returns all the HTML for this item
	 * @return string
	 */
	public function html()
	{
		// Console Saving turned off. Don't return anything
		if(!TC_getConfig('console_saving'))
		{
			return parent::html();
		}

		// Load Necessary
		$this->setIDAttribute('TSv_Console');
		$this->addClassCSSFile('TSv_Console', true);
		$this->addClassJSFile('TSv_Console');
		$this->addClassJSInit('TSv_Console');
		
		$this->attachView($this->consoleHeader());
		
		$scroller = new TCv_View('console_scroller_container');
//		$feed_container = new TCv_View('');
//		$feed_container->setTag('table');
//		$tbody = new TCv_View();
//		$tbody->setTag('tbody');
//		$feed_container->attachView($tbody);
//		$scroller->attachView($feed_container);
		$this->attachView($scroller);
		
		$loading_container = new TCv_View();
		$loading_container->addClass('loading_container');
			$loading_icon = new TCv_View();
			$loading_icon->setTag('i');
			$loading_icon->addClass('fa-circle-notch');
			$loading_icon->addClass('fa-spin');
			$loading_icon->addClass('fa-fw');
			
			$loading_container->attachView($loading_icon);
		$this->attachView($loading_container);
		

		return parent::html();
		
	}
	
	/**
	 * Returns an array of views for the console item
	 *
	 * @param TSm_ConsoleItem $console_item
	 * @return TCv_HTMLTableRow[]
	 */
	public function rowViewsForConsoleItem($console_item)
	{
		$this->processTypes($console_item);
			
		$views = array();
		if($console_item->type() == TSm_ConsolePageStart)
		{
			$views[] = $this->pageRow($console_item);
		}
		else
		{
			$views[] = $this->mainConsoleRow($console_item);
		}
		$views[] = $this->detailsRow($console_item);
		$views[] = $this->traceRow($console_item);
		
		return $views;
	}		
	
	/**
	 * Returns the next views as JSON that works with the view
	 * @param string $last_parent_microtime
	 * @param int $last_number
	 * @param null|string $last_instance_id The last instance that was called. If we hit the end, the value is null
	 * to indicate we should grab the next parent microtime
	 */
	public function nextScrollerViewsJSON($last_parent_microtime, $last_number, $last_instance_id)
	{
		$return_values = array();
		$return_values['items'] = array();
		$this->row_count = $last_number; // start counting at the right place
		$num_loaded = 0;
		$last_num = 0;

		$console_item_list = TSm_ConsoleItemList::init();
		
		// Get the console items
		$console_items = [];
		
		// We have an existing parent microtime
		if($last_parent_microtime > 0)
		{
			$console_items = $console_item_list->consoleItemsWithMicrotimeInstanceIdWithQuantity($last_parent_microtime, $last_instance_id, $this->num_to_load);
		}
		
		// We're below the threshold, so we finished, grab some more
		if(count($console_items) < $this->num_to_load)
		{
			$more_console_items = $console_item_list->consoleItemsBeforeMicrotimeWithQuantity($last_parent_microtime,
			                                                                             $this->num_to_load);
			$console_items = array_merge($console_items,$more_console_items);
		}
		
		$num_added = 0;
		foreach($console_items as $console_item)
		{
			$last_parent_microtime = $console_item->parentMicrotime();
			$last_instance_id = $console_item->instanceID();
			$console_item_values = array();
			$content_values['view_html'] = '';
			foreach($this->rowViewsForConsoleItem($console_item) as $view)
			{
				if($view instanceof TCv_View)
				{
					$content_values['view_html'] .= $view->html();
				}
				$num_added++;			
			}
			$return_values['items'][] = $content_values;
				
		}	
		
		// is_done is only really set if there are no more items. 
		// until we return nothing, there might be more
		
		$return_values['is_done']  = ($num_added == 0) +0; // return an int
		$return_values['last_parent_microtime'] = $last_parent_microtime;
		$return_values['last_instance_id'] = $last_instance_id;
		$return_values['last_number']  = $this->row_count;

		header('Content-Type: application/json');
		echo json_encode($return_values);
		
		
		
	}
	
}
?>