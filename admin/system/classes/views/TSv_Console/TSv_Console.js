class TSv_Console {
	element = null;
	loading_container = null;
	scroller_container = null;
	is_done = false;
	fetch_abort_controller = null;
	last_parent_microtime = 0;
	last_instance_id = null;
	last_index		= 0;

	options = {
		param			:	false,
	};

	/**
	 * @param {Element} element
	 * @param {Object} params
	 */
	constructor(element, params) {
		this.element = element;
		this.loading_container = this.element.querySelector('.loading_container');
		this.scroller_container = this.element.querySelector('#console_scroller_container');
		// Save config settings
		Object.assign(this.options, params);

		let show_button = document.querySelector('.TSv_ConsoleButton_show');
		if(show_button)
		{
			show_button.addEventListener('click', (e) => this.showConsole(e));
		}

		let hide_button = document.querySelector('.TSv_ConsoleButton_hide');
		if(hide_button)
		{
			hide_button.addEventListener('click', (e) => this.hideConsole(e));
		}


		// Detect the keypress event
		document.addEventListener('keydown', (e) => this.detectShortcut(e));

		// Listen for scrolling
		this.scroller_container.addEventListener('scroll', (e) => {this.pageScrolled()});


		this.element.addEventListener('click', this.delegateClickHandler.bind(this));

	}

	/**
	 * Handles delegate clicks for various items, all related to interface items that are created after the script loads
	 * @param event
	 */
	delegateClickHandler(event) {

		let row = event.target.closest('.console_item_row');
		if(row)
		{
			let row_id = row.getAttribute('data-row-id');
			// Details button
			if(event.target.closest('.details_button') || event.target.closest('.expand_button'))
			{
				event.preventDefault();
				let target_row = this.element.querySelector('#console_desc_'+row_id);
				if(target_row)
				{
					target_row.toggle();
				}
			}
			else if(event.target.closest('.backtrace_button'))
			{
				event.preventDefault();
				let target_row = this.element.querySelector('#console_backtrace_'+row_id);
				if(target_row)
				{
					target_row.toggle();
				}
			}
		}
	}

	/**
	 * Shows the console, which starts from the top with the newest stuff.
	 * @param {Event} event
	 */
	showConsole(event)
	{
		event.preventDefault();

		this.element.classList.add('showing');
		this.loading_container.show('flex');
		document.body.style.overflow = 'hidden';

		// Empty the Console
		this.scroller_container.innerHTML = '<table><tbody></tbody></table>';
		this.last_parent_microtime = 0;
		this.last_index = 0;
		this.updateInfiniteScroller();

	}


	/**
	 * Hides the console
	 * @param {Event} event
	 */
	hideConsole(event)
	{
		event.preventDefault();

		this.is_done = 0;
		if (this.fetch_abort_controller !== null)
		{
			this.fetch_abort_controller.abort();
			this.fetch_abort_controller = null;
		}

		this.element.classList.remove('showing');
		document.body.style.overflow = 'auto';


	}

	/**
	 * Detects the shortcut keys which is Shift+Esc to show/hide the console
	 * @param {KeyboardEvent} event
	 */
	detectShortcut(event)
	{
		if (event.shiftKey && event.key === 'Escape')
		{
			if(this.element.classList.contains('showing'))
			{
				this.hideConsole(event);
			}
			else
			{
				this.showConsole(event);
			}
		}

	}

	/**
	 * Creates the server request to get the infinite scroller data for this console
	 */
	updateInfiniteScroller() {

		if(!this.is_done && this.fetch_abort_controller === null && this.last_index >= 0 )
		{

			this.loading_container.show('flex');

			let post_data = new FormData();
			post_data.append('last_parent_microtime', this.last_parent_microtime ) ;
			post_data.append( 'last_index', this.last_index) ;
			post_data.append( 'last_instance_id', this.last_instance_id) ;

			let url = '/admin/system/classes/views/TSv_Console/ajax_update_console_scroller.php';

			// set up the abort controller
			this.fetch_abort_controller = new AbortController();

			fetch(url, {
				method 	: 'POST',
				signal 	: this.fetch_abort_controller.signal, // pass the abort controller signal
				body	: post_data,
			})	.then(response => response.json())
				.then(response => {

					// Add each of the items to the list
					response.items.forEach(response_object => {
						this.scroller_container.querySelector('tbody').insertAdjacentHTML('beforeend', response_object.view_html);
					});
					this.last_index = response.last_number;

					// Track is_done and last parent microtime values
					this.is_done = response.is_done;
					this.last_parent_microtime = response.last_parent_microtime;
					this.last_instance_id = response.last_instance_id;

					// Nullify the abort controller
					this.fetch_abort_controller = null;

					if(!this.is_done)
					{
						this.pageScrolled();
					}

				})
				// Error handling
				.catch()
				// Re-enable the form items we disabled earlier once we're done
				.finally(() => {
					// Always hide loading
					this.loading_container.hide();

					// Always nullify the abort controller just in case something went wrong
					this.fetch_abort_controller = null;
				});


		}



	}

	/**
	 * Event handler for any page scrolls in the console
	 */
	pageScrolled()
	{
		let container_height = this.scroller_container.offsetHeight;
		let table_height = this.scroller_container.querySelector('table').offsetHeight;
		let scroll_top = this.scroller_container.scrollTop;
		let scroll_buffer = 30;

		let scroll_trigger = scroll_top + container_height + scroll_buffer > table_height;
		if(scroll_trigger)
		{
			this.updateInfiniteScroller();
		}
	}

}