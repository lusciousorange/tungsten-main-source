<?php
/**
 * Class TSv_Tungsten
 *
 * The main class for Tungsten in the system
 */
class TSv_Tungsten extends TCv_Website
{
	protected $modules = false; // The list of installed modules
	protected $logged_in = true; // Indicate if the system is currently logged in

	/** @var TSm_Module[] $installed_modules */
	protected $installed_modules = false; // The list of installed modules for the system
	/** @var TSm_Module[] $available_modules */
	protected $available_modules = false; // The list of available modules for the system
	protected $skip_authentication = false; // Indicates if authentication should be skipped

	/** @var bool|TSm_Module $current_module The current module being viewed */
	protected $current_module = false;
	
	// Appearance Settings
	protected bool $show_left_bar = true; // [bool] = Indicates if the left bar should be shown
	
	protected bool $is_admin_site = true; // set here to ensure it overrides on init of the main website class
	protected $main_content_view = false;
	protected $sub_navigation_view = false;
	protected $workflow_attached;
	
	protected bool $show_module_bar = true;
	
	protected ?string $version = null;
	/**
	 * TSv_Tungsten constructor.
	 *
	 * The main constructor for Tungsten
	 */
	public function __construct()
	{
		parent::__construct('tungsten');
		
		TC_setConfig('admin_mode', true); // sets a config to indicate that we are in an admin view
		
		$this->title = 'Tungsten';
		if(TC_getConfig('site_tungsten_title') !='')
		{
			$this->title = TC_getConfig('site_tungsten_title');
		}
		$this->description = 'The Tungsten Content Management System';
		
		// OVERRIDE PARENT VALUES
		// 0 is /admin/ so we need #1
		$this->section_name = $this->sections[1] ?? '';
		

		// LOAD CSS and JS
		$this->loadTungstenCSSandJS();
		
		
		$this->addMetaTag('cache-control','<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">');
		
		// SKINNING
		$skin = TC_getConfig('skin');
		if(isset($skin))
		{
			$this->addCSSFile('skin', '/admin/skins/'.TC_getConfig('skin').'/skin.css');
		
		}
		
		$this->body_tag->addClass('tungsten_module_'.htmlspecialchars($this->section_name));
		
		$this->main_content_view = new TCv_View('tungsten_main_content');
		$this->main_content_view->setTag('main');
		$this->main_content_view->addClass('tungsten_main_content');
		$this->sub_navigation_view = new TCv_View('sub_navigation');
		
		// Handle Favicon
		$this->favicon = '/admin/images/favicon_v8.png';
		if(TC_getConfig('is_localhost'))
		{
			$this->favicon = '/admin/images/favicon_v8_grey.png';
		}
		elseif(TC_getConfig('is_staging_domain'))
		{
			$this->favicon = '/admin/images/favicon_v8_orange.png';
		}
		
		//$this->enablePerformanceTracking();
		
		// Add the workflow JS to the page, so it loads even with async
		if(TC_getConfig('use_workflow'))
		{
			$this->addJSFile('workflow_next_prev_handler',$this->classFolderFromRoot('TMv_WorkflowNextPrevious', true).'/workflow_next_prev_handler.js?v'.TCv_Website::gitVersion());
		}
		
		// This add the class tag for version
		
		$this->addClass('tungsten_version_'.$this->mainVersionNumber());
	}
	
	/**
	 * Function to load CSS and JS files for Tungsten, which in some cases might need to be replaced, rather than
	 * extended
	 * @return void
	 */
	public function loadTungstenCSSandJS() : void
	{
		$this->addClassCSSFile('TSv_Tungsten');
		$this->addCSSFile('tungsten_print', '/admin/system/classes/views/TSv_Tungsten/TSv_TungstenPrint.css', 'print ');
		
		$this->addClassJSFile('TSv_Tungsten');
		$this->addClassJSInit('TSv_Tungsten');
	}
	
	/**
	 * Break the singleton so that it uses its own ID of `tungsten` which is set whenever it's initialized.
	 * @return bool
	 */
	public static function isSingleton(): bool
	{
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOGGED IN USER
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the user ID in the system
	 * @param ?int $id
	 */
	public function setUserID(?int $id) : void
	{
		parent::setUserID($id);
		$this->installedModules();
	}

	//////////////////////////////////////////////////////
	//
	// SYSTEM FUNCTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the version of the system
	 * @return string
	 */
	public function version() : string
	{
		if(is_null($this->version))
		{
			include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/config/system_version.php');
			$this->version = $admin_version;
		}
		return $this->version;
	}
	
	/**
	 * Grab the first part of the version number
	 * @return int
	 */
	public function mainVersionNumber() : int
	{
		return explode('.', $this->version())[0];
	}
	
	/**
	 * Returns the class name to load when loading an instance of Tungsten. The default class for tungsten can be
	 * overridden by settings the TC_config value for 'Tungsten_class_name'. This is an advanced feature used to skin or
	 * override operational features of Tungsten.
	 * @return class-string<TSv_Tungsten>
	 */
	public static function tungstenClassName() : string
	{
		// Grab it from the override class name
		$override_class = TC_getConfig('class_overrides','TSv_Tungsten');
		if($override_class != '' && $override_class != NULL)
		{
			return $override_class;
		}
		
		$class_name = TC_getConfig('Tungsten_class_name');
		if($class_name != '' && $class_name != NULL)
		{
			return $class_name;
		}
		
		return "TSv_Tungsten";
	}


	//////////////////////////////////////////////////////
	//
	// MODULE FUNCTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * The list of installed modules
	 * @return TSm_Module[]
	 */
	public function installedModules()
	{
		if($this->installed_modules)
		{
			return $this->installed_modules;
		}
		
		
		if($this->user())
		{
			/** @var TSm_ModuleList $module_list */
			$module_list = TSm_ModuleList::init();
			
			$this->installed_modules = $module_list->installedModulesForUser($this->user());
		}

		else
		{
			$this->installed_modules = array();
			
		}
		return $this->installed_modules;
		
	}
	
	/**
	 * Acquires the list of available modules in the system. This includes those that are installed as well as those
	 * that could be installed but currently aren't.
	 *
	 * @return TSm_Module[]
	 */
	public function availableModules()
	{
		if($this->available_modules)
		{
			return $this->available_modules;
		}
		$this->available_modules = array();
		
		$this->installedModules(); // go through that effort to find the list of installed ones
		
		$d = dir($_SERVER['DOCUMENT_ROOT'].'/admin/'); // get the directory
		
		// loop through each item in the directory
		while($module = $d->read()) 
		{
			$module_path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$module;
		
			// Check for version.php file.
			if(file_exists($module_path.'/config/settings.php'))
			{
				if(@$this->installed_modules[$module])
				{
					$this->available_modules[$module] = $this->installed_modules[$module]; // TSm_Module instance
				}
				else
				{
					$this->available_modules[$module] = new TSm_Module($module); // not installed
				}
			}
		}
		
		// sort by keys ie: module names
		ksort($this->available_modules);
		
		// return list
		return $this->available_modules;
	}
	
	/**
	 * Checks if a given module is installed based on the folder name for it.
	 * @param string $module_folder
	 * @return bool
	 */
	public function moduleInstalled($module_folder)
	{
		foreach($this->installedModules() as $module)
		{
			if($module->folder() == $module_folder)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Checks if the user that is logged in has access to view the module. This is done to ensure that someone does not
	 * type in a URL to a page for which they do not have access to. If it is found to be true, it will print out a
	 * warning message and stop any further actions from occurring.
	 * @return bool
	 */
	public function checkModuleAccess()
	{
		if($this->section_name == 'login')
		{
			return true;
		}
		if($this->section_name == 'install')
		{
			$this->title .= ' : Install';
			
			// if we're trying to load something form the install module
			// AND the system module is installed but the install module is not
			// that indidates a completed installation
			if($this->installComplete())
			{
				// Create an access denied view item
				/** @var TSv_AccessDenied $access_denied */
				$access_denied = TSv_AccessDenied::init();
				$this->attachToMainContentView($access_denied);
			
				// print the site and stop. 
				echo $this->html();
				exit();
			}
			else // no problems being in the install module
			{
				return false;
				
			}
			
		}
		elseif($this->skip_authentication && $this->section_name != 'login')
		{
			return false;
		}
		elseif($this->section_name == 'TCore')
		{
			$this->title .= '';
			return false;
		
		}
		
		// IF HERE, THEN LOGGED IN
		$installed_modules = $this->installedModules();
		
		if(!isset($installed_modules[$this->section_name]))
		{
			$this->show_left_bar = false; // don't show the left menu bar
			// Create an access denied view item
			/** @var TSv_AccessDenied $access_denied */
			$access_denied = TSv_AccessDenied::init();
			$this->attachToMainContentView($access_denied);
			$this->addConsoleWarning('Access Denied: Module Not Installed');
		
			// print the site and stop. 
			echo $this->html();
			exit();
		}
		
			
		$this->title .= ' : '.$this->currentModule()->title();
		
	}
	
	/**
	 * Autoloads the CSS file for a module if it exists. The file needs to be in a folder called "styles" in the module
	 * folder. The file name needs to match the name of the folder. ie: if you want a CSS file loaded any time you are
	 * in the Users module, it needs to be in /admin/users/styles/users.css
	 */
	public function addModuleCSS()
	{
		
		if($this->current_module)
		{
			$file_root_path = '/admin/'.$this->current_module->folder().'/styles/'.$this->current_module->folder().'.css';
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$file_root_path))
			{
				$this->addCSSFile('module_'.$this->current_module->folder(), $file_root_path);
			}
			
		}
		
	}
	
	/**
	 * Returns the current module being loaded
	 * @return bool|TSm_Module
	 */
	public function currentModule()
	{
		// Sections to ignore completely
		if(in_array($this->section_name, ['','TCore']))
		{
			return false;
		}
		if($this->current_module === false)
		{
			$this->current_module = TSm_Module::init($this->section_name);
		}
		return $this->current_module;
	
	}
	
	/**
	 * Returns if Tungsten is mid-install
	 * @return bool
	 */
	public function isInstalling()
	{
		return $this->currentModule()->folder() == 'install';
	
	}
	
	/**
	 * Returns the number of modules that require updates.
	 * @return int
	 */
	public function numModulesWithUpdates()
	{
		$num_upgrades = 0;
		foreach($this->installedModules() as $module)
		{
			if($module->requiresUpgrade())
			{
				$num_upgrades++;
			}	
		}
		return $num_upgrades;
	}
	
	/**
	 * Returns the current URL target. If at any point, it can't find a valid URL target, it will return false.
	 * @return ?TSm_ModuleURLTarget
	 */
	public function currentURLTarget() : ?TSm_ModuleURLTarget
	{
		return TC_currentURLTarget();
	}

	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Indicates if Tungsten should skip authentication. This must be done direclty after initialization before any authentication calls are made.
	 * @param bool $skip
	 */
	public function setSkipAuthentication($skip = false)
	{
		$this->skip_authentication = $skip;
	}

	/**
	 * Checks if the current URL target being loaded is set to skip authentication
	 * @return bool
	 */
	public function processURLTargetAuthenticationSkipping()
	{
		// NOte that this method can't instantiate the proper controller. That will trigger validations which can't happen yet. This must be a one-off that happens before any validations. They will inherently require the current user which by-defintion, aren't set yet.
		if($module = $this->currentModule())
		{
			$controller_class = TC_actualClassNameUsed($module->controllerClass());

			/** @var TSc_ModuleController $controller */
			$controller = new $controller_class($module);
			
			if($current_url_target = $controller->currentURLTarget())
			{
				
				if($current_url_target->skipsAuthentication())
				{
					$this->skip_authentication = true;
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Handles the login attempt from the user. If successful, all the necessary values will be saved and the session variables will be appropriately set.
	 * @param string $email
	 * @param string $password
	 * @return bool
	 */
	public function processLogin($email, $password)
	{
		// delete old setting first
		unset($_SESSION['tungsten_user_id']);
		if($user = $this->authenticateEmailPassword($email, $password))
		{
			$this->user = $user;
			$this->user->setAsLoggedIn();
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
	/**
	 * Check that the email and password provided equate to a real person in the Users system. It will check that the
	 * credentials provided match a user in the system.  It will also check that the user is a member of one of the
	 * groups that are permitted to vie at least one module in Tungsten.
	 * @param string $email
	 * @param string $password
	 * @return bool|TMm_User
	 */
	public function authenticateEmailPassword($email, $password)
	{
		// username and password work
		$user = parent::authenticateEmailPassword($email, $password);
		if(!$user)
		{
			TC_message('The email and password combination you provided were not found in our system. Please try again or contact your administrator.',false);
			return false;
		}

		// Disabled Accounts can't login
		if(!$user->isActive())
		{
			return false;
		}


		// Verify Check for Tungsten Credentials
		if($user->isAdmin())
	  	{
	  		return $user;
	  	}


		
	  	// Not admin, now we check the complex access group permissions
	  	if($user->hasTungstenAccess())
	  	{
		 	return $user;
	  	}
	  	else
	  	{
			TC_message('Your account does not have access to the Tungsten Administrative System.',false);
			return false;
		}
	}

	/**
	 * Check that the current session is valid. It also will check that the user has permission for a given group.
	 */
	public function authenticateSession()
	{
		// Set the user ID first, needed for other processing
		if($this->sessionIsValid())
		{
			$this->setUserID($_SESSION["tungsten_user_id"]);
		}

		if($this->skip_authentication || $this->processURLTargetAuthenticationSkipping())
		{
			return;
		}
		
		// Invalid Session
		if(!$this->sessionIsValid())
		{
			$_SESSION['tungsten_login_original_page'] = $_SERVER["REQUEST_URI"];
			header("Location: /admin/login/process_invalid_session.php");
			exit;
		}

		// someone logged in, but trying to view tungsten page or run a script as "Tungsten" without access
		if($this->user())
		{
			if(!$this->user()->hasTungstenAccess())
		  	{
		  		// Deal with the case that we're impersonating someone who doesn't have tungsten access
		  		if(TC_getConfig('use_impersonate_user') && isset($_SESSION['impersonation_id']))
				{
					$this->user = null;
					$access_denied = new TSv_AccessDenied('impersonation_warning');
					$access_denied->setAsBlue();
					$access_denied->setMainTitle('Impersonating Non-Tungsten User');
					$access_denied->setExplanation(
						'<p>You are currently impersonating a user who does not have Tungsten access. You will not be able to use any functionality in Tungsten until you disable impersonation.</p>
						<p>Consider opening a separate browser (or incognito mode) to create a new session if you still need Tungsten access.</p>
						<p><a class="big_button" href="/admin/system/do/cancel-impersonation/">Cancel Impersonation</a></p>');
					$this->attachToMainContentView($access_denied);
				}
				else
				{
					$this->logout('Your account does not have access to Tungsten');
				}

			}
		}
		else
		{
			$this->logout('Your account was not found');
		}  	
		
	}
	
	/**
	 * Check that the current session is valid. It also will check that the user has permission for a given group.
	 * @return bool
	 */
	public function sessionIsValid()
	{
		if (!isset($_SESSION["tungsten_user_id"]))
		{
			return false;
		}
		
		
		
		
		return true;
	}
	
	/**
	 * Indicates if someone is logged in
	 * @return bool
	 */
	public function loggedIn()
	{
		return $this->sessionIsValid();
	}
	
	/**
	 * Logs someone out of the website
	 * @param bool|string $message
	 * @param bool $successful
	 */
	public function logout($message = false, $successful = false)
	{
		parent::logout($message, $successful);

		// Detect if we are loading from somewhere in the admin
		if(substr($_SERVER['REQUEST_URI'],0,7) == '/admin/')
		{
			header("Location: /admin/");
			exit();
		}
		
		header("Location: /");
		exit();
		
	}
	
	/**
	 * A singleton method that always returns the current instance of the website.
	 * @return TCv_Website
	 */
	public static function website() : TCv_Website
	{
		// If not set, or if we have a regular website, we need a TSv_Tungsten website
		if(is_null(self::$website) || get_class(self::$website) == 'TCv_Website')
		{
			$tungsten_class_name = TSv_Tungsten::tungstenClassName();
			self::$website = new $tungsten_class_name('tungsten');
			TC_saveActiveModel(self::$website);
		}
		
		return self::$website;
	}

	//////////////////////////////////////////////////////
	//
	// Workflow
	//
	// Functions related to the worfklow and the setup to manage that.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns if workflow should be shown
	 */
	protected function showWorkflow()
	{
		return TC_moduleWithNameInstalled('workflow');
	}

	/**
	 * Returns the workflow bar if it's meant to exist
	 */
	protected function workflowBar()
	{
		if($this->showWorkflow() && class_exists('TMv_WorkflowControlBar'))
		{
			$workflow_module = TMm_WorkflowModule::init();
			if(TC_getConfig('use_workflow') && $workflow_module->currentModel())
			{
				$workflow_bar = TMv_WorkflowControlBar::init();
				return $workflow_bar;
			}
			
		}

		return false;
	}

	
	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////


	/**
	 * The header view of Tungsten
	 * @return TCv_View
	 */
	public function headerView()
	{
		$header = new TCv_View("tungsten_header");
		$header->setTag('header');
		
			$header->attachView(TSv_StagingWarningBar::init());
			
			$header_top = new TCv_View('tungsten_header_top');
		
		
			// LOGO
			$logo = new TCv_View('header_logo');
			$logo->setTag('img');
			$logo->setAttribute('src','/admin/images/tungsten_logo_v8.png');
			$header_top->attachView($logo);
			
			// SEARCH
			if($this->isLoggedIn())
			{
				$search_form = TMv_TungstenSearchForm::init();
				$header_top->attachView($search_form);
			}
			
			// CONTROL BUTTONS
			$header_top->attachView($this->controlButtons());
			$header->attachView($header_top);
			
			
			
			//if($this->isLoggedIn())
			//{
				$toggle_link = new TCv_Link('tungsten_mobile_toggle');
				$toggle_link->addText('Main Menus');
				$toggle_link->setURL('#');
				$header->attachView($toggle_link);
			
			// Only show module bar when logged in and not installing
			if($this->show_module_bar && !$this->isInstalling() && $this->isLoggedIn())
			{
				$header->attachView($this->moduleBar()); 
			}
			
			
		return $header;
	}

	
	/**
	 * Returns the view for the control buttons
	 * @return TCv_View
	 */
	public function controlButtons()
	{
		$buttons = new TCv_View('control_buttons');
	
		if($this->isLoggedIn())
		{
			if($this->isInstalling())
			{
				$profile = new TCv_View('control_user_name');
				$profile->addClass('control_button_item');
				$profile->addText($this->user()->fullName());
				$buttons->attachView($profile);
				
			}
			else
			{
				$profile = new TCv_Link('control_user_name');
				$profile->addClass('control_button_item');
				$profile->setTitle('Edit your profile');
				$profile->setURL('/admin/profile/');
				$profile->addText($this->user()->fullName());
				$buttons->attachView($profile);
				
				
				// Potential Admin Cart Button
				if(class_exists('TMv_AdminCartButton')
					&& TC_getModuleConfig('store','show_admin_cart')
					&& TC_currentUser() && TC_currentUser()->isAdmin())
				{
					$buttons->attachView(TMv_AdminCartButton::init());
				}
				
				$buttons->attachView($this->showConsoleLink());
		
		
				if($this->user()->isAdmin())
				{
					$num_upgrades = $this->numModulesWithUpdates();
					
					$system = new TCv_Link('control_system_button');
					$system->addClass('control_button_item');
					$system->setTitle('System settings');
					$system->setURL('/admin/system/');
					$system->addText('System');
					if($num_upgrades > 0)
					{
						$system->addText('<span class="system_update_icon">'.$num_upgrades.'</span>');
						$system->addClass('has_notification');
					}
					
					$buttons->attachView($system);
				}
			
				
				
				
				$logout = new TCv_Link();
				$logout->addClass('control_button_item');
				$logout->setTitle('Logout');
				$logout->setURL('/admin/login/do/logout/');
				$logout->addText('Logout');
				$buttons->attachView($logout);
			}
			
			
			
		}
		
		
		return $buttons;
		
	}
	
	/**
	 * Returns module bar for Tungsten
	 * @return TCv_View
	 */
	public function moduleBar()
	{
		
		
		$module_bar = new TCv_View('tungsten_module_bar');
		$module_bar->setTag('ul');
		
		$num_modules = 0;
		// Dashboard item
		foreach($this->installedModules() as $folder => $module)
		{
			if($module->showInMenu() && $module->folderExists())
			{		
				$num_modules++;		
				$li = new TCv_View();
				$li->setTag('li');
				
				$link = new TCv_Link('tungsten_menu_'.$module->folder());
				$link->setURL('/admin/'.$module->folder().'/');
					$icon_view = new TCv_View();
					$icon_view->setTag('i');
					$icon_view->addClass($module->iconCode());
					$link->attachView($icon_view);
				$link->addText($module->title());
				$link->setTitle('View '.$module->title().' Module');
				if($module->folder() == $this->sectionName())
				{
					$link->addClass("current");
				}
				
				$li->attachView($link);
				$module_bar->attachView($li);

				if($module->buttonColor())
				{
					$css = 'a#tungsten_menu_'.$module->folder().' { color:'.$module->buttonColor().' !important; } ';
					$this->addCSSLine('button_tungsten_menu_'.$module->folder(),$css);

				}
			}	
		
		}
		$module_bar->addClass('num_modules_'.$num_modules);
		if($num_modules > 4)
		{
			$module_bar->addClass('num_modules_greater_4');
		}
		if($num_modules > 6)
		{
			$module_bar->addClass('num_modules_greater_6');
		}
		if($num_modules > 8)
		{
			$module_bar->addClass('num_modules_greater_8');
		}
		
		return $module_bar;

	}

	/**
	 * Turns on print friendly mode to appear in the browser, rather than just the print screen
	 */
	public function displayPrintFriendlyView()
	{
		$this->addCSSFile('tungsten_print', '/admin/system/classes/views/TSv_Tungsten/TSv_TungstenPrint.css', 'all ');
	}
	
	/**
	 * A specialized attachView() function which ensures the view is added to the "main content" for Tungsten. This
	 * ensures a level of customization when extending the Tungsten class
	 * @param TCv_View $view
	 */
	public function attachToMainContentView($view)
	{
		$this->main_content_view->attachView($view);
	}
	
	/**
	 * A specialized attachView() function which ensures the view is added to the "sub navigation" for Tungsten.
	 * This ensures a level of customization when extending the Tungsten class
	 * @param TCv_View $view
	 */
	public function attachToSubNavigationView($view)
	{
		if($view instanceof TSv_ModuleNavigation_SubmenuBar)
		{
			$workflow_bar = $this->workflowBar();
			if($workflow_bar && $this->workflow_attached == null)
			{
				$view->attachView($workflow_bar);
				$view->addClass('showing_workflow');
				$this->workflow_attached = true;
			}
		}
		$this->sub_navigation_view->attachView($view);
	}
		

	/**
	 * Adds any necessary header and other views that need to be added before the main content.
	 */
	protected function configureForPrint()
	{
		$this->attachView($this->headerView());
		
		
	}
	
	/**
	 * Override of the main website function to wrap the body in a site_container
	 */
	public function completeBodyTag()
	{
		parent::completeBodyTag();
	
		$site_container = new TCv_View('site_container');
		$this->body_tag->wrapContentInTag($site_container);
	}
	
	/**
	 * Function called to handle front-end routing, which if enabled, replaces the rendered view with one of the
	 * theme. This function must be called in the render() process since it overrides the output
	 * @return ?TCv_Website Returns the complete view, if that's what we should show. Otherwise null.
	 */
	public function handleFrontEndRouting() : ?TCv_Website
	{
		// Only bother if we have a URL target to reference
		if($current_target = TC_currentURLTarget())
		{
			//$this->addConsoleDebug('*** handleFrontEndRouting');
			
			// Only bother if we're actually loading a URL that is routing
			// Permission to load the view is handled in the access denied functionality
			// So we can just stream any question to the front-end
			// Knowing that bad requests get caught
			if(substr($_SERVER['REQUEST_URI'],0,3) == '/w/')
			{
				// At this point, we have to load the public theme, but with all the views from Tungsten
				// We need to generate a theme view based on the public side, then
				// Add our Tungsten views to that page we're loading
				$theme_name = $current_target->frontEndRoutingThemeName();
				$theme_view = ($theme_name)::init();
				
				
				// Create a dummy page with values that set it up to successfully render, but without assuming
				// it belongs anywhere in the navigation
				$frame_page = ($theme_name)::frontEndRoutingMenuItem();
				
				
				$theme_view->setViewedMenu($frame_page);
				
				// Attach the content to page, wrapping it in a section
				$renderer = new TCv_View();
				$renderer->addClass('TMv_PageContentRendering');
				
				$section = new TMv_ContentLayout_Section();
				$section->addClass('basic_style section_1');
				
				$row = new TMv_ContentLayout_1Column();
				$row->attachViewToColumnNum($this->main_content_view, 1);
				
				$section->attachView($row);
				$renderer->attachView($section);
				
				
				$theme_view->attachViewToPagesContent($renderer);
				
				
				return $theme_view;
			}
		}
		
		// Nothing to do, keep going
		return null;
	}
	
	public function html()
	{
		// Handle the front-end rendering, and if it happened, there's nothing more to do.
		if($theme_view = $this->handleFrontEndRouting() )
		{
			echo $theme_view->html();
			TMm_BenchmarkEntry::instance()?->trackPrintTimes();
			exit();
		}
	
		return parent::html();
	}
	
	/**
	 * Return the html for the site
	 */
	public function render()
	{
		$this->configureForPrint();
		
		// Sanity check to avoid exposing navigation if we happen to load a view that skips authentication
		if($this->loggedIn() || $this->sectionName() == 'login')
		{
			$this->attachView($this->sub_navigation_view);
		}
		
		$dialog = new TCv_Dialog();
		$this->attachToMainContentView($dialog);

		$this->attachView($this->main_content_view);

		//$this->addConsoleDebug($_SESSION);
		//$this->addConsoleDebug('Memory: '.TC_currentMemoryUsage());
		
//		$this->addConsoleDebug('Clearing memory');
//		$GLOBALS['TC_classes']['classes']['TCm_FormTracker'] = null;
//		$_SESSION['TCm_FormTracker'] = null;
//
//
//		$this->addConsoleDebug('Memory: '.TC_currentMemoryUsage());
		
		
		
	
	}
	
	
	/**
	 * Extend to cover Tungsten 9 loading
	 * @param mixed $args,... The arguments provided for the init
	 * @return string
	 * @see get_called_class()
	 * @see TCu_Item::overrideClassName()
	 */
	public static function classNameForInit(...$args)
	{
		// If we override, always use that one
		if(TC_configIsSet('Tungsten_class_name'))
		{
			return TC_getConfig('Tungsten_class_name');
		}
		
		if(TC_getConfig('use_tungsten_9'))
		{
			return 'TSv_Tungsten9';
		}

		return 'TSv_Tungsten';
		
	}

}

define('TUNGSTEN_LOGGED_IN', true);
define('TUNGSTEN_LOGGED_OUT', false);

?>