class TSv_Tungsten {
	element = null;
	help_timeout = null;

	constructor(element, params) {
		this.element = element;

		let help_button = this.element.querySelector('#help_toggle_button');
		if(help_button)
		{
			help_button.addEventListener('click', (e) => this.showHelpView(e));
			this.element.querySelector('#help_close_button').addEventListener('click', (e) => this.hideHelpView(e));

			this.element.querySelector('.help_view_contents').addEventListener('click', (e) => this.helpButtonHighlight(e));
		}

		// Listen for form submissions
		this.element.addEventListener('submit', (e) => {
			e.target.classList.add('tungsten_loading');
		});

		// Action that removes the tungsten_loading from any class that was given it
		// Safety measure to allow for self-correction for correcting on pages that need access again
		// Requires capturing instead of bubbling, so we can't use event listeners
		document.body.addEventListener('click', (e) => {
			// find tungsten_loading element
			let el = e.target.closest('.tungsten_loading');
			if(el)
			{
				el.classList.remove('tungsten_loading');
			}
		},	true); // use capture



	}

	/**
	 * Shows the help view
	 * @param event
	 */
	showHelpView(event) {
		this.element.querySelector('#help_view_container').slideDown();
	}

	/**
	 * Hides the help view
	 * @param event
	 */
	hideHelpView(event) {
		this.element.querySelector('#help_view_container').slideUp();
	}

	/**
	 * Highlights a particular item when a link is clicked in a help view
	 * @param event
	 */
	helpButtonHighlight(event)
	{
		if(event.target.tagName === 'A')
		{
			// Only do all teh cool stuff if we aren't looking at a real link
			let href = event.target.getAttribute('href');

			if(href === '#' || href === '')
			{
				event.preventDefault();

				let target_identifier = event.target.getAttribute('data-highlight');

				// clear any highlighting
				this.clearHelpHighlights();

				// clear existing timeout
				if(this.help_timeout != null)
				{
					clearTimeout(this.help_timeout);
				}

				// highlight the new one
				let target_element = document.querySelector(target_identifier);
				if(target_element)
				{
					target_element.classList.add('help_highlight');
				}

				// remove the class after 4 seconds, animation runs for 3 seconds
				this.help_timeout = setTimeout(this.clearHelpHighlights, 4000);
			}
		}



	}

	/**
	 * Clear any highlights on a help view
	 */
	clearHelpHighlights()
	{
		let highlighted = this.element.querySelector('.help_highlight');
		if(highlighted)
		{
			highlighted.classList.remove('help_highlight');
		}
	}


}