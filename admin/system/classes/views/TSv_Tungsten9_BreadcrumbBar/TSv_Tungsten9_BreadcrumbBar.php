<?php
/**
 * Class TSv_Tungsten9_BreadcrumbBar
 *
 * The breadcrumbs in Tungsten
 */
class TSv_Tungsten9_BreadcrumbBar extends TCv_View
{
	protected ?TSm_ModuleURLTarget $current_url_target = null;
	protected array $right_menus = [];
	protected array $breadcrumbs = [];
	
	/**
	 * TSv_ModuleNavigation_BreadcrumbBar constructor.
	 * @param bool $id (Optional) Default false.
	 */
	public function __construct($id = false)
	{
		parent::__construct('breadcrumb_menus');
		
		$this->addClassCSSFile('TSv_Tungsten9_BreadcrumbBar');
		$this->addFontAwesomeClassAsSymbol('fa-caret-right', 'fa-caret-right');
		
		$this->current_url_target = TC_currentURLTarget();
		
		
	}
	
	/**
	 * Extends the attachView method to add the view to the breadcrumbs
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 */
	public function attachView($view, $prepend = false)
	{
		$this->attachBreadcrumbView($view);
	}
	
	/**
	 * @param TCv_View $view
	 */
	public function attachBreadcrumbView($view)
	{
		$this->breadcrumbs[] = $view;
	
	}
	
	/**
	 * @param string $title
	 * @param bool|string $link
	 */
	public function addBreadcrumb($title, $link = false)
	{
		if($link)
		{
			$view = new TCv_Link();
			$view->setTitle($title);
			$view->setURL($link);
		}
		else
		{
			$view = new TCv_View();
			$view->setTag('span');
		}
		
		$view->addText($title);
		$this->attachBreadcrumbView($view);
		
	}
	
	/**
	 * Adds a menu item, link or view to the right side
	 * @param TCv_View $view
	 */
	public function addRightView($view)
	{
		if($view instanceof TCv_Link)
		{
			if($view->contentIsOnlyIcon())
			{
				$view->addClass('icon_only');
			}
		}
		
		$this->right_menus[] = $view;
	}
	
	/**
	 * Removes the last breadcrumb from the list
	 */
	public function removeLastBreadcrumb()
	{
		array_pop($this->breadcrumbs);
	}
	
	/**
	 * Returns the list of breadcrumbs
	 * @return TCv_View[]
	 */
	public function breadcrumbViews()
	{
		return $this->breadcrumbs;
	}

	/**
	 * Returns the right views
	 * @return TCv_View[]
	 */
	public function rightView()
	{
		return $this->right_menus;
	}
	
	//////////////////////////////////////////////////////
	//
	// CONFIGURATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Configures the breadcrumb bar based on the current URL target
	 */
	public function configureForCurrentURLTarget() : void
	{
		if(!$this->current_url_target)
		{
			return;
		}
		
		$this->processBreadCrumbURLTargets();
		
		$this->processRightButtons();
	
		
	}
	
	protected function processBreadCrumbURLTargets() : void
	{
		$current_module = $this->current_url_target->module();
		
		// Add The module name to the breadcrumb bar
		$this->addBreadcrumb($current_module->title(),
		                     '/admin/'.$current_module->folder().'/');
		
		
		$url_target_menu_buttons = array();
		$breadcrumb_url_target = $this->current_url_target;
		while($breadcrumb_url_target instanceof TSm_ModuleURLTarget)
		{
			if($breadcrumb_url_target->title() != '' && $breadcrumb_url_target->userHasAccess())
			{
				$url_target_menu_buttons[] = TSv_ModuleURLTargetLink::init($breadcrumb_url_target);;
			}
			$breadcrumb_url_target = $breadcrumb_url_target->parentURLTarget();
		}
		
		
		krsort($url_target_menu_buttons);
		foreach($url_target_menu_buttons as $button)
		{
			// Add the button
			$this->attachView($button);
		}
		
	}
	
	protected function processRightButtons() : void
	{
		// Detect if we're looking at a model
		if($model = TC_primaryModel() )
		{
			$url = '';
			$show_open_button = false;
			if($model instanceof TMm_PagesMenuItem)
			{
				// Pages that are not template pages can all be opened
				if(!$model->isModelTemplatePage())
				{
					$show_open_button = true;
					$url = $model->pageViewURLPath();
				}
				else // Deal with showing the template
				{
					
					$template = new TCv_View();
					$template->addClass('template_indicator');
					$template->addText("Template page : ");
					$model_class = $model->modelClassName();
					$template->addText(' <strong>'.$model_class::modelTitlePlural().'</strong>');
					$this->addRightView($template);
				}
			}
			
			// case two, page model viewable
			elseif(TC_classUsesTrait($model,'TMt_PageModelViewable'))
			{
				$url = $model->pageViewURLPath();
				if($url != '')
				{
					$show_open_button = true;
					
				 }
			}
			
			// Show the button
			if($show_open_button)
			{
				$right_button = TCv_Link::init();
				$right_button->openInNewWindow();
				$right_button->setURL($url);
				$icon_view = new TCv_View();
				$icon_view->setTag('i');
				$icon_view->addClass('fa-external-link');
				$right_button->attachView($icon_view, true); // prepend icons
				$right_button->addClass('right-open-model');
				$right_button->addText('Open');
				$this->addRightView($right_button);
			}
		}
		
		// Loop through the URL targets and add inay that are right buttons
		$current_module = $this->current_url_target->module();
		foreach($current_module->urlTargetsByParent() as $url_target)
		{
			if($url_target->isRightButton() && $url_target->name() != 'settings')
			{
				//if($this->current_url_target->isParentURLTarget($url_target))
				if($url_target->isParentURLTarget($this->current_url_target) || $url_target->parentURLTarget() == false)
				{
					$show_button = true;
					
					// requires validation before showing.
					if($validation_method = $url_target->rightButtonValidationMethod())
					{
						// get the model instance
						$model = TC_activeModelWithClassName($url_target->modelName());
						
						// call the validation method and if it retursn false, don't show the button
						$is_visible = $model->$validation_method();
						if(!$is_visible)
						{
							$show_button = false;
						}
					}
					// No validation but a model name still exists
					elseif($url_target->modelInstanceRequired())
					{
						$model = TC_activeModelWithClassName($url_target->modelName());
						if(!$model)
						{
							$show_button = false;
						}
						
					}
					
					
					// ensure there's access before showing
					if(!$url_target->userHasAccess())
					{
						$show_button = false;
					}
					
					if($show_button)
					{
						/** @var TSv_ModuleURLTargetLink $right_button */
						$right_button = TSv_ModuleURLTargetLink::init($url_target);;
						if($url_target->iconCode() != '')
						{
							$icon_view = new TCv_View();
							$icon_view->setTag('i');
							$icon_view->addClass($url_target->iconCode());
							$right_button->attachView($icon_view, true); // prepend icons
							$right_button->addClass('right-'.$url_target->iconCode());
						}
						$this->addRightView($right_button);
					}
					
					
				}
			}
		}
	}
	
	
	/**
	 * Generates the right menus for the breadcrumb bar
	 * @return TCv_Menu
	 */
	protected function rightMenuView()
	{
		$menus = new TCv_Menu('right_menu_container');
		
		foreach($this->right_menus as $right_menu_view)
		{
			// Check validity for Module URL links
			$is_valid = true;
			if( $right_menu_view instanceof TSv_ModuleURLTargetLink && !$right_menu_view->isValid())
			{
				$is_valid = false;
			}
			// Only bother if it is a valid view
			if($is_valid)
			{
				$right_menu_view->addClass('right_menu');
				$menus->attachView($right_menu_view);
			}
			
			
		}
		
		
		
		return $menus;
	}
	
	protected function breadcrumbMenuView()
	{
		$menus = new TCv_Menu('nav_breadcrumbs');
		
		$count = 1;
		$page_title_parts = array();
		$last_page_title_part = 'no title';
		foreach($this->breadcrumbs as $breadcrumb_view)
		{
			$title = false;
			if(method_exists($breadcrumb_view, 'title'))
			{
				$title = $breadcrumb_view->title();
			}
			
			$li = new TCv_View();
			$li->setTag('li');
			$li->addClass('breadcrumb');
			if($count == 1)
			{
				$li->addClass('first');
			}
			$li->attachView($breadcrumb_view);
			$count++;
			
			// Avoid double titles, which happen and looks awkward
			if($title != $last_page_title_part || $title === false)
			{
				$page_title_parts[] = $title;
				$menus->attachView($li);
			}
			$last_page_title_part = $title;
		}
		
		// Set the title of the page then lock it.
		// lock avoids content items setting the page title in the pages module
		$this->setBrowserPageTitle(implode(' – ', $page_title_parts), true);
		
		return $menus;
	}
	
	/**
	 */
	public function render()
	{
		$this->configureForCurrentURLTarget();
		
		
		$menus = $this->breadcrumbMenuView();
		if($menus->numItems() > 0)
		{
			parent::attachView($menus);
		}
		
		
		$menus = $this->rightMenuView();
		if($menus->numItems() > 0)
		{
			parent::attachView($menus);
		}
		
		
		
	}
	
}	
?>