<?php
class TSv_TikTokPixel extends TSv_TrackingPixel
{
	protected static $pixel_name = 'TikTok';
	protected static $pixel_icon = 'fab fa-tiktok';
	protected static $track_function_name = 'ttq';
	
	/**
	 * TSv_TikTokPixel constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		
		// Correct if it's set to not wait for rendering, needs it in case a view or theme sets a gtag value
		if($plugin_model && !$plugin_model->waitsForRendering())
		{
			$plugin_model->updateWithValues(['wait_to_load_on_rendering' => 1]);
		}
	}
	
	/**
	 *
	 */
	public function html()
	{
		// STEP 1 : Load the fb tag function
		$this->addJSLine('tt_tag',
		                 "\n// TIKTOK PIXEL \n".
		                 '!function (w, d, t) {
	w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];
	ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],
	ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};
	for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

	ttq.load("'.$this->pluginVariable(1).'");
	ttq.page();
}(window, document, "ttq");', false);
		
		// STEP 2 : User identity
		if($user = TC_currentUser())
		{
			$values = [
				'sha256_email' => hash('sha256', strtolower($user->email())),
			];
			// @see https://ads.tiktok.com/gateway/docs/index?identify_key=2b9b4278e47b275f36e7c39a4af4ba067d088e031d5f5fe45d381559ac89ba48&language=ENGLISH&doc_id=1701890972946433#item-link-Best%20Practices
			$this->addJSLine('tt_identity',"ttq.identify(".json_encode($values).")");
			
		}
		
		// STEP 3 : Event tracking calls
		$this->addEventTrackingCalls();
		
	
		
	}
	
	

	
}
