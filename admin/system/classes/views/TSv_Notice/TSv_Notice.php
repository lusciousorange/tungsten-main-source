<?php

class TSv_Notice extends TCv_View
{
	protected $title = false;
	protected $message = false;
	
	/**
	 * Constuctor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TSv_Notice');
	}
	
	/**
	 * Changes the title
	 * @param $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Sets the message
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}
	
	
	public function render()
	{
		if($this->title)
		{
			$title_view = new TCv_View();
			$title_view->setTag('h2');
			$title_view->addText($this->title);
			$this->attachView($title_view);
		}
		
		if($this->message)
		{
			$message_view = new TCv_View();
			$message_view->addClass('message');
			$message_view->addText($this->message);
			$this->attachView($message_view);
		}
		
	}
}
?>