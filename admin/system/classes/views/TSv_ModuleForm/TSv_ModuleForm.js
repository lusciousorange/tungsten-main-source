class TSv_ModuleForm extends  TCv_Form{

	constructor(element, params) {
		super(element, params);

		this.addFieldEventListener('is_public','change',this.isPublicChanged.bind(this), true);
	}


	/**
	 * Event handler for when is_public changes
	 * @param event
	 */
	isPublicChanged(event)
	{
		let is_public = this.element.querySelector('#is_public').value;
		if(is_public === '1')
		{
			this.hideFieldRows('user_groups');
		}
		else
		{
			this.showFieldRows('user_groups');
		}
	}
}