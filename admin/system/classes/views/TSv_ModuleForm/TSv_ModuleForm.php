<?php
/**
 * Class TSv_ModuleForm
 *
 * The form for editing module settings
 */
class TSv_ModuleForm extends TCv_FormWithModel
{
	/**
	 * TSv_ModuleForm constructor.
	 *
	 * @param string|TCm_Model $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
		$this->setSuccessURL('/admin/system/');
		
		$this->addClassJSFile('TSv_ModuleForm');
		$this->addClassJSInit('TSv_ModuleForm');
		
		
	}
	
	/**
	 * Configures the form elements
	 */
	public function configureFormElements()
	{
		
		$name = new TCv_FormItem_TextField('title', 'Title');
		$name->setHelpText('The title of the module');
		$name->setIsRequired();
		$this->attachView($name);
			
			
		$show_in_menu = new TCv_FormItem_Checkbox('show_in_menu', 'Show In Menu');
		$show_in_menu->setHelpText('Indicate if this module should appear in the menu listing.');
		$this->attachView($show_in_menu);
	
		$is_public = new TCv_FormItem_Select('is_public', 'User Group Access');
		$is_public->setHelpText('Indicate if access to this module is limited to only some user groups.');
		if($this->model->folder() != 'system')
		{
			$is_public->addOption('1', 'Public – All users can access module');
		}
		$is_public->addOption('0', 'Restricted – Only users in certain groups can access module');
		
		$this->attachView($is_public);
	
		$user_group_list = new TMm_UserGroupList();
		$user_groups = new TCv_FormItem_CheckboxList('user_groups', 'User Groups');
		$user_groups->setSaveToDatabase(false);
		$user_groups->setHelpText('Select the groups that can view this module. The Administrator group can view all modules and that cannot be changed.');

		$user_groups->setUseMultiple(
			TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
			'module_access_groups',
			'group_id',
			$this->model()
			
			);


		$selected_groups = array();
		if($this->isEditor())
		{
			$selected_groups = $this->model->permittedUserGroups();
		}
		$possible_groups = $user_group_list->groups();
		unset($possible_groups[1]);// remove admin from options
		
		$user_groups->setValuesFromObjectArrays($possible_groups, $selected_groups);
		$this->attachView($user_groups);
		
		
		
	}

}
?>