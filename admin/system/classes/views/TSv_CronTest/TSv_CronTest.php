<?php

/**
 * Class TSv_CronTest
 *
 * A basic view that runs the cron so that the console is available to be accessed. This view also disables the
 * one-minute delay on the cron manager.
 */
class TSv_CronTest extends TCv_View
{
	public function render()
	{
		$cron = TSm_CronManager::init();
		$cron->run();
		
		$this->addText('Cron Successfully Ran');
	}
}