<?php
/**
 * Class TSv_FontAwesome
 */
class TSv_FontAwesome extends TSv_Plugin
{
	public static string $library_folder = 'fontawesome-free-6.6.0-web';
	
	/**
	 * TSv_FontAwesome constructor.
	 *
	 * @param bool|TSm_Plugin $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Font Awesome');
		$this->setPluginDescription('Loads the <a href="https://http://fontawesome.io/" target="_blank" >Font Awesome library</a> which is used throughout the system for icons and user interface features. ');
		$this->setPluginIconName('fa-font');
		//$this->enableVariableForNum(1, 'Version', '4.7.0');
		//$this->enableVariableForNum(4, 'Use Local Version', '0');
		
		$this->setPublicAsRequired();
		$this->setAdminAsRequired();
		$this->setLiveAsRequired();
		$this->setBetaAsRequired();
		$this->setAsNotDeletable();
		
	}
	
	/**
	 *
	 */
	public function html()
	{
		
		// Separate files on purpose to streamline downloading
		$files = ['fontawesome.min.js', 'solid.min.js','brands.min.js',];//,'light.js','regular.js'];
		foreach($files as $filename)
		{
			$path = $this->classFolderFromRoot('TSv_FontAwesome',true).'/'.static::$library_folder."/js/".$filename;

			$this->addJSLine('fontawesome-pro-'.$filename,"
			script = document.createElement('script');
			script.src = '".$path."';
			document.head.appendChild(script);
		",true);
		
		}
		
	}
	
	
}
