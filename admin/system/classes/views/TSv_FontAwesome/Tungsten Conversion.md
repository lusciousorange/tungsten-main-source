# Tungsten Conversion

Read this doc 

https://fontawesome.com/how-to-use/upgrading-from-4

##Things to Search For
+ Any brand icons and update
+ Anytime FontAwesome is loaded as the FontFamily in CSS
+ Replace them with `$button.find('[data-fa-i2svg]').toggleClass('fa-eye').toggleClass('fa-eye-slash');`
+ usages of `.fa` in CSS, probably replace with SVG
+ Anything using the pseudo elements needs to be marked as hidden, then it inserts the SVG separately. You probably need to deal with that SVG still. 

