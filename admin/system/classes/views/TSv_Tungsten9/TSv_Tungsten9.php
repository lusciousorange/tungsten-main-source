<?php

/**
 * Extend the normal Tungsten to load this one instead trying to instantiate a new class entirely.
 */
class TSv_Tungsten9 extends TSv_Tungsten
{
	protected ?TSm_ModuleURLTarget $current_url_target = null;
	
	/**
	 * TSv_Tungsten constructor.
	 *
	 * The main constructor for Tungsten
	 */
	public function __construct()
	{
		parent::__construct();
		
		// Google Font for Tungsten
		$this->addMetaTag('gf_preconnect','<link rel="preconnect" href="https://fonts.googleapis.com">');
		$this->addMetaTag('gstatic_preconnect','<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>');
		$this->addMetaTag('google_font_poppins','<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,500;0,700;1,300;1,500;1,700&display=swap" rel="stylesheet">');
		
		
		// Handle Favicon
		$this->favicon = '/admin/system/classes/views/TSv_Tungsten9/images/favicon_v9_colour.png';
		if(TC_getConfig('is_localhost'))
		{
			$this->favicon = '/admin/system/classes/views/TSv_Tungsten9/images/favicon_v9_grey.png';
		}
		elseif(TC_getConfig('is_staging_domain'))
		{
			$this->favicon = '/admin/system/classes/views/TSv_Tungsten9/images/favicon_v9_orange.png';
		}
		
		// Set the form select arrow
		if(TC_isTungstenView())
		{
			$this->addFontAwesomeClassAsSymbol('TCv_Form9_select_arrow', 'fas fa-chevron-down');
		}
		
		// Disable the module bar
		$this->show_module_bar = false;
	}
	
	/**
	 * Overwrite the loading of the CSS and JS to only load Tungsten 9 stuff
	 * @return void
	 */
	public function loadTungstenCSSandJS() : void
	{
		$this->addClassCSSFile('TSv_Tungsten9');
		$this->addCSSFile('tungsten_print', '/admin/system/classes/views/TSv_Tungsten9/TSv_TungstenPrint.css', 'print ');
		
		$this->addClassJSFile('TSv_Tungsten');
		$this->addClassJSInit('TSv_Tungsten');
	}
	
	/**
	 * Returns the current URL target. If at any point, it can't find a valid URL target, it will return false.
	 * @return ?TSm_ModuleURLTarget
	 */
	public function currentURLTarget() : ?TSm_ModuleURLTarget
	{
		if(is_null($this->current_url_target))
		{
			if($module = $this->currentModule())
			{
				
				if($controller = $module->controller())
				{
					$this->current_url_target = $controller->currentURLTarget();
				}
			}
			
		}
		
		
		return $this->current_url_target;
	}
	
	/**
	 * The header view of Tungsten
	 * @return TCv_View
	 */
	public function headerView()
	{
		$header = new TCv_View("tungsten_header");
		$header->setTag('header');
		
		$header->attachView(TSv_StagingWarningBar::init());
		
		$header_top = new TCv_View('tungsten_header_top');
		
		// SEARCH
		if($this->isLoggedIn())
		{
			$search_form = TMv_TungstenSearchForm::init();
			$header_top->attachView($search_form);
		}
		
		$header_top->attachView($this->controlButtons());
		
		$header->attachView($header_top);
		
		//if($this->isLoggedIn())
		//{
		$toggle_link = new TCv_Link('tungsten_mobile_toggle');
		$toggle_link->addText('Main Menus');
		$toggle_link->setURL('#');
		$header->attachView($toggle_link);
		
		
		
		return $header;
	}
	
	/**
	 * Extends the control buttons
	 * @return TCv_View
	 */
	public function controlButtons()
	{
		$buttons = new TCv_View('control_buttons');
		
		if($this->isLoggedIn())
		{
			if($this->isInstalling())
			{
				$profile = new TCv_View('control_user_name');
				$profile->addClass('control_button_item');
				$profile->addText($this->user->firstName());
				$buttons->attachView($profile);
				
			}
			else
			{
				$profile = new TCv_Link('control_user_name');
				$profile->addClass('control_button_item');
				$profile->setTitle('Edit your profile');
				
				if(TC_getConfig('use_impersonate_user') && isset($_SESSION['impersonation_id']))
				{
					$profile->addClass('impersonation');
					$profile->setIconClassName('fas fa-user-ninja');
					$profile->setTitle('Cancel impersonation');
					$profile->addText('Impersonating ');
					$profile->setURL('/admin/system/do/cancel-impersonation/');
					
				}
				else
				{
					$profile->setIconClassName('fas fa-user-circle');
					$profile->setURL('/admin/profile/');
					
				}
				
				$profile->addText($this->user->firstName());
				$buttons->attachView($profile);
				
				
				// Potential Admin Cart Button
				if(class_exists('TMv_AdminCartButton') && TC_getModuleConfig('store','show_admin_cart'))
				{
					$buttons->attachView(TMv_AdminCartButton::init());
				}
				
				
				$logout = new TCv_Link('tungsten_sign_out_button');
				$logout->addClass('control_button_item');
				$logout->setURL('/admin/login/do/logout/');
				$logout->addText('Sign out');
				$logout->setIconClassName('fal fa-sign-out-alt');
				$buttons->attachView($logout);
			}
			
			
			
		}
		else // Signed Out
		{
			$logout = new TCv_Link('tungsten_sign_out_button');
			$logout->addClass('control_button_item');
			$logout->setURL('/');
			$logout->addText('Live website');
			$logout->setIconClassName('fal fa-sign-out-alt');
			$buttons->attachView($logout);
		}
		
		
		return $buttons;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// MAIN CONTENT VIEWS
	//
	// Handles the setting of main content views
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Sets the main content view for this Tungsten rendering. Dealing with the old setup, it's possible that we're
	 * given an array of views
	 * @param TCv_View|array $view
	 */
	public function setMainContentView($view) : void
	{
		// Possibly an array. Need to sort through and see how many we have
		if(is_array($view))
		{
			// Just one, clean view, attach it directly
			if(count($view) === 1)
			{
				$this->main_content_view = array_pop($view);
			}
			else // multiple or none
			{
				$container = new TCv_View('main_views');
				foreach($view as $view_item)
				{
					$container->attachView($view_item);
				}
				$this->main_content_view = $container;
			}
			
			
		}
		elseif ($view instanceof TCv_View)
		{
			$this->main_content_view = $view;
		}
	}
	
	
	
	
	/**
	 * Return the html for the site
	 * @return string
	 */
	public function render()
	{
		// Detect the fullpage and get out
		// Never load anything else
		if(isset($_GET['return_type']) && $_GET['return_type'] == 'fullpage')
		{
			return;
		}
		
		
		$this->configureForPrint();
		
		$dialog = new TCv_Dialog();
		$this->attachView($dialog);
		
		// Build the 3-panel interface
		$panel_container = TSv_Tungsten9Panels::init();
		
		// Add the three panels
		$panel_container->setLeftPanelView(TSv_Tungsten9_ModuleNavigationList::init());
		$panel_container->setCenterPanelView($this->main_content_view);
		$panel_container->configureRightPanelView();
		
		$this->attachView($panel_container);
		
		
	}
}