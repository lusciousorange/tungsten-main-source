<?php

/**
 * Class TSm_ModuleVariable
 *
 * This is currently a shell class this is used to define the tables, however it is not used in existing code. It was
 * created retroactively to properly support the new schema() mechanisms in Tungsten.
 */
class TSm_ModuleVariable extends TCm_Model
{
	protected $module_name, $variable_name, $value;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'module_variable_id';
	public static $table_name = 'module_variables';
	public static $model_title = 'Module variable';
	
	
	/**
	 * TMm_User constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		if(!$this->exists) { return null; }
		
		
	}
	
	/**
	 * The name of this variable
	 * @return string
	 */
	public function name()
	{
		return $this->variable_name;
	}
	
	/**
	 * The value of this variable
	 * @return string
	 */
	public function value()
	{
		return $this->value;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'module_name' => [
					'title'         => 'Module Name',
					'comment'       => 'The name of the module that this variable belongs to',
					'type'          => 'varchar(36)',
					'nullable'      => false,
					'index'         => 'module_name',
				],
				'variable_name' => [
					'title'         => 'Variable Name',
					'comment'       => 'The name of this variable',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'index'         => 'module_name',
				],
				'value' => [
					'title'         => 'Value',
					'comment'       => 'The value for this variable',
					'type'          => 'text',
					'nullable'      => false,
				],
			
			];
	}
	


}