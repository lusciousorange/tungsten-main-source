<?php

/**
 * The history item for a single row from a model table. The columns will vary for each model but this generalized
 * model is used to represent distinct states of the history for that item.
 */
class TSm_ModelHistoryState extends TCm_Model
{
	protected int $_history_id = 0;
	protected ?string $_history_timestamp;
	protected ?string $_history_version;
	protected string $_history_action;
	
	protected ?int $_history_user_id;
	
	/** @var class-string<TSt_ModelWithHistory>  */
	protected string $history_class_name;
	protected string $history_class_id_column;
	protected array $changes = [];
	
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = '_history_id'; // [string] = The id column for this class
	public static $model_title = 'Model history state';
	public static $primary_table_sort = '_history_timestamp DESC';
	
	/**
	 * The row from the history, whatever it is for the model.
	 * @param class-string<TCm_Model> $history_class_name The name of the class for this History Item.
	 * @param array $history_data_row
	 */
	public function __construct(string $history_class_name, array $history_data_row)
	{
		$this->addConsoleDebug($history_class_name);
		$this->addConsoleDebug($history_data_row);
		$this->history_class_name = $history_class_name;
		$this->history_class_id_column = $history_class_name::$table_id_column;
		parent::__construct($history_data_row);
	}
	
	/**
	 * Returns the date added formatted to a specific format.
	 * @param string $format The format to be used
	 * @return string
	 * @uses date()
	 * @uses strtotime()
	 */
	public function dateFormatted($format = 'F j, Y \a\t g:ia')
	{
		return date($format, strtotime($this->_history_timestamp));
	}
	
	/**
	 * Returns the history timestamp which is unformatted
	 * @return string
	 */
	public function historyTimestamp() : string
	{
		return $this->_history_timestamp;
	}
	
	/**
	 * Returns the history user ID
	 * @return ?int
	 */
	public function historyUserId() : ?int
	{
		return $this->_history_user_id;
	}
	
	/**
	 * Returns the name of the action that was taken which is update or delete
	 * @return string
	 */
	public function action() : string
	{
		return $this->_history_action;
	}
	
	public function isInsert() : bool
	{
		return $this->action() == 'insert';
	}
	
	public function isUpdate() : bool
	{
		return $this->action() == 'update';
	}
	
	public function isDelete() : bool
	{
		return $this->action() == 'delete';
	}
	
	/**
	 * Returns the name of the version
	 * @return ?string
	 */
	public function version() : ?string
	{
		return $this->_history_version;
	}
	
	/**
	 * Returns the name of the action that was taken which is update or delete
	 * @return ?TMm_User
	 */
	public function user() : ?TMm_User
	{
		if($this->_history_user_id > 0)
		{
			$user = TMm_User::init($this->_history_user_id);
			if($user->exists())
			{
				return $user;
			}
		}
		
		return null;
		
	}
	
	/**
	 * Returns the user can edit this state, which
	 * @param false $user
	 * @return
	 */
	public function userCanEdit($user = false) : bool
	{
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		if($model = $this->affectedModel())
		{
			return $model->userCanEdit($user);
		}
		
		// TODO: Sort out the scenario for deleted items
		return false;
		
		
	}
	
	/**
	 * changes for this item against a given state provided. The history state is an older one compared against a
	 * new value. If the state provided is the model itself, then we're comparing the changes against the live data
	 * set which is what is often done to the most recent change
	 * @param ?TSm_ModelHistoryState $previous_state
	 */
	public function trackChangesAgainstPreviousState(?TSm_ModelHistoryState $previous_state) : void
	{
		$this->changes = []; // Reset the changes, just in case it's called twice
		
		// Previous state is manually created means we don't show anything
//		if(is_null($previous_state))
//		{
//			return;
//		}
		
		// Compare the values in this state versus the provided state
		$schema = $this->history_class_name::schema();
		$column_type = 'text';
	
		$id_column_name = $this->history_class_name::$table_id_column;
		
		$ignored_columns = ($this->history_class_name)::historyAllIgnoredColumns();
		
		
		// Comparing two states
		foreach($schema as $column_name => $schema_values)
		{
			// Don't try to process the table keys values in schemas
			if($column_name == 'table_keys')
			{
				continue;
			}
			// Don't bother with ignored columns that might have slipped in
			if(in_array($column_name, $ignored_columns))
			{
				continue;
			}
			// Don't do anything for deleted columns
			if(isset($schema_values['delete']))
			{
				continue;
			}
			
			// Skip deletion. No need to track changes
			if($this->isDelete())
			{
				continue;
			}
			
			// Updates with the same values are skipped
			if($this->isUpdate() && $previous_state && $this->$column_name == $previous_state->valueForProperty($column_name))
			{
				continue;
			}
			
			// Get the class adjustments for the given values
			$class_adjustments = ($this->history_class_name)::historyChangeAdjustments($column_name, $this, $previous_state);
			
			// Null indicates to skip the change altogether
			if(is_null($class_adjustments))
			{
				continue;
			}
			
			
			// Determine the old value
			$old_value = '&nbsp;';
			if(isset($class_adjustments['old_value']))
			{
				$old_value = $class_adjustments['old_value'];
			}
			elseif($previous_state instanceof TSm_ModelHistoryState)
			{
				$old_value = $previous_state->valueForProperty($column_name);
			}
			
			$new_value = $this->$column_name;
			if(isset($class_adjustments['new_value']))
			{
				$new_value = $class_adjustments['new_value'];
			}
//			if(is_array($class_adjustments))
//			{
//				// Empty array returned, indicates to skip the value
//				if(count($class_adjustments) == 0)
//				{
//					continue;
//				}
//			}
			
			// Create the history change
			// Only bother if we're inserting OR we have a change in values
			//if($this->isInsert() || $old_value != $new_value)
			//{
				$history_state_change = new TSm_ModelHistoryChange($this->history_class_name,
				                                                   $this->$id_column_name,
				                                                   $column_name,
				                                                   $old_value,
				                                                   $new_value);
				
				// Detect a title adjustment
				if(isset($class_adjustments['title']))
				{
					$history_state_change->setColumnDisplayTitle($class_adjustments['title']);
				}
				
				// Show them all for INSERT
				if($this->isInsert())
				{
					$history_state_change->setAction('insert');
					
				}
				
				// UPDATE
				elseif($previous_state instanceof TSm_ModelHistoryState)
				{
					
					$history_state_change->setOldHistoryID($previous_state->historyID());
					
				}
				
				// Add the change
				$this->addChange($history_state_change);
				
			//}
			
		}
			
		
	}
	
	/**
	 * Returns the changes that have be processed for this state. This requires tracking against another state. THese
	 * are indexed by model name, to ensure proper grouping.
	 * @return TSm_ModelHistoryChange[][]
	 */
	public function changes() : array
	{
		return $this->changes;
	}
	
	/**
	 * Returns all the changes but flattened into a single array, removing any indexing based on classes
	 * @return TSm_ModelHistoryChange[]
	 */
	public function changesFlattened() : array
	{
		$flattened = [];
		foreach($this->changes() as $change_array)
		{
			foreach($change_array as $change)
			{
				$flattened[] = $change;
			}
			
		}
		
		return $flattened;
	}
	
	
	/**
	 * Adds a change to this state.
	 * @param TSm_ModelHistoryChange $history_change The model history change
	 * @param ?string $reference_index The index to store this under. If not set, it uses the reference index of the
	 * change by default. This can be used to attach a change to a specific model's history by using that's model's
	 * reference index.
	 * @return void
	 */
	public function addChange(TSm_ModelHistoryChange $history_change, ?string $reference_index = null) : void
	{
		if(is_null($reference_index))
		{
			$reference_index = $history_change->referenceIndex();
			
		}
		
		// Don't add empty values that are empty on insert
		// Just a useless value in most of the time
		if($this->isInsert() && (is_null($history_change->newValue()) || $history_change->newValue() == ''))
		{
			return;
		}
		
		
		
		if(!isset($this->changes[$reference_index]))
		{
			$this->changes[$reference_index] = [];
		}
		
//		$this->addConsoleDebug('class_name: '.$history_change->className());
//		$this->addConsoleDebug('column_name: '.$history_change->columnName());
//		$this->addConsoleDebug('model_id: '.$history_change->modelId());
//
		$this->changes[$reference_index][] = $history_change;
	}
	
	/**
	 * Takes an array of changes and adds them to this state. This handles possible scenarios where we have nested
	 * arrays
	 * @param array $changes
	 * @return void
	 */
	public function addChanges(array $changes) : void
	{
		foreach($changes as $change)
		{
			if($changes instanceof TSm_ModelHistoryChange)
			{
				
				$this->addChange($change);
			}
			
			// Recursive crawl through changes
			if(is_array($changes))
			{
				$this->addChanges($changes);
			}
		}
	}
	
	/**
	 * Returns if this state has any changes for a given reference index
	 * @param string $reference_index
	 * @return bool
	 */
	public function hasChangesForReferenceIndex(string $reference_index) : bool
	{
		return isset($this->changes[$reference_index]);
	}
	
	/**
	 * Returns the name of the model class used
	 * @return class-string<TCm_Model>
	 */
	public function className() : string
	{
		return $this->history_class_name;
	}
	
	/**
	 * Returns the history id , which is unique for each entry
	 * @return int
	 */
	public function historyID() : int
	{
		return $this->_history_id;
	}
	
	/**
	 * Gets the model that was impacted by this state. This may return null if the model no longer exists.
	 * @return TCm_Model|null
	 */
	public function affectedModel() : ?TCm_Model
	{
		// Get the column, in order to
		$class_column_name = $this->history_class_id_column;
		
		if($model = ($this->className())::init($this->$class_column_name))
		{
			return $model;
		}
		
		return null;
	}
	
	//////////////////////////////////////////////////////
	//
	// REVERSIONS
	//
	//////////////////////////////////////////////////////
	
	public function revertColumn(string $column) : void
	{
		$model = $this->affectedModel();
		if($model)
		{
			$model->updateDatabaseValue($column, $this->$column);
			TC_message('Reversion successful');
		}
		else
		{
			TC_message('Reversion failed : column not found');
		}
	}
	
	public static function revert($class_name, $history_id, $column) : void
	{
//		TC_addToConsole($class_name);
//		TC_addToConsole($history_id);
//		TC_addToConsole($column);
		
		$model = new TCm_Model(false);
		
		// Get all the history rows for this table, starting with the most recent
		$query = "SELECT * FROM `_history_".($class_name::tableName())
			."` WHERE _history_id = :_history_id  ORDER BY _history_timestamp DESC";
		$result = $model->DB_Prep_Exec($query, ['_history_id' => $history_id]);
		if($row = $result->fetch())
		{
			$state = new TSm_ModelHistoryState($class_name, $row);
			$state->revertColumn($column);
		}
		else
		{
			TC_message('Reversion failed : revision not found', false);
		}
		
	}
	
}