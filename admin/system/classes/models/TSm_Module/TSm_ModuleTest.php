<?php
class TSm_ModuleTest extends TC_ModelTestCase
{
	
	
	
	//////////////////////////////////////////////////////
	//
	// USER GROUPS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Tests adding a user group to a module
	 * @return array
	 */
	public function testAddToUserGroup()
	{
		// Use a module that always exists
		$module = TSm_Module::init('login');
		
		$user_group = TMm_UserGroup::createWithValues(['title' => 'Test Add Group']);
		
		// Add to user group
		$module->updateGroupSetting($user_group, true);
		$this->assertTrue($module->groupHasAccess($user_group));
		
		return ['module' => $module, 'group' => $user_group];
	}
	
	/**
	 * Tests removing a user group from a module
	 * @param array $values An array with two indices for the module and the group
	 * @depends testAddToUserGroup
	 */
	public function testRemoveFromUserGroup(array $values)
	{
		$this->enableDebugger();
		$module = $values['module'];
		$user_group = $values['group'];
		
		// Add to user group
		$module->updateGroupSetting($user_group, false);
		$this->assertFalse($module->groupHasAccess($user_group));
		
	}
	
}