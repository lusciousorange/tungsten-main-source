<?php
/**
 * Class TSm_Module
 *
 * A model that represents a module in the system
 */
class TSm_Module extends TCm_Model
{
	protected $module_id; // [int] = The id for this module
	protected $folder; // [string] = The folder for this module
	protected $title; // [string] = The title for this module
	protected $show_in_menu; // [bool] = Indicates if this module appears in the menus
	
	// A value to indicate if the module can be shown in the left menu
	// This is pulled from the settings. A value of null indicates that
	// The setting can be switched, which is what is actually stored in
	// `$show_in_menu` above, which is the value that is followed in that case.
	protected ?bool $show_in_left_menu = null;
	
	protected $version; // [string] = The current version of this module
	protected $server_version; // [string] = The version that is sitting on the server
	protected $installed = false; // [bool] = Indicates if the module is installed
	protected $permitted_user_groups = false; // [array] = List of TMm_UserGroup items that can see this module
	protected bool $installable = true; // [bool] = Indicates if it can be installed. Default to true
	protected $controller_class = false;
	protected $variables_form_class = false;
	protected $module_settings_class = 'TSm_ModuleSettings';
	protected $variables = false;
	protected ?string $model_name = null;
	protected array $model_names = array();
	protected $is_public = false;
	
	
	protected $is_login_startpage = false;
	protected $url_targets = false;
	protected $class_names, $configuration_type, $icon_code, $variable_values;
	protected $button_color = false;
	protected $warning_message = false;
	protected $auto_update = false;
	
	protected $model_names_secondary = null;
	
	protected $notifications = [];
	
	protected $api_settings = null;
	
	// Values pulled from config that might be set
	protected string $min_system_version;
	protected string $module_title;
	protected string $override_module_class_name;
	protected bool $is_dashboard = false;
	
	public static $table_id_column = 'module_id';
	public static $table_name = 'modules'; // [string] = The table for this class
	public static $model_title = 'Module';
	
	// All the possible values, which we grab even if deprecated
	// Excludes any values which are overwritten by the module settings in the DB. Those are ignored.
	public static array $module_setting_values = ['module_title','version','min_system_version','icon_library_code','model_name',
			'model_names_secondary','controller_class','variables_form_class','override_module_class_name','show_in_left_menu',
			'button_color','warning_message','module_settings_class','installable','is_public'];
	
	/**
	 * Returns the upload folder path
	 * @return string
	 */
	public static function uploadFolder()
	{
		return TC_getConfig('saved_file_path').'/module-settings/';
	}
	
	
	/**
	 * TSm_Module constructor.
	 * @param array|int|string  $module_id
	 */
	public function __construct($module_id)
	{
		
		parent::__construct($module_id, false);
		$this->addDeleteTable('modules', 'folder');
		
		if(is_array($module_id) || is_int($module_id) || ctype_digit($module_id))
		{
			$this->setPropertiesFromTable();
			$this->installed = true;
		}
		elseif($module_id == 'install')
		{
			$this->setPropertiesFromModuleFolder($module_id);
			$this->installed = false;
		}
		else
		{
			if(TCm_Database::tableInstalled('modules'))
			{
				$mem_key = 'TSm_Module_'.$module_id;
				$row = TC_Memcached::get($mem_key);
				if(!is_array($row))
				{
					$query = "SELECT * FROM `modules` WHERE folder = :folder LIMIT 1";
					$result = $this->DB_Prep_Exec($query, array('folder' => $module_id));
					$row = $result->fetch();

					// Avoid trying to get them if they aren't set
					// Set it to an empty arrow so we know we have the data
					if($row === false)
					{
						$row = [];
					}
					
					TC_Memcached::set($mem_key, $row); // save to cache
				}

				if($row)
				{

					$this->id = $row['module_id'];
					$this->convertArrayToProperties($row);
					$this->installed = true;
				}
				else
				{

					$this->setPropertiesFromModuleFolder($module_id);
					$this->installed = false;
				}



			}
			else
			{
				$this->setPropertiesFromModuleFolder($module_id);
				$this->installed = false;
				
			}
		}
		
		$this->updateModelValuesWithConfigSettings();
		
	}
	
	/**
	 * An internal function that takes all teh config settings and updates the properties of this model with those values.
	 * @return void
	 */
	protected function updateModelValuesWithConfigSettings() : void
	{
		$config_settings = static::moduleConfigSettings($this->folder);
	
		// A lookup table when we should use a different internal value for the value in the settings
		$variable_name_changes = [
			'version' => 'server_version',
			'icon_library_code' => 'icon_code',
		];
		
		// Loop through every config setting, some might be null
		foreach($config_settings as $config_name => $config_value)
		{
			// If the value isn't null, then we have an override essentially
			if(!is_null($config_value))
			{
				$class_property_name = $config_name;
				if(isset($variable_name_changes[$config_name]))
				{
					$class_property_name = $variable_name_changes[$config_name];
				}
				
				$this->$class_property_name = $config_value;
			}
		}
		
		
		
		
		if(is_string($this->model_name))
		{
			$this->model_names[$this->model_name] = array( 'url_target_name_prefix' => '', 'init_url_targets' => true);
		}
		
		// Need both as we might end up with no values
		if(isset($config_settings['model_names_secondary']) && is_array($config_settings['model_names_secondary']))
		{
			foreach($config_settings['model_names_secondary'] as $model_name => $values)
			{
				// enforce the dash if necessary
				if(trim($values['url_target_name_prefix']) != ''
					&& substr($values['url_target_name_prefix'], -1)  != '-')
				{
					$values['url_target_name_prefix'] .= '-';
				}
				$this->model_names[$model_name] = $values;
			}
			//	$this->model_names = array_merge($this->model_names, $model_names_secondary);
		}
		
		
		
		
		if($this->controller_class == '')
		{
			$this->controller_class = 'TSc_ModuleController';
		}
		
		if($this->icon_code == '')
		{
			$this->icon_code = 'cube';
		}
		
		$this->configuration_type = 'automatic';
		
	
	}
	
	/**
	 * Acquires the module settings for this module
	 * @return array
	 */
	public static function moduleConfigSettings($folder) : array
	{
		// Get the site version, since we update on each version
		
		$version = TCv_Website::gitVersion();
		// A unique key for this module, which is updated with each version
		$key_name = 'module_settings_'.$folder.':'.$version;
	
		// Try and find a saved version
		if(TC_Memcached::enabled())
		{
			$mem_value = TC_Memcached::get($key_name);
			if(!is_array($mem_value))
			{
				$mem_value = static::processModuleSettingsFile($folder);
				TC_Memcached::set($key_name, $mem_value, 600);
			}
			
			return $mem_value;
			
			
			
		}
		else // use session saving
		{
			// It's not set, so calculate it
			if(!isset($_SESSION[$key_name]))
			{
				$_SESSION[$key_name] = static::processModuleSettingsFile($folder);
			}
			
			return $_SESSION[$key_name];
		}
	}
	
	/**
	 * Internal function to take the module setting file and generate the array of values that is used in the future
	 * @param string $folder
	 * @return array
	 */
	protected static function processModuleSettingsFile(string $folder) : array
	{
		$settings = [];
		
		
		$path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$folder.'/config/settings.php';
		if(file_exists($path))
		{
			include($path);
			foreach(static::$module_setting_values as $variable_name)
			{
				// If that variable exists in the scope, then it was set
				// Otherwise it's a null value which indicates it wasn't set but we still want the array index to exist here
				if(isset($$variable_name))
				{
					$settings[$variable_name] = $$variable_name;
				}
				else
				{
					$settings[$variable_name] = null;
				}
				
			}
		}
		
		
		return $settings;
	}
	
	/**
	 * Enables the functionality to auto-install this module whenever it comes up
	 */
	public function setAsAutoUpdate()
	{
		$this->auto_update = true;
	}
	
	/**
	 * A hook method that allows for a class to trigger actions immediately after the init. This allows for the class
	 * to be fully instantiated before calling methods that may refer back to it.
	 */
	public function runAfterInit()
	{
		// Handle auto_update for this module
		if($this->auto_update && $this->requiresUpgrade())
		{
			$this->install();
		}
		
		
	}
	
	
	/**
	 * Used to instantiate a module object that is not currently installed in the system. In this case,
	 * it fills in the information it can from the folder.
	 * @param string $folder
	 */
	public function setPropertiesFromModuleFolder(string $folder) : void
	{
		$this->folder = $folder;
		$this->show_in_menu = false;
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/admin/'.$folder.'/config/settings.php'))
		{
			$config_settings = static::moduleConfigSettings($folder);
			
			$this->title = $config_settings['module_title'];
			$this->server_version = $config_settings['version'];
		}
		else
		{
			$this->title = 'Not Found';
		}
		
	}
	
	/**
	 * Checks if a user has permission to view the module
	 * @param TMm_User $user
	 * @return bool
	 */
	public function checkUserPermission(TMm_User $user) :bool
	{
		if($user->inGroupID(1))
		{
			return true;
		}
		elseif($this->isAllAccess())
		{
			return true;
		}
		
		else
		{
			foreach($this->permittedUserGroups() as $group)
			{
				if($user->inGroupID($group->id()))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns if this module is set to be public. If so, anyone with access can see it.
	 * @return bool
	 */
	public function isPublic() : bool
	{
		return $this->is_public;
	}
	
	/**
	 * Returns if this module is set to be accessed by anyone in tungsten.
	 * @return bool
	 */
	public function isAllAccess() : bool
	{
		return $this->is_public;
	}
	
	/**
	 * The list of groups that can view this group
	 * @return TMm_UserGroup[]|bool
	 */
	public function permittedUserGroups()
	{
		if($this->permitted_user_groups !== false)
		{
			return $this->permitted_user_groups;
		}
		
		$this->permitted_user_groups = array();
		
		$group = TMm_UserGroup::init( 1);
		$this->permitted_user_groups[$group->id()] = $group;
		
		
		$query = "SELECT * FROM `module_access_groups` WHERE module_id = :module_id";
		$result = $this->DB_Prep_Exec($query, array('module_id' => $this->id()));
		while($row = $result->fetch())
		{
			$group =  TMm_UserGroup::init( $row['group_id']);
			$this->permitted_user_groups[$group->id()] = $group;
			
		}
		
		return $this->permitted_user_groups;
	}
	
	/**
	 * Checks if the group provided has access to this module
	 * @param TMm_UserGroup $user_group
	 * @return bool
	 */
	public function groupHasAccess(TMm_UserGroup $user_group)
	{
		return $this->groupWithIDHasAccess($user_group->id());
	}
	
	/**
	 * @param int $group_id
	 * @return bool
	 */
	public function groupWithIDHasAccess(int $group_id)
	{
		$this->permittedUserGroups(); // prime system
		return isset($this->permitted_user_groups[$group_id]);
	}
	
	/**
	 * Checks if a user is an a particular user group given the code for that group
	 * @param string $code
	 * @return bool
	 * @deprecated User Group codes are no longer used and supported
	 */
	public function groupWithCodeHasAccess(string $code)
	{
		foreach($this->permittedUserGroups() as $group)
		{
			if($group->code() == $code)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Updates a single group value to indicate if the module can be seen by this group or not. This function coincides
	 * with the required function for the TCv_FormItem_CheckboxList class.
	 * @param TMm_UserGroup $user_group
	 * @param bool $is_member
	 */
	public function updateGroupSetting(TMm_UserGroup $user_group, bool $is_member)
	{
		$current_member = $this->groupHasAccess($user_group);
		
		// Current Member that shouldn't be
		if($current_member && !$is_member)
		{
			$query = "DELETE FROM module_access_groups WHERE module_id = :module_id AND group_id = :group_id";
			$this->DB_Prep_Exec($query, array('module_id' => $this->id(), 'group_id' => $user_group->id()));
			unset($this->permitted_user_groups[$user_group->id()]);
			
		}
		
		// NOT A MEMBER
		elseif(!$current_member && $is_member)
		{
			TSm_ModuleAccessGroup::createWithValues(['module_id' => $this->id(), 'group_id' => $user_group->id()]);
			$this->permitted_user_groups[$user_group->id()] = $user_group;
		}
	}
	
	
	/**
	 * Returns the folder for this module
	 * @return string
	 */
	public function folder()
	{
		return $this->folder;
	}
	
	/**
	 * Returns if the folder exists, which may not always be true if we're dealing with branched repos or other
	 * scenarios where a folder gets deleted.
	 * @return bool
	 */
	public function folderExists()
	{
		return is_dir($_SERVER['DOCUMENT_ROOT'].'/admin/'.$this->folder());
	}
	
	/**
	 * Returns the folder for this module
	 * @return string
	 */
	public function controllerClass()
	{
		return $this->controller_class;
	}
	
	/**
	 * Returns the controller for this module
	 *
	 * @return bool|TSc_ModuleController
	 */
	public function controller()
	{
		return ($this->controller_class)::init($this);
	}
	
	/**
	 * Returns the title for this module
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Returns the button color for this module. If set to false, then defaults are used.
	 * @return string|bool
	 */
	public function buttonColor()
	{
		return $this->button_color;
	}
	
	/**
	 * Returns the warning message. If set to false, then it is not displayed.
	 * @return string|bool
	 */
	public function warningMessage()
	{
		return $this->warning_message;
	}
	
	/**
	 * Returns the configuration type
	 * @return string
	 */
	public function configurationType()
	{
		return $this->configuration_type;
	}
	
	/**
	 * Returns the title for the model for objects in this module
	 * @return string
	 */
	public function modelTitle()
	{
		$model_class_name = $this->modelName();
		if($model_class_name)
		{
			return $model_class_name::$model_title;
		}
		return '';
	}
	
	/**
	 * Returns if this module is shown in the menus. This method respects the `showInLeftMenu` functionality which is
	 * hard-coded into some modules with booleans that are immutable.
	 * @return bool
	 */
	public function showInMenu() : bool
	{
		// Weird catch for Tungsten 8 vs 9. System module in particular is shown
		
		// A null value indicates that we can actually follow the setting in Tungsten, however a hard-coded boolean
		// indicates an immutable option that can't be overridden.
	
		if(is_null($this->showInLeftMenu()))
		{
			return $this->show_in_menu;
		}
		
		return $this->showInLeftMenu();
	}
	
	/**
	 * Returns the module-setting value which indicates if this module can be shown in the menu at all. If this value
	 * is a boolean, then that's an indication that the setting can't be changed. Otherwise, it's editable by the user.
	 * @return ?bool
	 */
	public function showInLeftMenu() : ?bool
	{
		return $this->show_in_left_menu;
	}
	
	/**
	 * Returns if this module is shown in the menus
	 */
	public function setToShowInMenu() : void
	{
		$this->show_in_menu = true;
	}
	
	/**
	 * Toggles if this module is shown in the menus
	 */
	public function toggleShowInMenu()
	{
		if($this->installed)
		{
			try
			{
				$query = "UPDATE modules SET show_in_menu = !show_in_menu WHERE module_id = :module_id";
				$this->DB_Prep_Exec($query, array('module_id' => $this->id()));
			}
			catch(PDOException $e)
			{
			
			}
			$this->show_in_menu = !$this->show_in_menu;
		}
	}
	
	/**
	 * Returns if the module is installed
	 * @return bool
	 */
	public function installed()
	{
		return $this->installed;
	}
	
	/**
	 * Installs the module with the values provided
	 * @uses TSu_InstallProcessor
	 * @see TSu_InstallProcessor::installModule()
	 */
	public function install()
	{
		$processor = new TSu_InstallProcessor('update_module');
		$processor->installModule($this->folder());
		
		
	}
	
	/**
	 * Updates the module configuration which is stored in the `modules` table.
	 */
	public function updateModuleConfiguration()
	{
		// double check when trying the "install" module. It never triggers "installed" value
		if($this->folder == 'install')
		{
			$query = "SELECT * FROM `modules` WHERE folder = :folder LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('folder'=> 'install'));
			
			if($row = $result->fetch())
			{
				$this->installed = true;
			}
		}
		
		
		// Don't update the version number if there was ANY error
		if($this->DB()->hasError())
		{
			return ;
		}
		
		// VERSION FILE PROPERTIES
		include($_SERVER['DOCUMENT_ROOT'] . '/admin/' . $this->folder() . '/config/settings.php');
		// TC_addToConsole('Reading: '.$_SERVER['DOCUMENT_ROOT'] . '/admin/' . $this->folder() . '/config/settings.php');
		$values = [];
		$values['folder'] = $this->folder();
		$values['version'] = @$version;
		$values['title'] = @$module_title;
		$values['show_in_menu'] = @$show_in_menu ? '1' : '0';
		$values['is_public'] = @$is_public ? '1' : '0';
		
		
		if($this->installed)
		{
			$install_values = [];
			$install_values['title'] = $values['title'];
			$install_values['version'] = $values['version'];
			
			$this->updateWithValues($install_values);
			
		}
		else
		{
			$query = "INSERT INTO modules SET ";
			foreach($values as $name => $db_value)
			{
				$query .= "$name = :$name ,";
			}
			
			$query .= "date_added = now()";
			$this->DB_Prep_Exec($query, $values);
			$this->installed = true;
		}
		
	}
	
	/**
	 * Returns if the module requires an upgrade
	 * @return bool
	 */
	public function requiresUpgrade() : bool
	{
		if($this->installed && is_string($this->server_version) && is_string($this->version))
		{
			return version_compare($this->server_version, $this->version, '>');
		}
		return false;
	}
	
	/**
	 * Returns the version of the module that is sitting on the server. Not necessarily installed
	 * @return string
	 */
	public function serverVersion()
	{
		return $this->server_version;
	}
	
	
	/**
	 * Returns the version of the module that is setup in the DB
	 * @return string
	 */
	public function version()
	{
		return $this->version;
	}
	
	/**
	 * Returns if the module is installable
	 * @return bool
	 */
	public function installable()
	{
		return $this->installable;
	}
	
	/**
	 * Returns the icon code for the module
	 * @return string
	 */
	public function iconCode()
	{
		return $this->icon_code;
	}
	
	//////////////////////////////////////////////////////
	//
	// START PAGE
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns if this module is the start page module
	 * @return bool
	 */
	public function isLoginStartPage()
	{
		return $this->is_login_startpage;
	}
	
	/**
	 * Sets this module as the start page module
	 */
	public function setAsStartPage()
	{
		if($this->installed)
		{
			$query = "UPDATE modules SET is_login_startpage = 0";
			$this->DB_Prep_Exec($query);
			
			$query = "UPDATE modules SET is_login_startpage = 1 WHERE module_id = :module_id";
			$this->DB_Prep_Exec($query, array('module_id' => $this->id()));
			$this->is_login_startpage = true;
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// MODELS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the title for the model for objects in this module
	 * @return ?string
	 */
	public function modelName() : ?string
	{
		return $this->model_name;
	}
	
	/**
	 * Returns the array of model names for this module
	 * @return string[]
	 */
	public function modelNames() : array
	{
		return $this->model_names;
	}
	
	/**
	 * Returns the URL target prefix for a given model name of model
	 * @param TCm_Model $model
	 * @return string
	 */
	public function urlTargetNamePrefixForModel($model)
	{
		if($model instanceof TCm_Model)
		{
			$model_name = get_class($model);
		}
		else
		{
			$model_name = $model;
		}
		return $this->model_names[$model_name]['url_target_name_prefix'];
	}
	
	//////////////////////////////////////////////////////
	//
	// VARIABLES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the name of the form that is used to edit variables for this class
	 * @return string
	 */
	public function variableFormClassName()
	{
		return $this->variables_form_class;
	}
	
	/**
	 * Returns the object that manages the module settings
	 * @return TSm_ModuleSettings|bool
	 */
	public function moduleSettings()
	{
		return ($this->module_settings_class)::init($this);
	}
	
	/**
	 * Returns the variables for this module
	 * @return array|bool
	 */
	public function variables()
	{
		return $this->moduleSettings()->variables();
	}
	
	/**
	 * Returns the last date in which the variables were modified.
	 * @param string $variable_name
	 * @return string
	 */
	public function variableDateModified($variable_name)
	{
		return $this->moduleSettings()->variableDateModified($variable_name);
	}
	
	/**
	 * Returns the variable for the name given
	 * @param string $variable_name
	 * @return string
	 */
	public function variable($variable_name)
	{
		// Save it globally, if we haven't pulled it yet
		if( ! isset($GLOBALS['TC_module_configs'][$this->folder()][$variable_name] ) )
		{
			$variable = $this->moduleSettings()->variable($variable_name);
			$GLOBALS['TC_module_configs'][$this->folder()][$variable_name] = $variable;
		}
		return $GLOBALS['TC_module_configs'][$this->folder()][$variable_name];
	}
	
	/**
	 * Returns the variable for the name given
	 * @param string $name
	 * @return bool
	 */
	public function variableExists($name)
	{
		return $this->moduleSettings()->variableExists($name);
	}
	
	/**
	 * Returns if this module has variables that can be set
	 * @return bool
	 */
	public function hasVariables()
	{
		return $this->variables_form_class != false;
	}
	
	/**
	 * Updates the variables for this module
	 * @param array $values
	 */
	public function updateVariables($values)
	{
		$this->moduleSettings()->updateVariables($values);
	}
	
	/**
	 * Updates the variables for this module
	 * @param string $variable_name
	 * @param string $value
	 */
	public function updateVariableWithName($variable_name, $value)
	{
		$this->moduleSettings()->updateVariableWithName($variable_name, $value);
	}
	
	/**
	 * Updates the variables for this module
	 * @param array $values
	 */
	public function installVariables($values)
	{
		$this->moduleSettings()->installVariables($values);
	}
	
	/**
	 * Deletes a variable with a given name
	 * @param string $variable_name
	 */
	public function deleteVariable(string $variable_name) : void
	{
		$this->moduleSettings()->deleteVariable($variable_name);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// URL TARGETS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the list of URL targets for tis module
	 * @return TSm_ModuleURLTarget[]
	 */
	public function URLTargets()
	{
		if($this->url_targets === false)
		{
			$controller_class = $this->controller_class;
			$controller = ($controller_class)::init($this);
			$this->url_targets = $controller->URLTargets();
		}
		return $this->url_targets;
	}
	
	/**
	 * Returns the number of URL targets for this module
	 * @return int
	 */
	public function numURLTargets()
	{
		return sizeof($this->URLTargets());
	}
	
	/**
	 * Returns the URL Target with name provided in this module
	 * @param string $name
	 * @return bool|TSm_ModuleURLTarget
	 */
	public function URLTargetWithName($name)
	{
		foreach($this->URLTargets() as $url_target)
		{
			if($url_target->name() == $name)
			{
				return $url_target;
			}
		}
		return false;
	}
	
	/**
	 * Returns the URL targets by parent name
	 * @return TSm_ModuleURLTarget[]
	 */
	public function urlTargetsByParent()
	{
		$ordered_url_targets = array();
		
		foreach($this->URLTargets() as $url_target)
		{
			// no parent, add it to the list
			if(!$url_target->parentURLTarget())
			{
				$ordered_url_targets[$url_target->name()] = $url_target;
				$ordered_url_targets = $ordered_url_targets + $this->urlTargetsForParent($url_target);
			}
		}
		
		return $ordered_url_targets;
		
	}
	
	/**
	 * Recursive function which returns an array of URL targets associated with a parent.
	 * @param TSm_ModuleURLTarget $parent_URL_target
	 * @return array
	 */
	protected function urlTargetsForParent($parent_URL_target)
	{
		$child_url_targets = array();
		// loop through them all, yup, super inefficient, but that's what we got
		foreach($this->URLTargets() as $url_target)
		{
			if($url_target->parentURLTarget())
			{
				// if the parent's name matches the name of the endpoitn being looked at
				if($parent_URL_target->name() == $url_target->parentURLTarget()->name())
				{
					$child_url_targets[$url_target->name()] = $url_target;
					$child_url_targets = $child_url_targets + $this->urlTargetsForParent($url_target);
				}
			}
		}
		
		return $child_url_targets;
	}
	
	/**
	 * Returns the path for this module to the /do/ folder for actions
	 * @return string
	 */
	public function pathForDoAction()
	{
		return '/admin/' . $this->folder(). '/do/';
		
	}
	
	/**
	 * Returns the list of all the class names found within this module
	 * @param string|null type_folder The name of the type folder such as 'models', 'views', etc
	 *
	 * @return array
	 */
	public function classNames($type_folder = null)
	{
		if($this->class_names === NULL)
		{
			$this->class_names = array();
			$folder_path = $_SERVER['DOCUMENT_ROOT'] . '/admin/' . $this->folder() . '/classes/';
			if (is_dir($folder_path))
			{
				// loop through the classes folder
				$classes_folders = array_diff(scandir($folder_path), array('..', '.'));
				foreach ($classes_folders as $class_folder_name)
				{
					// Skip if we've defined a folder and this iteration isn't for that folder
					if($type_folder != null && $class_folder_name != $type_folder)
					{
						continue;
					}
					if (is_dir($folder_path . $class_folder_name))
					{
						// loop through the classes in each folder
						$class_names = scandir($folder_path . $class_folder_name);
						foreach ($class_names as $class_name)
						{
							if(class_exists($class_name) || interface_exists($class_name) || trait_exists($class_name))
							{
								$this->class_names[] = $class_name;
							}
						}
						
					}
				}
				
			}
		}
		return $this->class_names;
	}
	
	/**
	 * A static method to detect if a particular module has an override method for the actual module class. This is
	 * set via the `$override_module_class_name` property in the settings for that module.
	 * @param mixed $args,... The arguments provided for the init
	 * @return string
	 * @see get_called_class()
	 * @see TCu_Item::overrideClassName()
	 */
	public static function classNameForInit(...$args)
	{
//		// Init directly on the parent class
		if(get_called_class() != 'TSm_Module')
		{
			return static::overrideClassName(get_called_class());
		}
		
		// Check for an override on
		
		
		
		// Check for an extended class
		if(isset($args[0]) && $args[0] != 'TSm_Module')
		{
			// array of db values
			if(is_array($args[0]))
			{
				$folder = $args[0]['folder'];
			}
			else // string being passed in
			{
				$folder = $args[0];
			}
			
			// Get the settings and try to load it
			$settings = static::moduleConfigSettings($folder);
			
			if( isset($settings['override_module_class_name'])
				&& is_string($settings['override_module_class_name'])
				&& class_exists($settings['override_module_class_name']))
			{
				return static::overrideClassName($settings['override_module_class_name']);
			}
			
		}
		return 'TSm_Module';
	}
	
	/**
	 * The value that should be used as a parameter for singletons. By default all singletons use a string called
	 * singleton which ensures they fall in the same category. It's possible to override that value.
	 * @return string|mixed
	 */
	public static function initSingletonIdentifier()
	{
		// singleton loading a parent class, in which case we want to use the folder for the module for consistent
		// loading. Eg: If they call `TMm_PagesModule`, we want to use the string `pages` as the value return.
		if(get_called_class() != 'TSm_Module')
		{
			$reflection = new ReflectionClass(get_called_class());
			$path = $reflection->getFileName();
			$str_pos = strpos($path,'/admin/');
			return explode('/', substr($path,$str_pos+7) )[0];
			
		}
		return parent::initSingletonIdentifier();
	}
	
	//////////////////////////////////////////////////////
	//
	// NOTIFICATIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the number of notifications for this module
	 * @return int
	 */
	public function numNotifications() : int
	{
		return count($this->notifications);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the classes for this module that use localization
	 * @return string[]|TCm_Model[]
	 */
	public function classesUsingLocalization()
	{
		$class_names = [];
		
		// Loop through the classes for this module
		foreach($this->classNames('models') as $class_name)
		{
			// Not using TCm_Model
			if(!method_exists($class_name,'classNameForInit'))
			{
				continue;
			}
			
			// Get the proper class name
			$class_name = $class_name::classNameForInit();
			
			// Values must be TCm_Models
			if(!is_subclass_of($class_name, 'TCm_Model'))
			{
				continue;
			}
			
			// Don't bother if it doesn't have localized schema
			if(!method_exists($class_name, 'localizedSchemaFields'))
			{
				continue;
			}
			
			$localized_schema = $class_name::localizedSchemaFields();
			
			// Don't bother if there is no localized schema fields
			if(count($localized_schema) == 0)
			{
				continue;
			}
			
			// We made it here, so save the class name
			$class_names[$class_name] = $class_name;
			
			
		}
		
		// Deal with Pages, which has some special cases
		if($this->folder() == 'pages')
		{
			// Renderer Items
			// Used in other modules, shouldn't be there
			unset($class_names['TMm_PageRendererContent']);
			unset($class_names['TMm_PageRendererContentVariable']);
			
			// Page Content Variables, only certain ones are needed
			//	unset($class_names['TMm_PagesContentVariable']);
		}
		
		return $class_names;
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// API SETTINGS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this module uses the API
	 * @return bool
	 */
	public function usesAPI()
	{
		if(!TC_getConfig('use_api'))
		{
			return false;
		}
		
		$path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$this->folder().'/config/api.php';
		return file_exists($path);
	}
	
	
	/**
	 * Returns the api settings for this module
	 * @return array
	 */
	public function apiSettings()
	{
		if($this->api_settings == null)
		{
			$this->api_settings = array();
			
			// Only bother if we're using the API
			if($this->usesAPI())
			{
				include_once( $_SERVER['DOCUMENT_ROOT'].'/admin/'.$this->folder().'/config/api.php');
				$this->api_settings = $api;
			}
			
		}
		
		return $this->api_settings;
	}
	
	
	public function userCanDelete($user = false)
	{
		if($this->folder() == 'install')
		{
			return true;
		}
		
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	public function clearCache(): void
	{
		parent::clearCache();
		
		$version = TCv_Website::gitVersion();
		
		//TC_addToConsole('clearing '.$this->folder());
		// A unique key for this module, which is updated with each version
		$key_name = 'module_settings_'.$this->folder().':'.$version;
		TC_Memcached::delete($key_name);
		
		// Also delete the session memory
		unset($_SESSION[$key_name]);
	}
	
	/**
	 * Clears all the cache that it cna find for this module. This includes the module's cache as well as any related
	 * models that it can find
	 * @return void
	 */
	public function clearAllModelMemcached() : void
	{
		// Clear this module's cache
		$this->clearCache();
		
		if(is_string($this->model_name) && ($this->model_name)::hasTrait('TCt_MemcachedModel') )
		{
			$all_items = ($this->model_name)::allModels();
			foreach($all_items as $model)
			{
				$model->clearCache();
			}
			
		}
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'folder' => [
					'title'         => 'Folder',
					'comment'       => 'The folder for this module',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title displayed for this module',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'is_public' => [
					'title'         => 'Public',
					'comment'       => 'Indicates if the module is publicly accessible to all users in admin',
					'type'          => 'bool ',
					'nullable'      => false,
				],
				'show_in_menu' => [
					'title'         => 'Show in Menu',
					'comment'       => 'Indicates if the module appears in the navigation in Tungsten',
					'type'          => 'bool ',
					'nullable'      => false,
				],
				'is_login_startpage' => [
					'title'         => 'Login Startpage',
					'comment'       => 'Indicates if the module is the start page when logging in',
					'type'          => 'bool DEFAULT false',
					'nullable'      => false,
				],
				'version' => [
					'title'         => 'Version',
					'comment'       => 'The version for the module',
					'type'          => 'varchar(16)',
					'nullable'      => false,
				],
				
				// We don't use this, breaks in some installations
				'display_order' => [
					'delete'         => true,
				],
			];
	}
	
}
