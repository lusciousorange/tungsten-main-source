<?php
/**
 * Class TSm_ConsoleItemList
 */
class TSm_ConsoleItemList extends TCm_Model
{
	protected $console_items = false;
	
	/**
	 * TSm_ConsoleItemList constructor.
	 */
	public function __construct()
	{
		parent::__construct('console_items');
		
		$console_installed = true;
		try 
		{
        	$db = TCm_Database::connection();
        	$result = $db->query("SELECT 1 FROM console_items LIMIT 1");
    	} 
    	catch (Exception $e) 
    	{
        	$console_installed = false;
    	}
		

		if($console_installed)
		{	
			$this->clearOldConsoleItems();
			
		}	

			
	}
	
	/**
	 * Returns a limited number of console items that happen before the provided microtime
	 *
	 * @param float $last_parent_microtime
	 * @param string $last_instance_id
	 * @param int $quantity
	 * @return TSm_ConsoleItem[]
	 */
	public function consoleItemsWithMicrotimeInstanceIdWithQuantity($last_parent_microtime, $last_instance_id, $quantity)
	{
		$console_items = [];
		
		$values = array();
		$values['session_id'] = session_id();
		$values['parent_microtime'] = $last_parent_microtime;
		$values['last_instance_id'] = $last_instance_id;
		
		// Add all the items within the quantity limit
		$query = "SELECT * FROM `console_items`
			WHERE session_id = :session_id AND parent_microtime = :parent_microtime AND instance_id > :last_instance_id
			ORDER BY instance_id ASC LIMIT $quantity ";
		
		//print $this->compositeQueryWithValues($query, $values);
		
		$result = $this->DB_Prep_Exec($query, $values);
		while($row = $result->fetch())
		{
			$console_item = new TSm_ConsoleItem($row, TSm_ConsoleOutput);
			$console_items[] = $console_item;
		}
		
		return $console_items;
		
		
	}
	
	/**
	 * Returns a limited number of console items that happen before the provided microtime
	 *
	 * @param float $last_parent_microtime
	 * @param int $quantity
	 * @return TSm_ConsoleItem[]
	 */
	public function consoleItemsBeforeMicrotimeWithQuantity($last_parent_microtime, $quantity)
	{
		$console_items = array();
		if($last_parent_microtime == 0)
		{
			$last_parent_microtime = 99999999999;
		}
		
		$values = array();
		$values['session_id'] = session_id();
		$values['parent_microtime'] = $last_parent_microtime;
		
		// Add all the items within the quantity limit
		$query = "SELECT * FROM `console_items`
			WHERE session_id = :session_id AND parent_microtime < :parent_microtime
			ORDER BY parent_microtime DESC, instance_id ASC LIMIT $quantity ";
		
		
		$result = $this->DB_Prep_Exec($query, $values);
		while($row = $result->fetch())
		{
			$console_item = new TSm_ConsoleItem($row, TSm_ConsoleOutput);
			$console_items[] = $console_item;
		}
	
		
		return $console_items;
	}
	
	
	/**
	 * Returns the console items
	 *
	 * @return bool|TSm_ConsoleItem[]
	 */
	public function consoleItems()
	{	
		return $this->console_items;
	}
	
	/**
	 * clears old console items
	 */
	public function clearOldConsoleItems()
	{
		$hours = TC_getModuleConfig('system', 'console_hour_limit');
		if($hours == '')
		{
			$hours = 12;
			TC_setModuleConfig('system','console_hour_limit',$hours);
		}
		$query = "DELETE FROM console_items WHERE date_added < DATE_SUB(NOW(), INTERVAL :hours HOUR);";
		
		// Direct call to database. No need for DB_Prep_Exec
		$db = TCm_Database::connection();
		$statement = $db->prepare($query);
		$statement->execute(['hours' => $hours]);
		
		
		
		
	}

	/**
	 * Clears the console
	 * @deprecated No longer used.
	 */
	public function clearConsole()
	{	
	}
	
}
?>