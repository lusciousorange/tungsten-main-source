<?php
/**
 * Class TSm_CronManager
 *
 * The manager that processes all the cron jobs in the system. This class loads any model which uses the interface
 * TSi_Cronable. Each class implements the runCron() method to perform the necessary tasks.
 */
class TSm_CronManager extends TCm_Model
{
	protected ?array $cron_model_names = null;
	
	/** @var string  Tracks the value for a single cron class if called */
	protected string $single_cron_class = '';
	
	/**
	 * TSm_CronManager constructor.
	 */
	public function __construct()
	{
		parent::__construct('cron_manager');
		
	}
	
	
	/**
	 * Manager is a singleton
	 * @return bool
	 */
	public static function isSingleton() : bool
	{
		return true;
	}
	
	/**
	 * Finds all possible cronable classes. If a class name was provided as a URL param, then that's the only one
	 * that will run.
	 */
	protected function processCronClasses() : void
	{
		if($this->cron_model_names === null)
		{
			$this->cron_model_names = [];
			
			// Single class
			if($this->single_cron_class != '')
			{
				$this->cron_model_names[] = $this->single_cron_class;
				
			}
			else
			{
				$module_list = TSm_ModuleList::init();
				foreach($module_list->modules() as $module)
				{
					// check that a dashboard plugin folder exists for the module
					$this->processModuleForCronClasses($module);
				}
			}
			
		}
	}
	
	/**
	 * Processes a single module folder looking for cron models
	 * @param TSm_Module $module
	 */
	protected function processModuleForCronClasses(TSm_Module $module) : void
	{
		// loop through the models for this module
		
		$directory = @dir($_SERVER['DOCUMENT_ROOT'].'/admin/'.$module->folder().'/classes/models/');
		if($directory)
		{
			while($class_name = $directory->read())
			{
				if(class_exists($class_name))
				{
					$interfaces = class_implements($class_name);
					if(isset($interfaces['TSi_Cronable']))
					{
						$this->cron_model_names[$class_name] = $class_name;
					}
				}
			}
		}	
	}
	
	/**
	 * Runs the cron job
	 */
	public function run() : void
	{
		date_default_timezone_set('America/Winnipeg');
		
		// Set the config to be in Tungsten
		// This simplifies access calls to use the parent
		TC_setConfig('in_tungsten_view', true);
		
		// Generate the tracking on the call
		$cron_log_item = $this->logCronCall();
		
		// prime to only load a single model
		if(isset($_GET['class_name']))
		{
			$this->single_cron_class = htmlentities($_GET['class_name']);
		}
		
		
		$this->triggerClassCalls();
		
		$this->triggerTaskCalls();
		
		$this->clearOldCronCalls();
		
		if($cron_log_item instanceof TSm_CronCall)
		{
			$cron_log_item->trackCronCompleted();
		}
		
		TC_setConfig('in_tungsten_view', false);
	}
	
	/**
	 *
	 * Triggers the calls for each class and calls the runCron() method on those classes. If a class_name was
	 * provided via the URL, then only that class will be called.
	 * @return void
	 */
	protected function triggerClassCalls() : void
	{
		// Load all classes if not set already
		$this->processCronClasses();
		foreach($this->cron_model_names as $model_name)
		{
			// for each class model, run the cron
			if( method_exists($model_name,'runCron') )
			{
				$model_name::runCron();
			}
		}
	}
	
	/**
	 * Triggers the calls for the tasks. If a class_name was provided then only tasks that have that class_name will
	 * be called.
	 * @return void
	 */
	protected function triggerTaskCalls() : void
	{
		$query = "SELECT * FROM `cron_tasks` WHERE is_complete = :is_complete ";
		$values = ['is_complete' => '0'];
		
		if($this->single_cron_class != '')
		{
			$query .= ' AND model_class_name = :model_class_name';
			$values['model_class_name'] = $this->single_cron_class;
		}
		
		$query .= ' ORDER BY date_added ASC ';
		$result = $this->DB_Prep_Exec($query, $values);
		
		// Loop through the tasks, run each one
		while($row = $result->fetch())
		{
			$task = TSm_CronTask::init($row);
			$task->run();
			unset($task);
		}
	}
	
	/**
	 * Triggers the tracking of the cron call
	 * @return TSm_CronCall|null
	 */
	public function logCronCall() : ?TSm_CronCall
	{
		// DB tracking
		if(TC_getModuleConfig('system', 'cron_tracking'))
		{
			$backfiles = array_reverse(debug_backtrace());
			$filepath = $backfiles[0]['file'];
			$filename = explode('/', $filepath);
			$filename = array_pop($filename);
			return TSm_CronCall::createWithValues([
				                                      'class_name' => $this->single_cron_class,
				                                      'file' => $filename]);
			
		}
		
		return null;
	}
	
	/**
	 * Clears old cron calls that are past 30 days. which is the limit we keep for these records
	 */
	public function clearOldCronCalls() : void
	{
		$query = "DELETE FROM cron_calls WHERE date_added < DATE_SUB(NOW(), INTERVAL 30 DAY)";
		$this->DB_Prep_Exec($query);
		
		
	}
	
	/**
	 * Runs the cron for a class name asynchronously from the rest of the script. This can be used to ensure immediate
	 * processing without interfering with the natural flow of the script.
	 * @param string $class_name
	 */
	public function runForClassAsynchronously(string $class_name) : void
	{
		// See http://stackoverflow.com/questions/962915/how-do-i-make-an-asynchronous-get-request-in-php
		// For TLS http://stackoverflow.com/questions/1757957/how-do-i-get-ssl-working-in-fsockopen
		
		$host = TC_getModuleConfig('system', 'cron_domain');

		if($host == '')
		{
			$host = $_SERVER['SERVER_NAME'];
		}
		//$this->addConsoleDebug('Running – Cron Async - '.$host );
		//$this->addConsoleDebug('Running – Server Port - '.$_SERVER['SERVER_PORT'] );

		try
		{
			$fp = fsockopen(
				$host,
				$_SERVER['SERVER_PORT'],
				$errno,
				$errstr,
				30
			);
			
			if(strpos($host, '://') > 0)
			{
				$host_url = explode('://', $host);
				$host = $host_url[1];
			}
			
			$out = "GET /cron.php?class_name=" . htmlentities($class_name) . " HTTP/1.1\r\n";
			$out .= "Host: " . $host . "\r\n";
			$out .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n\r\n";
			
			fwrite($fp, $out);
			fclose($fp);
		}
		catch(Exception $e)
		{
			$this->addConsoleDebug('fsock failure');
		}
	}

	
}
?>