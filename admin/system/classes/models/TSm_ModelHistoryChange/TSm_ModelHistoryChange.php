<?php

/**
 * Value object to track a change between two History states.
 */
class TSm_ModelHistoryChange
{
	/** @var string $class_name The name of the class for his change. This dictates how a lot of the code is
	 * formatted and shown.
	 */
	protected string $class_name;
	
	/** @var string $column_name The name of the column for this change. */
	protected string $column_name;
	
	/**
	 * @var string $column_type The type for this column. By default it's text since
	 */
	protected string $column_type = 'text';
	
	protected int $model_id;
	
	protected $old_value;
	protected $new_value;
	protected ?int $old_history_id = null;
	protected string $action = 'update';
	
	protected array $schema = [];
	
	protected bool $is_related_entry = false;
	protected bool $is_related_heading = false;
	
	/**
	 * @var string|null $column_display_title The title that is shown for this column, calculated based on a few factors
	 */
	protected ?string $column_display_title = null;
	
	/**
	 * @param class-string<TCm_Model> $class_name The class name for this change
	 * @param int $model_id The id of the model that we're tracking the change on
	 * @param string $column_name The column name for this change
	 * @param mixed $old
	 * @param mixed $new
	 */
	public function __construct(string $class_name, int $model_id, string $column_name, $old, $new)
	{
		$this->class_name = $class_name;
		$this->model_id = $model_id;
		$this->old_value = $old;
		$this->new_value = $new;
		$this->column_name = $column_name;
		
		// Get the column type from the schema
		// Only bother if it's set
		$this->schema = $class_name::schema();
		if(isset($this->schema[$column_name]))
		{
			$this->column_type = $this->schema[$column_name]['type'];
		}
		
	}
	
	/**
	 * A value that is used to reference this change which is unique to this change. Every change is associated with
	 * a state, which is a single moment in time when something happened.
	 * @return string
	 */
	public function referenceIndex(): string
	{
		return $this->className().'-'.$this->modelId();
	}
	
	/**
	 * Sets the ID for the old history ID, which is used for reversions
	 * @param int $history_id
	 * @return void
	 */
	public function setOldHistoryID(int $history_id) : void
	{
		$this->old_history_id = $history_id;
	}
	
	/**
	 * Returns the old history ID, which might be null
	 * @return int|null
	 */
	public function oldHistoryID() : ?int
	{
		return $this->old_history_id;
	}
	
	/**
	 * Sets the action for this change
	 * @param string $action The name of the action, which must match one of the three possible trigger event names
	 * of insert, update, or delete
	 * @return void
	 */
	public function setAction(string $action): void
	{
		$action = strtolower($action);
		// Validate the action name
		if($action != 'insert' && $action != 'update' && $action != 'delete')
		{
			TC_triggerError('Invalid action type');
		}
		$this->action = $action;
	}
	
	
	/**
	 * Returns the action name
	 * @return string
	 */
	public function action() : string
	{
		return $this->action;
	}
	
	public function isInsert() : bool
	{
		return $this->action() == 'insert';
	}
	
	public function isUpdate() : bool
	{
		return $this->action() == 'update';
	}
	
	public function isDelete() : bool
	{
		return $this->action() == 'delete';
	}
	
	public function className() : string
	{
		return $this->class_name;
	}
	
	public function modelId() : int
	{
		return $this->model_id;
	}
	
	public function columnName() : string
	{
		return $this->column_name;
	}
	
	public function setAsRelatedEntry(): void
	{
		$this->is_related_entry = true;
	}
	
	public function isRelatedEntry() : bool
	{
		return $this->is_related_entry;
	}
	
	
	public function setAsRelatedHeading(): void
	{
		$this->is_related_heading = true;
	}
	
	public function isRelatedHeading() : bool
	{
		return $this->is_related_heading;
	}
	
	
	/**
	 * Sets the display title for this change. This is an override to deal with presenting complex changes in a more
	 * user-friendly way.
	 * @param string $title
	 * @return void
	 */
	public function setColumnDisplayTitle(string $title) : void
	{
		$this->column_display_title = $title;
	}
	
	/**
	 * Determines and returns the display title for the column. This handles comparing against the schema and other
	 * factors.
	 * @return string
	 */
	public function columnDisplayTitle() : string
	{
		if(is_null($this->column_display_title))
		{
			$this->column_display_title = $this->column_name;
			
			// Detect if the schema has a title
			if(isset($this->schema[$this->column_name]['title']))
			{
				$this->column_display_title = $this->schema[$this->column_name]['title'];
			}
			
			// Detect if the type is a class name, use the singular version of the model title
			elseif(substr($this->column_type,0, 4) == 'TMm_')
			{
				$this->column_display_title = $this->column_type::$model_title;
			}
			
			// sentence case, replacing underscores with spaces
			else
			{
				$this->column_display_title = ucfirst(str_replace('_',' ', $this->column_display_title));
			}
		}
		
		// Deal with headings
		if($this->isRelatedHeading())
		{
			if($this->isInsert())
			{
				$this->column_display_title .= ' added';
			}
			elseif($this->isDelete())
			{
				$this->column_display_title .= ' deleted';
			}
			else
			{
				$this->column_display_title .= ' updated';
			}
		}
		
		return $this->column_display_title;
		
	}
	
	/**
	 * The icon used for this change.
	 * @return string
	 */
	public function displayIconName() : string
	{
		if($this->isUpdate() && $this->isRelatedHeading())
		{
			return 'fas fa-exchange';
		}
		
		if($this->isInsert())
		{
			return 'fas fa-plus';
		}
		elseif($this->isDelete())
		{
			return 'fas fa-times';
		}
		
		return 'fas fa-arrow-right';
		
	}
	
	/**
	 * Processes an input string which is normally text from the database and returns values that are safe for HTML
	 * output. This includes processing the change based on the attributes to show better values for strings, columna
	 * names, objects, etc
	 * @param ?string $input
	 * @return string
	 */
	protected function htmlSafeOutput(?string $input) : string
	{
		if(is_null($input) || $input == '')
		{
			return '<em>empty</em>';
		}
		
		// Boolean types
		if(substr($this->column_type,0,4) == 'bool')
		{
			return $input ? 'True' : 'False';
		}
		
		// Module Views
		if(substr($this->column_type, 0, 4) == 'TMv_')
		{
			// Page content views have titles
			if(method_exists($this->column_name,'pageContent_ViewTitle'))
			{
				return $this->column_name::pageContent_ViewTitle();
			}
		}
		
		
		
		// Models
		if(substr($this->column_type, 0, 4) == 'TMm_')
		{
			if($input > 0) // we need an id
			{
				// Try and find the model, based on the column type, we hae a model
				$model = ($this->column_type)::init($input);
				if($model)
				{
					return $model->title().' ['.$model->id().']';
				}
				else // ID but// model not found, since deleted
				{
					return '<em>' . $this->columnDisplayTitle() . ' not found</em>';
				}
			}
		}
		
		return $input;
	}
	
	public function oldHTML() : string
	{
		if($this->isInsert())
		{
			return '&nbsp;';
		}
		return $this->htmlSafeOutput($this->old_value);
	}
	
	public function newHTML() : string
	{
		return $this->htmlSafeOutput($this->new_value);
	}
	
	
	public function oldValue()
	{
		return $this->old_value;
	}
	
	public function newValue()
	{
		return $this->new_value;
	}
	
}