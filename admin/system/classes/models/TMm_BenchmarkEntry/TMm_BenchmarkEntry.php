<?php

class TMm_BenchmarkEntry extends TCm_Model
{
	protected static ?TMm_BenchmarkEntry $instance = null;
	protected ?int $start_microtime = null;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'benchmark_entry_id';
	public static $table_name = 'benchmarks';
	public static $model_title = 'Benchmark entry';

	/**
	 * Constructor for a new console item
	 */
	public function __construct($id)
	{
		
			parent::__construct($id);
	}
	


	
	
	/**
	 * Returns the instance for the benchmark entry, which can only exist once per script load
	 * @param array $init_values The initial values to be passed in when creating the benchmark. Has no effect after
	 * initial load
	 * @return ?TMm_BenchmarkEntry
	 */
	public static function instance(array $init_values = []) : ?TMm_BenchmarkEntry
	{
		// No benchmarking used
		if(!TC_getConfig('use_benchmarking'))
		{
			return null;
		}
		if (is_null(static::$instance))
		{
			$instance = static::createWithValues($init_values);
			
			// Null, likely a table install issue
			if(is_null($instance))
			{
				TC_clearMessenger();
				$install = new TSm_InstallTable('TMm_BenchmarkEntry',[]);
				$install->install();
				TC_message(TMm_BenchmarkEntry::tableName() .' now installed');
				$instance = static::createWithValues($init_values);
				
			}
			
			// Save the value
			static::$instance = $instance;
		}
		return static::$instance;
	}
	
	public function trackRenderTimes() : void
	{
		$render_microtime = microtime(true)*1000;
		$this->updateDatabaseValue('render_microtime', $render_microtime);
	}
	
	public function trackPrintTimes() : void
	{
		$render_microtime = microtime(true)*1000;
		$this->updateWithValues([
			                        'print_microtime' => $render_microtime,
			            //            'print_total' => $this->start_microtime - $render_microtime
		                        ]);
	}




	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [

				
				'start_microtime' => [
					'type'          => 'bigint unsigned',
					'nullable'      => true,
				],
				'session_start_microtime' => [
					'type'          => 'bigint unsigned',
					'nullable'      => true,
				],
				'time_to_session_start_ms' => [
					'comment'       => 'The calculated number of microseconds from start to session start',
					'type'          => 'int unsigned as (session_start_microtime - start_microtime) ',
					'nullable'      => true,
				],
				'render_microtime' => [
					'type'          => 'bigint unsigned',
					'nullable'      => true,
				],
				'time_to_render_ms' => [
					'comment'       => 'The calculated number of microseconds from start to render',
					'type'          => 'int unsigned as (render_microtime - start_microtime) ',
					'nullable'      => true,
				],
				'print_microtime' => [
					'type'          => 'bigint unsigned',
					'nullable'      => true,
				],
				'print_total' => [
					'comment'       => 'Calculated field',
					'type'          => 'int unsigned as (print_microtime - start_microtime) ',
					'nullable'      => true,
				],
				
				
				'url' => [
					'type'          => 'mediumtext',
					'nullable'      => true,
				],
				'is_annotation' => [
					'comment'       => 'Indicates if this is a manual annotation',
					'type'          => 'bool',
					'nullable'      => false,
				],
				


			];


	}


}
