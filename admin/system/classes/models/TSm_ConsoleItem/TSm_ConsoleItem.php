<?php

class TSm_ConsoleItem extends TCm_Model
{
	protected int $console_item_id;
	
	protected $mode = false;
	protected $db_transaction_id = false;
	
	protected $message = ''; // [string] = The message to be displayed
	protected $message_2 = ''; // [string] = The message to be displayed
	protected $message_3 = ''; // [string] = The message to be displayed
	
	protected $type; // [int] = The type of console item being added
	protected $is_error = false; // [bool] = Indicates if this console item is an error
	protected $is_skipped = false; // [bool] = Indicates if this console item was skipped
	
	protected $details = false; // [string] = The description of this console item
	protected ?string $class_name = null; // [string] = The class where the item occurred
	protected $backtrace = ''; // [array] = The list of backtraces for this console item
	protected ?float $time_spent = null;
	
	protected $page;
	protected $instance_id;
	protected $microtime;
	protected $parent_microtime;
	
	protected ?string $session_id;
	protected static $timer_lap = false;
	
	protected $memory_usage = 0;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'console_item_id'; // [string] = The id column for this class
	public static $table_name = 'console_items'; // [string] = The table for this class
	public static $model_title = 'Console Item';
	
	public static $committing_permitted = -1;
	
	/**
	 * Constructor for a new console item
	 * @param string|array $message_or_value_array
	 * @param string $type
	 * @param bool $is_error
	 */
	public function __construct($message_or_value_array, $type = TSm_ConsoleMessage, $is_error = false)
	{
		// Detect if we're setting a timer or the final page rendering
		if($type == TSm_ConsoleTimer || $type == TSm_ConsolePageRendering)
		{
			$microtime = microtime(true);
			
			$timer_start = TC_getConfig('timer_start');
			
			// Set the timer lap if it's set, we use the time spent field
			if(static::$timer_lap)
			{
				// Show the interval in the timer column
				$this->setTimeSpent($microtime - static::$timer_lap);
			}
			
			// Second column is the memory usage
			$this->message_2 = static::currentMemoryUsage();
			
			
			$this->message_3 =  number_format( $microtime - $timer_start,3).'s' ;
			
			// Save the latest lap timer
			static::$timer_lap = $microtime;
			
			
			
		}
		// Existing Model Mode
		if($type == TSm_ConsoleOutput)
		{
			parent::__construct($message_or_value_array);
			$this->setPropertiesFromTable();
			$this->mode = 'output';
		}
		else // new item mode
		{
			$this->mode = 'input';
			$this->message = $message_or_value_array;
			$this->type = $type;
			$this->is_error = $is_error;
			
			$this->includeBacktrace();
		}
		
		// if not set yet, then calculate one time
		if(static::$committing_permitted == -1)
		{
			try
			{
				$db = TCm_Database::connection();
				$result = @$db->query("SELECT 1 FROM console_items LIMIT 1");
				static::$committing_permitted = true;
			}
			catch (Exception $e)
			{
				static::$committing_permitted = false;
			}
		}
	}
	
	
	
	
	/**
	 * Console ALWAYS write to the main database
	 */
	public static function DB_Connection() : ?TCm_Database
	{
		return TCm_Database::connection();
	}
	
	
	//////////////////////////////////////////////////////
	//
	// MESSAGE
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Sets the message
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}
	
	/**
	 * Returns the message
	 * @return array|mixed|string
	 */
	public function message()
	{
		return $this->message;
	}
	
	/**
	 * Returns message1, since there can be three
	 * @return array|mixed|string
	 */
	public function message1()
	{
		return $this->message;
	}
	
	/**
	 * Returns message2, since there can be three
	 * @return array|mixed|string
	 */
	public function message2()
	{
		return $this->message_2;
	}
	
	/**
	 * Returns message3, since there can be three
	 * @return array|mixed|string
	 */
	public function message3()
	{
		return $this->message_3;
	}
	
	/**
	 * Sets this as a three column message with values
	 * @param string $column_1_content
	 * @param string $column_2_content
	 * @param string $column_3_content
	 * @return void
	 */
	public function setThreeColumnMessage($column_1_content, $column_2_content, $column_3_content )
	{
		$this->setMultiColumnMessage($column_1_content, $column_2_content, $column_3_content );
	}
	
	/**
	 * Sets as a multi-column message
	 * @param $column_1_content
	 * @param $column_2_content
	 * @param $column_3_content
	 * @return void
	 */
	public function setMultiColumnMessage($column_1_content, $column_2_content, $column_3_content = false)
	{
		$this->message = $column_1_content;
		if($column_2_content == NULL)
		{
			$column_2_content = false;
		}
		$this->message_2 = $column_2_content;
		$this->message_3 = $column_3_content;
	}
	
	/**
	 * Returns if this is a multi-column console item
	 * @return bool
	 */
	public function isMultiColumn()
	{
		return $this->message_2 != '' || $this->message_3 != '';
	}
	
	//////////////////////////////////////////////////////
	//
	// PAGE
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the page for this console item
	 * @return mixed|string
	 */
	public function page()
	{
		return $this->page;
	}
	
	/**
	 * Returns the position count
	 * @return mixed
	 */
	public function pagePositionCount()
	{
		return $this->instance_id;
	}
	
	//////////////////////////////////////////////////////
	//
	// TYPE
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the type for this console item
	 * @return int|string
	 */
	public function type()
	{
		return $this->type;
	}
	
	/**
	 * Returns the name of the type for this console item
	 * @return int|string
	 */
	public function typeName()
	{
		if($this->type == TSm_ConsoleMessage) return 'Message';
		if($this->type == TSm_ConsoleDebug) return 'Debug';
		if($this->type == TSm_ConsoleWarning) return 'Warning';
		
		if($this->type == TSm_ConsoleQuery_Select) return 'Query-Select';
		if($this->type == TSm_ConsoleQuery_Insert) return 'Query-Insert';
		if($this->type == TSm_ConsoleQuery_Update) return 'Query-Update';
		if($this->type == TSm_ConsoleQuerySkipped) return 'Query Skipped';
		if($this->type == TSm_ConsoleFormValidation) return 'Form Validation';
		if($this->type == TSm_ConsoleFormValue) return 'Form Value';
		if($this->type == TSm_ConsoleFormSetting) return 'Form Setting';
		if($this->type == TSm_ConsoleFormWarning) return 'Warning';
		if($this->type == TSm_ConsoleQuery_Delete) return 'Query-Delete';
		if($this->type == TSm_ConsoleFormDatabaseValue) return 'Form DB Value';
		if($this->type == TSm_ConsolePageRendering) return 'Page-Render';
		if($this->type == TSm_ConsolePageStart) return 'Page-Start';
		
		
		if($this->type == TSm_ConsoleAPICall) return 'API Call';
		
		if($this->type == TSm_ConsoleUnitTest) return 'Unit Test';
		
		if($this->type == TSm_ConsoleTimer) return 'Timer';
		
		// CUSTOM TYPE
		return $this->type;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// ERRORS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this console item is an error
	 * @return bool
	 */
	public function isError()
	{
		return $this->is_error;
	}
	
	/**
	 * Sets the value for if this console item is an error
	 * @param bool $is_error
	 * @return void
	 */
	public function setIsError($is_error = true)
	{
		$this->is_error = $is_error;
		
		$this->includeBacktrace();
		
	}
	
	//////////////////////////////////////////////////////
	//
	// SKIPPED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this console item is skipped
	 * @return bool|mixed
	 */
	public function isSkipped()
	{
		return $this->is_skipped;
	}
	
	/**
	 * Sets the value for if this console item is skipped
	 * @param bool $is_skipped
	 * @return void
	 */
	public function setIsSkipped($is_skipped = true)
	{
		$this->is_skipped = $is_skipped;
	}
	
	//////////////////////////////////////////////////////
	//
	// BACKTRACES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Adds the backtrace to the console item
	 * @return void
	 */
	public function includeBacktrace() : void
	{
		$backtrace = TC_backtrace(true);
		$this->backtrace = json_encode($backtrace);
		
		// Detect if there is no class
		if(is_null($this->class_name))
		{
			$filename = $backtrace[count($backtrace)-1]['f'];
			$filename = str_ireplace('.php','', $filename);
			if(class_exists($filename))
			{
				$this->setClassName($filename);
			}
			
			
		}
		
		
		
	}
	
	/**
	 * Indicates if there are backtraces
	 * @return bool
	 */
	public function hasBacktrace() : bool
	{
		return $this->backtrace != '';
	}
	
	/**
	 * Returns the backtraces for this item
	 * @return mixed|string
	 */
	public function backtrace()
	{
		return $this->backtrace;
	}
	
	//////////////////////////////////////////////////////
	//
	// DETAILS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the details
	 * @return bool|mixed
	 */
	public function details()
	{
		return $this->details;
	}
	
	/**
	 * Sets the value of the details
	 * @param string|array $details
	 * @return void
	 */
	public function setDetails($details)
	{
		if(is_array($details))
		{
			$this->details = $this->tableForDetailArray($details);
		}
		else
		{
			$this->details = $details;
		}
		
	}
	
	/**
	 * Return a table for an array
	 * @param array $array
	 * @return string
	 */
	public function tableForDetailArray($array)
	{
		$table_text = '<table class="data_table">';
		foreach($array as $index => $value)
		{
			$table_text .= '<tr>';
			$table_text .= '<td>'.$index.'</td>';
			$table_text .= '<td>';
			if(is_array($value))
			{
				$table_text .= $this->tableForDetailArray($value);
			}
			elseif(is_null($value))
			{
				$table_text .= '<em>null</em>';
			}
			elseif(is_object($value))
			{
				$table_text .= '[Object] '.get_class($value);
			}
			else
			{
				$table_text .= htmlentities($value);
				
			}
			$table_text .= '</td>';
			$table_text .= '</tr>';
			
		}
		$table_text .= '</table>';
		return $table_text;
	}
	
	//////////////////////////////////////////////////////
	//
	// CLASS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the class that this item occurred in
	 * @return ?string
	 */
	public function className() : ?string
	{
		return $this->class_name;
	}
	
	/**
	 * Sets the name of the class where this console item occurred
	 * @param string $class_name
	 * @return void
	 */
	public function setClassName(string $class_name) : void
	{
		$this->class_name = $class_name;
	}
	
	//////////////////////////////////////////////////////
	//
	// TIME
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the microtime that this was processed
	 * @return mixed
	 */
	public function timeProcessed()
	{
		return $this->microtime;
	}
	
	/**
	 * Returns the parent microtime
	 * @return mixed
	 */
	public function parentMicrotime()
	{
		return $this->parent_microtime;
	}
	
	/**
	 * Returns the amount of time spent on the action.
	 * @return null|float
	 */
	public function timeSpent() : ?float
	{
		return $this->time_spent;
	}
	
	/**
	 * Sets the amount of time spent on the action
	 * @param float $time_spent
	 * @return void
	 */
	public function setTimeSpent($time_spent)
	{
		$this->time_spent = $time_spent;
	}
	
	public function instanceID()
	{
		return $this->instance_id;
	}
	
	//////////////////////////////////////////////////////
	//
	// TRANSACTION IDs
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the DB transaction ID
	 * @param int $id
	 * @return void
	 */
	public function setDBTransactionID($id)
	{
		$this->db_transaction_id = $id;
	}
	
	/**
	 * Returns the DB transaction ID
	 * @return bool|mixed
	 */
	public function dbTransactionID()
	{
		return $this->db_transaction_id;
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMORY USAGE
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the memory usage for this console item. This is normally done automatically by the TSv_Console object
	 * when the console item is added to the console.
	 * @param $usage
	 * @return void
	 */
	public function setMemoryUsage($usage)
	{
		$this->memory_usage = $usage;
	}
	
	/**
	 * Returns the memory usage at the time the console item is added to the console
	 * @return int|mixed
	 */
	public function memoryUsage()
	{
		return $this->memory_usage;
	}
	
	//////////////////////////////////////////////////////
	//
	// COMMIT
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the calculated page name.
	 * @return mixed
	 */
	public function currentPage()
	{
		return $_SERVER['REQUEST_URI'];
	}
	
	/**
	 * A function that shows the errors for unit testing on code. This is a different set of errors that some console
	 * items might not show otherwise.
	 * @return void
	 */
	public function printUnitTestError()
	{
		if($this->isError())
		{
			print "\nERROR: ";
		}
		else
		{
			print "\n>> ";
		}
		print strip_tags($this->class_name.' : '.$this->message);
		
		if($this->isError())
		{
			
			
			if($this->details != '')
			{
				print "\n" . $this->details;
			}
			$backtrace = TC_backtrace(true);
			$count = 1;
			foreach($backtrace as $values)
			{
				print "\n" . $count++ . '. ' . $values['f'] . ' :: ' . $values['l'] . ' :: ' . $values['d'];
			}
			print "\n";
		}
		//trigger_error('', E_USER_ERROR);
	}
	
	/**
	 * Creates the console entry in the system
	 * @return void
	 */
	public function commit()
	{
		if(($this->is_error|| $this->type == TSm_ConsoleDebug ) && TC_getConfig('console_is_unit_test'))
		{
			$this->printUnitTestError();
		}
		
		if(!TC_getConfig('console_saving'))
		{
			return;
		}
		
		// check that committing is permitted
		if(!static::$committing_permitted)
		{
			return;
		}
		
		$values = array();
		$values['page'] = $this->currentPage();
		
		// Avoid tracking the update to the console script
		if(strpos($values['page'] , 'TSv_Console') > 0)
		{
			return;
		}
		
		$microtime = microtime(true);
		
		
		if($this->type == TSm_ConsolePageStart)
		{
			// Use the microtime from the start of the script
			if(TC_getConfig('timer_start'))
			{
				//	$microtime = TC_getConfig('timer_start');
				
				// Only ever try that once, so
				//	unset($GLOBALS['TC_config']['timer_start']);
				
			}
			
			$GLOBALS['TSm_Console_count'] = 0;
			$GLOBALS['TSm_Console_start_microtime'] = $microtime;
			
			$this_page = $_SERVER['PHP_SELF'];
			if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '')
			{
				$this_page .= '&'.$_SERVER['QUERY_STRING'];
			}
			
			$session_bytes = mb_strlen(session_encode(), '8bit');
			
			$this->setDetails([
				                  'page' => $this_page,
				                  'session' => $this->bytesToUnits($session_bytes),
				                  'memory' => static::currentMemoryUsage(),
				                  'GET' => $_GET,
				                  //''
			                  ]);
			
			
		}
		
		if(isset($GLOBALS['TSm_Console_count']))
		{
			$GLOBALS['TSm_Console_count']++;
		}
		else
		{
			$GLOBALS['TSm_Console_count'] = 0;
		}
		
		
		$values['session_id'] = session_id();
		if(is_array($this->message))
		{
			$values['message'] = $this->tableForDetailArray($this->message);
		}
		else
		{
			if($this->message == NULL)
			{
				$this->message = '';
			}
			$values['message'] = $this->message;
			
			
		}
		$values['message_2'] = $this->message_2;
		$values['message_3'] = $this->message_3;
		
		$values['type'] = $this->type;
		$values['is_error'] = $this->is_error ? 1 : 0;
		$values['db_transaction_id'] = $this->db_transaction_id;
		$values['is_skipped'] = $this->is_error ? 1 : 0;
		$values['details'] = $this->details;
		$values['backtrace'] = $this->backtrace();
		$values['class_name'] = is_null($this->class_name) ? '' : $this->class_name;
		$values['microtime'] = $microtime;
		
		if($this->time_spent > 0)
		{
			$values['time_spent'] = $this->time_spent;
		}
		
		if(isset($GLOBALS['TSm_Console_start_microtime']))
		{
			$values['parent_microtime'] = $GLOBALS['TSm_Console_start_microtime'];
		}
		else
		{
			$values['parent_microtime'] = 0;
		}
		
		
		
		
		
		$values['instance_id'] = str_pad($GLOBALS['TSm_Console_count'],4,'0', STR_PAD_LEFT);
		
		
		// BASIC QUERY, not using Tungsten's setup for DB calls
		// That would create infinite recursion
		$query = "INSERT INTO console_items SET ";
		foreach($values as $name => $db_value)
		{
			$query .= $name.' = :'.$name.', ';
		}
		$query .= 'date_added = now()';
		
		// Direct call to database. No need for DB_Prep_Exec
		$db = TCm_Database::connection();
		$statement = $db->prepare($query);
		$statement->execute($values);
		
		
		// If using error table, create an entry for error console messages
		if (TC_getConfig('use_error_table'))
		{
			// Set values
			$values = [
				'message' => $this->message,
				'class_name' => $this->className()
			];
			
			if ($this->isError())
			{
				TSm_ErrorLogItem::createWithValues($values);
			}
			
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'message' => [
					'title'         => 'Message',
					'comment'       => 'The message for the console',
					'type'          => 'text',
					'nullable'      => false,
				],
				'message_2' => [
					'title'         => 'Message 2',
					'comment'       => 'The second message for the console',
					'type'          => 'text',
					'nullable'      => false,
				],
				'message_3' => [
					'title'         => 'Message 3',
					'comment'       => 'The third message for the console',
					'type'          => 'text',
					'nullable'      => false,
				],
				'session_id' => [
					'title'         => 'Session ID',
					'comment'       => 'The ID of the session',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'page' => [
					'title'         => 'Page',
					'comment'       => 'The page URL that was loaded',
					'type'          => 'mediumtext',
					'nullable'      => false,
				],
				'instance_id' => [
					'title'         => 'Instance ID',
					'comment'       => 'Internal numbering for referencing an instance',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'type' => [
					'title'         => 'Type',
					'comment'       => 'The type of console item',
					'type'          => 'smallint(5)',
					'nullable'      => false,
				],
				'microtime' => [
					'title'         => 'Microtime',
					'comment'       => 'The microtime of the call',
					'type'          => 'varchar(16)',
					'nullable'      => false,
				],
				'parent_microtime' => [
					'title'         => 'Parent Microtime',
					'comment'       => 'The microtime of the parent call',
					'type'          => 'varchar(16)',
					'nullable'      => false,
				],
				'db_transaction_id' => [
					'title'         => 'Database transaction ID',
					'comment'       => 'An id for this transaction in the database',
					'type'          => 'varchar(16)',
					'nullable'      => false,
				],
				'is_error' => [
					'title'         => 'Is Error',
					'comment'       => 'Indicates if this is an error',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
				'is_skipped' => [
					'title'         => 'Is Skipped',
					'comment'       => 'Indicates if this is skipped',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
				'class_name' => [
					'title'         => 'Class name',
					'comment'       => 'The name of the model class ',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'details' => [
					'title'         => 'Details',
					'comment'       => 'The long-form details ',
					'type'          => 'text',
					'nullable'      => false,
				],
				'backtrace' => [
					'title'         => 'Backtrace',
					'comment'       => 'The backtrace information for this call ',
					'type'          => 'text',
					'nullable'      => false,
				],
				'time_spent' => [
					'title'         => 'Time spent',
					'comment'       => 'The time spent on this call in microseconds',
					'type'          => 'float(12,7)',
					'nullable'      => true,
				],
				
				
				
				
				// Delete the empty key which creates issues with PDO
				'empty_key' => [
					'delete'         => true,
				],
				'displayed' => [
					'delete'         => true,
				],
			
			
			
			];
		
		
	}
	
	
}

//////////////////////////////////////////////////////
//
// CONSTANTS
//
//////////////////////////////////////////////////////


define('TSm_ConsolePageStart', 99);
define('TSm_ConsolePageRendering', 100);

define('TSm_ConsoleOutput', -1);
define('TSm_ConsoleTimer', 1);
define('TSm_ConsoleMessage', 2);
define('TSm_ConsoleDebug', 3);
define('TSm_ConsoleQuery_Select', 4);
define('TSm_ConsoleQuery_Insert', 5);
define('TSm_ConsoleQuery_Update', 6);
define('TSm_ConsoleQuery_Delete', 12);
define('TSm_ConsoleQuerySkipped', 7);

define('TSm_ConsoleFormValidation', 8);
define('TSm_ConsoleFormValue', 9);
define('TSm_ConsoleFormDatabaseValue', 13);
define('TSm_ConsoleFormSetting', 10);
define('TSm_ConsoleFormWarning', 11);

define('TSm_ConsoleAPICall', 20);

define('TSm_ConsoleUnitTest', 40);

define('TSm_ConsoleWarning', 14);

define('TSm_ConsoleIsError', true);

