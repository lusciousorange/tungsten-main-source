<?php
/**
 * Class TSm_ModuleList
 */
class TSm_ModuleList extends TCm_ModelList
{
	protected $modules_id; // [array] = The list of modules indexed by id
	protected $available_modules = false;
	protected $models = array();
	
	public function __construct($init_model_list = true)
	{
		parent::__construct('TSm_Module',false);
		$modules_installed = true;
		try 
		{
        	$db = TCm_Database::connection();
        	$result = $db->query("SELECT 1 FROM `modules` LIMIT 1");
    	} 
    	catch (Exception $e) 
    	{
        	$modules_installed = false;
    	}
		
		if($modules_installed)
		{	
			$result = $this->DB_Prep_Exec("SELECT *, (folder = 'dashboard') as is_dashboard FROM `modules` ORDER BY is_dashboard DESC, title ASC");
			while($row = $result->fetch())
			{
				$module = TSm_Module::init($row);
				$this->models[$module->folder()] = $module;
				$this->modules_id[$module->id()] = $module;
			}
			
		}	
			
	}
	
	/**
	 * Returns all the modules
	 * @return TSm_Module[]
	 */
	public function modules()
	{	
		return $this->models;
	}
	
	/**
	 * Returns the list of modules that are installed for a given user
	 * @param TMm_User $user
	 * @return TSm_Module[]
	 */
	public function installedModulesForUser($user)
	{	
		$installed_modules = $this->models;
		foreach($this->models as $module)
		{
			if(!$module->installed() || !$module->checkUserPermission($user))
			{
				unset($installed_modules[$module->folder()]);
			}
		}
		
		return $installed_modules;
	}
	
	/**
	 * Returns the list of modules that are installed
	 * @return TSm_Module[]
	 */
	public function installedModules() : array
	{
		$installed_modules = $this->models;
		foreach($this->models as $module)
		{
			if(!$module->installed())
			{
				unset($installed_modules[$module->folder()]);
			}
		}
		
		return $installed_modules;
	}
	
	/**
	 * Returns the list of modules that are visible even when the user is logged out.
	 * @return TSm_Module[]
	 */
	public function modulesVisibleLoggedOut()
	{	
		$logged_out_modules = $this->models;
		foreach($this->models as $module)
		{
			if($module->folder() != 'install' && $module->folder() != 'login')
			{
				unset($logged_out_modules[$module->folder()]);
			}
		}
		
		
		return $logged_out_modules;
	}
	
	/**
	 * Returns if the install process is complete. Checks to see if the system module is installed but the "install" module no longer is.
	 * @return bool
	 */
	public function installComplete()
	{	
		return isset($this->models['system']) && !isset($this->models['install']);
		
	}
	
	/**
	 * 	Returns the module that should be shown the Tungsten logs in
	 * @return bool|mixed|TSm_Module
	 */
	public function loginStartModule()
	{
		$first_module = false;
		foreach($this->modules() as $module)
		{
			if($first_module === false)
			{
				$first_module = $module;
			}
			if($module->isLoginStartPage())
			{
				return $module;
			}
		}
		
		// none set, turn on dashboard as default
		if($this->models['dashboard'])
		{
			$this->models['dashboard']->setAsStartPage();
			return $this->models['dashboard'];
		} 
		
		return $first_module;	
	}
	
	/**
	 * Acquires the list of available modules in the system. This includes those that are installed as well as those
	 * that could be installed but currently aren't.
	 * @return TSm_Module[]
	 */
	public function availableModules()
	{
		if($this->available_modules)
		{
			return $this->available_modules;
		}
		
		$this->available_modules = array();
		$d = dir($_SERVER['DOCUMENT_ROOT'].'/admin/'); // get the directory
		
		// loop through each item in the directory
		while($module_folder = $d->read())
		{
			$module_path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$module_folder;
			// Check for version.php file.
			if(file_exists($module_path.'/config/settings.php'))
			{
				if(@$this->models[$module_folder])
				{
					$module = $this->models[$module_folder]; // TSm_Module instance
				}
				else
				{
					$module = TSm_Module::init($module_folder); // not installed
				}
				
				if($module->installable())
				{
					$this->available_modules[$module_folder] = $module;
				}
				
				
			}
		}
		// sort by keys ie: module names
		ksort($this->available_modules);
		
		// return list
		return $this->available_modules;
	}
	
	/**
	 * Returns if the modules table is installed
	 * @return int
	 */
	public static function verifyModulesInstalled()
	{
		$db = TCm_Database::connection();
		$statement = $db->prepare("SHOW TABLES LIKE `modules`");
		$statement->execute();
		return $statement->rowCount();
	}
	
	/**
	 * Runs the installer on any module that requires an upgrade
	 * @return void
	 */
	public function installModulesRequiringUpgrade() : void
	{
		foreach($this->modules() as $module)
		{
			// Reacquire the module settings, in case they have changed
			$module->setPropertiesFromModuleFolder($module->folder());
			if($module->requiresUpgrade())
			{
				$module->install();
			}
		}
	}
	
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
	
}
?>