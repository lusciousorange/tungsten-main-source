<?php
/**
 * Class TSm_ModuleURLTarget
 *
 * A representation of a url target inside Tungsten
 */
class TSm_ModuleURLTarget extends TCm_Model
{
	/** @var bool|TSm_Module $module  */
	protected $module = false;
	protected $name = false;
	protected $view_name = false;
	protected $model_name = false;
	protected $model_action_method = false;
	protected $model_instance_required = false;
	protected $model_instance_id_required = true;
	protected $model_instance_object_required = false;
	protected $skips_authentication = false;
	protected $pass_id_into_view_directly = false;

	protected $ignore_model_permission;
	
	protected $model = false;
	
	protected $load_models = array();
	protected $parent_model_method_name = false;
	protected $parent_url_target_name = false;
	
	// Stores all the validation calls for this URL target. Each value in the array is another array with two keys
	// for 'model_name' and 'method_name'
	protected $validation_calls = [];
	
	protected $user_group_limitations = false;
	protected $is_valid = null; // NULL, not false. Used for manually setting the validation
	
	protected $pass_values_into_method = false;
	protected $pass_values_into_method_indices = array();
	
	protected $is_view = false;
	protected $parent_url_target = false;
	protected $next_url_target_name = false;
	protected $next_url_module_folder = false;
	protected $is_subsection_parent = false;
	protected $return_type = false;
	protected ?string $next_url_suffix = null;

	protected $title = false;
	protected $title_type = false;
	protected $is_title_method = false;
	protected $title_method = false;

	protected $right_button_icon_code = false;
	protected $right_button_validation_method = false;
	
	protected $is_module_right_button = false;

	protected $opens_in_new_window = false;
	
	/**
	 * @var bool Indicates if this URL target can be loaded in the front end with a matching URL, replaced with
	 * /w/ instead of /admin/
	 */
	protected bool $allow_front_end_routing = false;
	protected ?string $front_end_routing_theme = null;
	protected ?string $front_end_routing_next_model_name = null;
	protected ?string $front_end_routing_next_view_mode = null;
	
	
	// Indicators as primary create/view/edit/delete targets for specific models
	protected $is_primary_model_create_target = false;
	protected $is_primary_model_edit_target = false;
	protected $is_primary_model_view_target = false;
	protected $is_primary_model_delete_target = false;
	
	/** @var bool Indicates if this URL target is part of a model submenu. This will automatically add them if
	 * multiple URL targets exist for that model.
	 */
	protected bool $is_model_submenu = false;
	protected ?int $submenu_order = null;
	
	// Tungsten 9 – RIGHT PANEL
	protected ?array $load_in_right_panel = null;
	
	
	
	public static bool $disable_tungsten_caching = true; // turn this value on in a subclass to ensure it never gets cached. This may lead to more DB queries.
	
	/**
	 * TSm_ModuleURLTarget constructor.
	 *
	 * @param array|int $url_target_name The name of the URL Target within this module
	 */
	public function __construct($url_target_name)
	{
		if($url_target_name == 'referrer' || $url_target_name == 'referer')
		{
			TC_triggerError('The Module URL Target with the name <em>'.$url_target_name.'</em> is reserved. Please choose a different name for your URL target.');
			return false;

		}
		
		parent::__construct($url_target_name);
		$this->name = $url_target_name;
		
		// Handle processing a view name
		// Saves setting a bunch of values we can pull from the class
		if(class_exists($url_target_name) && is_subclass_of($url_target_name,'TCv_View'))
		{
			$this->setViewName($url_target_name);
			$this->setTitle($url_target_name);
			// Get the reflection method
			$reflection_method = new ReflectionMethod($url_target_name, '__construct');
			$reflection_parameters = $reflection_method->getParameters();
			
			// Detect if we have typed parameter
			if(count($reflection_parameters) > 0)
			{
				
				// This assumes a "typed" parameter which we can pull using a reflection
				$parameter_class_name = $reflection_parameters[0]->getType();
				if($parameter_class_name)
				{
					$this->setModelName($parameter_class_name->getName());
					$this->setModelInstanceRequired();
				}
				else // NO TYPE SET
				{
					TC_triggerError('URL Target Error : Implicit view usage in URL targets require all parameters to be typed. <br /><br />Update <em>'.$url_target_name.'::__construct()</em> to have a type for <em>'.$reflection_parameters[0].'</em>');
				}
				
			}
			
			
		}
		
	}
	
	/**
	 * @param TSm_Module $module
	 */
	public function setModule($module)
	{
		$this->module = $module;	
	}

	/**
	 * Returns the module
	 * @return bool|TSm_Module
	 */
	public function module()
	{
		return $this->module;
	}
	
	/**
	 * Returns the name of the URL Target
	 * @return array|bool|int
	 */
	public function name()
	{
		return $this->name;
	}
	
	/**
	 * Adds a prefix to this item, which can be useful for avoiding conflicts
	 * @param string $prefix
	 */
	public function addNamePrefix($prefix)
	{
		$this->name = $prefix.$this->name;
	}
	
	
	/**
	 * Returns the path for the URL Target
	 * @return string
	 */
	public function path()
	{
		return '/admin/'.$this->module()->folder().'/do/'.$this->name().'/';
	}
	
	/**
	 * Returns the path with an explanation
	 * @return string
	 */
	public function pathWithExplanation()
	{
		$path = '/admin/'.$this->module()->folder().'/do/';
		if($this->name() !='')
		{
			$path .= $this->name().'/';
		}
		
		if($this->modelInstanceRequired())
		{
			$path .= '<em style="opacity:0.75;">'.$this->variableNameForID().'</em>';
		}
		
		return $path;
	}


	//////////////////////////////////////////////////////
	//
	// TITLE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the title
	 *
	 * @param bool $show_generic
	 * @return bool
	 */
	public function title($show_generic = false)
	{
		if($show_generic || !$this->is_title_method)
		{
			return $this->title;
		}
		else
		{
			$method_name = str_ireplace('()', '', $this->title_method);
			if(!$this->model)
			{
				$this->model = TC_activeModelWithClassName($this->modelName());
			}
			
			if($this->model)
			{
				return $this->model->$method_name();
			}
			else // FAILURE
			{
				$this->addConsoleError('Model of class '.$this->modelName().' not found with Module URL Target <strong>'.$this->name().'</strong>. Use TSm_ModuleURLTarget::loadModelWithMethod() to define the model of that classname.');
			}	
		}	
		
		return $this->title;
	
	}
	
	/**
	 * Returns the title type
	 * @return string
	 */
	public function titleType()
	{
		return $this->title_type;
	}
	
	/**
	 * Sets the title
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Sets the title as text
	 * @param string $title
	 * @deprecated No longer needed
	 * @see TSm_ModuleURLTarget::setTitle()
	 */
	public function setTitleAsText($title)
	{
		$this->setTitle($title);
	}
	
	/**
	 * Indicates that the title for this URL target should be defined by a method of the model class for the URL target.
	 * @param string $method
	 */
	public function setTitleUsingModelMethod($method)
	{
		$this->is_title_method = true;
		$this->title_method = $method;
	}
	
	/**
	 * Returns if the title method for this url target is actually a class method
	 * @return bool
	 */
	public function hasTitleMethod()
	{
		return $this->is_title_method;
	}
	
	/**
	 * Return a user-friendly name for what the ID variable might look. This is used to make the interface easier to understand when comparing multiple URL targets.
	 * @return string
	 */
	public function variableNameForID()
	{
		$id = '';
		if($model_class_name = $this->modelName())
		{
			$id = $model_class_name::$table_id_column;
		}
		
		if($id == '')
		{
			$id = 'model_id';
		}
		
		return $id;
	}
	
	//////////////////////////////////////////////////////
	//
	// PARENT URL TARGETS
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the number of parent levels for this item
	 * @return int
	 */
	public function numParentLevels()
	{
		if($parent = $this->parentURLTarget())
		{
			$num_levels = 0;
			while($parent)
			{
				$num_levels++;
				$parent = $parent->parentURLTarget();
			}
			return $num_levels;	
		}
		else 
		{
			return 0;
		}
	}
	
	/**
	 * Returns an array of ancenstor URL Target models
	 * @return TSm_ModuleURLTarget[]
	 */
	public function ancestorURLTargets()
	{
		if($parent = $this->parentURLTarget())
		{
			$array = $parent->ancestorURLTargets();
			$array[] = $parent;
			return  $array;
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Returns the parent model URL target
	 * @return TSm_ModuleURLTarget|bool
	 */
	public function parentURLTarget()
	{
		return $this->parent_url_target;
	}
	
	/**
	 * Checks if the provided URL target is the parent URL target
	 * @param TSm_ModuleURLTarget $url_target
	 * @return bool
	 */
	public function isParentURLTarget($url_target)
	{
		TC_assertObjectType($url_target, 'TSm_ModuleURLTarget');
		if(!$this->parent_url_target)
		{
			return false;
		}
		return $this->parent_url_target->name() == $url_target->name();
	}
	
	/**
	 * Sets the name of the parent URL Target. This affects how the module breadcrumbs work as well as what models are
	 * loaded. If a parent module requires a model instance, then you must provide the method name that will provide a model to that parent.
	 *
	 * @param string $url_target_name
	 * @param bool|string $model_method_name
	 */
	public function setParentURLTargetWithName($url_target_name, $model_method_name = false)
	{
		$this->parent_url_target_name = $url_target_name;
		if($model_method_name)
		{
			$this->parent_model_method_name = str_ireplace('()', '', $model_method_name);
		}
		
	
	}
	
	/**
	 * Returns the name of the parent URL target.
	 * @return bool
	 */
	public function parentURLTargetName()
	{
		return $this->parent_url_target_name;
	
	}
	
	/**
	 * Sets what the URL target is that this URL target is a "child" of. This affects how the module breadcrumbs work as
	 * well as what models are loaded. If a parent module requires a model instance, then you must provide the method
	 * name that will provide a model to that parent.
	 *
	 * @param string $url_target
	 * @param bool|string $model_method_name
	 */
	public function setParent($url_target, $model_method_name = false)
	{
		if(!($url_target instanceof TSm_ModuleURLTarget))
		{
			TC_triggerError('URL Target with name <strong>"'.$this->name().'"</strong> is attempting to set a parent that does not exist in the method <strong>setParent()</strong>.'); 
		}
		
		$this->setParentURLTargetWithName($url_target->name(), $model_method_name);
		
		
		if($this->modelInstanceRequired() && $url_target->modelInstanceRequired() && !$this->parent_model_method_name)
		{
			if($this->modelName() != $url_target->modelName())
			{
				TC_triggerError('URL Target with name <strong>'.$this->name().'</strong> requires a second parameter on the method <strong>setParent()</strong> because the parent URL target <em>'.$url_target->name().'</em> requires a model instance. Provide a method name that can be called on <em>'.$this->modelName().'</em> which returns a single instance of <em>'.$url_target->modelName().'</em>.'); 
			}
		}
		
		
		$this->parent_url_target = $url_target;
		$this->parent_url_target_name = $url_target->name();
		
		

	}
	
	/**
	 * Returns the parent model method name
	 * @return bool|string
	 */
	public function parentModelMethodName()
	{
		return $this->parent_model_method_name;
	}
	
	/**
	 * Returns if the URL target has the name or if one of it's parents has the name
	 *
	 * @param string $url_target_name
	 * @return bool
	 */
	public function relatedToName(string $url_target_name) : bool
	{
		if($this->name() == $url_target_name)
		{
			return true;
		}
		
		if($this->parentURLTarget())
		{
			return $this->parentURLTarget()->relatedToName($url_target_name);
		}
	
		return false;

	}
	
	/**
	 * Sets this item as a subsection parent
	 */
	public function setAsSubsectionParent() : void
	{
		$this->is_subsection_parent = true;
	}
	
	/**
	 * Sets this item as a subsection parent
	 */
	public function removeAsSubsectionParent() : void
	{
		$this->is_subsection_parent = false;
	}
	
	
	
	/**
	 * Indicates that this URL target is a subsection parent if subsections are used.
	 *
	 * @return bool
	 */
	public function isSubsectionParent()
	{
		return $this->is_subsection_parent;
	}
	
	//////////////////////////////////////////////////////
	//
	// MODELS
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the model name
	 * @return class-string
	 */
	public function modelName() : string
	{
		return $this->model_name;
	}
	

	/**
	 * Sets the model name
	 * @param string $model_name
	 */
	public function setModelName($model_name)
	{
		$this->model_name = $model_name;
		//$this->is_view = true; // should not enable view
	}
	
	/**
	 * Returns if a model instance is required
	 * @return bool
	 */
	public function modelInstanceRequired()
	{
		return $this->model_instance_required;
	}
	
	/**
	 * Sets this item to require a model instance
	 */
	public function setModelInstanceRequired()
	{
		$this->model_instance_required = true;
		$this->setModelInstanceIDRequired();
	}

	/**
	 * Sets this item to require a model instance
	 */
	public function setPassIDIntoViewDirectly()
	{
		$this->pass_id_into_view_directly = true;
	}

	/**
	 * Returns if the ID should be passed in directly. No models will be generated
	 * @return bool
	 */
	public function passIDIntoViewDirectly()
	{
		return $this->pass_id_into_view_directly;
	}

	/**
	 * Returns if an instance ID is required
	 * @return bool
	 */
	public function modelInstanceIDRequired()
	{
		return $this->model_instance_id_required;
	}
	
	/**
	 * Sets this as a static call, which turns off any of the required instance functionality.
	 */
	public function setAsStaticFunctionCall() : void
	{
		$this->model_instance_id_required = false;
		$this->model_instance_required = false;
		$this->model_instance_object_required = false;
	}
	
	
	/**
	 * Indicates if an ID is required for the instance of the model. If set to false, then the no ID will be passed in
	 * when instantiating the model.
	 *
	 * @param bool $is_required
	 */
	public function setModelInstanceIDRequired($is_required = true)
	{
		$this->model_instance_id_required = $is_required;
		$this->model_instance_required = true;
		$this->model_instance_object_required = false;
	}
	
	/**
	 * Indicates that the class being used for this URL Target also requires a class when being instantiated. This is
	 * more commonly used with AJAX calls or when the loading of the URL Target is less clear. When used, the class_name
	 * must be provided first, then the ID for the class in the URL. Such as /admin/module/do/action/<class_name>/<id>. This function cannot be used in conjuction with the modelInstanceID functions.
	 */
	public function setModelInstanceObjectRequired()
	{
		$this->model_instance_required = true;
		$this->model_instance_object_required = true;
		$this->model_instance_id_required = false;
	}
	
	/**
	 * Returns if a model instance object is required
	 * @return bool
	 */
	public function modelInstanceObjectRequired()
	{
		return $this->model_instance_object_required;
	}
	
	/**
	 * Loads a model with the provided method
	 * @param string $method_name
	 */
	public function loadModelWithMethod($method_name)
	{
		$this->load_models[] = str_ireplace('()', '', $method_name);
	}
	
	/**
	 * The list of load models
	 * @return TCm_Model[]
	 */
	public function loadModelMethods()
	{
		return $this->load_models;
	}
	
	/**
	 * Sets the model that is being used for this URL Target. This is an optional method. If not set, then the class
	 * will always use the "active" model for the model name provided.
	 *
	 * @param TCm_Model $model
	 */
	public function setModel($model)
	{
		TC_assertObjectType($model, $this->model_name);
		
		$this->model = $model;
	}
	
	//////////////////////////////////////////////////////
	//
	// MODEL SUBMENUS
	//
	// Submenus are organized by model name, so a boolean
	// value exists to indicate that this should be one
	// of them. This only has an effect if there is a model
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets this as a model submenu, which will be shown next to other
	 * URL targets that share the same model
	 * @param ?int $order The order in which this should be shown in submenus. Defaults to null. Anything with an
	 * order is shown first, followed by all the ones that are null.
	 * @return void
	 */
	public function setAsModelSubmenu(?int $order = null) : void
	{
		$this->is_model_submenu = true;
		$this->submenu_order = $order;
	}
	
	/**
	 * Sets this as a model submenu, which will be shown next to other
	 * URL targets that share the same model
	 * @return bool
	 */
	public function isModelSubmenu() : bool
	{
		return $this->is_model_submenu;
	}
	/**
	 * Returns the submenu order for this URL target
	 * @return ?int
	 */
	public function submenuOrder() : ?int
	{
		return $this->submenu_order;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PRIMARY CREATE/VIEW/EDIT/DELETE
	//
	// Methods associated with indicating if this target
	// is set as the primary one for creating, viewing, editing
	// or deleting a model in Tungsten
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this URL target is the primary one for creating a model
	 * @return bool
	 */
	public function isPrimaryModelCreateTarget()
	{
		return $this->is_primary_model_create_target;
	}
	
	/**
	 * Sets if this is the primary model create target
	 * @param bool $is_set (Default true)
	 */
	public function setAsPrimaryModelCreateTarget($is_set = true)
	{
		$this->is_primary_model_create_target = $is_set;
	}
	
	/**
	 * Returns if this URL target is the primary one for viewing a model
	 * @return bool
	 */
	public function isPrimaryModelViewTarget()
	{
		return $this->is_primary_model_view_target;
	}
	
	/**
	 * Sets if this is the primary model view target
	 * @param bool $is_set (Default true)
	 */
	public function setAsPrimaryModelViewTarget($is_set = true)
	{
		$this->is_primary_model_view_target = $is_set;
	}
	
	/**
	 * Returns if this URL target is the primary one for editng a model
	 * @return bool
	 */
	public function isPrimaryModelEditTarget()
	{
		return $this->is_primary_model_edit_target;
	}
	
	/**
	 * Sets if this is the primary model edit target
	 * @param bool $is_set (Default true)
	 */
	public function setAsPrimaryModelEditTarget($is_set = true)
	{
		$this->is_primary_model_edit_target = $is_set;
	}
	
	/**
	 * Returns if this URL target is the primary one for deleting a model
	 * @return bool
	 */
	public function isPrimaryModelDeleteTarget()
	{
		return $this->is_primary_model_delete_target;
	}
	
	/**
	 * Sets if this is the primary model view target
	 * @param bool $is_set (Default true)
	 */
	public function setAsPrimaryModelDeleteTarget($is_set = true)
	{
		$this->is_primary_model_delete_target = $is_set;
	}
	
	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION
	//
	// WARNING: This functionality will override Tungsten's
	// authentication to ensure someone is logged in.
	//
	//////////////////////////////////////////////////////

	// ! ----- AUTHENTICATION -----
	//
	
	/**
	 * This method will have this URL target skip Tungsten's authentication for a user. This can pose a security issue
	 * and should only be used if the information to be shown can be safely presented to anyone with the URL. Enabling
	 * this feature will also force the URL Target to ignore any other validations that are set.
	 */
	public function setAsSkipsAuthentication()
	{
		$this->skips_authentication = true;
	}
	
	/**
	 * Returns if this URL Target skips authentication
	 * @return bool
	 */
	public function skipsAuthentication()
	{
		return $this->skips_authentication;
	}
	
	//////////////////////////////////////////////////////
	//
	// VALIDATION
	//
	//////////////////////////////////////////////////////

	
	
	/**
	 * Manually sets the is_valid setting which overrides any other validity check. This is commonly used to set the
	 * validity based on an external boolean value calculated at the time of declaring the URL target in a controller.
	 * @param bool $is_valid
	 * @return void
	 */
	public function setIsValid($is_valid)
	{
		$this->is_valid = $is_valid;
	}
	/**
	 * Indicate a class name and method name that will be called to check if the given URL Target is valid for the provided
	 * model. This rquires a model instance for this URL Target. Settings this value will trigger the method to be called
	 * on the Model instance before performing any functions. The method must return a boolean. Returning false means that
	 * any navigation items won't appear and any views or actions won't be performed. It will not throw an error since
	 * it's used as a verification, not as an error.
	 *
	 * @param string $class_name. The name of the class OR `current_user` to validate against the current user logged in
	 * @param string $method_name
	 */
	public function setValidationClassAndMethod($class_name, $method_name)
	{
		$this->addValidation($class_name, $method_name);
	}
	
	/**
	 * Adds a validation to the URL target. They can have multiple validations.
	 * @param string $model_name The name of the class OR `current_user` which validates against the user logged in,
	 * which will be an instance of `TMm_User`
	 * @param string $method_name The name of the method to be called on the class
	 * @return void
	 */
	public function addValidation($model_name, $method_name)
	{
		$method_name = str_ireplace('()', '', $method_name);
		$this->validation_calls[] = [
			'model_name' => $model_name,
			'method_name' => $method_name];
	}
	
	/**
	 * Returns all the validation calls for this URL target
	 * @return array|mixed
	 */
	public function validationCalls()
	{
		return $this->validation_calls;
	}
	
	/**
	 * Returns the validation method name
	 * @return bool|string
	 * @deprecated This method will only acquire the first item in the list of validation calls and might not be
	 * entirely accurate for complex URL targets
	 */
	public function validationMethodName()
	{
		return $this->validation_calls[0]['method_name'];
	}
	
	/**
	 * Returns the validation model name
	 * @return bool
	 * @deprecated This method will only acquire the first item in the list of validation calls and might not be
	 * entirely accurate for complex URL targets
	 */
	public function validationModelName()
	{
		return $this->validation_calls[0]['model_name'];
	}
	
	/**
	 * Returns the validation model
	 * @param string $model_name The name of the model class we're looking to validate
	 * @return bool|TCm_Model|TCu_Item
	 */
	public function validationModel($model_name)
	{
		// Check for the special case current user value
		if($model_name == 'current_user')
		{
			return TC_currentUser();
		}
		
		
		// A model is set and the validation is trying to use a model name that matches was we have
		// In that case, we should use this model instead of the active one
		if($this->model instanceof $model_name )
		{
			return $this->model;
		}
	
		
		// Look for modules which are instantiated but not active models
		if(is_subclass_of($model_name, 'TSm_Module'))
		{
			// Get an instance of the module
			$model = $model_name::init();
			
			if($model instanceof TSm_Module)
			{
				return $model;
			}
		}
		
		// No Model Yet, Try to load the active model
		$model = TC_activeModelWithClassName($model_name);

		// No model found yet, see if one of the active models is a parent of what is being requested
		if($model)
		{
			return $model;

		}
		else
		{
			if(class_exists($model_name))
			{
				foreach(TC_activeModels() as $active_model)
				{
					if($active_model instanceof $model_name)
					{
						return $active_model;
					}
				}
			}
		}

		return false;

	}
	
	/**
	 * A method to analyze the validation
	 *
	 * @param bool $analyze
	 * @param string $failure_message
	 */
	public function validationAnalysis($analyze, $failure_message)
	{
		if($analyze)
		{
			$this->addConsoleDebug('validateAccess/analysis : '.$this->module()->folder().'/do/'.$this->name().' : '.$failure_message);	
		}
		
	}
	
	/**
	 * Sets this URL target to ignore permissions.
	 *
	 * WARNING: This bypasses the validation around userCanView()
	 * @return void
	 */
	public function ignoreModelPermission()
	{
		$this->ignore_model_permission = true;
	}
	
	/**
	 * Determines if this URL Target can be shown given everything known about it.
	 *
	 * @param bool $analyze Indicate if the analysis should be triggered
	 * @return bool
	 */
	public function validateAccess($analyze = false)
	{
		$is_valid = true;
		
		// Detect front-end routing and reject if they are trying to use it on one that hasn't been set
		if(substr($_SERVER['REQUEST_URI'],0,3) == '/w/' && !$this->allowsFrontEndRouting())
		{
			$this->validationAnalysis($analyze, 'front-end routing not enabled');
			return false;
		}
		
		if($this->ignore_model_permission)
		{
			return true;
		}
		
		// Quick Exit option – ensure there's access before showing
		// Skips if we're not authenticating
		if(!$this->skipsAuthentication() && !$this->userHasAccess())
		{
			$this->validationAnalysis($analyze, 'userHasAccess = false');
			return false;
		} 
		
		// Check for manually set validity
		if($this->is_valid !== null)
		{
			if(!$this->is_valid)
			{
				$this->validationAnalysis($analyze, 'is_valid = false');
				return false;
			}
		}
		
		// If we require a model, we should check the userCanView for that model
		if($this->modelInstanceRequired() && $this->modelInstanceIDRequired())
		{
			if($this->model)
			{
				$model = $this->model;
			}
			else
			{
				$model = TC_activeModelWithClassName($this->modelName());
			}
			
			if($model instanceof TCm_Model)
			{
				if(!$model->userCanView())
				{
					$this->validationAnalysis($analyze, 'userCanView = false');
					$this->addConsoleWarning('userCanView() is false  for  '.$this->modelName());
					return false;
				}
			}
		}
		
		// Loop through all the validation calls
		foreach($this->validation_calls as $validation_values)
		{
			$validation_method_name = $validation_values['method_name'];
			$model_name = $validation_values['model_name'];
			
			// uses validation, requires a model
			$model = $this->validationModel($model_name);
			// If we've ever found a model, return the validation,otherwise false
			
			// Handle static method callss
			if(substr($validation_method_name, 0, 2) == '::')
			{
				$validation_method_name = str_ireplace('::', '', $validation_method_name);
				$is_valid = $model_name::$validation_method_name();
				
				if(!$is_valid)
				{
					$this->validationAnalysis($analyze, 'class validation method ' . $model_name . '::' . $validation_method_name . ' failed.');
				}
				
				
			}
			elseif($model)
			{
				$is_valid = $model->$validation_method_name();
				if(!$is_valid)
				{
					$this->validationAnalysis($analyze, 'validation method ' . get_class($model) . '->' . $validation_method_name . ' failed.');
				}
				
			}
			else // requires validation, so it rquires a model. If it can't be found, don't show. failsafe.
			{
				$is_valid = false;
				$this->validationAnalysis($analyze, 'requires validation, model of class "' . $model_name . '" not set.');
				
			}
			
			
		}
		return $is_valid;
	}
	
	//////////////////////////////////////////////////////
	//
	// ACTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the model action method
	 * @return bool|string
	 */
	public function modelActionMethod()
	{
		return $this->model_action_method;
	}
	
	/**
	 * Sets the model to perform an action which will be a method name for that model. This requires a model instance in
	 * order to work properly. If the method returns a string, then that will be response notification when the action is
	 * completed. Model actions are commonly tied to actions such as "delete", "create", "cancel", "undo" or anything else
	 * that is processed and returned to the browser.
	 *
	 * @param string $method_name
	 */
	public function setModelActionMethod($method_name)
	{
		$this->model_action_method = $method_name;
	}
	
	/**
	 * Returns the method type for passing values in
	 * @return bool
	 */
	public function passValuesIntoActionMethodType()
	{
		return $this->pass_values_into_method;
	}
	
	/**
	 * Passes the URL values into the action method that is set, in the order they are provided. This means that if the
	 * target path is /admin/module/do/action/<id>/<id_2>, then <id> and <id_2> will be passed into the model action method.
	 * This setting has no effect if the model action method isn't set.
	 *
	 * @deprecated Passthrough method for backwards compatibility.
	 * @see TSm_ModuleURLTarget::setPassValuesIntoActionMethodType() if possible.
	 */
	public function setPassValuesURLIntoActionMethod()
	{
		$this->pass_values_into_method = 'url';
		$this->pass_values_into_method_indices = 'all';
		
	}
	
	/**
	 * A method to indicate that values are passed into the action method. The values can be passed via the URL values
	 * (/admin/module/do/action/<id_1>/<id_2>), or via $_POST or $_GET. The function accepts any number of arguments
	 * after the first one, which must be the names of the indexes to be passed. When using URL values, the indices must
	 * be numbers starting at 1, indicting which url values should be passed. When using GET or POST, you can skip adding
	 * any additional arguments and all the properties will be passed in. This should only be used if the order in which
	 * they are passed does not matter.
	 *
	 * @param string $type 'url', 'post' or 'get'. Using post or get requires additional parameters
	 */
	public function setPassValuesIntoActionMethodType($type)
	{
		$type = strtolower($type);
		if($type == 'url' || $type == 'post' || $type = 'get')
		{
			$this->pass_values_into_method = $type;	
			$index_names = func_get_args();
			array_shift($index_names); // remove teh first one, which is $type
			
			if(sizeof($index_names) > 0)
			{
				$this->pass_values_into_method_indices = $index_names;		
			}
			else
			{
				$this->pass_values_into_method_indices = 'all';		
			}
			
			
		}
	}
	
	/**
	 * Returns the values that are passed into the method
	 * @return array
	 */
	public function passValueIntoActionMethodIndices()
	{
		return $this->pass_values_into_method_indices;
	}

	//////////////////////////////////////////////////////
	//
	// RETURN TYPES
	//
	// Functions that set the return type and will handle
	// different types of return values to force a type
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets this URL target to return JSON. If a view is returned, it will render the HTML and return a json with the
	 * html. If an array is returned, it will convert it to json.
	 */
	public function setReturnAsJSON()
	{
		$this->return_type = 'json';

		// Force the next target to be null, this will output to screen
		// Manually call to avoid disabled is_view
		$this->next_url_target_name = NULL;
		
		
		//$has_view_set = $this->is_view;
		//$this->setNextURLTarget(null);

	}
	
	/**
	 * Sets this URL target to return whatever is returned from the method call.
	 */
	public function setReturnAsMethodValue()
	{
		$this->return_type = 'method_value';
		
		// Force the next target to be null, this will output to screen
		// Manually call to avoid disabled is_view
		$this->next_url_target_name = NULL;
		$this->is_view = false;
		
		
	}

	/**
	 * Sets this URL target to return the view html broken up as HTML, JS and CSS.
	 *
	 * This method only works on url targets that have a view set.  The returned values will be JSON.
	 *
	 */
	public function setReturnViewAsJSONParts()
	{
		$this->return_type = 'json';

		// Force the next target to be null, this will output to screen
		$this->setNextURLTarget(NULL);
		$this->is_view = true; // must be re-enabled to work as expected

	}

	/**
	 * Sets this URL target to return the view html as one piece
	 *
	 */
	public function setReturnViewHTMLOnly()
	{
		$this->return_type = 'view_html';

		// Force the next target to be null, this will output to screen
		$this->setNextURLTarget(NULL);
		$this->is_view = true; // must be re-enabled to work as expected

	}

	/**
	 * Returns the "return type" which is false if not set and should be ignored. Other values are "json".
	 * @return bool|string
	 */
	public function returnType()
	{
		return $this->return_type;
	}
	
	public function setReturnType($return_type)
	{
		$this->return_type = $return_type;
	}
	
	//////////////////////////////////////////////////////
	//
	// URLS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the path to this URL Target. This function will respect all the necessary requirements and if a model
	 * instance is required, it will use the active model unless another model is provided via the setModel() method.
	 *
	 * @return string
	 */
	public function url()
	{
		if(!TC_isTungstenView() && $this->allowsFrontEndRouting())
		{
			$path = '/w/'.$this->module()->folder().'/';
			if($this->name() != '')
			{
				$path .= $this->name().'/';
			}
		}
		else
		{
			$path = '/admin/'.$this->module()->folder().'/';
			if($this->name() != '')
			{
				$path .= 'do/'.$this->name().'/';
			}
		}
		
		if($this->modelInstanceRequired() && $this->modelInstanceIDRequired())
		{
			if($this->model)
			{
				$model = $this->model;
			}
			else
			{
				$model = TC_activeModelWithClassName($this->modelName());	
				
			}
			
			if($model)
			{
				$path .= $model->id();	
			}
			else // required a model but not found – ERROR
			{
				$message = 'Model (<em>'.$this->modelName().'</em>) not found when loading Module URL Target <em>'.$this->name().'</em>. Attempting to generate a URL similar to <em>'.$path.'</em>';
				

/*
				// ERROR MESSAGE CAN'T BE SPECIFIC TO THE CURRENT URL
				// COMMENTED OUT BUT INCLUDED FOR LONG-TERM NEEDS
				// JUST IN CASE
				if($this->current_url_target->modelName() != $url_target->modelName())
				{
				
					if($this->current_url_target->parentModelMethodName())
					{
						$message .= 'Check that <em>'.$this->current_url_target->modelName().'->'.$this->current_url_target->parentModelMethodName().'()</em> returns a valid instance of <em>'.$url_target->modelName().'</em>. This is what the URL Target is set to use.';
					}
					else
					{
						$message .= '<br /><br/>The loaded URL Target <em>'.$this->current_url_target->name().'</em> requires a <em>'.$this->current_url_target->modelName().'</em>, but must also reference a <em>'.$url_target->modelName().'</em> when trying to load the URL Target <em>'.$url_target->name().'</em>.  
						
						<br /><br />You must provide a method name when setting the URL Target\'s parent. That method will be called on <em>'.$this->current_url_target->modelName().'</em> and must return a single <em>'.$url_target->modelName().'</em>.';
					}
				}
*/
				TC_triggerError($message);
			}
		}
		
		return $path;	
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// VIEWS
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the view name
	 * @param string $view_name
	 */
	public function setViewName($view_name)
	{
		$this->view_name = $view_name;
		$this->is_view = true;
	}
	
	/**
	 * Returns the view name
	 * @return bool|class-string<TCv_View>
	 */
	public function viewName()
	{
		return $this->view_name;
	}
	
	/**
	 * Returns if there is a view for this URL Target
	 * @return bool
	 */

	public function isView()
	{
		return $this->is_view;
	}
	

	//////////////////////////////////////////////////////
	//
	// RIGHT BUTTON
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns if this is setup to be a right button
	 * @return bool
	 */
	public function isRightButton()
	{
		return $this->is_module_right_button;
	}
	
	/**
	 * Indicates that this URL target should appear as a module right button when appropriate.
	 * @param bool|string  $code The font awesome icon name that will appear next to the title
	 * @param bool|string $validation_method This feature only works on URL targets which have have a required model instance.
	 * In those cases, the validation method is called on that model which should return a boolean indicating if the
	 * button should appear. The called function can't have any parameters. If the endoint does not have a required model
	 * instance or the function doesn't exist, then an error will occur.
	 */
	public function setAsRightButton($code = false, $validation_method = false)
	{
		$this->is_module_right_button = true;
		$this->right_button_icon_code = $code;
		$this->right_button_validation_method = false;
		
		if($validation_method)
		{
			if(!$this->modelInstanceRequired() || !$this->modelName())
			{
				TC_triggerError('URL target with name <em>'.$this->name().'</em> cannot have setAsRightButton() called with the second parameter of a validation method without a model instance being set and that instance being required');
			}
			
			if(!method_exists($this->modelName(), $validation_method))
			{
				TC_triggerError('URL target with name <em>'.$this->name().'</em> attempting to set the validation method for setAsRightButton() as <em>'.$validation_method.'</em>. The method name does not exist in the class <em>'.$this->modelName().'</em>.');
			}
			
			$this->right_button_validation_method = $validation_method;
		}
	}
	
	/**
	 * Indicates that this URL target should appear as a module right button when appropriate.
	 *
	 * @return bool|string
	 */
	public function rightButtonIconCode()
	{
		return $this->right_button_icon_code;
	}
	
	/**
	 * Returns the icon code for this URL target
	 *
	 * @return bool|string
	 */
	public function iconCode()
	{
		return $this->right_button_icon_code;
	}
	
	/**
	 * Sets the icon code for this URL target
	 * @param string|bool $icon_code
	 */
	public function setIconCode($icon_code)
	{
		// Trim the common presets since we don't want to bias the input
		// Doesn't trim `fab` because brand prefixes are required
		$icon_code = trim($icon_code);
		$icon_code = str_replace(['fas ','far ','fal'],'', $icon_code);
		$this->right_button_icon_code = $icon_code;
	}
	
	
	/**
	 * Returns the validation method for the right button
	 *
	 * @return bool|string
	 */
	public function rightButtonValidationMethod()
	{
		return $this->right_button_validation_method;
	}
	
	/**
	 * Turns off any settings that would have enable this as a right button
	 */
	public function disableRightButton()
	{
		$this->is_module_right_button = false;
	}

	//////////////////////////////////////////////////////
	//
	// PERFORMING ACTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Indicates that buttons with this URL should open in a new window
	 */
	public function setOpenInNewWindow()
	{
		$this->opens_in_new_window = true;
	}

	/**
	 * Returns
	 * @return bool
	 */
	public function openInNewWindow()
	{
		return $this->opens_in_new_window;
	}



	/**
	 * Sets the next URL Target to be loaded when this action is done. This is will disable the "view" mode and turn this
	 * URL Target into an "action" that calls a method then redirects. An option also exists to enter the URL for a
	 * specific page instead of another URL target.
	 * @param string $url_target_name The name of the next URL target. A value of NULL will stop any redirect from
	 * happening. If you provide a string that start with `/`, it indicates that you want to load a specific URL on
	 * the site and completing the action will go to that URL.
	 * @param bool|string $module_folder (Optional) Default false. The name of the module folder
	 */
	public function setNextURLTarget($url_target_name, $module_folder = false)
	{
		if($url_target_name instanceof TSm_ModuleURLTarget)
		{
			$this->next_url_target_name = $url_target_name->name();
		}
		else
		{
			$this->next_url_target_name = $url_target_name;
		}
		
		$this->next_url_module_folder = $module_folder;
		
		// Only bother if we're setting it to null. Otherwise we might be setting a value which is processing a view
		// but that might need to be returned as JSON
		//if($url_target_name !== null)
		//{
			$this->is_view = false;
		//}
		
	}
	
	
	/**
	 * Returns the next URL target name
	 * @return bool|string
	 */
	public function nextURLTargetName()
	{
		return $this->next_url_target_name;
	}
	
	/**
	 * Returns the next url module folder
	 * @return bool|string
	 */
	public function nextURLTargetModuleFolder()
	{
		return $this->next_url_module_folder;
	}
	
	/**
	 * Sets a suffix for the next URL. Often used to append values such as hashes
	 * @param string|null $suffix
	 * @return void
	 */
	public function setNextURLSuffix(?string $suffix) : void
	{
		$this->next_url_suffix = $suffix;
	}
	
	/**
	 * Returns the optional URL suffix for the next url
	 * @return string|null
	 */
	public function nextURLSuffix() : ?string
	{
		return $this->next_url_suffix;
	}
	
	//////////////////////////////////////////////////////
	//
	// USER GROUP RESTRICTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns if this has any user group limitations. Otherwise everyone can load this URL Target assuming they have access to the module.
	 *
	 * @return bool
	 */
	public function hasUserGroupLimitations()
	{
		return is_array($this->user_group_limitations);
	}
	
	/**
	 * Returns the list of user groups that can see this URL Target
	 *
	 * @return array|bool
	 */
	public function userGroupLimitations()
	{
		if(is_array($this->user_group_limitations))
		{
			return $this->user_group_limitations;
		}
		
		return false;
	}
	
	/**
	 * Adds a user group limitation for this URL Target.
	 *
	 * @param TMm_UserGroup|int|string  $user_group
	 */
	public function addUserGroupLimitations($user_group)
	{
		if($this->user_group_limitations === false)
		{
			$this->user_group_limitations = array();
		}
		
		if(is_int($user_group))
		{
			$user_group = TMm_UserGroup::init( $user_group);
		}
		elseif(is_string($user_group))
		{
			$user_group = TMm_UserGroup::init( $user_group);
		}
		
		if($user_group instanceof TMm_UserGroup)
		{
			$this->user_group_limitations[$user_group->id()] = $user_group;	
		}
		
		
	}
	
	/**
	 * Checks if the provided user can access this URL Target. If this URL Target has no User Group Restrictions or if
	 * the user is a part of at least one group which has access to this URL Target, then it will return true.
	 * @param bool|TMm_User $user (Optional) Default false. The user being tested against. If left blank, it will test against the ucrrently logged in user.
	 * @return bool
	 */
	public function userHasAccess($user = false)
	{
		
		// doesn't use limitatiosn, most common, return true
		if(!$this->hasUserGroupLimitations())
		{
			return true;
		}
		if($user === false)
		{
			$user = TC_currentUser();
		}
		
		TC_assertObjectType($user, 'TMm_User');
		
		if($this->hasUserGroupLimitations())
		{
			foreach($this->userGroupLimitations() as $user_group)
			{
				if($user->inGroup($user_group))
				{
					return true;
				}
			}
		}
		else
		{
			return true;
		}
		
		// Failsafe, has limitations but no match was found, or anything else that might lead to this function never returning true.
		return false;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// FRONT-END ROUTING
	// Functionality related to allowing URL targets to
	// load in the front-end theme. This must be explicitly
	// set in the URL target.
	//
	// URLs can then replace /admin/ with /w/ to
	// enable the routing, and use the public theme.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Turns on front-end routing for this URL target. Allows it to be loaded using `w` instead of `admin` and
	 * will load using the website's theme.
	 * @return void
	 */
	public function enableFrontEndRouting(?string $theme_name = null) : void
	{
		$this->allow_front_end_routing = true;
		$this->front_end_routing_theme = $theme_name;
		
	}
	
	/**
	 * Returns if front-end routing is allowed for this URL target
	 * @return bool
	 */
	public function allowsFrontEndRouting() : bool
	{
		return $this->allow_front_end_routing;
	}
	
	/**
	 * Returns the theme name for front-end routing that should load.
	 * @return string
	 */
	public function frontEndRoutingThemeName() : string
	{
		if(is_string($this->front_end_routing_theme))
		{
			return $this->front_end_routing_theme;
		}
		
		return trim(TC_getModuleConfig('pages', 'website_theme'));
		
	}
	
	/**
	 * Sets the "next url" for when we are using front-end routing on a URL target that is processing rather than
	 * rendering. In that scenario, we don't want to redirect to somewhere in the admin, but instead somewhere in the
	 * page navigation.
	 *
	 * In order to be precise, we tie into teh model structure that we use to define certain pages as specific types
	 * for certain models. This uses the $public_page_model_modes which are set in `TMm_PagesMenuItem`.
	 *
	 * For example, setting it to `TMm_NewsPost` and `view` will try and find the active news post based on what we
	 * have, and then try and find the page designated as the view page for a news post. Then it redirects it there.
	 *
	 * This also requires that the model name is active for the the URL target either as the primary or additonal
	 * model loaded.
	 * @param ?class-string<TCm_Model> $model_name The name of the model that we want to use for
	 * @param ?string $view_mode
	 * @return void
	 */
	public function setFrontEndRouting_NextModelAndViewMode(?string $model_name, ?string $view_mode) : void
	{
		$this->front_end_routing_next_model_name = $model_name;
		$this->front_end_routing_next_view_mode = $view_mode;
		
	}
	
	public function frontEndRouting_NextModel() : ?string
	{
		return $this->front_end_routing_next_model_name;
	}
	
	public function frontEndRouting_NextViewMode() : ?string
	{
		return $this->front_end_routing_next_view_mode;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TUNGSTEN 9 RIGHT PANELS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets this URL target to load it's content as a right panel of the parent. This means it won't appear in
	 * navigation but only when viewing the parent.
	 *
	 * This loading can be based on the parent, OR on a matching model
	 * @param string $icon_name The font-awesome icon name for this panel
	 * @param string $visibility Indicates how it decides to appear. The default is `parent` which means the right
	 * panel must have the same parent URL target as the one being viewed. If a value of `model` is provided, then it
	 * checks based having the same model requirements. So if they both require a model of the same type, then it
	 * appears.
	 */
	public function setLoadInRightPanel(string $icon_name, string $visibility = 'parent') : void
	{
		$this->load_in_right_panel = ['icon_name' => $icon_name, 'visibility' => $visibility];
		
	}
	
	/**
	 * Returns if this item loads in the right panel
	 * @return bool
	 */
	public function loadsInRightPanel() : bool
	{
		return !is_null($this->load_in_right_panel);
	}
	
	/**
	 * Indicates if this url target loads in the right panel by referencing a matching parent URL target.
	 * @return bool
	 */
	public function loadsInRightPanelUsingParent() : bool
	{
		if($this->loadsInRightPanel())
		{
			return $this->load_in_right_panel['visibility'] == 'parent';
		}
		return false;
	}
	
	/**
	 * Returns the icon for the right panel icon
	 * @return ?string
	 */
	public function rightPanelIcon()  : ?string
	{
		if($this->loadsInRightPanel())
		{
			return $this->load_in_right_panel['icon_name'];
		}
		return null;
	}
	
	/**
	 * Returns the array of URL targets that are right-panels for this URL target
	 * @return TSm_ModuleURLTarget[]
	 */
	public function rightPanelURLTargets() : array
	{
		$right_panel_url_targets = [];
		
		$controller = $this->module()->controller();
		foreach($controller->URLTargets() as $right_url_target)
		{
			// Check if this URL target loads in the right panel. We might need it.
			if($right_url_target->loadsInRightPanel())
			{
				// Panels based on the parent, so we match
				if($right_url_target->loadsInRightPanelUsingParent())
				{
					if($right_url_target->parentURLTargetName() == $this->name())
					{
						$right_panel_url_targets[] = $right_url_target;
					}
				}
				
				// Panels based on a model match
				else
				{
					if($right_url_target->modelName() == $this->modelName()
						&& $right_url_target->modelInstanceRequired() && $this->modelInstanceRequired())
					{
						$right_panel_url_targets[] = $right_url_target;
					}
				}
			}
		}
		
		return $right_panel_url_targets;
	}
	
}
?>