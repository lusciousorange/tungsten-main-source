<?php
/**
 * Class TSm_Plugin
 *
 * A top-level codebase that can be loaded and added to the website to perform functionality that is generalized
 * across the website.
 */
class TSm_Plugin extends TCm_Model
{
	protected $plugin_id;
	protected $title;
	
	protected $module_id = false;
	protected $enable_public = false; 
	protected $enable_admin = false; 
	protected $enable_live_site = false; 
	protected $enable_beta_site = false;
	
	protected int $display_order = 0;
	
	protected $variable_1, $variable_2, $variable_3, $variable_4, $variable_5, $variable_6, $variable_7, $variable_8;
	
	/**
	 * @var bool|string|TCv_View
	 * The class name that is loaded, which is string that represents a specific TCv_View
	 */
	protected $class_name = false; 
	protected $is_installed = true;
	protected $wait_to_load_on_rendering = false;
	
	public static $table_id_column = 'plugin_id';
	public static $table_name = 'plugins';
	public static $model_title = 'Plugin'; 
	public static $primary_table_sort = 'display_order ASC';
	
	/**
	 * TSm_Plugin constructor.
	 * @param string|array|int $plugin_id
	 */
	public function __construct($plugin_id)
	{
		parent::__construct($plugin_id, false);
		
		if(is_array($plugin_id) || ctype_digit($plugin_id) )
		{
			$this->setPropertiesFromTable();
		}
		else // found a class name
		{
			$mem_key = 'TSm_Plugin'.$plugin_id;
			$plugin_row = TC_Memcached::get($mem_key);
			if(!is_array($plugin_row))
			{
				$query = "SELECT * FROM `plugins` WHERE class_name = :class_name LIMIT 1";
				$result = $this->DB_Prep_Exec($query, array('class_name' => $plugin_id));
				$plugin_row = $result->fetch();
				
				// Avoid trying to get them if they aren't set
				// Set it to an empty arrow so we know we have the data
				if($plugin_row === false)
				{
					$plugin_row = [];
				}
				
				TC_Memcached::set($mem_key, $plugin_row); // save to cache
			}
			
			
			$this->id = $plugin_row['plugin_id'];
			$this->convertArrayToProperties($plugin_row);
		}
	}
	
	/**
	 * Returns if this plugin waits for rendering
	 *
	 * @return bool
	 */
	public function waitsForRendering()
	{
		return $this->wait_to_load_on_rendering;
	}
	
	/**
	 * Returns if this plugin is installed
	 * @return bool
	 */
	public function isInstalled()
	{
		return $this->is_installed;
	}
	
	/**
	 * Returns the title for this plugin
	 * @return string
	 */
	public function title()
	{
		return $this->viewClass()->pluginTitle();
	}
	
	/**
	 * Returns the view class for this plugin
	 * @return bool|TCv_View
	 */
	public function viewClass()
	{
		return ($this->class_name)::init( $this);

	}
	
	/**
	 * Returns the view class name
	 * @return bool|string
	 */
	public function className()
	{
		return $this->class_name;
	}
	
	/**
	 * Returns if this plugin is enabled on the public facing website
	 * @return bool
	 */
	public function enabledOnPublic()
	{
		return $this->enable_public;
	}
	
	/**
	 * Toggles if this plugin is enabled on the public facing website
	 */
	public function toggleEnabledOnPublic()
	{
		$this->updateWithValues(['enable_public' => !$this->enable_public]);
	}
	
	/**
	 * Returns if this plugin is enabled on the admin website
	 * @return bool
	 */
	public function enabledOnAdmin()
	{
		return $this->enable_admin;
	}
	
	
	/**
	 * Toggles if this plugin is enabled on the admin website
	 */
	public function toggleEnabledOnAdmin()
	{
		$this->updateWithValues(['enable_admin' => !$this->enable_admin]);
	}
	
	/**
	 * Returns if this plugin is enabled on live (non-staging) websites
	 * @return bool
	 */
	public function enabledOnLiveSite()
	{
		return $this->enable_live_site;
	}
	
	
	/**
	 * Toggles if this plugin is enabled on live (non-staging) websites
	 */
	public function toggleEnabledOnLiveSite()
	{
		$this->updateWithValues(['enable_live_site' => !$this->enable_live_site]);
	}
	
	/**
	 * Returns if this plugin is enabled on beta/staging websites
	 * @return bool
	 */
	public function enabledOnBetaSite()
	{
		return $this->enable_beta_site;
	}
	
	/**
	 * Toggles if this plugin is enabled on beta/staging websites
	 */
	public function toggleEnabledOnBetaSite()
	{
		$this->updateWithValues(['enable_beta_site' => !$this->enable_beta_site]);
	}
	
	/**
	 * Toggles if this plugin waits for rendering
	 */
	
	public function toggleWaitForRendering()
	{
		$this->updateWithValues(['wait_to_load_on_rendering' => !$this->wait_to_load_on_rendering]);
	}
		
	/**
	 * Returns a variable value for a given variable number
	 * @param int $num
	 * @return false
	 */
	public function variableValueForNum(int $num)
	{
		$variable_name = 'variable_'.$num;
		if(isset($this->$variable_name))
		{
			return $this->$variable_name;
		}
		return false;
	}
	
	/**
	 * Determines if this plugin should be shown given a set of beta and admin settings.
	 * @param bool $is_beta
	 * @param bool $is_admin
	 * @return bool
	 */
	public function determineIfShown(bool $is_beta, bool $is_admin)
	{
		
		$is_live = !$is_beta;
		$is_public = !$is_admin;
		
		// beta disabled
		if($is_beta && !$this->enabledOnBetaSite())
		{
			return false;	
		}
		
		// live disabled
		if($is_live && !$this->enabledOnLiveSite())
		{
			return false;	
		}
		
		// admin disabled
		if($is_admin && !$this->enabledOnAdmin())
		{
			return false;	
		}
		
		// public disabled
		if($is_public && !$this->enabledOnPublic())
		{
			return false;	
		}
		
		return true;	
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		$schema =  parent::schema() + [
				
				'class_name' => [
					'title'         => 'Class Name',
					'comment'       => 'The name of the class for this plugin',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'index'         => 'class_name'
				],
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title displayed for this module',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'enable_public' => [
					'title'         => 'Public Enabled',
					'comment'       => 'Indicates if enabled on the public side of websites',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'enable_admin' => [
					'title'         => 'Admin Enabled',
					'comment'       => 'Indicates if enabled on the admin',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'enable_live_site' => [
					'title'         => 'Live Enabled',
					'comment'       => 'Indicates if enabled on live websites',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				
				],
				'enable_beta_site' => [
					'title'         => 'Beta Enabled',
					'comment'       => 'Indicates if enabled on beta websites',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				
				],
				'wait_to_load_on_rendering' => [
					'title'         => 'Wait to load on rendering',
					'comment'       => 'Indicates if the plugin should wait to load until the site has rendered',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'display_order' => [
					'title'         => 'Display Order',
					'comment'       => 'The order in which they are displayed, which also is the order they are loaded',
					'type'          => 'smallint(5) unsigned',
					'nullable'      => false,
				],
			];
		
		for($i = 1; $i <= 8; $i++)
		{
			$schema['variable_'.$i] = [
				'title'         => 'Variable '.$i,
				'comment'       => '',
				'type'          => 'varchar(255)',
				'nullable'      => false,
			];
		}
		
		return $schema;

	}

	
	
}
?>