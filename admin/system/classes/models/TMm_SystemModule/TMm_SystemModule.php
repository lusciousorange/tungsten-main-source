<?php
class TMm_SystemModule extends TSm_Module
{
	/**
	 * TSm_Module constructor.
	 * @param array|int $module_id
	 */
	public function __construct($module_id = 'system')
	{
		parent::__construct($module_id);
		
	}
	
	/**
	 * Extend to deal with Tungsten 8 never wanting this to appear
	 * @return bool
	 */
	public function showInMenu(): bool
	{
		// Skip all the logic. If we're in Tungsten 9 show it, otherwise don't
		return TC_getConfig('use_tungsten_9');
	}
}