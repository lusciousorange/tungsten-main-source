<?php
class TSm_ModuleSettings extends TCm_Model
{
	/** @var TSm_Module $module */
	protected $module;

	protected ?array $variables = null;
	protected ?array $variable_values = null;


	/**
	 * TSm_Module constructor.
	 * @param TSm_Module}string $module
	 */
	public function __construct($module)
	{
		if(is_string($module))
		{
			$module = TSm_Module::init($module);
		}
		$this->module = $module;
		parent::__construct('module_settings_'.$module->folder());

		if(is_null($this->variables))
		{
			$this->variables = [];
			$this->variable_values = [];

			// Variables are memcached
			$rows = TC_Memcached::get($this->cacheKeyName());
			if(!is_array($rows))
			{
				$query = "SELECT * FROM `module_variables` WHERE module_name = :module_name";
				$result = $this->DB_Prep_Exec($query, array('module_name' => $this->module->folder()));
				$rows = $result->fetchAll();
				TC_Memcached::set($this->cacheKeyName(), $rows);
			}

			foreach($rows as $row)
			{
					$this->variables[$row['variable_name']] = $row['value'];
					$this->variable_values[$row['variable_name']] = $row;
			}
		}

	}


	/**
	 * Returns the module for these settings.
	 * @return TSm_Module
	 */
	public function module()
	{
		return $this->module;
	}

	/**
	 * Returns the variables for this module
	 * @return array|bool
	 */
	public function variables()
	{
		return $this->variables;
	}

	/**
	 * Returns the last date in which the variables were modified.
	 * @param string $variable_name
	 * @return string
	 */
	public function variableDateModified($variable_name)
	{
		return $this->variable_values[$variable_name]['date_added'];
	}

	/**
	 * Returns the variable for the name given
	 * @param string $variable_name
	 * @return string
	 */
	public function variable($variable_name)
	{
		if(TC_getConfig('use_localization'))
		{
			$localization = TMm_LocalizationModule::init();
			$loaded_language = $localization->loadedLanguage();

			if($loaded_language != 'en')
			{
				$variable_name_non_en = $variable_name.'_'.$loaded_language;
				if($this->variableExists($variable_name_non_en))
				{
					// only returns here if using localization AND non-English AND variable with suffix exists
					return $this->variables[$variable_name_non_en];
				}
			}
		}

		// Check if the variable is set in the form values
		if(isset($this->variables[$variable_name]))
		{
			return $this->variables[$variable_name];
		}

		// Check if the method is defined in the module settings
		elseif(method_exists($this, $variable_name))
		{
			return call_user_func(array($this, $variable_name));
		}


		return false;
	}

	/**
	 * Returns the variable for the name given
	 * @param string $name
	 * @return bool
	 */
	public function variableExists($name)
	{
		if(isset($this->variables[$name]))
		{
			return true;
		}

		if(method_exists($this, $name))
		{
			return true;
		}

		return false;
	}
	/**
	 * Updates the variables for this module
	 * @param array $values
	 */
	public function updateVariables($values)
	{
		foreach($values as $variable_name => $new_value)
		{
			// VALUE EXISTS, UPDATE
			if(isset($this->variables[$variable_name]))
			{
				$query = "UPDATE module_variables SET value = :value
					WHERE module_name = :module_name AND variable_name = :variable_name LIMIT 1";
			}
			
			// DOESN'T EXIST, create
			else
			{
				$query = "INSERT INTO module_variables
				SET module_name = :module_name, variable_name = :variable_name, value = :value, date_added = now()";
			}
			
			$new_value_array = [
				'module_name' => $this->module->folder(),
				'variable_name' => $variable_name,
				'value' => $new_value ];
			
			// UPDATE THE VALUE
			$this->DB_Prep_Exec($query, $new_value_array);
			
			// UPDATE LOCAL VALUES
			$this->variables[$variable_name] = $new_value;
			$this->variable_values[$variable_name] = $new_value_array;
		}

		// Clear cache
		TC_Memcached::delete($this->cacheKeyName());


	}



	/**
	 * Updates the variables for this module
	 * @param string $variable_name
	 * @param string $value
	 */
	public function updateVariableWithName($variable_name, $value)
	{
		$values = [
			$variable_name => $value
		];
		
		// Call the update variables function which only updates the values that are provided.
		$this->updateVariables($values);

		// Clear cache
		TC_Memcached::delete($this->cacheKeyName());

	}


	/**
	 * Updates the variables for this module
	 * @param array $values
	 */
	public function installVariables($values)
	{
		$query = "INSERT INTO module_variables SET 
			module_name = :module_name, variable_name = :variable_name, value = :value, date_added = now()";
		$statement = $this->DB_Prep($query);
		$db_values = ['module_name' => $this->module->folder(), 'variable_name' => '', 'value' => ''];
		foreach($values as $id => $value)
		{
			if(!$this->variableExists($id))
			{
				$db_values['variable_name'] = $id;
				$db_values['value'] = $value;
				$this->DB_Exec($statement, $db_values);
			}
		}

		// Clear cache
		TC_Memcached::delete($this->cacheKeyName());

	}
	
	/**
	 * Deletes a variable with a specific name for this module
	 * @param string $variable_name
	 * @return void
	 */
	public function deleteVariable(string $variable_name)
	{
		$query = "DELETE FROM module_variables WHERE module_name = :module_name AND variable_name = :variable_name";
		$db_values = ['module_name' => $this->module->folder(), 'variable_name' => $variable_name];
		$this->DB_Prep_Exec($query, $db_values);

		// Clear cache
		TC_Memcached::delete($this->cacheKeyName());

	}
	
	
}
?>