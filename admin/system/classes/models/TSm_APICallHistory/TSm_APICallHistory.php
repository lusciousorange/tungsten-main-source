<?php
class TSm_APICallHistory extends TCm_Model
{
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'call_id'; // [string] = The id column for this class
	public static $table_name = 'api_calls'; // [string] = The table for this class
	public static $model_title = 'API call';
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////

	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'http_method' => [
					'title'         => 'HTTP Method',
					'comment'       => 'The method used such as GET, POST , etc',
					'type'          => 'varchar(10)',
					'nullable'      => false,
				],
				'module_name' => [
					'title'         => 'Module Name',
					'comment'       => 'The name of the module',
					'type'          => 'varchar(20)',
					'nullable'      => false,
				],
				'error_code' => [
					'title'         => 'Error Code',
					'comment'       => 'The error code that was called',
					'type'          => 'varchar(50)',
					'nullable'      => false,
				],
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The id of the user who made the call ',
					'type'          => 'TMm_User',
					'nullable'      => false,
				],
				'api_key' => [
					'title'         => 'API Key',
					'comment'       => 'The api key that was used',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'address' => [
					'title'         => 'Address',
					'comment'       => 'The IP address where the call originated from',
					'type'          => 'varchar(30)',
					'nullable'      => false,
				],
				'path' => [
					'title'         => 'URL Path',
					'comment'       => 'The path that was called',
					'type'          => 'mediumtext',
					'nullable'      => false,
				],
				'source' => [
					'title'         => 'Source',
					'comment'       => 'Optional value passed in the headers to indicate a source. Sent via HTTP Header `api-source`',
					'type'          => 'varchar(32)',
					'nullable'      => true,
				],
			
			];
	}
	
}