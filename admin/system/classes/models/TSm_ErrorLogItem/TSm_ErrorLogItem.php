<?php
class TSm_ErrorLogItem extends TCm_Model
{
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'error_id'; // [string] = The id column for this class
	public static $table_name = 'errors'; // [string] = The table for this class
	public static $model_title = 'Error';
	
	// Switch to enable/disable the saving of backtrace in the DB. We do not want to have this on 100% of the time,
	// because the backtrace is huge. It can be turned on for enhanced debugging if the basic details are insufficient
	public static $save_backtrace = false;
	
	
	/**
	 * Extends the functionality to set some extra values we are getting from server or session.
	 * Removes the need to include those values that don't change every place that calls this function
	 *
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model (Optional) Default true. Indicates if the newly created object should be
	 * instantiated and returned. This is a heuristic option to speed up complex calls where the model isn't needed
	 * @return bool|TCm_Model
	 */
	public static function createWithValues($values, $bind_param_values = array(), $return_new_model = true)
	{
		// Set basic values that can be set without any other stuff
		// This means we can create an error log item with just a message and the rest is set here
		$values['page'] = $_SERVER['REQUEST_URI'];
		
		if (isset($_SERVER['HTTP_REFERER']))
		{
			$values['referrer'] = $_SERVER['HTTP_REFERER'];
		}
		
		// Check for user, set if exists. Don't instantiate user. Might be the cause of the error
		if (isset($_SESSION["tungsten_user_id"]))
		{
			$values['user_id'] = $_SESSION["tungsten_user_id"];
		}
		
		$backtrace = debug_backtrace();
		if (static::$save_backtrace === true)
		{
			// Save a serialized version of the backtrace, on the chance that it may be useful
			$values['backtrace'] = serialize(debug_backtrace());
		}
	
		// Run the query manually to avoid having it appear in the console. It doubles up with existing messages.
		$query = "INSERT INTO errors SET ";
		foreach($values as $name => $value)
		{
			$query .= ' '. $name .' = :'.$name.',';
		}
		
		$query .= ' date_added = now()';
		
		$db = TCm_Database::connection();
		$statement = $db->prepare($query);
		
		// Bind values
		foreach($values as $name => $value)
		{
			$statement->bindValue($name, $value);
		}
		
		try {
			$statement->execute();
		}
		catch(Exception $e)
		{
			// Do nothing, we didn't successfully track the error and that shouldn't trigger OTHER errors
		}
		
		return false;

		
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'message' => [
					'title'         => 'Message',
					'comment'       => 'The error message',
					'type'          => 'text',
					'nullable'      => true,
				],
				'page' => [
					'title'         => 'Page',
					'comment'       => 'The page URL that was loaded',
					'type'          => 'mediumtext',
					'nullable'      => true,
				],
				'referrer' => [
					'title'         => 'Referrer',
					'comment'       => 'The referer URL that led to current page',
					'type'          => 'mediumtext',
					'nullable'      => true,
				],
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The ID of the user, if any',
					'type'          => 'int(10)',
					'nullable'      => true,
				],
				'class_name' => [
					'title'         => 'Class name',
					'comment'       => 'The name of the class that causes the error ',
					'type'          => 'varchar(64)',
					'nullable'      => true,
				],
				'is_warning' => [
					'title'         => 'Class name',
					'comment'       => 'The name of the class that causes the error ',
					'type'          => 'tinyint(1) DEFAULT 0',
					'nullable'      => true,
				],
				'backtrace' => [
					'title'         => 'Backtrace',
					'comment'       => 'The php debug backtrace',
					'type'          => 'text',
					'nullable'      => true,
				],
			];
		
		
	}
	
}