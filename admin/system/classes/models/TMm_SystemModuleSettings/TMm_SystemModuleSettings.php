<?php
class TMm_SystemModuleSettings extends TSm_ModuleSettings
{
	/**
	 * TSm_Module constructor.
	 * @param TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

	}

	/**
	 * @return bool
	 */
	public function noUserLoggedIn()
	{
		$website = TC_website();
		return !$website->isLoggedIn();
	}

	/**
	 * @return bool
	 */
	public function userLoggedIn()
	{
		$website = TC_website();
		return $website->isLoggedIn();
	}




}
?>