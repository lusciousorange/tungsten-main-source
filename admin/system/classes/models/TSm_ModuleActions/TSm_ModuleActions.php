<?php

class TSm_ModuleActions extends TCm_Model
{
	protected $module = false;
	protected $actions = array();
	protected $module_folder_path = false;
	
	private static $instance = NULL;
	
	/**
	 * @param $module
	 */
	public function __construct($module)
	{
		parent::__construct('module_actions_'.$module->folder());
		
		$this->module = $module;
		$this->module_folder_path = '/admin/'.$this->module->folder().'/';
		$model_title = $this->module->modelTitle();
		
		
				
	}
	
	
	/**
	 * Returns the action with the given code
	 * @param string $code
	 * @return mixed
	 */
	public function actionWithCode($code)
	{
		return $this->actions[$code];
	}
	
	
	/**
	 * @return mixed|TSm_ModuleActions|null
	 */
	public static function initialize()
	{
	    if (!self::$instance)
		{	
			$module = TC_website()->currentModule();
			
			$class_name = $module->modelName().'_ModuleActions';
			if(!class_exists($class_name))
			{
				$class_name = 'TSm_ModuleActions';
			}
			self::$instance = new $class_name($module);
	
		}
		return self::$instance;
    }
	
}
?>