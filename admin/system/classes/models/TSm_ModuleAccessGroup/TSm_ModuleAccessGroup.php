<?php

/**
 * Class TSm_ModuleAccessGroup
 */
class TSm_ModuleAccessGroup extends TCm_Model
{
	protected $group_id, $module_id;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id';
	public static $table_name = 'module_access_groups';
	public static $model_title = 'Module access group';
	public static $primary_table_sort = 'date_added DESC';
	
	/**
	 * TMm_UserGroupMatch constructor.
	 * @param $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		$this->setPropertiesFromTable();
	}
	
	
	/**
	 * Returns the user group
	 * @return bool|TMm_UserGroup
	 */
	public function group()
	{
		return TMm_UserGroup::init($this->group_id);
	}
	
	
	/**
	 * Returns the user
	 * @return bool|TSm_Module
	 */
	public function module()
	{
		return TSm_Module::init($this->module_id);
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'module_id' => [
					'title'         => 'Module ID',
					'comment'       => 'The id of the module ',
					'type'          => 'TSm_Module',
					'nullable'      => false,
					
					// If the module is deleted, set this value to null but we want to keep the entry
					'foreign_key'   => [
						'model_name'    => 'TSm_Module',
						'delete'        => 'CASCADE'
					],
				],
				
				'group_id' => [
					'title'         => 'Group ID',
					'comment'       => 'The id of the user group ',
					'type'          => 'TMm_UserGroup',
					'nullable'      => false,
					
					// If the group is deleted, set this value to null but we want to keep the entry
					'foreign_key'   => [
						'model_name'    => 'TMm_UserGroup',
						'delete'        => 'CASCADE'
					],
				],
			
			];
		
		
	}
	
	
	
}