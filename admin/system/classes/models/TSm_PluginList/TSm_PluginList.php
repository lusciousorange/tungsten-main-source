<?php
/**
 * Class TSm_PluginList
 */
class TSm_PluginList extends TCm_ModelList
{
	protected $plugin_names_found = array();
	
	/** @var bool|TSv_Plugin[]  */
	protected $all_plugins = false;

	/**
	 * TSm_PluginList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = false)
	{
		parent::__construct('TSm_Plugin', $init_model_list);

		$mem_key = 'plugins_table_check';
		$test_value = TC_Memcached::get($mem_key);

		if(!$test_value)
		{
			// Deal with no table existing for plugins
			try
			{
				$db = TCm_Database::connection();
				$db->query("SELECT 1 FROM plugins LIMIT 1");
				TC_Memcached::set($mem_key, 1);
			}
			catch (Exception $e)
			{
				$this->models = array();
			}
		}
	}

	/**
	 * Returns all the models for this plugin list
	 * @param bool $subset_name
	 * @return array|TSm_Plugin[]|TCm_Model[]|bool
	 */
	public function models($subset_name = false)
	{
		if($this->models === false)
		{
			$models = parent::models();
			foreach($models as $id => $plugin_model)
			{
				/** @var TSm_Plugin $plugin_model */
				unset($models[$id]);
				$models[$plugin_model->className()] = $plugin_model;
				//	unset($this->plugin_names_found[$plugin_model->className()]);
			}

			$this->models = $models;
		}

		return $this->models;
	}


	/**
	 * Returns all the plugins, regardless of if they are installed
	 * @return array|bool|TCm_Model[]|TSm_Plugin[]
	 */
	public function allPlugins()
	{
		if($this->all_plugins === false)
		{

			// start with our models
			$this->all_plugins = $this->models();
			$this->processAvailablePlugins();

			// manually init plugins without an id that should be represented

			foreach($this->plugin_names_found as $view_class_name => $path)
			{
				// Avoid any unusable ones
				if(!$view_class_name::isUsable())
				{
					continue;
				}
				
				if(!isset($this->all_plugins[$view_class_name]))
				{
					$init_values = [
						'class_name' => $view_class_name,
						'id' => $view_class_name,
						'is_installed' => 0
					];
					$model = new TSm_Plugin($init_values);

					$this->all_plugins[$view_class_name] = $model;
				}

			}
		}
		return $this->all_plugins;
	}

	/**
	 * Gets the list of all the available plugins
	 */
	protected function processAvailablePlugins() : void
	{
		// the root of the admin
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		//$d = dir($admin_root);
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			// Process the pages content folder for any content views that should be loaded
			$class_folder_path = $admin_root.$module_folder.'/classes/views/';
			$this->processModuleFolder($class_folder_path, $module_folder);
		}

	}

	/**
	 * processes a module folder for the list of plugins
	 * @param string $class_folder_path
	 * @param string $module_folder
	 */
	protected function processModuleFolder(string $class_folder_path, string $module_folder) : void
	{
		// check if it is a directory and skip . and ..
		if(is_dir($class_folder_path) && $module_folder != '.' && $module_folder != '..')
		{
			// if here then we are looking at /admin/<module>/
			$plugin_folder = dir($class_folder_path);
			while($plugin_file = $plugin_folder->read())
			{
				if($plugin_file != '.' && $plugin_file != '..') // ignore the two return links
				{
					// found a file or folder, only save it if it is a class
					$view_class_name = str_ireplace('.php','',$plugin_file);
					if(class_exists($view_class_name))
					{
						//$interfaces = class_implements($view_class_name);
						
						//$plugins_view = ($view_class_name)::init(false);
						
						
						if(is_subclass_of($view_class_name, 'TSv_Plugin'))// $view_class_name instanceof TSv_Plugin)//($plugins_view,'TMv_PagesContent'))
						{
								if(!isset($this->plugin_names_found[$view_class_name]))
								{
									//$this->plugins_views[$module_folder][$view_class_name] = $view_class_name;
									$this->plugin_names_found[$view_class_name] = $class_folder_path;
								}
								
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Installs the plugin provided
	 * @param $plugin_name
	 */
	public function installPlugin($plugin_name) : void
	{
		$this->models(); // initialize models
		$this->allPlugins();
		if(isset($this->all_plugins[$plugin_name]))//$plugin_model->isInstalled())
		{
			if(!$this->all_plugins[$plugin_name]->isInstalled())
			{
				$installed = 0;
				foreach($this->models() as $model)
				{
					if($model->isInstalled())
					{
						$installed++;	
					}	
				}
				
				$values = [
					'class_name' => $plugin_name,
					'display_order' => $installed+1
				];
				
				/** @var TSv_Plugin $view */
				$view = ($plugin_name)::init(false);
				foreach($view->defaultInstallValues() as $name => $value)
				{
					$values[$name] = $value;
				}
				
				TSm_Plugin::createWithValues($values);
				
			}
		}
	
	}
	
	/**
	 * Uninstalls the plugin
	 * @param string $plugin_name
	 */
	public function uninstallPlugin(string $plugin_name) : void
	{
		$plugin = TSm_Plugin::init($plugin_name);
		if($plugin)
		{
			$plugin->delete();
		}
	}
	
	/**
	 * Returns the list of installed plugins
	 * @return TSm_Plugin[]
	 */
	public function installedPlugins() : array
	{
		$installed = array();
		foreach($this->models() as $class_name =>  $plugin_model)
		{
			if($plugin_model->isInstalled())
			{
				$installed[$class_name] = $plugin_model;	
			}
		}	
		
		return $installed;
	}
	
	/**
	 * Returns if the indicated plugin is installed
	 * @param string $plugin_name
	 * @return bool
	 */
	public function pluginIsInstalled(string $plugin_name) : bool
	{
		$this->models(); // initialize models
		if(isset($this->models[$plugin_name]))
		{
			return $this->models[$plugin_name]->isInstalled();
		}
		return false;
	}
	
	
	/**
	 * Processes a list of values to rearranges the list according to those values
	 *
	 * @param array $values
	 */
	public function processRearrangedList($values) : void
	{
		$query = "UPDATE plugins SET display_order = :display_order WHERE plugin_id = :plugin_id";
		try
		{
			$this->DB()->beginTransaction();
			$statement = $this->DB_Prep($query);
			for($display_order = 1; $display_order <= $values['num_items']; $display_order++)
			{
				$plugin_id = $values['item_'.$display_order];
				$db_values = array('display_order' => $display_order, 'plugin_id' => $plugin_id);
				$statement->execute($db_values);
			}
			$this->DB()->commit();
		}
		catch (Exception $e) 
		{
			$this->DB()->rollBack($e->getMessage());
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
	
	
	
	
}

?>