<?php

/**
 * This model represents help text for a given component. This is displayed on the site as needed for different
 * components in Tungsten and possibly elsewhere.
 */
class TSm_ComponentHelp extends TCm_Model
{
	protected string $help_html = '';
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'help_id';
	public static $table_name = 'help';
	public static $model_title = 'Help';
	
	/**
	 * Returns the HTML for the component help
	 * @return string
	 */
	public function helpHTML() : string
	{
		return $this->help_html;
	}
	
	/**
	 * Tries to find a component help for the
	 * @param class-string<TCv_View> $view_class
	 * @return TSm_ComponentHelp|null
	 */
	public static function findHelpForViewName(string $view_class) : ?TSm_ComponentHelp
	{
		if(trim($view_class) == '' || !class_exists($view_class))
		{
			return null;
		}
		
		//	TC_addToConsole('Looking for '.$view_class);
		$view_class = $view_class::baseClass();
		
		$query = "SELECT * FROM `help` WHERE view_class = :view_class LIMIT 1";
		//$db = TCm_Database::connection();
		$result = static::DB_RunQuery($query,['view_class' => $view_class]);
		if($row = $result->fetch())
		{
			return TSm_ComponentHelp::init($row);
		}
		
		// No help found, but default exists, so install it
		elseif(is_string($view_class::defaultHelpHTML()))
		{
			return TSm_ComponentHelp::createWithValues([    'view_class' => $view_class,
				                                            'help_html' => $view_class::defaultHelpHTML()]);
		}
		return null;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Extends the schema for the custom form to add additional fields
	 * @return array[]
	 */
	public static function schema() : array
	{
		return parent::schema() + [
				'view_class' => [
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'help_html' => [
					'type' 			=> 'text',
					'nullable'      => false,
					
				],
			];
	}
}