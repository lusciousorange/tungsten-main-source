<?php

/**
 * Class TSm_CronCall
 * Represents a single cron call to the system
 */
class TSm_CronCall extends TCm_Model
{
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'call_id';
	public static $table_name = 'cron_calls';
	public static $model_title = 'Cron call';
	
	/**
	 * Updates the status for this cron call, marking it as complete, which is the "done" status.
	 * @return void
	 */
	public function trackCronCompleted() : void
	{
		$this->setStatus('done');
	}
	
	/**
	 * Updates the status for this cron call, marking it as skipped.
	 * @return void
	 */
	public function trackAsSkipped() : void
	{
		$this->setStatus('skipped');
	}
	
	/**
	 * Updates the status for this cron call. Any string value will work, however the most common are "done"
	 * @param string $status
	 * @return void
	 */
	public function setStatus(string $status) : void
	{
		$this->updateDatabaseValue('status', $status);
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////

	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'class_name' => [
					'title'         => 'Class Name',
					'comment'       => 'The class name that was called',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'status' => [
					'title'         => 'Status',
					'comment'       => 'The status of the cron call',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'file' => [
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
			
			
			];
	}
}