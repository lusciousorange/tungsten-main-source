<?php

/**
 * A security class used to compare the current working repo with what should be there using git status and then
 * reverts it if anything is found to ensure any compromises are removed. This is normally run via a cron job.
 */
class TSm_GITScanning extends TCm_Model implements TSi_Cronable
{
	/**
	 * A method that is implemented on your class which performs the necessary function that will be run on each cron
	 * call.
	 */
	public static function runCron()
	{
		
		if(TC_getConfig('use_git_scanning_email') != false)
		{
			$status = shell_exec('git status');
			
			// SOMETHING FOUND
			if(strpos($status,'working tree clean') === false)
			{
				$file_contents = '';
				// Grab the content from the files
				$lines = explode("\n",$status);
				foreach($lines as $filename)
				{
					$filename = trim(str_replace('modified:','',$filename));
					if(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$filename))
					{
						$file_contents .= "<br />\n**".$filename."**<br />\n--------------------------<br />\n";
						$contents = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$filename);
						$file_contents .= htmlentities($contents);
					}
				}
				
				// Remove any modifications, does not delete added files
				exec('git checkout .');
				
				$email = new TCv_Email(false);
				$email->addRecipient(TC_getConfig('use_git_scanning_email'));
				$email->setSubject('Security: '.$_SERVER['HTTP_HOST']);
				$email->addText(nl2br($status));
				
				$email->addText("<br /><br />");
				$email->addText($file_contents);
				$email->send();
				
			}
			
		}
		
	}
}