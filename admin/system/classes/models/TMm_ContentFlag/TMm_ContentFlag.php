<?php

/**
 * Class TMm_ContentFlag
 *
 * A content flag is an indicator that an item in the system has something about it that is incorrect or invalid.
 * This class is designed to be "low overhead" since hundreds of them could be instantiated in theory. The class is
 * also not associated with a table in the database and is a pure data structure for tracking values.
 */
class TMm_ContentFlag
{
	use TCt_FactoryInit;
	
	protected $type = '';
	protected $message = '';
	
	/**
	 * TMm_ContentFlag constructor.
	 *
	 * Creates a flag with a specific type and message which are used to help organize and explain the flags
	 * @param string $type The type should be a brief but consistent code for each type of content flag. These can be
	 * self-determined.
	 * @param string $message The message for this flag which can be specific to the field or value being flagged. It
	 * is possible for a model to have multiple flags of the same type with different messages. For example if three
	 * fields are empty.
	 */
	public function __construct(string $type, string $message)
	{
		$this->type = $type;
		$this->message = ucfirst($message);
	}
	
	/**
	 * Returns the type for this content flag
	 * @return string
	 */
	public function type()
	{
		return $this->type;
	}
	
	/**
	 * Returns the message for this content flag
	 * @return string
	 */
	public function message()
	{
		return $this->message;
	}
}