<?php
	/* This is a script used in Tungsten to find all the possible paths 
	 * for a class and check each of those to see if it can find the 
	 * class. Since they are all in different folders, it needs to find
	 * the list of folders and check each of those.
	 *
	 */
	function tungsten_class_autoload($class_name)
	{
		
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		
		// CONTENT LAYOUTS
		if(substr($class_name,0,17) == 'TMv_ContentLayout')
		{
			$plugin_file = $admin_root.'/pages/layouts/'.$class_name.'/'.$class_name.'.php';
			if(file_exists($plugin_file))
			{
				include_once($plugin_file); // grab the plugin file
				return;
			}
		}
		
		includeModuleClassWithName($class_name);
		
		
	}
	
	// Register the function using SPL
	spl_autoload_register('tungsten_class_autoload');


	
	function includeModuleClassWithName($class_name, $allow_tests = false)
	{
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		
		// the root of the admin
		$d = dir($admin_root);
		
		// Only bother for classes that start with TC
		$type = substr($class_name,2,1);
		$is_view = $type == 'v';
		$is_model = $type == 'm';
		$is_controller = $type == 'c';
		$is_trait = $type == 't';
		$is_interface = $type == 'i';
		$is_utility = $type == 'u';
		
		$class_name_folder = $class_name;
		
		// If it's a test file, we override the class_name folder
		// TMm_UserTest will look in /TMm_User/TMm_UserTest.php
		if($allow_tests && str_ends_with($class_name,'Test'))
		{
			$class_name_folder = substr($class_name,0, -4);
		}
		
		// loop through all the folders in /admin/
		while($tungsten_folder = $d->read())
		{
			// check if it is a directory and skip . and ..
			if(is_dir($admin_root.$tungsten_folder) && $tungsten_folder != '.' && $tungsten_folder != '..')
			{
				// list of possible locations within this tungsten folder
				$plugin_file_options = array();
				
				if($is_view)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/views/'.$class_name_folder.'/'.$class_name.'.php';
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/themes/'.$class_name_folder.'/'.$class_name.'.php';
				}
				elseif($is_model)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/models/'.$class_name_folder.'/'.$class_name.'.php';
				}
				elseif($is_controller)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/controllers/'.$class_name_folder.'/'.$class_name.'.php';
				}
				elseif($is_trait)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/traits/'.$class_name_folder.'/'.$class_name.'.php';
				}
				elseif($is_interface)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/interfaces/'.$class_name_folder.'/'.$class_name.'.php';
				}
				elseif($is_utility)
				{
					$plugin_file_options[] = $admin_root.$tungsten_folder.'/classes/utilities/'.$class_name_folder.'/'.$class_name.'.php';
				}
				
				// Loop through possible options, return if it's found
				foreach($plugin_file_options as $plugin_file_path)
				{
					if(is_file($plugin_file_path))
					{
						include_once($plugin_file_path); // grab the plugin file
						return;
					}
				}
				
				
			}
		}
	}
	
	/**
	 * Parses the folders in the directory to find composer.json files and to grab the relevant information from them
	 */
	function parseModuleComposerFiles()
	{
		// PARSE composer.json files found in module > models folder
		// This only needs to happen once
		$composer_paths = array();
		$admin_root = $_SERVER['DOCUMENT_ROOT'] . '/admin/';

		// the root of the admin
		$d = dir($admin_root);

		// loop through all the folders in /admin/
		while ($tungsten_folder = $d->read())
		{
			// check if it is a directory and skip . and ..
			$composer_folder = $admin_root.$tungsten_folder.'/classes/composer/';
			if(is_dir($composer_folder) && $tungsten_folder != '.' && $tungsten_folder != '..')
			{
				$composer_dir = dir($composer_folder);
				while ($composer_class_folder = $composer_dir->read())
				{
					$composer_file = $composer_folder.$composer_class_folder.'/composer.json';
					if(file_exists($composer_file))
					{
						$json = file_get_contents($composer_file);
						$data = json_decode($json, TRUE);

						if (isset($data['autoload']['files']))
						{
							foreach ($data['autoload']['files'] as $index => $filename_to_include)
							{
								$file_path = $composer_folder . $composer_class_folder . '/' . $filename_to_include;
								include_once($file_path);
							}
						}
						if (isset($data['autoload']["psr-4"]))
						{
							foreach ($data['autoload']["psr-4"] as $namespace => $autoload_path)
							{
								//print '<br />ADDING : '.$namespace .' = '. $autoload_path;
								$file_path = $composer_folder . $composer_class_folder . '/' . $autoload_path;
								$composer_paths[$namespace] = $file_path;

							}

						}
					}
				}

			}

		}

		return $composer_paths;

	}

	// Call the parser
	parseModuleComposerFiles();


	function tungstenComposerClassAutoLoad($class_name)
	{
		// Auto-Skip anything that's a Tungsten class
		if (substr($class_name, 0, 2) == 'TM' || substr($class_name, 0, 2) == 'TC')
		{
			return;
		}

		unset($_SESSION['composer_paths']);
		// Only do it once, must happen after session_start(), so it goes here
		if(!isset($_SESSION['composer_paths']))
		{
			$_SESSION['composer_paths'] = parseModuleComposerFiles();
		}

		$class_namespace = explode('\\',$class_name);
		$class_name = array_pop($class_namespace);
		$class_namespace = implode('\\', $class_namespace).'\\';

		$path_prefix = '';
		while(!isset($_SESSION['composer_paths'][$class_namespace]) && $class_namespace != '\\')
		{
			$class_namespace = rtrim($class_namespace,"\\"); // remove the trailing \
			$parts = explode('\\',$class_namespace);
			$last_part = array_pop($parts);
			$class_namespace = implode('\\', $parts).'\\';

			$path_prefix =  $last_part .'/'.$path_prefix;

		}

		$path = @$_SESSION['composer_paths'][$class_namespace];
		if($path_prefix != '')
		{
			$path .= '/'.$path_prefix;
		}


		$potential_file = $path.$class_name.'.php';
		if(is_file($potential_file))
		{
			include_once($potential_file); // grab the plugin file
			return;

		}


	}

	// Register the function using SPL
	spl_autoload_register('tungstenComposerClassAutoLoad');


	/**
	 * A function that handles fatal errors, and uses their details to create a TSm_ErrorLogItem
	 * This is going to be used as the shutdown function.
	 */
	function fatalErrorHandler()
	{
		if (TC_getConfig('use_error_table'))
		{
			// Get last error
			$error = error_get_last();
			
			if((is_array($error)))
			{
				$error_types = [E_ERROR, E_COMPILE_ERROR, E_CORE_ERROR, E_RECOVERABLE_ERROR, E_USER_ERROR];
				
				// If last error is a fatal/shutdown or warning error, save it
				if(in_array($error['type'], $error_types))
				{
					TSm_ErrorLogItem::createWithValues(['message' => $error['message']]);
				}
			}
		}
		
	}
	
	// Register function for fatal errors
	register_shutdown_function( "fatalErrorHandler");


?>