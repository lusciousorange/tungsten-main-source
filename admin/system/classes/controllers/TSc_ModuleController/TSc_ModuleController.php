<?php
 /**
 * Class TSc_ModuleController
 */
class TSc_ModuleController extends TCc_Controller
{
	protected ?TSm_Module $module = null;
	protected $model = false;
	protected array $models = array();
	protected array $views = array();
	protected bool $views_generated = false;
	protected array $primary_url_targets_for_models = array();
	protected bool $is_valid = true;
	protected ?string $validation_error_message = null;
	
	protected $id_1;
	// URL T
	protected string $url_target_name = '';
	
	/** @var TSm_ModuleURLTarget[] $module_url_targets */
	protected array $module_url_targets = array() ;
	
	protected ?TSm_ModuleURLTarget $current_url_target = null;
	
	/** @var bool|TSm_ModuleURLTarget $current_url_target_view  */
	protected $current_url_target_view = false;
	protected int $num_values_from_url = 0;
	
	protected array $submenu_groupings = array();
	
	protected bool $use_subsections = false;
	protected $subsection_bar = false;
	protected $subsection_menu;
	protected $primary_id = false;
	protected $id_2 = false;
	protected $id_3 = false;
	
	protected array $url_targets_using_model = [];
	
	/**
	 * TSc_ModuleController constructor.
	 * @param int|string|TSm_Module $module The id for this module. If a string is provided, it indicates that a non-installed module is being instatiated. In this case it will
	 */
	public function __construct($module)
	{
		parent::__construct('module_controller');
		
		if(is_string($module))
		{
			$module = TSm_Module::init($module);
		}
		
		$this->module = $module;
		
		if($this->module)
		{
			
			// Always have a user available, if one every doesn't exist, load this. \
			// Any other loading of a TMm_User will overwrite this one
/*
			if(!TC_activeModelWithClassName('TMm_User'))
			{
				TC_saveActiveModel(TC_currentUser());
			}
*/
		
		
			// Initialize the different lists
			$this->subsection_menu = new TSv_ModuleNavigation_SectionBar();
			if(isset($_GET['url_target']))
			{
					$this->url_target_name = htmlspecialchars($_GET['url_target']);
			}
			
			
			$this->parseIDsFromURL();

			if($this->module->hasVariables()) // handle first in case it's grouped. Also can be overridden
			{
				$this->addSettingsURLTarget();	
			}
			
			$this->defineURLTargets(); // handle any url targets (views, model methods, etc)
			//$this->detectModelWith301Redirects();
			
			// Identify if Module folder matches actual folder being processed. Avoid generating URL targets for wrong module
			// This will only work if the 3rd folder is "index.php" which means we're laoding a url target
			if($this->urlSection(2) == $module->folder() && $this->urlSection(3) == 'index.php')
			{
				$this->current_url_target = null;
				if(isset($this->module_url_targets[$this->url_target_name]))
				{
					$this->current_url_target = $this->module_url_targets[$this->url_target_name];
				}
				
				if(!$this->current_url_target)
				{
					// Detect auto loading views
					// Explicitly check that the view exists in the module for this controller
					$potential_view_path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$module->folder().'/classes/views/'
						. $this->url_target_name.'/'.$this->url_target_name.'.php';
					
					// If a view file exists in this module and we're logged in as admin, we can render it
					if(file_exists($potential_view_path) && TC_currentUser() && TC_currentUser()->isAdmin())
					{
						$target = TSm_ModuleURLTarget::init( $this->url_target_name);
						$this->addModuleURLTarget($target);
						
						$this->current_url_target = $target;
						
					}
					else
					{
						TC_triggerError(get_called_class() . ': No URL Target defined for <em>"' . $this->url_target_name . '"</em>. If you are using a custom controller, ensure it is properly defined in the module /<em>module_name</em>/config/settings.php file.');
						return false;
					}
				}

				// Catch if we're going to need a user that doesn't exist
				// If it doesn't skip authentication but we don't have a user, then we need to just exist and logout
				if(!$this->current_url_target->skipsAuthentication() && !TC_currentUser())
				{
					// Needs a user and we don't have one. Get out, no matter what
					header('Location: /admin/');
					exit();
					
				}

				// Now we can bother with validation
				$this->parseURLForModel(); // get the model from the URL
				$this->validateURLTarget();
				
			}
			
			
		}	
		
		
	}
	
		
	/**
	 * Returns the messenger view
	 * @return TCv_Messenger
	 */
	public function messengerView()
	{
		return TCv_Messenger::instance();
	}
		
	/**
	 * Returns the module for this controller
	 * @return TSm_Module
	 */
	public function module()
	{
		return $this->module;
	}
	
	/**
	 * Returns the id for this controller, which will match the module id
	 * @return int
	 */
	public function id()
	{
		return $this->module()->id();
	}

	//////////////////////////////////////////////////////
	//
	// URL TARGETS
	//
	// Functions related to URL targets
	//
	//////////////////////////////////////////////////////



	/**
	 * Returns the current URL Target
	 * @return bool|TSm_ModuleURLTarget
	 */
	public function currentURLTarget()
	{
		// Possibly called before init complete in defineURLTargets()1250
		if(is_null($this->current_url_target))
		{
			$this->current_url_target = @$this->module_url_targets[$this->url_target_name];

		}
		return $this->current_url_target;
	}
	
	/**
	 * Adds the Settings URL Target
	 */
	protected function addSettingsURLTarget()
	{
		/** @var TSm_ModuleURLTarget $settings_url_target */
		$settings_url_target = TSm_ModuleURLTarget::init( 'settings');
		$settings_url_target->setViewName($this->module->variableFormClassName());
		$settings_url_target->setModelName('TSm_Module');
		$settings_url_target->setTitle('Settings');
		$settings_url_target->setAsRightButton('fa-cog');
		$this->addModuleURLTarget($settings_url_target);
	}			
	
	/**
	 * A method to calculate the list title for a given model. This is used by the generated default URL Targets.
	 * @param string $model_name
	 * @return string
	 */
	public function calculateListTitleForModel($model_name)
	{
		$list_title = '';
		if($model_name::$model_title_plural !== false)
		{
			return $model_name::$model_title_plural;
		}

		$last_letter_of_name = substr($model_name::$model_title, -1);
		if($last_letter_of_name == 'y')
		{
			$list_title = substr($model_name::$model_title, 0, strlen($model_name::$model_title) - 1).'ies';
		}
		else
		{
			$list_title = ($model_name::$model_title).'s';
		}
		return $list_title;
	}
	
	/**
	 * A shortcut method which creates a default set of URL targets for a model. It creates the list, create, edit, and
	 * delete url targets for the appropriate module. The List and Form view classes name must be formatted specifically
	 * for automatic setup. If the model name is 'TMm_ModelName', then the list view must be called 'TMv_ModelNameList'
	 * and the form view called 'TMv_ModelNameForm'. The model also requires a function called "title()". This function
	 * can also be used to generate url targets for a model which belongs to another parent model. This is common for one
	 * model to have a list of something else that belongs to it. In that case, two additional parameters are set which
	 * connect to two models together and the endpoints are adjusted appropriately.
	 *
	 * @param class-string<TCm_Model> $model_name
	 * @param bool|string  $url_target_name_prefix (Optional) Default false. An optional prefix that is necessary for any
	 * url targets which isn't the primary item for a module. If a module has more than one class which is being generated,
	 * then the secondary (ie: not main class represented by the module) needs a prefx. Any prefix will have a dash inserted after it.
	 * @param bool|string $parent_model_name (Optional) Default false. The model class name that this model will belong to.
	 * This is used when generating url targets for a model that actually belongs to a "parent" model. IF this is set, you
	 * must also set the next parameter which is a function to be called on the model which provides an instance of the parent.
	 * @param bool|string $parent_model_method (Optional) Default false. The name of the method which is called on the model,
	 * which returns a single instance of the parent model.
	 * @param bool|string $validation_method_name (Optional) Default NULL. Tee method to be called on the
	 * parent_model_name which returns a boolean, indicating if this item should be shown for the particular item being viewed. This requires
	 * the parent_model_name to be set in order to properly perform. This method is applied on top of the existing
	 * `userCanView` which is called every time.
	 *
	 * If you pass in a boolean into the validation method name, it will apply that as a yes/no value using setIsValid
	 */
	public function generateDefaultURLTargetsForModelClass(
									$model_name, 
									$url_target_name_prefix = false,
									$parent_model_name = false,
									$parent_model_method = false,
									$validation_method_name = null)
	{
		// Turns on the navigation subsections since we'll now want them
		// Only bother if the parent model name is false, which means it's top level
		if($parent_model_name == false)
		{
			$this->enableNavigationSubsections();
		}
		
		if(substr($model_name, 0,3) == 'TMm')
		{
			$view_title_start = str_ireplace('TMm_', 'TMv_', $model_name);
		}
		elseif(substr($model_name, 0,3) == 'TSm')
		{
			$view_title_start = str_ireplace('TSm_', 'TSv_', $model_name);
			
		}
		
		if(trim($url_target_name_prefix) != '' && substr($url_target_name_prefix, -1)  != '-')
		{
			$url_target_name_prefix .= '-';
		}
		
		
		$item_list_name = $url_target_name_prefix.'list';

		$item_list = TSm_ModuleURLTarget::init( $item_list_name);
		
		// Deal with the change to calling internal Tungsten lists "Manager Lists"
		$manager_list_name = $view_title_start.'ManagerList';
		if(!class_exists($manager_list_name))
		{
			// We don't have the new format, use the TMv_***List format
			$manager_list_name = $view_title_start.'List';
		}
		
		$item_list->setViewName($manager_list_name);
		$item_list->setTitle($this->calculateListTitleForModel($model_name));
		if($parent_model_name)
		{
			if($parent_model_method === false)
			{
				TC_triggerError('<em>generateDefaultURLTargetsForModelClass</em>: Attempting to set a parent model name without setting the parent model method.');
			}
			
			$item_list->setModelName($parent_model_name);
			$item_list->setModelInstanceRequired();
			if(is_string($validation_method_name))
			{
				$item_list->addValidation($parent_model_name, $validation_method_name);
			}
			
			
			if(isset($this->primary_url_targets_for_models[$parent_model_name]))
			{
				$item_list->setParentURLTargetWithName($this->primary_url_targets_for_models[$parent_model_name]);
			}
		}
		else // NO parent model
		{
			$item_list->setAsSubsectionParent(); // only if it has no parent
			if(is_bool($validation_method_name))
			{
				$item_list->setIsValid($validation_method_name);
			}
			elseif(!is_null($validation_method_name))
			{
				TC_triggerError('<em>generateDefaultURLTargetsForModelClass</em>: Attempting to use the validation_method_name without setting the parent_model_name.');
			}
			
		}
		$this->addModuleURLTarget($item_list);



		/** @var TSm_ModuleURLTarget $item_create */
		$item_create = TSm_ModuleURLTarget::init( $url_target_name_prefix.'create');
		$item_create->setViewName($view_title_start.'Form');
		if($parent_model_name)
		{
			$item_create->setModelName($parent_model_name);
			if(is_string($validation_method_name))
			{
				$item_create->addValidation($parent_model_name, $validation_method_name);
			}
			$item_create->setModelInstanceRequired();
		}
		else
		{
			$item_create->setModelName($model_name);
			if(is_bool($validation_method_name))
			{
				$item_create->setIsValid($validation_method_name);
			}
		}
		
		
		$create_title = 'Add '.$model_name::modelTitleSingularLower();
		$item_create->setTitle($create_title);
		
		
		$item_create->setParentURLTargetWithName($item_list_name);
		$item_create->setAsRightButton('fa-plus');
		$item_create->setAsPrimaryModelCreateTarget();
		$this->addModuleURLTarget($item_create);



		$item_edit = TSm_ModuleURLTarget::init($url_target_name_prefix.'edit');
		$item_edit->setViewName($view_title_start.'Form');
		$item_edit->setModelName($model_name);
		$item_edit->setTitleUsingModelMethod('title');
		$item_edit->setModelInstanceRequired();
		$item_edit->setAsModelSubmenu(1); // adds it as a submenu
		$item_edit->setIconCode('fa-sliders-h');
		
		
		// Tungsten 9 just calls them attributes
		if(TC_getConfig('use_tungsten_9'))
		{
			$item_edit->setTitle('Attributes');
		}
		else
		{
			$item_edit->setTitle('Edit ' . $model_name::$model_title);
		}
		$item_edit->addValidation($model_name, 'userCanEdit');
		if(is_string($validation_method_name))
		{
			$item_edit->addValidation($parent_model_name, $validation_method_name);
		}
		elseif(is_bool($validation_method_name))
		{
			$item_edit->setIsValid($validation_method_name);
		}
		$item_edit->setAsPrimaryModelEditTarget();
		if($parent_model_name)
		{
			$item_edit->setParentURLTargetWithName($item_list_name, $parent_model_method);
		}
		else
		{
			$item_edit->setParentURLTargetWithName($item_list_name);
		}
		$this->addModuleURLTarget($item_edit);
		$this->setPrimaryURLTargetForModelName($model_name, $url_target_name_prefix.'edit');



		$item_delete = TSm_ModuleURLTarget::init( $url_target_name_prefix.'delete');
		$item_delete->setModelName($model_name);
		$item_delete->setModelInstanceRequired();
		$item_delete->setModelActionMethod('delete');
		$item_delete->setTitle('Delete');
		$item_delete->setTitleUsingModelMethod('title');
		$item_delete->setNextURLTarget('referrer');
		$item_delete->enableFrontEndRouting();
		$item_delete->addValidation($model_name, 'userCanDelete');
		if(is_string($validation_method_name))
		{
			$item_delete->addValidation($parent_model_name, $validation_method_name);
		}
		elseif(is_bool($validation_method_name))
		{
			$item_edit->setIsValid($validation_method_name);
		}
		$item_delete->setAsPrimaryModelDeleteTarget();
		if($parent_model_name)
		{
			$item_delete->setParentURLTargetWithName($item_list_name, $parent_model_method);
		}
		else
		{
			$item_delete->setParentURLTargetWithName($item_list_name);
		}
		$this->addModuleURLTarget($item_delete);
		
		// ------------------------------------
	
		// DEAL WITH THE OTHER ITEMS ASSOCIATED WITH THE MODEL
		
		// PAGE BUILDER
		$this->detectPageBuilderForModel($model_name, $url_target_name_prefix);

		// METADATA PAGE MODEL VIEWABLE
		$this->detectAdvancedMetadataForModel($model_name, $url_target_name_prefix);
		
		// 301 REDIRECTS
		$this->detect301RedirectsForModel($model_name, $url_target_name_prefix);
		
		// HISTORY
		$this->detectHistoryForModel($model_name, $url_target_name_prefix);
		
		
		
	}
	
	/**
	 * Finds if the model uses a page builder, which means we need to add in the content for it
	 * @param string $model_name
	 * @param string $url_target_name_prefix
	 * @return void
	 */
	protected function detectPageBuilderForModel(string $model_name, string $url_target_name_prefix) : void
	{
		if( class_exists($model_name) && TC_classUsesTrait($model_name,'TMt_PageRenderer'))
		{
			// URL target to edit page content
			$target = TSm_ModuleURLTarget::init($url_target_name_prefix.'edit-content');
			$target->setViewName('TMv_PageBuilder_ThemeWrapper');
			$target->setModelName($model_name);
			$target->setModelInstanceRequired();
			$target->setParentURLTargetWithName($url_target_name_prefix.'edit');
			$target->setTitle('Content');
			$target->setIconCode('fa-pencil-ruler');
			$target->setAsModelSubmenu();
			$this->addModuleURLTarget($target);
			
			//////////////////////////////////////////////////////
			//
			// PUBLISHING
			//
			//////////////////////////////////////////////////////
			// Tungsten 9 does NOT use the main 'edit'
			// Instead, have it redirect to page builder
			if(TC_getConfig('use_tungsten_9') && TC_getConfig('use_pages_version_publishing'))
			{
				$target = TSm_ModuleURLTarget::init('publish');
				$target->setTitle('Publish');
				$target->setModelName($model_name);
				$target->setModelInstanceRequired();
				$target->setModelActionMethod('publish');
				$target->setNextURLTarget('referrer');
				$this->addModuleURLTarget($target);
				
				$target = TSm_ModuleURLTarget::init('discard-unpublished-changes');
				$target->setTitle('Discard unpublished');
				$target->setModelName($model_name);
				$target->setModelInstanceRequired();
				$target->setModelActionMethod('discardUnpublishedChanges');
				$target->setNextURLTarget('referrer');
				$this->addModuleURLTarget($target);
			}
			
			
		}
	}
	
	/**
	 * Finds if there is a history URL target required for the provided model, using the prefixes that we already define
	 * @param class-string $model_name
	 * @param string $url_target_name_prefix
	 * @return void
	 */
	protected function detectHistoryForModel(string $model_name, string $url_target_name_prefix) : void
	{
		if(TC_getConfig('use_tungsten_9') && TC_getConfig('use_track_model_history'))
		{
			// Only bother with history if there's a class name to test for
			if(class_exists($model_name))
			{
				if($model_name::hasTrait('TSt_ModelWithHistory'))
				{
					$target = TSm_ModuleURLTarget::init( $url_target_name_prefix.'history');
					$target->setModelName($model_name);
					$target->setModelInstanceRequired();
					$target->setTitle('History');
					$target->setViewName('TSv_ModelHistoryStateList');
					$target->setParentURLTargetWithName($url_target_name_prefix.'edit');
					$target->setAsModelSubmenu();
					$target->addValidation($model_name, 'userCanEdit');
					$target->addValidation('current-user', 'isAdmin');
					
					$target->setIconCode('fa-history');
					
					$this->addModuleURLTarget($target);
				}
			}
		}
	}
	
	/**
	 * Finds if there is an advanced metadata URL target required for the provided model, using the prefixes that we already define
	 * @param class-string $model_name
	 * @param string $url_target_name_prefix
	 * @return void
	 */
	protected function detectAdvancedMetadataForModel(string $model_name, string $url_target_name_prefix) : void
	{
		if(TC_getConfig('use_seo_advanced') &&
			class_exists($model_name) &&
			// If it's a page menu itself OR it's a class that is page module viewwable
			(
				$model_name == 'TMm_PagesMenuItem'
				|| is_subclass_of($model_name, 'TMm_PagesMenuItem')
				|| TC_classUsesTrait($model_name,'TMt_PageModelViewable')
			))
		{
			$target = TSm_ModuleURLTarget::init($url_target_name_prefix.'metadata');
			$target->setViewName('TMv_PageContentViewableMetadataForm');
			$target->setModelName($model_name);
			$target->setTitle('Metadata');
			$target->setModelInstanceRequired();
			$target->setParentURLTargetWithName($url_target_name_prefix.'edit');
			$target->setAsModelSubmenu();
			$target->setIconCode('fa-circle-info');
			
			$target->addValidation($model_name, 'userCanEdit');
			$target->setIsValid($model_name::modelUsesAdvancedMetaData());
			$this->addModuleURLTarget($target);
			
			
		}
	}
	
	/**
	 * Finds if there is an 301 redirect URL target required for the provided model, using the prefixes that we already define
	 * @param class-string $model_name
	 * @param string $url_target_name_prefix
	 * @return void
	 */
	protected function detect301RedirectsForModel(string $model_name, string $url_target_name_prefix) : void
	{
		
		$pages_module = TMm_PagesModule::init();
		
		if( class_exists($model_name)
			&& TC_classUsesTrait($model_name,'TMt_Page301Redirectable')
			&& $pages_module->use301Redirects())
		{
			$target = TSm_ModuleURLTarget::init( $url_target_name_prefix.'page-301-redirect-list');
			$target->setViewName('TMv_Pages301RedirectList');
			$target->setModelName($model_name);
			$target->setModelInstanceRequired();
			$target->setTitle('301 redirects');
			$target->setParentURLTargetWithName($url_target_name_prefix.'edit');
			$target->setAsModelSubmenu();
			$target->setIconCode('fa-external-link-alt');
			
			$this->addModuleURLTarget($target);
			
			$target = TSm_ModuleURLTarget::init( $url_target_name_prefix.'create-page-301-redirect');
			$target->setViewName('TMv_Pages301RedirectForm');
			$target->setModelName($model_name);
			$target->setModelInstanceRequired();
			$target->setTitle('New 301 redirect');
			$target->setAsRightButton('fa-plus');
			$target->setParentURLTargetWithName($url_target_name_prefix.'page-301-redirect-list');
			$this->addModuleURLTarget($target);
			
			$target = TSm_ModuleURLTarget::init( $url_target_name_prefix.'301-redirect-edit');
			$target->setViewName('TMv_Pages301RedirectForm');
			$target->setModelName('TMm_Pages301Redirect');
			$target->setModelInstanceRequired();
			$target->setTitle('Edit 301 redirect');
			$target->setParentURLTargetWithName($url_target_name_prefix.'page-301-redirect-list','targetModel');
			$this->addModuleURLTarget($target);
			
			
		}
	}
	
	/**
	 * Indicates the URL Target that should be refered to when trying to "point" to a particular model. Using the automated
	 * mechanisms will automatically set it to the editor for a model.
	 * @param string $model_name
	 * @param string $url_target_name
	 */
	public function setPrimaryURLTargetForModelName($model_name, $url_target_name)
	{
		$this->primary_url_targets_for_models[$model_name] = $url_target_name;
	}
	
	/**
	 * Define the list of available URL targets for this module
	 */
	public function defineURLTargets()
	{
		foreach($this->module->modelNames() as $model_name => $values)
		{
			if(class_exists($model_name))
			{
				if($values['init_url_targets'])
				{
					$this->generateDefaultURLTargetsForModelClass($model_name, $values['url_target_name_prefix']);
				}
			}
		}
		
		// Add the default one for the module
		/** @var TSm_ModuleURLTarget $item_list_redirect */
		$item_list_redirect = TSm_ModuleURLTarget::init('');
		$item_list_redirect->setNextURLTarget('list');
		
		$list_target = $this->URLTargetWithName('list');
		if($list_target)
		{
			$item_list_redirect->setParent($this->URLTargetWithName('list'));
			$this->addModuleURLTarget($item_list_redirect);
			
		}
		
		$this->traitURLTargets();
		
	}
	
	/**
	 * Detects if the primary model for this module is enabled with 301 redirects. If so, it adds the necessary URL
	 * targets to support that functionality.
	 * @deprecated This has been incorporated into the core functionality elsewhere
	 * @uses TMt_Page301Redirectable
	 * @uses TMv_Pages301RedirectList
	 */
	protected function detectModelWith301Redirects()
	{
	
		
		
		
	}
	
	/**
	 * Detects if the primary model for this module is enabled with page model viewable and if SEO is enabled.
	 * @deprecated: NO longer needed as this is properly handled in the generateDefaultURLTargetsForModelClass function
	 * for any of the models. It detects a model and tries to find the same conditions.
	 * @uses TMt_PageModelViewable
	 * @see TSc_ModuleController::generateDefaultURLTargetsForModelClass()
	 */
	protected function detectModelWithPageModelViewableSEO()
	{
	
		
	}
	
	/**
	 * Adds a URL target to a submenu grouping that contains 'edit'. This is the main URL target for an element
	 * @param string $target_name
	 * @return void
	 */
	protected function addURLTargetToEditSubmenuGrouping(string $target_name)
	{
		$found = false;
		
		// Find groupings and add this to the grouping with `edit`
		foreach($this->submenu_groupings as $name => $grouping)
		{
			foreach($grouping as $index => $grouping_url_target)
			{
				if($grouping_url_target == 'edit')
				{
					$found = true;
					$this->submenu_groupings[$name][] = $target_name;
				}
			}
		}
		
		// Add it if it doesn't exist, assuming 'edit' exists
		if(!$found && $this->hasURLTargetWithName('edit'))
		{
			$this->defineSubmenuGroupingWithURLTargets('edit',$target_name);
		}
		
	}

	
	/**
	 * A method to be overridden in traits to define URL targets that don't conflict with the defineURLTargets() method.
	 * This method is called at the end of the main defineURLTargets function within the main TSc_ModuleController.
	 */
	public function traitURLTargets()
	{
	}
	
	/**
	 * Returns the list of URL targets
	 * @return TSm_ModuleURLTarget[]
	 */
	public function URLTargets()
	{
		return $this->module_url_targets;
	}
	
	/**
	 * Adds a module URL target to this controller
	 * @param TSm_ModuleURLTarget $url_target
	 */
	public function addModuleURLTarget(TSm_ModuleURLTarget $url_target) : void
	{
		$url_target->setModule($this->module);
		
		if($url_target_name = $url_target->parentURLTargetName())
		{
			$url_target->setParent($this->URLTargetWithName($url_target_name));
		}
		
		// DETECT MODEL SUBMENUS
		if($url_target->isModelSubmenu())
		{
			if(!isset($this->submenu_groupings[$url_target->modelName()]))
			{
				$this->submenu_groupings[$url_target->modelName()] = [];
			}
			$this->submenu_groupings[$url_target->modelName()][] = $url_target->name();
			
			
		}
		
		
		$this->module_url_targets[$url_target->name()] = $url_target;
	}
	
	/**
	 * Removes a URL Target with a specific name.
	 * @param string $url_target_name
	 */
	public function removeURLTargetWithName($url_target_name)
	{
		unset($this->module_url_targets[$url_target_name]);
		
		$this->removeURLTargetWithNameFromSubmenuGroupings($url_target_name);
	
	}
	
	/**
	 * Returns a URL Target with the provided name
	 * @param string $name
	 * @return ?TSm_ModuleURLTarget
	 */
	public function URLTargetWithName(string $name) : ?TSm_ModuleURLTarget
	{
		if($this->hasURLTargetWithName($name))
		{
			return $this->module_url_targets[$name];
		}
		return null;
	}
	
	/**
	 * Returns if this controller has a url target with a given name
	 * @param string $url_target_name
	 * @return bool
	 */
	public function hasURLTargetWithName(string $url_target_name) : bool
	{
		return isset($this->module_url_targets[$url_target_name]);
	}
	
	/**
	 * Performs the URL target with the provided name
	 * @param string $url_target_name
	 * @param bool|string  $module_folder
	 */
	public function performURLTargetWithName($url_target_name, $module_folder = false)
	{
		if($url_target_name == 'referrer' || $url_target_name == 'referer')
		{
			header("Location: ".$_SERVER['HTTP_REFERER'].$this->current_url_target->nextURLSuffix());
			exit();
		}
		
		// Passed a value of null as a string, DO NOTHING
		if($url_target_name == 'null')
		{
			exit();
		}
		
		
		if($module_folder)
		{
			$url_target = TC_URLTargetFromModule( $module_folder ,$url_target_name);
			
		//	TC_triggerError('Attempting to perform url target outside of this module - '.$module_folder);
		}
		else
		{
			$url_target = $this->module_url_targets[$url_target_name];
		}
				
		if($url_target)
		{
			$url = $url_target->url();
			
			$return_type_string = '';
			// Deal with an override coming via a parameter
			if(isset($_GET['return_type']))
			{
				$url .= '?return_type='.$_GET['return_type'];
			}
			elseif(isset($_POST['return_type']))
			{
				$url .= '?return_type='.$_POST['return_type'];
			}
			
			
			
			header("Location: ".$url);
			exit();
		
		}
		else
		{
			TC_triggerError('Module URL Target with name <em>'.$url_target_name.'</em> does not exist. Please define the URL Target in your module controller');
		}
	}
	
	/**
	 * Returns if the current URL Target that is being viewed can properly "see" another given URL Target. This checks
	 * that the current target returns all the appropriate models and classes as necessary without throwing any errors.
	 * @param TSm_ModuleURLTarget $url_target
	 * @return bool
	 */
	public function currentTargetCanSeeURLTarget($url_target)
	{
		TC_assertObjectType($url_target, 'TSm_ModuleURLTarget') ;
		
		//$path = '/admin/'.$this->module->folder().'/do/'.$url_target->name().'/';
		if($url_target->modelInstanceRequired() && $url_target->modelInstanceIDRequired())
		{
			$model = TC_activeModelWithClassName($url_target->modelName());
			
			if(!$model)
			{
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * Processes the action from the URL
	 * @return TCv_View[]
	 */
	public function processActionFromURL()
	{
		$views = array();
		
		if($this->current_url_target && $this->is_valid)
		{
			$model = TC_activeModelWithClassName($this->current_url_target->modelName());

			// Perform Action Methods
			if($method_name = $this->current_url_target->modelActionMethod())
			{
				$method_name = str_ireplace('()', '', $method_name);
				$class_name = $this->current_url_target->modelName();
				
				// Deal with model lists and other singletons which commonly have this issue
				if($class_name::isSingleton())
				
				{
					$model = $class_name::init();
				}
				
				// performing an action requires a model
				if(!$model && $this->current_url_target->modelInstanceObjectRequired())
				{
					if($this->current_url_target->modelInstanceIDRequired())
					{
						$model = TC_activeModelWithClassName($this->current_url_target->modelName());
					}
					else
					{
						$model = ($this->current_url_target->modelName())::init();
					}
					
					
				}
				
				if(!$model) // try for static
				{
					$model = $this->current_url_target->modelName();
				}

				$pass_values_method_type = $this->current_url_target->passValuesIntoActionMethodType(); // false, url, post or get
				$indices = $this->current_url_target->passValueIntoActionMethodIndices(); // array of indices
				
				
				if(!method_exists($model, $method_name))
				{
					$this->addConsoleError("Method with name '$method_name' does not exist the model");
					TC_triggerError("Method with name '$method_name' does not exist the model");
				}
				
				// Handle Legacy case first. Old system that only cared about URLs and pushed them all in. Ugh.
				if($indices == 'all')
				{
					$parameter_array = [];

					if($pass_values_method_type == 'url')
					{
						for($i = 1; $i <= $this->num_values_from_url; $i++)
						{
							$var_name = 'id_'.$i;
							$parameter_array[] = $this->$var_name;
						}
					}
					elseif($pass_values_method_type == 'post')
					{
						$parameter_array = $_POST;
					}
					elseif($pass_values_method_type == 'get')
					{
						$parameter_array = $_GET;
					}
					
					// Named parameters in PHP 8 will break when passed in
					// @see https://dev.to/seongbae/unknown-named-parameter-2gln
					// @see https://www.php.net/manual/en/function.call-user-func-array.php
					$parameter_array = array_values($parameter_array);
					
					
					$result_message = call_user_func_array(array($model, $method_name), $parameter_array);


				}
				
				
				// Method type is set and it's not legacy, grab values and pass them in appropriately
				elseif($pass_values_method_type != false)
				{
					$parameter_array = [];
					
					// Loop through indices, attempting to find a value dpeending on the method type
					foreach($indices as $index_name)
					{
						// Default to null
						$parameter_value = null;
						$var_name = 'id_'.$index_name;
						
						if($pass_values_method_type == 'post' && isset($_POST[$index_name]))
						{
							$parameter_value = $_POST[$index_name];
						}
						elseif($pass_values_method_type == 'get' && isset($_GET[$index_name]))
						{
							$parameter_value = $_GET[$index_name];
						}
						elseif($pass_values_method_type == 'url' && isset($this->$var_name))
						{
							$parameter_value = $this->$var_name;
						}
						
						// Save the value to the array
						$parameter_array[] = $parameter_value;
					}
					
					
					$result_message = call_user_func_array(array($model, $method_name), $parameter_array);
				}
				else
				{
					$result_message = call_user_func_array(array($model, $method_name),[]);
				}

				$this->processPossibleReturnTypeWithValue($result_message);



				if(is_string($result_message) && trim($result_message) != '' )
				{
					$messenger = TCv_Messenger::instance();
					$messenger->setSuccessTitle($result_message);
	
				}
				else
				{
					$result_message = '';
				}
				
				
			}
			
			// Present any Views
			if($this->current_url_target->isView())
			{
				// Only attach if we're not in Tungsten 9
				if(!TC_getConfig('use_tungsten_9'))
				{
					$views[] = $this->messengerView();
					
					// add the help view if it exists
					$help_view = $this->current_url_target_view->helpView();
					if($help_view instanceof TCv_View)
					{
						$views[] = $this->helpViewContainer($help_view);
					}
				}
				
				$views[] = $this->current_url_target_view;
			}
			
			// FRONT-END ROUTING NEXT PAGE
			if(substr($_SERVER['REQUEST_URI'],0,3) == '/w/' && $this->current_url_target->allowsFrontEndRouting())
			{
				// Only bother if we've set the next page with setFrontEndRouting_NextModelAndViewMode()
				$next_model = $this->current_url_target->frontEndRouting_NextModel();
				$next_view_mode = $this->current_url_target->frontEndRouting_NextViewMode();
				
				// If yes, we have a setting, use this no matter what
				if(is_string($next_model) && is_string($next_view_mode))
				{
					if(class_exists($next_model) && $active_model = TC_activeModelWithClassName($next_model))
					{
						if($menu_item = $next_model::menuItemWithPrimaryViewMode($next_view_mode))
						{
							header("Location: ".$menu_item->viewURL().$this->currentURLTarget()->nextURLSuffix());
							exit();
						}
					}
					
					// Failsafe
					// If we get here, someone TRIED to set it because we had string values
					// We're front-end routing, so don't let it redirect to admin ever
					// If it failed, go to the referrer, better than admin
					header("Location: ".$_SERVER['HTTP_REFERER']);
					exit();
				}
			}
			
			
			// Perform Next Action
			$next_url_target_name = $this->current_url_target->nextURLTargetName();
			
			// Deal with an override coming via a parameter
			if(isset($_GET['next_url_target_name']))
			{
				$next_url_target_name = $_GET['next_url_target_name'];
			}
			elseif(isset($_POST['next_url_target_name']))
			{
				$next_url_target_name = $_POST['next_url_target_name'];
			}
			
			
			if(is_null($next_url_target_name) || isset($_GET['cancel_next_action']) || isset($_POST['cancel_next_action']))
			{
				exit();
			
			}
			elseif(substr($next_url_target_name,0,1) == '/') // redirecting to a internal URL
			{
				header("Location: ".$next_url_target_name.$this->currentURLTarget()->nextURLSuffix());
				exit();
			}
			elseif($next_url_target_name !== false)
			{
				$this->performURLTargetWithName($next_url_target_name, $this->current_url_target->nextURLTargetModuleFolder());
			}
			
			
		}
		
		return $views;
	}
	
	//////////////////////////////////////////////////////
	//
	// PRIMARY CREATE/VIEW/EDIT/DELETE
	//
	// Methods associated with indicating the primary URL
	// targets for creating, viewing, editing or deleting
	// a model in Tungsten
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the URL targets that use a specific model class
	 * @param string $model_class
	 * @return TSm_ModuleURLTarget[]
	 */
	protected function urlTargetsUsingModelClass(string $model_class)
	{
		if(!isset($this->url_targets_using_model[$model_class]))
		{
			$this->url_targets_using_model[$model_class] = [];
			foreach($this->module_url_targets as $url_target)
			{
				// @TODO: Check for extended classes
				if($url_target->modelName() == $model_class)
				{
					$this->url_targets_using_model[$model_class][] = $url_target;
				}
			}
		}
		
		
		return $this->url_targets_using_model[$model_class];
	}
	
	/**
	 * Returns the URL target (if it exists) that is set as the primary edit target for the provided model class
	 * @param string $model_class
	 * @return false|TSm_ModuleURLTarget
	 */
	public function primaryEditURLTargetForModelClass(string $model_class)
	{
		foreach($this->urlTargetsUsingModelClass($model_class) as $url_target)
		{
			if($url_target->isPrimaryModelEditTarget())
			{
				return $url_target;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the URL target (if it exists) that is set as the primary delete target for the provided model class
	 * @param string $model_class
	 * @return false|TSm_ModuleURLTarget
	 */
	public function primaryDeleteURLTargetForModelClass(string $model_class)
	{
		foreach($this->urlTargetsUsingModelClass($model_class) as $url_target)
		{
			if($url_target->isPrimaryModelDeleteTarget())
			{
				return $url_target;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the URL target (if it exists) that is set as the primary view target for the provided model class
	 * @param string $model_class
	 * @return false|TSm_ModuleURLTarget
	 */
	public function primaryViewURLTargetForModelClass(string $model_class)
	{
		foreach($this->urlTargetsUsingModelClass($model_class) as $url_target)
		{
			if($url_target->isPrimaryModelViewTarget())
			{
				return $url_target;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the URL target (if it exists) that is set as the primary create target for the provided model class
	 * @param string $model_class
	 * @return false|TSm_ModuleURLTarget
	 */
	public function primaryCreateURLTargetForModelClass(string $model_class)
	{
		foreach($this->urlTargetsUsingModelClass($model_class) as $url_target)
		{
			if($url_target->isPrimaryModelCreateTarget())
			{
				return $url_target;
			}
		}
		
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ACCESS VALIDATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Validates if the current URL Target should display any views or perform any actions. If the URL target is found
	 * to be invalid, no error will be thrown but the internal value of is_valid will be set to false and the controller will not perform any actions.
	 */
	protected function validateURLTarget()
	{
		if($this->is_valid)
		{
			$this->is_valid = $this->current_url_target->validateAccess();
			if (!$this->is_valid)
			{
				$this->addConsoleError('URL Target Not Valid : ' . $this->current_url_target->name());
			}
		}
	}
	
	/**
	 * Sets the controller to show a validation error. This will only ever show the first error found, ensuring it moves
	 * sequentially and doesn't reveal more than it should.
	 * @param string $message
	 */
	protected function triggerValidationError($message)
	{
		// ensure is_valid is set to false
		$this->is_valid = false;
		
		if(is_null($this->validation_error_message))
		{
			$this->validation_error_message = $message;	
		}
		
	}
		
	/**
	 * Indicates if the URL Target being viewed is valid. If so, then it will load as normal. If not, then it indicates
	 * that something is being viewed which shouldn't be.
	 * @return bool
	 */
	protected function isValid()
	{
		return $this->is_valid;
	}

	//////////////////////////////////////////////////////
	//
	// IDS
	//
	//////////////////////////////////////////////////////

	/**
	 * Parses the IDs from the URL
	 */
	protected function parseIDsFromURL()
	{

		$this->num_values_from_url = 0;
		$keep_looking = false;
		if(isset($_GET['id']))
		{
			$this->primary_id = $_GET['id'];
			$this->id_1 = $_GET['id'];
			$this->num_values_from_url++;
			$keep_looking = true;
		}
		
		$num = 2;
		while($keep_looking)
		{
			$var_name = 'id_'.$num;
			if(isset($_GET[$var_name]))
			{
				$this->$var_name = $_GET[$var_name];
				$this->num_values_from_url++;
				
			}
			else
			{
				$keep_looking = false;
			}
			$num++;
			
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// MODELS
	//
	//////////////////////////////////////////////////////


	/**
	 * A function that will return the current model instance that exists for the current URL target that is loaded.
	 * @return bool|TCm_Model|TCu_Item
	 */
	protected function currentModelForCurrentURLTarget()
	{
		if($this->currentURLTarget()->modelName() != '')
		{
			return TC_activeModelWithClassName($this->currentURLTarget()->modelName());
		}
		
		return false;
	}		
				
	/**
	 * Parses the URL and determines if there is a model being viewed. For more complex modules, this function can be
	 * overridden to allow for models other than the primary one. In those scenarios, the primary model should be set
	 * using the resulting model. Failure to do so will create errors.
	 */
	protected function parseURLForModel()
	{
		$current_model_name = $this->current_url_target->modelName();
		if($current_model_name != '')
		{
			$model = false;
			// Model says current user, so that's the model we load
			if($current_model_name == 'current-user')
			{
				$model = TC_currentUser();
			}
			
			// Requires an ID to be passed and we have one
			elseif($this->current_url_target->modelInstanceIDRequired()
				&& $this->primary_id !== false
				&& $this->primary_id != '')
			{
				$model = $current_model_name::init($this->primary_id);
			}
			
			// Requires an object to be passed, so we need first value is the class name, the second is the ID
			// We instantiate that model and pass it
			elseif($this->current_url_target->modelInstanceObjectRequired() && $this->primary_id && $this->id_2)
			{
				$id_class = $this->primary_id;
				$init_model = $id_class::init($this->id_2);
				$model = $current_model_name::init( $init_model);
			}
			
			// The model is a singleton, instantiate without any params
			elseif($current_model_name::isSingleton())
			{
				$model = $current_model_name::init();
			}
			
			// We indicate that we need an instance, but NO ID. That means we need to load it with no params
			// Which is similar to singleton, but less explicit. It might be a class that has a default but we
			// want that default to load, so it's fine
			elseif($this->current_url_target->modelInstanceRequired() &&
				!$this->current_url_target->modelInstanceIDRequired())
			{
				$model = $current_model_name::init();
			}
			
			// Instance is required, so we get one with the active model name
			elseif($this->current_url_target->modelInstanceRequired())
			{
				// Try to see if the model is already loaded
				$model = TC_activeModelWithClassName($this->current_url_target->modelName());
			}
			else
			{
				// STATIC CALL IS HAPPENING
			}

			
			if($model)
			{
				TC_saveActiveModel($model);
				TC_setPrimaryModel($model);
				
				foreach($this->current_url_target->loadModelMethods() as $load_model_method)
				{
					$loaded_model = TC_primaryModel()->$load_model_method();
					if($loaded_model)
					{
						TC_saveActiveModel($loaded_model);
					}	
					else
					{
						$this->addConsoleError('Module Controller: method '.get_class($model).'::'.$load_model_method.'() could instantiate a valid model.');	
					}
				}
				
				// handle parent model methods
				if($method_name = $this->current_url_target->parentModelMethodName())
				{
					$this_model = TC_primaryModel();
					$parent_model = $this_model->$method_name();
					
					if($parent_model instanceof TCm_Model)
					{
						TC_saveActiveModel($parent_model);
					}
					else // not a model
					{
						TC_triggerError('URL Target named <em>'.$this->current_url_target->name().'</em> attempting to call <em>'.get_class($this_model).'->'.$method_name.'()</em> but the returned value is set to a <em>'.$parent_model.'</em>. Check that the values provided to the configuration <em>setParentURLTargetWithName()</em> are properly set. ');
					}
				}
			}

			// ERROR
			elseif($this->current_url_target->modelInstanceRequired())
			{
				TC_triggerError('URL Target named "<em>'.$this->current_url_target->name().'"</em> could not be instantiated. Requires a model "<em>'.$this->current_url_target->modelName().'"</em> with the ID "<em>'.$this->primary_id.'</em>"');
			}
		}
	
	}


	//////////////////////////////////////////////////////
	//
	// BREADCRUMB BAR
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the breadcrumb bar
	 * @return TSv_ModuleNavigation_BreadcrumbBar
	 */
	public function breadcrumbBar()
	{
		/** @var TSv_ModuleNavigation_BreadcrumbBar $breadcrumb_bar */
		$breadcrumb_bar = TSv_ModuleNavigation_BreadcrumbBar::initWithoutCache(false);
		
		// Add The module name to the breadcrumb bar
		$breadcrumb_bar->addBreadcrumb($this->module->title(), '/admin/'.$this->module->folder().'/');
			
			
		$current_url_target = $this->current_url_target;
		$url_target_menu_buttons = array();
		while($current_url_target instanceof TSm_ModuleURLTarget)
		{
			if($current_url_target->title() != '' && $current_url_target->userHasAccess())
			{
				$url_target_menu_buttons[] = TSv_ModuleURLTargetLink::init( $current_url_target);;
			}
			$current_url_target = $current_url_target->parentURLTarget();	
		}
		
		krsort($url_target_menu_buttons);
		foreach($url_target_menu_buttons as $button)
		{
			$breadcrumb_bar->attachView($button);
		}
		
		if($this->current_url_target)
		{
			foreach($this->module_url_targets as $url_target)
			{
				if($url_target->isRightButton() && $url_target->name() != 'settings')
				{
					//if($this->current_url_target->isParentURLTarget($url_target))
					if($url_target->isParentURLTarget($this->current_url_target) || $url_target->parentURLTarget() == false)
					{
						$show_button = true;
						
						// requires validation before showing.
						if($validation_method = $url_target->rightButtonValidationMethod())
						{
							// get the model instance
							$model = TC_activeModelWithClassName($url_target->modelName());
							
							// call the validation method and if it retursn false, don't show the button
							$is_visible = $model->$validation_method();
							if(!$is_visible)
							{
								$show_button = false;
							}
						}
						// No validation but a model name still exists
						elseif($url_target->modelInstanceRequired())
						{
							if($url_target->modelInstanceIDRequired())
							{
								$model = TC_activeModelWithClassName($url_target->modelName());
							}
							else
							{
								$model = ($url_target->modelName())::init();
							}
							
							if(!$model)
							{
								$show_button = false;
							}

						}


						// ensure there's access before showing
						if(!$url_target->userHasAccess())
						{
							$show_button = false;
						} 
						
						if($show_button)
						{
							/** @var TSv_ModuleURLTargetLink $right_button */
							$right_button = TSv_ModuleURLTargetLink::init( $url_target);;
							if($url_target->rightButtonIconCode() != '')
							{
								$icon_view = new TCv_View();
								$icon_view->setTag('i');
								$icon_view->addClass($url_target->rightButtonIconCode());
								$right_button->attachView($icon_view, true); // prepend icons
							}
							$breadcrumb_bar->addRightView($right_button);	
						}
						
				
					}
				}
			}
		}
		
		return $breadcrumb_bar;
				
	}

	//////////////////////////////////////////////////////
	//
	// SUBMENU BAR
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the indices for all the possible submenus that could be shown for this item
	 * @return int[]
	 */
	public function activeSubmenuIndices()
	{
		return array();
	}
	
	
	/**
	 * Returns an array of URL targets that make up the sub-group for the provided URL target URL target
	 * @param ?TSm_ModuleURLTarget $test_url_target The URL target to test, if null is provided, it gets the current one
	 * @returns TSm_ModuleURLTarget[]
	 */
	public function submenuGroupingForURLTarget(?TSm_ModuleURLTarget $test_url_target = null) : array
	{
		if(is_null($test_url_target))
		{
			$test_url_target = $this->currentURLTarget();
		}
		// Commenting out the code that goes back all the levels. Just the current URL target matters
		// Build an array of all the parent and current url target names
		//$path_url_targets = $this->currentURLTarget()->ancestorURLTargets();
		//$path_url_targets[] = $this->currentURLTarget();
		
		
		
		// No grouping found yet, check if the parents are in the group URL targets
		foreach($this->submenu_groupings as $group_index => $group_url_target_names)
		{
			// The current url target is in the group
			if(in_array($test_url_target->name(), $group_url_target_names))
			{
				
				// Loop through the names and generate an array of URL targets for those names
				$group_url_targets = [];
				foreach($group_url_target_names as $url_target_name)
				{
					// check for validity
					$url_target = $this->URLTargetWithName($url_target_name);
					if($url_target->validateAccess())
					{
						$group_url_targets[$url_target_name] = $url_target;
					}
				}
				
				// Don't bother if there's just one
				if(count($group_url_targets) > 1)
				{
					return $group_url_targets;
				}
				
			}
			
			
		}
		
		return [];
	}
	
	/**
	 * @param string $grouping_variable_name
	 * @return bool|TSv_ModuleNavigation_SubmenuBar|TSv_ModuleNavigation_SubmenuBar[]
	 */
	public function submenuBar($grouping_variable_name = 'submenu_groupings')
	{
		$groups_to_show = [];
	
		// Build an array of all the parent and current url target names
		$path_url_targets = $this->currentURLTarget()->ancestorURLTargets();
		$path_url_targets[] = $this->currentURLTarget();
		
		// No grouping found yet, check if the parents are in the group URL targets
		foreach($this->$grouping_variable_name as $group_index => $group_url_targets)
		{
			foreach($path_url_targets as $this_url_target)
			{
				if(in_array($this_url_target->name(), $group_url_targets))
				{
					$groups_to_show[$group_index] = $group_index;
				}
			}
		
		}
		
		if(count($groups_to_show) > 0)
		{
			$submenus = array();
			foreach($groups_to_show as $group_index)
			{
				$groupings = $this->$grouping_variable_name;
				$group_url_targets = $groupings[$group_index];
				$submenus[] = $this->generateSubmenuForURLTargetNames($group_url_targets);
			}
			return $submenus;
		}
		else
		{
			// Return empty submenus
			return TSv_ModuleNavigation_SubmenuBar::init('breadcrumb_submenus');
			
		}
		
	}
	
	/**
	 * Generates a submenu for the provided URL Target name
	 * @param string[] $url_target_names
	 * @return bool|TSv_ModuleNavigation_SubmenuBar
	 */
	protected function generateSubmenuForURLTargetNames($url_target_names)
	{
		// Get all the URL targets
		$url_targets = [];
		foreach($url_target_names as $url_target_name)
		{
			if($this->hasURLTargetWithName($url_target_name))
			{
				$url_targets[] = $this->URLTargetWithName($url_target_name);
			}
		}
		
		// SORT URL TARGETS BY submenu order, which may or may not be set
		// Loop through each one, splitting them into two groups, sorted and not
		$sortable = [];
		$unsorted = [];
		foreach($url_targets as $url_target)
		{
			
			$submenu_order =$url_target->submenuOrder();
			if(is_null($submenu_order))
			{
				$unsorted[] = $url_target;
			}
			else // sortable
			{
				$num = 0;
				do
				{
					// Try decimals to differentiate, takes the number and divides by ten
					// So we're using X.1, X.2, etc
					$submenu_order += $num/10;
					$num++;
				}
				while(isset($sortable[$submenu_order]));
				
				// Made it through, add it t
				$sortable[$submenu_order] = $url_target;
			}
		}
		
		// rearrange the sortable ones by their unique inidces
		ksort($sortable);
		
		// Combine the sortable and unsorted
		$url_targets = array_merge($sortable, $unsorted);
		
		
		
		$submenu = TSv_ModuleNavigation_SubmenuBar::init('submenus_'.$url_target_names[0]);
		
		
		$menu_items = array();
		$current_menu_found = false;
		foreach($url_targets as $url_target)
		{
			// check for validity
			if($url_target->validateAccess())
			{
				$link = TSv_ModuleURLTargetLink::init($url_target);
				$link->addRelatedMenuClasses();
				$link->useGenericTitle();
				$menu_items[] = $link;
				
				if($url_target_name == $this->currentURLTarget()->name())
				{
					$current_menu_found = true;
				}
			}
		}
		
		foreach($menu_items as $menu_item)
		{
			if($current_menu_found)
			{
				$menu_item->removeClass('parent_menu');
				$menu_item->removeClass('active_menu');
			}
			$submenu->attachView($menu_item); // use generic titles on submenus
			
		}
		
		return $submenu;
	}
	
	/**
	 * Indicates a grouping of submenus that should be shown if any of the url targets are active. A grouping can
	 * contain any number of url targets, so long as they all require the same model (if any). This is commonly used for
	 * "editors" where there are different tabs when editing particular item. the different url targets can be grouped
	 * together to ensure they appear if that object is being edited. The logic for showing these submenus is handled by
	 * the controller, so long as all the url targets are valid.
	 */
	public function defineSubmenuGroupingWithURLTargets() : void
	{
		
		// find the list endpoints from the argument list
		$url_target_names = func_get_args();
		$grouping_class_name = null;
		
		// Check for an array being passed in
		foreach($url_target_names as $index => $url_target_name)
		{
			if(is_array($url_target_name))
			{
				unset($url_target_names[$index]);
				$url_target_names = array_merge($url_target_names , $url_target_name);
			}
			
			
		}
		
		// Process Tungsten 9, to deal with loading in right panel
		// Must be done after arrays are processed
		if(TC_getConfig('use_tungsten_9'))
		{
			foreach($url_target_names as $index => $url_target_name)
			{
				// in Tungsten 9, ignore this grouping if it's a right panel
				$url_target = $this->URLTargetWithName($url_target_name);
				if($url_target && $url_target->loadsInRightPanel())
				{
					unset($url_target_names[$index]);
				}
				
			}
		}
			
			// check if at least two ULR targets are provided
		// Disabled becuase the interface detects how many there are, not this
//		if(sizeof($url_target_names) < 2 )
//		{
//			return; // No need to show it, just ignore and get out
//		}

		$existing_group_found_index = false;
		// process url targets
		foreach($url_target_names as $index => $url_target_name)
		{
			$url_target = $this->URLTargetWithName($url_target_name);
			if(!$url_target)
			{
				TC_triggerError('Attempting to add a non-existent URL target <em>'.$url_target_name.'</em>  to the
			 submenu group in <em>'.get_called_class().'</em>. All Module URL Targets must be defined before grouping.');
				
			}
			
			// Detcct a class name for this set
			if(is_string($url_target->modelName()))
			{
				$grouping_class_name = $url_target->modelName();
			}
			
			// loop through the existing groupings to find a duplicate
			foreach($this->submenu_groupings as $test_group_index => $group_url_targets)
			{
				// check if url target already in another group
				if(in_array($url_target_name, $group_url_targets) )
				{
					$existing_group_found_index = $test_group_index;
					//TC_triggerError('Module URL Target with name <em>'.$url_target_name.'</em> is already in
					// another submenu group in <em>'.get_called_class().'</em>');
				}
			}	
			
		}
		
		

		if($existing_group_found_index !== false)
		{
			foreach($url_target_names as $url_target_name)
			{
				if(!in_array($url_target_name, $this->submenu_groupings[$existing_group_found_index]))
				{
					$this->submenu_groupings[$existing_group_found_index][] = $url_target_name;
				}
			}



		}
		else
		{
			// save the grouping
			if($grouping_class_name)
			{
				foreach($url_target_names as $name)
				{
					$this->submenu_groupings[$grouping_class_name][] = $name;
				}
				
			}
			else
			{
				// If there is no grouping model or name, 99% chance it's the top level
				// Needs to go at the very beginning since other models may have been added first
				// - MAIN LIST
				// - TMm_Model1
				// - TMm_Model2
				array_unshift($this->submenu_groupings, $url_target_names);
				
			}
			

		}


	}
	
	/**
	 * Remmoves a URL Target from any submenu groupings. This leaves it as a valid URL target but avoids it being
	 * show in the list of options.
	 * @param string $url_target_name
	 */
	public function removeURLTargetWithNameFromSubmenuGroupings($url_target_name)
	{
		
		// Remove Submenu Groupings
		foreach($this->submenu_groupings as $name => $grouping)
		{
			foreach($grouping as $index => $grouping_url_target)
			{
				if($grouping_url_target == $url_target_name)
				{
					unset($this->submenu_groupings[$name][$index]);
				}
			}
		}
	}
	
	
	//////////////////////////////////////////////////////
	//
	// SUBSECTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Turns on subsections which add another level of categorization to the navigation for the module. The subsections
	 * are broken down the the "parent" ULR targets which don't have another parent.
	 */
	public function enableNavigationSubsections()
	{
		$this->use_subsections = true;	
	}
	
	/**
	 * Turns off subsections which add another level of categorization to the navigation for the module. The subsections
	 * are broken down the the "parent" ULR targets which don't have another parent.
	 */
	public function disableNavigationSubsections()
	{
		$this->use_subsections = false;	
	}
	
	/**
	 * Process the URL Targets to determine the subsections that should be shown
	 */
	public function defineNavigationSubsections()
	{
		foreach($this->module_url_targets as $url_target_name => $url_target)
		{
			if($url_target->isSubsectionParent() && $url_target->userHasAccess())
			{
				$link = TSv_ModuleURLTargetLink::init($url_target);
				
				// Check if the currently viewed URL target is related to this top-level target
				if($this->currentURLTarget()->relatedToName($url_target_name))
				{
					$link->addClass('active_menu');
				}
				$this->subsection_menu->attachView($link);
			
			}
		}
	}

	//////////////////////////////////////////////////////
	//
	// HELP VIEWS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the help view container for a provided help view which is generated within a view that is loaded through
	 * a URL target
	 * @param TCv_View $help_view
	 * @return TCv_View
	 */
	public function helpViewContainer($help_view)
	{
		$help_view_container = new TCv_View('help_view_container');
		
			// add the heading
			$heading = new TCv_View();
			$heading->setTag('h2');
			$heading->addClass('help_view_title');
				$close_button = new TCv_Link('help_close_button');
				$close_button->addClass('fa-times');
				$close_button->setURL('#');
				$heading->attachView($close_button);
				$help_icon = new TCv_View();
				$help_icon->addClass('fa-question-circle');
				$heading->attachView($help_icon);
			
				$heading_content = new TCv_View('help_heading_content');
				$heading_content->addText('Help for this page');
				$heading_content->setTag('span');
				$heading->attachView($heading_content);
			$help_view_container->attachView($heading);
	
		// add the actual help view
		$help_view->addClass('help_view_contents');
		$help_view_container->attachView($help_view);
	
		return $help_view_container;
	}

	//////////////////////////////////////////////////////
	//
	// VIEWS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns any views to be shown
	 * @return array
	 */
	public function views()
	{
		$this->views['headers'] = array();
		$this->views['main_content'] = array();
		
		$return_type = $this->returnType();
		if(!$this->is_valid)
		{
			if($return_type == 'json')
			{
				$array = [
					'is_error'  => 1,
					''
				];
			}
			
			// If this is performing a round-trip action, it may expose the Tungsten interface
			if($method_name = $this->current_url_target->modelActionMethod())
			{
				if($return_type == 'json')
				{
					$array = [
						'is_error'  => 1,
						'message' => 'The action was not permitted for URL target: '.$this->current_url_target->name(),
					];
					print json_encode($array);
					exit();
				}
				
				if(isset($_SERVER['HTTP_REFERER']))
				{
					TC_messageConditionalTitle('The action was not permitted', false);
					TC_message('Details: '.$this->current_url_target->name(), false);
					header("Location: ".$_SERVER['HTTP_REFERER']);
					exit();
				}
			}
			
			$access_denied = TSv_AccessDenied::init('denied no view');
			$this->addConsoleError('Access Denied: Invalid');
			
			if($this->validation_error_message)
			{
				$access_denied->setExplanation($this->validation_error_message);
			}

			// If the return type is HTML
			if($return_type == 'view_html')
			{
				print $access_denied->htmlWithCSSandJS();
				exit();
			}


			$this->views['main_content'][] = $access_denied;

			return $this->views;
		}
		
		if(!$this->views_generated)
		{
			
			// Process the view for this URL target right at the start
			if($this->current_url_target->isView())
			{
				$model = TC_activeModelWithClassName($this->current_url_target->modelName());
				
				// Check if instance required but not found
				if($this->current_url_target->modelInstanceRequired() && !$model)
				{
					TC_triggerError('Failed attempt to create the view <em>'.$this->current_url_target->viewName().'</em>. The model <em>'.$this->current_url_target->modelName().'</em> with ID <em>'.$this->primary_id.'</em> does not exist.');
				}
				
				// Localization Switch
				// Adds in a switch for localization to test views in languages
				// this must happen before we generate the views so localization kicks in
				if(TC_getConfig('use_localization'))
				{
					// Only bother for page content views, regular Admin views don't need it
					
					if(TC_currentUser() && TC_classUsesTrait($this->current_url_target->viewName(),'TMt_PagesContentView'))
					{
						$language_selector = TMv_Localization_TungstenLanguageSelector::init($this->current_url_target);
						$this->views['main_content'][] = $language_selector;
					}
					
				}

				
				$this->current_url_target_view = $this->viewForURLTarget($this->current_url_target, $model);
				
				// Handle TCv_Forms That have a parent URL target, setting the success URL if not set
				if($this->current_url_target_view instanceof TCv_Form)
				{
					// check to see if the success URL is set
					if(!$this->current_url_target_view->successURL())
					{
						// has parent URL target
						if($this->current_url_target->parentURLTarget())
						{
							$path = $this->current_url_target->url();
							$this->current_url_target_view->setSuccessURL($path);
						}
						
					}
				}
			}

			// Deal with return_type
			if($this->current_url_target_view instanceof TCv_View)
			{
				$this->processPossibleReturnTypeWithValue($this->current_url_target_view);

			}



			// PROCESS CONTENT VIEWS
			if($this->use_subsections)
			{
				$this->defineNavigationSubsections();
				$this->views['headers'][] = $this->subsection_menu;
			}	
			
			
			
			// Breadcrumb Bar NAVIGATION
			// Only happens in Tungsten 8
			if(!TC_getConfig('use_tungsten_9'))
			{
				
				$breadcrumb_bar = $this->breadcrumbBar();
				
				
				// Handle Help Button
				if($this->current_url_target->isView())
				{
					// add the help view if it exists
					$help_view = $this->current_url_target_view->helpView();
					if($help_view instanceof TCv_View)
					{
						$help_button = new TCv_Link();
						$help_button->setID('help_toggle_button');
						//$help_button->addText('<i class="fa fa-question-circle" style="margin-left:4px;"></i>');
						$help_button->setIconClassName('fa-question-circle');
						$help_button->setURL('#');
						$help_button->setTitle('Show help for this page');
						
						$breadcrumb_bar->addRightView($help_button);
					}
				}
				
				// Handle settings view
				if($this->module->hasVariables())
				{
					$settings_url_target = clone($this->URLTargetWithName('settings'));
					if($settings_url_target->isRightButton())
					{
						$setting_button = TSv_ModuleURLTargetLink::init($settings_url_target);
						$setting_button->setTitle('Change settings for this module');
						$setting_button->detachAllViews();
						$setting_button->setIconClassName('fa-cog');
						$setting_button->addText('Settings');
						$breadcrumb_bar->addRightView($setting_button);
					}
				}
				
				$this->views['headers'][] = $breadcrumb_bar;
			}
			
			// SUBMENU BARS
			// Only happens in Tungsten 8
			if(!TC_getConfig('use_tungsten_9'))
			{
				$submenu_bars = $this->submenuBar();
				if($submenu_bars instanceof TSv_ModuleNavigation_SubmenuBar)
				{
					$submenu_bars = array($submenu_bars);
				}
				
				
				foreach($submenu_bars as $submenu_bar)
				{
					if($submenu_bar->numItems() > 0 || TC_getConfig('use_workflow'))
					{
						$this->views['headers'][] = $submenu_bar;
					}
					
				}
			}
			// HANDLE MODULE WARNING MESSAGE
			$message = $this->current_url_target->module()->warningMessage();
			if($message)
			{
				$message_view = new TCv_View('module_warning_message');
				$message_view->addText($message);

				if($button_color = $this->current_url_target->module()->buttonColor())
				{
					$message_view->setAttribute('style','background-color:'.$button_color);
				}
				$this->views['headers'][] = $message_view;

			}


			// MAIN CONTENT add main tungsten content
			foreach($this->processActionFromURL() as $view)
			{
				$this->views['main_content'][] = $view;
			}
			
			$this->views_generated = true;
				
		}
		return $this->views;
	
	}
	
	/**
	 * Renders the view as needed given the parameters provided
	 * @param TSm_ModuleURLTarget$url_target
	 * @param TCm_Model $model
	 * @return bool|TCv_View
	 */
	public function viewForURLTarget($url_target, $model)
	{
		// don't botther if it's not a view
		if($url_target->isView())
		{
			$parameter = false;
			
			// Model name and instance requirements
			if($url_target->modelName())
			{
				if($url_target->modelInstanceRequired())
				{
					$parameter = $model;
				}
				else
				{
					$parameter = $url_target->modelName();
					
				}
			}

			if($url_target->passIDIntoViewDirectly())
			{
				$parameter = $this->primary_id;
			}
			
			// Ensure there is a view name
			if($url_target->viewName() == '')
			{
				
				TC_triggerError('Module URL Target <em>"'.$url_target->id().'"</em> requires a view to be set using setViewName(). If this URL target is not meant to be a show a view, then the next URL Target must be set using setNextURLTarget(). ');
			}
			
			// ensure there's access before showing
			if(!$url_target->userHasAccess())
			{
				$access_denied = TSv_AccessDenied::init('no access');
				$this->addConsoleError('Access Denied: User not permitted');
		
				return $access_denied;
			} 
						

			$view_name = $url_target->viewName();
			if($parameter)
			{
				$view = $view_name::init($parameter);
			}
			else
			{
				$view = $view_name::init();
			}
			return $view;
		
		}
		return false;
	}
	
	
	/**
	 * Returns if the return-type for the current processing
	 * @return string
	 */
	protected function returnType()
	{
		// RETURN TYPES
		$return_type = $this->current_url_target->returnType();
		
		
		// Setting the return type for any request will trigger this without being defined in the URL Target
		if(
			(isset($_GET['return_type']) && $_GET['return_type'] == 'json')
			|| (isset($_POST['return_type']) && $_POST['return_type'] == 'json')
			|| (isset($_GET['id']) && $_GET['id'] == 'json')
			|| (isset($_GET['id_2']) && $_GET['id_2'] == 'json')
		)
		{
			$return_type = 'json';
		}
		elseif(
			(isset($_GET['return_type']) && $_GET['return_type'] == 'view_html')
			|| (isset($_POST['return_type']) && $_POST['return_type'] == 'view_html')
		)
		{
			$return_type = 'view_html';
		}
		elseif(
			(isset($_GET['return_type']) && $_GET['return_type'] == 'fullpage')
			|| (isset($_POST['return_type']) && $_POST['return_type'] == 'fullpage')
		)
		{
			$return_type = 'fullpage';
		}
		
		return $return_type;
		
		
	}
	
	protected function processPossibleReturnTypeWithValue($value) : void
	{
		$return_type = $this->returnType();
		
		// Method values just dump the value
		if($return_type == 'method_value')
		{
			print $value;
			exit();
		}
		
		
		
		if($return_type == 'json')
		{
			$json_response = false;
			if(is_array($value) )
			{
				$json_response = $value;
			}
			elseif($value instanceof TCv_View)
			{
				$json_response = $value->returnAsJSONDefaultValues();
				$json_response['html'] = $value->html();
				$json_response['css'] = $value->headCSS();
				$json_response['js'] = $value->headJS(false);
			}
			
			// Detect if we have a model, in which case we can certainly return the basic properties of that model
			elseif($value instanceof TCm_Model)
			{
				$json_response = [];
				$json_response['type'] = 'object';
				$json_response['class_name'] = get_class($value);
				$json_response['id'] = $value->id();
			}
			
			// Some value returned from the method
			else
			{
				$json_response = [];
				$json_response['value'] = $value;
			}

			// We have a response
			if($json_response)
			{
				header('Content-Type: application/json; charset=utf-8');
				print json_encode($json_response);
				exit();
			}


		}
		elseif($return_type == 'view_html')
		{
			if($value instanceof TCv_View)
			{
				if($value instanceof TCv_Form)
				{
					$value->setSuccessURL(false);
				}
				print $value->htmlWithCSSandJS();
				exit();
			}
		}
		
		// Fullpage which is used mostly for iframe loading
		// Only works in Tungsten 9
		elseif(TC_getConfig('use_tungsten_9') && $return_type == 'fullpage')
		{
			if($value instanceof TCv_View)
			{
				// We are already authenticated here, so it's just about loading the page
				// with the least amount of rendering or impact
				$website = new TSv_Tungsten9();
				$website->attachView(new TCv_Dialog()); // add the dialog
				$website->attachView($value);
				print $website->html();
			}
			exit();
		}

	}
	
	
	
	/**
	 * Defines the URL targets that will be shown in the Users Module. This must be an array of defined URL targets
	 * in the same way they are defined in any controller. The list will be added to the `edit` url target and
	 * require that a model name be provided of a TMm_User
	 *
	 * The URL targets that are returned will be added to the Users Module url targets and will be prefixed by the
	 * module name and a dash. So a url-target with the name `list` in the `cards` module will be added as
	 * `cards-list`.
	 * 
	 * @return TSm_ModuleURLTarget[]
	 */
	public static function defineURLTargetsForUsersModule()
	{
		return array();
	}
}
?>