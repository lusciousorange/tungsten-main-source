<?php
/**
 * Class TSc_SystemController
 */
class TSc_SystemController extends TSc_ModuleController
{
	/**
	 * TSc_SystemController constructor.
	 * @param int|string $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
		// manual set the model to the user. That's what the dashboard wants
		TC_saveActiveModel(TC_website()->user());
		
	//	$this->enableNavigationSubsections();
	
		
	}
	
	/**
	 * Define the list of available URL targets for this module
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets();
		$this->removeURLTargetWithName('create');
		$this->removeURLTargetWithName('delete');
		
		/** @var TSm_ModuleURLTarget $item */

		$item = TSm_ModuleURLTarget::init( 'module-toggle-show-in-menu');
		$item->setModelName('TSm_Module');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleShowInMenu');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('referrer');
		$item->setParentURLTargetWithName('list'); 
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'module-set-as-start-page');
		$item->setModelName('TSm_Module');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('setAsStartPage');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('referrer');
		$item->setParentURLTargetWithName('list'); 
		$this->addModuleURLTarget($item);
	
	
	
	
		$item = TSm_ModuleURLTarget::init( 'module-install');
		$item->setModelName('TSm_Module');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('install');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('referrer');
		$item->setParentURLTargetWithName('list'); 
		$this->addModuleURLTarget($item);
	
		$item = TSm_ModuleURLTarget::init( 'module-url-targets');
		$item->setModelName('TSm_Module');
		$item->setViewName('TSv_ModuleURLTargetList');
		$item->setModelInstanceRequired();
		$item->setTitle('URL targets');
		$item->setParentURLTargetWithName('edit'); 
		$this->addModuleURLTarget($item);
		
		//////////////////////////////////////////////////////
		//
		// SETTINGS
		//
		//////////////////////////////////////////////////////
		if(TC_getConfig('use_tungsten_9'))
		{
			// Default load for the system module moves to this settings list
			$this->URLTargetWithName('')->setNextURLTarget('settings-list');
			
			$settings = TSm_ModuleURLTarget::init('settings-list');
			$settings->setTitle('Module settings');
			$settings->setAsSubsectionParent();
			$settings->setIconCode('fa-sliders-h');
			$settings->setViewName('TSv_ModuleSettingsManager');
			
			$this->addModuleURLTarget($settings);
			
			$item = TSm_ModuleURLTarget::init( 'edit-settings');
			$item->setViewName('TSv_ModuleSettingsForm');
			$item->setModelName('TSm_Module');
			$item->setModelInstanceRequired();
			$item->setTitleUsingModelMethod('title');
			$item->setParentURLTargetWithName('settings-list');
			$this->addModuleURLTarget($item);
		}
		
		
		//////////////////////////////////////////////////////
		//
		// PLUGINS
		//
		//////////////////////////////////////////////////////
		
		/** @var TSm_ModuleURLTarget $plugins */
		$plugins = TSm_ModuleURLTarget::init( 'plugins');
		$plugins->setViewName('TSv_PluginListing');
		$plugins->setTitle('Plugins');
		if(TC_getConfig('use_tungsten_9'))
		{
			$plugins->setAsSubsectionParent();
		}
		
		$this->addModuleURLTarget($plugins);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-edit');
		$item->setViewName('TSv_PluginForm');
		$item->setModelName('TSm_Plugin');
		$item->setTitleUsingModelMethod('title');
		$item->setModelInstanceRequired();
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
	
		$item = TSm_ModuleURLTarget::init( 'plugin-toggle-enabled-on-public');
		$item->setModelName('TSm_Plugin');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleEnabledOnPublic');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-toggle-enabled-on-admin');
		$item->setModelName('TSm_Plugin');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleEnabledOnAdmin');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-toggle-enabled-on-live-site');
		$item->setModelName('TSm_Plugin');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleEnabledOnLiveSite');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-toggle-enabled-on-beta-site');
		$item->setModelName('TSm_Plugin');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleEnabledOnBetaSite');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-toggle-wait-for-rendering');
		$item->setModelName('TSm_Plugin');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleWaitForRendering');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-install');
		$item->setModelName('TSm_PluginList');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('installPlugin');
		$item->setPassValuesURLIntoActionMethod();
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'plugin-uninstall');
		$item->setModelName('TSm_PluginList');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('uninstallPlugin');
		$item->setPassValuesURLIntoActionMethod();
		$item->setNextURLTarget('plugins');
		$item->setParentURLTargetWithName('plugins');
		$this->addModuleURLTarget($item);
		
		
		$item = TSm_ModuleURLTarget::init( 'test-cron');
		$item->setViewName('TSv_CronTest');
		$this->addModuleURLTarget($item);
		
		
		
		
		// ! ----- IMPERSONATION -----

		$item = TSm_ModuleURLTarget::init( 'impersonate');
		$item->setViewName('TSv_UserImpersonationForm');
		$item->setTitle('Impersonate user');
		$item->setIconCode('fa-user-ninja');
		$item->setIsValid(TC_getConfig('use_impersonate_user'));
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'cancel-impersonation');
		$item->setModelName('TCv_Website');
		//$item->setModelInstanceRequired();
		$item->setModelActionMethod('cancelPublicImpersonation');
		$item->setNextURLTarget('referrer');
		$item->setAsSkipsAuthentication();
		$this->addModuleURLTarget($item);
		
		
		
		//////////////////////////////////////////////////////
		//
		// ACCESS CONTROL / USER GROUP
		//
		//////////////////////////////////////////////////////
		
		if(TC_getConfig('use_tungsten_9'))
		{
			// ABOUT
			$access = TSm_ModuleURLTarget::init('access');
			$access->setTitle('Access');
			//$access->setModelName('TCv_Website');
			//$access->setModelInstanceRequired();
			$access->setNextURLTarget('user-group-list');
			$access->setAsSubsectionParent();
			$this->addModuleURLTarget($access);
			
			$this->generateDefaultURLTargetsForModelClass(
				'TMm_UserGroup',
				'user-group',
				
			);
			
			$user_group_list = $this->URLTargetWithName('user-group-list');
			$user_group_list->removeAsSubsectionParent();
			$user_group_list->setParent($access); // must set the parent
			$user_group_list->setIconCode('fa-users-rectangle');
			
			
			// MODULE ACCESS
			$module_access = TSm_ModuleURLTarget::init( 'module-access');
			$module_access->setViewName('TSv_ModuleAccessList');
			$module_access->setTitle('Modules');
			$module_access->setIconCode('fa-cubes');
			$module_access->setParentURLTargetWithName('access');
			$this->addModuleURLTarget($module_access);
			
			// SUBMENU GROUPING FOR ACCESS
			$this->defineSubmenuGroupingWithURLTargets('user-group-list', 'module-access', 'impersonate' );
			
			// Change the module edit form to be a child of the module-access
		//	$this->URLTargetWithName('edit')->setParentURLTargetWithName('module-access');
			$this->URLTargetWithName('edit')->setParent($module_access);
			
			// Remove the list URL as an option, we show these in the access group instead
			$this->removeURLTargetWithName('list');
			
		}
		
		
		
		//////////////////////////////////////////////////////
		//
		// ABOUT SUBSECTION
		//
		//////////////////////////////////////////////////////
		
		// ABOUT
		$item = TSm_ModuleURLTarget::init( 'about');
		$item->setTitle('About');
		//$item->setModelName('TCv_Website');
		//$item->setModelInstanceRequired();
		$item->setNextURLTarget('version');
		$item->setAsSubsectionParent();
		$this->addModuleURLTarget($item);
		
		// LICENSE
		$item = TSm_ModuleURLTarget::init( 'license');
		$item->setViewName('TSv_License');
		$item->setTitle('License');
		$item->setIconCode('fa-gavel');
		$item->setParentURLTargetWithName('about');
		$this->addModuleURLTarget($item);
		
		// VERSION
		$item = TSm_ModuleURLTarget::init( 'version');
		$item->setViewName('TSv_TungstenVersion');
		$item->setTitle('Version');
		$item->setIconCode('fa-code-branch');
		$item->setParentURLTargetWithName('about');
		$this->addModuleURLTarget($item);
		
		//////////////////////////////////////////////////////
		//
		// SUBMENU GROUPS
		//
		//////////////////////////////////////////////////////
		if(TC_getConfig('use_tungsten_9'))
		{
			// About
			$this->defineSubmenuGroupingWithURLTargets('version', 'license' );
		}
		else
		{
			$this->defineSubmenuGroupingWithURLTargets('list', 'plugins', 'license', 'version', 'impersonate'); //
			// settings exists in two places, but also requries a module which breaks stuff
			$this->defineSubmenuGroupingWithURLTargets('edit', 'module-url-targets');
		}
		
		//////////////////////////////////////////////////////
		//
		// HISTORIES
		//
		//////////////////////////////////////////////////////
		
		// REVERT
		// Requires the parameters to be passed in
		// Class_name
		// history_id
		// column
		$target = TSm_ModuleURLTarget::init( 'history_revert');
		$target->setModelName('TSm_ModelHistoryState');
		$target->setAsStaticFunctionCall();
		//$target->setModelInstanceIDRequired(false);
		$target->setTitle('Revert');
		$target->setIconCode('fa-code-branch');
		$target->setPassValuesIntoActionMethodType('url');
		$target->setModelActionMethod('revert');
		$target->setNextURLTarget('referrer');
		$target->addValidation('current-user', 'isAdmin');
		
		$this->addModuleURLTarget($target);
		
		
		//////////////////////////////////////////////////////
		//
		// MEMCACHED
		//
		//////////////////////////////////////////////////////
		
		$item = TSm_ModuleURLTarget::init( 'clear-module-memcached');
		$item->setModelName('TSm_Module');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('clearAllModelMemcached');
		$item->setTitleUsingModelMethod('title');
		$item->setNextURLTarget('referrer');
		//$item->setParentURLTargetWithName('list');
		$this->addModuleURLTarget($item);
		
		
	}





}
?>