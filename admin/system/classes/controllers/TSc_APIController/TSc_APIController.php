<?php
/**
 * The controller for all APIs.
 *
 * Enabling APIs must happen in TC_config via the `use_api` setting. None of this will work without it.
 *
 * API calls are organized by module and all start with `/t-api/module_name/`.
 *
 * HTTP Request Methods
 * ----------------------------
 * Every request must use the correct HTTP method used to indicate the type of action.
 * GET retrieves information on a single item or list of items
 * POST create a new item
 * PATCH updates an item
 * DELETE will remove an item
 *
 * Defining API Endpoints
 * ----------------------------
 * All api endpoints must be explicitly defined in the modules `/config/api.php` file. The format for these is
 * specific and consists of an array of values called `$api` each of which contains an API definition.
 * http_method : {string} the http method used
 * path : {string] the additional string after the module name that can be looked for. If an ID is to be provided,
 * then it should be wrapped in `{your_id}`
 * model : {string] The name of the model that we're interacting with
 * function : {string] The name of the method on that model that will be called.
 *
 * There are default methods that are called for different HTTP requests and the `function` value can be skipped
 *
 * GET => apiValues
 * POST => createWithValues (static call)
 * PATCH => updateWithValues
 *
 * Automated Validation
 * ----------------------------
 * Methods that access information must pass userCanView() validation.
 * Models that are called or returned for values must implement the TSt_ModelWithAPI trait
 *
 */

class TSc_APIController extends TCc_Controller
{
	protected $method; // GET, POST, ETC
	protected $module_name; // The admin module this data is connected to
	protected $model_name;
	protected $path;
	protected $error_code = false;
	protected $api_key;
	
	/** @var TSm_Module $module */
	protected $module;

	/** @var TMm_User $user */
	protected $user;
	
	/** @var TCm_Model $model The model that is being called  */
	protected $model;
	
	/** @var array $called_api Stores the called api configuration  */
	protected $called_api;
	
	/** @var array $parameters The parameters that are passed in */
	protected $parameters;
	
	protected $caller;
	
	const ERRORS = array(
		'x-no-api' => array('message' => 'API not enabled', 'http_status' => '403 Forbidden'),
		'x-no-mod' => array('message' => 'Module not found', 'http_status' => '422 Unprocessable Entity'),
		'x-mod-no-api' => array('message' => 'Module does not have api', 'http_status' => '422 Unprocessable Entity'),
		'x-no-key' => array('message' => 'Missing API Key', 'http_status' => '401 Unauthorized'),
		'x-bad-key' => array('message' => 'Invalid API Key', 'http_status' => '401 Unauthorized'),
		'x-user-api-access' => array('message' => 'User API access denied', 'http_status' => '401 Unauthorized'),
		'x-bad-api-url' => array('message' => 'Invalid URL', 'http_status' => '405 Method Not Allowed'),
		'x-model-not-found' => array('message' => 'Model with ID not found', 'http_status' =>	'412 Precondition Failed'),
		'x-invalid-parameter' => array('message' => 'Parameter is not permitted', 'http_status' => '406 Not Acceptable'),
		'x-view-denied' => array('message' => 'View access denied', 'http_status' => '412 Precondition Failed'),
		'x-edit-denied' => array('message' => 'Edit access denied', 'http_status' => '412 Precondition Failed'),
		'x-delete-denied' => array('message' => 'Delete access denied', 'http_status' => '412 Precondition Failed'),
		'x-create-denied' => array('message' => 'Create access denied', 'http_status' => '412 Precondition Failed'),
		'x-method-not-found' => array('message' => 'Method does not exist for model', 'http_status' => '412 Precondition Failed'),
		'x-validation-fail' => array('message' => 'Validation access failed', 'http_status' => '412 Precondition Failed'),
		'x-class-trait-error' => array('message' => 'Class not API accessible', 'http_status' => '406 Not Acceptable'),
		'x-db-error' => array('message' => 'Database error', 'http_status' => '406 Not Acceptable'),
		'x-process-error' => array('message' => 'Processing error', 'http_status' => '406 Not Acceptable'),
		'x-field-error' => array('message' => 'Field validation error', 'http_status' => '406 Not Acceptable'),
		'x-class-id-missing-in-url' => array('message' => 'No ID found in URL with model ID name', 'http_status' => '406 Not Acceptable'),
	
	);
	
	/**
	 * TSc_APIController constructor.
	 */
	public function __construct()
	{
		parent::__construct('api_controller');
	
		// No matter what, we return JSON
		header('Content-Type: application/json');
		
		$this->validateAPIEnabled();
		
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->module_name = $_GET['module'];
		$this->path = trim($_GET['path'], '/ ');
		$this->module = TSm_Module::init($this->module_name);
		
		
		$this->validateAPIPath();
		
		$this->validateAPIKey();
		
	}
	
	/**
	 * Validates if the API feature toggle is enabled.
	 * @see TC_config.php => use_api
	 */
	public function validateAPIEnabled()
	{
		// validate if the API is even accessible
		if(!TC_getConfig('use_api'))
		{
			$this->generateError('x-no-api');
		}
		
	}
	
	/**
	 * Method to validate the path provided. This performs checks on the module and if the path contains valid values.
	 */
	public function validateAPIPath()
	{
		if(!TC_moduleWithNameInstalled($this->module_name))
		{
			$this->generateError('x-no-mod', array('module_name' => $this->module_name));
		}
		
		
		if(!$this->module->usesAPI())
		{
			$this->generateError('x-mod-no-api', array('module_name' => $this->module_name));
		}
		
	}
	
	/**
	 * Method to acquire a model with a provided API key. This returns a user by
	 * @param string $api_key
	 * @return TCm_Model|null
	 */
	protected function getUserModelWithAPIKey(string $api_key) : ?TMm_User
	{
		/** @var class-string<TMm_User> $class_name */
		$class_name = TMm_User::classNameForInit();
		return $class_name::userWithAPIKey($api_key);
	}
	
	
	
	/**
	 * Validates the provided API key. If a user is found, then they are set to be logged in so that future calls to
	 * userCanEdit() and userCanView() are respected.
	 */
	public function validateAPIKey() : void
	{
		$headers = getallheaders();
		
		// Missing API Key
		if(!isset($headers['Api-Key']))
		{
			$this->generateError('x-no-key');
		}
		
		$this->api_key = $headers['Api-Key'];
		$this->user = $this->getUserModelWithAPIKey($this->api_key);
		if(!$this->user)
		{
			$this->generateError('x-bad-key');
		}
		
		if(!$this->user->hasAPIAccess())
		{
			$this->generateError('x-user-api-access');
		}
		
		$this->trackAPICall();
		
		// Set the User ID as logged in for this call
		$this->user->setAsLoggedIn();
		
	}
	
	
	
	/**
	 * @param string $error_code The error code which references the ERRORS value
	 * @param array $details Any additional details
	 */
	protected function generateError(string $error_code, $details = array())
	{
		$error_values = self::ERRORS[$error_code];
		
		$this->error_code = $error_code;
		
		header('HTTP/1.1 '.$error_values['http_status']);
		
		$values = array(
			'error_message' => $error_values['message'],
			'error_code' => $this->error_code,
		
		
		);
		$values = $values + $details;
		
		print json_encode($values);
		$this->trackAPICall();
		
		// Destroy the session to avoid carry-over
		session_destroy();
		exit();
	}
	
	/**
	 * Method to find the API settings based on the values provided. Generates errors if it can't find the API setting.
	 */
	function parseAPICall()
	{
		// Comparing paths uses generic # instead of actual numbers
		// The called path would be /module/#/folder instead of /module/45/folder
		// The api_settings would be /module/#/folder instead of /module/{something_id}/folder
		$api_settings = $this->module->apiSettings();
		// Convert the path called into a generic version with # replacing actual IDs
		$called_generic_path = preg_replace('/\d+/i', '#', $this->path);
		foreach($api_settings as $api_config)
		{
			$api_config['path'] = trim($api_config['path'],'/ ');// trim off slashes
			$api_generic_path = preg_replace('/{\w+}/i', '#', $api_config['path']);
			
			// At this point, we're already in the right module
			// Match the method, and match the path
			if(strtoupper($api_config['http_method']) == $this->method // same HTTP method
				&& $called_generic_path == $api_generic_path)
			{
				$this->called_api = $api_config;
				break; // exit the loop
			}
		}
		
		// No API call found
		if($this->called_api == null)
		{
			$this->generateError('x-bad-api-url',
			                    array(
			                    	'module_name' => $this->module_name,
									'details' => 'URL does not exist with '.$this->method.' and that configuration'));
		}
		
		
		$this->saveParameters();
	
		$this->saveModel();
		
		$this->validateAPICall();
		
		$this->saveCaller();
		
		
		// ACTUALLY MAKE THE CALL
		$response = call_user_func(array($this->caller, $this->called_api['function']), $this->parameters);
		
		// HANDLE ANY PROCESS ERRORS
		foreach(TC_processErrors() as $error_values)
		{
			$error_array = [
				'model' => ($this->model_name)::$model_title,
				'message' => $error_values['message']
			];
			
			if($error_values['field_id'] != null)
			{
				$error_array['field_id'] = $error_values['field_id'];
			}
			
			$type = $error_values['type'];
			if($type == 'db_error')
			{
				$this->generateError('x-db-error',$error_array);
			}
			elseif($type == 'field_validation')
			{
				$this->generateError('x-field-error',$error_array);
			}
			else // processing error
			{
				$this->generateError('x-process-error',$error_array);
			}
			
			
		}

		// Handle response, any issues would have already generated errors at this point
		
		// PATCH responses should always be the model that was patched
		if($this->method == 'PATCH')
		{
			$response = $this->model;
		}
		elseif($this->method == 'DELETE')
		{
			$response = ['deleted' => true];
			
			if(($this->caller instanceof TCm_Model)  && $this->caller->id())
			{
				$response[($this->caller)::$table_id_column] = $this->caller->id();
			}
			
		}
		else // Process the returned values to handle nesting
		{
			// Process the response to deal with returned values
			$response = $this->processCallResponse($response);
			
			
		}
		
		// Deal with a model being returned
		if($response instanceof  TCm_Model)
		{
			// Check if the returned model uses the trait
			// It can be returned from anywhere in code, so verify
			if(TC_classUsesTrait($response, 'TSt_ModelWithAPI'))
			{
				$response = $response->apiValues();
			}
			else
			{
				$this->generateError('x-class-trait-error',[ 'model' => get_class($response)]);
			}
			
		}
		
		print json_encode($response);

		// End of call, log out, destroy the session
		session_destroy();
	}
	
	/**
	 * Saves the parameters passed, which only are required for POST and PATCH
	 */
	protected function saveParameters()
	{
		// Handle parameters that are passed in
		$this->parameters = [];
		if($this->method == 'POST')
		{
			$this->parameters = $_POST;
		}
		elseif($this->method == 'PATCH')
		{
			$this->parseRawHTTPRequest(); // convert the data into the parameters
		}
		
		// Compare the path we have in the called path with the one we loaded
		// Find the variables that are marked as {variable-name} and save them to the parameters
		// Matching up the order based on the exploded URLs
		
		$called_parts = explode('/',$this->called_api['path']);
		$this_url_parts = explode('/',$this->path);
		
		foreach($called_parts as $index => $name)
		{
			if(substr($name, 0, 1) == '{')
			{
				$name = str_replace(['{','}'], '',$name);
				$this->parameters[$name] = $this_url_parts[$index];
			}
		}
		
	}
	
	/**
	 * Saves the caller. Requires a set model to exist
	 */
	protected function saveCaller()
	{
		$this->caller = $this->model; // model exists AFTER the validated API call
		
		// Handle Determine the caller
		if($this->method == 'POST')
		{
			if(!$this->caller)
			{
				$this->caller = $this->model_name;
			}
		}
		elseif($this->method == 'PATCH')
		{
			$this->caller = $this->model;
		}
		elseif($this->method == 'DELETE')
		{
			$this->caller = $this->model;
		}
		
		if(is_null($this->caller))
		{
			$this->generateError('x-model-not-found', ['model_name' => $this->model_name]);
		}
		
	}
	
	
	/**
	 * Finds the model to be loaded which is the primary thing we interact with on an API call
	 */
	protected function saveModel() : void
	{
		$this->model_name = trim($this->called_api['model']);
		
		// Track if we are looking at the current user, get out after we find it
		if($this->model_name == 'current-user' || $this->model_name == 'current_user')
		{
			$this->model_name = 'TMm_User';
			$this->model = TC_currentUser();
			
		}
		
		// Make sure to get any override classes so that validation checks those too
		$this->model_name = $this->overrideClassName($this->model_name);
		
		
		// Ensure model uses API
		if(!TC_classUsesTrait($this->model_name,'TSt_ModelWithAPI'))
		{
			$this->generateError('x-class-trait-error',[ 'model' => $this->model_name , 'location' => 'saveModel']);
		}
		
		// Deal with calling for a controller or a model list
		// Error if we can't find it
		if(	($this->model_name)::isSingleton() )
		{
			$this->model = ($this->model_name)::init();
			if(!$this->model)
			{
				$this->generateError('x-model-not-found',[ 'model' => $this->model_name , 'location' => 'saveModel']);
			}
		}
		
		
		
		// NO MODEL FOUND YET
		// We need an ID, which must have a matching ID in the URL to the id column for the model
		// If we don't have a model yet (possible set as current user), then we try and find it
		if(!$this->model)
		{
			// Check if a parameter passed in is the ID of the model
			if(!isset($this->parameters[$this->model_name::$table_id_column]))
			{
				$this->generateError('x-class-id-missing-in-url',[ 'model' => $this->model_name , 'location' => 'saveModel']);
				
			}
			
			// Save the model ID, then remove it from the parameters since it's not passed in.
			$model_id = $this->parameters[$this->model_name::$table_id_column];
			unset($this->parameters[$this->model_name::$table_id_column]);
			
//
//			// Try to instantiate the model if there's an ID
//			preg_match_all('/\d+/i', $this->path, $ids);
//			$ids = $ids[0]; // get the first ones
//			if(isset($ids[0]))
//			{
//				$model_id = $ids[0];
//
				// Try to instantiate the model
				$this->model = ($this->model_name)::init($model_id);
				if(!$this->model)
				{
					$this->generateError('x-model-not-found',
					                     ['model' => $this->model_name, 'id' => $model_id]);
				}
			//}
		}
	}
	
	/**
	 * Validates the API call based on the properties of that API
	 */
	protected function validateAPICall()
	{
		$http_method = $this->called_api['http_method'];
		
		$model_title = $this->model_name;
		if(method_exists($this->model_name,'modelTitleSingular'))
		{
			$model_title = ($this->model_name)::modelTitleSingular();
		}
		
		
		// Not every model being called with necessarily have a schema
		if(method_exists($this->model_name,'schema'))
		{
			// Check for properties that are expressly rejected in the schema
			// Those can't be provided as a parameter
			foreach($this->model_name::schema() as $field_id => $schema_field_values)
			{
				if(isset($schema_field_values['is_api_param'])
					&& !$schema_field_values['is_api_param']
					&& isset($this->parameters[$field_id]))
				{
					$this->generateError('x-invalid-parameter', ['field_id' => $field_id]);
				}
			}
		}
		
		// DEAL WITH DEFAULT FUNCTION VALUES
		
		// POST => createWithValues
		if(!isset($this->called_api['function']) && $http_method == 'POST')
		{
			$this->called_api['function'] = 'createWithValues';
		}
		
		// GET => apiValues
		if(!isset($this->called_api['function']) && $http_method == 'GET' && $this->model instanceof TCm_Model)
		{
			$this->called_api['function'] = 'apiValues';
		}
		
		// PATCH => updateWithValues
		if(!isset($this->called_api['function']) && $http_method == 'PATCH' && $this->model instanceof TCm_Model)
		{
			$this->called_api['function'] = 'updateWithValues';
		}
		
		// DELETE => delete
		if(!isset($this->called_api['function']) && $http_method == 'DELETE' && $this->model instanceof TCm_Model)
		{
			$this->called_api['function'] = 'delete';
		}
		
		// Otherwise, we need a function defined
		if(!isset($this->called_api['function']))
		{
			$this->generateError('x-method-not-found',
			                     [
			                     	 'method_name' => TC_localize('no_api_function', 'No function defined in API'),
				                     'location' => 'validateAPICall : non-standard function '
			                     ]);
		}
		
		// Just in case they add the brackets
		$this->called_api['function'] = str_replace(['(',')'],'',$this->called_api['function']);
		$this->addConsoleDebug($this->called_api['function']);
		
			// Even if a function exists, check that it exists on the model provided
		if(!method_exists($this->model_name,$this->called_api['function']))
		{
			$this->generateError('x-method-not-found',
			                     [
			                     	 'model' => $model_title,
				                     'method_name' => $this->called_api['function'],
				                     'flag' => 'no',
				                     'location' => "validateAPICall : method name does not exist",
			                     ]);
		}
		
		// Call the default userCanX() methods
		// These are part of the bigger system and should be respected for consistency
		
		if($http_method == 'POST' && !$this->model_name::userCanCreate())
		{
			$this->generateError('x-create-denied');
		}
		
		if($http_method == 'GET' && !$this->model->userCanView())
		{
			$this->generateError('x-view-denied');
		}
		
		if($http_method == 'PATCH' && !$this->model->userCanEdit())
		{
			$this->generateError('x-edit-denied');
		}
		
		if($http_method == 'DELETE' && !$this->model->userCanDelete())
		{
			$this->generateError('x-delete-denied');
		}
		
		
		// Call the API validation methods
		// These are different than the schema validation methods
		// These methods indicate if the API can even be called,
		if(isset($this->called_api['validations']) && is_array($this->called_api['validations']))
		{
			foreach($this->called_api['validations'] as $validation_method)
			{
				if(!method_exists($this->model_name,$validation_method))
				{
					$this->generateError('x-method-not-found',
					                     [
						                     'model' => $model_title,
						                     'method_name' => $validation_method
					                     ]);
				}
			
				// call the validation method
				$response = call_user_func_array(array($this->model, $validation_method), []);
				
				// validation failed
				if(!$response)
				{
					$this->generateError('x-validation-fail',
					                     [
						                     'model' => $model_title,
						                     'method_name' => $validation_method
					                     ]);
				}
				
			}
		}
	}
	
	/**
	 * Parses a response to convert any TCm_Model values to their api values. This is a recurive method that
	 * determines when an object is passed and returns teh API values instead. This method will also remove keys in
	 * arrays when the entire array has models.
	 *
	 * This method can be unpredictable IF an array exists with some keys and not others
	 * @param array|TCm_Model $response
	 * @return array
	 * @see TSt_ModelWithAPI
	 */
	protected function processCallResponse($response) : array
	{
		$all_objects_in_array = true;
		if(is_array($response))
		{
			foreach($response as $key => $value)
			{
				// If we have a model, replace it with it's API values
				if($value instanceof TCm_Model)
				{
					// If we're showing a model in an API response, it needs to have the trait
					// We can never allow a model to be returned as a value if it doesn't have the permission to be
					// shown in the API
					if(!TC_classUsesTrait($value,'TSt_ModelWithAPI'))
					{
						$this->generateError('x-class-trait-error',[ 'model' => (get_class($value))::$model_title ]);
					}
					
					$response[$key] = $this->processCallResponse($value->apiValues());
				}
				elseif(is_array($value))
				{
					$response[$key] = $this->processCallResponse($value);
					$all_objects_in_array = false;
				}
				else // Any other value, we can't strip the indices
				{
					$all_objects_in_array = false;
				}
			}
		}
		
		// We identfied an array of objects, which means the IDs are in there and this should just be a regular array
		if($all_objects_in_array)
		{
			$response = array_values($response);
		}
		
		return $response;
	}
	
	/**
	 * Tracks the API call to our system for historical purposes.
	 */
	protected function trackAPICall()
	{
		$values = array();
		$values['error_code'] = $this->error_code;
		$values['module_name'] = $this->module_name;
		$values['http_method'] = $this->method;
		$values['path'] = $this->path;
		$values['address'] = $_SERVER['REMOTE_ADDR'];
		$values['api_key'] = $this->api_key;
		
		// Track the source value if provided
		if(isset($_SERVER['HTTP_API_SOURCE']))
		{
			$values['source'] = $_SERVER['HTTP_API_SOURCE'];
		}
		
		if($this->user)
		{
			$values['user_id'] = $this->user->id();
		}
		
		TSm_APICallHistory::createWithValues($values);
		
	}
	
	/**
	 * A method to take in the values in the raw encoding of form data (not x-www-form-urlencoded) and convert it
	 * into an array of values. This is used for HTTP methods that don't have global superarrays such as PATCH and PUT.
	 *
	 * In these cases, we need to manually parse the boundary that is provided and modify the provided array to
	 * represent what we'd normally get with $_POST and $_GET.
	 * @see https://stackoverflow.com/questions/5483851/manually-parse-raw-multipart-form-data-data-with-php
	 */
	protected function parseRawHTTPRequest()
	{
		// read incoming data
		$input = file_get_contents('php://input');
		
		// grab multipart boundary from content type header
		preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
		$boundary = $matches[1];
		
		// split content by boundary and get rid of last -- element
		$a_blocks = preg_split("/-+$boundary/", $input);
		array_pop($a_blocks);
		
		// loop data blocks
		foreach ($a_blocks as $id => $block)
		{
			if (empty($block))
				continue;
			
			// you'll have to var_dump $block to understand this and maybe replace \n or \r with a visibile char
			
			// parse uploaded files
			if (strpos($block, 'application/octet-stream') !== FALSE)
			{
				// match "name", then everything after "stream" (optional) except for prepending newlines
				preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
			}
			// parse all other fields
			else
			{
				// match "name" and optional value in between newline sequences
				preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
			}
			$this->parameters[$matches[1]] = $matches[2];
		}
	}
	
}