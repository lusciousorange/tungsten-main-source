<?php
trait TSt_ModelWithAPI
{
	// The main method used is apiValues() which is defined in TCm_Model but only works if this trait is applied
	// The method exists in TCm_Model so that it can be safely extended by all classes
}