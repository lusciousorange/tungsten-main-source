<?php

/**
 * Trait TMt_ContentFlaggable
 *
 * A trait with all the functions related to having a model be a content TMt_ContentFlaggable. This is separated out for
 * convenience.
 */
trait TMt_ContentFlaggable
{
	/**
	 * @var TMm_ContentFlag[] $content_flags
	 * The array of flags for this content item which are lazy-loaded when requested
	 */
	protected $content_flags;
	
	/**
	 * Checks all the flags for this model and returns if it has any flags
	 * @return bool
	 */
	public function hasContentFlags()
	{
		return $this->numContentFlags() > 0;
	}
	
	/**
	 * Returns how many content flags exist
	 * @return int
	 */
	public function numContentFlags()
	{
		return count($this->contentFlags());
	}
	
	/**
	 * A hook method to implement custom content flags that are unique to this model. These will be included with the
	 * normal flags for the item
	 * @return array
	 */
	public function customContentFlags()
	{
		return [];
	}
	
	/**
	 * Returns all the content flags for this model
	 * @return TMm_ContentFlag[]
	 */
	public function contentFlags()
	{
		if($this->content_flags == null)
		{
			$this->content_flags = [];
			
			foreach(static::schema() as $field_name => $values)
			{
				if(isset($values['content_flags']) && is_array($values['content_flags']))
				{
					// Loop through each method and call them, passing in the field name
					foreach($values['content_flags'] as $flag_method)
					{
						if(method_exists($this, $flag_method))
						{
							
							// Call the method and if it returns a TMm_ContentFlag, save it
							$result = $this->$flag_method($field_name);
							if($result instanceof TMm_ContentFlag)
							{
								$this->content_flags[] = $result;
							}
						}
						
					}
					
				}
			}
			
			$this->content_flags = array_merge($this->content_flags, $this->customContentFlags());
		}
		return $this->content_flags;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FLAGGING METHODS
	//
	// These are common methods used for flagging which are
	// available to any method that uses flagging.
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Flags if the value in a particular field is empty
	 * @param string  $field_name
	 * @return false|TMm_ContentFlag
	 */
	public function flagIsFieldEmpty(string $field_name)
	{
		if(trim($this->$field_name) == '')
		{
			$user_friendly_fieldname = str_ireplace(['-','_'],' ', $field_name);
			return new TMm_ContentFlag('empty-'.$field_name,$user_friendly_fieldname. ' is empty');
		}
		
		return false;
	}
}