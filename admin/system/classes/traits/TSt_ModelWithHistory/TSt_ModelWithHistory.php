<?php

/**
 * The trait applied to models that use history. Adding this trait requires an install of the module for this model,
 * to ensure that the triggers are installed and the history table is created.
 */
trait TSt_ModelWithHistory
{
	protected ?array $history_states = null;
	
	// Tracks the history states, using version codes to group items together
	/** @var TSm_ModelHistoryState[] $history_states_by_version */
	protected array $history_states_by_version = [];
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates the columns that should not be included as part of the history. Note that this has an impact on
	 * restoration because we cannot restore values that are missing.
	 * @return array
	 */
	public static function hook_historyIgnoredColumns() : array
	{
		return [];
	}
	
	/**
	 * Indicates all the columns that are ignored for this class, this takes into account name. This accounts for
	 * common columns for many
	 * classes.
	 * @return array
	 */
	public static function historyAllIgnoredColumns() : array
	{
		// Get all the ignored columns, then include the date fields on every model
		$ignored_columns = static::hook_historyIgnoredColumns();
		
		return $ignored_columns;
	}
	
	/**
	 * Indicate if this model should track deletions with histories.
	 * @return bool
	 */
	public static function historyTrackDeletions() : bool
	{
		return true;
	}
	
	/**
	 * Returns the array of related models that are associated with this model for history purposes. This is usually
	 * used to show the complete history of changes to a model, by pulling in history from other related models.
	 * For example, changing the user groups for a TMm_User shows up in the user's history. News categories are
	 * another example.
	 * @return array[]
	 */
	public static function historyRelatedModels() : array
	{
		return [
			// Add the model that should be aded to this history
//			[
//				'model_name' => 'TMm_RelatedModel',
//				'reference_column_name' => 'this_model_id_column_name',
//				'heading_display_column_id'     => 'category_id', // or 'title', etc uses schema to determine what to show
//				'heading_display_title'  => 'Category match', // the name for this item

//			],
		];
	}
	
	
	
	/**
	 * Returns the array of model history states for this model.
	 * @return TSm_ModelHistoryState[]
	 */
	public function historyStates() : array
	{
		if(is_null($this->history_states))
		{
			$this->history_states = [];
			
			$this->processPrimaryModelHistoryStates();
			
			// RELATED MODELS
			foreach(static::historyRelatedModels() as $related_model_values)
			{
				$this->processRelatedHistoryModel($related_model_values);
			}
			
			// MODELS WITH PAGE BUILDERS
			// Deal with detecting models that also have page content
			// Ignore for page menu items, since that's already set
			if(static::hasTrait('TMt_PageRenderer')
				&& !is_subclass_of($this, 'TMm_PagesMenuItem'))
			{
				$this->processRelatedHistoryModel(
				[
					'model_name' => 'TMm_PageRendererContent',
					'reference_column_name' => 'item_id',
					'heading_display_column_id' => 'view_class',
					'heading_display_title' => 'Content',
					'additional_filter_where' => 't.item_class_name = "'.get_called_class().'"',
				]);
			}
			
			// MODELS WITH 301 REDIRECTS
			// Ignore for page menu items, since that's already set
			if(static::hasTrait('TMt_Page301Redirectable')
				&& !is_subclass_of($this, 'TMm_PagesMenuItem'))
			{
				$this->processRelatedHistoryModel(
					[
						'model_name' => 'TMm_Pages301Redirect',
						'reference_column_name' => 'item_id',
						'heading_display_column_id' => ' ',
						'heading_display_title' => '301 Redirect',
						'additional_filter_where' => 't.class_name = "'.get_called_class().'"',
					]);
			}
		
		}
		
		
		// Sort by absolute date
		ksort($this->history_states);
		return $this->history_states;
		
		
		
	}
	
	/**
	 * Gets the history states for THIS model which comes directly from its history
	 * @return void
	 */
	protected function processPrimaryModelHistoryStates() : void
	{
		// Track the newer state as we work backwards in the history, getting to the start
		$previous_state = null;
		$insert_found = false;
		
		$is_first = true;
		
		// Get all the history rows for this table, starting with the most recent
		// Work backwards in time, since this might be enabled on existing systems, so it's an incomplete
		// history
		$query = "SELECT * FROM `_history_" . static::tableName()
			. "` WHERE " . static::$table_id_column . " = :id  ORDER BY _history_timestamp ASC";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		
		// No results, create the initial insert
		if($result->rowCount() == 0)
		{
			$first_state = $this->createModelInsertHistoryState();
			$this->trackHistoryState($first_state);
		}
		
		
		// Iterate, moving back in time to the beginning
		while($row = $result->fetch())
		{
			// Deal with bad dates, we just can't have those in the system
			// Just skip over them entirely
			if(substr($row['_history_timestamp'], 0,4) == '0000')
			{
				continue;
			}
			
			// Deal with the first row, which should be the creation
			if($is_first)
			{
				// Deal with missing insert
				if($row['_history_action'] != 'insert')
				{
					$first_state = $this->createModelInsertHistoryState();
					$this->trackHistoryState($first_state);
					$previous_state = $first_state;
				}
				
				$is_first = false;
			}
			
			// Create the history state
			$state = new TSm_ModelHistoryState(get_called_class(), $row);
			$state->trackChangesAgainstPreviousState($previous_state);
			
			// Track if there were multiple updates in the same version.
			// Likely a script performing multiple queries. Group them as a single change
			if(isset($this->history_states_by_version[$state->version()]))
			{
				// Append all the changes to the existing state
				$this->history_states_by_version[$state->version()]->addChanges($state->changes());
				
			}
			else // New version, track as a separate state
			{
				// Track version if necessary
				if(is_string($state->version()))
				{
					$this->history_states_by_version[$state->version()] = $state;
				}
				
				$this->trackHistoryState($state);
			}
			
			
			
			// Track this state as the next state so that it keeps checking as we move backwards in time
			$previous_state = $state;
		}
	}
	
	/**
	 * Processes the related models which are associated with this main model, but the changes need to be
	 * incorporated into it.
	 * @param array $related_model_values
	 * @return void
	 */
	protected function processRelatedHistoryModel(array $related_model_values) : void
	{
		// Reset the previous states, which are based on the primary keys of the table
		// Every time we come across one, we track the previous state in an array
		$previous_related_states = [];
		
		// Track the list of IDs for related models. This is used later when if we need to deal with third level
		// related models since it needs to lookup based on those IDs.
		$related_model_ids = [];
		
		// Get the values which don't change for this model
		//$this->addConsoleDebug($related_model_values);
		$model_name = $related_model_values['model_name'];
		$related_table_name = $model_name::tableName();
		$primary_key_column_id = $model_name::$table_id_column;
		
		$reference_column_name = $related_model_values['reference_column_name'];
		
		$heading_display_column_id = $primary_key_column_id;
		
		if(isset($related_model_values['heading_display_column_id']))
		{
			$heading_display_column_id = $related_model_values['heading_display_column_id'];
			
		}
		
		// FIND MISSING INSERT HISTORIES
		// left join the main table with the histories, flagging missing inserts
		$query = "SELECT t.*, ISNULL(h.".$primary_key_column_id.") as missing_insert_history
				FROM ".$related_table_name." t LEFT JOIN _history_".$related_table_name." h
		ON(t.".$primary_key_column_id." = h.".$primary_key_column_id." AND h._history_action = 'insert' ) WHERE t.".$reference_column_name." = :id";
		if(isset($related_model_values['additional_filter_where']))
		{
			$query .= ' AND '.$related_model_values['additional_filter_where'];
			
		}
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		while($row = $result->fetch())
		{
			if($row['missing_insert_history'])
			{
				$model = $model_name::init($row);
				$first_state = $model->createModelInsertHistoryState();
				$previous_related_states[$model->id()] = $first_state;
			}
		}
		
		$query = "SELECT * FROM `_history_" . $related_table_name .'` t '
			. " WHERE " . $reference_column_name . " = :id";
		
		if(isset($related_model_values['additional_filter_where']))
		{
			$query .= ' AND '.$related_model_values['additional_filter_where'];
			
		}
		
		$query .= "  ORDER BY _history_timestamp ASC";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);

		// Iterate, moving back in time to the beginning
		while($related_row = $result->fetch())
		{
			$related_model_id = $related_row[$primary_key_column_id];
			$related_model_ids[$related_model_id] = $related_model_id;
			// Fill empty array values
			if(!isset($previous_related_states[$related_model_id]))
			{
				$previous_related_states[$related_model_id] = null;
			}
			
			// Create a history state for this row, track against possible previous matching state
			$related_state = new TSm_ModelHistoryState($model_name, $related_row);
			$related_state->trackChangesAgainstPreviousState($previous_related_states[$related_model_id]);
			
			// Find the parent state, which is where it gets attached
			$parent_state = $this->parentStateForRelatedState($related_state);
			
			
			if(isset( $related_row[$heading_display_column_id]))
			{
				$heading_new_value = $related_row[$heading_display_column_id];
			}
			else // text or something
			{
				$heading_new_value = $heading_display_column_id;
			}
			
			// Deal with the heading
			$heading_state_change = $this->createHeadingStateChange($related_model_values,
			                                                        $related_model_id,
			                                                        $heading_display_column_id,
			                                                        $related_state,
			                                                        $previous_related_states[$related_model_id],
			                                                        $heading_new_value);
			
			// Null indicates to skip the heading and change altogether
			if(is_null($heading_state_change))
			{
				continue;
			}
		
			$parent_state->addChange($heading_state_change);
			
			// ADD ALL THE CHANGES
			// Track the changes, add them to the parent state
			foreach($related_state->changesFlattened() as $change)
			{
				// Insertion skips in few cases
				if($related_state->isInsert())
				{
					// empty values are skipped
					if(is_null($change->newValue()) || $change->newValue() == '')
					{
						continue;
					}
					
					// heading_display_column_id
					if($change->columnName() == $heading_display_column_id)
					{
						continue;
					}
				}
				
				
				// Don't bother with values that connect to the reference column name
				if($change->columnName() == $reference_column_name)
				{
					continue;
				}
				
				$change->setAsRelatedEntry();
				
				// Match the action of the related state that we're dealing with
				$change->setAction($related_state->action());
				$parent_state->addChange($change);
				
			}
			
			
			
			// Update previous state, based on the primary key
			$previous_related_states[$related_model_id] = $related_state;
		}
		
		// DEAL WITH THIRD LEVEL HISTORIES
		// These might be the only change and there could be no 2nd-level at all, so we need to acquire them separately
		// Then roll them into the larger history, possibly creating intermediary 2nd-level related items to attach
		// them to.
		if(method_exists($model_name,'historyRelatedModels'))
		{
			foreach($model_name::historyRelatedModels() as $third_level_related_models)
			{
				
				$this->processThirdLevelRelatedHistory($third_level_related_models, $related_model_values, $related_model_ids);
			}
		}
	}
	
	/**
	 * This takes third level changes and appends them to the top level that it's associated with. This performs
	 * slightly differently than the second level:
	 *
	 * 1. It's adding to the top level, but possibly sorting based on the second level
	 * 2. All values are added.
	 *
	 * @param array $third_level_related_model_values
	 * @param array $second_level_related_model_values
	 * @param array $second_level_related_model_ids
	 * @return void
	 */
	protected function processThirdLevelRelatedHistory(array $third_level_related_model_values,
													   array $second_level_related_model_values,
	                                                   array $second_level_related_model_ids) : void
	{
//		$this->addConsoleDebug($third_level_related_model_values);
//		$this->addConsoleDebug($second_level_related_model_values);
//		$this->addConsoleDebug($second_level_related_model_id);
		
		// Don't bother with any of this, if there are no third-level IDs to process
		if(count($second_level_related_model_ids) == 0)
		{
			return;
		}
		
		// Reset the previous states, which are based on the primary keys of the table
		// Every time we come across one, we track the previous state in an array
		$previous_related_states = [];
		
		// Save common values, easier for naming
		$model_name = $third_level_related_model_values['model_name'];
		$related_table_name = $model_name::tableName();
		$primary_key_column_id = $model_name::$table_id_column;
		
		$second_level_model_name = $second_level_related_model_values['model_name'];
		$second_level_primary_column_id = $second_level_model_name::$table_id_column;
		
		$reference_column_name = $third_level_related_model_values['reference_column_name'];
		
		$heading_display_column_id = $second_level_primary_column_id;
		
		// Pull in the related value for the display column id
		if(isset($second_level_related_model_values['heading_display_column_id']))
		{
			$heading_display_column_id = $second_level_related_model_values['heading_display_column_id'];
			
		}
		
		// FIND MISSING INSERT HISTORIES
		// left join the main table with the histories, flagging missing inserts
		$query = "SELECT t.*, ISNULL(h.".$primary_key_column_id.") as missing_insert_history
				FROM ".$related_table_name." t LEFT JOIN _history_".$related_table_name." h
				ON(t.".$primary_key_column_id." = h.".$primary_key_column_id." AND h._history_action = 'insert' )
				WHERE t.".$reference_column_name." IN (".implode(', ', $second_level_related_model_ids).")";
		$result = $this->DB_Prep_Exec($query );
		while($row = $result->fetch())
		{
			if($row['missing_insert_history'])
			{
				$model = $model_name::init($row);
				$first_state = $model->createModelInsertHistoryState();
		//		$previous_related_states[$model->id()] = $first_state;
			}
		}
		
		// Loop through history, adding changes to the appropriate state
		// Left joints with the secondary table to get the display column ID, just in case it's needed for
		// creating secondary headings. Happens if there's nothing changed for the second level, so we just need to
		// create it
		$query = "SELECT t.*, s.".$heading_display_column_id." as parent_heading
				FROM `_history_" .$related_table_name."` t
				LEFT JOIN `".$second_level_model_name::tableName()."` s USING(".$reference_column_name.")
				WHERE t.".$reference_column_name." IN (".implode(', ', $second_level_related_model_ids).")
				ORDER BY t._history_timestamp ASC";
		$result = $this->DB_Prep_Exec($query);
		
		// Iterate, moving back in time to the beginning
		while($related_row = $result->fetch())
		{
			$second_level_id = $related_row[$second_level_primary_column_id];
			$related_model_id = $related_row[$primary_key_column_id];
			//$this->addConsoleDebug('Inspecting '.$related_model_id);
			
//			if($related_row['content_id'] == 2290)
//			{
//				$this->addConsoleDebug($related_row);
//			}
			
			// Fill empty array values
			if(!isset($previous_related_states[$related_model_id]))
			{
				$previous_related_states[$related_model_id] = null;
			}
			
			// Create a history state for this row, track against possible previous matching state
			$related_state = new TSm_ModelHistoryState($model_name, $related_row);
			$related_state->trackChangesAgainstPreviousState($previous_related_states[$related_model_id]);
			
			// Find the parent state, which is where it gets attached
			$parent_state = $this->parentStateForRelatedState($related_state);
			$second_level_reference_index = $second_level_model_name.'-'.$second_level_id;
			// Detect if the parent state has any entries for the second level
			// IF NOT, we need to create the heading
			if(!$parent_state->hasChangesForReferenceIndex($second_level_reference_index))
			{
				// Deal with the heading
				$heading_state_change = $this->createHeadingStateChange($second_level_related_model_values,
				                                                        $second_level_id,
				                                                        $reference_column_name,
				                                                        $related_state,
				                                                        $previous_related_states[$related_model_id],
				                                                        $related_row['parent_heading']);
				
				// Null indicates to skip the heading and change altogether
				if(is_null($heading_state_change))
				{
					continue;
				}
				
				$parent_state->addChange($heading_state_change, $second_level_reference_index);
				
			}

			
			// ADD ALL THE CHANGES
			// Track the changes, add them to the parent state
			foreach($related_state->changesFlattened() as $change)
			{
				
				// Insertion skips in few cases
				if($related_state->isInsert())
				{
					// empty values are skipped
					if(is_null($change->newValue()) || $change->newValue() == '')
					{
						continue;
					}

					// Skip if we're referencing the ID column of the second level
					if($change->columnName() == $reference_column_name)
					{
						continue;
					}

				}
				
				
				// Don't bother with values that connect to the reference column name
				//if($change->columnName() != $reference_column_name)
				//{
					$change->setAsRelatedEntry();
					
					// Match the action of the related state that we're dealing with
					$change->setAction($related_state->action());
					//$this->addConsoleDebug($change);
					$parent_state->addChange($change, $second_level_reference_index);
				//}
			}
			
			
			
			// Update previous state, based on the primary key
			$previous_related_states[$related_model_id] = $related_state;
		}
		
		
		
	}
	
	/**
	 * Returns the parent state for a given related state. If one exists with the same version it returns that. If
	 * not it will generate a dummy to ensure consistency in the broader history of the main model.
	 * @param TSm_ModelHistoryState $related_state
	 * @return TSm_ModelHistoryState
	 */
	protected function parentStateForRelatedState(TSm_ModelHistoryState $related_state) : TSm_ModelHistoryState
	{
		// All related items are attached to a parent
		// We either find the parent based on the history version
		// OR we create a dummy one
		$parent_state = null;
		
		// Try and associate it with an existing version
		// Happens in scripts that do multiple things at once
		if(is_string($related_state->version()) && isset($this->history_states_by_version[$related_state->version()]))
		{
			$parent_state = $this->history_states_by_version[$related_state->version()];
		}
		else // no connected version, create an empty parent_state
		{
			// Create an update row to add this to it
			//$this->trackHistoryState($state);
			$parent_state = $this->createEmptyHistoryState(
				$related_state->historyTimestamp(),
				$related_state->version(), // pass the version through
				$related_state->historyUserId(),
				'update'
			);
			
			// Add this to our list of tracked states
			$this->trackHistoryState($parent_state);
			
			// Track the history by version, just in case others need to latch onto it as well
			$this->history_states_by_version[$parent_state->version()] = $parent_state;
		}
		
		return $parent_state;
	}
	
	/**
	 * Creates the manual insert history state which is used when we have enabled history at some point after the
	 * initial creation of this model.
	 * @param string $date_added The date string for this item. Usually set based on other values we're dealing with
	 * @param ?string $version The version value, which defaults to what is provided
	 * @param int $user_id The user ID
	 * @param string $action The action for this empty state
	 * @return TSm_ModelHistoryState
	 */
	protected function createEmptyHistoryState(string $date_added,
	                                           ?string $version = 'manual_create',
											   ?int $user_id = null,
	                                           string $action = 'insert') : TSm_ModelHistoryState
	{
		// Add a state for the creation of the model
		return new TSm_ModelHistoryState(get_called_class(),
		                                 [
			                                 static::$table_id_column => $this->id(),
			                                 '_history_version' => $version,
			                                 '_history_user_id' => $user_id,
			                                 '_history_action' => $action,
			                                 '_history_timestamp' => $date_added,
		                                 
		                                 ]);
	}
	
	/**
	 * Creates the initial insert history so that comparisons work correctly. It will use the current values of the
	 * model, however back-date the insertion. This is auto-created in the update trigger that tests for this value,
	 * to avoid double-dipping. Either way, this exists before we make any changes to the history.
	 * @return TSm_ModelHistoryState
	 */
	public function createModelInsertHistoryState() : TSm_ModelHistoryState
	{
		// Go through the columns in the schema, adding the appropriate ones to the values to copy
		$db_values = [];
		$ignored_columns = static::historyAllIgnoredColumns();
		foreach(static::schema() as $column_name => $schema_values)
		{
			// Skip table keys
			if($column_name == 'table_keys')
			{
				continue;
			}
			
			// Skip the ignored columns
			if(in_array($column_name, $ignored_columns))
			{
				continue;
			}
			
			// Skip deleted columns
			if(isset($schema_values['delete']))
			{
				continue;
			}
			
			$db_values[$column_name] = $this->$column_name;
			
		}
		
		
		$db_values['_history_action'] = 'insert';
		$db_values['_history_timestamp'] = $this->date_added;
		$db_values[static::$table_id_column] = $this->id();
		$db_values['_history_version'] = 'retroactive';
		$db_values['_history_user_id'] = 0;
		$table_name = static::tableName();
		
		$snippets = [];
		$query = "INSERT INTO _history_".$table_name."  SET ";
		foreach($db_values as $column => $value)
		{
			$snippets[] = $column . ' = :'.$column;
		}
		
		$query .= implode(', ', $snippets);
		// Create the history
		$this->DB_Prep_Exec($query, $db_values);
		
		// Save the ID and return the state
		$db_values['_history_id'] = $this->DB()->lastInsertID();
		return new TSm_ModelHistoryState(get_called_class(), $db_values);
	}
	
	public function createHeadingStateChange(array $related_model_values,
	                                         ?int $model_id,
	                                         string $column_name,
	                                         TSm_ModelHistoryState $current_state,
	                                         ?TSm_ModelHistoryState $previous_state,
	                                         $new_value) : ?TSm_ModelHistoryChange
	{
		$model_name = $related_model_values['model_name'];
		
		// Get the heading adjustments for the given values
		$heading_adjustments = $model_name::historyHeadingAdjustments($current_state,
		                                                              $previous_state);
		
		// Null indicates to skip the change altogether
		if(is_null($heading_adjustments))
		{
			return null;
		}
		
		// Determine the new value
		if(isset($heading_adjustments['new_value']))
		{
			$new_value = $heading_adjustments['new_value'];
		}
		
		// Page content views have titles
		if(class_exists($new_value) && method_exists($new_value,'pageContent_ViewTitle'))
		{
			$new_value = $new_value::pageContent_ViewTitle();
		}
		
		$heading_state_change = new TSm_ModelHistoryChange($model_name,
		                                                   $model_id,
		                                                   $column_name,
		                                                   '&nbsp;',
		                                                   $new_value);
		$heading_state_change->setAction($current_state->action());
		$heading_state_change->setAsRelatedHeading();
		
		if(isset($related_model_values['heading_display_title']))
		{
			$heading_state_change->setColumnDisplayTitle(
				$related_model_values['heading_display_title']
			);
			//." [".$related_model_id."]"
		}
		
		return $heading_state_change;
	}
	
	/**
	 * Method to track the history state, which handles indexing
	 * @param TSm_ModelHistoryState $state
	 * @return void
	 */
	protected function trackHistoryState(TSm_ModelHistoryState $state) : void
	{
		$index = $state->dateFormatted(DateTime::ATOM) . $state->className(). $state->historyID();
		$this->history_states[$index] = $state;
	}
	
	/**
	 * Callback function to provide history change adjustments on values being sent, in order to identify if the
	 * presentation should vary from what is being shown. This function returns an array with different values that
	 * will be handled. You can return an array with no changes or only some.
	 *
	 *
	 * title, old_value, and new_value which are used instead of the defaults from creating a change.
	 *
	 * To use this function declare it for the model in question, then use the provided column name and states to
	 * determine if the values should be changed.
	 *
	 * If you only want to override one of the values, return an array with only those values. AN empty array
	 * therefore indicates no changes to the display.
	 *
	 * Returns null if the change should be IGNORED and not shown.
	 *
	 * @param string $column_name
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyChangeAdjustments(string $column_name,
	                                            TSm_ModelHistoryState $current_state,
	                                            ?TSm_ModelHistoryState $previous_state) : ?array
	{
		return [];

	}
	
	/**
	 * Callback function to provide history heading adjustments on values being sent
	 *
	 * Returns null if the change should be IGNORED and not shown.
	 *
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyHeadingAdjustments(TSm_ModelHistoryState $current_state,
	                                                ?TSm_ModelHistoryState $previous_state) : ?array
	{
		return [];
		
	}
	
	
	
}