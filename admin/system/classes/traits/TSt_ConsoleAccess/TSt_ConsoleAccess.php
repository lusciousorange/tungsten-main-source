<?php
/**
 * Trait TSt_ConsoleAccess
 *
 * A trait added to classes that can interact with the console
 */
trait TSt_ConsoleAccess
{
	
	/**
	 * Adds an console item to the console. This is a wrapper function
	 *
	 * @param TSm_ConsoleItem $console_item
	 */
	public function addConsoleItem($console_item)
	{
		$console_item->commit();	
	}
	
	
	/**
	 * Adds an console message with a specified type
	 *
	 * @param string $message The message to be shown
	 * @param int $type The type of message
	 * @param bool $is_error (Optional) Default false. Indicates if there is an error
	 * @param bool|string $details (Optional) Default false. The details for this message
	 */
	public function addConsoleMessageWithType($message, $type, $is_error = false, $details = false)
	{
		$console_item = new TSm_ConsoleItem($message, $type, $is_error);
		$console_item->setClassName(get_class($this));
		
		if($details)
		{
			$console_item->setDetails($details);
		}
		$this->addConsoleItem($console_item);
	}
	
	/**
	 * @param string $message The message to be shown
	 * @param bool $is_error (Optional) Default false. Indicates if there is an error
	 */
	public function addConsoleMessage($message, $is_error = false)
	{
		$console_item = new TSm_ConsoleItem($message, TSm_ConsoleMessage, $is_error);
		$console_item->setClassName(get_class($this));
		
		$this->addConsoleItem($console_item);
	}
	
	/**
	 * Adds a console item which is of type `TSm_ConsoleDebug`
	 * @param string|array|TCm_Model $message The message to be shown. If you pass in a model, it will pass it
	 * through to addConsoleDebugObject, getting the title of the object.
	 * @param bool $is_error (Optional) Default false. Indicates if there is an error
	 */
	public function addConsoleDebug($message, $is_error = false)
	{
		if($message instanceof TCm_Model)
		{
			$this->addConsoleDebugObject($message->title(), $message);
		}
		else
		{
			$console_item = new TSm_ConsoleItem($message, TSm_ConsoleDebug, $is_error);
			$console_item->setClassName(get_class($this));
			
			$this->addConsoleItem($console_item);
		}
	}
	
	/**
	 * @param string $message The message to be shown
	 */
	public function addConsoleTimer($message)
	{
		$console_item = new TSm_ConsoleItem($message, TSm_ConsoleTimer, false);
		$console_item->setClassName(get_class($this));
		
		$this->addConsoleItem($console_item);
	}
	
	
	/**
	 * Adds a debug message with an object. These are usually temporary
	 * @param string $message The message to be shown
	 * @param TCm_Model $object
	 */
	public function addConsoleDebugObject($message, $object)
	{
		$console_item = new TSm_ConsoleItem('', TSm_ConsoleDebug);
		$console_item->setClassName(get_class($this));
		
		if(is_object($object))
		{
			$console_item->setThreeColumnMessage($message, get_class($object), $object->id() );
		}
		else
		{
			$console_item->setThreeColumnMessage($message, 'Class Init Failed', '');
		}	
		
		$this->addConsoleItem($console_item);
	}
	
	
	
	
	/**
	 * Adds a console item which is of type `TSm_ConsoleWarning`
	 * @param string $message The message to be shown
	 */
	public function addConsoleWarning($message)
	{
		$console_item = new TSm_ConsoleItem($message, TSm_ConsoleWarning);
		$console_item->setClassName(get_class($this));
		
		$this->addConsoleItem($console_item);
	}
	
	/**
	 * Adds a console message that's an error
	 * @param string $message The message to be shown
	 */
	public function addConsoleError($message)
	{
		$this->addConsoleMessage($message, true);
	}
	
	/**
	 * Adds teh current amount of memory usages to the console
	 * @param bool|string $message
	 */
	public function addConsoleDebugMemoryUsage($message = false)
	{
		$console_item = new TSm_ConsoleItem('', TSm_ConsoleDebug);
		$console_item->setClassName(get_class($this));
		
		$console_item->setMultiColumnMessage($message, $this->bytesToUnits(memory_get_usage(TC_getConfig('console_show_real_memory'))) );
		
		$this->addConsoleItem($console_item);
	}
	
	
}
?>