<?php

interface TSi_Plugin
{
	/**
	 * @return mixed
	 */
	public static function pluginTitle();
	
	/**
	 * Returns the icon name
	 * @return mixed
	 */
	public static function pluginIconName();
	
	
	/**
	 * Returns the descriptoion
	 * @return mixed
	 */
	public static function pluginDescription();
	
	/**
	 * Plugins can use up to 8 variables which are plain text values that can be saved and used by the plugin. In order to use variables, you must provide a name for the variable which will be presented to the user. If you return a value of false, then that means the variable isn't used or shown. For example, if you have two variables, then you would check the $variable_number and if 1 or 2 return the appropriate name, otherwise return false.
	 * @param int $variable_number
	 * @return mixed
	 */
	public static function pluginVariableNameForNum($variable_number);
	
	
}
?>