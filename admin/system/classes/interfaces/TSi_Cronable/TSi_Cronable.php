<?php
/**
 * Interface TSi_Cronable
 *
 * An interface that adds the ability to run cron jobs for this class. Cron Jobs are run through the TSm_CronManager
 * class and it will call the runCron() method. Your implementation of this interface must perform whatever actions
 * are necessary for this class form within the static runCron() method.
 */
interface TSi_Cronable
{
	/**
	 * A method that is implemented on your class which performs the necessary function that will be run on each cron
	 * call.
	 */
	public static function runCron();
	
}
?>