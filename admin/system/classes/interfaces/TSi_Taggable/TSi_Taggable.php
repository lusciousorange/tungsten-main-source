<?php

/**
 * Abstract class for which each item in the system extends from. An item in the item cannot used in the tagging system unless it is extended from this class.
 */
interface TSi_Taggable 
{
	/**
	 * A function to put the title wrapped in a link for easy display. There are various parameters that can be set on the link
	 *
	 * @param bool|string $new_window Indicate if the link should open in a new window
	 * @param bool|string $link_class Class(es) assigned to the link.  Multiple classes can be assigned with spaces in-between them in one string
	 * @param bool|string$link_id An ID for the link.  Only one is permitted. Inserting a # character in the text will cause the # to be replaced by the ID of the item
	 * @param bool $use_admin_link Indicate that the admin link should be used
	 * @return mixed
	 */
	public function linkedTitle($new_window = false, $link_class = false, $link_id = false, $use_admin_link = false);

	/**
	 * Tests if a user_id has permission to see this item.
	 *
	 * @param int $user_id
	 * @return mixed
	 */
	public function checkUserCanView($user_id);
	

	/**
	 * Provides html of the image. If the image cannot be found, the option exists to show an icon for this type of item. This function uses the Imager class to create the html for the image
	 *
	 * @param int $max_width The maximum width that the image can be
	 * @param int $max_heightThe maximum height that the image can be
	 * @param bool $even_spacedIndicate if the should be padded to the maximum width and height
	 * @return mixed
	 */
	public function image($max_width, $max_height, $even_spaced = false);	
	
	/**
	 * Gets the main content for a tag of this type. This should only contain inline HTML with break tags separating them
	 * @return mixed
	 */
	public function tagContent();	
	
	/**
	 * Gets the content for this tag when it is viewed in Search. This is a very specific function but the search is also entirely based on tags
	 * @return mixed
	 */
	public function searchContent();	
	
	/**
	 * Returns the tag for this item
	 * @return mixed
	 */
	public function getTag();
	
	/**
	 * Gets the list of tags that match a given search value
	 * @param string $guess
	 * @return mixed
	 */
	public static function getSearchTags($guess);
	
	
		 
}


?>