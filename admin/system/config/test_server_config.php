<?php

// Include the basic config files to grab DB values
include_once($_SERVER['DOCUMENT_ROOT']."/TC_config.php"); // Grab the config file. Everything start there.
@include_once($_SERVER['DOCUMENT_ROOT']."/TC_config_env.php"); // Grab the config file. Everything start there.

// We should hard exit if we're not ready to show errors
// Avoids running on production sites
if(!$TC_config['show_errors'])
{
    return;
}

// Turn on all the error reporting
ini_set('display_errors','1');
error_reporting(E_ALL);

	print '<html><head><title>Tungsten Server Tests</title></head><body><h1>System Checks</h1>';

	$loaded_extensions = get_loaded_extensions();
	foreach($loaded_extensions as $extension)
	{
	//	print '<br />'.$extension;
	}
	// Test PHP Version

	printCheckStatus('Document Root: '.$_SERVER['DOCUMENT_ROOT'], 'skipped');

    if(function_exists('posix_getpwuid') && function_exists('posix_geteuid'))
    {
        $username = posix_getpwuid(posix_geteuid())['name'];
        printCheckStatus('Username: '.$username, 'skipped');
    }

	$memory_limit = ini_get('memory_limit');
	$memory_limit = str_replace('M','',$memory_limit);
	printCheckStatus('Memory Limit: '. $memory_limit.'MB', $memory_limit > 64);
	
	$memory_limit = ini_get('upload_max_filesize');
    $memory_limit = str_replace('M','',$memory_limit);
	printCheckStatus('Upload Max Size: '. $memory_limit.'MB', $memory_limit > 32);

    $post_max_size = ini_get('post_max_size');
    $post_max_size = str_replace('M','',$post_max_size);
    printCheckStatus('Post Max Size: '. $post_max_size.'MB', $post_max_size > 32);

    $max_execution_time = ini_get('max_execution_time');
    printCheckStatus('Max Execution Time: '. $max_execution_time, $max_execution_time > 12);

    $file_uploads = ini_get('file_uploads');
    printCheckStatus('File Uploads: '. ($file_uploads ? 'On' : 'Off'), $file_uploads == '1');

    printCheckStatus('PHP Version '.phpversion(), version_compare(phpversion(),'7.2.0', '>'));
	printCheckStatus('GD Installed', in_array('gd',$loaded_extensions));
	printCheckStatus('Curl Installed', in_array('curl',$loaded_extensions));
	printCheckStatus('PDO Installed', in_array('PDO',$loaded_extensions));
	printCheckStatus('PDO-MySQL Installed', in_array('pdo_mysql',$loaded_extensions));
	printCheckStatus('SPL Installed', in_array('SPL',$loaded_extensions));
	printCheckStatus('Reflection Installed', in_array('Reflection',$loaded_extensions));
	printCheckStatus('Sessions Installed', in_array('session',$loaded_extensions));
	printCheckStatus('exec() callable', function_exists('exec'));

    printCheckStatus('htaccess readable', is_readable($_SERVER['DOCUMENT_ROOT'].'/.htaccess'));
    
    // Test folder creation
    $test_folder = $_SERVER['DOCUMENT_ROOT'].'/test/';
    mkdir($test_folder,0755);
    printCheckStatus('mkdir /test/', is_dir($test_folder));
    
    $filename = 'abc.txt';
    file_put_contents($test_folder.$filename,'abc');
    printCheckStatus('save file', file_exists($test_folder.$filename));
    
    unlink($test_folder.$filename);
    printCheckStatus('delete file', !file_exists($test_folder.$filename));
    
    rmdir($test_folder);
    printCheckStatus('rmdir /test/', !is_dir($test_folder));

// Test BitBucket Port Open
	$host = 'bitbucket.org';
	$ports = array(22, 80);
	foreach ($ports as $port)
	{
		$connection = @fsockopen($host, $port);
		
		if (is_resource($connection))
		{
			//echo '<h2>' . $host . ':' . $port . ' ' . '(' . getservbyport($port, 'tcp') . ') is open.</h2>' . "\n";
			printCheckStatus($host . ':' . $port . ' ('.getservbyport($port, 'tcp').') Responding', true);
			
			fclose($connection);
		}
		
		else
		{
			printCheckStatus($host . ':' . $port .' NOT Responding', false);
		}
	}
	
	// GIT VERSION
	if(function_exists('exec'))
	{
		$git_path = exec('git --version', $output);
		$output = $output[0];
		$is_valid = strpos('command not found', $output) === false;
		printCheckStatus('GIT Installed: '.$output , $is_valid);

		// Never works, wrong user
//		$git_path = exec('ssh -T git@bitbucket.org', $output);
//		var_dump($output);
//		$is_valid = strpos('logged in as', $output) !== false;
//		printCheckStatus('SSH To Git: '.$output , $is_valid);

	}
	else
	{
		$output = 'exec() required';
		$is_valid = 'skipped';
		printCheckStatus('GIT Installed: '.$output , $is_valid);
	}

	// TEST DB CONNECTION
	if(in_array('PDO',$loaded_extensions))
	{
		$connection_string = "mysql:host=".$TC_config['DB_hostname'].";dbname=".$TC_config['DB_database'].";charset=utf8";
		if(isset($TC_config['DB_port']))
		{
			$connection_string .= ';port='.$TC_config['DB_port'];
		}

		$error_found = false;
		$connection = false;
		try
		{
			$connection = new PDO($connection_string, $TC_config['DB_username'] , $TC_config['DB_password']);

		}
		catch(Exception $e)
		{
			$error_found = true;
			printCheckStatus('DB Connection Test: '.$e, false);
		}

		if(!$error_found)
		{
			printCheckStatus('DB Connection Test: Success', true);

		}


		if($connection)
		{
			$query = "SHOW TABLES";
			$statement =  $connection->prepare($query);
			$statement->execute();
			$error_code = $statement->errorCode();

			$table_rows = $statement->fetchAll();
			printCheckStatus('DB Show Tables : '.
                             ($error_code == '00000' ? 'Success - '.count($table_rows).' Tables' : 'Error '.$error_code),
                             $error_code== '00000');
			
			
			$query = "SHOW ENGINES";
			$statement =  $connection->prepare($query);
			$statement->execute();
			
			$innodb_found = false;
			while($row = $statement->fetch())
			{
				if($row['Engine'] == 'InnoDB')
				{
					$innodb_found = true;
				}
			}
			printCheckStatus('DB InnoDB Support', $innodb_found);
			
			
		}




	}

	/**
	 * Prints a status message for a given proper
	 * @param string $message
	 * @param bool|string $is_valid
	 */
	function printCheckStatus($message, $is_valid)
	{
		$status_class = ($is_valid ? 'valid' : 'invalid');
		if (is_string($is_valid))
		{
			$status_class = $is_valid;
		}
		print '<div class="check_status '.$status_class.'">';
		print ($is_valid ? '+' : '-');
		print " ".$message;
		print '</div>';
	}


?>

<style>
	.check_status {
		display: block;
		padding:5px;
		margin-bottom:5px;
		color: #FFF;
		font-family:Arial, sans-serif;
		}
	.check_status.skipped {
		background: #999;
		}
	.check_status.valid {
		background: #090;
		}
	.check_status.invalid {
		background: #900;
		}
</style>
<?php
print '</body></html>';
//print phpinfo();
?>