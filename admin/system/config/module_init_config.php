<?php
	// Loads any files within a module that is located in /config/init.php.
	// Since this must load before any models, it will load the init for any module, even if it isn't installed yet

	$admin_path = $_SERVER['DOCUMENT_ROOT'] . '/admin/';

		// loop through the classes folder
		$module_folders = scandir($admin_path);
		foreach ($module_folders  as $module_folder_name)
		{
			if($module_folder_name != 'index.php' && $module_folder_name != 'LICENSE.txt')
			{
				$init_path = $_SERVER['DOCUMENT_ROOT'] . '/admin/' . $module_folder_name . '/config/init.php';
				if(file_exists($init_path))
				{
					include_once($init_path);
				}
			}
		}




?>