<?php
	$TC_config['is_staging_domain'] = false;
	$TC_config['staging_domain'] = '';
	$TC_config['staging_exists'] = false;
	
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/TC_config_env.php"))
	{
		$TC_config['is_staging_domain'] = true;
		$TC_config['staging_domain'] = $_SERVER['HTTP_HOST'] ?? ''; // isset, otherwise empty string
		$TC_config['staging_exists'] = true;
		
		// include the actual file, which will override any settings previously set
		include_once($_SERVER['DOCUMENT_ROOT']."/TC_config_env.php");
	}
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/TC_config_local.php"))
	{
		$TC_config['is_staging_domain'] = true;
		$TC_config['staging_domain'] = $_SERVER['HTTP_HOST'] ?? ''; // isset, otherwise empty string
		$TC_config['staging_exists'] = true;
		
		// include the actual file, which will override any settings previously set
		include_once($_SERVER['DOCUMENT_ROOT']."/TC_config_local.php");
	}

// Deal with Cypress by detecting HTTP User Agent
	// Only bother on staging domains – won't load on production environments
	if($TC_config['is_staging_domain'] && isset($_SERVER['HTTP_USER_AGENT']) &&  strpos($_SERVER['HTTP_USER_AGENT'],'Cypress') > 0)
	{
		// We're running in Cypress using Electron Browser
		// SWITCH TO OUR TEST DATABASE
		if(isset($TC_config['DB_UNIT_database']))
		{
			$TC_config['DB_database'] = $TC_config['DB_UNIT_database'];
		}
		else
		{
			$TC_config['DB_database'] = 'UNIT_TEST';
		}
	}
	
	
?>