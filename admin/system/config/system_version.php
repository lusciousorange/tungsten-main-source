<?php
$use_tungsten_9 = false;

// Define the version 8 numbers
$admin_version = '8.16.0';
$php_min_version = '7.4.0';

// Try to find a way to determine the version number
if(isset($TC_config))
{
	$use_tungsten_9 = $TC_config['use_tungsten_9'];
}
elseif(function_exists('TC_getConfig'))
{
	$use_tungsten_9 = TC_getConfig('use_tungsten_9');
}

// Override them with the version 9, if we have it
if($use_tungsten_9)
{
	$admin_version = '9.0.1';
	$php_min_version = '8.1.0';
}
