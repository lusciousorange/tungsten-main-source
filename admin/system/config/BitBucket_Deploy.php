<?php

//date_default_timezone_set('America/Los_Angeles');
include_once($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_config.php"); // Grab the config file. Everything start there.


////////////////////////////////////////////////
//
// VERIFY BitBucket X_HUB_SIGNATURE
//
////////////////////////////////////////////////
// These are set in Bitbucket as part of the webhook
// If the webhook has this setup, then you need to set the config value to match
if(TC_configIsSet('bitbucket_webhook_secret'))
{
	$postdata = file_get_contents("php://input");
	$secret = TC_getConfig('bitbucket_webhook_secret');
	$my_hash = 'sha256='.hash_hmac('sha256',$postdata,$secret);
	
	if($my_hash != $_SERVER['HTTP_X_HUB_SIGNATURE'])
	{
		echo "Webhook Secret Failure";
		exit();
	}
	
}


class Deploy 
{

    /**
     * The name of the file that will be used for logging deployments. Set to 
     * FALSE to disable logging.
     * 
     * @var string
     */
    private $_log = 'deployments.log';

    /**
     * The timestamp format used for logging.
     * 
     * @link    http://www.php.net/manual/en/function.date.php
     * @var     string
     */
    private $_date_format = 'Y-m-d H:i:sP';

   /**
     * The directory where your website and git repository are located, can be 
     * a relative or absolute path
     * 
     * @var string
     */
    private $_directory;

    /**
     * Sets up defaults.
     * 
     * @param  string  $directory  Directory where your website is located
     * @param  array   $options       Information about the deployment
     */
    public function __construct($directory, $options = array())
    {
        $this->_log = false;
        
        // Determine the directory path
        $this->_directory = realpath($directory).DIRECTORY_SEPARATOR;

        $available_options = array('log', 'date_format');

        foreach ($options as $option => $value)
        {
            if (in_array($option, $available_options))
            {
                $this->{'_'.$option} = $value;
            }
        }

        //$this->log('Attempting deployment...');
    }

    /**
     * Writes a message to the log file.
     * 
     * @param  string  $message  The message to write
     * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
	 * @return bool
     */
    public function log($message, $type = 'INFO')
    {
        
        return false;
        if($this->_log)
        {
            // Set the name of the log file
            $filename = $this->_log;

            if ( ! file_exists($filename))
            {
                // Create the log file
                file_put_contents($filename, '');

                // Allow anyone to write to log files
                chmod($filename, 0666);
            }

            // Write the message into the log file
            // Format: time --- type: message
            file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
        }
    }

    /**
     * Executes the necessary commands to deploy the website.
     */
    public function execute()
    {
        try
        {
            // Make sure we're in the right directory
            exec('cd '.$this->_directory, $output);
            
            // Discard any changes to tracked files since our last deploy
			//exec('git reset --hard HEAD', $output);
            //$this->log('Reseting repository... '.implode(' ', $output));

			// Adding files breaks repo rules turned off
			// Add any new files
            //exec('git add * ', $output);
           
            // Commit any new files
            //exec('git commit -m "Files added from remove server " ', $output);
           
			// Pushing to origin
            //exec('git push ', $output);
           
			// Update the local repository
            exec('git pull', $output);

			// Update any submodules
			exec('git submodule update --init --recursive', $output);

			// Secure the .git directory
            //exec('chmod -R og-rx .git');
            //$this->log('Securing .git directory... ');

            $this->postDeploy();
            
            
          
        }
        catch (Exception $e)
        {
            $this->log($e, 'ERROR');
        }
    }
    
    /**
     * Called after deploy to perform any additional actions
     */
    public function postDeploy()
    {
	    // insert additional actions here
	    $module_list = TSm_ModuleList::init();
	    $module_list->installModulesRequiringUpgrade();
	    
    }

}

// This is just an example
$deploy = new Deploy($_SERVER['DOCUMENT_ROOT']);
/*

$deploy->post_deploy = function() use ($deploy) {
    // hit the wp-admin page to update any db changes
    exec('curl http://www.foobar.com/wp-admin/upgrade.php?step=upgrade_db');
    $deploy->log('Updating wordpress database... ');
};
*/

$deploy->execute();

?>