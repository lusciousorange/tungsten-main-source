<?php


//////////////////////////////////////////////////////
//
// PHPUNIT BOOTSTRAP FOR TUNGSTEN
//
// This bootstrap is designed to configure Tungsten for
// everything necessary in order to run unit tests.
//
// This assumes you have PHPUnit properly installed and configured.
//
// Most of the bootstrap is concerned with avoiding notice errors and ensuring
// the basic system will run correctly.
//
//
//////////////////////////////////////////////////////


$_SERVER['DOCUMENT_ROOT'] = substr(dirname(__FILE__),0,strpos(__DIR__,'/admin'));

// SET DEFAULT SERVER SETTINGS
$_SERVER['HTTP_HOST'] = 'phpunit.test';
$_SERVER['HTTP_USER_AGENT'] = 'phpunit';
$_SERVER['REQUEST_URI'] = '';
$_SERVER['QUERY_STRING'] = '';
$_SERVER['SERVER_PORT'] = '8888';



//////////////////////////////////////////////////////
//
// Include TC_config, then modify before the main config
//
//////////////////////////////////////////////////////

include_once($_SERVER['DOCUMENT_ROOT']."/TC_config.php"); // Grab the config file. Everything start there.



// Get the local config
// Since this code is shared and available on the live server, we must
// do a hard exist if no local config exists
if(
	!file_exists($_SERVER['DOCUMENT_ROOT']."/TC_config_env.php")
	&& !file_exists($_SERVER['DOCUMENT_ROOT']."/TC_config_local.php")
)
{
	print 'UNIT TESTING requires TC_config_env';
	exit();
}

// Set the default unit testing database, which can be overwritten by the local if necessary
$TC_config['DB_UNIT_database'] = 'UNIT_TEST';

// The email for the admin user created
// This can be overridden in the TC_config_env as needed
$TC_config['unit_test_email'] = 'unit_test@tungsten.com';
$TC_config['unit_test_password'] = 'test123';

// include the actual file, which will override any settings previously set
// the `include_once` ensures it gets loaded here and not again in `staging_config.php`
if(file_exists($_SERVER['DOCUMENT_ROOT'] . "/TC_config_env.php"))
{
	include_once($_SERVER['DOCUMENT_ROOT'] . "/TC_config_env.php");
}
elseif(file_exists($_SERVER['DOCUMENT_ROOT'] . "/TC_config_local.php"))
{
	include_once($_SERVER['DOCUMENT_ROOT'] . "/TC_config_local.php");
}

// Check for the additional flag for allowing unit testing
if(!$TC_config['allow_unit_testing'])
{
	print 'UNIT TESTING NOT ENABLED in TC_config_env';
	exit();
}

//////////////////////////////////////////////////////
//
// HARD CONFIG OVERRIDES
//
//////////////////////////////////////////////////////
$TC_config['console_saving'] = true; // avoid saving to console
$TC_config['console_is_unit_test'] = false; // instead have console items throw hard errors
$TC_config['show_errors'] = E_ALL ^ E_DEPRECATED;



//////////////////////////////////////////////////////
//
// DATABASE NAMES
//
//////////////////////////////////////////////////////

// Save local module settings
$local_database_name = $TC_config['DB_database'];

// Setup the DB be a separate DB to ensure we never trigger changes
$TC_config['DB_database'] = $TC_config['DB_UNIT_database'];


$TC_config['tables_to_copy'] = array(
	'pages_menus',
	'pages_content',
	'pages_content_variables',
	'pages_content_views_hidden',
	'pages_css_styles',
	'pages_template_settings',
	'modules',
	'plugins'
);


//////////////////////////////////////////////////////
//
// INSTALL DATABASE
//
//////////////////////////////////////////////////////
resetUnitTestDatabase($TC_config);

//exit();

// Include the normal config
require_once($_SERVER['DOCUMENT_ROOT'] . "/admin/system/headers/tungsten_config.php");

//////////////////////////////////////////////////////
//
// SESSION
//
// These sessions always run under the same ID for testing
// purposes. The code might generate unexpected session IDs
// So we destroy it, set the same ID and start it
//
//////////////////////////////////////////////////////
// Deal with session IDs
session_destroy();

// Manually set the session ID
session_id('unit-testing');

session_start();


// Force committing on, so that we now get values
TSm_ConsoleItem::$committing_permitted = true;



//////////////////////////////////////////////////////
//
// START TIME
//
//////////////////////////////////////////////////////
$_SESSION['timer_start'] = microtime(true);
$_SESSION['timer_lap'] = $_SESSION['timer_start'];
trackTime('Start');

//////////////////////////////////////////////////////
//
// COPY TABLES
//
//////////////////////////////////////////////////////

// DISABLE FOREIGN KEYS
$dbh = TCm_Database::connection();
$dbh->query("SET FOREIGN_KEY_CHECKS = 0");

copyLocalDatabaseTables($local_database_name);
trackTime('Copy All Tables');


//////////////////////////////////////////////////////
//
// INSTALL LOCAL SNAPSHOT
// There are small things we want to grab from the local
// database. This should be the minimal amount necessary
// to run the site, which means :
// * Module Variables
// * Pages
//
//////////////////////////////////////////////////////
installLocalSnapshot($TC_config, $local_database_name, $TC_config['DB_database']);
trackTime('Local Snapshot');

//////////////////////////////////////////////////////
//
// INSTALL FOREIGN KEYS
//
// These do not copy when using CREATE TABLE ... LIKE ...
// Uses the module's schema setup
//
//////////////////////////////////////////////////////
installForeignKeys();
trackTime('Foreign Keys');

//////////////////////////////////////////////////////
//
// CREATE DEFAULT USER
//
// Generate the first admin user, which can also be
// used to log into an alternate environment with a
// session_id set to `unit-testing`
//
//////////////////////////////////////////////////////
createDefaultAdminUser($TC_config['unit_test_email'], $TC_config['unit_test_password']);


// Clear the console of items that are from install
$dbh->query('TRUNCATE console_items');

//////////////////////////////////////////////////////
//
// MODULE SPECIFIC UNIT-TEST CONFIGS
// Each module might have a file in their config called
// unit_test_config.php which needs to be run as part
// of the bootstrap.
//
//////////////////////////////////////////////////////
runModuleUnitTestScripts();
trackTime('Module Unit Test Scripts');


//////////////////////////////////////////////////////
//
// RESET SAVED VALUES
// There are items which might have been cache before
// all the data was ready. We should wipe those
// to avoid false positives
//
//////////////////////////////////////////////////////
unset($GLOBALS['TC_module_configs']);

// ---------------- END PROCESSING ---------------- //



//////////////////////////////////////////////////////
//
// FUNCTIONS
//
// Separating out functions for easy reading
//
//////////////////////////////////////////////////////

/**
 * Copies the table structure from the local database.
 * @param string $local_database_name
 */
function copyLocalDatabaseTables(string $local_database_name) : void
{
	$db = TCm_Database::connection();
	$result = $db->query("SHOW TABLES IN $local_database_name");
	while($row = $result->fetch())
	{
		$table_name = $row['Tables_in_'.$local_database_name];
		$query = "CREATE TABLE $table_name LIKE $local_database_name.$table_name";
		$db->query($query);
	}
}

/**
 * Install foreign keys from the table definitions. These aren't copied when we do the install, so they have to
 * happen after the fact.
 * @see TSu_InstallProcessor Used for parsing schema file values
 * @uses TSm_InstallTable Runs the constraints from the table
 */
function installForeignKeys() : void
{
	$module_list = TSm_ModuleList::init();
	
	// DISABLE FOREIGN KEYS
	$module_list->DB_Prep_Exec("SET FOREIGN_KEY_CHECKS = 0");
	
	$processor = new TSu_InstallProcessor('update_module');
	
	foreach($module_list->modules() as $module)
	{
		// Parse the tables
		$processor->processTablesForModuleFolder($module->folder());
		// RUN TABLE INSTALLERS
		foreach($processor->tables() as $table_name => $table)
		{
			// Run the installer
			$table->addConstraints();
		}
		
	}
	
	// ENABLE FOREIGN KEYS
	$module_list->DB_Prep_Exec("SET FOREIGN_KEY_CHECKS = 1");
	
	
}

/**
 * Installs the DB with nothing currently set
 * @param array $TC_config
 */
function resetUnitTestDatabase(array $TC_config) : void
{
	// Generate PDO connection without a database set
	// This is a non-Tungsten process since we need to drop and create a database
	$connection_string = "mysql:host=" . $TC_config['DB_hostname'] . ";charset=utf8";
	if(isset($TC_config['DB_port']) && is_string($TC_config['DB_port']))
	{
		$connection_string .= ';port=' . $TC_config['DB_port'];
	}
	
	// PDO connection to the server, no DB selected
	$dbh = new PDO($connection_string, $TC_config['DB_username'], $TC_config['DB_password']);
	
	$dbh->exec("DROP DATABASE IF EXISTS `" . $TC_config['DB_database'] . "`;");
	
	$dbh->exec("CREATE DATABASE `" . $TC_config['DB_database'] . "`;");
}


/**
 * Creates a default user and user group in the system
 * @param string $unit_test_email
 */
function createDefaultAdminUser(string $unit_test_email, ?string $password = null)  : void
{
	
	$values = array('group_id'=>1, 'code'=>'admin', 'title' => 'System Administrator', 'tungsten_access' => '1', 'api_access' => '1');
	TMm_UserGroup::createWithValues($values, array(), false);
	
	
	$values = [
		'first_name' => "Test",
		'last_name' => 'Admin',
		'is_active' => 1,
		'email' => $unit_test_email];
	
	if(!is_null($password))
	{
		$text = new TCu_Text($password);
		$values['password'] = $text->hash();
		
	}
	
	
	TMm_User::createWithValues($values, array(), false);
	
	$values = array('user_id' => 1,'group_id' => '1');
	TMm_UserGroupMatch::createWithValues($values);
	
	// Log the user in, necessary for admin debugging
	$_SESSION["tungsten_user_id"] = 1;
}

/**
 * @param array $TC_config
 * @param string $local_database_name The name of the local database
 * @param string $unit_test_database_name The name of the unit test database
 */
function installLocalSnapshot(array $TC_config, string $local_database_name, string $unit_test_database_name  ) : void
{
	$dbh = TCm_Database::connection();
	
	// Switch to the local DB
	$dbh->query("USE ".$local_database_name);
	
	// MODULE VARIABLES
	// An array of override values for module values. This is a 2D array with the first index being the module folder
	// name, the second being the variable name and the value for that cell being the override value.
	
	// Example
	// $TC_config['unit_test_module_values'] = array();
	// $TC_config['unit_test_module_values']['module_name']['use_sandbox'] = 1;
	$statement = $dbh->prepare("SELECT * FROM module_variables");
	$statement->execute();
	
	$local_module_settings = $statement->fetchAll();
	
	$variables_by_module = array();
	
	// save for a shorter name
	$new_values = [];
	if(isset($TC_config['unit_test_module_values']))
	{
		$new_values = $TC_config['unit_test_module_values'];
		
	}
	// Update Module Variables
	// The variables are copied from the localhost DB and updated here
	foreach($local_module_settings as $index => $setting_values)
	{
		$module_name = $setting_values['module_name'];
		$variable_name = $setting_values['variable_name'];
		
		// if the new value is set, replace the current value with the new value
		if( isset($new_values [$module_name] [$setting_values['variable_name']] ) )
		{
			$local_module_settings[$index]['value'] = $new_values [$module_name] [$setting_values['variable_name']];
		}
		
		// Save to the array grouped by module name
		$variables_by_module[$module_name][$variable_name] = $local_module_settings[$index]['value'];
	}
	
	// Switch to the local DB
	$dbh->query("USE ".$unit_test_database_name);
	
	// INSTALL local module variables
	$query = "INSERT INTO module_variables(module_name,variable_name,value) VALUES ";
	$value_strings = [];
	foreach($variables_by_module as $module_name => $module_values)
	{
		foreach($module_values as $variable_name => $variable_value)
		{
			$value_strings[] = '("'.$module_name.'","'.$variable_name.'","'.$variable_value.'")';
			
		}
		//$module = TSm_Module::init($module_name);
		//	$module->updateVariables($module_values);
	}
	$query .= implode(', ', $value_strings);
	$dbh->query($query);
	
	// COPY RELEVANT TABLES FOR OPTIONAL SITE PREVIEW
	
	$tables = TC_getConfig('tables_to_copy');
	
	foreach( $tables as $table_name)
	{
		$query = "INSERT ".$table_name." SELECT * FROM " . $local_database_name . "." . $table_name;
		$dbh->query($query);
	}
	
	
}

/**
 * Clears the data that might have built up in the tables
 */
function clearDataInTables() : void
{
	
	// Clear anything that might have been created in previous tests
	// We copied certain tables at the start, so we don't touch those
	$protected_tables = TC_getConfig('tables_to_copy');
	
	// Don't clear users
	$protected_tables[] = 'users';
	$protected_tables[] = 'user_groups';
	$protected_tables[] = 'user_group_matches';
	$protected_tables[] = 'console_items';
	$protected_tables[] = 'errors';
	
	// TURN OFF FOREIGN KEYS
	$dbh = TCm_Database::connection();
	$dbh->query("SET FOREIGN_KEY_CHECKS = 0");
	
	
	$result = $dbh->query("SHOW TABLES IN ".TC_getConfig('DB_database'));
	
	$query ='';
	while($row = $result->fetch())
	{
		$table_name = $row[0];
		// Ignore the tables that we copy over, truncating them creates issues
		if(!in_array($table_name, $protected_tables))
		{
			$query .= 'DELETE FROM '.$table_name.'; ';
		}
	}
	
	// Truncate them all
	$dbh->query($query);
	
	$dbh->query("SET FOREIGN_KEY_CHECKS = 1");
}


/**
 * A debug function to track the loading speed of the bootstrap
 * @param string $message
 */
function trackTime($message) : void
{
	//return; // disable to see timing
	$timer_current = microtime(true);
	
	$total_time =  number_format( $timer_current - $_SESSION['timer_start'] ,2,'.','').'s' ;
	$lap_time =  number_format( $timer_current - $_SESSION['timer_lap'],2,'.','').'s' ;
	$_SESSION['timer_lap'] = $timer_current; // change the lap time
	
	$message = substr($message,0, 20);
	
	if($message == 'Start')
	{
		print "\n";
		print str_pad($message, 25, " ", STR_PAD_RIGHT);
		print str_pad('Lap', 10, " ", STR_PAD_LEFT);
		print str_pad('Totals', 10, " ", STR_PAD_LEFT);
		print "\n";
		return;
	}
	
	print str_pad($message, 25, " ", STR_PAD_RIGHT);
	print str_pad($lap_time,10," ", STR_PAD_LEFT);
	print str_pad($total_time,10," ", STR_PAD_LEFT);
	print "\n";
}

/**
 * Loops through each module and looks for a file called unit_test_config.php and runs it
 */
function runModuleUnitTestScripts() : void
{
	$d = dir($_SERVER['DOCUMENT_ROOT'] . '/admin/'); // get the directory
	// loop through each item in the directory
	while($module_name = $d->read())
	{
		$module_path = $_SERVER['DOCUMENT_ROOT'] . '/admin/' . $module_name;
		
		// Check for version.php file.
		if(file_exists($module_path . '/config/unit_test_config.php'))
		{
			include_once ($module_path . '/config/unit_test_config.php');
		}
	}
}



//////////////////////////////////////////////////////
//
// AUTOLOADERS
//
// Running tests reference static methods in test classes
// which require a separate auto-loader since they aren't
// normally found in Tungsten. Nor would we want that.
//
//////////////////////////////////////////////////////

function phpUnit_autoloader($class_name) : void
{
	includeModuleClassWithName($class_name, true);
	
}

// Register the function using SPL
spl_autoload_register('phpUnit_autoloader');

