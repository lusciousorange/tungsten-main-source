<?php
$module_title = 'Custom Forms'; // The name displayed for the Module
$version = "1.0.5"; // the current version of this module
$min_system_version = '8.0';// the earliest version that this module will work with
$show_in_menu = true; // Forced in the first spot manually

// ICON FONT LIBRARY CODE
// Every module can have a single icon, which is limited to using a code from a font library. The available list of font libraries is outlined below. In each case, you must provide a valid icon value and the system will handle any processing to ensure it is presented properly.
// Font Awesome - fontawesome.github.io/Font-Awesome/icons/
$icon_library_code = 'fa-clipboard-list';

// MODEL NAME
// The class name for the primary model for this module. Most modules have on primary class that is represented. False otherwise.
$model_name = 'TMm_CustomForm';

// SECONDARY MODEL NAMES
// Some complex modules may have secondary models which are also managed within it. This is common for modules that have associated values to be controlled which don't necessitate a separate module. In those instances, those models must also be defined since they are used throughout the system including in the auto-generation of module URL targets in the module controller. For any secondary model, provide a key-array pair. The key must be the model name. The array value must have settings for the "url_target_name_prefix" [string], and "init_url_targets" [boolean] which indicates if the controller should automatically initialize the URL targets. If you manually generate URL targets, make sure to use the same prefix as defined in this setting.
$model_names_secondary = array();

// MODULE CONTROLLER CLASS
// The controller class that defines the url targets for this module. Many modules will require a custom module controller, however the most basic module will default to using the TSc_ModuleController.php class. If a custom controller is not needed, comment out the line below.
$controller_class = 'TMc_CustomFormsController';
