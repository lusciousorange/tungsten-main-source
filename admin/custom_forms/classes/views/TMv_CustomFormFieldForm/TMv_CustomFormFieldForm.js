class TMv_CustomFormFieldForm extends TCv_Form
{

	constructor(element, options) {
		super(element, options);

		this.max_select_options = options.max_select_options;

	//	console.log('init');
		//this.element.addEventListener('click', this.handleHelpText.bind(this));

		this.addFieldEventListener('field_type','change', this.fieldTypeChanged.bind(this), true);

		this.addFieldEventListener('option_method','change', this.optionFieldChanged.bind(this), true);

		for (let num = 1; num <= this.max_select_options; num++)
		{
			this.addFieldEventListener('option_'+ num, 'input', this.selectFieldsChanged.bind(this), false);

		}
		this.selectFieldsChanged(null);	// Only run it once
	}

	fieldTypeChanged(event) {
		//	console.log('field type changed');

		// Save the type value for testing
		let type = this.fieldValue('field_type');
		//	console.log('type', type);

		// Calculate if heading should appear for field configuration
		// This is true unless it's a text box, heading, or checkbox
		let show_heading = type !== ''
			&& (type === 'TCv_FormItem_TextField' || type === 'TCv_FormItem_Select' || type === 'TCv_FormItem_RadioButtons')
			;

		this.showHideFieldRows(show_heading, ['field_configuration_heading'], true);
		this.showHideFieldRows(type === 'TCv_FormItem_TextField', ['text_field_group'], true);
		this.showHideFieldRows(type === 'TCv_FormItem_Select' || type === 'TCv_FormItem_RadioButtons'
			, ['select_group'], true);

		// Deal with special cases, inside each of those field options
		// Mostly dealing with select/radio differences and special cases

		// Radios don't deal with empty options
		this.showHideFieldRows(type === 'TCv_FormItem_Select', '.select_empty', true);

	}

	optionFieldChanged(event) {
	//	console.log('optionFieldChanged');
		let option_method = this.fieldValue('option_method');
		this.showHideFieldRows(option_method === 'model', ['select_options'], true);

		this.showHideFieldRows(option_method === 'manual', '.select_text_field', true);

		this.selectFieldsChanged(event);

	}

	selectFieldsChanged(event) {
		this.handleExpandingFieldList(this.max_select_options,'option_', event);
	}

	/**
	 * Handles the processing of expanding field lists for text boxes where only the relevant ones should appear
	 * @param {Number} num_fields The number of fields in the list
	 * @param {String} prefix The prefix used for all the fields
	 * @param {Event} event the event that triggered the expansion
	 */
	handleExpandingFieldList(num_fields, prefix, event) {
		let num_to_show = 0;
		// Start with the second one and show only the ones that aren't filled
		for (let num = 1; num <= num_fields; num++)
		{
			let field_val = this.element.querySelector('#'+prefix+num).value;
			if(field_val.trim() != '')
			{
				num_to_show = num;
			}
		}
		num_to_show++; // add one more since we always show the next one

		// Process them all
		let show_ids = [];
		let hide_ids = [];
		for (let num = 1; num <= num_fields; num++)
		{
			if(num <= num_to_show)
			{
				show_ids.push(prefix+num); // pass in the field name
				show_ids.push('.'+prefix+num+'_row.localization'); // hide any associated localization fields. ONLY works with 1 more language

			}
			else
			{
				hide_ids.push(prefix+num);
				hide_ids.push('.'+prefix+num+'_row.localization'); // hide any associated localization fields. ONLY works with 1 more language

			}
		}

		this.showFieldRows(show_ids);
		this.hideFieldRows(hide_ids);

		// HANDLE FOCUS
		// set focus to the last one, just in case we deleted stuff
		if(event)
		{
			let id = event.target.getAttribute('id');
			let num = id.replace(prefix,'');

			// We just lost a hidden field
			if(num > num_to_show)
			{
				this.element.querySelector('#'+prefix+num_to_show).focus();
			}
		}
	}
}