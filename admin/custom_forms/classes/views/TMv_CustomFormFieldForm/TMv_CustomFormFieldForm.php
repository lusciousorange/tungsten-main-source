<?php
/**
 * Class TMv_CustomFormFieldForm
 * The form for creating and editing a custom form field.
 *
 * @author Katie Overwater
 */
class TMv_CustomFormFieldForm extends TCv_FormWithModel
{
	protected TMm_CustomForm $custom_form;
	protected TMm_CustomFormField $custom_field;

	/**
	 * TMv_CustomFormFieldForm constructor.
	 * @param TMm_CustomForm|TMm_CustomFormField $model
	 */
	public function __construct($model)
	{
		if ($model instanceof TMm_CustomForm)
		{
			$this->custom_form = $model;
			parent::__construct('TMm_CustomFormField');
		}
		else
		{
			parent::__construct($model);
			$this->custom_form = $model->form();
			$this->custom_field = $model;
		}
		
		$this->addClassJSFile('TMv_CustomFormFieldForm');
		$this->addJSClassInitValue('max_select_options',TMm_CustomFormField::$max_select_options );

	}

	/**
	 * Configures the form items that are added to view.
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('field_type', 'Field type');
		$field->addOption('','Select a field type');
		foreach (TMm_CustomFormField::$field_types as $key => $label)
		{
			$field->addOption($key, $label);
		}
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$field->setHelpText("The title for this field");
		$field->setPlaceholderText("First name, email, etc");
		$field->setMaxLength(255);
		$this->attachView($field);
		
		
		
		$field = new TCv_FormItem_TextField('help_text', 'Help text');
		$field->setHelpText('The help text is what you see here. A short explanation for the field.');
		$this->attachView($field);
		
//		$header = new TCv_FormItem_Heading('ux_heading','User experience settings');
//		$header->addHelpText('These values help improve the user experience for the people filling out the form. It provides guidance and help to avoid confusion.');
//		$this->attachView($header);

	
		
		
		
		$header = new TCv_FormItem_Heading('field_configuration_heading','Field configuration');
		$this->attachView($header);
		
		
		
		
		
		// FIELD-TYPE OPTIONS
		$this->attachTextFieldOptions(); // TextField
		//$this->attachFileDropOptions(); // FileDrop
		$this->attachSelectBoxOptions(); // Select

		$field = new TCv_FormItem_Hidden('custom_form_id','');
		$field->setValue($this->custom_form->id());
		$this->attachView($field);
		
		
		$header = new TCv_FormItem_Heading('data_management','Data management settings');
		$header->addHelpText('These are settings related to what type of values the users can input into this field.');
		$this->attachView($header);
		
		$field = new TCv_FormItem_Select('is_required', 'Required');
		$field->setHelpText('Indicate if this field must be filled in, to submit the form. ');
		$field->addOption(1,'Yes');
		$field->addOption(0,'No');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('is_visible', 'Visible');
		$field->setHelpText('Indicates if this field is shown on the form. Hidden fields will not appear.');
		$field->addOption(1,'Yes – show this field on the form');
		$field->addOption(0,'No - hide this field on the form');
		$this->attachView($field);
		
	}


	/**
	 * Attaches options related to Text Fields
	 * @return void
	 */
	public function attachTextFieldOptions() : void
	{
		// TEXT FIELD CONFIG
		$group = new TCv_FormItem_Group('text_field_group');
		
		$field = new TCv_FormItem_TextField('placeholder_text', 'Placeholder text');
		$field->setHelpText('The text that appears in the field as an example of format or entry.');
		$field->setPlaceholderText('Example of placeholder text');
		$group->attachView($field);

		$field = new TCv_FormItem_TextField('character_limit', 'Character limit');
		$field->setHelpText('Limits the length of submission.');
		$field->setIsInteger();
		$group->attachView($field);

		$this->attachView($group);
	}

	/**
	 * Attach fields required for select box options
	 * @return void
	 */
	protected function attachSelectBoxOptions() : void
	{
		$group = new TCv_FormItem_Group('select_group');
		
		$field = new TCv_FormItem_TextField('select_empty_value', 'Empty value name');
		$field->setHelpText('The text that appears when no option is selected. ');
		$field->setDefaultValue('Select an option');
		$field->addClass('select_empty');
		$group->attachView($field);
		
		
		$model_list_options = TC_modelsWithTrait('TMt_CustomFormModelListSelectable');
		$this->addConsoleDebug($model_list_options);
		
		if(count($model_list_options) > 0)
		{
			
			// Check how options should be set
			$field = new TCv_FormItem_Select('option_method', 'Option method');
			$field->setHelpText('How the select field options will be added.');
			$field->setSaveToDatabase(false);
			$field->addClass('option_field');
			//$field->addOption('','None - To add options, select a method.');
			$field->addOption('manual', 'Manual entry - Enter values below');
			
			if(count($model_list_options) > 0)
			{
				$field->addOption('model', 'Content model - Use a list of items in the system');
			}
			if($this->isEditor())
			{
				if($this->custom_field->usesModelOptions())
				{
					$field->setDefaultValue('model');
				}
				else
				{
					$field->setDefaultValue('manual');
				}
			}
			else // editor defaults to manual
			{
				$field->setDefaultValue('manual');
			}
			$group->attachView($field);
			
			// Model options
			$field = new TCv_FormItem_Select('select_options', 'Model class');
			$field->setEmptyValuesAsNull();
			//	$field->addOption('', 'Select an item group to use as options');
			
			// Use the trait to get all options available
			foreach($model_list_options as $list_name)
			{
				/** @var TCm_ModelList|TMt_CustomFormModelListSelectable $list */
				$list = $list_name::init();
				
				foreach($list->formattedOptions() as $option)
				{
					$field->addOption($option['id'], $option['title']);
				}
			}
			$group->attachView($field);
			
		}
//		else
//		{
//			$field = new TCv_FormItem_Hidden('option_method', '');
//			$field->setSaveToDatabase(false);
//			//$field->addOption('','None - To add options, select a method.');
//			$field->setValue('manual');
//			$group->attachView($field);
//
//		}
		
		
		// Manual options fields
		$existing_options = [];
		// Get existing values from the db. It won't do it automatically if they are in a json format
		if ($this->isEditor() && !$this->custom_field->usesModelOptions())
		{
			$existing_options = $this->model()->selectOptions();
		}

		$use_localization = TC_getConfig('use_localization');
		$localization = null;
		$non_default_languages = null;
		if($use_localization)
		{
			// Go through the non-default languages and clone the fields with other language values
			$localization = TMm_LocalizationModule::init();
			$non_default_languages = $localization->nonDefaultLanguages();
		}
		
		for($num = 1; $num<=TMm_CustomFormField::$max_select_options; $num++)
		{
			$field = new TCv_FormItem_TextField('option_'.$num,  'Option '.$num);
			$field->setEmptyValuesAsNull();
			$field->setSaveToDatabase(false);
			$field->addClass('option_'.$num.'_row');
			$field->setMaxLength(TMm_CustomFormField::$max_select_option_length);
			if (isset($existing_options[$num - 1]))
			{
				$field->setDefaultValue($existing_options[$num - 1]);
			}
			$group->attachView($field);
			
			if($use_localization)
			{
				foreach($non_default_languages as $language => $language_settings)
				{
				
				}
				
			}
		}
		
		$field = new TCv_FormItem_Select('select_show_other', 'Show Other… option');
		$field->setHelpText('Indicate if the last item in the list is Other… which allows them to type in their own response. ');
		$field->addOption('0','No – The "other" option is not shown');
		$field->addOption('1','Yes – The last option is "other"');
		
		$group->attachView($field);
		
		$this->attachView($group);
	}
	
	

	//////////////////////////////////////////////////////
	//
	// FORM PROCESSOR
	//
	//////////////////////////////////////////////////////

	/**
	 * We want to perform some actions before any DB actions are performed.
	 * @param $form_processor
	 * @return void
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$form = TMm_CustomForm::init($form_processor->formValue('custom_form_id'));

		if ($form_processor->isCreator() && $form->hasMaxFields())
		{
			$form_processor->fail('You already have the maximum number of fields for this form. 
			To proceed, either remove an existing field or contact a developer.');
		}


		$field_type = $form_processor->formValue('field_type');
		
		// We want to clear any extra select box values
		if ($field_type != 'TCv_FormItem_Select' && $field_type !== 'TCv_FormItem_RadioButtons')
		{
			$form_processor->removeDatabaseValue('select_options');
			
		}
	}

	/**
	 * Performs the primary database action
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor) : void
	{
		$form_processor->addConsoleDebug('customFormProcessor_performPrimaryDatabaseAction');
		
		$method = 'manual';
		if($form_processor->formValue('option_method') != '')
		{
			$method = $form_processor->formValue('option_method');
		}
		
	//	$form_processor->addConsoleDebug('Option Method: '.$form_processor->formValue('option_method'));
		// Set up the options JSON for select fields
		if (($form_processor->formValue('field_type') == 'TCv_FormItem_Select'
			|| $form_processor->formValue('field_type') == 'TCv_FormItem_RadioButtons')
			&& $method == 'manual')
		{
			$select_options = [];
			for($num = 1; $num<=TMm_CustomFormField::$max_select_options; $num++)
			{
				if (!empty($form_processor->formValue('option_'.$num)))
				{
					$select_options[] = $form_processor->formValue('option_'.$num);
				}

			}

		//	$form_processor->addConsoleDebug($select_options);
			
			$form_processor->addDatabaseValue('select_options',json_encode($select_options));
			
			// LOCALIZATION OF VALUES
			if(TC_getConfig('use_localization'))
			{
				// Go through the non-default languages and clone the fields with other language values
				$localization = TMm_LocalizationModule::init();
				foreach($localization->nonDefaultLanguages() as $language => $language_settings)
				{
					$select_options = [];
					for($num = 1; $num<=TMm_CustomFormField::$max_select_options; $num++)
					{
						if (!empty($form_processor->formValue('option_'.$num.'_'.$language)))
						{
							$select_options[] = $form_processor->formValue('option_'.$num.'_'.$language);
						}
						
					}
					
					$form_processor->addDatabaseValue('select_options_'.$language,json_encode($select_options));
					
				}
				
			}
		}
		
		
		
		if($form_processor->isCreator())
		{
			$custom_form = TMm_CustomForm::init($form_processor->formValue('custom_form_id'));
			$form_processor->addDatabaseValue('display_order', $custom_form->numFormFields()+1);
		}

		// Perform the DB actions
		parent::customFormProcessor_performPrimaryDatabaseAction($form_processor);
	}

	/**
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		$form_processor->setSuccessURL('/admin/custom_forms/do/field-list/'.$form_processor->model()->form()->id());
	}


}