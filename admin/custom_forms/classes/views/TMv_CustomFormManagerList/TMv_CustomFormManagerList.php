<?php
/**
 * Class TMv_CustomFormManagerList
 * Admin list of all the custom_forms in the system.
 *
 * @project-id
 * @author Katie Overwater
 */
class TMv_CustomFormManagerList extends TCv_SearchableModelList
{
	/**
	 * TMv_CustomFormManagerList constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->setModelClass('TMm_CustomForm');
		$this->defineColumns();
	}

	/**
	 * Define columns
	 */
	public function defineColumns() : void
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('fields');
		$column->setTitle('Fields');
		$column->setContentUsingListMethod('fieldsColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('submissions');
		$column->setTitle('Submissions');
		$column->setContentUsingListMethod('submissionsColumn');
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('is_visible');
		$column->setTitle('Visible');
		$column->setAlignment('center');
		$column->setContentUsingListMethod('visibleColumn');
		$this->addTCListColumn($column);

		$this->addDefaultEditIconColumn();
		$this->addDefaultDeleteIconColumn();
	}


	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomForm $model
	 * @return TCv_View
	 */
	public function titleColumn(TMm_CustomForm $model) : TCv_View
	{
		$link = $this->linkForModuleURLTargetName($model, 'edit');
		$link->addText($model->title());
		return $link;

	}
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomForm $model
	 * @return string
	 */
	public function fieldsColumn(TMm_CustomForm $model) : string
	{
		$field_titles = [];
		foreach($model->formFields() as $form_field)
		{
			$field_titles[] = $form_field->title();
		}
		
		return implode('<br />',$field_titles);
		
	}
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomForm $model
	 * @return TCv_Link
	 */
	public function submissionsColumn(TMm_CustomForm $model) : TCv_Link
	{
		$num_submissions = $model->numSubmissions();
		
		$link = $this->linkForModuleURLTargetName($model, 'submission-list');
		$link->addText($num_submissions);
		return $link;
		
	}
	
	
	/**
	 * Returns a Visibility "toggle"
	 *
	 * @param TMm_CustomForm $model
	 * @return TSv_ModuleURLTargetLink
	 */
	public function visibleColumn (TMm_CustomForm $model) : TSv_ModuleURLTargetLink
	{
		if ($model->isVisible())
		{
			$edit_button = $this->listControlButton($model, 'toggle-visible', 'fas fa-eye');
			$edit_button->addClass('turned_on_list_toggle');
		}
		else
		{
			$edit_button = $this->listControlButton($model, 'toggle-visible', 'fas fa-eye-slash');
			$edit_button->addClass('turned_off_list_toggle');
		}

		return $edit_button;
	}

	/**
	 * Defines the filters for view
	 */
	public function defineFilters()
	{
		parent::defineFilters();
	}
}
