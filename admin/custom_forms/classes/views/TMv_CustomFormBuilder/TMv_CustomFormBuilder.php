<?php
/**
 * Class TMv_CustomFormBuilder
 * The basic created version of a custom form
 * @author Katie Overwater
 */
class TMv_CustomFormBuilder extends TCv_FormWithModel
{
	protected TMm_CustomForm $form;

	/**
	 * TMv_CustomFormBuilder constructor.
	 */
	public function __construct(TMm_CustomForm $form)
	{
		parent::__construct('TMm_CustomFormSubmission');
		$this->form = $form;
		
		if($url = $form->successPageURL())
		{
			$this->setSuccessURL($url);
		}
	}

	/**
	 * Configures the form items that are added to view.
	 */
	public function configureFormElements()
	{
		foreach ($this->form->visibleFields() as $field)
		{
			$this->attachFieldView($field);
		}

		$this->setButtonText($this->form->submitButtonText());

		$field = new TCv_FormItem_Hidden('custom_form_id','');
		$field->setValue($this->form->id());
		$field->setSavedValue($this->form->id());
		$this->attachView($field);
		
		// If recaptcha is enabled, automatically add it
		if(TC_getConfig('use_google_recaptcha'))
		{
			$field = new TCv_FormItem_GoogleRecaptcha('recaptcha','');
			$this->attachView($field);
			
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// FIELD BUILDER
	// This is the functionality which turns this model and all it's data into a working form field.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Creates the form item view using this model's data
	 * @param TMm_CustomFormField $field
	 * @return void
	 */
	public function attachFieldView(TMm_CustomFormField $field) : void
	{
		$field_type = $field->fieldType();
		
		/** @var TCv_FormItem $field_view */
		$field_view = new $field_type($field->fieldID(), $field->localizeProperty('title'));
		
		// Required
		if (!$field->isHeading() && $field->isRequired())
		{
			$field_view->setIsRequired();
		}
		
		// Help text
		$field_view->setHelpText($field->localizeProperty('help_text'));
		
		
		// Setting for the different form field types we are supporting
		if ($field->isTextField())
		{
			$this->addViewSettingsForText($field_view, $field);
		}
		elseif ($field->isFileDrop())
		{
			$this->addViewSettingsForFileDrop($field_view, $field);
		}
		elseif ($field->isSelect())
		{
			$this->addViewSettingsForSelect($field_view, $field);
		}
		elseif ($field->isRadioButtons())
		{
			$this->addViewSettingsForRadioButtons($field_view, $field);
		}
		elseif ($field->isCheckbox())
		{
			$this->addViewSettingsForCheckbox($field_view, $field);
		}
		$this->attachView($field_view);
		
		// Deal with secondary views to be added
		if($field->isSelect() && $field->selectShowOther())
		{
			$select_other_field = new TCv_FormItem_TextField('select_other_'.$field->id(),'');
			$select_other_field->setIsRequired(true);
			$select_other_field->setVisibilityToFieldValue('custom_field_'.$field->id(), 'select_other');
			$this->attachView($select_other_field);
		}
	}
	
	/**
	 * Text field-specific settings are added here. Placeholder text, and character limit, etc
	 */
	public function addViewSettingsForText(TCv_FormItem $field_view, TMm_CustomFormField $field)
	{
		if ($field->isTextField())
		{
			$field_view->setPlaceholderText($field->localizeProperty('placeholder_text'));
			$field_view->setMaxLength($field->characterLimit());
		}
	}
	
	/**
	 * Any file-drop settings are added here. Includes things like permitted extensions, etc
	 */
	public function addViewSettingsForFileDrop(TCv_FormItem $field_view, TMm_CustomFormField $field)
	{
		if ($field_view instanceof TCv_FormItem_FileDrop)
		{
			
			$field_view->setUploadFolder(TMm_CustomForm::uploadFolder());
			
			$field_view->addPermittedMimeType('image/jpeg');
			$field_view->addPermittedMimeType('image/png');
			$field_view->addPermittedMimeType('image/gif');
			
		}
		
	}
	
	/**
	 * Any select-specific settings are added here. These would include populating the options
	 */
	public function addViewSettingsForSelect(TCv_FormItem $field_view, TMm_CustomFormField $field)
	{
		if ($field->isSelect())
		{
			// Adding options
			$field_view->addOption('',$field->selectEmptyValue());
			foreach ($field->selectOptions() as $option)
			{
				if (is_object($option)) // For Model options.
				{
					/** @var TCm_Model $option */
					$field_view->addOption($option->contentCode(),$option->title());
				}
				else // Manual entry options
				{
					$field_view->addOption($option,$option);
				}
				
			}
			
			// The optional "Other" field
			if($field->selectShowOther())
			{
				$field_view->addOption('select_other','Other…');
				
			}
		}
	}
	
	/**
	 * Any select-specific settings are added here. These would include populating the options
	 */
	public function addViewSettingsForRadioButtons(TCv_FormItem $field_view, TMm_CustomFormField $field)
	{
		$this->addConsoleDebug('addViewSettingsForRadioButtons');
		if ($field->isRadioButtons())
		{
			// Adding options
			foreach ($field->selectOptions() as $option)
			{
				if (is_object($option)) // For Model options.
				{
					/** @var TCm_Model $option */
					$field_view->addOption($option->contentCode(),$option->title());
				}
				else // Manual entry options
				{
					$field_view->addOption($option,$option);
				}
				
			}
		}
	}
	
	/**
	 * Any select-specific settings are added here. These would include populating the options
	 */
	public function addViewSettingsForCheckbox(TCv_FormItem_Checkbox $field_view, TMm_CustomFormField $field)
	{
		if ($field->isCheckbox())
		{
			$field_view->useTitleForLabel();
		}
	}
	
	
	/**
	 * For this form in particular we want to create a TMm_CustomFormSubmission.
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		$form = TMm_CustomForm::init($form_processor->formValue('custom_form_id'));
		$submission = $form->saveSubmissionWithFormProcessor($form_processor);
		
		$submission->sendNotificationEmail();
		
	}
}