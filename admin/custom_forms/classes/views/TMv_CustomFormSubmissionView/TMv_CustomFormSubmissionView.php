<?php
/**
 * Class TMv_CustomFormSubmissionView
 * The view that displays the full form submission.
 * @author Katie Overwater
 */
class TMv_CustomFormSubmissionView extends TCv_View
{

	protected TMm_CustomFormSubmission $submission;

	/**
	 * TMv_CustomFormSubmissionView constructor.
	 */
	public function __construct(TMm_CustomFormSubmission $submission)
	{
		parent::__construct('TMm_CustomFormSubmission');

		$this->addClassCSSFile('TMv_CustomFormSubmissionView');

		$this->submission = $submission;

	}


	/**
	 * Separating the custom submissions into their own function in order to keep things organized.
	 * @return void
	 */
	public function render() : void
	{
		// Table makes it easier for copy/paste but also bulletproof for emails
		$table = new TCv_HTMLTable();
		
		// Heading row
//		$row = new TCv_HTMLTableRow();
//		$row->createHeadingCellWithContent('Form field');
//		$row->createHeadingCellWithContent('Value');
//		$table->attachView($row);
		
		// Date row
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Submission ID');
		$row->createCellWithContent($this->submission->idNumber());
		$table->attachView($row);
		
		
		// Date row
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Date');
		$row->createCellWithContent($this->submission->dateAddedFormatted());
		$table->attachView($row);
		
		foreach($this->submission->form()->formFields() as $field)
		{
			$row = new TCv_HTMLTableRow();
			
			if($field->isHeading())
			{
				$cell = new TCv_HTMLTableCell();
				$cell->setColumnSpan(2);
				$cell->addText($field->title());
				$cell->addClass('heading');
				$row->attachView($cell);
			}
			else // value field
			{
				$row->createCellWithContent($field->title());
				$row->createCellWithContent($this->submission->fieldDisplayValue($field->id()));
			}
			$table->attachView($row);
			
		//	$row->createCellWithContent($values['field']->title());
		
		}
//
//		$response_fields = $this->submission->responses();
//		$count = 1;
//
//		foreach ($response_fields as $name => $values)
//		{
//			$row = new TCv_HTMLTableRow();
//			$row->createCellWithContent($values['field']->title());
//
//			$content = $values['value'];
//
//			if($values['type'] === 'model')
//			{
//				$model = $values['model'];
//				$content = $model->title();
//			}
//
//			// Files are loaded outside site root, no previews available
////			elseif($values['type'] === 'file')
////			{
////				$preview = new TCv_View();
////				$preview->addClass('image_preview');
////				//$field->addClass('image_field');
////
////				$file = new TCm_File('file_'.$count,
////				                     $values['value'],
////				                     TMm_CustomFormSubmission::uploadFolder(),
////				                     TCm_File::PATH_IS_FROM_SERVER_ROOT);
////				$photo = new TCv_Image('image_'.$count,$file);
////				$photo->scaleInsideBox(250,250);
////				$photo->includeFullDomainInSrc();
////				$preview->attachView($photo);
////
////				$link = new TCv_Link();
////				$link->addText('View original file');
////				$link->setURL($file->fullDomainName(). $file->filenameFromSiteRoot());
////				$preview->attachView($link);
////				$content = $preview;
////
////
////			}
//
//			// Attach the content
//			$row->createCellWithContent($content);
//			$table->attachView($row);
//
//			$count++;
//		}
		
		$this->attachView($table);
	}

}