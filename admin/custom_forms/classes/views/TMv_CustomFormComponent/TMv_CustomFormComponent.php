<?php
/**
 * Class TMv_CustomFormComponent
 * The form created by a custom form model.
 * @author Katie Overwater
 */
class TMv_CustomFormComponent extends TMv_CustomFormBuilder
{
	use TMt_PagesContentView;

	protected TMm_CustomForm $form;

	/**
	 * TMv_CustomFormComponent constructor.
	 */
	public function __construct(TMm_CustomForm $form)
	{
		$this->form = $form;
		parent::__construct($form);
	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = [];

		// SERVICE
		$field = new TCv_FormItem_Select('custom_form_id', 'Custom form');
		$field->setHelpText('Indicates which custom form will be added to page.');
		$field->addOption('','Select a form');
		$forms = TMm_CustomFormList::init()->modelsVisible();
		foreach ($forms as $form)
		{
			$field->addOption($form->id(), $form->title());
		}

		$form_items[] = $field;

		return $form_items;
	}

	/**
	 * The title for this view
	 * @return string
	 */
	public static function pageContent_ViewTitle() : string
	{
		return 'Custom form';
	}

	/**
	 * A description of the view that appears in the interface to add views
	 * @return string
	 */
	public static function pageContent_ViewDescription() : string
	{
		return 'Displays one of the forms created in the Custom Forms module.';
	}
	
	/**
	 * Returns an additional view that should be attached to the builder preview. This method only works if the ShowPreviewInBuilder
	 * is turned off.
	 * @param TMm_PagesContent|TMm_PageRendererContent $content_model
	 * @return ?TCv_View
	 */
	public static function pageContent_BuilderPreviewContent($content_model) : ?TCv_View
	{
		$custom_form_id = $content_model->variable('custom_form_id');
		$custom_form = TMm_CustomForm::init($custom_form_id);
		$view = new TCv_View();
		
		if($custom_form)
		{
			$view->addText($custom_form->title());
		}
		else
		{
			$view->addText('Form not found');
		}
		return $view;
	}
	
	/**
	 * Indicates if the preview is shown in the builder. If true, it renders the view, otherwise it shows a grey box
	 * @return bool
	 */
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false;
	}

	/**
	 * Returns the name of the model which is required to instantiate this view
	 * @return string
	 */
	public static function pageContent_View_InputModelName() : null|array|string
	{
		return 'TMm_CustomForm';
	}

	/**
	 * @param bool|TMm_PagesContent $content_model
	 * @return TMm_CustomForm|null
	 */
	public static function overrideViewParameter($content_model = false) : ?TMm_CustomForm
	{
		$custom_form_id = $content_model->variable('custom_form_id');

		if($custom_form_id !== '')
		{
			return TMm_CustomForm::init($custom_form_id);
		}
		else
		{
			return null;
		}
	}
}