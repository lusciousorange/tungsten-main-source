<?php
/**
 * Class TMv_CustomFormForm
 * Admin form for creating and editing a custom form. A list of fields can be found in the content models tab.
 *
 * @author Katie Overwater
 */
class TMv_CustomFormForm extends TCv_FormWithModel
{

	/**
	 * TMv_CustomFormForm constructor.
	 * @param string|TMm_CustomForm $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
	}

	/**
	 * Configures the form items that are added to view.
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$this->attachView($field);

		$field = new TCv_FormItem_Select('is_visible', 'Visible');
		$field->setHelpText('Whether this form is visible. Deactivating the form will hide it on pages, 
		and no longer show it as an option for adding to page.');
		$field->addOption(1,'Yes');
		$field->addOption(0,'No');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('submit_button_text', 'Submit button text');
		$field->setHelpText('The "action" text displayed on submit button for this form.');
		$field->setDefaultValue('Submit form');
		$this->attachView($field);
		
		// Disabled since it gets really inconsistent. Files get saved regardless. Possibly multiples if they keep trying
		// Just too much complexity for something we barely care about
		// Most people want the backup as well
//		$field = new TCv_FormItem_Select('save_submissions', 'Save submissions');
//		$field->setHelpText('Indicates if the values for the form should be saved whenever this form is submitted');
//		$field->addOption(1,'Yes - save submissions in the database');
//		$field->addOption(0,'No – there is no record of submissions for this form');
//		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('notification_emails', 'Notification emails');
		$field->setHelpText('A comma-separated list of emails that should be notified when this form is submitted.');
		$field->setDefaultValue('');
		$field->setPlaceholderText('name@domain.com, different@example.com');
		$this->attachView($field);
		
		$field = new TMv_FormItem_LinkablePageSelect('success_page','Success page');
		$field->setHelpText('Select the page that the person will be redirected to when successfully completed.');
		$field->addOption('','No redirect, return to form');
		$field->setEmptyValuesAsNull();
		$this->attachView($field);
		
	}



	//////////////////////////////////////////////////////
	//
	// FORM PROCESSOR
	//
	//////////////////////////////////////////////////////

	/**
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		$form_processor->setSuccessURL('/admin/custom_forms/do/field-list/'.$form_processor->model()->id());
	}


}