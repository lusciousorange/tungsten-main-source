<?php
/**
 * Class TMv_CustomFormFieldManagerList
 * Admin list of all the custom form fields for a provided form
 *
 * @author Katie Overwater
 */
class TMv_CustomFormFieldManagerList extends TCv_RearrangableModelList
{
	protected TMm_CustomForm $form;

	/**
	 * TMv_CustomFormFieldManagerList constructor.
	 */
	public function __construct($form = false)
	{
		if ($form instanceof TMm_CustomForm)
		{
			$this->form = $form;
			$this->addModels($form->formFields());

		}
		parent::__construct();

		$this->setModelClass('TMm_CustomFormField');
		$this->defineColumns();
	}

	/**
	 * Define columns
	 */
	public function defineColumns() : void
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('field_type');
		$column->setTitle('Field Type');
		$column->setContentUsingModelMethod('fieldTypeLabel');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('required');
		$column->setTitle('Required');
		$column->setContentUsingListMethod('requiredColumn');
		$this->addTCListColumn($column);
		
		
		
		$column = new TCv_ListColumn('is_visible');
		$column->setTitle('Visible');
		$column->setAlignment('center');
		$column->setContentUsingListMethod('visibleColumn');
		$this->addTCListColumn($column);

		$this->addDefaultEditIconColumn();
		$this->addDefaultDeleteIconColumn();
	}


	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomFormField $model
	 * @return TCv_View
	 */
	public function titleColumn(TMm_CustomFormField $model) : TCv_View
	{
		$link = $this->linkForModuleURLTargetName($model, 'field-edit');
		$link->addText($model->title());
		return $link;

	}

	public function requiredColumn(TMm_CustomFormField $model) : string
	{
		// Headings don't have that value
		if($model->isHeading())
		{
			return '';
		}
		
		return $model->isRequired() ? 'Yes' : 'No';
		
	}
	
	/**
	 * Returns a Visibility "toggle"
	 *
	 * @param TMm_CustomFormField $model
	 * @return TSv_ModuleURLTargetLink
	 */
	public function visibleColumn (TMm_CustomFormField $model) : TSv_ModuleURLTargetLink
	{
		if ($model->isVisible())
		{
			$edit_button = $this->listControlButton($model, 'field-toggle-visible', 'fas fa-eye');
			$edit_button->addClass('turned_on_list_toggle');
		}
		else
		{
			$edit_button = $this->listControlButton($model, 'field-toggle-visible', 'fas fa-eye-slash');
			$edit_button->addClass('turned_off_list_toggle');
		}

		return $edit_button;
	}
}
