<?php
/**
 * Class TMv_CustomFormSubmissionManagerList
 * Admin list of all the submissions for a form
 *
 * @author Katie Overwater
 */
class TMv_CustomFormSubmissionManagerList extends TCv_ModelList
{
	protected TMm_CustomForm $form;
	
	/**
	 * TMv_CustomFormSubmissionManagerList constructor.
	 */
	public function __construct($form = false)
	{
		if($form instanceof TMm_CustomForm)
		{
			$this->form = $form;
			$this->addModels($form->submissions());
		}
		parent::__construct();
		$this->setAsHorizontalScrolling();
		
		$this->setModelClass('TMm_CustomFormSubmission');
		$this->defineColumns();
		
		$this->addClassCSSFile('TMv_CustomFormSubmissionManagerList');
		
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns() : void
	{
		$column = new TCv_ListColumn('id');
		$column->setTitle('ID');
		$column->setContentUsingListMethod('idColumn');
		$column->setWidthAsPixels(70);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('date');
		$column->setTitle('Date ');
		$column->setContentUsingModelMethod('dateAdded');
		$column->setWidthAsPixels(150);
		$this->addTCListColumn($column);
		
		foreach($this->form->formFields() as $field)
		{
			if($field->isHeading())
			{
				continue;
			}
			$column = new TCv_ListColumn('field_'.$field->id());
			$column->setTitle($field->title());
			$column->setContentUsingModelMethod('fieldDisplayValue', $field->id());
			
			$column->setWidthAsPixels(180);
			$this->addTCListColumn($column);
			
		}
		
//
//		$column = new TCv_ListColumn('response');
//		$column->setTitle('Responses');
//		$column->setContentUsingListMethod('responseColumn');
//		$this->addTCListColumn($column);
//
		
		// View submission
//		$edit_button = $this->controlButtonColumnWithListMethod('viewSubmissionColumn');
//		$this->addTCListColumn($edit_button);
		
		
		$download_button = $this->controlButtonColumnWithListMethod('downloadColumn');
		$this->addTCListColumn($download_button);
		
		$this->addDefaultDeleteIconColumn();
	}
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomFormSubmission $model
	 * @return TCv_View
	 */
	public function idColumn(TMm_CustomFormSubmission $model) : TCv_View
	{
		$link = $this->linkForModuleURLTargetName($model, 'submission-view');
	//	$link->setIconClassName('fa-eye');
	//	$link->addClass('list_control_button');
		$link->addText($model->idNumber());
		return $link;
	}
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomFormSubmission $model
	 * @return TCv_View
	 */
	public function fieldValueColumn(TMm_CustomFormSubmission $model)
	{
	
	}

	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_CustomFormSubmission $model
	 * @return TCv_View
	 */
	public function responseColumn(TMm_CustomFormSubmission $model) : TCv_View
	{

		$view = new TCv_View();
		$view->addClass('form_submissions_box');

		$this->addConsoleDebug($model->responses());
		
		foreach ($model->responses() as $values)
		{
			$field_title = $values['field']->title();
			$value = $values['value'];
			
			if(isset($values['model']) && $values['model'] instanceof TCm_Model)
			{
				$value = $values['model']->title();
			}
			
			$item = new TCv_View();
			$item->addClass('form_submission');
			$item->addText('<span>'.$field_title.':</span> '.$value);
			$view->attachView($item);
		}

		return $view;

	}

	/**
	 * A button to view full submission
	 *
	 * @param TMm_CustomFormSubmission $model
	 * @return TCv_Link
	 */
	public function viewSubmissionColumn(TMm_CustomFormSubmission $model) : TCv_Link
	{
		$link = $this->linkForModuleURLTargetName($model, 'submission-view');
		$link->setIconClassName('fa-eye');
		$link->addClass('list_control_button');
		return $link;
	}
	
	/**
	 * A button to view full submission
	 *
	 * @param TMm_CustomFormSubmission $model
	 * @return TCv_Link
	 */
	public function downloadColumn(TMm_CustomFormSubmission $model) : TCv_Link
	{
		$link = $this->linkForModuleURLTargetName($model, 'submission-download');
		$link->setIconClassName('fa-download');
		$link->addClass('list_control_button');
		return $link;
	}
}

