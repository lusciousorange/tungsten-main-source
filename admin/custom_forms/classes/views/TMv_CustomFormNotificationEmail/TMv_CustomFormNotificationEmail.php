<?php
/**
 * Class TMv_CustomFormNotificationEmail
 *
 * The email sent to the people who should be notified when a custom form is submitted.
 */
class TMv_CustomFormNotificationEmail extends TCv_Email
{
	protected ?TMm_CustomFormSubmission $submission = null;
	protected ?TMm_CustomForm $form = null;
	/**
	 *
	 * @param TMm_CustomFormSubmission $submission
	 */
	public function __construct(TMm_CustomFormSubmission $submission)
	{
		parent::__construct(null);
		
		$this->submission = $submission;
		$this->form = $submission->form();
		$this->addRecipient($this->form->notificationEmails());
		$this->setSubject('Form submission: '.$this->form->title());
		$this->setShowInConsole(true);
	}
	
	/**
	 * Render the view
	 */
	public function render()
	{
		$p = new TCv_View();
		$p->setTag('h2');
		$p->addText('A form has been submitted on the website.');
		$this->attachView($p);
		
		$this->attachView(TMv_CustomFormSubmissionView::init($this->submission));
//
//		// Loop through each value
//		foreach($this->form->formFields() as $field)
//		{
//			$name = $field->title();
//
//			if($field->isHeading())
//			{
//				$heading = new TCv_View();
//				$heading->setTag('h3');
//				$heading->addText($name);
//				$this->attachView($heading);
//			}
//			else
//			{
//				$value = $this->form_processor->formValue('custom_field_' . $field->id());
//				if($field->isRadioButtons() || $field->isSelect())
//				{
//					// Check for a model name
//					if(substr($value,0,4) == 'TMm_')
//					{
//
//					}
//				}
//				$this->addConsoleDebug($name);
//				$this->addConsoleDebug($value);
//				$p = new TCv_View();
//				$p->setTag('p');
//				$p->addText('<strong>' . $name . '</strong> : ' . $value);
//				$this->attachView($p);
//			}
//		}
		
		
	}
	
	
	
}
