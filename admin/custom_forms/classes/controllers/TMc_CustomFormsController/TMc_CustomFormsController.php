<?php

/**
 * Class TMc_CustomFormsController
 * The module controller. This is where we define URL targets and setup module navigation, etc
 *
 * @author Katie Overwater
 */
class TMc_CustomFormsController extends TSc_ModuleController
{
	/**
	 * TMc_CustomFormsController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets();

		$this->generateDefaultURLTargetsForModelClass(	'TMm_CustomFormField',
			'field-',
			'TMm_CustomForm',
			'form()');

		$target = TSm_ModuleURLTarget::init( 'toggle-visible');
		$target->setModelName('TMm_CustomForm');
		$target->setModelInstanceRequired();
		$target->setTitle('Toggle visible');
		$target->setNextURLTarget('referrer');
		$target->setModelActionMethod('toggleVisible');
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init( 'field-toggle-visible');
		$target->setModelName('TMm_CustomFormField');
		$target->setModelInstanceRequired();
		$target->setTitle('Toggle visible');
		$target->setNextURLTarget('referrer');
		$target->setModelActionMethod('toggleVisible');
		$this->addModuleURLTarget($target);

		// Admins/creators need a way to view what the current version of a form will look like
		$target = TSm_ModuleURLTarget::init( 'preview-form');
		$target->setModelName('TMm_CustomForm');
		$target->setModelInstanceRequired();
		$target->setTitle('Preview');
		$target->setTitleUsingModelMethod('title');
		$target->setViewName('TMv_CustomFormBuilder');
		$this->addModuleURLTarget($target);


		// Add Submission targets
		$this->defineSubmissionURLTargets();

		$this->defineSubmenuGroupingWithURLTargets('edit','field-list','preview-form','submission-list');
	}

	/**
	 * Adds URL targets related to Form Submissions
	 * @return void
	 */
	public function defineSubmissionURLTargets()
	{
		$this->generateDefaultURLTargetsForModelClass(	'TMm_CustomFormSubmission',
			'submission-',
			'TMm_CustomForm',
			'form()',
		'saveSubmissions');

		// Submissions are created via the custom form, so we don't currently need a create/edit form like other models
		$this->removeURLTargetWithName('submission-create');
		$this->removeURLTargetWithName('submission-edit');

		// Add a "View" to show all values of a submission
		$target = TSm_ModuleURLTarget::init( 'submission-view');
		$target->setModelName('TMm_CustomFormSubmission');
		$target->setModelInstanceRequired();
		$target->setTitle('View submission');
		$target->setTitleUsingModelMethod('title');
		$target->setViewName('TMv_CustomFormSubmissionView');
		$target->setParentURLTargetWithName('submission-list','form');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'submission-send-email');
		$target->setModelName('TMm_CustomFormSubmission');
		$target->setModelInstanceRequired();
		$target->setTitle('Re-send email');
		$target->setModelActionMethod('sendNotificationEmail');
		$target->setNextURLTarget('referrer');
		$target->setParentURLTargetWithName('submission-view','form');
		$target->setAsRightButton('fa-envelope');
		
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('submission-download');
		$target->setModelName('TMm_CustomFormSubmission');
		$target->setModelInstanceRequired();
		$target->setTitle('Download');
		$target->setModelActionMethod('download()');
		$target->setNextURLTarget(NULL);
		$target->setAsRightButton('fa-download');
		$this->addModuleURLTarget($target);
		
		
		
	}

}
