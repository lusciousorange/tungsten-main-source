<?php

/**
 * Trait TMt_CustomFormModelListSelectable
 *
 * A trait to set up a model list for use as custom form field options.
 * This will give any model list the functionality required,
 * as well as giving the field permission to use the models as options
 */
trait TMt_CustomFormModelListSelectable
{
	/**
	 * The function that returns the title for a model
	 * @return string
	 */
	public function optionLabelMethod() : string
	{
		return 'title';
	}

	/**
	 * The option set up for a select form. It was originally done in the form,
	 * but it is more efficient to have it here.
	 * @return array
	 */
	public function formattedOptions() : array
	{
		$options = [];
		foreach ($this->modelSubsets() as $key => $values)
		{
			$options[] = [
				'id' => $this->modelClassName().'List'.'__'.$key,
				'title' => $this->modelClassName()::modelTitlePlural().' - '.$values['title']
				];
		}

		return $options;
	}

	/**
	 * The different subset options for this list. Standard would include 'models()' and 'modelsVisible()'.
	 * Can be extended to add additional subsets.
	 * @return array
	 */
	public function modelSubsets() : array
	{
		return [
			"all" => [
				"title" => "All",
				"method_name" => 'models'
			],
			"visible" => [
				"title" => "Visible items",
				"method_name" => 'modelsVisible'
			]
		];
	}

}