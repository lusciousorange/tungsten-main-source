<?php
/**
 * Class TMm_CustomForm
 * Custom forms are the model used to let an admin create their own forms.
 * I want to leave a lot of potential for expansion, but the first version needs to allow
 * admins to add form fields, choose what action happens when submitted, and maybe more
 *
 * @author Katie Overwater
 */
class TMm_CustomForm extends TCm_Model
{
	protected int $custom_form_id;
	
	// DATABASE COLUMNS
	protected ?string $title, $submit_button_text;
	protected int $is_visible;

	// Other model values
	protected array $fields;
	protected array $visible_fields;

	
	protected ?string $success_page = null;
	
	//protected bool $save_submissions = true;
	protected ?array $submissions = null;
	protected string $notification_emails = '';

	public static int $max_fields = 50; // The maximum amount of fields we are allowing the form to have

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'custom_form_id';
	public static $table_name = 'custom_forms';
	public static $model_title = 'Custom Form';
	public static $primary_table_sort = 'title ASC';

	/**
	 * TMm_CustomForm constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	/**
	 * Returns the upload folder which is outside of the site root to avoid files getting uploaded from a public form.
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		$path_parts = explode('/', $_SERVER['DOCUMENT_ROOT']);
		array_pop($path_parts);
		return implode('/', $path_parts).'/files/custom_forms/';
		
	}
	/**
	 * Title of the form. Should be displayed on the form at the top
	 * @return string
	 */
	public function title() : string
	{
		return $this->title;
	}

	/**
	 * @return string|null
	 */
	public function submitButtonText() : ?string
	{
		return $this->localizeProperty('submit_button_text');
	}
	
	/**
	 * Returns the success page URL.
	 * @return string|null
	 */
	public function successPageURL() : ?string
	{
		// The success URL only works if the page is set and it exists
		// We process the value to get a model from the view code, then get the page View URL path for the model
		if(is_string($this->success_page))
		{
			$model = $this-> modelForPageViewCode($this->success_page);
			if($model)
			{
				return $model->pageViewURLPath();
			}
			
		}
		
		return null;
	}

	//////////////////////////////////////////////////////
	//
	// SUBMISSIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the submissions for this form should be saved
	 * @return bool
	 */
	public function saveSubmissions() : bool
	{
		return true; // Always enabled things get too messy otherwise
	}
	
	/**
	 * The fields belonging to this form
	 *
	 * @return array|TMm_CustomFormSubmission[]
	 */
	public function submissions() : array
	{
		if (empty($this->submissions))
		{
			$this->submissions = [];


			$query = "SELECT * FROM `custom_form_submissions`
				         WHERE custom_form_id = :form_id 
				         ORDER BY " . TMm_CustomFormSubmission::$primary_table_sort;
			$result = $this->DB_Prep_Exec($query, array('form_id' => $this->id()));
			while($row = $result->fetch())
			{
				$item = TMm_CustomFormSubmission::init( $row);
				$this->submissions[] = $item;
			}
		}

		return $this->submissions;
	}
	
	/**
	 * Returns the number of submissions for this form
	 * @return int
	 */
	public function numSubmissions() : int
	{
		return count($this->submissions());
	}
	
	/**
	 * Saves a submission using a provided form processor that has all the values needed
	 * @param TCc_FormProcessor $form_processor
	 * @return TMm_CustomFormSubmission
	 */
	public function saveSubmissionWithFormProcessor(TCc_FormProcessor $form_processor) : TMm_CustomFormSubmission
	{
//		if(!$this->saveSubmissions())
//		{
//			return null;
//		}
//
		// We want to get the field titles and response data, and turn into a JSON array
		$responses = [];
		foreach ($this->formFields() as $field)
		{
			if (!$field->isHeading())
			{
				$type = 'text';
				
				$form_value = $form_processor->formValue($field->fieldID());
				
				// Declare codes in title, so we know how to deal with special types
				if ($field->isFileDrop())
				{
					$type = 'file';
				}
				if (($field->isSelect() || $field->isRadioButtons()) && $field->usesModelOptions())
				{
					$type = 'model';
				}
				
				// Deal with other options on selects
				if($field->isSelect() && $form_value == 'select_other')
				{
					$form_value = 'Other… '.$form_processor->formValue('select_other_'.$field->id());
				}
				
				$responses[] = [
					'field_id' => $field->id(),
					'value' => $form_value,
					'type' => $type,
				];
				
			}
			
		}
		
		$values = [
			'custom_form_id' => $this->id(),
			'responses' => json_encode($responses)
		];
		
		if ($user = TC_currentUser())
		{
			$values['user_id'] = $user->id();
		}
		
		return TMm_CustomFormSubmission::createWithValues($values);
	}
	
	//////////////////////////////////////////////////////
	//
	// NOTIFICATION EMAILS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the comma-separated list of notification emails that should receive an email whenever this form is
	 * submitted.
	 * @return string
	 */
	public function notificationEmails() : string
	{
		return $this->notification_emails;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FORM FIELDS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The fields belonging to this form
	 * @return TMm_CustomFormField[]
	 */
	public function formFields() : array
	{
		if (empty($this->fields))
		{
			$this->fields = [];
			$this->visible_fields = [];

			$query = "SELECT * FROM `custom_form_fields`
				         WHERE custom_form_id = :form_id 
				         ORDER BY ".TMm_CustomFormField::$primary_table_sort;
			$result = $this->DB_Prep_Exec($query, array('form_id' => $this->id()));
			while($row = $result->fetch())
			{
				$field = TMm_CustomFormField::init( $row);
				$this->fields[$field->id()] = $field;

				if ($field->isVisible())
				{
					$this->visible_fields[$field->id()] = $field;
				}
			}
		}

		return $this->fields;
	}
	
	/**
	 * Returns the number of form fields in this form
	 * @return int
	 */
	public function numFormFields() : int
	{
		return count($this->formFields());
	}

	/**
	 * Fields that are set to visible
	 * @return array|TMm_CustomFormField[]
	 */
	public function visibleFields() : array
	{
		$this->formFields();

		return $this->visible_fields;
	}

	/**
	 * Whether this form has the maximum fields.
	 * @return bool
	 */
	public function hasMaxFields() : bool
	{
		return count($this->formFields()) >= static::$max_fields;
	}

	//////////////////////////////////////////////////////
	//
	// VISIBILITY
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether this form is visible, which dictates whether it shows up as an option to add to a page.
	 * @return bool
	 */
	public function isVisible() : bool
	{
		return $this->is_visible == 1;
	}

	/**
	 * Toggles whether this custom form is visible
	 */
	public function toggleVisible() : void
	{
		if ($this->isVisible())
		{
			$this->updateDatabaseValue('is_visible', 0);
		}
		else
		{
			$this->updateDatabaseValue('is_visible', 1);

		}
	}

	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether user can view this model in different spots on the site
	 * @param bool $user
	 * @return bool
	 */
	public function userCanView($user = false) : bool
	{
		if (TC_isTungstenView())
		{
			return parent::userCanView();
		}

		return $this->isVisible();
	}

	/**
	 * Whether user has permission to make permanent changes to this model
	 * @param bool $user
	 * @return bool
	 */
	public function userCanEdit ($user = false) : bool
	{
		return parent::userCanEdit($user);
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the custom form to add additional fields
	 * @return array[]
	 */
	public static function schema() : array
	{
		return parent::schema() + [
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title to be shown on top of the form',
					'type'          => 'varchar(128)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'submit_button_text' => [
					'title'         => 'Button text',
					'comment'       => 'The action text displayed on the submit button',
					'type'          => "varchar(128)",
					'nullable'      => true,
					'localization'  => true,
				],
				'is_visible' => [
					'title'         => 'Visible',
					'comment'       => 'Whether this form is in use.',
					'type'          => 'boolean DEFAULT 1',
					'nullable'      => false,
				],
//				'save_submissions' => [
//					'comment'       => 'Indicate if the submissions for this form are saved in the database.',
//					'type'          => 'boolean DEFAULT 1',
//					'nullable'      => false,
//				],
				'notification_emails' => [
					'comment'       => 'Comma-separated list of emails that are notified when a submission occurs.',
					'type'          => 'varchar(2048)',
					'nullable'      => false,
				],
				'success_page' => [
					'type'          => 'varchar(255)',
					'nullable'      => true,
				
				],
			];
	}

}