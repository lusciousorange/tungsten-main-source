<?php
/**
 * Class TMm_CustomFormTest
 * Unit testing class, to be used for unit tests related to TMm_CustomForm
 */
class TMm_CustomFormTest extends TC_ModelTestCase
{

	public static int $created_count = 1;

	public function testGenerate()
	{
		$custom_form = static::generate();

		$this->assertInstanceOf('TMm_CustomForm',$custom_form);
	}

	////////////////////////////////////////////////
	//
	// GENERATE
	//
	////////////////////////////////////////////////
	/**
	 * Generates a test form
	 *
	 * @param array $override_values
	 * @return TMm_CustomForm
	 */
	public static function generate(array $override_values = []): TMm_CustomForm
	{
		$values = array(
			'title' => 'Test Form #'.static::$created_count,

		);

		$values = array_merge($values, $override_values);

		static::$created_count++;

		return TMm_CustomForm::createWithValues($values);
	}
}