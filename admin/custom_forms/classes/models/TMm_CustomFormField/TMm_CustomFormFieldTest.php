<?php
/**
 * Class TMm_CustomFormTest
 * Unit testing class, to be used for unit tests related to TMm_CustomForm
 */
class TMm_CustomFormFieldTest extends TC_ModelTestCase
{

	public static int $created_count = 1;

	/**
	 * Testing the generation of a text field
	 */
	public function testGenerateTextField()
	{
		$custom_field = static::generate('TCv_FormItem_TextField');

		$this->assertInstanceOf('TMm_CustomFormField',$custom_field);
		$this->assertTrue($custom_field->isTextField());
	}


	/**
	 * Testing the generation of a file-drop field
	 */
	public function testGenerateFileDropField()
	{
		$custom_field = static::generate('TCv_FormItem_FileDrop');

		$this->assertInstanceOf('TMm_CustomFormField',$custom_field);
		$this->assertTrue($custom_field->isFileDrop());
	}


	/**
	 * Testing the generation of a select field
	 */
	public function testGenerateSelectField()
	{
		$custom_field = static::generate('TCv_FormItem_Select');

		$this->assertInstanceOf('TMm_CustomFormField',$custom_field);
		$this->assertTrue($custom_field->isSelect());
	}



	/**
	 * Testing the generation of a heading
	 */
	public function testGenerateHeading()
	{
		$custom_field = static::generate('TCv_FormItem_Heading');

		$this->assertInstanceOf('TMm_CustomFormField',$custom_field);
		$this->assertTrue($custom_field->isHeading());
	}

	
	////////////////////////////////////////////////
	//
	// GENERATE
	//
	////////////////////////////////////////////////
	/**
	 * Generates a test form field
	 *
	 * @param string $field_type We need to know which type of field to create
	 * @param array $override_values
	 * @return TMm_CustomFormField
	 */
	public static function generate(string $field_type, array $override_values = []): TMm_CustomFormField
	{
		$form = TMm_CustomFormTest::generate();

		$values = array(
			'title' => 'Field #'.static::$created_count,
			'custom_form_id' => $form->id(),
			'field_type' => $field_type
		);

		$values = array_merge($values, $override_values);

		static::$created_count++;

		return TMm_CustomFormField::createWithValues($values);
	}
}