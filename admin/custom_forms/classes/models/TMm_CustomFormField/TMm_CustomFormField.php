<?php
/**
 * Class TMm_CustomFormField
 * The form fields that are added to a custom form.
 *
 * @author Katie Overwater
 */
class TMm_CustomFormField extends TCm_Model
{
	protected int $custom_form_field_id;
	
	// DATABASE COLUMNS
	protected ?string $title, $help_text, $field_type;
	protected int $is_visible, $custom_form_id, $display_order, $is_required;
	
	// Text field columns
	protected ?string $placeholder_text;
	protected ?int $character_limit;
	
	// Select/Checkbox columns
	protected ?string $select_options;
	protected ?string $select_empty_value = null;
	protected bool $select_show_other = false;
	
	public static array $field_types = [
		'TCv_FormItem_TextField'    => 'Text field',
		'TCv_FormItem_TextBox'      => 'Text box',
		'TCv_FormItem_Select'       => 'Select',
		'TCv_FormItem_RadioButtons' => 'Radio buttons',
		'TCv_FormItem_Heading'      => 'Heading',
		'TCv_FormItem_FileDrop'     => 'Photo upload',
		'TCv_FormItem_Checkbox'     => 'Checkbox',
	];
	
	public static int $max_select_options = 10;
	public static int $max_select_option_length = 128;
	
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'custom_form_field_id';
	public static $table_name = 'custom_form_fields';
	public static $model_title = 'Field';
	public static $primary_table_sort = 'display_order ASC';
	
	/**
	 * TMm_CustomFormField constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	/**
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		return TC_getConfig('saved_file_path') . '/custom_forms/fields/';
	}
	
	/**
	 * Title/label of the field.
	 * @return string
	 */
	public function title() : string
	{
		return $this->title;
	}
	
	/**
	 * The ID string. Created using the title and field ID
	 * @return string
	 */
	public function fieldID() : string
	{
		return 'custom_field_' . $this->id();
	}
	
	/**
	 * The help text for this field
	 * @return string
	 */
	public function helpText() : string
	{
		if(!is_null($this->help_text))
		{
			return $this->help_text;
		}
		
		return '';
	}
	
	/**
	 * The form field type
	 * @return string
	 */
	public function fieldType() : string
	{
		return $this->field_type;
	}
	
	
	
	
	public function isRequired() : bool
	{
		return $this->is_required == 1;
	}

	/**
	 * The human-readable title of the field type
	 * @return string
	 */
	public function fieldTypeLabel() : string
	{
		$name = static::$field_types[$this->field_type];
		
		if($this->isSelect() || $this->isRadioButtons())
		{
			// Deal with scenario where options are null or an empty string
			if(is_null($this->select_options) || $this->select_options == '')
			{
				$this->select_options = '[]';
			}
			if($this->usesModelOptions())
			{
				$details = str_replace('__',' ', $this->select_options);
				$details = str_replace('TMm_','', $details);
				$name .= ' – '.$details;
			}
			else
			{
				$name .= ' – '.implode(', ', json_decode($this->select_options));
			}
			
		}
		
		if($this->isSelect() && $this->selectShowOther())
		{
			$name .= ', Other…';
		}
		
		
		return $name;
	}

	/**
	 * Returns the custom form this field belongs to
	 * @return TMm_CustomForm
	 */
	public function form() : TMm_CustomForm
	{
		return TMm_CustomForm::init($this->custom_form_id);
	}

	/**
	 * Whether this field is a text field
	 * @return bool
	 */
	public function isHeading() : bool
	{
		return $this->fieldType() === 'TCv_FormItem_Heading';
	}


	

	//////////////////////////////////////////////////////
	//
	// TEXTBOX FIELD
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Whether this field is a text field
	 * @return bool
	 */
	public function isTextBox() : ?bool
	{
		return $this->fieldType() === 'TCv_FormItem_TextBox';
	}
	
	//////////////////////////////////////////////////////
	//
	// CHECKBOX FIELD
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Whether this field is a text field
	 * @return bool
	 */
	public function isCheckbox() : ?bool
	{
		return $this->fieldType() === 'TCv_FormItem_Checkbox';
	}
	
	//////////////////////////////////////////////////////
	//
	// TEXT FIELD
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether this field is a text field
	 * @return bool
	 */
	public function isTextField() : ?bool
	{
		return $this->fieldType() === 'TCv_FormItem_TextField';
	}

	/**
	 * Maximum number of allowed characters
	 * @return int
	 */
	public function characterLimit() : ?int
	{
		return $this->character_limit;
	}

	/**
	 * The placeholder text
	 * @return string
	 */
	public function placeholderText() : string
	{
		if (!is_null($this->placeholder_text))
		{
			return $this->placeholder_text;
		}

		return '';
	}


	//////////////////////////////////////////////////////
	//
	// FILE DROP
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether field is a file drop type
	 * @return bool
	 */
	public function isFileDrop() : bool
	{
		return $this->fieldType() == 'TCv_FormItem_FileDrop';
	}


	//////////////////////////////////////////////////////
	//
	// SELECT FIELD + Radio since same functionality
	//
	//////////////////////////////////////////////////////

	
	
	/**
	 * Whether this is a select-type form field
	 * @return bool
	 */
	public function isRadioButtons() : bool
	{
		return $this->fieldType() == 'TCv_FormItem_RadioButtons';
	}

	
	/**
	 * Whether this is a select-type form field
	 * @return bool
	 */
	public function isSelect() : bool
	{
		return $this->fieldType() == 'TCv_FormItem_Select';
	}

	/**
	 * Whether this is using a model class instead of manual options
	 * @return bool
	 */
	public function usesModelOptions() : bool
	{
		// All models start with TMm_ so we know this is a model
		$sub = substr($this->select_options,0,4);
		return $sub === 'TMm_';
	}

	/**
	 * Returns an array of select options
	 * @return array|null
	 */
	public function selectOptions() : ?array
	{
		if ($this->isSelect() || $this->isRadioButtons())
		{
			// The DB value has 2 types. The options can be an array of strings, or a model list
			if ($this->usesModelOptions())
			{
				// The format is saved as a code 'list_class__subset_title'
				$values = explode('__',$this->select_options);

				/** @var TCm_ModelList|TMt_CustomFormModelListSelectable $list */
				$list = $values[0]::init();
				$method_name = $list->modelSubsets()[$values[1]]['method_name'];

				return $list->$method_name();
			}
			else
			{
				return json_decode($this->localizeProperty('select_options'));
			}
		}

		return null;
	}
	
	/**
	 * The placeholder text
	 * @return string
	 */
	public function selectEmptyValue() : string
	{
		if (!is_null($this->select_empty_value))
		{
			return $this->localizeProperty('select_empty_value');
		}
		
		return TC_localize('custom_form_select_option','Select an option');
	}
	
	/**
	 * Indicates if this field allows the "other" option for select fields.
	 * @return string
	 */
	public function selectShowOther() : string
	{
		
		return $this->select_show_other;
	}
	
	//////////////////////////////////////////////////////
	//
	// VISIBILITY
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether this form is visible, which dictates whether it shows up as an option to add to a page.
	 * @return bool
	 */
	public function isVisible() : bool
	{
		return $this->is_visible == 1;
	}

	/**
	 * Toggles whether this custom form is visible
	 */
	public function toggleVisible() : void
	{
		if ($this->isVisible())
		{
			$this->updateDatabaseValue('is_visible', 0);
		}
		else
		{
			$this->updateDatabaseValue('is_visible', 1);

		}
	}

	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Whether user can view this model in different spots on the site
	 * @param bool $user
	 * @return bool
	 */
	public function userCanView($user = false) : bool
	{
		if (TC_isTungstenView())
		{
			return parent::userCanView();
		}

		return $this->isVisible();
	}

	/**
	 * Whether user has permission to make permanent changes to this model
	 * @param bool $user
	 * @return bool
	 */
	public function userCanEdit ($user = false) : bool
	{
		return parent::userCanEdit($user);
	}
	
	/**
	 * Extende deletion permissions to not allow it if the form has submissions.
	 * @param $user
	 * @return bool
	 */
	public function userCanDelete($user = false) : bool
	{
		// Cannot delete a field, once submissions exist
		if($this->form()->numSubmissions() > 0)
		{
			return false;
		}
		return parent::userCanDelete($user);
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Extends the normal schema field localized to manually return true for some fields that need it but aren't in the schema.
	 * @param string $field_name
	 * @return bool
	 */
	public static function schemaFieldIsLocalized(string $field_name)
	{
		// Anything that is option_ with a number after it
		if(preg_match('/^option_\d.*/',$field_name))
		{
			return true;
		}
		
		return parent::schemaFieldIsLocalized($field_name);
	}

	/**
	 * Extends the schema for the custom form to add additional fields
	 * @return array[]
	 */
	public static function schema() : array
	{
		$types = "'".implode("','",array_keys(static::$field_types))."'";

		return parent::schema() + [
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The field label/title',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'localization'  => true,
				],
				'custom_form_id' => [
					'title'         => 'Form',
					'type' 			=> 'TMm_CustomForm',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name' => 'TMm_CustomForm',
						'delete'        => 'CASCADE'
					],
				],
				'field_type' => [
					'title'         => 'Field type',
					'comment'       => 'The field label/title',
					'type'          => 'enum('.$types.')',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
				],
				'help_text' => [
					'title'         => 'Help text',
					'comment'       => 'The field help text',
					'type'          => 'varchar(512)',
					'nullable'      => true,
					'localization'  => true,
				],
				'is_visible' => [
					'title'         => 'Visible',
					'comment'       => 'Whether this form field is in use.',
					'type'          => 'boolean DEFAULT 1',
					'nullable'      => false,
				],
				'is_required' => [
					'title'         => 'Required',
					'comment'       => 'Whether this form field is required when submitting.',
					'type'          => 'boolean DEFAULT 1',
					'nullable'      => false,
				],
				'display_order' => [
					'title'         => 'Display order',
					'type'          => 'smallint(5) unsigned',
					'nullable'      => false,
				],
				// Text Fields
				'placeholder_text' => [
					'title'         => 'placeholder text',
					'comment'       => 'The placeholder text used for text field form items',
					'type'          => 'varchar(512)',
					'nullable'      => true,
					'localization'  => true,
				],
				'character_limit' => [
					'title'         => 'Character limit, or max length',
					'type'          => 'smallint(5) unsigned DEFAULT 128',
					'nullable'      => true,
				],

				// Select columns
				'select_options' => [
					'title'         => 'Options',
					'comment'       => 'A JSON array of all select options.',
					'type'          => 'text',
					'nullable'      => true,
					'localization'	=> true,
				],
				'select_empty_value' => [
					'title'         => 'Select empty value',
					'type'          => 'varchar(512)',
					'nullable'      => true,
					'localization'  => true,
				],
				'select_show_other' => [
					'title'         => 'Indicates if the "other" option appears for this select field',
					'type'          => 'boolean',
					'nullable'      => false,
				],
			
			
			
			];
	}

}