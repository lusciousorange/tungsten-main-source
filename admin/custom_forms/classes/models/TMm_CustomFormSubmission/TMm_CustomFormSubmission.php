<?php
/**
 * Class TMm_CustomFormSubmission
 * The responses to a custom form. No matter what other action is taken, we want to save a copy
 *
 * @author Katie Overwater
 */
class TMm_CustomFormSubmission extends TCm_Model
{

	// DATABASE COLUMNS
	protected string $responses;
	protected ?array $response_values = null;
	protected ?int $custom_form_id, $user_id;


	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'custom_form_submission_id';
	public static $table_name = 'custom_form_submissions';
	public static $model_title = 'Submission';
	public static $primary_table_sort = 'custom_form_id ASC,date_added DESC';

	/**
	 * TMm_CustomFormSubmission constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
	}

	public function title(): string
	{
		return 'Submission '.$this->idNumber();
	}
	
	public function idNumber() : string
	{
		return '#' . str_pad($this->id(),6,'0',STR_PAD_LEFT);
	}
	
	/**
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		return TC_getConfig('saved_file_path').'/custom_forms/submissions/';
	}
	
	/**
	 * Returns the field display value for this submission
	 * @param string $field_id
	 * @return string
	 */
	public function fieldDisplayValue(string $field_id) : string
	{
		$responses = $this->responses();
		if(isset($responses[$field_id]))
		{
			if(is_null($responses[$field_id]['value']))
			{
				return '--';
			}
			return $responses[$field_id]['value'];
		}
		return '––';
	}
	
	/**
	 * parses the response values for the submission, processing relevant items as necessary
	 * @return array
	 */
	public function responses() : array
	{
		if(is_null($this->response_values))
		{
			$form_fields = $this->form()->formFields();
			
			$this->response_values = [];
			
			$json_values = json_decode($this->responses,true);
			foreach($json_values as $index => $response_value)
			{
				// Save the value that was submitted
				$value = $response_value['value'];
				
				$field_id = $response_value['field_id'];
				$this->response_values[$field_id] = [
					'value' => $value,
					'type' => $response_value['type'],
				];
				
				// Form fields can't be deleted, so they always exist here
				$form_field = $form_fields[$field_id];
				
				$this->response_values[$field_id]['field'] = $form_field;
				
				// Checkbox should be yes/no, instead of the number
				if($form_field->isCheckbox())
				{
					$this->response_values[$field_id]['value'] = $value ? 'Yes' : 'No';
				}
				
				// Save a model if provided
				if($response_value['type'] == 'model')
				{
					$model = TC_initClassWithContentCode($value);
					$this->response_values[$field_id]['model'] = $model;
				}
				
				// Update the select_other to show the real name, isntead of the unique code
				if($form_field->isSelect() && $value == 'select_other')
				{
					$this->response_values[$field_id]['value'] = "Other…";
					
				}
				
				
			}
			
		}
		return $this->response_values;
	}

	/**
	 * The form being responded to
	 * @return TMm_CustomForm
	 */
	public function form() : TMm_CustomForm
	{
		return TMm_CustomForm::init($this->custom_form_id);
	}
	
	/**
	 * Sends the notification email
	 * @return void
	 */
	public function sendNotificationEmail() : void
	{
		$email = new TMv_CustomFormNotificationEmail($this);
		$email->send();
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FILES/DOWNLOADS
	//
	//////////////////////////////////////////////////////
	
	public function zipFilename() : string
	{
		return 'submission_'.$this->id().'.zip';
	}
	
	/**
	 * The path where zip files are stored. These are generated outside the site root.
	 * @return string
	 */
	public function zipFilePath() : string
	{
		if(!is_dir(TMm_CustomForm::uploadFolder()))
		{
			mkdir(TMm_CustomForm::uploadFolder(), 0755);
		}
		
		$path = TMm_CustomForm::uploadFolder().'zip_files/';
		if(!is_dir($path))
		{
			mkdir($path, 0755);
		}
		$zip_filename = $this->zipFilename();
		
		return $path.$zip_filename;
	}
	
	public function generateZip() : void
	{
		// Generate it every time, just in case things shift and change
		//if( true || !file_exists($this->zipFilePath()))
		//{
		//	$this->addConsoleDebug('Creating ZIP: '.$this->zipFilePath());
		$zip = new ZipArchive;
		$zip->open($this->zipFilePath(), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		
		$zip->addFromString('form_values.txt', $this->infoFileText());
		
		foreach($this->responses() as $response_value)
		{
			$value = $response_value['value'];
			
			// Detect files and add them to the zip file
			if($response_value['type'] == 'file' && $value != '')
			{
				$zip->addFile(TMm_CustomForm::uploadFolder().$value, $value);
			}
		}
		
		$zip->close();
		//}
		
	}
	
	
	
	
	/**
	 * Generates a zip to be downloaded
	 */
	public function download() : void
	{
		// References
		// http://php.net/manual/en/class.ziparchive.php#116345
		
		$this->generateZip();
		
		header("Content-Type: application/zip");
		header("Content-Disposition: attachment; filename=".$this->zipFilename().'');
		header("Content-Length: " . filesize($this->zipFilePath()));
		
		readfile($this->zipFilePath());
		exit;
		
		
	}
	
	/**
	 * Returns the text for an info file
	 * @return string
	 */
	private function infoFileText() : string
	{
		$info_file = '';
		$info_file .= 'SUBMISSION VALUES'.PHP_EOL;
		$info_file .= '--------------------'.PHP_EOL;
		$info_file .= PHP_EOL;
		
		$info_file .= "Form : ".$this->form()->title().PHP_EOL.PHP_EOL;
		$info_file .= "Date : ".$this->dateAddedFormatted().PHP_EOL.PHP_EOL;
		
		foreach($this->responses() as $response)
		{
			$title = $response['field']->title();
			$value = $response['value'];
			if($response['type'] == 'model' && $response['model'] instanceof TCm_Model)
			{
				$value = $response['model']->title();
			}
			$info_file .= $title .  " : " . $value.PHP_EOL;
			$info_file .= PHP_EOL;
			
		}
		
		$info_file .= PHP_EOL;
		
		
		return $info_file;
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////


	/**
	 * Extends the schema for the custom form to add additional fields
	 * @return array[]
	 */
	public static function schema() : array
	{
		return parent::schema() + [
				'responses' => [
					'title'         => 'Response data',
					'comment'       => 'A formatted question:answer string',
					'type'          => 'text',
					'nullable'      => true,
				],
				'custom_form_id' => [
					'title'         => 'Form',
					'type' 			=> 'TMm_CustomForm',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					],
					'foreign_key'   => [
						'model_name' => 'TMm_CustomForm',
						'delete'        => 'CASCADE'
					],
				],
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The ID of the user, if any',
					'type'          => 'int(10)',
					'nullable'      => true,
				],
			];
	}

}