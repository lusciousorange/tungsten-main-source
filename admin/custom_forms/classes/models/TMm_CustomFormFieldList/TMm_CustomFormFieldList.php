<?php
/**
 * Class TMm_CustomFormFieldList
 * The basic list class for a TMm_CustomFormField.
 *
 * @author Katie Overwater
 */
class TMm_CustomFormFieldList extends TCm_ModelList
{

	/**
	 * TMm_CustomFormFieldList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_CustomFormField',$init_model_list);

	}

	//////////////////////////////////////////////////////
	//
	// FILTERING
	//
	//////////////////////////////////////////////////////

	/**
	 * Processes the filter values and returns a PDOStatement of the users
	 * @param array $filter_values
	 * @return PDOStatement
	 */
	public function processFilterList($filter_values) : PDOStatement
	{
		$query = "SELECT * FROM `custom_form_fields` ";
		$db_values = array();
		$where_clauses = array();


		if(isset($filter_values['search']) && $filter_values['search'] != '')
		{
			$where_clauses[] = "(title LIKE :search)";
			$db_values[':search'] = $filter_values['search'].'%';
		}

		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
		}

		$query .= "  ORDER BY ".TMm_CustomFormField::$primary_table_sort;

		return $this->DB_Prep_Exec($query, $db_values);
	}

}
