<?php
/**
 * Class TMm_CustomFormList
 * The basic list class for a TMm_CustomForm.
 *
 * @author Katie Overwater
 */
class TMm_CustomFormList extends TCm_ModelList
{

	/**
	 * TMm_CustomFormList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_CustomForm',$init_model_list);
	}

	//////////////////////////////////////////////////////
	//
	// FILTERING
	//
	//////////////////////////////////////////////////////

	/**
	 * Processes the filter values and returns a PDOStatement of the users
	 * @param array $filter_values
	 * @return PDOStatement
	 */
	public function processFilterList($filter_values) : PDOStatement
	{
		$query = "SELECT * FROM `custom_forms` ";
		$db_values = array();
		$where_clauses = array();


		if(isset($filter_values['search']) && $filter_values['search'] != '')
		{
			$where_clauses[] = "(title LIKE :search)";
			$db_values[':search'] = $filter_values['search'].'%';
		}

		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
		}

		$query .= "  ORDER BY ".TMm_CustomForm::$primary_table_sort;

		return $this->DB_Prep_Exec($query, $db_values);
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
}
