<?php
/**
 * Class TMm_CustomFormSubmissionList
 * The basic list class for a TMm_CustomFormSubmission.
 *
 * @author Katie Overwater
 */
class TMm_CustomFormSubmissionList extends TCm_ModelList
{

	/**
	 * TMm_CustomFormSubmissionList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_CustomFormSubmission',$init_model_list);

	}
}
