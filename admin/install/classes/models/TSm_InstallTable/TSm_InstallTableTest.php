<?php

class TSm_InstallTableTest extends TC_ModelTestCase
{
	protected $table;
	
	protected function setUp(): void
	{
		parent::setUp();
		
		$this->table = new TSm_InstallTable('table_name',[]);
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA CONVERSION
	//
	// Methods related to new schema() functions we have in models
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Tests the generation of a basic schema with just a field and a type
	 */
	public function testGenerateDefinitionForSchemaValues_basicText()
	{
		$field_id = 'field';
		$schema_values = [
			'type' => 'text'
		];
		
		$definition = $this->table->generateDefinitionForSchemaValues($field_id, $schema_values);
		$this->assertEquals('field text', $definition,'basic text failed');
	}
	
	/**
	 * Tests when the type is a model name instead of a known type
	 */
	public function testGenerateDefinitionForSchemaValues_classNameAsType()
	{
		$field_id = 'field';
		$schema_values = [
			'type' => 'TMm_User' // any class name
		];
		
		$definition = $this->table->generateDefinitionForSchemaValues($field_id, $schema_values);
		$this->assertEquals('field int(10) unsigned', $definition);
	}
	
	/**
	 * Tests when the type is a model name instead of a known type
	 */
	public function testGenerateDefinitionForSchemaValues_nullable()
	{
		// NULLABLE TRUE
		$field_id = 'field';
		$schema_values = [
			'type' => 'text',
			'nullable' => true
		];
		
		$definition = $this->table->generateDefinitionForSchemaValues($field_id, $schema_values);
		$this->assertEquals('field text', $definition);
		
		// NULLABLE FALSE
		$field_id = 'field';
		$schema_values = [
			'type' => 'text',
			'nullable' => false
		];
		
		$definition = $this->table->generateDefinitionForSchemaValues($field_id, $schema_values);
		$this->assertEquals('field text NOT NULL', $definition);
		
		
	}
	
	/**
	 * Tests when the type has a comment
	 */
	public function testGenerateDefinitionForSchemaValues_comments()
	{
		$field_id = 'field';
		$schema_values = [
			'type' => 'text' ,
			'comment' => 'Something'
		];
		
		$definition = $this->table->generateDefinitionForSchemaValues($field_id, $schema_values);
		$this->assertEquals('field text COMMENT "Something"', $definition);
	}
	
	
	/**
	 * Test the addition of a table column definition
	 */
	public function testSaveTableColumnDefinition()
	{
		$definition = "field123 text";
		$this->table->saveTableColumnDefinition($definition);
		
		
		$this->assertEquals('field123 text', $this->table->columnDefinition('field123'));
	}
	
	
	
}