<?php

/**
 * Class TSm_InstallTable
 *
 * Represents a table that is meant to be installed on the server.
 */
class TSm_InstallTable extends TCm_Model
{
	/** @var ?class-string<TCm_Model> $class_name */
	protected ?string $class_name = null;
	
	protected ?string $table = null;
	
	/** @var bool Indicates if we are in mirror-mode where we install the mirror tables and any foreign keys loop up
	 * the mirror tables for classes as well.
	 */
	protected bool $mirror_mode = false;
	protected $indices = [];
	protected $columns = [];
	protected $constraints = [];
	protected $primary_key  = null;
	protected $delete_columns = [];
	protected $existing_columns = [];
	protected $install_queries = [];
	protected bool $is_auto_increment = true;
	
	/**
	 * @var ?TMm_LocalizationModule
	 * The localization module if installed and being used, otherwise false
	 */
	protected $localization = null;
	
	/**
	 * @var null|bool
	 * Indicates if this table exists in the system already
	 */
	protected ?bool $table_exists = null;
	
	/**
	 * Returns the table name for a given model> This is used to consistently acquire the correct table, accounting
	 * for special cases like mirror table mode.
	 * @param class-string<TCm_Model> $model_name
	 * @return string
	 */
	protected function modelTableName(string $model_name) : string
	{
		$table = $model_name::tableName();
		if($this->mirror_mode)
		{
			$table .= $model_name::$mirror_table_suffix;
		}
		
		return $table;
	}
	
	/**
	 * Stores the current indices if they are called
	 * @var null|array
	 */
	protected ?array $current_indices = null;
	
	/**
	 * Stores the query for "SHOW TABLE" which helps determine the current settings
	 * @var null|string
	 */
	protected ?string $show_table_query = null;
	
	/**
	 * TSm_InstallTable constructor.
	 * @param string|class-string<TCm_Model> $name The name of the table OR the name of a class. If a class is found and it's a model,
	 * then we use the table and schema for that class
	 * @param array|string $table_definition The definition which is hopefully an array coming from a schema().
	 * However it can be a table definition string as well. 
	 */
	public function __construct(string $name, $table_definition)
	{
		// Check for a class_name
		if(class_exists($name))
		{
			$this->class_name = $name;
			$name = $this->modelTableName($this->class_name);
			
		}
		parent::__construct($name);
		$this->table = $name;
		
		if(TC_moduleWithNameInstalled('localization') && class_exists('TMm_LocalizationModule'))
		{
			$this->localization = TMm_LocalizationModule::init();
		}
		
		// GO RIGHT TO THE SCHEMA
		if($this->class_name)
		{
			$this->processModelSchema($this->class_name);
		}
		
		// Check for copy/pasted table declaration
		elseif(is_string($table_definition))
		{
			$this->parseMySQLString($table_definition);
		}
		elseif(is_array($table_definition))
		{
			if(isset($table_definition['primary_key']))
			{
				$this->savePrimaryKey($table_definition['primary_key']);
			}
			if(isset($table_definition['columns']))
			{
				foreach($table_definition['columns'] as $column_definition)
				{
					$this->saveTableColumnDefinition($column_definition);
				}
			}
			if(isset($table_definition['indices']))
			{
				foreach($table_definition['indices'] as $field_id => $value)
				{
					$this->saveTableColumnIndex($field_id, $value);
				}
			}
			if(isset($table_definition['foreign_key']))
			{
				foreach($table_definition['foreign_key'] as $field_id => $value)
				{
					$this->processForeignKeyValue($field_id, $value);
				}
			}
			
			
			
			
		}
		
	}
	
	/**
	 * Returns the name of the table
	 * @return string
	 */
	public function installTableName() : string
	{
		return $this->table;
	}
	
	/**
	 * Returns the columns for this table
	 * @return array
	 */
	public function columns() : array
	{
		return $this->columns;
	}
	
	/**
	 * Returns the constraints for this table
	 * @return array
	 */
	public function constraints() : array
	{
		return $this->constraints;
	}
	
	//////////////////////////////////////////////////////
	//
	// Indices
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the indices for this table
	 * @return array
	 */
	public function indices() : array
	{
		return $this->indices;
	}
	
	/**
	 * Returns the index for a specific field ID
	 * @param string $field_id
	 * @return mixed|null
	 */
	public function indexForField(string $field_id)
	{
		if(isset($this->indices[$field_id]))
		{
			return $this->indices[$field_id];
		}
		
		return null;
	}
	
	/**
	 * Returns if an index already exists for this table
	 * @param string $column_name
	 * @return bool
	 */
	protected function indexExists(string $column_name) : bool
	{
		if($this->current_indices == null)
		{
			$this->current_indices = [];
			$query = "SHOW INDEX FROM `".$this->installTableName()."`";
			$result = $this->DB_Prep_Exec($query);
			while($row = $result->fetch())
			{
				$this->current_indices[$row['Key_name']] = $row['Key_name'];
			}
		}
		
		return isset($this->current_indices[$column_name]);
		
	}
	
	/**
	 * Adds the indices to the table
	 */
	protected function addIndices() : void
	{
		foreach($this->indices() as $name => $values)
		{
			$index = $values['index'];
			$type = $values['type'];
			if($this->indexExists($name))
			{
				continue; // Move onto the next index
			}
			
			if(strtolower($type) == 'unique')
			{
				$index = str_replace(['(',')'],'', $index); // Remove brackets
				$index = str_replace(['"',"'"],'`', $index); // Ensure ` for strings
				
				$query = "ALTER TABLE `".$this->installTableName()."` ADD UNIQUE `$name` ($index)";
				
			}
			else
			{
				$query = "CREATE INDEX `$name` ON `".$this->installTableName()."` ($index)";
			}
			$this->DB_Prep_Exec($query);
	
			// Update the tracking to avoid errors mid-processing
			$this->current_indices[$name] = $name;
		}
	}
	

	//////////////////////////////////////////////////////
	//
	// Primary Keys
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the primary key
	 * @return mixed|null
	 */
	public function primaryKey() : ?string
	{
		return $this->primary_key;
	}
	
	/**
	 * Returns if this tables has a primary key
	 * @return bool
	 */
	public function hasPrimaryKey() : bool
	{
		return $this->primary_key != null && $this->primary_key != false;
	}
	
	/**
	 * Saves the primary key
	 * @param string $key
	 */
	public function savePrimaryKey(string $key) : void
	{
		// Trim empty spaces as well as brackets
		$this->primary_key = trim($key, "\t\n\r\0\x0B()");
	}
	
	/**
	 * Sets if this table uses auto increment for the primary key
	 * @param bool $is_auto_increment
	 * @return void
	 */
	public function setPrimaryKeyAutoIncrement(bool $is_auto_increment = true) : void
	{
		$this->is_auto_increment = $is_auto_increment;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// Parsing
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Parses regular MySQL `CREATE TABLE` calls and converts them into an array of values that can be safely parsed
	 * by the installer.
	 * @param string $mysql_string The input string that needs to be converted into a table value
	 */
	public function parseMySQLString(string $mysql_string) : void
	{
		
		$lines = explode(PHP_EOL, $mysql_string);
		
		// Loop through each line to convert it into a column definition
		foreach($lines as $line)
		{
			$line = trim($line);
			$line_uppercase = strtoupper($line);
			if($line == ''
				|| substr($line_uppercase, 0, 12) == 'CREATE TABLE'
				|| strpos($line_uppercase, 'ENGINE=') != false
			)
			{
				// DO NOTHING
			}
			elseif(substr($line_uppercase, 0, 12) == 'PRIMARY KEY ')
			{
				// PRIMARY KEY
				$primary_line = trim(str_ireplace('PRIMARY KEY ', '', $line), ' ,');
				
				$column_name = str_ireplace(array('`', '(', ')'), '', $primary_line);
				$column_name = explode(' ', $column_name);
				$column_name = $column_name[0]; // grab the first
				$this->savePrimaryKey($column_name);
				
				// Remove the column name, it will be created as part of the creation if necessary
				// These have a specific format in Tungsten
				unset($this->columns[$column_name]);
			}
			elseif(substr($line_uppercase, 0, 4) == 'KEY ')
			{
				// REGULAR KEY
				
				// Find the column name
				$bracket_start = strpos($line, '(');
				
				$bracket_end = strpos($line, ')');
				$column_name = substr($line, $bracket_start, $bracket_end - $bracket_start + 1);
				$column_name = str_ireplace('`', '', $column_name);
				$column_name = trim($column_name, '(');
				
				// Deal with possibility of a charlimit
				// We either have `field(24)` or `field)`
				if(substr_count($column_name, '(') == 0)
				{
					$column_name = trim($column_name, ')');
				}
				
				$field_id = str_ireplace(array("(", ")"), '', $column_name);
				$this->saveTableColumnIndex($field_id, $column_name);
				
			}
			elseif(substr($line_uppercase, 0, 11) == 'CONSTRAINT ')
			{
				// CONSTRAINT
				$parts = explode(' ', $line);
				
				// remove the "foreign key" part
				array_shift($parts); // "CONSTRAINT"
				array_shift($parts); // "constraint_name"
				array_shift($parts); // "FOREIGN"
				array_shift($parts); // "KEY"
				
				// put the rest back together
				$column_name = $parts[0];
				$column_name = str_ireplace(array('`', '(', ')'), '', $column_name);
				
				$constraint = trim(implode(' ', $parts), ',');
				$this->processForeignKeyValue($column_name, $constraint);
			}
			else // We have a regular line
			{
				$line = trim($line, ", ");
				
				$this->saveTableColumnDefinition($line);
			}
		}
		
	}
	
	
	/**
	 * Saves a column index for a table
	 * @param string $field_id The name of the field that we are adding an index
	 * @param string|bool $index The index to be added. a value of `true` indicates that it should just used the field ID
	 * @param string $type (Default "normal" Indicates the type
	 */
	public function saveTableColumnIndex(string $field_id, string|bool $index, string $type = 'normal') : void
	{
		// If the value is just true, then we use the field ID, which is easier to document
		if($index === true)
		{
			$index = $field_id;
		}
		
		$this->indices[$field_id] = ['index' => $index, 'type' => $type];
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// CONSTRAINTS AND FOREIGN KEYS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Saves a constraint (often foreign keys) for the table
	 * @param string $name
	 * @param string $constraint
	 */
	public function saveTableConstraint(string $name, string $constraint) : void
	{
		$this->constraints[$name] = $constraint;
	}
	
	/**
	 * Returns the constraints with a given name for a given column
	 * @param string $name
	 * @return string
	 */
	public function constraint(string $name) : string
	{
		return $this->constraints[$name];
	}
	
	/**
	 * A standardized method for returning the name of a foreign key constraint based on the column and table names.
	 * @param string $column_name
	 * @param string $foreign_table (Optional) Default ''. The name of the foreign table being referenced.
	 * @return string
	 */
	public function foreignKeyConstraintName(string $column_name, string $foreign_table = '') : string
	{
		$name = $this->table.'_'.$column_name;
		if($foreign_table != '')
		{
			$name .= '_'.$foreign_table;
		}
		return  substr($name,0,64); // maximum of 64 characters
		
	}
	
	/**
	 * Processes a foreign key value into an actual constraint string. These have been defined in various ways over
	 * the years, so this method is designed to detect three scenarios.
	 *
	 * 1. The value is just another table name. This is a shorthand to indicate that the column name in the table is
	 * the same as it is for this table, and it is meant to be a constraint that sets ON DELETE to CASCADE
	 *
	 * 2. The value is an array with a model name value. This then references the table for that model and the the
	 * key column is the primary key for that table. There then requires additional values of either 'delete' or
	 * 'update' which will correspond to what should happen ON DELETE or ON UPDATE
	 *
	 * 3. The value is a string that doesn't fit the pattern and should be added as a constraints as-is. This falls
	 * into two categories since it's possible to have start with "UNIQUE" which shouldn't have foreign key
	 * @param string $column_name
	 * @param string|array $value
	 */
	public function processForeignKeyValue(string $column_name, $value) : void
	{
		// SCENARIO 1 : Shortcut used in original Tungsten table definitions
		if(is_string($value) && strpos($value,' ') === false) // string with no spaces
		{
			// Generates a foreign key on delete, cascade for the table and the column name
			$symbol = $this->foreignKeyConstraintName($column_name, $value);
			$this->saveTableConstraint($symbol,
			                           "FOREIGN KEY (".$column_name.") REFERENCES ".$value." (".$column_name.") ON DELETE CASCADE");
			
			
		}
		
		// SCENARIO 2 : An array coming from a schema() declaration in an model
		elseif(is_array($value))
		{
			/** @var class-string<TCm_Model> $model_name */
			$model_name = $value['model_name'];
			$foreign_table = $this->modelTableName($model_name);
		
			
			// Detect symbol values
			if(isset($value['symbol_name']))
			{
				$symbol = $value['symbol_name'];
			}
			else
			{
				$symbol =  $this->foreignKeyConstraintName($column_name, $foreign_table);
				
			}
			$foreign_primary_key = $model_name::$table_id_column;
			$constraint = "FOREIGN KEY (".$column_name.") REFERENCES ".$foreign_table." (".$foreign_primary_key.")";
			if(isset($value['delete']))
			{
				$constraint .= ' ON DELETE '.$value['delete'];
			}
			if(isset($value['update']))
			{
				$constraint .= ' ON UPDATE '.$value['update'];
			}
			$this->saveTableConstraint($symbol, $constraint);
		}
		
		// SCENARIO 3 : A string is provided which is likely a constraint that doesn't fit the mold
		elseif(is_string($value))
		{
			$symbol = $this->foreignKeyConstraintName($column_name, '');
			$constraint_string = trim($value);
			
			// UNIQUE KEYS
			if(strtoupper(substr($constraint_string,0,6)) == 'UNIQUE')
			{
				$this->saveTableConstraint($symbol, $constraint_string);
			}
			else
			{
				$this->saveTableConstraint($symbol, "FOREIGN KEY ".$constraint_string);
			}
		}
		
		// Create the index as well
		$this->saveTableColumnIndex($column_name,$column_name);
	
	}
	
	/**
	 * Adds the constraints to the table
	 */
	public function addConstraints() : void
	{
		foreach($this->constraints() as $symbol => $constraint)
		{
			// Constraint doesn't exist yet
			if(!$this->constraintExists($symbol))
			{
				$query = "ALTER TABLE ".$this->installTableName()." ADD CONSTRAINT ".$symbol.' '.$constraint;
				$this->DB_Prep_Exec($query);
				
			}
			
		}
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// MODEL SCHEMAS
	// The more modern way we add schemas is via the model's
	// schema() method
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Processes a schema for a model which is added to and replaces any values currently set for this table. These
	 * are often loaded via installer or other systems which is why it's loaded after the fact, rather than as part
	 * of the init.
	 *
	 * There's a lot of legacy support going on here, so the default is the "old way" and these new versions take
	 * precedence by being loaded afterwards. It's used in `TSu_InstallProcessor` primarily.
	 * @param class-string<TCm_Model> $class_name The class name that uses this schema for this table
	 */
	public function processModelSchema(string $class_name) : void
	{
		// Sanity check to make sure we never cross the wrong values. Classes have tables, so we can check that
		if($this->modelTableName($class_name) != $this->table)
		{
			TC_triggerError('Table mismatch in processing schema() attempting to load '.$class_name.' table for '
			                .$this->table);
		}
		
		// Save the primary key
		$this->savePrimaryKey($class_name::$table_id_column);
		$this->setPrimaryKeyAutoIncrement($class_name::$table_use_auto_increment);
		
		
		// Process the schema for the class
		foreach($class_name::schema() as $field_id => $schema_field_values)
		{
			
			// Handle delete columns
			if(isset($schema_field_values['delete']))
			{
				$this->saveDeleteColumn($field_id);
				
				// Don't do anything else for this field
				continue;
			}
			
			// Detect the special table keys  table commands
			if(strtolower($field_id) == 'table_keys')
			{
				foreach($schema_field_values as $key_name => $key_values)
				{
					if(!isset($key_values['type']))
					{
						$key_values['type'] = 'normal';
					}
					
					// Detect if it's a foreign key
					// Then we use the key name as the column name and pass in all the values to be dealt with
					// by the installer
					if($key_values['type'] == 'foreign_key')
					{
						$this->processForeignKeyValue($key_name, $key_values);
					}
					else // traditional table key
					{
						$this->saveTableColumnIndex($key_name, $key_values['index'], $key_values['type']);
					}
					
				}
				// Don't do anything else for this field
				continue;
			}
			
			$definition = $this->generateDefinitionForSchemaValues($field_id, $schema_field_values);
			$this->saveTableColumnDefinition($definition);
			
			// Handle the index value if it exists
			if(isset($schema_field_values['index']))
			{
				$this->saveTableColumnIndex($field_id, $schema_field_values['index']);
			}
			
			// Handle the index value if it exists
			if(isset($schema_field_values['foreign_key']))
			{
				$this->processForeignKeyValue($field_id, $schema_field_values['foreign_key']);
			}
			
			// Handle localization
			if(TC_getConfig('use_localization')
				&& isset($schema_field_values['localization'])
				&& $this->localization)
			{
				// add definition for each non-english language
				foreach($this->localization->nonDefaultLanguages() as $language => $language_settings)
				{
					$localized_field_id = $field_id.'_'.$language;
					
					// add language to the comment
					$schema_field_values['comment'] .= ' ['.$language_settings['title'].']';
					$definition = $this->generateDefinitionForSchemaValues($localized_field_id, $schema_field_values);
					$this->saveTableColumnDefinition($definition);
				}
			}
			
		}
		
		// Save the install queries
		$this->install_queries = $class_name::installUpdateQueries();
		
	}
	
	/**
	 * Runs the queries that are meant to be run after a particular version
	 * @param ?string $current_version
	 */
	public function runInstallQueriesAfterVersion(?string $current_version) : void
	{
		// Don't bother with fresh installs
		if(is_null($current_version))
		{
			return;
		}
		
		foreach($this->install_queries as $version => $queries)
		{
			if(version_compare($version, $current_version, '>'))
			{
				foreach($queries as $query)
				{
					if(is_array($query))
					{
						list($class_name, $method) = $query;
						//$class = $class_name::init();
						call_user_func([$class_name, $method]);
					}
					else
					{
						$this->DB_Prep_Exec($query);
					}
					
				}
			}
		}
	}
	
	/**
	 * Generates a schema definition from the values provided
	 * @param string $field_id
	 * @param array $schema_values
	 * @return string
	 */
	public function generateDefinitionForSchemaValues(string $field_id, array $schema_values) : string
	{
		$definition =  $field_id;
		
		$type = $schema_values['type'];
		
		// Detect a classname, change the type to be the same value we use for all primary keys
		if(substr($type, 0,1) == 'T' && substr($type, 2,1) == 'm')
		{
			$definition_found = false;
			
			// Potential class name
			if(class_exists($type))
			{
				/** @var TCm_Model|string $reference_class_name */
				$reference_class_name = $type;
				$schema = $reference_class_name::schema();
				// Primary Key defined in schema
				if(isset($schema[$reference_class_name::$table_id_column]))
				{
					// Get the type from the primary key definition
					$definition .= ' '.$schema[$reference_class_name::$table_id_column]['type'];
					$definition_found = true;
				}
			}
			
			if(!$definition_found)
			{
				$definition .= ' int(10) unsigned';
			}
			
		}
		else
		{
			$definition .= ' '.$type;
		}
		
		// Handle nullable
		if(isset($schema_values['nullable']) && !$schema_values['nullable'])
		{
			$definition .= ' NOT NULL';
			
			$default_value = $this->defaultValueForType($type, $definition);
			if($default_value)
			{
				$definition .= " DEFAULT ".$default_value;
				
			}
			
		}
		else // POSSIBLY NEED TO EXPLICITLY SET NULL
		{
			if( !str_contains($definition,' as ' ) // Calculated column
				&& !str_contains($definition,'default ') // Already has default
			)
			{
				$definition .= ' DEFAULT NULL';
			}
			
		}
		
		// COMMENTS
		if(isset($schema_values['comment']))
		{
			$definition .= ' COMMENT "'.$schema_values['comment'].'"';
		}
		
		return $definition;
	}
	
	/**
	 * Returns the default value for a given type in a schema
	 * @param string $type
	 * @param string $definition The existing definition string
	 * @return string|null
	 */
	protected function defaultValueForType(string $type, string $definition) : ?string
	{
		// Force lowercase for comparisons
		$type = strtolower($type);
		
		// Not a calculated column and doesn't have a default already
		if( !str_contains(strtolower($definition),' as ' )
			&& !str_contains(strtolower($definition),'default ')
		)
		{
			// STRINGS have empty strings
			if(str_contains($type, 'char')
				|| str_contains($type,'char' ))
			{
				return "''";
			}
			
			// NUMBERS default to 0
			if(str_contains($type, 'int')
				|| $type == 'float'
				|| $type == 'decimal'
				|| $type == 'double'
				|| $type == 'numeric'
			
			)
			{
				return "'0'";
			}
			
			// BOOLEANs
			if(str_contains($type, 'bool'))
			{
				return "'0'";
			}
		}
		
		
		return null;
	}
	
	/**
	 * Saves the definition for a particular column in this table
	 * @param string $definition The definition for a column in that table
	 */
	public function saveTableColumnDefinition(string $definition) : void
	{
		// Get rid of any backticks which are often part of the declarations
		$definition = str_ireplace('`', '', $definition);
		
		$column_name = explode(' ', trim($definition), 2);
		$column_name = $column_name[0];
		$this->columns[$column_name] = $definition;
	}
	
	/**
	 * Returns the column definition for a given column
	 * @param string $column_name
	 * @return string
	 */
	public function columnDefinition(string $column_name) : string
	{
		return $this->columns[$column_name];
	}
	
	//////////////////////////////////////////////////////
	//
	// COLUMN DELETION
	// Methods related to deleting columns that might already exist
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Saves a column as deletable
	 * @param string $column_name
	 */
	protected function saveDeleteColumn(string $column_name) : void
	{
		$this->delete_columns[$column_name] = $column_name;
	}
	
	/**
	 * Deletes the columns for this table
	 */
	protected function processDeletedColumns() : void
	{
		foreach($this->delete_columns as $column_name)
		{
			// These are set at the start of install()
			if(isset($this->existing_columns[$column_name]))
			{
				$query = 'ALTER TABLE '.$this->installTableName().' DROP '.$column_name ;
				$this->DB_Prep_Exec($query);
				$this->addConsoleError('Delete Column  : '.$column_name);
				
			}
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// INSTALLATION
	// Methods related to the installation of a table
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Installs the table
	 */
	public function install() : void
	{
		try
		{
			$this->addConsoleTimer("INSTALL: ".$this->installTableName());
			$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 0');
			
			// No transactions, since internal calls will commit the transaction, leading to errors when it tries again
			
			$this->processTableInstallation();
			
			// Handles creating/updating the history tables and settings associated with it
			$this->processInstallHistorySetup();
			
			
			
			// Detect mirror tables, defined by having a static string value for $mirror_table_suffix
			// This is appended to the table name
			if($this->class_name)
			{
				if(isset(($this->class_name)::$mirror_table_suffix))
				{
					$this->table .= ($this->class_name)::$mirror_table_suffix;
				//	$this->addConsoleDebug('Install MIRROR: '.$this->table);
					
					// Change the mode and process the schema again
					$this->mirror_mode = true;
					
					// Clear out values that need to be acquired again
					$this->constraints = []; // Clear out any existing constraints
					$this->show_table_query = null; // Cached query results should be null
					$this->processModelSchema($this->class_name);
					
					// Add the column related to original_id
					$this->columns['original_id'] = "original_id int(10) unsigned ";
					// DO it again for the mirror table, no history
					
				//	$this->addConsoleDebug('MIRROR TABLE INSTALL');
				//	$this->addConsoleDebug($this->constraints);
//					$this->constraints = [];
					$this->processTableInstallation();
					
				}
			}
			
			
			$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 1');
			
		}
		catch (Exception $e)
		{
			$this->DB()->rollBack($this->installTableName().' install did not succeed');
		}
	
		
	}
	
	/**
	 * Processes the core steps in installing a table
	 */
	public function processTableInstallation() : void
	{
		// Attempt to create the table if it doesn't exist
		// This function will add teh rows at one time or incrementally
		if($this->tableExists())
		{
			$this->addTableRows();
		}
		else
		{
			$this->createTable();
		}
		
		// Handle Indices
		$this->addIndices();
		
		// Handle constraints
		$this->addConstraints();
		
		// Handle deleted tables
		$this->processDeletedColumns();
	}
	
	/**
	 * Returns if this table exists
	 * @return bool
	 */
	public function tableExists() : bool
	{
		return static::DB_Connection()::tableInstalled($this->table);
	}
	
	
	/**
	 * Creates the table in the database
	 */
	protected function createTable() : void
	{
		// Track the primary_key
		if($this->hasPrimaryKey())
		{
			if(!isset($this->columns[$this->primaryKey()]))
			{
				$declaration = $this->primaryKey() . " int(10) unsigned NOT NULL";
				if($this->is_auto_increment)
				{
					$declaration .= ' auto_increment';
				}
				
				// pass in an associative array for consistency, and to avoid doubleups
				$this->columns = array_merge([$this->primaryKey() => $declaration], $this->columns );
				
			}
		}
		
		// Only create if it doesn't exist
		$query = "CREATE TABLE IF NOT EXISTS `" . $this->installTableName() . "`";
		$query .= "( ";
		
		// Add all the columns, which will include the private key
		$query .= "\n\t" . implode(",\n\t", $this->columns());
		
		if($this->hasPrimaryKey())
		{
			$query .= ",\n\tPRIMARY KEY (" . $this->primaryKey() . ")";
		}
		
		$query .= "\n) ENGINE=InnoDB CHARSET=utf8;";
		$this->DB_Prep_Exec($query);
		
		// Update the history of tables
		(static::DB_Connection())::$installed_tables[$this->installTableName()] = $this->installTableName();
		
		
	}
	
	
	
	/**
	 * Creates a series of rows in the table provided. If the row does not exist then it is added, if it does exist
	 * then the table is modified to use the new setup provided.
	 *
	 */
	public function addTableRows() : void
	{
		// Save in variable for easier to read code
		$table_name = $this->installTableName();
		
		
		$result = $this->DB_Prep_Exec("SHOW COLUMNS FROM `".$table_name."`");
		while($row = $result->fetch())
		{
			$this->existing_columns[$row['Field']] = true;
		}
		
		if($result)
		{
			foreach($this->columns() as $statement)
			{
				$column_name = str_ireplace('`', '', strtok($statement, " ")); // first word, remove possible ` around column name
				if(isset($this->existing_columns[$column_name]))
				{
					$query = "ALTER TABLE `".$table_name."` MODIFY COLUMN $statement";
				}
				else
				{
					$query  = "ALTER TABLE `".$table_name."` ADD COLUMN $statement";
				}
				
				if(!$this->DB_Prep_Exec($query))
				{
					$message = "Cannot add column to table <em>".$table_name."</em> using <em>".htmlspecialchars($statement)."</em>";
					TC_trackProcessError($message,'install-cannot-add-column');
				}
				
			}
		}
		else
		{
			// NO TABLE DEFINED
			if(!$table_name)
			{
				$message = "Error with addTableRows: No table is defined";
				TC_trackProcessError($message,'install-no-table');
			}
			// NO COLUMNS FOUND, WEIRD ERROR
			else
			{
				$message = "Cannot find columns for table <em>".$table_name."</em> ";
				TC_trackProcessError($message,'install-show-column-fail');
			}
		}
		//TC_triggerError('Rebuilding tables');
		
	}
	
	/**
	 * Internal method to save the "SHOW TABLES" query result.
	 */
	protected function saveShowTableQuery() : void
	{
		if($this->show_table_query == null)
		{
			$query = "SHOW CREATE TABLE `" . $this->installTableName()."`";
			$result = $this->DB_Prep_Exec($query);
			$row =  $result->fetch();
			$this->show_table_query =$row['Create Table'];
		}
	}
	
	/**
	 * Indicates if the table has a constraint with the provided symbol name.
	 * @param string $symbol_name
	 * @return bool
	 */
	protected function constraintExists(string $symbol_name) : bool
	{
		$this->saveShowTableQuery();

		return strpos($this->show_table_query, "CONSTRAINT `$symbol_name`") > 0;
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * @return string
	 */
	public function historyTableName() : string
	{
		return '_history_'.$this->installTableName();
	}
	
	/**
	 * Installs and updates the history functionality
	 */
	protected function processInstallHistorySetup() : void
	{
		// Only bother if the feature is enabled.
		if(TC_getConfig('use_track_model_history'))
		{
			// Only bother with history if there's a class name to test for
			if(class_exists($this->class_name) )
			{
				if($this->class_name::hasTrait('TSt_ModelWithHistory'))
				{
					$this->processHistoryTable();
					$this->processHistoryTriggers();
				}
				else
				{
					$this->deleteHistorySetup();
				}
				
				
			}
			
		}
	}
	
	/**
	 * Handle the creation/updating of the history table. This creates a duplicate of the current table, but with a
	 * few columns adjusted.
	 */
	public function processHistoryTable() : void
	{
		
		// Only bother if the feature is enabled.
		if(TC_getConfig('use_track_model_history'))
		{
			
			$table_name = $this->historyTableName();
			
			// Make a COPY of the columns
			$columns = array_merge([],$this->columns()) ;
			
			// Rewrite the primary key, put it first
			unset($columns[$this->primaryKey()]);
			
			$columns = array_merge(
				[$this->primaryKey() => $this->primaryKey()." int(10) unsigned"],
				$columns);
			$columns['_history_id'] = '`_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT';
			$columns['_history_version'] = '`_history_version` varchar(255) DEFAULT NULL';
			$columns['_history_action'] = '`_history_action` varchar(255) DEFAULT NULL';
			$columns['_history_timestamp'] = '`_history_timestamp` timestamp NULL DEFAULT NULL';
			$columns['_history_user_id'] = '`_history_user_id` int(10) unsigned NULL DEFAULT NULL';
			
			// Table
			if(static::DB_Connection()::tableInstalled($table_name))
			{
				// Updating rows
				
				// Get the columns for the table
				$result = $this->DB_Prep_Exec("SHOW COLUMNS FROM `".$table_name."`");
				
				while($row = $result->fetch())
				{
					$history_existing_columns[$row['Field']] = true;
				}
				
				$ignored_columns = ($this->class_name)::historyAllIgnoredColumns();
				
				foreach($columns as $column_name => $column_definition)
				{
					// Don't try to install any of the ignored columns
					if(in_array($column_name, $ignored_columns))
					{
						continue;
					}
					
					
					// Avoid any not-null properties
					// These are copies so we want accurate values and if something was missed, we want to see that happen.
					// If columns get added later, it would change the history by backfilling values that didn't exist.
					
					if(substr($column_name,0,9) != '_history_')
					{
						$column_definition = str_ireplace('NOT NULL', 'NULL', $column_definition);
					}
					
					if(isset($history_existing_columns[$column_name]))
					{
						$query = "ALTER TABLE `".$table_name."` MODIFY COLUMN ".$column_definition;
					}
					else
					{
						$query  = "ALTER TABLE `".$table_name."` ADD COLUMN ".$column_definition;
					}
					$this->DB_Prep_Exec($query);
					
				}
				
				
			}
			else
			{
				// creating the table
				$query = "CREATE TABLE IF NOT EXISTS `" . $table_name . "`";
				$query .= "( ";
				
				// Add all the columns, which will include the private key
				$query .= "\n\t" . implode(",\n\t", $columns);
				
				$query .= ",\n\tPRIMARY KEY (_history_id)";
				
				
				$query .= "\n) ENGINE=InnoDB CHARSET=utf8;";
				$this->DB_Prep_Exec($query);
				
				// Track the database name
				static::DB_Connection()::$installed_tables[$table_name] = $table_name;
				
			}
		}
	}
	
	/**
	 * Creates the history trigger on the table
	 */
	public function processHistoryTriggers() : void
	{
		// Create all the triggers
		$this->updateTrigger('insert');
		$this->updateTrigger('update');
		$this->updateTrigger('delete');
	}
	
	
	
	public function updateTrigger($mode) : void
	{
		// Only bother if the feature is enabled.
		if(TC_getConfig('use_track_model_history'))
		{
			// DELETE the existing trigger
			$this->DB_Prep_Exec('DROP TRIGGER IF EXISTS history_' . $mode . '_' . $this->installTableName());
			
			// Test if we're deleting and we don't want to track deletions
			if($mode == 'delete' && !($this->class_name)::historyTrackDeletions())
			{
				return;
			}
			
			$column_names = array_keys($this->columns());
			if(!in_array($this->primaryKey(),$column_names))
			{
				$column_names[] = $this->primaryKey();
			}
			
			// Remove the ignored columns from the trigger
			foreach(($this->class_name)::historyAllIgnoredColumns() as $ignored_column)
			{
				foreach($column_names as $index => $column_name)
				{
					if($column_name == $ignored_column)
					{
						unset($column_names[$index]);
					}
				}
			}
			
			// Create a column that adds all the columns of the column to the history table
			// Then adds the additional columns that are history related
			$query = "
			CREATE TRIGGER history_".$mode. '_' . $this->installTableName()
				. " AFTER " . strtoupper($mode) . " ON `" . $this->installTableName() . "` FOR EACH ROW BEGIN ";
			
			// Add the check to ensure we insert missing history
			// This detects if we already have the insert history and if not adds it
			// Weird format is to make it readable in the trigger preview
			if($mode == 'update')
			{
				$query .=
"	IF 'insert' NOT IN (
		SELECT h._history_action FROM " . $this->historyTableName() . " h
			WHERE h.".$this->primaryKey()." = OLD.".$this->primaryKey()." AND h._history_action = 'insert'
	) THEN
		INSERT INTO " . $this->historyTableName() . " (" . implode(', ', $column_names)
		. " , _history_action, _history_version, _history_timestamp, _history_user_id)"
	. " VALUES (";
				
				// Append the old values
				foreach($column_names as $column_name)
				{
					$query .= 'OLD.' . $column_name . ', ';
				}
				// We're doing a retroactive insert, using the date added value for backdating
				$query .= "'insert', 'retroactive', OLD.date_added, @session_user_id);
	END IF;";

			
			}
			
			$query .= "
INSERT INTO " . $this->historyTableName() . " (" . implode(', ', $column_names)
				. " , _history_action, _history_version, _history_timestamp, _history_user_id)"
				. " VALUES (";
			
			// Identify if we want values from the old or the new
			$prefix = 'new';
			if($mode == 'delete')
			{
				$prefix = 'old';
			}
			
			// Append the old values
			foreach($column_names as $column_name)
			{
				$query .= $prefix.'.' . $column_name . ', ';
			}
			$query .= "'" . strtolower($mode) . "', @session_version, CURRENT_TIMESTAMP, @session_user_id);
END;";
		
			$this->DB_Prep_Exec($query);
		}
	}
	
	/**
	 * Deletes any history installations the table. This includes removing the table and the triggers related to
	 * tracking the model history
	 */
	public function deleteHistorySetup() : void
	{
		// DELETE the existing triggers
		$this->DB_Prep_Exec('DROP TRIGGER IF EXISTS history_insert_' . $this->installTableName());
		$this->DB_Prep_Exec('DROP TRIGGER IF EXISTS history_update_' . $this->installTableName());
		$this->DB_Prep_Exec('DROP TRIGGER IF EXISTS history_delete_' . $this->installTableName());
		
		// DROP THE history TABLE
		$this->DB_Prep_Exec('DROP TABLE IF EXISTS `' . $this->historyTableName().'`');
		
	}
	
}