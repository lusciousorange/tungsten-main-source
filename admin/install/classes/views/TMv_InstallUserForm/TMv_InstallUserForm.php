<?php

class TMv_InstallUserForm extends TMv_UserForm
{
	public function __construct($model)
	{	
		$users_module = TSm_Module::init('users');
		if(!$users_module->installed() )
		{
			//$model = false;
			//$this->model_name = false;
		}


		parent::__construct($model);
		$this->hideUserGroups();
	}
	
	/**
	 * Hides the user groups option from the list
	 *
	 * @return void
	 */
	public function hideUserGroups()
	{
		$this->show_user_groups = false;
	}
	
	public function configureFormElements()
	{
		if(TC_getConfig('use_single_name_field'))
		{
			$field = new TCv_FormItem_TextField('first_name', 'Name');
			$field->setIsRequired();
			$this->attachView($field);
		}
		else
		{
			$field = new TCv_FormItem_TextField('first_name', 'First Name');
			$field->setIsRequired();
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('last_name', 'Last Name');
			$field->setIsRequired();
			$this->attachView($field);
			
		}
		
		$field = new TCv_FormItem_TextField('email', 'Email');
		$field->setIsRequired();
		$field->setIsEmail();
		$this->attachView($field);
		
		$field = new TCv_FormItem_Hidden('is_active', 'Active');
		$field->setValue(1);
		$this->attachView($field);
		
		$field = new TCv_FormItem_Hidden('is_developer', 'Developer');
		$field->setValue(1);
		$this->attachView($field);
		
		
		if(!$this->isEditor())
		{
			$password = new TCv_FormItem_Password('password', 'Password');
			$password->setValidatePasswordFormat();
			$this->attachView($password);
			
			$password_verify = new TCv_FormItem_Password('password_verify', 'Verify');
			$password_verify->setSaveToDatabase(false);
			$password_verify->setValidateMatchField($password->id());
			$this->attachView($password_verify);
		}
		
		if($this->show_user_groups)
		{
			$user_group_list = new TMm_UserGroupList();
			$user_groups = new TCv_FormItem_CheckboxList('user_groups', 'User Groups');
			
			$selected_groups = array();
			if($this->isEditor())
			{
				$selected_groups = $this->model->userGroups();
			}
			$user_groups->setValuesFromObjectArrays($user_group_list->groups(), 'updateGroupSetting', $selected_groups);
			
			$this->attachView($user_groups);
		}
	}
	
}