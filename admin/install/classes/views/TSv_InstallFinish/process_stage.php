<?php
	$skip_tungsten_authentication = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	//////////////////////////////////////////////////////
	//
	// PROCESSOR STARTUP
	//
	//////////////////////////////////////////////////////
	$processor = new TSu_InstallProcessor('install_5');
	$processor->setSuccessURL('/admin/system/do/edit-settings/pages');
	
	$install_module = new TSm_Module('install');
	$install_module->delete();
	TC_clearMessenger();

	//////////////////////////////////////////////////////
	//
	// FINISH AND REDIRECT
	//
	//////////////////////////////////////////////////////
	$processor->updateCompletedInstallStep(5);
	$processor->finish();	
	
?>