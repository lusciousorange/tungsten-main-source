<?php

class TSv_InstallFinish extends TSv_InstallStep
{
	

	public function __construct()
	{	
		parent::__construct('TSv_InstallFinish');
		$this->next_step_message = '<i class="fa fa-check"></i>Finish and Proceed to Login';
		$this->back_url = '/admin/install/do/administrator/';
	
		
	}

	
	public function html()
	{
		$left_column = new TCv_View('left_column');
		
		$p = new TCv_View();
		$p->setTag('h2');
		$p->addText('Tungsten has been successfully installed');
		$left_column->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('You will now be directed to the settings where you can customize your website.');
		$left_column->attachView($p);
					
		$this->attachView($left_column);
		
		
		$this->attachView($this->nextStepBar());
		return parent::html();
	
	}
	
	
	
}
?>