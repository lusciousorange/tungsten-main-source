<?php

abstract class TSv_InstallStep extends TCv_View
{
	protected $next_step_message = ''; // [string] = The message used if the step was successful
	protected $status = true; // [bool] = The current status of the installer
	protected $button_href = 'href="process_stage.php"';
	protected $back_url = false;
	
	public function __construct($id)
	{	
		ini_set('display_errors','1'); 
		error_reporting(E_ALL &  ~E_NOTICE & ~E_STRICT);
	
		parent::__construct($id);
		
		$this->addClassCSSFile('TSv_InstallStep');
		$this->next_step_message = 'Keep Going';
		$this->button_href = $this->classFolderFromRoot().'/process_stage.php';
		
		// Font Awesome
		$plugin = new TSv_FontAwesome(false);
		$this->attachView($plugin);
		
	
		
	}
	
	
	protected function updateFailStatus($status)
	{
		if(!$status)
		{
			$this->status = false;
		}
	}
	
	protected function attachStatusViews(&$table, $title, $success, $fail_message )
	{
		$row = new TCv_View();
		$row->setTag('tr');
		$row->addClass('setup_check');
		$row->addClass($success ? 'success' : 'failure');
		
		$td = new TCv_View();
		$td->setTag('td');
		$td->addClass('title');
		$td->addText($title);
		$row->attachView($td);
		
		$td = new TCv_View();
		$td->setTag('td');
		$td->addClass('result');
		$td->addText($success ? 'Yes' : 'No');
		$row->attachView($td);
		
		$table->attachView($row);
		
		if(!$success)
		{
			$row = new TCv_View();
			$row->setTag('tr');
			$row->addClass('setup_extra');
			
			$td = new TCv_View();
			$td->setTag('td');
			$td->addClass('fail_description');
			$td->setAttribute('colspan', '2');
			$td->addText($fail_message);
			$row->attachView($td);
			
			$table->attachView($row);
		}
			
	}
	
	public function nextButton()
	{
		// NEXT BUTTON
		if($this->status)
		{
			$next_button = new TCv_Link();
		}
		else
		{
			$next_button = new TCv_View();
			$next_button->setTag('span');
		
		}
		$next_button->addClass('action_button');
		$next_button->addClass('tungsten_button');
		$next_button->addClass('big_button');
		$next_button->addClass($this->status ? 'success' : 'fail');
		
		if($this->status)
		{
			$next_button->setURL($this->button_href);
			$next_button->addText($this->next_step_message);
		}
		else
		{
			$next_button->addText('<i class="fa fa-exclamation-triangle"></i>Errors were found. Please correct them and try again.');
		}
		
		return $next_button;
	}
	
	
	public function nextStepBar()
	{
		
		$control_bar = new TCv_View('control_bar');
		
		$next_button = $this->nextButton();
		// BACK BUTTON
		
		if($this->back_url)
		{
			$back_button = new TCv_Link();
			$back_button->addClass('back_button');
			$back_button->addClass('action_button');
			$back_button->addClass('tungsten_button');
			$back_button->addClass('big_button');

			$back_button->addText('<i class="fa fa-chevron-left"></i> Back');
			$back_button->setURL($this->back_url);
			$back_button->addClass('two_column_1');
			$control_bar->attachView($back_button);
			
			$next_button->addClass('two_column_2');		
		}
		
		$control_bar->attachView($next_button);
		return $control_bar;
	}	
	
}
?>