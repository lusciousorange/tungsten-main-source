<?php 
	$skip_tungsten_authentication = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 
	TC_setConfig('force_show_errors',true);

	//////////////////////////////////////////////////////
	//
	// PROCESSOR STARTUP
	//
	//////////////////////////////////////////////////////
	$processor = new TSu_InstallProcessor('install_2');
	$processor->setSuccessURL('/admin/install/do/administrator/');

	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	//////////////////////////////////////////////////////
	TC_messageConditionalTitle('The database was successfully setup', true);
	TC_messageConditionalTitle('The database could not be setup', false);

	//////////////////////////////////////////////////////
	//
	// INSTALL
	//
	//////////////////////////////////////////////////////
	$processor->configureDatabase();
	
	
	//////////////////////////////////////////////////////
	//
	// FINISH AND REDIRECT
	//
	//////////////////////////////////////////////////////
	$processor->updateCompletedInstallStep(2);
	$processor->finish();
	

?>