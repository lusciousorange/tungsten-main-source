<?php

/**
 * Class TSv_InstallDatabaseSetup
 * The install view for the basic database setup
 */
class TSv_InstallDatabaseSetup extends TSv_InstallStep
{
	protected $connection = false; // 
	
	/**
	 * TSv_InstallDatabaseSetup constructor.
	 */
	public function __construct()
	{	
		parent::__construct('TSv_InstallDatabaseSetup');
		$this->next_step_message = '<i class="fa fa-database"></i>Configure The Database ';
		$this->back_url = '/admin/install/do/system-setup/';
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		$left_column = new TCv_View('left_column');
		$right_column = new TCv_View('right_column');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Database Setup<br /><em>'.TC_getConfig('DB_database').'</em>');
		$left_column->attachView($heading);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Tungsten requires the use of a MySQL database in order operate.');
		$left_column->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('The host name, database, username and password are all configured through your hosting provider. The host name is often <em>localhost</em>. Contact your hosting provider for more information.');
		$left_column->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('The table prefix is a string attached to the beginning of the table names to differentiate them from other tables. This can only have letters, numbers or the underscore character.');
		$left_column->attachView($p);
		
		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText('Configuration Tests');
		$right_column->attachView($heading);
		
		$table = new TCv_View();
		$table->setTag('table');
		$table->addClass('setup_checklist');
		
		$right_column->attachView($table);
				
		$this->attachView($left_column);
		//$this->attachView($right_column);
		
		
		$this->attachView($this->nextStepBar());
		return parent::html();
		
			}
	
	
	
}
?>