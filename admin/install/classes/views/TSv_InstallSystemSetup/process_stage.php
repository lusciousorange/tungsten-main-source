<?php 
	$skip_tungsten_authentication = true;
	$skip_tungsten_connection = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 
	
	/**********************************************/
	/*										   	  */
	/*             PROCESSOR STARTUP              */
	/*										   	  */
	/**********************************************/
	$processor = new TSu_InstallProcessor('install_1');
	//$form_processor->setDatabaseTableValues('users', 'user_id');
	$processor->setSuccessURL('/admin/install/do/database-setup/');
	
	/**********************************************/
	/*										   	  */
	/*            CUSTOMIZE MESSENGER             */
	/*										   	  */
	/**********************************************/
	$messenger = TC_website()->messenger();
	//$messenger->setSuccessTitle('Your password was successfully updated');
	//$messenger->setFailureTitle('Your password could not be updated');
	
	/**********************************************/
	/*										   	  */
	/*                 VALIDATE                   */
	/*										   	  */
	/**********************************************/
	//$form_processor->processFormItems();
	
	/**********************************************/
	/*										   	  */
	/*                UPDATE DB                   */
	/*										   	  */
	/**********************************************/
	//$form_processor->updateDatabase();
	
	/**********************************************/
	/*										   	  */
	/*            FINISH AND REDIRECT             */
	/*										   	  */
	/**********************************************/
	$processor->updateCompletedInstallStep(1);
	$processor->finish();	
	

?>