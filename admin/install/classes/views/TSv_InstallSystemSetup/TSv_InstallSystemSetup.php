<?php

class TSv_InstallSystemSetup extends TSv_InstallStep
{
	protected $min_php_version = 0; // [int] = The minimnum PHP version
	
	/**
	 *
	 */
	public function __construct()
	{	
		parent::__construct('TSv_InstallSystemSetup');
		include($_SERVER['DOCUMENT_ROOT'].'/admin/system/config/system_version.php');
		$this->min_php_version = $php_min_version;
		$this->next_step_message = '<i class="fa fa-cog"></i>Confirm System Settings ';
	}
	
	/**
	 * Returns the status for the PHP Version
	 * @param $table
	 * @return null
	 */
	private function statusForPHP($table)
	{
		$title = 'PHP '.phpversion().' Installed';
		$test = version_compare(phpversion(), $this->min_php_version, '>=');
		$fail = "You require at least PHP ".$this->min_php_version." to install Tungsten. The current version of PHP installed is ".phpversion().". ";
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}
	
	/**
	 * Returns the status for the GD Library
	 * @param $table
	 * @return null
	 */
	private function statusForGD($table)
	{
		$title = 'GD Support';
		$test = function_exists("gd_info");
		$fail = 'You require PHP support for the GD Library. ';
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}
	
	/**
	 * Returns the status for the MySQL Library
	 * @param $table
	 * @return null
	 */
	private function statusForDatabase($table)
	{
		$title = 'Database Connection - <em>'.TC_getConfig('DB_database').'</em>';
		$test = true;
		try 
		{
			$db = TCm_Database::connection();
		}
		catch(Exception $e)
		{
			$test = false;
		}
		$fail = 'Your connection to the database failed. Please verify the connection values in the TC_config file.  ';
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}
	
	/**
	 * Returns the status for the TCore Library
	 * @param $table
	 * @return null
	 */
	private function statusForTCore($table)
	{
		$title = 'Tungsten Core Installed';
		$test = file_exists($_SERVER['DOCUMENT_ROOT'].'/admin/TCore/');
		$fail = 'The folder <em>TCore</em> is not installed. Check the installation or try uploading it again.';
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}
	
	/**
	 * Returns the status for the config file existing
	 * @param $table
	 * @return null
	 */
	private function statusForConfigExists($table)
	{
		$title = 'TC_config.php Exists';
		$test = file_exists($_SERVER['DOCUMENT_ROOT'].'/TC_config.php');
		$fail = 'The file TC_config.php does not exists in the server\'s root directory. Check the installation or try uploading it again.';
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}

	/**
	 * Returns the status for the config file writable
	 * @param $table
	 * @return null
	 */
	private function statusForConfigWritable($table)
	{
		$title = 'TC_config Writable';
		$test = is_writable($_SERVER['DOCUMENT_ROOT'].'/TC_config.php');
		$fail = 'The file <em>TC_config.php</em> is not writable. Change the permissions on the file.';
	
		$this->updateFailStatus($test);
		return $this->attachStatusViews($table, $title, $test, $fail);
	
	}
			
	/**
	 * @return string
	 */
	public function html()
	{
		$left_column = new TCv_View('left_column');
		$right_column = new TCv_View('right_column');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('System Setup');
		$left_column->attachView($heading);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Your first step is to install the core Tungsten systems. If you meet the minimum requirements and the everything is configured properly, you just need to click the install button below.');
		$left_column->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Tungsten requires PHP with a MySQL database to store its information. Additionally, there is a TC_config.php file that must be located at the site root.');
		$left_column->attachView($p);
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Configuration Tests');
		$right_column->attachView($heading);
		
		$table = new TCv_View();
		$table->setTag('table');
		$table->addClass('setup_checklist');
		
		$this->statusForPHP($table);
		$this->statusForGD($table);
		$this->statusForDatabase($table);
		$this->statusForTCore($table);
		$this->statusForConfigExists($table);
		
		$right_column->attachView($table);
				
		$this->attachView($left_column);
		$this->attachView($right_column);
		
		$this->attachView($this->nextStepBar());
		return parent::html();
		
	}
	
	
	
}
?>