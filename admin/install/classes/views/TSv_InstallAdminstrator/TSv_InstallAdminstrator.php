<?php

/**
 *
 */
class TSv_InstallAdminstrator extends TSv_InstallStep
{
	protected $user_exists = false;
	protected $user = false;

	/**
	 * Constructor
	 */
	public function __construct()
	{	
		parent::__construct('TSv_InstallAdminstrator');
		$this->next_step_message = '<i class="fa fa-user"></i>Create Account';
		$this->back_url = '/admin/install/do/database-setup/';

		$users_module = TSm_Module::init('users');
		if($users_module->installed())
		{


			$this->user = new TMm_User(1); // try and find the user with ID 1
			if ($this->user->exists())
			{
				$this->user_exists = true;
			}
		}


	}

	/**
	 * Returns the bottom bar. If there were no errors in the status then it provides the "Next" button with the message provided. If there was an issue then it provides the "Stop" message.
	 * @return TCv_Link|TCv_View
	 */
	public function nextButton()
	{
		$next_button = parent::nextButton();


		if(!$this->user_exists)
		{
			$next_button->setURL('#');
			$next_button->setAttribute('onclick',"document.querySelector('form#TMm_User').submit(); ");
		}
		
		return $next_button;
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		
		$left_column = new TCv_View('left_column');
		$right_column = new TCv_View('right_column');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Administrator Setup');
		$left_column->attachView($heading);
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('The system requires at least one administrator. Enter the required information to create the first user for this installation.');
		$left_column->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('The password must be entered twice and consist of at least 6 characters with 1 number and no spaces. Please note that passwords are CaSe SeNsiTiVe');
		$left_column->attachView($p);
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Create Administrator');
		$right_column->attachView($heading);
		
		// USER FORM

		if($this->user_exists)
		{
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('The following user is already configured as the first user for this installation.');
			$right_column->attachView($p);

			$p = new TCv_View();
			$p->setTag('p');
			$p->addText($this->user->fullName() . '<br />' . $this->user->email());
			$right_column->attachView($p);

			$this->next_step_message = '<i class="fa fa-chevron-right"></i>Next';


		}
		else
		{
			$form = new TMv_InstallUserForm('TMm_User');
			$form->setAction($this->classFolderFromRoot() . '/process_form.php');
			$form->hideUserGroups();
			$form->hideSubmitButton();
			$right_column->attachView($form);

		}

		$this->attachView($left_column);
		$this->attachView($right_column);
		
		
		$this->attachView($this->nextStepBar());
		return parent::html();
		
		
	}
	
	
	
}
?>