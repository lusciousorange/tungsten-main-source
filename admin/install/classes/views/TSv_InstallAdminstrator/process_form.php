<?php 
	$skip_tungsten_authentication = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	//////////////////////////////////////////////////////
	//
	// PROCESSOR STARTUP
	//
	//////////////////////////////////////////////////////
	$form_processor = TCc_FormProcessorWithModel::init();
	$form_processor->setSuccessURL('/admin/install/do/finish/');

	$install_processor = new TSu_InstallProcessor('install_3');

	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	//////////////////////////////////////////////////////
	$messenger = TC_website()->messenger();
	$messenger->setSuccessTitle('The user was successfully setup');
	$messenger->setFailureTitle('The user could not be setup');

	//////////////////////////////////////////////////////
	//
	// CONFIGURE
	//
	//////////////////////////////////////////////////////
	$install_processor->configureUsers();
	
	//////////////////////////////////////////////////////
	//
	// CREATE ADMIN
	//
	//////////////////////////////////////////////////////
	if(!isset($_POST['admin_user_id']))
	{
		$form_processor->processFormItems();
		$form_processor->updateDatabase();
		if($form_processor->isValid())
		{
			// ADD USER GROUP MATCH
			///$form_processor->fail();
			//$admin_group = TMm_UserGroup::createWithValues($values);
			$user = TMm_User::init( 1);
			$admin_group = new TMm_UserGroup(1);
			//$user = $form_processor->model();
			if($user instanceof TMm_User)
			{
				$user->updateGroupSetting($admin_group, true);
				$user->setAsLoggedIn();
			}
			else
			{
				$form_processor->fail('The user was not properly created. Please try again or contact the developer.');
			}
			
		}
		else
		{
			TC_message('Administrator Not Account Created');
		}
	}


	//////////////////////////////////////////////////////
	//
	// INSTALL PAGES
	//
	//////////////////////////////////////////////////////
	$install_processor->configurePages();


	//////////////////////////////////////////////////////
	//
	// FINISH AND REDIRECT
	//
	//////////////////////////////////////////////////////
	if($form_processor->isValid() && $install_processor->isValid())
	{
		$install_processor->updateCompletedInstallStep(3);
	}
	else
	{
		$form_processor->fail();
	}
	$form_processor->finish();	
	

?>