<?php 
	$skip_tungsten_authentication = true;
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	//////////////////////////////////////////////////////
	//
	// PROCESSOR STARTUP
	//
	//////////////////////////////////////////////////////
	$processor = new TSu_InstallProcessor('install_3');
	$processor->setSuccessURL('/admin/install/do/finish/');

	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	//////////////////////////////////////////////////////
	$messenger = TC_website()->messenger();
	$messenger->setSuccessTitle('The user was successfully setup');
	$messenger->setFailureTitle('The user could not be setup');

	//////////////////////////////////////////////////////
	//
	// FINISH AND REDIRECT
	//
	//////////////////////////////////////////////////////
	$processor->updateCompletedInstallStep(3);
	$processor->finish();
	

?>