<?php

class TMc_InstallController extends TSc_ModuleController
{
	public function __construct($module)
	{
		parent::__construct($module);
		
	}
	
	public function defineURLTargets()
	{
		$item = TSm_ModuleURLTarget::init( '');
		$item->setNextURLTarget('system-setup');
		$item->setAsSkipsAuthentication();
		$this->addModuleURLTarget($item);
	
		$modules = TSm_ModuleURLTarget::init( 'system-setup');
		$modules->setViewName('TSv_InstallSystemSetup');
		$modules->setTitle('System setup');
		$modules->setAsSkipsAuthentication();
		$this->addModuleURLTarget($modules);
		
		$modules = TSm_ModuleURLTarget::init( 'database-setup');
		$modules->setViewName('TSv_InstallDatabaseSetup');
		$modules->setTitle('Database setup');
		$modules->setAsSkipsAuthentication();
		$this->addModuleURLTarget($modules);
		
		$modules = TSm_ModuleURLTarget::init( 'administrator');
		$modules->setViewName('TSv_InstallAdminstrator');
		$modules->setTitle('Administrator setup');
		$modules->setAsSkipsAuthentication();
		$this->addModuleURLTarget($modules);
		
		$modules = TSm_ModuleURLTarget::init( 'finish');
		$modules->setViewName('TSv_InstallFinish');
		$modules->setTitle('Finish setup');
		$modules->setAsSkipsAuthentication();
		$this->addModuleURLTarget($modules);
		
		
	}
	
	

}
?>