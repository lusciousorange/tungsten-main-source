<?php
/**
 * Class TSu_InstallProcessor
 * A processor specifically targeting at installing Tungsten
 */
class TSu_InstallProcessor extends TCu_Processor
{
	protected $current_table = false; // [string] = The current table that is be altered
	
	protected $foreign_key_queries = array();
	protected $foreign_table_references = array();
	
	/**
	 * @var TSm_InstallTable[]
	 * The tables that are being processed
	 */
	protected $tables = [];
	
	/**
	 * Creates a new installer
	 * @param int|string $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		
	}
	
	
	/**
	 * An internal method that performs spot checks during debuggin
	 * @param string $message
	 * @return bool
	 */
	protected function spotCheck($message)
	{
		return false;
		//print '<br />'.':: '.$message.($this->isValid() ? '' : ' - invalid' );
		//TC_message(':: '.$message.($this->isValid() ? '' : ' - invalid' ));
	}
	
	
	/**
	 * Installs a module
	 * @param string $module_folder_name
	 * @param bool $install_variables Indicates if variables should be installed
	 */
	public function installModule(string $module_folder_name, $install_variables = true)
	{
		$this->spotCheck('installModule() ' . $module_folder_name);
		
		
		// Parse the tables
		$this->processTablesForModuleFolder($module_folder_name);
		
		// RUN TABLE INSTALLERS
		$this->runTableInstalls();
		
		// Deal with update queries that run after the install but before the version update
		if($module = TSm_Module::init($module_folder_name))
		{
			foreach($this->tables as $table_name => $table)
			{
				$table->runInstallQueriesAfterVersion($module->version());
				
			}
			
		}
		
		// Update the version and module configuration
		$module = TSm_Module::init($module_folder_name);
		$module->updateModuleConfiguration();
		
		// INSTALL MODULE VARIABLES LISTING
		if($install_variables && isset($variables_form_class))
		{
			if($variables_form_class !== false)
			{
				$this->spotCheck('variable form class = ' . $variables_form_class);
				
				$setting_view = new $variables_form_class($module);
				
				$module->installVariables($setting_view->installValues());
			}
			
		}
		
	}
	
	/**
	 * Runs the installers on the tables that we have
	 * @return void
	 */
	public function runTableInstalls() : void
	{
		foreach($this->tables() as $table_name => $table)
		{
			// Run the installer
			$table->install();
		}
	}
	
	/**
	 * Generates the tables for this module folder which combined the older schema.php files with the more modern
	 * schema() method in model classes. The result is the filled in `tables` property of this class.
	 * @param string $module_folder
	 */
	public function processTablesForModuleFolder(string $module_folder)
	{
		// Reset the values
		$this->tables = [];
		$tables = [];
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/' . $module_folder . '/config/schema.php'))
		{
			// Tables array is overwritten if it exists
			include($_SERVER['DOCUMENT_ROOT'] . '/admin/' . $module_folder . '/config/schema.php');
		}
		
		// Generate the table models
		foreach($tables as $table_name =>  $table_values)
		{
			$this->tables[$table_name] = new TSm_InstallTable($table_name, $table_values);
		}
		
		
		// INCORPORATE TABLE SCHEMAS
		// This uses the newer `schema()` method to update the tables
		$this->updateTablesWithModuleModelSchemas($module_folder);
		
	}
	
	
	/**
	 * Updates the table definitions with the values from the models that exist in the module
	 * @param string $module_folder_name
	 */
	protected function updateTablesWithModuleModelSchemas(string $module_folder_name)
	{
		// Get the module, loop through the models in that module
		$module = TSm_Module::init($module_folder_name);
		
		// Find classnames, making sure to handle override models
		// avoid duplicates
		$class_names = [];
		foreach($module->classNames('models') as $class_name)
		{
			// Only bother or methods that use factory init
			if(method_exists($class_name,'classNameForInit'))
			{
				// Only bother if there's actually a base class to look at
				// Otherwise not installable
				if(method_exists($class_name, 'baseClass'))
				{
					// Only bother if the base class is also in this module
					// We don't want to trigger installs on and update queries on the wrong module
					// Happens in localization and if we declare a model that inherits from another module
					$install_folder = $class_name::installModuleFolder();
					if($install_folder == $module_folder_name)
					{
						$class_name = $class_name::classNameForInit();
						$class_names[$class_name] = $class_name;
					}
					else
					{
						$this->addConsoleWarning('Install skipped for `'.$class_name.'`. Attempting to install across modules into `'.$install_folder.'`. See installModuleFolder() for setup.');
					}
				}
			}
		}
		
		// Now that we have the proper models, load the tables
		foreach($class_names as $class_name)
		{
			$this->updateTableSchemaForModel($class_name);
		}
		
	}
	
	/**
	 * Updates the table schema for a single model
	 * @param class-string<TCm_Model> $class_name The class name for the model
	 */
	public function updateTableSchemaForModel(string $class_name) : void
	{
		
		// Pass in the class name, so that it processes it properly from the start
		if(is_subclass_of($class_name, 'TCm_ModelWithHorizontalSharding'))
		{
			$reflection = new ReflectionClass($class_name);
			if(!$reflection->isAbstract())
			{
				// Loop through all the models, running the installer for the table based on the models
				foreach($class_name::shardInstallModels() as $shard_model)
				{
					$shard_model->setAsCurrentShardModel();
					$this->processModelClassName($class_name);
				}
			}
		}
		else
		{
			$this->processModelClassName($class_name);
		}
		
		
	}
	
	/**
	 * Internal processor for a class name which creates an install table
	 * @param string $class_name
	 * @return void
	 */
	public function processModelClassName(string $class_name) : void
	{
		// Check if the class has a table name at all
		if(is_subclass_of($class_name, 'TCm_Model'))
		{
			// Get the table name, which isn't set for every model
			$table_name = $class_name::tableName();
			if($table_name != '')
			{
				// If the table doesn't exist, we need to generate it with no values
				if(!isset($this->tables[$table_name]))
				{
					$this->tables[$table_name] = new TSm_InstallTable($class_name, []);
				}
			}
		}
	}
	
	/**
	 * Returns the array of tables
	 * @return TSm_InstallTable[]
	 */
	public function tables()
	{
		return $this->tables;
	}
	
	
	

	
	
	//////////////////////////////////////////////////////
	//
	// FINISH
	//
	//////////////////////////////////////////////////////


	/**
	 * updates the stored install step if necessary
	 * @param int $step
	 */
	public function updateCompletedInstallStep($step)
	{
		$messenger = TCv_Messenger::instance();

		if($this->isValid() && $messenger->isSuccess())
		{
			if(isset($_SESSION['install_step']))
			{
				$_SESSION['install_step'] = $step+1;
			}
		}

		$_SESSION['install_step'] = 1;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// CONFIGURE DATABASE
	//
	//////////////////////////////////////////////////////

	/**
	 * Configures the database for the intial setup
	 */
	public function configureDatabase()
	{
		$this->spotCheck('configureDatabase() ');

		$this->installModule('system');
		
		$this->installModule('login');
		$this->installModule('install');
		
		// Install Default Plugins
		if($this->isValid())
		{
			$this->spotCheck('- installing plugins');
// Manually unset the previously saved plugin and module lists
			unset($GLOBALS['TC_classes']['classes']['TSm_PluginList']);
			unset($GLOBALS['TC_classes']['classes']['TSm_ModuleList']);
			
			// Re initialize the plugin and module lists to force the re-readh
			$plugin_list = TSm_PluginList::init(true); // force init
			
			$plugin_list->installPlugin('TSv_ErrorReporting');
			$plugin_list->installPlugin('TSv_FontAwesome');
			$plugin_list->installPlugin('TSv_Console');
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// CONFIGURE USERS
	//
	//////////////////////////////////////////////////////

	/**
	 * Configures the database for users setup
	 */
	public function configureUsers() : void
	{
		$this->installModule('users'); //must come before user_groups, foreign keys order matters
		$this->installModule('user_groups');
		$this->installModule('profile');
		
		$query = "SELECT * FROM `user_groups` WHERE group_id = 1 LIMIT 1";
		$result = $this->DB_Prep_Exec($query);
		
		if($result->rowCount() == 0)
		{
		
			$values = array('group_id'=>1, 'code'=>'admin', 'title' => 'System Administrator', 'tungsten_access' => '1', 'api_access' => '1');
			TMm_UserGroup::createWithValues($values);
		}
		
	}
	
	public function configurePages() : void
	{
		// Detect the install for workflows, must happen before pages
		if(TC_getConfig('use_workflow'))
		{
			$this->installModule('workflow');
		}
	
		$this->installModule('pages');
		
		// ONly install pages and types once, requires empty table
		$query = "SELECT menu_id FROM pages_menus LIMIT 1";
		$result = $this->DB_Prep_Exec($query);
		if($result->rowCount() == 0)
		{
			
			// Set pages as the start page
			$query = "UPDATE modules SET is_login_startpage = 1 WHERE folder = 'pages' LIMIT 1";
			$this->DB_Prep_Exec($query);
			
			// Create the first page navigation type
			$type = TMm_PagesNavigationType::createWithValues([
				                                                  'title' => 'Main menus',
				                                                  'short_code' => 'main_menus',
			                                                  
			                                                  ]);
			
			// Create homepage
			$homepage = TMm_PagesMenuItem::createWithValues([
				                                                'title' => 'Homepage',
				                                                'folder' => '/',
				                                                'display_order' => 1,
				                                                'content_entry' => 'manage',
				                                                'parent_id' => 0,
			                                                
			                                                
			                                                ]);
			
			TMm_PagesNavigationTypeMenuItemMatch::createWithValues([
				                                                       'menu_id' => $homepage->id(),
				                                                       'navigation_id' => $type->id(),
				                                                       'display_order' => 1,
			                                                       
			                                                       ]);
		}
	
	}
	
	/**
	 * Creates the first administrator in the system
	 * @param string $first_name
	 * @param string $last_name
	 * @param string $email
	 * @param string $password
	 */
	public function createFirstAdministrator($first_name, $last_name, $email, $password)
	{
		$user_list = TMm_UserList::init();
		$users = $user_list->models();
		if(sizeof($users) < 1)
		{
			$values = array();
			$values['first_name'] = $first_name;
			$values['last_name'] = $last_name;
			$values['email'] = $email;
			$values['is_developer'] = '1';
			$values['is_active'] = '1';
			
			
			// Handle Password
			$text = new TCu_Text($password);
			$password = $text->hash();
			$values['password'] = $password;
			
			$user = TMm_User::createWithValues($values);
			
			$admin_group = new TMm_UserGroup(1);
			if($user instanceof TMm_User)
			{
				$user->updateGroupSetting($admin_group, true);
			}
		}
	}

	/**
	 * This function is used by some classes to dynamically keep DBs in sync, such as taxes in stores
	 *
	 * Creates a series of rows in the table provided. If the row does not exist then it is added, if it does exist
	 * then the table is modified to use the new setup provided.
	 *
	 * @param string[] $row_array An array of MySQL partial statements that can be used to create a column.  Each
	 * array value statement will be applied to an ALTER TABLE <table> ADD/MODIFY COLUMN <statement> that is used to
	 * add the row.
	 * @param bool|string  $table_name (Optional) Default false. The name of the table, if false, then it will use
	 * the last table that was created.  This is due to the fact that normally, this function is called directly
	 * after the createTable() function.
 	 */

	public function addTableRows($row_array, $table_name = false)
	{
		
		// NO table SO USE THE OTHER ONE USED
		if(!$table_name)
		{
			$table_name = $this->current_table;
		}
		
		$existing_columns = array();
		$result = $this->DB_Prep_Exec("SHOW COLUMNS FROM `".$table_name."`");
		while($row = $result->fetch())
		{
			$existing_columns[$row['Field']] = true;
		}
		
		if($result)
		{
			foreach($row_array as $statement)
			{
				$column_name = str_ireplace('`', '', strtok($statement, " ")); // first word, remove possible ` around column name
				if(@$existing_columns[$column_name])
				{
					$query = "ALTER TABLE `" . $table_name . "` MODIFY COLUMN $statement";
				}
				else
				{
					$query = "ALTER TABLE `" . $table_name . "` ADD COLUMN $statement";
				}
				
				if(!$this->DB_Prep_Exec($query))
				{
					$message = "Cannot add column to table <em>" . $table_name . "</em> using <em>" . htmlspecialchars($statement) . "</em>";
					$this->fail($message);
				}
				
			}
		}
		else
		{
			// NO TABLE DEFINED
			if(!$table_name)
			{
				$message = "Error with addTableRows: No table is defined";
				$this->fail($message);
			}
			// NO TABLE DEFINED
			else
			{
				$message = "Cannot add column to table <em>" . $table_name . "</em> ";
				$this->fail($message);
			}
		}
	}



//////////////////////////////////////////////////////
	//
	// CONFIGURE PAGES
	//
	//////////////////////////////////////////////////////

	/**
	 * Configures the database for users setup
	 */
	public function configurePagesModule()
	{
	//	$this->installModule('pages');
	}
	
	
	
}
?>