<?php
/**
 * Class TMv_UserList
 *
 * This is a deprecated view, that exists for backwards compatibility.
 */
class TMv_UserList extends TMv_UserManagerList
{
	public function __construct()
	{
		parent::__construct();
		$this->addConsoleDebug('USER LIST = Bad');
	}
}
?>