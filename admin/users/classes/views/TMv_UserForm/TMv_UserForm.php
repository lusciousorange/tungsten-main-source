<?php

/**
 * Class TMv_UserForm
 */
class TMv_UserForm extends TCv_FormWithModel
{
	protected $show_user_groups = true;
	protected $show_password = true;


	/**
	 * TMv_UserForm constructor.
	 * @param string|TMm_User $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);

		// Only admins can alter user groups
		if(!TC_currentUser() || !TC_currentUser()->isAdmin())
		{
			$this->hideUserGroups();
		}
		
	}
	
	/**
	 * Set the required model for the form
	 * @return class-string<TCm_Model>|null
	 */
	public static function formRequiredModelName(): ?string
	{
		return 'TMm_User';
	}
	
	
	/**
	 * Disables the user groups from being shown.
	 */
	public function hideUserGroups()
	{
		$this->show_user_groups = false;
	}

	/**
	 * @return bool|TMm_User
	 */
	public function model()
	{
		return parent::model();
	}


	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		if(TC_getConfig('use_single_name_field'))
		{
			$field = new TCv_FormItem_TextField('first_name', 'Name');
			$field->setIsRequired();
			$this->attachView($field);
		}
		else
		{
			$field = new TCv_FormItem_TextField('first_name', 'First name');
			$field->setIsRequired();
			$field->setAsHalfWidth();
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('last_name', 'Last name');
			$field->setIsRequired();
			$field->setAsHalfWidth();
			$this->attachView($field);
			
		}
		
		
		$email = new TCv_FormItem_TextField('email', 'Email');
		$email->setIsRequired();
		$email->setIsEmail();
		$this->attachView($email);
		
	
		if(TC_currentUser() && TC_currentUser()->isAdmin())
		{
			$field = new TCv_FormItem_Select('is_active', 'Active account');
			$field->addOption('1', 'Active');
			$field->addOption('0', 'Deactivated');
			$this->attachView($field);
			
			
			$field = new TCv_FormItem_Select('is_developer', 'Developer account');
			$field->addOption('0', 'No – User is NOT a website developer');
			$field->addOption('1', 'Yes – User is a website developer');
			$this->attachView($field);
		}

		$this->attachPasswordFields();

		$this->attachUserGroupFields();
		
		if(TC_currentUserIsDeveloper())
		{
			$field = new TCv_FormItem_Select('error_reporting','Error reporting');
			$field->setHelpText('Indicates if system errors should be displayed for this user when they are logged in');
			$field->addOption(0,'NO – Errors hidden (Default and Recommended)');
			$field->addOption(1,'YES – All errors, warnings displayed (Advanced)');
			$this->attachView($field);
			
		}
		
		
	}

	// Attaches the fields related to passwords
	protected function attachPasswordFields()
	{
		if(!$this->isEditor() && $this->show_password)
		{
			$password = new TCv_FormItem_Password('password', 'Password');
			//	$password->setValidatePasswordFormat();
			$password->setIsRequired();
			$this->attachView($password);
			
			$password_verify = new TCv_FormItem_Password('password_verify', 'Verify');
			$password_verify->setSaveToDatabase(false);
			$password_verify->setValidateMatchField($password->id());
			$password_verify->setIsRequired();
			$this->attachView($password_verify);
		}
		
	}
	
	/**
	 * Attaches the fields related to user groups
	 */
	protected function attachUserGroupFields()
	{
		if($this->show_user_groups)
		{
			
			
			$field = new TCv_FormItem_CheckboxList('user_groups', 'User groups');
			
			$field->setUseMultiple(TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
			                       'user_group_matches',
			                       'group_id',
			                       $this->model());
			
			
			$selected_groups = array();
			
			if($this->isEditor())
			{
				$selected_groups = $this->model()->userGroups();
			}
			
			$user_group_list = new TMm_UserGroupList();
			
			$field->setValuesFromObjectArrays($user_group_list->groups(), $selected_groups);
			
			$this->attachView($field);
			
		
		}
	}
	


}