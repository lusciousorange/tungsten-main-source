<?php
/**
 * Class TMv_ResetPasswordEmail
 *
 * The email that is sent when a user requests a password reset. This is sent to their email address and includes a
 * link to change their password.
 */
class TMv_ResetPasswordEmail extends TCv_Email
{
	protected $user = false;
	protected $reclaim_url = '/reset-password/';
	
	/**
	 * TMv_ResetPasswordEmail constructor.
	 * @param bool|TMm_User $user
	 */
	public function __construct($user)
	{
		parent::__construct('reset_password_'.$user->id());
		$this->user = $user;
		
		$this->addRecipient($this->user->email());
		$this->setSubject('Password Reset');
		

	}
	
	/**
	 * Sets the URL that is provided in the email. The domain for the site will always be prepended onto it.
	 * @param string $url The reclaim URL if something other than the normal is needed.
	 */
	public function setReclaimURL($url)
	{
		$this->reclaim_url = $url;
	}
	
	/**
	 * Sets this to be a password reset in the admin, which means we want to redirect them back to the admin.
	 * @return void
	 */
	public function setAsTungstenAdminReset() : void
	{
		$this->reclaim_url = '/admin/login/do/reset-password/';
	}
	
	/**
	 * Sets this to be a password reset in the front-end routing, so it uses the public styling.
	 * @return void
	 */
	public function setAsFrontEndRouting() : void
	{
		$this->reclaim_url = '/w/login/reset-password/';
	}
	
	
	/**
	 * Configures the form
	 */
	public function configureViews()
	{
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText($this->user->firstName().',');
		$this->attachView($explanation);
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText('You recently asked to reset your password for '.$_SERVER['HTTP_HOST'].'. To complete your request, please follow this link:');
		$this->attachView($explanation);
		
		$code = $this->user->generatePasswordResetCode();
		$url = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'];
		$url .= $this->reclaim_url.$this->user->id().'/'.$code;
		
		$link = new TCv_Link();
		$link->setURL($url);
		$link->addText($url);
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->attachView($link);
		
		$this->attachView($explanation);
		
	}
	
	/**
	 * Returns the view html
	 * @return bool|string
	 */
	public function html()
	{
		$this->configureViews();
		return parent::html();
	}
	
	
	
}

?>