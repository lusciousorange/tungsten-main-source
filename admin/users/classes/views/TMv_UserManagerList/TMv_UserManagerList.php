<?php
class TMv_UserManagerList extends TCv_SearchableModelList
{
	/**
	 * TMv_UserManagerList constructor.
	 */
	public function __construct($id = 'user_manager_list')
	{
		parent::__construct($id);
		
		$this->setModelClass('TMm_User');

		// Disabled. For the lists where it matters, it creates an extra long query
//		$user_list = TMm_UserList::init();
//		if(count($user_list->models()) > 2)
//		{
			$this->enableBulkEditMode();
			$this->setPagination(25);
		//}
		
		
		$this->defineColumns();
		
		$this->addClassCSSFile('TMv_UserManagerList');
		
	}
	
	/**
	 * @param TMm_User $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		if($model->isActive())
		{
			$row->addClass('active_account');
		}
		else
		{
			$row->addClass('inactive_account');
		}
		
		return $row;
	}
	
	/**
	 * Defines the columns for the list
	 */
	public function defineColumns()
	{
		$field = new TCv_ListColumn('name');
		$field->setTitle('User');
		if($this->is_horizontal_scrolling)
		{
			$field->setWidthAsPixels(200);
		}
		$field->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($field);
		
		$field = new TCv_ListColumn('email');
		if($this->is_horizontal_scrolling)
		{
			$field->setWidthAsPixels(160);
		}
		else
		{
			$field->setWidthAsPercentage(30);
		}
		$field->setTitle('Email');
		$field->setContentUsingListMethod('emailColumn');
		$this->addTCListColumn($field);
		
		$field = new TCv_ListColumn('user_groups');
		if($this->is_horizontal_scrolling)
		{
			$field->setWidthAsPixels(180);
		}
		else
		{
			$field->setWidthAsPercentage(40);
		}
		$field->setTitle('User groups');
		$field->setContentUsingListMethod('userGroupsColumn');
		$this->addTCListColumn($field);
		
		
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
		
		//$cancel_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		//$this->addTCListColumn($cancel_button);
		
		$column = $this->controlButtonColumnWithListMethod('disableColumn');
		$this->addTCListColumn($column);
		
	}
	
	
	/**
	 * A column for the view
	 * @param TMm_User $model
	 * @return TCv_Link|TCv_View
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'edit');
		$link->addText($model->fullName());
		return $link;
	}
	
	/**
	 * A column for the view
	 * @param TMm_User $model
	 * @return TCv_Link|TCv_View
	 */
	public function emailColumn($model)
	{
		$link = new TCv_Link();
		$link->setURL('mailto:'.$model->email());
		$link->addText($model->email());
		return $link;
	}
	
	/**
	 * A column for the view
	 * @param TMm_User $model
	 * @return TCv_Link|TCv_View|string
	 */
	public function userGroupsColumn($model)
	{
		return implode('<br />', $model->userGroupsTitles());
	}
	
	/**
	 * A column for the view
	 * @param TMm_User $model
	 * @return TCv_Link|TCv_View
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'edit', 'fa-pencil');
	}

//	/**
//	 * A column for the view
//	 * @param TMm_User $model
//	 * @return TCv_Link|TCv_View
//	 */
//	public function deleteIconColumn($model)
//	{
//		return $this->listControlButton_Confirm($model, 'delete', 'fa-trash');
//	}
	
	/**
	 * A column for the view
	 * @param TMm_User $model
	 * @return TCv_Link|TCv_View
	 */
	public function disableColumn($model)
	{
		if($model->isActive())
		{
			$title = 'Are you sure you want to disable this account?';
			$icon = 'fa-user-slash';
		}
		else
		{
			$title = 'Are you sure you want to enable this account?';
			$icon = 'fa-user-check';
		}
		
		
		$button = $this->listControlButton_Confirm($model, 'toggle-disable', $icon);
		$button->setTitle($title);
		
		return $button;
		
	}
	
	
	
	/**
	 * Defines the filters for view
	 */
	public function defineFilters()
	{
		parent::defineFilters();
		
		$user_group_list = TMm_UserGroupList::init();
		$groups = new TCv_FormItem_Select('group_id', 'User groups');
		$groups->addOption('', 'All user groups');
		foreach($user_group_list->groups() as $group)
		{
			$groups->addOption($group->id(), $group->title());
		}
		
		$this->addFilterFormItem($groups);
		
		$field = new TCv_FormItem_Select('is_active', 'Active/disabled');
		$field->setDefaultValue(1);
		$field->addOption('', 'All users');
		$field->addOption('0', 'Inactive users');
		$field->addOption('1', 'Active users');
		
		$this->addFilterFormItem($field);
		
		
		// BULK FORM SETTINGS
		
		if($this->bulk_column_added)
		{
			$this->addBulkFields();
		}
	}
	
	/**
	 * The view that contains the bulk edit group
	 */
	protected function addBulkFields()
	{
		$user_group_list = TMm_UserGroupList::init();
		
		// BULK - CHANGE USER GROUP
		$field = new TCv_FormItem_Select('change_user_group', 'User group');
		$field->addOption('', 'Assign a user group');
		foreach($user_group_list->groups() as $group)
		{
			$field->addOption($group->id(), $group->title());
		}
		$field->addOption('delete', 'Remove all user groups');
		$this->addBulkUpdateFilterField($field, 'bulk-user-update-user-group');
		
		// BULK - CHANGE ACTIVE
		$field = new TCv_FormItem_Select('change_user_active', 'Active state');
		$field->addOption('', 'Change active/inactive');
		$field->addOption('1', 'Set as active');
		$field->addOption('0', 'Set as inactive');
		$this->addBulkUpdateFilterField($field, 'bulk-user-update-active');
		
		
	}
	
	
}