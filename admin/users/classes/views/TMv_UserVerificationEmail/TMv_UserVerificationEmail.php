<?php
/**
 * Class TMv_UserVerificationEmail
 *
 * The email sent to verify if a user's email is valid. This interacts directly with the "is_active" flag on accounts
 * which is already used to restrict access.
 */
class TMv_UserVerificationEmail extends TCv_Email
{
	protected $user = false;
	protected $reclaim_url = '/admin/users/do/verify-email/';
	
	/**
	 * TMv_ResetPasswordEmail constructor.
	 * @param bool|TMm_User $user
	 */
	public function __construct($user)
	{
		parent::__construct('verify_email_'.$user->id());
		$this->user = $user;
		
		$this->addRecipient($this->user->email());
		$this->setSubject(TC_localize('email_verify_subject','New account email verification'));
		
		
	}
	
	/**
	 * Render the view
	 */
	public function render()
	{
		// Hello,
		$p = new TCv_View();
		$p->setTag('p');
		$p->addClass('hello');
		$p->addText(TC_localize('verify_email_hello','Hello,'));
		$this->attachView($p);
		
		// Thank you for joining WEBSITE. Your account needs to be verified. Please click on the link below to verify
		// your email.
		$p = new TCv_View();
		$p->setTag('p');
		$p->addClass('explanation');
		$p->addText(TC_localize('verify_email_thank_you','Thank you for joining')
		            .' '.TC_getModuleConfig('pages', 'website_title').'. ');
		
		$p->addText(TC_localize('verify_email_needs_click',
		                        'Your account needs to be verified. Please click on the link below to verify your email.'));
		
		$this->attachView($p);
		
		// BUTTON
		$code = $this->user->generateVerifyEmailCode();
		$url = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'];
		$url .= $this->reclaim_url.$this->user->id().'/'.$code;
		
		$link = new TCv_Link();
		$link->setURL($url);
		$link->addClass('verify_button');
		$link->addText(TC_localize('verify_email_button','Verify account'));
		$link->addClass('big_button');
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->attachView($link);
		
		$this->attachView($p);
	}
	
	
	
}

?>