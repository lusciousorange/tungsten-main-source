<?php
/**
 * Class TMv_UserAccountRedirect
 *
 * A view that can be added to a page which redirects the user if they are already logged in
 */
class TMv_UserAccountRedirect extends TCv_View 
{
	use TMt_PagesContentView;
	
	protected string $logged_in_menu = '';
	protected string $logged_out_menu = '';
	
	/**
	 * TMv_UserAccountRedirect constructor.
	 */
	public function __construct()
	{
		parent::__construct('logged_in_redirect');
		
	}

	/**
	 * Redirects the user to the account
	 * @return string
	 */
	public function html()
	{
		if(TC_getModuleConfig('login', 'show_authentication'))
		{
			
			$website = TC_website();
			$logged_in = $website->isLoggedIn();
			$url = null;
		
			// Logged in and redirect is set for that option
			if($logged_in && $this->logged_in_menu != '')
			{
				$menu = TMm_PagesMenuItem::init( $this->logged_in_menu);
				$url = '/';
				if($menu)
				{
					$url = $menu->pathToFolder();
				}
			}
			elseif(!$logged_in && $this->logged_out_menu != '')
			{
				$menu = TMm_PagesMenuItem::init( $this->logged_out_menu);
				$url = '/';
				if($menu)
				{
					$url = $menu->pathToFolder();
				}
			}
			
			if(is_string($url))
			{
				header("Location: ".$url);
				exit();
			}
			
			// Do nothing
			
		}
		
		
		return '';
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		
		$form_items = array();
		
		$redirect = new TCv_FormItem_Select('logged_in_menu','Logged In Redirect Page');
		$redirect->setHelpText('Select a page that this menu item will redirect to ONLY IF the user is already logged IN to the site. This would perform a similar function to setting the menu item to redirect.');
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		$redirect->addOption('', 'No Redirect');
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$form_items[] = $redirect;
		
		$redirect = new TCv_FormItem_Select('logged_out_menu','Logged Out Redirect Page');
		$redirect->setHelpText('Select a page that this menu item will redirect to ONLY IF the user is already logged OUT the site. ');
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		$redirect->addOption('', 'No Redirect');
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$form_items[] = $redirect;
		
		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string { return 'User account redirect'; }
	public static function pageContent_IconCode() : string { return 'fa-arrow-right'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	public static function pageContent_ShowPreviewInBuilder() :bool { return false; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A utility view that will redirect this page if the user is logged in or out. The values are set in the login module settings. ';
	}

}
?>