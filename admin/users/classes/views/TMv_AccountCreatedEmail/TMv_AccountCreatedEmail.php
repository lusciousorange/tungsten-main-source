<?php
/**
 * Class TMv_AccountCreatedEmail
 */
class TMv_AccountCreatedEmail extends TCv_Email
{
	protected $user = false;
	protected $view_url = '/account/';
	
	/**
	 * TMv_AccountCreatedEmail constructor.
	 * @param bool|TMm_User $user
	 */
	public function __construct($user)
	{
		parent::__construct('account_created_'.$user->id());
		$this->user = $user;
		
		$this->addRecipient($this->user->email());
		$this->setSubject('Welcome to '.TC_getModuleConfig('pages', 'website_title'));
		$this->setShowInConsole(true); // turn on the debugging in the console

	}
	
	/**
	 * Sets the URL that is shown when they are directed to view their account
	 * @param string $url The reclaim URL if something other than the normal is needed.
	 */
	public function setViewURL($url)
	{
		$this->view_url = $url;
	}
	
	/**
	 * Configures the form
	 */
	public function configureViews()
	{
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText($this->user->firstName().',');
		$this->attachView($explanation);
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText(nl2br(TC_getModuleConfig('system','user_created_email_explanation')));
		$this->attachView($explanation);
		
		//$code = $this->user->generatePasswordResetCode();
		$url = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'];
		$url .= $this->view_url;
		
		$link = new TCv_Link();
		$link->setURL($url);
		$link->addText($url);
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->attachView($link);
		
		$this->attachView($explanation);
		
	}
	
	/**
	 * Returns the view html
	 * @return bool|string
	 */
	public function html()
	{
		$this->configureViews();
		return parent::html();
	}
	
	
	
}

?>