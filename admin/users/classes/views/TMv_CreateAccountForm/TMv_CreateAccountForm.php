<?php
/**
 * Class TMv_CreateAccountForm
 * A form that allows someone to create an account in the system. This is used on websites that allow users to sign
 * in on the front-end, separately from Tungsten. It requires values to be set in the login module.
 */
class TMv_CreateAccountForm extends TCv_FormWithModel 
{
	use TMt_PagesContentView;

	protected bool $use_phone_number = false;


	/**
	 * TMv_CreateAccountForm constructor.
	 */
	public function __construct()
	{
		parent::__construct('TMm_User');
		$this->setButtonText('Create Account');
		
		$login_module = TMm_LoginModule::init();
		$this->setSuccessURL($login_module->publicLoggedInHomepageURL());
		
		
	}

	

	/**
	 * Configures the form to use a phone number or not.
	 * @param int $required (Optional) Default 1. 1 means shown and required, 0 means not shown, 2 means shown but
	 * not required.
	 */
	public function setUsePhoneNumber(int $required = 1) : void
	{
		$this->use_phone_number = $required;
	}

	/**
	 * Attaches the views related to personal form items
	 */
	public function personalFormItems()
	{
		if(TC_getConfig('use_single_name_field'))
		{
			$field = new TCv_FormItem_TextField('first_name', 'Name');
			$field->setIsRequired();
			$this->attachView($field);
		}
		else
		{
			$field = new TCv_FormItem_TextField('first_name', 'First Name');
			$field->setIsRequired();
			$field->disableAutoComplete();
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('last_name', 'Last Name');
			$field->setIsRequired();
			$field->disableAutoComplete();
			$this->attachView($field);
			
		}
		

		$email = new TCv_FormItem_TextField('email', 'Email');
		$email->setIsRequired();
		$email->setIsEmail();
		$email->disableAutoComplete();
		$this->attachView($email);

		if($this->use_phone_number > 0 )
		{
			$phone = new TCv_FormItem_TextField('cell_phone', 'Mobile Phone');
			if($this->use_phone_number == 1)
			{
				$phone->setIsRequired();
			}

			$phone->disableAutoComplete();
			$this->attachView($phone);

		}
	}

	/**
	 * Attaches the view related to the password form items
	 */
	public function passwordFormItems() : void
	{
		$password = new TCv_FormItem_Password('password', 'Password');
		$password->setValidatePasswordFormat();
		$password->setIsRequired();
		$password->disableAutoComplete();
		$this->attachView($password);

		$password_verify = new TCv_FormItem_Password('password_verify', 'Verify');
		$password_verify->setSaveToDatabase(false);
		$password_verify->setValidateMatchField($password->id());
		$password_verify->setIsRequired();
		$password_verify->disableAutoComplete();
		$this->attachView($password_verify);


	}

	/**
	 * Configures the form
	 */
	public function configureFormElements()
	{
		$this->personalFormItems();
		$this->passwordFormItems();

	}

	/**
	 * Turns off authentication for this form, allowing it to process without being logged in
	 * @return bool
	 */
	public static function customFormProcessor_skipAuthentication() { return true; }
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		
		// Phone validation
		if($form_processor->fieldIsSet('cell_phone'))
		{
			$cell_phone = $form_processor->formValue('cell_phone');
			if(!TCu_Text::validatePhoneNumber($cell_phone))
			{
				$form_processor->failFormItemWithID('cell_phone', 'It appears your phone number is incorrectly formatted. Please include an area code plus 7 digits. eg: (234) 567-8901. International number must begin with +.');
			}
		}
		
	}
	
	/**
	 * This function is called after the traditional form processor updateDatabase() function. A form instance can override
	 * this function to do custom form processing that doesn't fit within the standard operating procedure for many Tungsten forms.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		// Log in the user
		/** @var TMm_User $user */
		$user = $form_processor->model();
		
		if(TC_getConfig('use_email_verification'))
		{
			$user->sendVerifyEmailCode();
		}
		else
		{
			// No verification required
			// Log them in, send the "account created" email
			$user->setAsLoggedIn();
			$user->sendAccountCreatedEmail();
		}
		
		
		$form_processor->addConsoleDebug('Sent account email');
	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();	
		

		$use_phone = new TCv_FormItem_Select('use_phone_number','Phone Number');
		$use_phone->addOption(0, 'Phone number is not asked for');
		$use_phone->addOption(2,"Phone number is asked for BUT NOT required");
		$use_phone->addOption(1, 'Phone number is asked for AND required');

		$form_items[] = $use_phone;

		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string { return 'Create account form'; }
	public static function pageContent_IconCode() : string { return 'fa-user-plus'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A form to create a new account.';
	}
}
?>