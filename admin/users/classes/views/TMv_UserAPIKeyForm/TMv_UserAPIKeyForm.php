<?php
class TMv_UserAPIKeyForm extends TCv_FormWithModel
{
	/** @var string|TMm_User $user */
	protected $user;
	/**
	 * TMv_UserAPIKeyForm constructor.
	 * @param string|TMm_User $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->user = $model;
		$this->setButtonText('Generate New API Key');
	}
	
	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		if($this->user->hasAPIAccess())
		{
			$field = new TCv_FormItem_TextField('api_key', 'API Key');
			$field->setAttribute('readonly','readonly');
			$field->setPlaceholderText('Auto generated with form submission');
			$field->setSaveToDatabase(false);
		}
		else
		{
			$field = new TCv_FormItem_HTML('explanation', 'API Key');
			$field->addText('API access is restricted to active users who are a part of at least one user group with API Access. <br />This user does not have access.');
			
			$this->hideSubmitButton();
			$this->setAction('#');
		}
		
		$field->setHelpText('API keys are unique to a user. Generating a new key will invalidate the current key for this user');
		$this->attachView($field);
		
		$field = new TCv_FormItem_HTML('date', 'Date');
		$field->addHelpText('The date the API Key was generated. ');
		$field->addText($this->user->apiKeyDate());
		$this->attachView($field);
		
		$field = new TCv_FormItem_HTML('num-calls', 'Number of Calls');
		$field->addText($this->user->numAPICalls());
		$this->attachView($field);
		
		
	}
	
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		/** @var TMm_User $user */
		$user = $form_processor->model();
		$user->generateNewAPIKey();
	}
}