<?php
/**
 * Class TMv_ForgotPasswordForm
 *
 * A form for users who have forgotten their password
 */
class TMv_ForgotPasswordForm extends TCv_Form 
{
	use TMt_PagesContentView;
	
	protected $explanation = '';
	protected $reclaim_menu_item = '';

	/**
	 * TMv_ForgotPasswordForm constructor.
	 */
	public function __construct()
	{
		parent::__construct('forgot_password_form');
		$this->setButtonText('Reset Your Password');
		$this->addClass('stacked');
		
	}
			
	/**
	 * Turns on instant confirmation which uses AJAX to provide feedback.
	 * @deprecated
	 */
	public function useInstantConfirmation()
	{
		// Do nothing
	}
	
	/**
	 * Configure the form
	 */
	public function configureFormElements()
	{
		$this->addClassCSSFile('TMv_ForgotPasswordForm');
		
		//$this->setSuccessURL($this-);
		

		if(trim($this->explanation) != '')
		{
			$explanation = new TCv_View();
			$explanation->addClass('explanation');
			$explanation->addText($this->explanation);
			$this->attachView($explanation);
		}
		
		$email = new TCv_FormItem_TextField('email', TC_localize('Email','Email'));
		$email->setIsRequired();
		$email->disableAutoComplete();
		$this->attachView($email);
		
		
//		$reclaim_url = new TCv_FormItem_Hidden('reclaim_url', '');
//		if($this->reclaim_menu_item != '')
//		{
//			$reclaim_menu_item = TMm_PagesMenuItem::init( $this->reclaim_menu_item);
//			$reclaim_url->setValue($reclaim_menu_item->pathToFolder());
//		}
//		else // no menu item, loading for admin
//		{
//			$reclaim_url->setValue('/admin/login/do/reset-password/');
//		}
//		$this->attachView($reclaim_url);
//
		$this->attachSubmitButton();
		
		
	}
	

	/**
	 * Ensure form skips authentication
	 * @return bool
	 */
	public static function customFormProcessor_skipAuthentication()
	{
		return true;
	}
	
	/**
	 * Turns off database primary action
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return bool
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		return false;
	}
	
	
	/**
	 * The form validation that ensures that a user exists and if so, it sends the reset email
	 *
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @uses TMv_ResetPasswordEmail::send()
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$user = TMm_User::userWithEmail($form_processor->formValue('email'));
	
		if($user)
		{
			TC_message("A password reset email has been sent to the email address you provided. Follow the instructions in that email to reset your password.");
			
			$email = TMv_ResetPasswordEmail::init($user);
			
			if(TC_isTungstenView())
			{
				$email->setAsTungstenAdminReset();
			}
			else
			{
				$email->setAsFrontEndRouting();
			}
			$email->send();
			
		}
		
		// multiple entries, fail
		else
		{
			$form_processor->fail('The email you provided is not in our system or it may not be unique within our system. Please contact an administrator for assistance.');
		}
	}




	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$explanation = new TCv_FormItem_HTMLEditor('explanation','Explanation');
		$explanation->setDefaultValue('<h2>Account Retrieval</h2><p>If you have an existing account or if you forgot your password, you can reset your password by entering your email address in the field below.</p>');
		$explanation->setHelpText('The text that will be shown the the user when they are viewing the form.');
		$explanation->useBasicEditor();
		
		$form_items[] = $explanation;
		
		
		$redirect = new TCv_FormItem_Select('reclaim_menu_item','Reset Password Page');
		$redirect->setHelpText('Select a page that has the form to reset a password. This is the page that the email will link to.');
		$redirect->useFiltering();
			/** @var TMm_PagesMenuItem $menu_0 */
			$menu_0 = TMm_PagesMenuItem::init(0);
			foreach($menu_0->descendants() as $menu_item)
			{
				$titles = array();
				foreach($menu_item->ancestors() as $ancestor)
				{
					$titles[] = $ancestor->title();
				}
				
				$redirect->addOption($menu_item->id(), implode(' – ', $titles));
			}
		
		$form_items[] = $redirect;
		
		$explanation = new TCv_FormItem_TextBox('account_found_text','Account Found Text');
		$explanation->setDefaultValue('We found your account and have sent you an email with instructions on how to reset your password.');
		$explanation->setHelpText('The text that will be shown if the account is found.');
		$form_items[] = $explanation;
		
		$explanation = new TCv_FormItem_TextBox('account_not_found_text','Account Not Found Text');
		$explanation->setDefaultValue("You don't appear to be in the system. Sign up today or contact us.");
		$explanation->setHelpText("The text that will be shown if the account is not found.");
		$form_items[] = $explanation;
		
		
		
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string { return 'Forgot password form'; }
	public static function pageContent_IconCode() : string { return 'fa-key'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A form showing an email field for those who have forgotten their password.'; 
	}

	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		return [
			'explanation' => true,
			'account_found_text' => false,
			'account_not_found_text' => false,
		
		];
	}

	
}
?>