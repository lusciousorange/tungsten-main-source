<?php
/**
 * Class TMv_EmailResetPasswordForm
 *
 * A form that handles a password reset attempt via a code passed in the URL
 */
class TMv_EmailResetPasswordForm extends TCv_FormWithModel
{
	use TMt_PagesContentView;
	
	protected ?TMm_User $user = null;
	protected ?string $code = null;

	/**
	 * TMv_EmailResetPasswordForm constructor.
	 */
	public function __construct()
	{
		$this->determineUser();
		$this->addClass('stacked');
		if($this->user)
		{
			parent::__construct($this->user);
		}
		else
		{
			parent::__construct('TMm_User');
			$this->hideSubmitButton();
		}
		
	}


	/**
	 * Processes the URLs provided and determines what user is being looked up via the access code
	 */
	public function determineUser() : void
	{
		if(isset($_GET['id']) && isset($_GET['id_2']))
		{
			$user_id = $_GET['id'];
			$this->code = $_GET['id_2'];

			if(is_string($this->code) && $this->code != '')
			{
				if($user = TMm_User::userWithPasswordResetCode($this->code))
				{
					if($user->id() == $user_id)
					{
						$this->user = $user;
					}
				}
			}
		}
	}
	
	/**
	 * Configures the form
	 */
	public function configureFormElements()
	{
		// FIND THE USER WITH THE CODE PROVIDED
		if($this->user)
		{
			// Determine the success URL
			if(TC_isTungstenView())
			{
				$this->success_url = '/admin/login/';
				
			}
			else // public view
			{
				$login_module = TMm_LoginModule::init();
				$this->setSuccessURL($login_module->publicLoggedInHomepageURL());
			}
			
			
			$notice = new TCv_FormItem_Heading('notice', $this->user->fullName());
			
			$this->attachView($notice);
		
		
			$password = new TCv_FormItem_Password('password', 'New password');
			$password->setIsRequired();
			$password->setValidatePasswordFormat();
			$password->setValidateUniqueHistory();
			$this->attachView($password);
		
			$password_verify = new TCv_FormItem_Password('password_verify', 'Verify password');
			$password_verify->setIsRequired();
			$password_verify->setSaveToDatabase(false);
			$password_verify->setValidateMatchField($password->id());
			$this->attachView($password_verify);
	
			$code = new TCv_FormItem_Hidden('code','code');
			$code->setValue(htmlentities($this->code));
			$code->setSaveToDatabase(false);
			$this->attachView($code);
			
			$user_id = new TCv_FormItem_Hidden('user_id','user_id');
			$user_id->setValue($this->user->id());
			$user_id->setSaveToDatabase(false);
			$this->attachView($user_id);
			
			// Submit Button
			$submit = new TCv_FormItem_SubmitButton('submit', TC_localize('change_your_password_button','Change your password'));
			$this->setSubmitButton($submit);
		}
		
		else
		{
			$code_not_found_container = new TCv_View('code_not_found_container');
			
			// SHOW A MESSAGE SAYING 'NOT FOUND'
			$message = new TCv_View('password_code_not_found_message');
			$message->setTag('p');
			$message->addText(TC_getModuleConfig('login','password_code_note_found'));
			$code_not_found_container->attachView($message);
			
			// SHOW THE BUTTON TO TRY AGAIN
			$login_module = TMm_LoginModule::init();
			$login_url = $login_module->publicLoggedInHomepageURL();
		
			
			$button = new TMv_QuickButton();
			$button->setURL($login_url);
			$button->addText(TC_localize('code_not_found_back_to_login','Return to sign in page'));
			$code_not_found_container->attachView($button);
			
			$this->attachView($code_not_found_container);
		}
		
	}
	
	/**
	 * An override to ensure this form processing skips authentication
	 * @return bool
	 */
	public static function customFormProcessor_skipAuthentication() : bool
	{
		return true;
	}
	
	/**
	 * Ensures that the password_reset_code is emptied and that proper messaging is sent to the user.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor) : void
	{

		// override messaging
		TC_messageConditionalTitle(TC_localize('password_updated_message','Password updated'), true);
		TC_messageConditionalTitle(TC_localize('password_not_updated_message','Password not updated'), false);


		/** @var TMm_User $user */
		$user = $form_processor->model();

		if($user->passwordResetCode() != $form_processor->formValue('code'))
		{
			$form_processor->fail('The password reset code you are using is no longer valid. Please use the latest email or try the reset link again to obtain a fresh code.');
		}
		$form_processor->addDatabaseValue('password_reset_code','');


	}
	
	/**
	 * Run after the processor that logs them in if it's possible for the user
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		/** @var TMm_User $user */
		$user = $form_processor->model();
		
		if($user)
		{
			$user->setAsLoggedIn();
		}
		
	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();	
	
		
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string { return 'Forgot password reset form'; }
	public static function pageContent_IconCode() : string { return 'fa-key'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public static function pageContent_ViewDescription(): string
	{ 
		return 'The form that is shown when someone navigates to a page to reset their password.'; 
	}
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		return [
			'not_found' => true,
			
		];
	}
}
?>