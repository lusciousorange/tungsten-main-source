<?php
/**
 * Class TMm_UserList
 */
class TMm_UserList extends TCm_ModelList
{
	protected bool $process_filter_list_returns_as_array = false;

	/**
	 * TMm_UserList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_User',false);

		// Override the default all_items_query
		$this->all_items_query = "SELECT * FROM `users` WHERE is_active = 1 ORDER BY ".TMm_User::$primary_table_sort;
	}
	
	/**
	 * Returns all the users in the system
	 * @return TMm_User[]|TCm_Model[]
	 */
	public function users()
	{
		return $this->models();
	}
	
	/**
	 * Returns the most recent X users that were added to the system
	 * @param int $num_to_show (Optional) Default 10
	 * @return TMm_User[]
	 */
	public function recentUsers($num_to_show = 10)
	{
		$recent_users = array();
		$num_found = 0;
		foreach($this->users() as $user)
		{
			$count = 0;
			$index = $user->dateAdded().$count;
			while(isset($recent_users[$index]))
			{
				$count++;
				$index = $user->dateAdded().$count;
			
			}
			$recent_users[$index] = $user;
			
			// count the number found, return if we hit that number
			$num_found++;
			if($num_found >= $num_to_show)
			{
				return $recent_users;
			}
		}
	
		// return no matter what. 
		// only happens if number required is less than found
		return $recent_users;
	}
	
	/**
	 * Returns the users in a given group
	 * @param TMm_UserGroup $group
	 * @return TMm_User[]
	 * @uses TMm_User::inGroup()
	 */
	public function usersInGroup($group)
	{
		$users = array();
		foreach($this->users() as $user)
		{
			if($user->inGroup($group))
			{
				$users[$user->id()] = $user;
			}
		}
		return $users;
	}

	/**
	 * Returns the users who are a part of at least one group of any type
	 * @return TMm_User[]
	 * @uses TMm_User::inGroup()
	 */
	public function usersInAnyGroup()
	{
		$users = array();
		$query = "SELECT u.* FROM users u INNER JOIN user_group_matches USING (user_id) WHERE is_active = 1 GROUP BY u.user_id ORDER BY "
				.TMm_User::$primary_table_sort;
		$result = $this->DB_Prep_Exec($query);
		while($row = $result->fetch())
		{
			$users[] = TMm_User::init( $row);
		}
		return $users;
	}

	//////////////////////////////////////////////////////
	//
	// FILTERING
	//
	//////////////////////////////////////////////////////

	/**
	 * Processes the filter values and returns a PDOStatement of the users
	 * @param array $filter_values
	 * @return PDOStatement
	 */
	public function processFilterList($filter_values)
	{
		$values = $this->generateFilterQuery($filter_values);
		
		$query = $values['query'];
		$db_values = $values['db_values'];
		$where_clauses = $values['where_clauses'];
		
		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
		}


		if($this->process_filter_list_returns_as_array)
		{
			return [
				'db_query' => $query,
				'db_params' => $db_values
			];
		}
		else
		{
			$model_name = $this->modelClassName();
			$query .= "  ORDER BY ".$model_name::$primary_table_sort;

			return $this->DB_Prep_Exec($query, $db_values);

		}


	}

	/**
	 * Indicates that this user list returns the process filter list as an array, rather than a PDOStatement. This is for
	 * backwards compatibility to not break older setups. Newer lists should implement this.
	 * @return void
	 */
	public function setProcessFilterListReturnAsArray() : void
	{
		$this->process_filter_list_returns_as_array = true;
	}

	/**
	 * An internal function to generate the filter values which can be extended
	 * @param array $filter_values
	 * @return array An array of three values with indices for "query", "db_values" and "where_clause".
	 */
	protected function generateFilterQuery($filter_values)
	{
		$query = "SELECT u.* FROM users u ";
		$db_values = array();
		$where_clauses = array();
		
		if(isset($filter_values['is_active']))
		{
			if($filter_values['is_active'] != '')
			{
				$where_clauses[] = "u.is_active = :is_active ";
				$db_values[':is_active'] = $filter_values['is_active'];
			}
		}
		else // if not set, hide those inactive ones by default
		{
			$where_clauses[] = "u.is_active = 1";
		}
		
		
		if($filter_values['group_id'] > 0)
		{
			$query .= " INNER JOIN user_group_matches um USING(user_id) ";
		}
		
		
		if($filter_values['search'] != '')
		{
			$search_values = $this->searchFilterValues($filter_values['search']);
		//	$this->addConsoleDebug($search_values);
			foreach($search_values['db_values'] as $index => $value)
			{
				$db_values[$index] = $value;
			}
			
			
			
			$where_clauses[] = "(".implode(' OR ', $search_values['where_clauses']).")";
			
		}
		
		if($filter_values['group_id'] > 0)
		{
			$where_clauses[] = "um.group_id = :group_id ";
			$db_values[':group_id'] = $filter_values['group_id'];
		}
		
		return [
			'query' => $query,
			'where_clauses' => $where_clauses,
			'db_values' => $db_values
		];
	}
	
	/**
	 * Returns the array of search fields which contains two indices for `db_values` and `where_clauses`. Due to the nature
	 * of how search tends to be compacted with an OR, we need a way to override this so that it works properly if we want
	 * to add more filters for users.
	 * @param string $search_string
	 * @return array
	 */
	public function searchFilterValues(string $search_string) : array
	{
		$response = [
			'db_values' => [],
			'where_clauses' => []
		];
		
		$response['db_values'][':search'] = $search_string.'%';
		
		$response['where_clauses']['first_name'] = 'first_name LIKE :search';
		$response['where_clauses']['email'] = 'email LIKE :search';
		
		if(TC_getConfig('use_single_name_field'))
		{
			// Search the first name, but with a space beforehand to catch the last name
			$response['where_clauses']['first_name_spaces'] = 'first_name LIKE :search_w_space';
			$response['db_values'][':search_w_space'] = "% ". $search_string.'%';
		}
		else // last name is used, search it AND the concatenation
		{
			$response['where_clauses']['last_name'] = 'last_name LIKE :search';
			$response['where_clauses']['concat_name'] = "CONCAT(first_name,' ',last_name) LIKE :search";
		}
		
		return $response;
	}
	
	/**
	 * A filter that searches for specific names
	 * @param $filter_values
	 * @return array
	 */
	public function processSearchNames($filter_values)
	{
		$query = "SELECT u.* FROM users u ";
		$db_values = array();
		$where_clauses = array();
		$where_clauses[] = "u.is_active = 1";

		if(isset($filter_values['group_id']) && $filter_values['group_id'] > 0)
		{
			$query .= " INNER JOIN user_group_matches um USING(user_id) ";	
		}
		
		
		if(isset($filter_values['search']) && $filter_values['search'] != '')
		{
			$where_clauses[] = "(first_name LIKE :search OR last_name LIKE :search OR CONCAT(first_name, ' ', last_name) LIKE :search)";
			$db_values[':search'] = '%'.$filter_values['search'].'%';
		}
		
		if(isset($filter_values['group_id']) && $filter_values['group_id'] > 0)
		{
			$where_clauses[] = "um.group_id = :group_id ";
			$db_values[':group_id'] = $filter_values['group_id'];
		}




		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
		}
		
		$query .= "  ORDER BY u.last_name ASC";
		
		$result = $this->DB_Prep_Exec($query, $db_values);
		
		return $this->returnValueForFilterResult($filter_values, $result);
		
	}

	/**
	 * Searches the names of users for the provided name.
	 * @param string $name_string The string that is being search for
	 * @return TMm_User[]
	 */
	public function usersWithNameSearch($name_string)
	{

		$users = array();
		$query = "SELECT * FROM `users` WHERE is_active = 1 AND ";

		$query_values = array();
		$query .= '( last_name LIKE :name_part OR ';
		$query .=  "CONCAT(first_name, ' ', last_name) LIKE :name_part )";
		$query_values['name_part'] = $name_string.'%';

		$query .= " ORDER BY last_name, first_name LIMIT 100";
		$result = $this->DB_Prep_Exec($query, $query_values);


		while($row = $result->fetch())
		{
			/** @var TMm_User $user */
			$users[] = TMm_User::init( $row);;
		}

		return $users;
	}
	
	/**
	 * Searches the names and emails of users for the provided name.
	 * @param string $name_string The string that is being search for
	 * @return TMm_User[]
	 */
	public function usersWithNameAndEmailSearch($name_string)
	{
		
		$users = array();
		$query = "SELECT * FROM `users` WHERE is_active = 1 AND ";
		
		$query_values = array();
		$query .= '( last_name LIKE :name_part OR ';
		$query .=  "CONCAT(first_name, ' ', last_name) LIKE :name_part  OR email LIKE :email_part )";
		$query_values['name_part'] = $name_string.'%';
		$query_values['email_part'] = '%'.$name_string.'%';
		$query .= " ORDER BY last_name, first_name LIMIT 100";
		$result = $this->DB_Prep_Exec($query, $query_values);
		
		
		while($row = $result->fetch())
		{
			/** @var TMm_User $user */
			$users[] = TMm_User::init( $row);;
		}
		
		return $users;
	}
	
	/**
	 * Searches the names of users for the provided name. This method can be used with the useFiltering() option for
	 * large select boxes with many users.
	 * @param string $name_string The string that is being search for
	 * @param int|bool $selected_id The id of the selected item if necessary
	 * @return string[]|bool
	 */
	public function namesForSearchString($name_string, $selected_id = false)
	{
		/** @var TMm_User $user */


		$selected_ids = explode(',', $selected_id);
		$users = array();
		foreach($selected_ids as $id)
		{
			if($user = TMm_User::init( $id))
			{
				$users[$user->id()] = $user->fullName() . ' ( ' . $user->email() . ')';
			}
		}


		foreach($this->usersWithNameSearch($name_string) as $user)
		{
			$users[$user->id()] = $user->fullName();
		}

		return $users;
	}


	/**
	 * Searches the names and emails of users for the provided name. This method can be used with the useFiltering()
	 * option for large select boxes with many users.
	 * @param string $name_string The string that is being search for
	 * @param int|bool|array $selected_id The id of the selected item if necessary
	 * @return string[]|bool
	 */
	public function namesAndEmailsForSearchString($name_string, $selected_id = false)
	{
		/** @var TMm_User $user */

		$selected_ids = explode(',', $selected_id);
		$users = array();
		foreach($selected_ids as $id)
		{
			if($user = TMm_User::init( $id))
			{
				$users[$user->id()] = $user->fullName() . ' ( ' . $user->email() . ')';
			}
		}

		// Find all that match
		foreach($this->usersWithNameAndEmailSearch($name_string) as $user)
		{
			$users[$user->id()] = $user->fullName().' ( '.$user->email().')';
		}



		return $users;
	}
	
	/**
	 * Bulk updates a list of IDs that need to be changed
	 * @param string $user_group_id The user_group being submitted
	 * @param string $user_ids_strings A comma-separated list of user IDs that to be changed
	 */
	public function bulkUpdateUserGroup(string $user_group_id, string $user_ids_strings)
	{
		$user_group = false;
		if($user_group_id > 0)
		{
			$user_group = TMm_UserGroup::init($user_group_id);
		}
		
		// Do nothing if a blank value is provided
		if($user_group_id == '')
		{
			return;
		}
		
		$user_ids = explode(',', $user_ids_strings);
		foreach($user_ids as $user_id)
		{
			if($user = TMm_User::init($user_id))
			{
				if($user_group)
				{
					$user->updateGroupSetting($user_group, true);
				}
				// Specifically clear all the user groups
				elseif($user_group_id == 'delete')
				{
					$user->removeAllUserGroups();
				}
				
			}
		}
	
	}
	
	/**
	 * Bulk updates a list of IDs that need to be changed
	 * @param string $is_active Indicates if the user should be set to active or inactive
	 * @param string $user_ids_strings A comma-separated list of user IDs that to be changed
	 */
	public function bulkUpdateUserActive(string $is_active, string $user_ids_strings)
	{
		
		// Do nothing if a blank value is provided
		if($is_active === '')
		{
			return;
		}
		
		$user_ids = explode(',', $user_ids_strings);
		foreach($user_ids as $user_id)
		{
			if($user = TMm_User::init($user_id))
			{
				if($is_active)
				{
					$user->activate();
				}
				else
				{
					$user->deactivate();
				}
				
			}
		}
		
	}
}

?>