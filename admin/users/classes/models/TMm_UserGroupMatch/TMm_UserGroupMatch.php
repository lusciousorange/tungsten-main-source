<?php


class TMm_UserGroupMatch extends TCm_Model
{
	use TSt_ModelWithHistory;
	
	
	protected $group_id, $user_id;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id';
	public static $table_name = 'user_group_matches';
	public static $model_title = 'User group match';
	public static $primary_table_sort = 'date_added DESC';

	/**
	 * TMm_UserGroupMatch constructor.
	 */
	public function __construct($matchId)
	{
		parent::__construct($matchId);
		$this->setPropertiesFromTable();
	}

	/**
	 * Returns the group id
	 * @return int
	 */
	public function groupId()
	{
		return $this->group_id;
	}

	/**
	 * Returns the user group
	 * @return bool|TMm_UserGroup
	 */
	public function group()
	{
		return TMm_UserGroup::init($this->groupId());
	}

	/**
	 * Returns the user id
	 * @return integer
	 */
	public function userId()
	{
		return $this->user_id;
	}

	/**
	 * Returns the user
	 * @return bool|TMm_User
	 */
	public function user()
	{
		return TMm_User::init($this->userId());
	}
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'user_id' => [
					'title'         => 'User ID',
					'comment'       => 'The id of the user ',
					'type'          => 'TMm_User',
					'nullable'      => false,
					
					// If the user is deleted, set this value to null but we want to keep the entry
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'CASCADE'
					],
				],
				
				'group_id' => [
					'title'         => 'Group ID',
					'comment'       => 'The id of the user group ',
					'type'          => 'TMm_UserGroup',
					'nullable'      => false,
					
					// If the group is deleted, set this value to null but we want to keep the entry
					'foreign_key'   => [
						'model_name'    => 'TMm_UserGroup',
						'delete'        => 'CASCADE'
					],
				],
				
				// Unique constraint on user-group matches
				'table_keys' => [
					'user_id_group_id' => ['index' => "(`user_id`,`group_id')", 'type' => 'unique'],
				]
			
			];
	}
	


}