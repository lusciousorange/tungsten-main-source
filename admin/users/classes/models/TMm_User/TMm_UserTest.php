<?php
class TMm_UserTest extends TC_ModelTestCase
{
	/**
	 * @var int
	 * A counter to ensure that every request to create a test user generates a different email. Tests CANNOT be
	 * based on this value since it depends on how many tests are run and in what order.
	 */
	public static $test_user_count = 1;

//	protected function setUp () : void
//	{
//		parent::setUp();
//		$this->shopping_cart = $this->createFilledShoppingCart();
//	}
	
	/**
	 * @param array $override_values
	 * @return TMm_User
	 * @deprecated use TMm_UserTest::generate instead
	 */
	protected function createTestUser(array $override_values = [])
	{
		return static::generate($override_values);
	}
	
	/**
	 * Tests the creation of a user with no email address
	 */
	public function testCreateUserWithNoEmail()
	{
		$user =  static::generate(['email' => '']);
		
		$this->assertTungstenSchemaFieldValidationError('email', "user with no email created");
		$this->assertIsNotObject($user, 'User incorrectly created with no email');
	}
	
	/**
	 * Tests the creation of a user with no email address
	 */
	public function testCreateDuplicateEmail()
	{
		// Create the first user
		$user = static::generate();
		
		// Ensure it exists
		$this->assertIsObject($user,"Creation of first user failed");
		
		// Try to create a second user with the same email
		// This should trigger a process error
		$user_2 = static::generate(['email' => $user->email()]);
		
		// We EXPECT an error to happen because the email validation fails
		$this->assertTungstenSchemaFieldValidationError('email', "user with duplicate email created");
		
	}
	
	/**
	 * Tests the updating of a user's email
	 */
	public function testUpdateEmail()
	{
		$user = static::generate();
		
		// Test same email
		$user->updateWithValues(['email' => $user->email()]);
		$this->assertNoTungstenProcessError();
		
		// Create new user to test against
		$user_2 = static::generate();
		
		// Attempt to change user 1 email to what user 2 has
		$user->updateWithValues(['email' => $user_2->email()]);
		$this->assertTungstenSchemaFieldValidationError('email', "user email changed to another user's email");
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// ACTIVE STATE
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Tests the ability to detect if a user is active or not
	 */
	public function testActiveAndInactive()
	{
		$user = static::generate();
		
		// Test default value which is normally set to active
		$this->assertEquals(1, $user->isActive(),'Default user state is not active');
		
		// Deactivate and confirm value
		$user->deactivate();
		$this->assertEquals(0, $user->isActive(),'Deactivating user failed');
		
	}
	
	//////////////////////////////////////////////////////
	//
	// USER GROUPS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Tests adding a user group to a user
	 * @return array
	 */
	public function testAddToUserGroup()
	{
		$user = static::generate();
		
		$user_group = TMm_UserGroup::createWithValues(['title' => 'Test Add Group']);
		
		// Add to user group
		$user->updateGroupSetting($user_group, true);
		$this->assertTrue($user->inGroup($user_group));
		
		return ['user' => $user, 'group' => $user_group];
	}
	
	/**
	 * Tests removing a user group from a user
	 * @param array $values An array with two indices for the user and the group
	 * @depends testAddToUserGroup
	 */
	public function testRemoveFromUserGroup(array $values)
	{
		$user = $values['user'];
		$user_group = $values['group'];
		
		// Add to user group
		$user->updateGroupSetting($user_group, false);
		$this->assertFalse($user->inGroup($user_group));
		
	}
	
	////////////////////////////////////////////////
	//
	// GENERATE
	//
	////////////////////////////////////////////////
	/**
	 * Generates a test team member
	 * @param array $override_values
	 * @return TMm_User
	 */
	public static function generate(array $override_values = []) : ?TMm_User
	{
		// Avoid duplicate emails
		$rand_num = rand(1,99999);
		$rand_micro = microtime(true);
		
		$values = array(
			'first_name' => 'Test',
			'last_name' => 'User '.$rand_num,
			'email' => 'testuser_'.$rand_num.$rand_micro.'@website.com',
			'is_active' => 1,
		);
		
		$values = array_merge($values, $override_values);
		
		return TMm_User::createWithValues($values);
	}
	
	
}