<?php

/**
 * Class TMm_User
 *
 * The primary model class for all users in the system. This class should NEVER be edited, but instead extended
 * to add functionality to users.
 */
class TMm_User extends TCm_Model
{
	use TSt_ModelWithAPI;
	use TMt_Searchable;
	
	use TSt_ModelWithHistory;
	
	// DATABASE COLUMNS
	protected $user_id, $salutation, $email, $password, $password_reset_code, $title;
	
	
	protected ?string $home_phone;
	protected ?string $cell_phone;
	protected ?string $work_phone;
	
	protected $is_developer = false; // Indicates if this user is a developer
	
	protected $verify_email_code;

	protected int $failed_login_attempts = 0;
	
	// NAMES
	// The system uses separate first and last name fields by default
	// If you enable the `use_single_name_field` features switch, then it just uses first_name
	protected $first_name = '';
	protected $last_name = '';
	
	// OTHER VALUES
	protected string $photo_filename;
	
	// Workflow values that might break and require definition
	protected bool $workflow_email_comments = false;
	protected bool $workflow_email_stages = false;
	protected bool $workflow_email_assigned = false;
	/**
	 * @var bool
	 * Indicates if this user is currently active in the system
	 */
	protected $is_active;
	
	/**
	 * @var string
	 * The api key for this user to access the API if enabled
	 */
	protected $api_key;
	
	/**
	 * @var string
	 * The date that the  api key was generated
	 */
	protected $api_key_date;
	
	/**
	 * @var bool
	 * A yes/no value ot indicate if this user enables/disables error reporting
	 */
	protected $error_reporting;
	
	// CLASS-DEFINED PROPERTIES
	protected $user_groups = false;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'user_id';
	public static $table_name = 'users'; // [string] = The table for this class
	public static $model_title = 'User';
	public static $primary_table_sort = 'last_name ASC, first_name ASC';
	
	/**
	 * TMm_User constructor.
	 * @param array|int $user_id
	 */
	public function __construct($user_id)
	{
		parent::__construct($user_id);
		if(!$this->exists) { return null; }
		
		// Do not call fullName() from inside constructor. Potentially creates infinite loops
		
	}
	
	/**
	 * @return string
	 */
	public static function uploadFolder()
	{
		return TC_getConfig('saved_file_path').'/users/';
	}
	
	/**
	 * Obtain the full name of the user. This can be first/last or salutation/last. If the site is set to use a
	 * single name field, then it only uses the first_name value
	 * @param bool $salutation Indicates if the salutation option should be used instead of a full name
	 * @return string
	 */
	public function fullName($salutation = false)
	{
		if($this->user_id > 0)
		{
			if($salutation && isset($this->saluation))
			{
				if(TC_getConfig('use_single_name_field'))
				{
					return $this->salutation.' '.$this->first_name;
				}
				else
				{
					return $this->salutation.' '.$this->last_name;
				}
				
			}
			else
			{
				if(TC_getConfig('use_single_name_field'))
				{
					return $this->first_name;
				}
				else
				{
					return $this->first_name.' '.$this->last_name;
				}

			}
		}
		else
		{
			return 'Unknown';
		}
	}
	
	/**
	 * Returns the first and last initials of the user
	 * @return string
	 */
	public function initials()
	{
		return strtoupper(substr($this->first_name,0,1).substr($this->last_name,0,1));
	}
	
	/**
	 * Returns the user's first name
	 * @return string
	 */
	public function firstName()
	{
		return $this->first_name;
	}
	
	/**
	 * Returns the user's last name
	 * @return string
	 */
	public function lastName()
	{
		return $this->last_name;
	}
	
	/**
	 * Returns if the user is active.
	 * @return bool
	 */
	public function isActive()
	{
		return $this->is_active;
	}
	
	/**
	 * Returns if this user is a developer in the system.
	 * @return bool
	 */
	public function isDeveloper()
	{
		return $this->is_developer;
	}
	
	/**
	 * Activates the user
	 *
	 */
	public function activate()
	{
		$this->updateDatabaseValue('is_active',1);
	}
	
	/**
	 * Activates the user
	 *
	 */
	public function deactivate()
	{
		$this->updateDatabaseValue('is_active',0);
	}
	
	/**
	 * Disables this user account
	 */
	public function toggleDisable()
	{
		if($this->isActive())
		{
			$this->updateDatabaseValue('is_active', '0');
		}
		else
		{
			$this->updateDatabaseValue('is_active', '1');
		}
		
	}
	
	/**
	 * Passthrough function that returns fullName()
	 * @return string
	 */
	
	public function title()
	{
		return $this->fullName();
	}
	
	/**
	 * Returns the title of this user with the email in brackets after it
	 * @return string
	 */
	public function titleWithEmail()
	{
		return $this->title().' ('.$this->email().')';
	}
	
	
	/**
	 * Returns if the name as a given string in it. THe search is NOT case-sensitive
	 * @param string $string The string to search for
	 * @return bool
	 */
	public function nameHasString($string)
	{
		return stripos($this->fullName(), $string) !== false;
	}
	
	/**
	 * Returns the home phone
	 * @return string
	 */
	public function homePhone()
	{
		return $this->home_phone;
	}
	
	/**
	 * Returns the cell phone
	 * @return string
	 */
	public function cellPhone()
	{
		return $this->cell_phone;
	}
	
	/**
	 * Returns the moblie phone
	 * @return string
	 */
	public function mobilePhone()
	{
		return $this->cellPhone();
	}
	
	public function configureErrorReporting()
	{
		if($this->error_reporting > 0)
		{
			ini_set('display_errors','1');
			error_reporting(E_ALL);
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// EMAIL
	//
	// Methods related to a user's email, which is often
	// used as a primary method to identify people in the system.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the email address
	 * @return string
	 */
	public function email()
	{
		return $this->email;
	}
	
	/**
	 * Validates if an email being provided is a valid one to use. This method is called during the validation process
	 * of a form. It checks if anyone other than the this user has this email address. If so, then using this email won't work.
	 *
	 * @param string $field_id The id of the field being validated
	 * @param string $value The value being submitted
	 * @param TCm_Model|null $model The model of the object being modified. If it is not an editor,then it will be a
	 * blank model that will return false on exists()
	 * @return TCm_FormItemValidation
	 * @deprecated Replaced by schema validations instead
	 */
	public static function validateUniqueEmail($field_id, $value, $model = null)
	{
		
		
		$validation = new TCm_FormItemValidation($field_id, 'Validate Unique Email');
		if(trim($value) == '')
		{
			return $validation;
		}
		
		$model_id = null;
		if(($model instanceof TCm_Model) && $model->id() > 0)
		{
			$model_id = $model->id();
		}
		
		
		if(static::emailExists($value,$model_id ))
		{
			$validation->failWithMessage('Email already exists in the system.');
		}
		
		
		return $validation;
		
	}
	
	/**
	 * Validates if an email exists in the system already
	 * @param string $email_address The email address we're looking for
	 * @param null|int $ignored_user_id The id of a user we want to ignore. This is commonly used when editing an
	 * existing user to ignore their ID
	 * @return bool
	 */
	public static function emailExists(string $email_address, $ignored_user_id = null)
	{
		$model = new TCm_Model(false);
		
		$query = "SELECT email FROM `users` WHERE email = :email";
		$values = array('email'=> $email_address);
		
		if($ignored_user_id > 0)
		{
			$query .= ' AND user_id != :user_id';
			$values['user_id'] = $ignored_user_id;
		}
		$query .= ' LIMIT 1'; // Never need more than one
		
		$result = $model->DB_Prep_Exec($query, $values);
		if($row = $result->fetch())
		{
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Validates if an email is available in the system in the system already
	 * @param string $email_address The email address we're looking for
	 * @param null|int $ignored_user_id The id of a user we want to ignore. This is commonly used when editing an
	 * existing user to ignore their ID
	 * @return bool
	 */
	public static function emailIsAvailable(string $email_address, $ignored_user_id = null)
	{
		return ! static::emailExists($email_address, $ignored_user_id);
	}
	
	/**
	 * Checks if this user can use a new email address that is provided. This will ignore this own user's email
	 * address a possibility
	 * @param string $new_email The new email to be used.
	 * @return bool
	 */
	public function canUseNewEmail(string $new_email)
	{
		return static::emailIsAvailable($new_email, $this->id());
	}
	
	/**
	 * Validates if a name being  provided is unique in the system. Not all systems require this
	 *
	 * @param string $field_id The id of the field being validated
	 * @param string $first_name The first_name being submitted
	 * @param string $last_name The last being submitted
	 * @param TCm_Model $model The model of the object being modified. If it is not an editor,then it will be a blank model that will return false on exists()
	 * @return TCm_FormItemValidation
	 */
	public static function validateUniqueName($field_id, $first_name, $last_name, $model = null)
	{
		
		
		$validation = new TCm_FormItemValidation($field_id, 'Validate Unique Name');
		$query = "SELECT user_id FROM `users` WHERE first_name = :first_name AND last_name = :last_name ";
		$values = array('first_name'=> $first_name,'last_name' => $last_name);
		
		if($model && $model->id() > 0)
		{
			$query .= ' AND user_id != :user_id';
			$values['user_id'] = $model->id();
		}
		$result = $validation->DB_Prep_Exec($query, $values);
		if($row = $result->fetch())
		{
			$validation->failWithMessage('First and last name already exists in the system.');
		}
		
		return $validation;
		
	}
	
	
	/**
	 * Attempts to find a user with the provided email address and returns them if found, otherwise false.
	 *
	 * @param string $email
	 * @return bool|TMm_User
	 */
	public static function userWithEmail($email)
	{
		// Avoid any scenario that searches for blank emails
		if(trim($email) == '')
		{
			return false;
		}
		
		$query = "SELECT * FROM `users` WHERE email = :email LIMIT 1";
		$result = static::DB_RunQuery($query, ['email' => $email]);
		if($row = $result->fetch())
		{
			return TMm_User::init($row);
		}
		
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// PASSWORD
	//
	// Methods related to a user's password
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Creates a random password and sets it to the user's account.
	 * @return string
	 */
	public function generatePassword()
	{
		$password = new TCu_Text(TCu_Text::generatePassword()); // generate the password
		
		$this->setPassword($password->hash());
		
		return $password->getOriginalText();
	}
	
	/**
	 * Sets the password for the user to a new string provided
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$text = new TCu_Text($password);
		$password = $text->hash();
		
		$query = "UPDATE users SET password = :password WHERE user_id = :user_id";
		$this->DB_Prep_Exec($query, array('password' => $password,':user_id' => $this->id()));
		
	}
	
	/**
	 * Returns the password hash for the user
	 * @return string
	 */
	public function passwordHash()
	{
		return $this->password;
	}
	
	/**
	 * Generates the code that is used to reset a password. This will save the code to the user's reset column in the
	 * users table to ensure that if someone tries to reset the password, it only works if the code is shown.
	 * @return bool|string|TCu_Text
	 */
	public function generatePasswordResetCode()
	{
		$code = new TCu_Text(TCu_Text::generatePassword()); // generate the password
		$code = $code->hash();
		
		$code = substr($code,0,30);
		
		$this->updateDatabaseValue('password_reset_code', $code);
		
		return $code;
	}
	
	/**
	 * Authenticates a provided password
	 * @param string $password
	 * @return bool
	 */
	public function authenticatePassword(string $password) : bool
	{
		if(!$this->isActive())
		{
			TC_message('Your account is disabled.', false);
			return false;
		}
		$password_text = new TCu_Text($password);
		if($password_text->verifyMatchesHash($this->passwordHash()))
		{
			$this->processSuccessfulLogin();
			return true;
		}

		$this->processFailedLoginAttempt();
		return false;
		
	}
	
	/**
	 * Returns if this user has access to Tungsten
	 * @return bool
	 */
	public function hasTungstenAccess()
	{
		if(!$this->isActive())
		{
			return false;
		}
		foreach($this->userGroups() as $user_group)
		{
			if($user_group->tungstenAccess())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the user with a provided password_reset_code
	 * @param $code
	 * @return bool|TMm_User
	 */
	public static function userWithPasswordResetCode($code)
	{
		$query = "SELECT * FROM `users` WHERE password_reset_code = :password_reset_code LIMIT 1";
		$result = static::DB_RunQuery($query, array('password_reset_code' => $code));
		if($row = $result->fetch())
		{
			return TMm_User::init( $row);
		}
		
		return false;
	}
	
	/**
	 * Returns the password reset code for this user
	 * @return string
	 */
	public function passwordResetCode()
	{
		return $this->password_reset_code;
	}
	
	
	/**
	 * Performs a check on the submitted password to ensure it meets password formats.
	 * @param string $field_id The id of the field being validated
	 * @param string $value The  value being submitted
	 * @param TCm_Model $model The model of the object being modified. If it is not an editor,then it will be a blank model that will return false on exists()
	 * @param bool $is_required Indicate if the value is required. Although checked separately in most cases,
	 * necessary to ensure this validation doesn't trigger on empty values which would be valid.
	 * @return TCm_FormItemValidation
	 */
	public static function validatePasswordFormat($field_id, $value, $model, $is_required)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Password Format');
		
		// Case in which not required and the value is blank. Don't fail validation.
		if(!$is_required && $value == '')
		{
			return $validation;
		}
		
		
		if(!preg_match("/^.{6}/i", $value))
		{
			$validation->failWithMessage('Password must be at least six (6) characters long.');
		}
		
		return $validation;
		
	}
	
	/**
	 * Validates if the provided password is correct
	 * @param string $value
	 * @return TCm_FormItemValidation
	 */
	public function validatePassword($value)
	{
		$validation = new TCm_FormItemValidation($this->id(), 'Password Match');
		$password_text = new TCu_Text($value);
		
		
		if(!$password_text->verifyMatchesHash($this->passwordHash()))
		{
			$validation->failFieldWithMessage('Password', 'The password does not match the existing value in the system.');
		}
		
		return $validation;
		
	}
	
	/**
	 * Indicates if this user is currently logged into this session.
	 * @return bool
	 */
	public function isLoggedIn()
	{
		return $this->id() == TC_currentUser()->id();
	}
	
	/**
	 * Sets this user as logged in
	 */
	public function setAsLoggedIn()
	{
		// Only bother if we aren't logged in, no overrides
		if( !(TC_currentUser() instanceof TMm_User))
		{
			// Only active users can be logged in
			if($this->isActive())
			{
				// Only here after updates
				$_SESSION['tungsten_user_id'] = $this->id();
			}
		}
	}

	/**
	 * Returns the number of times this user has had a failed login attempt.
	 * @return int
	 */
	public function numFailedLoginAttempts() : int
	{
		return $this->failed_login_attempts;
	}

	/**
	 * Process function that is called with each failed login attempt, that handles checking for maximums. If it reaches
	 * a maximum, then the account is disabled and can only be turned back on by an administrator.
	 * @return void
	 */
	public function processFailedLoginAttempt() : void
	{
		if(TC_getConfig('password_max_failed_logins') > 0)
		{
			$this->updateDatabaseValue('failed_login_attempts', $this->numFailedLoginAttempts()+1);

			// Deal with them hitting the maximum
			if($this->numFailedLoginAttempts() >= TC_getConfig('password_max_failed_logins'))
			{
				// Deactivate the account
				$this->updateDatabaseValue('is_active', 0);
				TC_message('You have reached the maximum number of login attempts and your account has been disabled. Please contact an administrator.');
			}
		}
	}

	/**
	 * Triggered when a successful login happens, which resets the count
	 * @return void
	 */
	public function processSuccessfulLogin() : void
	{
		$this->updateDatabaseValue('failed_login_attempts', 0);

	}

	/**
	 * Validates if the password being entered has already be used previously
	 * @param $field_id
	 * @param $password
	 * @return TCm_FormItemValidation
	 */
	public function validatePasswordReuse($field_id, $password) : TCm_FormItemValidation
	{
		$validation = new TCm_FormItemValidation($field_id, 'Password Reuse');

		if(TC_getConfig('use_track_model_history') && TC_getConfig('password_prevent_reuse'))
		{
			// Get all the previous password hashes, to ensure they are never repeated
			$query = "SELECT password FROM _history_users WHERE user_id = :user_id GROUP BY password";
			$result = $this->DB_Prep_Exec($query, ['user_id' => $this->id()]);
			while($row = $result->fetch())
			{
				$old_hash = $row['password'];
				$password_text = new TCu_Text($password);

				if($password_text->verifyMatchesHash($old_hash))
				{
					$validation->failWithMessage(TC_localize('password_previously_used','The password has previously been used.'));
				}

			}
		}

		return $validation;

	}

	//////////////////////////////////////////////////////
	//
	// EMAIL VERIFICATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Generates the code for validating if a user account is connected to an email.
	 * @return string
	 */
	public function generateVerifyEmailCode()
	{
		$code = new TCu_Text(TCu_Text::generatePassword()); // generate the password
		$code = $code->hash();
		
		$code = substr($code,0,30);
		
		$this->updateDatabaseValue('verify_email_code', $code);
		
		return $code;
	}
	
	/**
	 * Sends the "verify email" code to their email
	 * @uses TMv_UserVerificationEmail
	 */
	public function sendVerifyEmailCode()
	{
		// Only bother send
		if( TC_getConfig('use_email_verification'))
		{
			// Only bother if they aren't active
			if(!$this->isActive())
			{
				TC_message(
					$this->email().': '.TC_localize('verify_email_message',
				"An email has been sent to verify your account. "));
					
				// Send the email
				$email = TMv_UserVerificationEmail::init($this);
				$email->send();
			}
			else
			{
				$this->addConsoleMessage('Email verification did not sent to already active user #'.$this->id().'. ');
			}
		}
		else
		{
			$this->addConsoleMessage('Email verification disabled');
		}
		
	}
	
	/**
	 * Verifies an email code for this user.
	 * @param string $code
	 * @return bool Indicates if the verification was successful
	 */
	public function verifyEmailWithCode(string $code) : bool
	{
		// Check that the code being provided matches and is non-empty
		if($code != '' && $this->verify_email_code == $code)
		{
			$values = [
				'is_active' => 1,
				'verify_email_code' => null,
			];
			$this->updateWithValues($values);
			TC_message(TC_localize('verify_email_message_verified','Your account has been verified'));
			return true;
		}
		else
		{
			TC_message(TC_localize('verify_email_message_not_verified','Your account could not be verified'),false);
			
			return false;
		}
		
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// USER GROUPS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this user is a System Administrator
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->inGroupID(1);
	}
	
	/**
	 * Returns if this user is a member of the provided group
	 * @param TMm_UserGroup $user_group
	 * @return bool
	 */
	public function inGroup($user_group)
	{
		return $this->inGroupID($user_group->id());
	}
	
	/**
	 * Returns if this user is a member of the group with the provided ID.
	 * @param int $group_id
	 * @return bool
	 */
	public function inGroupID($group_id)
	{
		$this->userGroups();
		return isset($this->user_groups[$group_id]);
	}
	
	/**
	 * Returns if this user is a member of the group with the provided code
	 * @param string $code
	 * @return bool
	 * @deprecated Group codes aren't required and shouldn't be used for validation
	 */
	public function inGroupWithCode($code)
	{
		foreach($this->userGroups() as $group)
		{
			if($group->code() == $code)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns the array of user groups that this user is a member of
	 * @return TMm_UserGroup[]
	 */
	public function userGroups()  : array
	{
		if($this->user_groups === false)
		{
			$this->user_groups = [];
			
			// CHECK FOR USER GROUP MATCHES
			if($this->id() > 0)
			{
				$user_group_rows = TC_Memcached::getArrayValueOrRun($this->cacheKeyName().':user_group_rows',
					function() {
						$query = "SELECT g.* FROM `user_group_matches` m INNER JOIN `user_groups` g USING(group_id) WHERE m.user_id = :user_id";
						$result = $this->DB_Prep_Exec($query, array('user_id' => $this->id()));
						return $result->fetchAll();
					});
				
				foreach($user_group_rows as $row)
				{
					// Rare possibility that a group was deleted and is no longer a valid user group, but the cache
					// is still there.
					if($group = TMm_UserGroup::init($row))
					{
						$this->user_groups[$group->id()] = $group;
					}
				}
			}
			
		}
		
		return $this->user_groups;
	}
	
	/**
	 * Returns the array of titles for this user's user groups
	 * @return string[]
	 */
	public function userGroupsTitles()
	{
		$titles = array();
		foreach($this->userGroups() as $user_group)
		{
			$titles[$user_group->id()] = $user_group->title();
		}
		return $titles;
	}
	
	/**
	 * Updates a single setting for this user and a provided user group.
	 *
	 * This function coincides with the required function for the TCv_FormItem_CheckboxList class.
	 *
	 * @param TMm_UserGroup $user_group The user group being referenced
	 * @param bool $new_value The new value which indicates if they should be added or removed
	 */
	public function updateGroupSetting($user_group, $new_value)
	{
		$current_value = $this->inGroup($user_group);
		
		// Currently YES, but should be NO
		if($current_value && !$new_value)
		{
			$query = "DELETE FROM user_group_matches WHERE user_id = :user_id AND group_id = :group_id";
			$this->DB_Prep_Exec($query, array('group_id'=> $user_group->id(),':user_id' => $this->id()));
			unset($this->user_groups[$user_group->id()]);
		}
		
		// Currently NO, but should be YES
		elseif(!$current_value && $new_value)
		{
			TMm_UserGroupMatch::createWithValues(['user_id' => $this->id(),'group_id' => $user_group->id()]);
			$this->user_groups[$user_group->id()] = $user_group;
			
		}
	}
	
	/**
	 * Updates the user groups for this User based on an array of provided IDs
	 * @param int[] $ids
	 * @uses TMm_User::updateGroupSetting()
	 */
	public function updateUserGroups($ids)
	{
		/** @var TMm_UserGroupList $user_group_list */
		$user_group_list = TMm_UserGroupList::init();
		foreach($user_group_list->groups() as $user_group)
		{
			$in_group = in_array($user_group->id(), $ids);
			$this->updateGroupSetting($user_group, $in_group);
		}
	}
	
	
	/**
	 * Returns the name of the module folder
	 * @return TSm_Module
	 */
	public function homeModule() : TSm_Module
	{
		foreach($this->userGroups() as $user_group)
		{
			if($module = $user_group->homeModule())
			{
				return $module;
			}
		}
		
		// Return the login module, IF they can access the login start module
		$module_list = TSm_ModuleList::init();
		$module = $module_list->loginStartModule();
		if($this->hasModuleAccess($module))
		{
			return $module;
		}
		
		// Check for any module that the user has access to
		$available_modules = [];
		foreach($this->userGroups() as $user_group)
		{
			foreach($user_group->permittedModules() as $module)
			{
				return $module;
			}
			
		}
		
		// IF nothing else, return the profile module, which always works
		return TSm_Module::init('profile');
		
		
	}
	
	/**
	 * Returns if the user can view this module. This is a passthrough function for checkUserPermission() on the module.
	 * @param TSm_Module $module
	 * @return bool
	 */
	public function hasModuleAccess(TSm_Module $module) : bool
	{
		return $module->checkUserPermission($this);
	}
	
	/**
	 * Removes all the user groups from this user
	 */
	public function removeAllUserGroups()
	{
		$query = "DELETE FROM user_group_matches WHERE user_id = :user_id";
		$this->DB_Prep_Exec($query, array(':user_id' => $this->id()));
		$this->user_groups = [];
		
	}
	
	/**
	 * Returns the name of the URL Target for this user. It will find the first user group that has
	 * @return string
	 */
	public function homeModuleURLTarget()
	{
		foreach($this->userGroups() as $user_group)
		{
			// Still check for home module. Matches first group found
			if($module = $user_group->homeModule())
			{
				return $user_group->homeURLTargetName();
			}
			
		}
		
		return '';
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	// Baked in functions related to localization which
	// is a submodule that can be installed
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the preferred language as a two-character code
	 * @return string
	 */
	public function preferredLanguage()
	{
		if(isset($this->preferred_language))
		{
			return $this->preferred_language;
		}
		
		return 'en';
	}
	
	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the user can see this user.
	 * @param $user
	 * @return bool
	 */
	public function userCanView($user = false)
	{
		// Get current user and fail if not found
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		// We can always see ourselves
		if($user->id() == $this->id())
		{
			return true;
		}
		
		return parent::userCanView($user);
	}
	
	
	
	/**
	 * Indicates if the provided user can delete this user. This is an override method to ensure System Administrators
	 * can't be deleted.
	 *
	 * @param bool|TMm_User $user
	 * @return bool
	 */
	public function userCanEdit($user = false)
	{
		if(!$user)
		{
			$user = TC_currentUser();
		}
		
		// Let a user edit themselves
		if($user->id() == $this->id())
		{
			return true;
		}
		
		return parent::userCanEdit($user);
	}
	
	/**
	 * Indicates if the provided user can delete this user. This is an override method to ensure System Administrators
	 * can't be deleted.
	 *
	 * @param bool|TMm_User $user
	 * @return bool
	 */
	public function userCanDelete($user = false)
	{
		if($this->inGroupID('1')) // can't delete system admins
		{
			return false;
		}
		
		return parent::userCanDelete($user);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// EMAIL TESTS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sends the account-created email
	 
	 */
	public function sendAccountCreatedEmail()
	{
		/** @var TMv_AccountCreatedEmail $email_view */
		$email_view = TMv_AccountCreatedEmail::init($this);
		$email_view->send();
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// Google Analytics User Properties
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of user properties that should be tracked. These are entirely defined for each site and will
	 * be passed to Google Analytics if the user is logged in. The returned value must be an associative array of
	 * values, each value must be a number or a string.
	 * @return array
	 * @see https://support.google.com/analytics/answer/12370404
	 */
	public function googleAnalyticsUserProperties()
	{
		return [];
	}
	
	
	//////////////////////////////////////////////////////
	//
	// API KEYS
	// Keys are used to access API endpoints in Tungsten
	//
	//////////////////////////////////////////////////////
	
	public static function currentUserCanGenerateAPIKeys()
	{
		// Don't bother if apis are disabled
		if(!TC_getConfig('use_api'))
		{
			return false;
		}
		
		$current_user = TC_currentUser();
		if(!$current_user)
		{
			return false;
		}
		
		// Only admins can generate API keys
		return $current_user->isAdmin();
		
	}
	
	/**
	 * Returns the api key for this user
	 * @return string
	 */
	public function apiKey() : string
	{
		return $this->api_key;
	}
	
	/**
	 * Returns the date when the API key was created
	 * @return string
	 */
	public function apiKeyDate($format = 'Y-m-d H:i') : string
	{
		if(is_null($this->api_key_date))
		{
			return 'Never set';
		}
		return date($format, strtotime($this->api_key_date));
	}
	
	
	/**
	 * Return if this user has API access
	 * @return bool
	 */
	public function hasAPIAccess() : bool
	{
		// Inactive users don't get access
		if(!$this->isActive())
		{
			return false;
		}
		
		// Loop through each user group and check the access
		foreach($this->userGroups() as $user_group)
		{
			if($user_group->apiAccess())
			{
				return true;
			}
		}
		
		return false;
		
	}
	
	/**
	 * Generates a new api-key for this user, which overwrites any previous keys and it is unique in the system.
	 */
	public function generateNewAPIKey() : void
	{
		if(!$this->hasAPIAccess())
		{
			TC_trackProcessError('User requires Tungsten access to generate API Key','tungsten-access','api_key');
			return;
		}
		
		$unique_key = false;
		
		
		while(!$unique_key)
		{
			$key = TCu_Text::generatePassword(7).'-'.TCu_Text::generatePassword(16).'-'.TCu_Text::generatePassword(7);
			$query = "SELECT * FROM `users` WHERE api_key = :key LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('key' => $key));
			if($result->rowCount() == 0)
			{
				$values = [
					'api_key' => $key,
					'api_key_date' => date('Y-m-d H:i:s',strtotime('now'))
				];
				
				$this->updateWithValues($values);
				$unique_key = true;
			}
		}
		
	}
	
	/**
	 * Returns all the API calls that this user ever made
	 */
	public function apiCalls()
	{
		$calls = [];
		$query = "SELECT * FROM `api_calls` WHERE user_id = :user_id ORDER BY date_added DESC";
		$result = $this->DB_Prep_Exec($query,['user_id' => $this->id()]);
		while($row = $result->fetch())
		{
			$calls[] = TSm_APICallHistory::init($row);
		}
		
		return $calls;
	}
	
	/**
	 * returns the number of API calls made by this user
	 * @return int
	 */
	public function numAPICalls()
	{
		return count($this->apiCalls());
	}
	
	/**
	 * Finds a user with the provided API key
	 * @param string $api_key The api key to search for
	 * @return TMm_User|null
	 */
	public static function userWithAPIKey(string $api_key) : ?TMm_User
	{
		$query = "SELECT * FROM `users` WHERE api_key = :key LIMIT 1";
		$result = static::DB_RunQuery($query, array('key' => $api_key));
		if($row = $result->fetch())
		{
			return TMm_User::init($row);
		}
		
		return null;
	}
	
	/**
	 * @return array
	 */
	public function apiValues() : array
	{
		$values = parent::apiValues();
		unset($values['title']); // not needed weird for a user
		
		$values['name'] = $this->fullName();
		$values['first_name'] = $this->firstName();
		$values['last_name'] = $this->lastName();
		$values['email'] = $this->email();
		return $values;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_Searchable
	//
	//////////////////////////////////////////////////////
	
	
	
	
	/**
	 * Returns the fields that should be searched.
	 * @return array[]
	 */
	public static function searchFields() : array
	{
		$fields = [
			['first_name', 'value%'], // first name starts with
			['last_name', 'value%'], // last name starts with
			['CONCAT(first_name, " ", last_name)', 'value%','concat_full_name'], // last name starts with
		];
		
		if(TC_isTungstenView())
		{
			// Add email contains
			$fields[] = ['email', '%value%'];
		}
		
		return $fields;
		
		
	}
	
	/**
	 * The `Where` portion of the query for searching this model. This method should not include the "Where" call
	 * itself, just what comes after it. This method must return an associative array with two indicies, both of
	 * which have arrays within them. The 'where' array return snippets of text to be added in the where clause. The
	 * 'parameters' array is an associative array of parameters to be passed to PDO.
	 * @return array
	 */
	public static function searchQueryWherePortion() : array
	{
		$values = array();
		$values['where'] = array('is_active = 1');
		$values['parameters'] = array();
		return  $values;
	}
	
	/**
	 * The number of items to be limited in the search
	 * @return null|int
	 */
	public static function searchQueryLimit() : ?int
	{
		return null;
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES – TSt_ModelWithHistory
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of related models that are associated with this model for history purposes. This is usually
	 * used to show the complete history of changes to a model, by pulling in history from other related models.
	 * For example, changing the user groups for a TMm_User shows up in the user's history. News categories are
	 * another example.
	 * @return array[]
	 */
	public static function historyRelatedModels() : array
	{
		return [
			[
				'model_name'            => 'TMm_UserGroupMatch',
				'reference_column_name' => 'user_id',
				'heading_display_title'         => 'User group',
				'heading_display_column_id'     => 'group_id',
			],
		];
	}
	
	
	/**
	 * Adjust histories to not return value variable name columns, but instead use them to explain the value.
	 * @param string $column_name
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyChangeAdjustments(string $column_name,
	                                                TSm_ModelHistoryState $current_state,
	                                                ?TSm_ModelHistoryState $previous_state) : ?array
	{
		
		$adjustments = [];
		if($column_name == 'password')
		{
			if($current_state->isUpdate())
			{
				$adjustments['old_value'] = '****';
			}
			else // insert or delete never track these
			{
				return null;
			}
			
			$adjustments['new_value'] = '******';
			
		}
		
		elseif($column_name == 'password_reset_code')
		{
			$adjustments['new_value'] = '******';
			if($current_state->isUpdate())
			{
				$adjustments['old_value'] = '****';
			}
			else // insert or delete never track these
			{
				return null;
			}
			
		}
		
		// Otherwise return "no changes"
		return $adjustments;
	}
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Make sure we clear the user group rows for this model.
	 * @return array
	 * @see TCm_Model::cacheKeyName()
	 */
	protected static function cacheAdditionalClearCacheSuffixes() : array
	{
		return [':user_group_rows'];
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema() + [
				
				'first_name' => [
					'comment'       => 'The first name or given name for the user. If single fields are used, the name is here as well.',
					'type'          => 'varchar(64)',
					'index'         => 'first_name(35)',
					'nullable'      => false,
				],
	
				'last_name' => [
					'comment'       => 'The last name or family name for the user',
					'type'          => 'varchar(64)',
					'index'         => 'last_name(35)',
					'nullable'      => false,
				],
				
				'email' => [
					'comment'       => 'The email address for the user',
					'type'          => 'varchar(128)',
					'index'         => 'email(25)',
					'nullable'      => true, // possible since we can have users deleted but we want to keep the hours
					'validations'   => [
						'required'      => true, // by default emails are required
						'methods'       => [
							[
								'method_name' => 'emailIsAvailable',
								'mode' => 'create',
								//'class_field_name' => 'project_id',
								'parameter_field_names' => ['email'],
								'error_message' => TC_localize('email_duplicate','A user with that email already exists')
							],
							[
								'method_name' => 'emailIsAvailable',
								'mode' => 'update',
								//'class_field_name' => 'user_id',
								'parameter_field_names' => ['email','user_id'],
								'error_message' => TC_localize('email_duplicate','A user with that email already exists')
							]
						]
					],
					
				],
				'password' => [
					'title'         => 'Password hash',
					'type'          => 'varchar(128)',
					'comment'       => 'The password has for this user',
					'nullable'      => false,
				
				],
				'is_developer' => [
					'comment'       => 'Indicates if this user is a developer in the system',
					'type'          => 'bool',
					'nullable'      => false,
				],
				
				'salutation' => [
					'comment'       => 'The salutation for user',
					'type'          => ' varchar(10)',
					'nullable'      => false,
				],
				
				'home_phone' => [
					'comment'       => 'The home phone number for the user',
					'type'          => ' varchar(20)',
					'nullable'      => false,
				],
				
				'cell_phone' => [
					'comment'       => 'The cell/mobile phone number for the user',
					'type'          => ' varchar(20)',
					'nullable'      => false,
				],
				
				
				'work_phone' => [
					'comment'       => 'The work phone number for the user',
					'type'          => ' varchar(20)',
					'nullable'      => false,
				],
				
				
				'photo_filename' => [
					'comment'       => 'The filename for the photo for this user',
					'type'          => ' varchar(120)',
					'nullable'      => false,
				],
				
				// If email validation is used, is_active defaults to false. Otherwise it's true
				'is_active' => [
					'title'         => 'Active User',
					'comment'       => 'Indicates if the user has an active account in the system',
					'type'          => 'bool default '
						.(TC_getConfig('use_email_verification') ? '0' : '1'),
					'index'         => 'is_active',
					'nullable'      => false,
				],
				
				'password_reset_code' => [
					'comment'       => 'The reset code to be used if the user wants to reset their password',
					'type'          => 'varchar(30)',
					'nullable'      => false,
				],

				'failed_login_attempts' => [
					'comment'		=> 'Tracks the number of failed login attempts',
					'type'			=> 'smallint',
					'nullable'		=> false,
				],
				
				'error_reporting' => [
					'comment'       => 'Indicates if this user shows errors when logged in',
					'type'          => 'bool default 0',
					'nullable'      => true,
				],
				
				// API SETTINGS
				'api_key' => [
					'title'         => 'API Key',
					'comment'       => 'The API for this user to access the API',
					'type'          => 'varchar(32)',
					'nullable'      => true,
				],
				
				'api_key_date' => [
					'title'         => 'API Key Date',
					'comment'       => 'The date the API key was created',
					'type'          => 'datetime',
					'nullable'      => true,
				],
				
				// LOCALIZATION
				'preferred_language' => [
					'title'         => 'Default language',
					'comment'       => 'The default language for this user',
					'type'          => 'varchar(8) DEFAULT "en"',
					'nullable'      => false,
				],
				
				'verify_email_code' => [
					'comment'       => 'The code used to verify an email account.',
					'type'          => 'varchar(30)',
					'nullable'      => true,
				],


				'table_keys' => [
					// Index for when we reference ID and class_name, which happens a lot
					'last_first_name' => ['index' => "`last_name`,`first_name`", 'type' => 'normal'],
				],



			];
	}
	
	
	/**
	 * Sets the ID on the database to this user's ID
	 */
	public function setAsDatabaseSessionID() : void
	{
		$this->DB_Prep_Exec('SET @session_user_id = '.$this->id());
	}
}
