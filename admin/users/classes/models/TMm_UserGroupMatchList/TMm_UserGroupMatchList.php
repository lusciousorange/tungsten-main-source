<?php

	/**
	 * Class TMm_UserGroupMatchList
	 */
class TMm_UserGroupMatchList extends TCm_ModelList
{

	/**
	 * TMm_UserGroupMatchList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = false)
	{
		parent::__construct('TMm_UserGroupMatch',$init_model_list);
	}

	/**
	 * Returns the list of matches
	 * @return TMm_UserGroupMatch[]
	 */
	public function matches()
	{
		return $this->models();
	}
	
	/**
	 * Returns the list of matches for the provided user ID
	 * @param int $user_id
	 * @return array
	 */
	public function matchesForUser($user_id)
	{
		$result = [];
		foreach ($this->matches() as $match)
		{

			if($match->userId() == $user_id)
			{
				$result[] = $match;
			}
		}
		return $result;
	}
}