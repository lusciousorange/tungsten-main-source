<?php
/**
 * Class TMc_UsersController
 */
class TMc_UsersController extends TSc_ModuleController
{
	/**
	 * TMc_UsersController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

	}


	/**
	 * A method to acquire any user forms that are to be added to the User editor as options.
	 *
	 * This method will find any view that fits the pattern TMv_XYZUserForm and will load that form as an option
	 * when editing a user's profile.
	 *
	 * @deprecated No longer implemented.
	 * @see TSc_ModuleController::defineURLTargetsForUsersModule()
	 *
	 */
	protected function addUserFormURLTargets()
	{
		// Not logged in, get out
		if(!TC_currentUser())
		{
			return;
		}
		
		$targets_to_combine = array();

		/** @var TSm_ModuleList $module_list */
		$module_list = TSm_ModuleList::init();
		foreach($module_list->installedModulesForUser(TC_currentUser()) as $module)
		{
			if($module->folder() != 'users')
			{
				// Find all the views that extend
				// loop through the classes folder
				$folder_path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$module->folder().'/classes/views/';
				if(is_dir($folder_path))
				{
					$view_folders = scandir($folder_path);
					foreach($view_folders as $view_folder_name)
					{
						if($view_folder_name != '.' && $view_folder_name != '..')
						{
							if(is_subclass_of($view_folder_name, 'TMv_UserForm'))
							{

								$title = str_ireplace('TMv_', '', $view_folder_name);
								$title = str_ireplace('UserForm', '', $title);

								if($title == '')
								{
									$title = $view_folder_name;
								}
								// Found a user form in a module that this user can see
								// Create the URL Target
								/** @var TSm_ModuleURLTarget $target */
								$target_name = 'edit-' . strtolower($title);
								$target = TSm_ModuleURLTarget::init($target_name);
								$target->setViewName($view_folder_name);
								$target->setModelName('TMm_User');
								//$target->setTitleUsingModelMethod('title');
								$target->setTitle($title);
								$target->setModelInstanceRequired();
								$target->setParentURLTargetWithName('edit');
								$this->addModuleURLTarget($target);

								$targets_to_combine[] = $target_name;


							}

						}

					}
				}
			}
		}
		
		
		$this->combineSubmenuURLTargetsForEditForm($targets_to_combine);
		
		
	}
	
	/**
	 * A method to acquire any user forms that are to be added to the User editor as options.
	 *
	 * This method will find any view that fits the pattern TMv_XYZUserForm and will load that form as an option
	 * when editing a user's profile.
	 *
	 */
	protected function findModuleControllerUserURLTargets()
	{
		// Not logged in, get out
		if(!TC_currentUser())
		{
			return;
		}
		
		$targets_to_combine = array();
		
		/** @var TSm_ModuleList $module_list */
		$module_list = TSm_ModuleList::init();
		foreach($module_list->installedModulesForUser(TC_currentUser()) as $module)
		{
			if($module->folder() != 'users')
			{
				$folder_path = $_SERVER['DOCUMENT_ROOT'] . '/admin/' . $module->folder() . '/classes/controllers/';
				if(is_dir($folder_path))
				{
					$view_folders = scandir($folder_path);
					foreach($view_folders as $view_folder_name)
					{
						if($view_folder_name != '.' && $view_folder_name != '..')
						{
							if(method_exists($view_folder_name, 'defineURLTargetsForUsersModule'))
							{
								$url_targets = $view_folder_name::defineURLTargetsForUsersModule();
								if(count($url_targets) > 0)
								{
									/** @var TSm_ModuleURLTarget $url_target */
									
									foreach($url_targets as $url_target)
									{
										// Adds the module prefix to avoid conflicts
										$url_target->addNamePrefix($module->folder().'-');
										
										// Add the user view as the parent
										// If it has a parent already, don't bother
										if($url_target->parentURLTargetName() == '')
										{
											$url_target->setParentURLTargetWithName('edit');
										}
										
										// Gets added to the user controller no matter what
										$this->addModuleURLTarget($url_target);
										
										// Right buttons don't appear in the list, clearly being added as part of
										// another interface so this avoids double-dipping where they appear
										if(!$url_target->isRightButton())
										{
											// adds the url target to the list of all of them so they can appear
											$targets_to_combine[$module->folder().'-'.$url_target->name()] = $url_target->name();
											
										}
									}
								}
								
							}
						}
					}
				}
			}
		}
		
		
		// END OF PROCESSING, ADD USER FORMS
		$this->combineSubmenuURLTargetsForEditForm($targets_to_combine);
		
		
	}
	
	
	protected function combineSubmenuURLTargetsForEditForm($targets_to_combine)
	{
		$url_targets = array();
		$url_targets[] = 'edit';
		
		// Deal with API keys which is allhandled via that validation method
		if(TMm_User::currentUserCanGenerateAPIKeys())
		{
			$url_targets[] = 'api-key-form';
		}
		$url_targets = array_merge($url_targets,  $targets_to_combine);
		
		$this->defineSubmenuGroupingWithURLTargets($url_targets);
		
	}
	
	
	
	

	/**
	 * Extensions of the defineURLTargets method to also find user forms.
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets();
		
		// API Key form, must exist before finding module controller
		$target = TSm_ModuleURLTarget::init('api-key-form');
		$target->setModelName('TMm_User');
		$target->setViewName('TMv_UserAPIKeyForm');
		$target->setTitle('API Key');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('edit');
		$target->setValidationClassAndMethod('TMm_User','currentUserCanGenerateAPIKeys');
		$this->addModuleURLTarget($target);
		
		
		
		// Adds User Forms, gets merged into 'edit'
		$this->findModuleControllerUserURLTargets();

		// Test Account Created Email
		$target = TSm_ModuleURLTarget::init('send-account-created-email');
		$target->setModelName('TMm_User');
		$target->setModelActionMethod('sendAccountCreatedEmail');
		$target->setNextURLTarget(null);
		$target->setTitle('Send account created email');
		$target->setModelInstanceRequired();
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init('view-account-created-email');
		$target->setViewName('TMv_AccountCreatedEmail');
		$target->setModelName('TMm_User');
		$target->setTitle('View account created email');
		$target->setModelInstanceRequired();
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init('toggle-disable');
		$target->setModelName('TMm_User');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('toggleDisable');
		$target->setNextURLTarget('list');
		$target->setModelInstanceRequired();
		$target->setValidationClassAndMethod('TMm_User','userCanEdit');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('bulk-user-update-user-group');
		$target->setModelName('TMm_UserList');
		$target->setModelActionMethod('bulkUpdateUserGroup');
		$target->setNextURLTarget(null);
		$target->setTitle('Bulk add user group');
		$target->setPassValuesIntoActionMethodType('POST','new_item_id','target_ids');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('bulk-user-update-active');
		$target->setModelName('TMm_UserList');
		$target->setModelActionMethod('bulkUpdateUserActive');
		$target->setNextURLTarget(null);
		$target->setTitle('Bulk update user active');
		$target->setPassValuesIntoActionMethodType('POST','new_item_id','target_ids');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('verify-email');
		$target->setModelName('TMm_User');
		$target->setModelActionMethod('verifyEmailWithCode');
		$target->setNextURLTarget('/'); // Redirect to the homepage
		$target->setTitle('Validate email');
		$target->setPassValuesIntoActionMethodType('URL','2'); // pass in the 2nd value, which is the code
		$target->setAsSkipsAuthentication(); // needed to load outside of login
		$this->addModuleURLTarget($target);
		
		
		
	}


}
?>