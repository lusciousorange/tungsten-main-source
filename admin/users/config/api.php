<?php

$api = [
	// User details about the account used to access the system
	[	'http_method'   => 'GET',
		'path'          => '/me',
		'model'         => 'current-user',
		'function'      => 'apiValues()',
		'description'   => "Basic info about the connected user "
	],

];
