<?php
	
	/**
	 * AutoLoading function that is called whenever a class is being searched for
	 * @param $class_name
	 * @return bool|void
	 */
	function tungsten_core_autoload($class_name)
	{
		// Only bother for classes that start with TC
		if(substr($class_name,0,2) != 'TC' && $class_name != 'PHPExcel')
		{
			return false;
		}
		
		// We'll need the config to get the core path

		// Shortcut through the TCore. Saves load time
		$TCore_path = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['TC_config']['TCore_path'].'/';
		$TCore_folders = array('System', 'Content', 'Forms', 'Lists', 'Controls', 'Content/Excel' );
		foreach($TCore_folders as $folder)
		{
			$file_path = $TCore_path.$folder.'/'.$class_name.'/'.$class_name.'.php';
			if(file_exists($file_path))
			{
				include_once($file_path);
	
				return true;
			}
		}
		
		// If here, we never found the class. Let's try again, just in case
		$path = findClassPathInFolder($class_name, $TCore_path);	
		if($path)
		{
			include_once($path);
			return true;
		}
		
		
	}
	
	/**
	 * Recursive function that goes through all the folders in the TCore and returns the path to that class
	 * @param $class_name
	 * @param $folder_path
	 * @return false|string
	 */
	function findClassPathInFolder($class_name, $folder_path)
	{
		$path = false;
		
		$directory = dir($folder_path);
		while($file = $directory->read())
		{
			$folder_path_file = $folder_path.$file;
			// only check directories
			if(is_dir($folder_path_file) && $file != '.' && $file != '..')
			{
				$file_path = $folder_path_file.'/'.$class_name.'/'.$class_name.'.php';
				if(file_exists($file_path))
				{
					return $file_path; // get out
				}
				else
				{
					$child_path = findClassPathInFolder($class_name, $folder_path_file.'/');
					if($child_path !== false)
					{
						return $child_path;
					}
				}
			}
		}
		return $path;
	}
	
	// Register the function using SPL
	spl_autoload_register('tungsten_core_autoload');
	
	
?>