<?php

/**
 * Class TCv_ModelList
 *
 * A list that uses a particular class
 */
class TCv_ModelList extends TCv_List
{
	/** @var bool|TCm_Model[] $models  */
	protected $models = false;
	protected $model_class = false;
	
	protected $model_list = false; // [TCm_ModelList] 
	protected $model_list_class_name = false; // [string]
	
	protected $view_model_for_class = false;
	protected $perform_access_validation = true;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_ModelList');
	
	/**
	 * @var bool
	 * Indicates if the rows are links which are clickable and then the method to called on the model for the URL.
	 * This requires the list ot use DIVs
	 * This changes the layout setting to use DIVs and also ensure the the row for model includes the link by default.
	 */
	protected $clickable_rows_method = false;
	/**
	 * TCv_ModelList constructor.
	 */
	public function __construct($id = null)
	{
		if(is_null($id))
		{
			$id = get_called_class();
		}
		parent::__construct($id);
		
		// Disable access validation for Tungsten view. This dramatically speeds up large lists.
		// Automatically applied to admins in Tungsten
		if(TC_isTungstenView() && TC_currentUser() && TC_currentUser()->isAdmin())
		{
			// Check for admin_mode, which is a setting used on sites that use Tungsten to load
			// as a skinned interface for non-admins
			// If the site does not use admin_mode, then it won't be set
			if(isset($_SESSION['admin_mode']))
			{
				// We are in admin mode, so disable the validation
				if($_SESSION['admin_mode'])
				{
					$this->disableAccessValidation();
				}
			}
			else // mode not set, regular tungsten
			{
				$this->disableAccessValidation();
			}
			
			
		}
	}
			
	/**
	 * This method turns off the validation on every model provided. By default this is turned on, which will call
	 * userCanView() on each model. Disabling this validation will automatically show all the models provided. When
	 * using this feature, be sure that each model being provided is always visible to the list viewer.
	 */
	protected function disableAccessValidation()
	{
		$this->perform_access_validation = false;
	}
	/**
	 * This method turns on the validation on every model provided.
	 */
	protected function enableAccessValidation()
	{
		$this->perform_access_validation = true;
	}
	
	
	/**
	 * Returns if this view performs access validation
	 * @return bool
	 */
	public function performAccessValidationEnabled()
	{
		return $this->perform_access_validation;
	}
	
	/**
	 * Returns if access validation is disabled
	 * @return bool
	 * @see TCv_ModelList::disableAccessValidation()
	 */
	public function accessValidationDisabled()
	{
		return !$this->perform_access_validation;
	}
	
	
	/**
	 * Function to set the model class. This must be done from within the constructor of an extended class for
	 * security reasons.
	 *
	 * @param string $model_class_name The name of the class
	 */
	protected function setModelClass($model_class_name)
	{
		$this->model_class = $model_class_name;
		$this->setEmptyMessage('No '.$model_class_name::modelTitlePluralLower().' found');

		// model list class name is not set yet
		if($this->model_list_class_name === false)
		{
			if(class_exists($this->model_class.'List'))
			{
				$this->setModelListClass($this->model_class.'List');
			}
			else
			{
				$this->addConsoleWarning('No Model List class exists. Expecting to instantiate class with name <em>'.$this->model_class.'List</em>. The list must have models manually provided or create the necessary class to provide models to the list.');
				$this->setModelListClass('TCm_ModelList');
			}
		}
		
	}
	
	/**
	 * Function to set the model class. This must be done from within the constructor of an extended class for
	 * security reasons.
	 * @param string $model_list_class_name The name of the class for the model list
	 */
	protected function setModelListClass($model_list_class_name)
	{
		//if( !($model_list_class_name instanceof TCm_Model))
		//{
		//	TC_triggerError('Provided Model List Class name <em>'.$model_list_class_name.'</em> provided must be an instance of TCm_Model.');
		//}
		$this->model_list_class_name = $model_list_class_name;
		
	}
	
	/**
	 * Returns the model class name for this list
	 * @return bool|string
	 */
	public function modelListClass()
	{
		return $this->model_list_class_name;
	}
	

	/**
	 * Returns the model list
	 * @return bool|TCm_ModelList
	 */
	public function modelList()
	{
		if($this->model_class == false)
		{
			TC_triggerError('Missing TMm_Model class for TCv_ModelList in <em>'.get_called_class().'</em>. All lists are connected to a primary model class which can be defined using the TCv_ModelList::setModelClass() function.');
		
		}

		/** @var TCm_ModelList $class_name */
		$class_name = $this->model_list_class_name;

		if($this->model_list === false)
		{
			$this->model_list = $class_name::init($this->model_class);
		}
		
		// failsafe if nothing loaded
		if(!$this->model_list)
		{
			$this->model_list = $class_name::init();
		}
		
		return $this->model_list;
	}
	
	
	/**
	 * Add an array of model to the current list of models
	 * @param TCm_Model[] $models
	 */
	public function addModels($models)
	{
		if($this->models === false)
		{
			$this->models = $models;
		}
		else
		{
			$this->models = array_merge($this->models, $models);
		}
	}
	
	/**Add an model to the current list of models
	 * @param TCm_Model $model
	 */
	public function addModel($model)
	{
		if($this->models === false)
		{
			$this->models = array($model);
		}
		else
		{
			$this->models[] = $model;
		}
	}
	
	/**
	 * Returns the list of models for this list
	 *
	 * @return bool|TCm_Model[]
	 */
	public function models()
	{
		return $this->models;
	}
	
	/**
	 * Returns the number of models for this list
	 *
	 * @return int
	 */
	public function numModels()
	{
		return sizeof($this->models);
	}

	/**
	 * Add an array of model to the current list of models
	 * @param TCm_Model[] $models
	 * @deprecated
	 * @see TCv_ModelList::addModels()
	 */
	public function addModelArray($models)
	{
		$this->addModels($models);
	}

	/**
	 * Adds the models to the list, overriding any existing values
	 * @param TCm_Model[] $models
	 */
	public function setModels($models)
	{
		$this->models = $models;
	}



	//////////////////////////////////////////////////////
	//
	// STANDARDIZED TUNGSTEN BUTTONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns a column for a control button with a list method. Useful shortcut to avoid errors and ensure
	 * consistency.
	 * @param string $list_method_name
	 * @return TCv_ListColumn
	 */
	public function controlButtonColumnWithListMethod($list_method_name)
	{
		$edit_button = new TCv_ListColumn($list_method_name);
		$edit_button->setAlignment('center');
		$edit_button->setContentUsingListMethod($list_method_name);
		$edit_button->setWidthAsPixels(28);
		return $edit_button;
		
	}
	
	/**
	 * A link to a specific Module URL Target
	 * @param TCm_Model $model
	 * @param string $url_target_name
	 * @param bool|string $module_folder
	 * @return TCv_Link|TCv_View
	 */
	public function linkForModuleURLTargetName($model, $url_target_name, $module_folder = false)
	{
		if($module_folder === false) // fixed deprecated issue with old values
		{
			$module = TC_moduleForClass($model->baseClassName());
		}
		else
		{
			$module = TSm_Module::init($module_folder);

		}

		$folder = $module->folder();
		$url_target = $module->URLTargetWithName($url_target_name);
		
		if($url_target_name === false) // failsafe to not return blank links
		{
			$link = new TCv_View();
			$link->setTag('span');
			$this->addConsoleWarning('URL Target with a blank name cannot be found.');
		}
		
		// Return false if the url target fails the user permission test
		elseif($url_target)
		{
			if(!$url_target->userHasAccess())
			{
				$link = new TCv_View();
				$link->setTag('span');
				$this->addConsoleWarning('User does not have access to URL Target with name "'.$url_target_name.'" .');
			}
			else
			{
				//We have a successful link to create
				$link = new TCv_Link();
				$link->setTitle($model::$model_title.' - '. ucwords( str_ireplace('-',' ', $url_target_name) ) );
				
				// If we're on the front end and routing is enabled, show that instead
				if(!TC_isTungstenView() && $url_target->allowsFrontEndRouting())
				{
					$link->setURL('/w/'.$folder.'/'.$url_target_name.'/'.$model->id());
				}
				else
				{
					$link->setURL('/admin/'.$folder.'/do/'.$url_target_name.'/'.$model->id());
				}
				
				
			}
		}
		else
		{
			$link = new TCv_View();
			$link->setTag('span');
			
			$this->addConsoleWarning('URL Target with name "'.$url_target_name.'" was not found.');
		}
		
		return $link;
	}
	
	/**
	 * Returns a default list control button
	 * @param TCm_Model $model The model for the button
	 * @param string  $url_target_name The name of the URL string
	 * @param string $icon_name THe icon name based on Font Awesome
	 * @param bool|string  $module_folder
	 * @return bool|TSv_ModuleURLTargetLink
	 */
	public function listControlButton($model, $url_target_name, $icon_name, $module_folder = false)
	{
		return $this->controlButton($model, $url_target_name, $icon_name, $module_folder);
		
	
	}
	
	/**
	 * Creates a control button for the provided model. This class uses the base model's module folder to allow for
	 * extending the class in a different module.
	 * @param TCm_Model $model
	 * @param string $url_target_name
	 * @param string $icon_name
	 * @param bool|string $module_folder (Optional) Default false.
	 * @return bool|TSv_ModuleURLTargetLink
	 */
	public function controlButton($model, $url_target_name, $icon_name, $module_folder = false)
	{
		if($module_folder === false) // fixed deprecated issue with old values
		{
			$module_folder = $model::moduleForClassName($model->baseClassName())->folder();
		}
		
		$url_target = TC_URLTargetFromModule($module_folder, $url_target_name);
		$url_target->setModel($model);
		
		$link = TSv_ModuleURLTargetLink::init($url_target);
		$link->setIconClassName($icon_name); //setIconClassName
		$link->addClass('list_control_button');
		$link->addOverrideText('');
		
		return $link;
	
	}
	
	/**
	 * Adds the default "edit" icon column which uses the `editIconColumn` list method that is defined. If a model is
	 * defined in a module using the standard loading mechanisms and it has a URL target that is defined as the
	 * primary edit URL target, then this should work without any additional code.
	 *
	 * @see TCv_ModelList::editIconColumn()
	 * @see TSm_ModuleURLTarget::setAsPrimaryModelEditTarget()
	 */
	public function addDefaultEditIconColumn()
	{
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	}
	
	/**
	 * Adds the default "delete" icon column which uses the `deleteIconColumn` list method that is defined. If a
	 * model is defined in a module using the standard loading mechanisms and it has a URL target that is defined as the
	 * primary delete URL target, then this should work without any additional code.
	 *
	 * @see TCv_ModelList::deleteIconColumn()
	 * @see TSm_ModuleURLTarget::setAsPrimaryModelDeleteTarget()
	 */
	public function addDefaultDeleteIconColumn()
	{
		$edit_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($edit_button);
	}
	
	
	/**
	 * The default column for the edit icon. This is calculated based on the primary edit URL target for the provided
	 * model. It returns a list control button with the "pencil" icon
	 * @param TCm_Model $model The model being viewed
	 * @return ?TCv_Link
	 */
	public function editIconColumn(TCm_Model $model)
	{
		// Find the primary URL target for editing these items
		if(TC_isTungstenView())
		{
			$url_target = $model->primaryEditURLTarget();
			
			if($url_target)
			{
				$link = TSv_ModuleURLTargetLink::init($url_target);
				$link->setIconClassName('fa-pencil');
				$link->addClass('list_control_button');
				$link->addOverrideText('');
				return $link;
			}
		}
		elseif(method_exists($model, 'pageEditURLPath'))
		{
			if($model->pageEditURLPath() && $model->userCanEdit())
			{
				$link = new TCv_Link();
				$link->setURL($model->pageEditURLPath());
				$link->setIconClassName('fa-pencil');
				$link->addClass('list_control_button');
				
				return $link;
			}
		}
		
		// Not found, we return an empty string
		return null;
		
	}
	
	/**
	 * The default column for the delete icon. This is calculated based on the primary delete URL target for the
	 * provided model. It returns a list control button with the "trash" icon
	 * @param TCm_Model $model The model being viewed
	 * @return string
	 */
	public function deleteIconColumn(TCm_Model $model)
	{
		// Find the primary URL target for editing these items
		$url_target = $model->primaryDeleteURLTarget();
		
		if($url_target)
		{
			$link = TSv_ModuleURLTargetLink::init($url_target);
			$link->setIconClassName('fa-trash');
			$link->addClass('list_control_button');
			$link->addOverrideText('');
			if($link) // avoid calling on non-links
			{
				$link->useDialog('Are you sure you want to delete this '.$model::$model_title.'?');
			}
			return $link;
		}
		
		// Not found, we return an empty string
		return '';
		
	}
	
	
	/**
     * Creates a control icon that isn't clickable.
     *
     * @param string $icon_code A font-awesome icon code
     * @return TCv_View
     */
	public function controlIcon($icon_code)
    {
    	$icon_container = new TCv_View();
    	$icon_container->setTag('span');
		$icon_container->addClass('list_control_button');
	
		$icon = new TCv_View();
        $icon->setTag('i');
        $icon->addClass($icon_code);
        $icon->addClass('link_icon');
      
        $icon_container->attachView($icon);
        
        return $icon_container;
    }

	/**
	 * A special control button for deleting items
	 *
	 * @param TCm_Model $model
	 * @param string $url_target_name
	 * @param string $icon_name
	 * @param bool|string  $verb
	 * @param bool|string $module_folder
	 * @return bool|TSv_ModuleURLTargetLink
	 */
	public function listControlButton_Confirm($model, $url_target_name, $icon_name, $verb = false, $module_folder = false)
	{
		$link = $this->listControlButton($model, $url_target_name, $icon_name, $module_folder);
		if($link) // avoid calling on non-links
		{
			$link->useDialog('Are you sure you want to '.($verb ? $verb : $url_target_name).' this '.$model::$model_title.'?');
		}
		return $link;
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	//////////////////////////////////////////////////////
	
	public function addContentFlaggingColumn()
	{
		if(TC_getConfig('use_content_flagging'))
		{
			$column = $this->controlButtonColumnWithListMethod('contentFlaggingColumn');
			$this->addTCListColumn($column);
		}
		
	}
	
	/**
	 * The default column for content flagging. This inserts the normal content flagging view that is shown on lists
	 * for a the provided model
	 * @param TCm_Model $model The model being viewed
	 * @return TMv_ContentFlagButton
	 */
	public function contentFlaggingColumn(TCm_Model $model)
	{
		$button = new TMv_ContentFlagButton($model);
		$button->addClass('list_control_button');
		return $button;
	
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the content for a given model. This method can be safely extedned to deal with methods for an entire
	 * row, particularly add CSS classes to the row for styling.
	 * @param TCv_ListColumn $column
	 * @param TCm_Model $model
	 * @return mixed|string
	 */
	public function contentForColumnWithModel($column, $model)
	{
		$content = '';
		$method_name = $column->contentMethod();
		$method_arguments = $column->contentArguments();
		
		// Option for no content is checked, otherwise process the column
		if($column->contentType() != 'none')
		{
			if($column->contentType() == 'model_method')
			{
				$model_class = $model;
			}
			elseif($column->contentType() == 'list_method')
			{
				$model_class = $this;
				array_unshift($method_arguments, $model); // add model as the first one
			}
			
			if($method_name == '')
			{
				TC_triggerError('Content method for TCv_ListColumn <em>"'.$column->id().'"</em> has not been set.');
			}
			
			
			if(method_exists($model_class, $method_name))
			{
				$content = call_user_func_array(array($model_class, $method_name), $method_arguments);
			}
			else
			{
				TC_triggerError('Method does not exist <em>"'.get_class($model_class).':'.$method_name.'"</em>');
			}
		}
		return $content;
	}
	
	/**
	 * Sets the view class that is used for the model. By default this will use a Table Row and operate as expected.
	 * If you provide a different view model name, then that view will be instantiated instead. Call this method also
	 * changes the update target to be the main list container. This ensures that the view being returned, which is
	 * almost never a table row, won't be inserted into an table, which breaks HTML standards.
	 * @param string $class_name
	 */
	protected function setViewClassForModel($class_name)
	{
		$this->view_model_for_class = $class_name;
	}
	
	
	/**
	 * Returns the row for a given model. This method can be safely extedned to deal with methods for an entire row,
	 * particulary add CSS classes to the row for styling.
	 * @param TCm_Model $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		// Deal with a row that is a shard primary model
		// Need to save the model as active so that it runs properly
		if($model instanceof TCm_ModelPrimaryHorizontalShard)
		{
			TC_saveActiveModel($model);
			
		}
		
		
		//If an array is passed in, it will use the indicates of the array to try and match content for the column
		if(is_array($model))
		{
			return $this->rowForArrayValues($model);
		}
		
		// An override class view is set
		if($this->view_model_for_class)
		{
			$class_name = $this->view_model_for_class;
			return $class_name::init($model);
		}
		// always create the row to return
		$row = $this->createRowForID($model->id());
		
		// Deal with clickable rows
		if($this->clickable_rows_method)
		{
			$method = $this->clickable_rows_method;

			// Change the tag to an anchor
			$row->setTag('a');

			// Set the URL to be the URL from the provided method
			$row->setAttribute('href', $model->$method());
		}
		
		foreach($this->columns() as $column)
		{
			$content = $this->contentForColumnWithModel($column, $model);
			$row->setColumnContent($column, $content);
		}
	
		
			
		return $row;
	}
	
	
	
	/**
	 * Process the list items
	 */
	public function processListItems()
	{
		foreach($this->models() as $index => $model)
		{
			// An array of values is passed, not a model
			if(is_array($model))
			{
				$row = $this->rowForModel($model);
				if($row)
				{
					$this->addTCListRow($row);
				}
				
			}
			else
			{
				
				if($this->perform_access_validation)
				{
					// only bother if user can view the model
					if($model->userCanView())
					{
						$row = $this->rowForModel($model);
						if($row)
						{
							$this->addTCListRow($row);
						}
					}
					else // remove it from the conversation
					{
						unset($this->models[$index]);
					}
				}
				else // no validation necessary, show all
				{
					$row = $this->rowForModel($model);
					if($row)
					{
						$this->addTCListRow($row);	
					}
				}
			}
		}



		
	}
		
	/**
	 * Extends the list box functionality to handle if we're using non-row views.
	 * @return TCv_View[]
	 */
	public function listViews()
	{
		if($this->view_model_for_class)
		{
			$views = [];
			
			foreach($this->rows as $row)
			{
				$views[] = $row;
			}
			return $views;
		}
		
		// Not that special case, return what would normally happen
		return parent::listViews();
	}
	
	/**
	 * Returns the column value
	 * @param TCm_Model|TMt_WorkflowItem $model
	 * @return TCv_View|string
	 */
	public function workflowColumn($model)
	{
		// ---- WORKFLOW -----
		if(TC_getConfig('use_workflow') )
		{
			$this->addClass('uses_workflow');
			
			return TMv_WorkflowItemSummaryListColumn::init($model);
			
		}
		
		return null;
	}
	
	/**
	 * A method to include the workflow column if necessary. This is included here for easy automation of Workflow
	 * into other lists.
	 *
	 * This methed is only called once for a list, and is used to pre-load anything related to workflows for this list.
	 *
	 * @uses TMt_WorkflowItem::allWorkflowModelMatches()
	 */
	public function handleWorkflowColumn()
	{
		// Deal with Workflow
		if( TC_isTungstenView() && TC_getConfig('use_workflow'))
		{
			if(class_exists($this->model_class) && TC_classUsesTrait($this->model_class,'TMt_WorkflowItem'))
			{
				// Pre-load all the workflow matches using the static method for the model
				($this->model_class)::allWorkflowModelMatches();
				
				$column = new TCv_ListColumn('workflow');
				$column->setTitle('Workflow');
				$column->setContentUsingListMethod('workflowColumn');
				$column->setWidthAsPixels(200);
				$this->addTCListColumn($column);
			}
		}
	}
	
	/**
	 * @return string
	 */
	public function render()
	{
		$this->handleWorkflowColumn();
		
		
		// default if nothing else set, load all the possible models
		if($this->models === false)
		{
			$this->addModels($this->modelList()->models());	
		}
		
		$this->processListItems();
		
		parent::render();
	}
	
	/**
	 * Activates an option where every row is a clickable link where the URL is called from the provided method.
	 * @param string $method (Optional) Default "pageViewURLPath"
	 */
	public function setClickableRows($method = 'pageViewURLPath')
	{
		$this->setRowTagName('a');
		$this->table->unsetAttribute('cellspacing');
//		$this->table->unsetAttribute('width');
//
		
		$this->clickable_rows_method = $method;
	}
	
	
}
