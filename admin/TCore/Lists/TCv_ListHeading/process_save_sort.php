<?php
	session_start();
	
	$_SESSION[htmlentities($_GET['filter']).'_sort_column'] = htmlentities($_GET['column']); 
	$_SESSION[htmlentities($_GET['filter']).'_sort_order'] = htmlentities($_GET['asc'] ? 'ASC' : 'DESC'); 
	
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit(); 
?>