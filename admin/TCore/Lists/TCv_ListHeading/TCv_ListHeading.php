<?php

/**
 * Class TCv_ListHeading
 */
class TCv_ListHeading extends TCv_ListRow
{
	protected $column_html = array(); // [array] = The list of html for this row. Each index is the ID of a column and the value is the html to be shown in that column.

	/**
	 * TCv_ListHeading constructor.
	 * @param $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		$this->cell_class = 'TCv_ListHeadingCell';
		
		$this->addClassCSSFile('TCv_ListHeading');
		
	}
	
	/**
	 * Override the normal TCv_ListRow method to provide heading html
	 *
	 * @param TCv_ListColumn $column
	 * @return mixed|string
	 */
	public function htmlForColumn($column)
	{
		TC_assertObjectType($column, 'TCv_ListColumn');
		
		// If it has been manually set
		if(isset($this->column_html[$column->id()]))
		{
			return $this->column_html[$column->id()];
		}
		
		$html = '<span>'.$column->title().'</span>';
		
		if($column->hasSorting())
		{
			
			$html .= '<a href="#" class="sort_col_button" data-sort="'.$column->sortColumnName().'"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 320 512"  xml:space="preserve">
<path class="sort_down" d="M41,288h238c21.4,0,32.1,25.9,17,41L177,448c-9.4,9.4-24.6,9.4-33.9,0L24,329C8.9,313.9,19.6,288,41,288
	z"/>
<path class="sort_up" d="M296,183L177,64c-9.4-9.4-24.6-9.4-33.9,0L24,183c-15.1,15.1-4.4,41,17,41h238
	C300.4,224,311.1,198.1,296,183z"/>
</svg></a>';
		}
		
		return $html;
		
	}

	/**
	 * @return string
	 */
	public function html()
	{
		return parent::html();
	}
	
}
?>