<?php
/**
 * Class TCv_ListRow
 *
 * The class to represent a single item in the list of items
 */
class TCv_ListRow extends TCv_View
{
	protected $column_html = array(); // [array] = The list of html for this row. Each index is the ID of a column and the value is the html to be shown in that column.
	protected $list = false; // [TCv_List] = The list that this row is attached to
	protected $cell_class = 'TCv_ListCell'; // [string][TCv_ListCell] = The default class for cells
	
	/** @var bool|TCv_ListColumn[] $columns */
	protected $columns = false;
	
	/**
	 * TCv_ListRow constructor.
	 * @param string $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		$this->setTag('tr');
		
	}

	/**
	 * Adds the columns to the row. This indicates what columns will exist in the row. This is often auto-called by
	 * the TCv_List class
	 * @param TCv_ListColumn[] $columns
	 */
	public function setColumns($columns)
	{
		if($this->columns === false) 
		{
			$this->columns = $columns;
		}
		
	}
	
	/**
	 * Adds a column to the row. This must be called before any other content
	 * @param TCv_ListColumn $column
	 */
	public function addColumn($column)
	{
		if($this->columns === false)
		{
			$this->columns = array();
		}
		
		$this->columns[] = $column;
	}
	
	/**
	 * Sets the content for the column. The provided content can be a TCv_View or straight HTML. Any other content
	 * will produce an error.
	 * @param TCv_ListColumn $column
	 * @param string|TCv_View $content
	 */
	public function setColumnContent($column, $content)
	{
		if(!($column instanceof TCv_ListColumn))
		{
			TC_triggerError('Provided column is of type "'.gettype($column).' ('.@get_class($column).')", should be a <em>TCv_ListColumn</em> object');
		}

		$html = '';
		if($content instanceof TCv_View)
		{
			$html = $content->html(); // render first, pull in other styles
			$this->attachCSSFromView($content);
			$this->attachJSFromView($content);
			$this->attachMetaTagsFromView($content);
		}
		else
		{
			try
			{
				$content = (string)$content;
				$html = $content;
			}
			catch(Exception $e)
			{
				TC_triggerError('Provided content must be a TCv_View class or castable to a string. Provided "'.gettype($content).' ('.@get_class($content).')".');
			}
		}
		
		$this->column_html[$column->id()] = $html;
		
		
		
	}
	
	/**
	 * Sets the value in this row for a particular column.
	 *
	 * @param TCv_ListColumn $column
	 * @param string $html
	 * @deprecated
	 * @see TCv_ListRow::setColumnContent()
	 */
	public function setHtmlForColumn($column, $html)
	{
		$this->setColumnContent($column, $html);
		
	}
	
	/**
	 * Sets the value in this row for a particular column.
	 *
	 * @param TCv_ListColumn $column
	 * @param TCv_view $view
	 * @deprecated
	 * @see TCv_ListRow::setColumnContent()
	 */
	public function setViewForColumn($column, $view)
	{
		$this->setColumnContent($column, $view);
	}
	
	
	/**
	 * Returns the HTML for this column
	 *
	 * @param TCv_ListColumn $column
	 * @return mixed|string
	 */
	public function htmlForColumn($column)
	{
		if(!($column instanceof TCv_ListColumn))
		{
			TC_triggerError('Provided column is of type "'.gettype($column).'", should be a <em>TCv_ListColumn</em> object');
		}
		
		if(isset($this->column_html[$column->id()]))
		{
			return $this->column_html[$column->id()];
		}
		else
		{
			return '';
		}
	}

	public function cellForColumn($column)
	{
		$cell = new $this->cell_class('cell_'.$this->id().'_'.$column->id());
		$cell->addClassArray($column->classes());
		
		// Not using tables
		if($this->tag() == 'tr')
		{
			if(method_exists($column, 'columnSpan') && $column->columnSpan() > 1)
			{
				$cell->setAttribute('colspan', $column->columnSpan());
			}
		}
		else // switch to divs
		{
			$cell->setTag('div');
		}
		$cell->addText($this->htmlForColumn($column));
		
		return $cell;
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		if($this->columns === false)
		{
			$this->addConsoleWarning('Attempting to get html() of a TCv_ListRow without having any columns. It is likely that you did not call TCv_ListRow->setList()');
		}
		
		foreach($this->columns as $column)
		{
			$this->attachView($this->cellForColumn($column));
		}
		
		return parent::html();
	}
	
}
?>