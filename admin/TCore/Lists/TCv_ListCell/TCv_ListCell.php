<?php
/**
 * Class TCv_ListCell
 *
 * The class to represent a single cell in the list of items
 */
class TCv_ListCell extends TCv_View
{
	/**
	 * TCv_ListCell constructor.
	 * @param string $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		$this->setTag('td');
		
	}

	/**
	 * @return string
	 */
	public function html()
	{
		return parent::html();
	}
	
}
?>