<?php


/**
 * Class TCv_List
 *
 * A generalized class for a list of items
 */
class TCv_List extends TCv_View
{
	protected $rows = array(); // [array] = The list of rows 
	protected $columns = array(); // [array] = The list of columns
	protected $show_column_headings = true; // [bool] = Indicate that the column headings should be shown

	/** @var bool|TCv_View $table */
	protected $table = false;

	/** @var bool|TCv_ListHeading $heading */
	protected $heading = false;

	protected $auto_width_column_name = false;
	protected $first_column_name = false;
	
	protected $width_pixels = false; // [int] = The width of this list in pixels
	protected $width_percentage = false;

	protected $use_divs = false; // [bool] = Indicates if divs should be used instead of a list
	protected $row_tag = 'tr';
	protected $empty_message = 'No Items Found'; // [string] = The message that is shown if there are no items in the
	
	protected bool $is_horizontal_scrolling = false;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_List');
	
	
	/**
	 * TCv_List constructor.
	 * @param $id
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TCv_List');
		$this->constructTable();
		
		$this->heading = new TCv_ListHeading('headings_'.$this->id());
		$this->heading->addClass('column_headings');
			
		
		
	}
	
	/**
	 * Configures this view to be a horizontal scrolling view. This must be called before columns are defined. When
	 * set to be horizontal scrolling, it requires EVERY column to have a pixel width and it will allow for scrolling.
	 * Percentage width aren't permitted since they mean nothing. This also disables the check that at least one
	 * column is auto, since that's also not possible
	 * @return void
	 */
	public function setAsHorizontalScrolling() : void
	{
		if(count($this->columns) > 0)
		{
			TC_triggerError('Cannot set a list as horizontal scrolling after columns are defined');
		}
		$this->is_horizontal_scrolling = true;
		$this->addClass('horz_scrolling');
	}
	
	
	
	/**
	 * Activates the option to use DIV tags instead of the normal table setting tags.
	 */
	public function setUseDivs()
	{
		$this->use_divs = true;
		$this->table->setTag('div');
		$this->table->unsetAttribute('cellspacing');
		$this->table->unsetAttribute('width');
	}
	
	/**
	 * Sets the tag name to be used for rows when rendered. This overrides the existing value which is normally `tr`.
	 * Any value other than that will switch the table to use DIVs instead of TABLES and
	 * @param string $tag_name
	 */
	public function setRowTagName(string $tag_name)
	{
		$this->setUseDivs();
		$this->row_tag = $tag_name;
		if($tag_name != 'tr')
		{
			$this->table->setTag('div');
			$this->table->unsetAttribute('cellspacing');
			$this->table->unsetAttribute('width');
			$this->addClass('table_format');
		}
		else
		{
			$this->removeClass('table_format');
		}
	}
	
	
	
	/**
	 * Constructs the table that this view will hold.
	 */
	public function constructTable()
	{
		$this->table = new TCv_View($this->id().'_table');
		$this->table->addClass('TCv_List_table');
		
		if($this->use_divs)
		{
			$this->table->setTag('div');
		}
		else
		{
			$this->table->setTag('table');
			$this->table->setAttribute('cellspacing', '0');
		}
	}
	
	
	
//	public function models()
//	{
//		return $this->models;
//
//	}
//
//	public function numModels()
//	{
//		return sizeof($this->models);
//	}
//
//	public function addModelArray($models)
//	{
//		if($this->models === false)
//		{
//			$this->models = $models;
//		}
//		else
//		{
//			$this->models = array_merge($this->models, $models);
//		}
//	}
//
//	public function addModel($model)
//	{
//		if($this->models === false)
//		{
//			$this->models = array($model);
//		}
//		else
//		{
//			$this->models[] = $model;
//		}
//	}


	/**
	 * @param $width
	 */
	public function setWidthAsPixels($width)
	{
		$this->width_pixels = $width;
		$this->width_percentage = false;
		
		$this->addCSSRuntime($this->id.'_list_width', '#'.$this->id().' { width:'.htmlspecialchars($this->width_pixels).'px;} ');
		
	}

	//////////////////////////////////////////////////////
	//
	// LIST COLUMNS
	//
	//////////////////////////////////////////////////////


	
	/**
	 * Add a column to the list
	 * @param TCv_ListColumn $column
	 */
	public function addTCListColumn($column) : void
	{
		TC_assertObjectType($column, 'TCv_ListColumn');
		
		$column->setList($this);
		
		if($this->first_column_name === false)
		{
			$this->first_column_name = $column->id();	
		}
		
		// Horizontal scrolling works differently, no auto widths, no responsive
		if($this->is_horizontal_scrolling)
		{
			if($column->widthFormat() == 'auto' || $column->widthFormat() == 'percentage')
			{
				TC_triggerError('Columns in horizontal scrolling lists must have EVERY column set with pixel widths. Percentage and auto are not permitted.');
			}
		}
		else // traditional fitting to the window
		{
			
			// first auto column must be visible in condensed view
			if($column->widthFormat() == 'auto' && $this->auto_width_column_name === false)
			{
				$this->auto_width_column_name = $column->id();
			}
			
			if($column->widthFormat() == 'auto')
			{
				$column->addClass('auto_width');
			}
		}
	
			
		$this->columns[$column->id()] = $column;
		
		// attaches views, classes, css, etc. Not actually visible
		
	}
	
	/**
	 * Reorders the columns using the IDs provided. If a column is found with the provided ID, it will be included in
	 * that order. Any column NOT shown will be excluded from the updated order.
	 * @param string[] $order
	 */
	public function reorderColumns($order)
	{
		$new_columns = [];
		
		foreach($order as $column_id)
		{
			if(isset($this->columns[$column_id]))
			{
				$new_columns[$column_id] = $this->columns[$column_id];
			}
		}
		
		$this->columns = $new_columns;
	}
	
	/**
	 * A rearranging method that moves a particular column to the end of the list
	 * @param string $column_name
	 * @return void
	 */
	public function moveColumnWithNameToTheEnd(string $column_name) : void
	{
		if($column = $this->columnWithName($column_name))
		{
			unset($this->columns[$column->id()]);
			$this->columns[$column->id()] = $column;
			
		}
	}
	
	
	
	
	/**
	 * Removes a column with a particular name
	 * @param string $column_name
	 */
	public function removeColumnWithName($column_name) : void
	{
		unset($this->columns[$column_name]);
	}
	
	/**
	 * Returns the array of columns
	 * @return TCv_ListColumn[]
	 */
	public function columns() : array
	{
		return $this->columns;
	}
	
	
	
	/**
	 * Returns the number of columns
	 * @return int
	 */
	public function numColumns() : int
	{
		return sizeof($this->columns);
	}
	
	/**
	 * Returns the column with a given name
	 *
	 * @param string $name
	 * @return TCv_ListColumn
	 */
	public function columnWithName($name)
	{
		return $this->columns[$name];
	}
	

	/**
	 * Sets the list not show the column headings
	 */
	public function hideColumnHeadings() : void
	{
		$this->show_column_headings = false;
	}
	
	/**
	 * Checks that this view has at least one auto width column
	 */
	public function validateAutoWidthColumn() : void
	{
		if(sizeof($this->columns) == 0)
		{
			TC_triggerError('No columns have been defined for the TCv_List <em>'.get_called_class().'</em>.');
		}
		
		if(!$this->is_horizontal_scrolling && $this->auto_width_column_name === false)
		{	
			TC_triggerError('At least one column in each TCv_List must have an "auto" width in class <em>'.get_called_class().'</em>.');
		}
	}
	
	/**
	 * Processes the list of columns to generate the CSS for the list.
	 */
	protected function compileColumnCSS() : void
	{
		foreach($this->columns() as $column)
		{
			$column_css = $column->columnCSSLines();
			
			$css_runtime = 	'#'.$this->id().' .TCv_ListColumn_'.$column->id().' { ';
			$css_runtime .= implode(" ", $column_css);
			$css_runtime .= " }";
			
			// Create a unique CSS runtime for this particular table and column
			$this->addCSSRuntime($this->id().'-'.$column->id(), $css_runtime);
		}
		
		
	}

	//////////////////////////////////////////////////////
	//
	// LIST ROWS
	//
	//////////////////////////////////////////////////////



	/**
	 * Adds a list row
	 * @param TCv_ListRow $row
	 */
	public function addTCListRow($row)
	{
		if($row instanceof TCv_ListRow)
		{
			$row->setColumns($this->columns());
		
			$row->setTag($this->row_tag);
			
			$row->addClass('TCv_ListRow_content');
		}
		
		if($row->id())
		{
			$this->rows[$row->id()] = $row;	
		}
		else
		{
			$this->rows[] = $row;
		}
		
	}
	
	/**
	 * A shortcut method which creates a row that has only one column that spans teh full width. This allows for
	 * custom layouts that quickly add rows with content that is directly added to the row.
	 * @param string $id
	 * @param string|TCv_View $content
	 * @return TCv_ListRow
	 */
	public function createFullWidthRowWithContent($id, $content)
	{
		$row = new TCv_ListRow($id);
			$column = new TCv_ListColumn('full_width');
			$column->setColumnSpan($this->numColumns());
			$row->addColumn($column);
			$row->setColumnContent($column, $content);
		return $row;	
	}
	
	
	/**
	 * Generates a TCv_ListRow for a given ID. This ensures consistent IDs which helps with some ajax functions.
	 * @param string $id
	 * @return TCv_ListRow
	 */
	public function createRowForID($id)
	{
		// Handle rows with no ID, which still need to be unique
		if($id == '' || is_null($id))
		{
			// Adds a second underscore to differentiate from auto-generated ones
			$id = '_'.count($this->rows);
		}
		$row = new TCv_ListRow($this->id().'_row_'.$id);
		$row->setAttribute('data-row-id', $id);
			
		return $row;
	}
	
	/**
	 * Return the array of rows for this list
	 * @return TCv_View[]
	 */
	public function rows()
	{
		return $this->rows;
	}
	
	/**
	 * Returns the row for provided array values. The index of the array value must match the ID of the corresponding
	 * column that is defined.
	 * @param $values
	 * @return TCv_ListRow
	 */
	public function rowForArrayValues($values) : TCv_ListRow
	{
		if(isset($values['id']))
		{
			$row = $this->createRowForID($values['id']);
		}
		else
		{
			$row = new TCv_ListRow(false);
		}
		
		foreach($this->columns() as $column)
		{
			if(isset($values[$column->id()]))
			{
				$row->setColumnContent($column, $values[$column->id()]);
			}
			
		}
		return $row;
	}
	
	//////////////////////////////////////////////////////
	//
	// EMPTY LISTS
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the message if there is an empty list
	 *
	 * @param string $message
	 */
	public function setEmptyMessage($message)
	{
		$this->empty_message = $message;
	}
	
	/**
	 * Creates an empty message row and attaches it to the table. This function will only add the row if the empty
	 * message is not blank.
	 * @return TCv_View
	 */
	public function emptyMessageView()
	{
		// DIVs just want another div
		if($this->use_divs)
		{
			$view = new TCv_View();
			$view->addClass('empty_message');
			$view->addText($this->empty_message);
			return $view;
		}
		
		// TABLE rows need better structure
		else
		{
			$row = new TCv_View();
			$row->setTag('tr');
			
			$td = new TCv_View();
			$td->setTag('td');
			$td->setAttribute('colspan',$this->numColumns());
			$view = new TCv_View();
			$view->addClass('empty_message');
			$view->addText($this->empty_message);
			$td->attachView($view);
			
			$row->attachView($td);
			
			return $row;
		}
		
		
	}

	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////


	/**
	 * This override checks to see that the list has been properly configured prior to adding it to another view
	 * (normally the site itself).
	 */
	public function handleAttachedToView()
	{
		
			
	}
	
	/**
	 * Returns the views for the container
	 *
	 * @return TCv_View[]
	 */
	public function listViews()
	{
		$views = [];
		
		
		// PAGINATION
		
		
		// HEADING
		if($this->show_column_headings)
		{
			$this->heading->setColumns($this->columns());
		
			$table_head = new TCv_View();
			$table_head->addClass('table_head');
			if($this->use_divs)
			{
				$this->heading->setTag('div');
			}
			else //normal table
			{
				$table_head->setTag('thead');
			}
			$table_head->attachView($this->heading);
			$this->table->attachView($table_head);
		}
		
		// LOOP THROUGH EACH ROW
		$table_body = new TCv_View();
		$table_body->addClass('table_body');
		if(!$this->use_divs)
		{
			$table_body->setTag('tbody');
		}
		foreach($this->rows as $row)
		{
			$table_body->attachView($row);
		}
		
		$this->table->attachView($table_body);
		$views[] = $this->table;


		if(count($this->rows) == 0)
		{
			$views[] = $this->emptyMessageView();
		}


		return $views;
	}
	
	
	/**
	 * @return string
	 */
	public function render()
	{
		$this->validateAutoWidthColumn();
		
		foreach($this->columns() as $column)
		{
			$this->attachView($column);
		}	
		
		$this->compileColumnCSS();
		
		$list_container = new TCv_View('TCv_List_container_'.$this->id());
		$list_container->addClass('list_container');
		
		foreach($this->listViews() as $view)
		{
			$list_container->attachView($view);
		}
		
		$this->attachView($list_container);
		
		parent::render();
	}
}
