<?php
/**
 * Class TCv_ListColumn
 *
 * The class to represent a column in a TCv_List
 */
class TCv_ListColumn extends TCv_View
{
	protected $title = ''; // [string] = The title for this menu item
	protected $list = false; // [TCv_List] = The list that this row is attached to
	protected $width = false; // [int] = The width of this column
	protected $width_format = 'auto'; // [string] = The format for the width value. It can be 'auto', 'pixels', or 'percentage'
	protected $alignment = 'left'; // [string] = The alignment of the column
	protected $vertical_alignment = false;//'middle'; // [string] = The alignment of the column
	protected $column_span = 1;
	
	protected $left_padding = null;
	protected $right_padding = null;

	protected $content_type, $content_method, $content_args;
	
	// SORTING
	protected ?string $sort_column = null;
	//protected $sort_type = false; // [string] = The type of sort used. 'mysql' or 'method'
	
	// CONDENSED VIEW
	protected $show_in_condensed_view = false;
	protected $hide_page_width = 600;
	
	
	/**
	 * TCv_ListColumn constructor.
	 * @param $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		
		$this->addClass('TCv_ListColumn_'.htmlspecialchars($id));
	}
	
	/**
	 * Sets the list of columns
	 *
	 * @param TCv_List $list
	 */
	public function setList($list)
	{
		TC_assertObjectType($list, 'TCv_List');
		
		$this->list = $list;
		
	}
	
	/**
	 * Sets the column span to be applied to these columns. This can be used when manually creating rows that don't follow the default layout for lists.
	 *
	 * @param int $span
	 */
	public function setColumnSpan($span)
	{
		$this->column_span = $span;
		
	}
	
	/**
	 * Returns the column span
	 *
	 * @return int
	 */
	public function columnSpan()
	{
		return $this->column_span;
		
	}
	
	/**
	 * Turns off column clipping for this column
	 */
	public function disableColumnClipping()
	{
		$this->addClass('no_clip');
	}

	//////////////////////////////////////////////////////
	//
	// TITLE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns teh title for the column
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Sets the title for this column
	 *
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// SORTING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the sort column which enables the ability to change the order.
	 * @param string|null $sort_column
	 * @return void
	 */
	public function setAsSortable(?string $sort_column) : void
	{
		$this->sort_column = $sort_column;
	}
	
	/**
	 * Returns if this column has sorting
	 * @return bool
	 */
	public function hasSorting() : bool
	{
		return !is_null($this->sort_column);
	}
	
	/**
	 * Returns the name for the sort column
	 * @return string|null
	 */
	public function sortColumnName() : ?string
	{
		return $this->sort_column;
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// ALIGNMENT
	//
	//////////////////////////////////////////////////////

	/**
	 * @param string $alignment left, center or right
	 */
	public function setAlignment($alignment)
	{
		$this->alignment = $alignment;
	}
	
	/**
	 * Returns the alignment
	 * @return string
	 */
	public function alignment()
	{
		return $this->alignment;
	}
	
	/**
	 * Sets the vertical alignment of the column
	 * @param string $alignment The preferred alignment. Top, Middle, or Bottom
	 */
	public function setVerticalAlignment($alignment)
	{
		$this->vertical_alignment = $alignment;
	}

	/**
	 * Returns the vertical alignment
	 * @return bool|string
	 */
	public function verticalAlignment()
	{
		return $this->vertical_alignment;
	}

	//////////////////////////////////////////////////////
	//
	// WIDTHS
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the width of this column to be a set value in pixels.
	 *
	 * @param int $width
	 */
	public function setWidthAsPixels($width)
	{
		$this->width = $width;
		$this->width_format = 'pixels';
	}
	
	/**
	 * Sets the width of this column to be a percentage of the total width.
	 * @param int $width
	 */
	public function setWidthAsPercentage($width)
	{
		$this->width = $width;
		$this->width_format = 'percentage';
	}
	
	/**
	 * Returns the format for the width of this column
	 *
	 * @return string pixels or percentage
	 */
	public function widthFormat()
	{
		return $this->width_format;
	}
	
	/**
	 * Returns the format for the width of this column
	 *
	 * @return bool|int
	 */
	public function width()
	{
		return $this->width;
	}
	
	/**
	 * Sets the padding in pixels for the left of this column
	 * @param int $number
	 * @return void
	 */
	public function setLeftPadding($number)
	{
		$this->left_padding = $number;
	}
	
	/**
	 * Sets the padding in pixels for the right of this column
	 * @param int $number
	 * @return void
	 */
	public function setRightPadding($number)
	{
		$this->right_padding = $number;
	}
	
	//////////////////////////////////////////////////////
	//
	// CSS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the lines for CSS for this column
	 * @return array
	 */
	public function columnCSSLines()
	{
		$column_css = [];
		
		// Add dynamic CSS to handle column width, aligment and responsive hiding
		if($this->alignment())
		{
			$column_css[]  = 'text-align:'.htmlspecialchars($this->alignment()).';';
		}
		
		if($this->verticalAlignment())
		{
			$column_css[]  = 'vertical-align:'.htmlspecialchars($this->verticalAlignment()).';';
		}
		
		if($this->widthFormat() == 'pixels')
		{
			$column_css[]  = 'width:'.htmlspecialchars($this->width()).'px;';
		}
		elseif($this->widthFormat() == 'percentage')
		{
			$column_css[]  = 'width:'.htmlspecialchars($this->width()).'%;';
		}
		
		if(!is_null($this->left_padding))
		{
			$column_css[]  = 'padding-left:'.htmlspecialchars($this->left_padding).'px;';
		}
		
		if(!is_null($this->right_padding))
		{
			$column_css[]  = 'padding-right:'.htmlspecialchars($this->right_padding).'px;';
		}
		
		return $column_css;
	}
	
	//////////////////////////////////////////////////////
	//
	// CONDENSED VIEW
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets this column to be visible in the condensed view. By default, only the "auto" width column is visible.
	 * This is used for screens smaller than 480px when the layout switches to a single column view for small devices.
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function setAsVisibleInCondensedView() : void
	{
	
	}

	/**
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function hideInCondensedView() : void
	{
	}

	/**
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function showInCondensedView() : void
	{
	}
	
	/**
	 * Sets this column to be visible in the condensed/responsive view. By default, only the "auto" width column is
	 * visible. This is used for screens smaller than 600px when the layout switches to a single column view for
	 * small devices.
	 *
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function showInResponsive() : void
	{
	
	}
	
	/**
	 * Sets this column to hide in a responsive view, which begins at 600 pixels. You can manually set when a column
	 * will hide by using the method hideAtPageWidth()
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function hideInResponsive() : void
	{
	
	}
	
	/**
	 * Sets this column to hide at a given page width.
	 *
	 * @param int $page_width
	 * @deprecated Lists now side-scroll. This function does nothing
	 */
	public function showInResponsiveUntilWidth(int $page_width) : void
	{
	}
	
	

	//////////////////////////////////////////////////////
	//
	// CONTENT DEFINITION
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the content for this column by calling a method of the model being shown.
	 *
	 * @param string|bool $method The name of the method in the model to be called
	 * @param mixed ...$additional_parameters Additional parameters which are passed into each call
	 */
	public function setContentUsingModelMethod($method, ...$additional_parameters)
	{
		$this->content_type = 'model_method';
		$this->content_method = str_ireplace('()', '', $method);
	
		$this->content_args = $additional_parameters;

	}
	
	/**
	 * Sets the content for this column by calling a method within the list view.
	 *
	 * @param string|bool $method The name of the list method that is called
	 * @param mixed ...$additional_parameters Additional parameters which are passed into each call
	 */
	public function setContentUsingListMethod($method, ...$additional_parameters)
	{
		$this->content_type = 'list_method';
		$this->content_method = str_ireplace('()', '', $method);
		
		$this->content_args = $additional_parameters;
		
	}
	
	/**
	 * Sets the content for this column as not existing. This must be actively done to avoid false-positives when
	 * developing.
	 */
	public function setAsNoContent()
	{
		$this->content_type = 'none';
		$this->content_method = 'none';
	}
	
	
	/**
	 * Returns the content type
	 *
	 * @return string|bool
	 */
	public function contentType()
	{
		return $this->content_type;
	}

	/**
	 * Returns the content method
	 * @return string|bool
	 */
	public function contentMethod()
	{
		return $this->content_method;

	}
	
	/**
	 * Returns the content arguments
	 * @return string|bool
	 */
	public function contentArguments()
	{
		return $this->content_args;

	}

	/**
	 * @return string
	 */
	public function html()
	{
		return '';
	}
	
}
?>