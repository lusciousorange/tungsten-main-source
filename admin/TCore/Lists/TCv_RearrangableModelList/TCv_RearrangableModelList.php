<?php


/**
 * Class TCv_RearrangableModelList
 *
 * A list view that allows for rearranging via drag and drop
 */
class TCv_RearrangableModelList extends TCv_ModelList
{
	protected $use_rearranging = true;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_RearrangableModelList');
	
	/**
	 * TCv_RearrangableModelList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->addClassJSFile('TCv_RearrangableModelList');
		$this->addClassCSSFile('TCv_RearrangableModelList');
		
		// Load SortableJS
		// @see https://sortablejs.github.io/Sortable/
		
		$this->addJSFile('SortableJS','https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js');
		
		$rearrange = new TCv_ListColumn('rearrange_list_handle');
		$rearrange->setWidthAsPixels(30);
		$rearrange->setTitle('<i class="fas fa-sort-amount-down"></i>');
		$rearrange->setAlignment('center');
		$rearrange->setContentUsingListMethod('rearrangeHandle');
		
		$this->addTCListColumn($rearrange);
		
		$this->addJSClassInitValue('list_view_class_name', get_called_class());
		
		$this->setProcessScriptPath('/admin/TCore/Lists/TCv_RearrangableModelList/ajax_rearrange_list.php');
		$this->setModelListClassMethodName('processRearrangedList');
	}
	
	/**
	 * Extend to detect if we've added sortable columns, which are not allowed in rearrangable lists
	 * @param TCv_ListColumn $column
	 */
	public function addTCListColumn($column) : void
	{
		if($column->hasSorting())
		{
			TC_triggerError('Columns cannot be set to sorting in rearrangable lists. Column: '.$column->id());
		}
		parent::addTCListColumn($column);
	}
	
	
	
	/**
	 * Sets the rearranging to use a model and a method on that model to process the list
	 * @param TCm_Model $model
	 * @param string $method_name
	 */
	protected function setProcessModelAndMethod($model, $method_name)
	{
		$this->addJSClassInitValue('model_name', get_class($model));
		$this->addJSClassInitValue('model_id', $model->id());
		
		$this->setModelListClassMethodName($method_name);
		
	}
	
	/**
	 * Function to set the model list class. This must be done from within the constructor of an extended class for
	 * security reasons.
	 * @param string $class_name
	 */
	protected function setModelListClass($class_name)
	{
		parent::setModelListClass($class_name);
		
		if(!method_exists($this->model_list_class_name, $this->js_init_values['rearrange_method_name']))
		{
			if(!isset($this->js_init_values['model_name']))
			{
				TC_triggerError('Class <em>'.$this->model_list_class_name.'</em> requires a public callable method named <em>'.$this->js_init_values['rearrange_method_name'].'($array)</em> which processes an array of values indicating the the list has been rearranged. The array will container a value called <em>num_items</em> and each item will be a value such as <em>item_#</em>=><em>display_order</em>. ');
			}
		}
	}
	
	/**
	 * Sets the path for the processing script
	 * @param string $path The script that will process the results from the arrangement
	 */
	protected function setProcessScriptPath($path)
	{
		$this->addJSClassInitValue('rearrange_script', $path);
		
	}
	
	/**
	 * Sets the list class method for rearranging. IF not not set, then it will use processRerarrangedList()
	 * @param string $method_name
	 */
	protected function setModelListClassMethodName($method_name)
	{
		$this->addJSClassInitValue('rearrange_method_name', str_ireplace('()', '', $method_name));
		
	}
	
	/**
	 * Returns the rearrange handle view
	 * @param TCm_Model $model A model that could be passed in.
	 * @return TCv_Link
	 */
	public function rearrangeHandle($model)
	{
		$handle = new TCv_Link();
		$handle->setURL('#');
		$handle->addClass('fa-sort');
		$handle->addClass('rearrange_handle');
		$handle->addClass('list_control_button');
		return $handle;
			
	}
	
	
	/**
	 * sets the script as processable with optional parameters
	 * @param string $process_script
	 * @param bool|array $parameters
	 */
	public function setAsRearrangeable($process_script, $parameters = false)
	{
		if(!$parameters)
		{
			$parameters = array();
		}
		
		$this->addJSClassInitValue('rearrange_script',$process_script);
		
		
	}
	
	public function disableRearranging()
	{
		$this->use_rearranging = false;
	}
	
	public function enableRearranging()
	{
		$this->use_rearranging = true;
	}
	
	/**
	 * @return string
	 */
	public function render()
	{
		if($this->use_rearranging && !$this->model_list_class_name)
		{
				TC_triggerError('Class TCv_RearrangableModelList used without setting model list class using the setModelListClass() function.');
		}
		
		// Not using rearranging, remove that column
		if(!$this->use_rearranging)
		{
			$this->removeColumnWithName('rearrange_list_handle');
		}
		
		// Force turn off pagination. Happens if lists are switching modes dynamically
		if(isset($this->pagination))
		{
			$this->pagination = false;
		}
		
		if($this->use_rearranging)
		{
			$this->addClassJSInit('TCv_RearrangableModelList');
			
		}
		parent::render();
	}
}
	
?>