<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 
	
	/* 
		This is the standard approach for filtering lists using ajax
		This function is called whenever the filter changes
		The model takes in those values and returns the array of items that should be shown
		That list gets turned into JSON with the indices being the items to be shown
		The TCv_List javascript takes care of hiding the appropriate rows.
		In order for the hiding to work, the id for each row must be row_<ID>
	*/ 
	
	if( isset($_POST['model_name']) && $_POST['model_name'] != '' )
	{
		$model = ($_POST['model_name'])::init($_POST['model_id']);
		if($model->userCanView())
		{
			$method_name = $_POST['method_name'];
			$model->$method_name($_POST);
		}
	}
	else
	{	
		// Load the view class being used. It will be used to grab the model list class
		/** @var TCv_RearrangableModelList $list_view */
		$list_view = ($_POST['view_class'])::init(false);
		
		// the model list,which requires the function processRearrangedList
		$model_list = ($list_view->modelListClass())::init(false);
		
		// deal with the rearrangement
		if(method_exists($model_list, 'processRearrangedList'))
		{
			$model_list->processRearrangedList($_POST);
		}
		else
		{
			$this->addConsoleError('Rearrangement Not Found');
		}

	}
?>