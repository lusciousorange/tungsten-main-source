class TCv_RearrangableModelList {

	element =null;
	options = {
		model_name: '',
		model_id: '',
		method_name: '',
		list_view_class_name: '',
		rearrange_script	:	false,
		pass_through : {},
		rearrange_method_name : '',
	};

	/**
	 * @param {Element} element
	 * @param {Object} params
	 */
	constructor(element, params) {
		this.element = element;

		// Save config settings
		Object.assign(this.options, params);

		this.configureSortable();
	}

	/**
	 * Configures the list to be sortable
	 */
	configureSortable() {

		// @see https://github.com/SortableJS/Sortable
		this.sortable = new Sortable(this.element.querySelector('tbody'), {
			handle		: '.rearrange_handle',
			draggable	: 'tr',
			animation	: 150,
			direction	: 'vertical',
			onSort		: (e) => {this.sortHandler(e); },

		});

		// Disable clicks on the arrows
		this.element.querySelectorAll('.rearrange_handle').forEach(el => {
			el.addEventListener('click', (e) => {e.preventDefault();} )
		});

	}

	/**
	 * Handles the sort request to the server
	 * @param event
	 */
	sortHandler(event) {
		let rows = this.element.querySelectorAll('.TCv_ListRow_content');

		this.element.classList.add('tungsten_loading');

		let post_data = new FormData();
		post_data.append('num_items', String(rows.length) ) ;
		post_data.append( 'view_class', this.options.list_view_class_name) ;
		post_data.append( 'model_id', this.options.model_id);
		post_data.append( 'model_name', this.options.model_name);
		post_data.append( 'method_name', this.options.rearrange_method_name);


		// Loop through the rows, get their data-row-ids and generate the list with item_1, item_2, etc
		let order = 1;
		rows.forEach(el => {
			post_data.append('item_'+order, el.getAttribute('data-row-id'));
			order++;
		});

		// Attach all the pass_through settings
		for(let index in this.options.pass_through )
		{
			post_data.append(index, this.options.pass_through[index]);
		}

		// Send the request
		fetch(this.options.rearrange_script, {
			method : 'POST',
			body : post_data,
		}).catch((err) => {
			console.log(err)
		}).finally(() => {
			this.element.classList.remove('tungsten_loading');
		});


	}
}