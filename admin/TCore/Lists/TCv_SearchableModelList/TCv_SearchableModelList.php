<?php
class TCv_SearchableModelList extends TCv_ModelList
{
	protected ?int $num_per_page = null;
	protected ?int $total_models = null;
	
	protected array $filters = array();
	protected bool $load_using_async = true;
	protected ?string $filter_method_name = 'processFilterList';
	protected bool $bulk_column_added = false;
	protected array $bulk_filter_fields = [];
	
	protected bool $disable_filtering = false;
	
	protected bool $show_footer_pagination = true;
	
	protected $save_filter_id;
	
	/** @var bool Indicates if filters should be saved on refresh */
	protected bool $save_filters = true;
	
	/** @var array An array of filters that are applied before any other processing. */
	protected array $pre_filters = [];
	
	// GTag product item list
	protected $gtag_list_id = null;
	protected $gtag_list_name = null;
	
	protected ?TCu_Item $filter_model = null;
	
	// Indicates if we load the first list asyncronously
	// By default it loads with the page content at the same time
	protected bool $load_first_async = false;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_SearchableModelList');
	
	/**
	 * TCv_SearchableModelList constructor.
	 * @param null|string $id The ID for this searchable model list. This also sets the filter ID. So if this value
	 * is provided, all the instances of this view will use the same ID for filtering. This is required for times
	 * when the filter values are used for related functions, such as download actions based on the set filters.
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		
		// Filter ID set by URL to avoid different renderings of the same view, mixing filters
		// That has always been an issue
		if(is_null($id))
		{
			$this->setSaveFilterID($this->id().$_SERVER['REQUEST_URI']);
		}
		else
		{
			$this->setSaveFilterID($id);
		}
		
		// Always add the JS file
		$this->addClassJSFile('TCv_SearchableModelList');
		
		$this->updateJSClassInitValues([
										   'bulk_update_fields' => [],
									   
									   ]);
		
		
		$this->models = array(); // force a value to avoid loading all values
		
	}
	
	/**
	 * Sets the ID for saving the filters. These are normally generated automatically set in the constructor, but can
	 * be manually set.
	 * @param $id
	 * @return void
	 */
	public function setSaveFilterID($id)
	{
		$this->save_filter_id = $id;
		$this->updateJSClassInitValues([ 'view_list_id' => $id ]);
		
	}
	
	/**
	 * Set the default sort column and order for this list. This value is used in conjuction with columns that have
	 * sorting to indicate which should work first. That functionality normally pulls the first sortable column, so
	 * this overrides that.
	 * @param string $column_name
	 * @param string $order
	 * @return void
	 */
	public function setSortDefaultColumn(string $column_name, string $order = 'asc') : void
	{
		$this->updateJSClassInitValues([
										   'sort_column' => $column_name,
										   'sort_order' => strtolower($order)
									   ]);
	}
	
	/**
	 * Generates the ORDER BY functionality for a query based on the provided values. This looks for the sort_column
	 * and sort_order values that are given with filtering.
	 * @param array $filter_values
	 * @return string
	 */
	public static function generateQueryOrderStringForFilters(array $filter_values) : string
	{
		$string = '';
		if(isset($filter_values['sort_column'])
			&& !is_null($filter_values['sort_column'])
			&& $filter_values['sort_column'] != 'null')
		{
			// Sorting includes a check for null values, which are always placed at the bottom.
			$string .= " ORDER BY ".
				"ISNULL(".$filter_values['sort_column'].") ASC, ".
				str_ireplace([' ',';'],'',$filter_values['sort_column']);
			
			if(isset($filter_values['sort_column']))
			{
				$string .= ' '. str_ireplace([' ',';'],'',$filter_values['sort_order']);
			}
		}
		
		// Sort by the default if the model class name exists and has a primary sort
		elseif(isset($filter_values['view_list_id'])
			&& isset($_SESSION['searchable_list_values'][$filter_values['view_list_id']]['model_class']))
		{
			$model_class_name = $_SESSION['searchable_list_values'][$filter_values['view_list_id']]['model_class'];
			$primary_sort = $model_class_name::$primary_table_sort;
			if($primary_sort != '')
			{
				$string .= " ORDER BY " . $primary_sort;
			}
		}
		
		
		
		return $string;
	}
	
	
	
	
	/**
	 * Sets the identifier of the element that should be updated. By default the script will update the tbody of the
	 * table that shows results. Settings this value will result in the update using that element instead.
	 * @param string $target_identifier The css identifier that indicates where the updated content should go
	 */
	public function setUpdateTarget($target_identifier)
	{
		$this->updateJSClassInitValues(
			[
				'filter_update_target' => $target_identifier
			]);
		
		$this->forceFirstLoadToUseAsync();
	}
	
	/**
	 * Ensures that the first time this list loads, it uses the async method, rather than trying to load it inline.
	 * The default operation for this view is to NOT do this, but there are scenarios where it needs to happen. Must be called
	 * before the renderer and some scenarios auto-trigger it.
	 * @return void
	 */
	public function forceFirstLoadToUseAsync() : void
	{
		// We don't know where the target is, so we can't load it with the view
		$this->load_first_async = true;
		$this->updateJSClassInitValues(['load_first_async' => true]);
	}
	
	/**
	 * Sets the view class that is used for the model. By default this will use a Table Row and operate as expected. If
	 * you provide a different view model name, then that view will be instantiated instead.
	 * @param string $class_name
	 */
	public function setViewClassForModel($class_name)
	{
		// default target is now the main list view
		$this->setUpdateTarget('#'.$this->id().' .list_container');
		parent::setViewClassForModel($class_name);
	}
	
	/**
	 * Manually turns off the filtering functionality
	 */
	public function disableFilterFunctionality()
	{
		$this->disable_filtering = true;
	}
	
	//////////////////////////////////////////////////////
	//
	// MODEL OVERRIDES
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Function to set the model class. This must be done from within the constructor of an extended class for security reasons.
	 * @param string $model_class_name
	 */
	protected function setModelClass($model_class_name)
	{
		parent::setModelClass($model_class_name);
		$this->updateJSClassInitValues([ 'model_class' => $model_class_name ]);
		
	}
	
	/**
	 * Function to set the model list class. This must be done from within the constructor of an extended class for security reasons.
	 * @param string $model_class_name THe name of the model class that will be used
	 * @param string $filter_method_name (Optional) Default "processFilterList" The name of the filter method that will be used on the model list class.
	 */
	protected function setModelListClass($model_class_name, $filter_method_name = 'processFilterList')
	{
		parent::setModelListClass($model_class_name);
		$this->filter_method_name = $filter_method_name;
		
		if(!method_exists($this->model_list_class_name, $filter_method_name))
		{
			TC_triggerError('Model List Class <em>'.$this->model_list_class_name.'</em> requires a public callable method named '.$filter_method_name.'($filter_values) which processes an array of values provided asynchronously via the TCv_SearchableModelList. The implementation varies for each list of models, so it must be implemented differently for each list.');
			
		}
		
		
		
	}
	
	/**
	 * Function to set the filtering mechanism to use a model instance rather htan the traditional model list. This will
	 * allow the search to use a specific instance and filter using that value. It can also allow a different filter
	 * function than the default.
	 * @param TCm_Model $model
	 * @param string $filter_method_name (Optional) Default "processFilterList". The name of the filter method that will be used on the model list class.
	 */
	protected function setFilterUsingModel($model, $filter_method_name = 'processFilterList')
	{
		$this->filter_model = $model;
		$this->setModelListClass(get_class($model), $filter_method_name);
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FORM FILTERS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Turns on filter saving
	 * @return void
	 */
	public function enableFilterSaving()
	{
		$this->save_filters = true;
	}
	
	/**
	 * Turns on filter saving
	 * @return void
	 */
	public function disableFilterSaving()
	{
		$this->save_filters = false;
	}
	
	/**
	 * Returns if this list uses filter saving
	 * @return bool
	 */
	public function useFilterSaving()
	{
		return $this->save_filters;
	}
	
	
	
	/**
	 * Adds a form item to this searchable list as a filter
	 * @param TCv_FormItem|TCv_FormItem_Group $filter The form item view that is the filter
	 */
	public function addFilterFormItem($filter)
	{
		// Check for presets coming in from URL
		if($filter instanceof TCv_FormItem)
		{
			// Maybe remove these lines
//			if(isset($_GET[$filter->id()]))
//			{
//				$this->saveFilterValue($filter->id(), $_GET[$filter->id()]);
//			}
		}
		
		// Save the filter ID for future processing
		$filter_id = $filter->id();
		
		// non form items, wrap just in case
		if( !( $filter instanceof TCv_FormItem) && !($filter instanceof TCv_FormItem_Group))
		{
			if($filter->id() == '')
			{
				$id = 'id_'.rand(100, 10000);
				$filter->setIDAttribute($id);
				$filter_id = $id;
			}
			
			$container = new TCv_FormItem_HTML('container_'.$filter->id(),'');
			$container->attachView($filter);
			$filter = $container;
		}
		
		
		// Special case for groups which need to actually separate out their items, insert the blank group then
		// reattach them as their own items. This method already deals with that, but it's a weird one
		if(($filter instanceof TCv_FormItem_Group))
		{
			// Pull the views out and remove them from the group
			$views = $filter->formItems();
			$filter->clear();
			
			// Attach the empty group
			$this->filters[$filter->id()] = $filter;
			
			// Attach the children as their own filters
			foreach($views as $filter_view)
			{
				$this->addFilterFormItem($filter_view);
			}
			
			
		}
		else
		{
			$filter->addClass('filter_item');
			$this->filters[$filter_id] = $filter;
			
		}
		
		// If we're saving filters,
		if($this->useFilterSaving())
		{
			
			
			if($filter instanceof TCv_FormItem_Hidden)
			{
				$this->clearSavedFilterValue($filter->id());
				
			}
			
			// properly handle saving default date/time values
			if($filter instanceof TCv_FormItem_SelectDateTime)
			{
				$filter_id = $filter->id();
				
				// if not set, then save
				if(@$_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id] == '')
				{
					$default = $filter->defaultValue();
					$date_and_time = explode(' ', $default);
					$date = @$date_and_time[0];
					$date_values = explode('-', $date);
					$time = @$date_and_time[1];
					$time_values = explode(':', $time);
					
					$this->saveFilterValue($filter_id . '_year', @$date_values[0]);
					$this->saveFilterValue($filter_id . '_month', @$date_values[1]);
					$this->saveFilterValue($filter_id . '_day', @$date_values[2]);
					$this->saveFilterValue($filter_id . '_hour', @$time_values[0]);
					$this->saveFilterValue($filter_id . '_minute', @$time_values[1]);
					$this->saveFilterValue($filter_id . '_second', @$time_values[2]);
				}
			}
		}
		
	}
	
	/**
	 * Returns the list of form items that are filters
	 * @return TCv_FormItem[]
	 */
	public function filters()
	{
		return $this->filters;
	}
	
	/**
	 * Adds a search filter form item. Only one per filter form, so it's a unique form item.
	 */
	public function addSearchFilterFormItem()
	{
		
		$search_box = new TCv_FormItem_TextField('form_filter_search', TC_localize('search','Search'));
		$search_box->setPlaceholderText(TC_localize('search','Search'));
		$search_box->disableAutoComplete();
		
		$icon_container = new TCv_View();
		$icon_container->addClass('search_icon_box');
		
		$search_icon = new TCv_View();
		$search_icon->addClass('fas fa-search');
		$search_icon->addClass('search_icon');
		$icon_container->attachView($search_icon);
		
		$clear_link = new TCv_Link();
		$clear_link->setIconClassName('fa-times');
		$clear_link->addClass('clear_search_link');
		$clear_link->setURL('#');

//		if($this->['use_barcode_scanning'])
//		{
//			$search_box->disableAutoComplete();
//		}
		
		$icon_container->attachView($clear_link);
		$search_box->attachViewAfter($icon_container);
		$this->addFilterFormItem($search_box);
		
	}
	
	/**
	 * Returns if this list uses a filter
	 * @return bool
	 */
	public function usesFilterForm()
	{
		return sizeof($this->filters) > 0;
	}
	
	/**
	 * Returns a value for the specified filter ID
	 * @param string $filter_id
	 * @return string
	 */
	public function filterValue($filter_id)
	{
		if($this->useFilterSaving())
		{
		
		}
		
		$value = @$_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id];
		
		if(isset($this->filters[$filter_id]))
		{
			$filter = $this->filters[$filter_id];
			
			// If the filter id provided is the main select date/time id, then return the full string
			if($filter instanceof TCv_FormItem_SelectDateTime)
			{
				$value = str_pad($this->filterValue($filter->id().'_year'), 4, '0', STR_PAD_LEFT);
				$value .= '-'.str_pad($this->filterValue($filter->id().'_month'), 2, '0', STR_PAD_LEFT);
				$value .= '-'.str_pad($this->filterValue($filter->id().'_day'), 2, '0', STR_PAD_LEFT);
				$value .= ' '.str_pad($this->filterValue($filter->id().'_hour'), 2, '0', STR_PAD_LEFT);
				$value .= ' '.str_pad($this->filterValue($filter->id().'_minute'), 2, '0', STR_PAD_LEFT);
				$value .= ' '.str_pad($this->filterValue($filter->id().'_second'), 2, '0', STR_PAD_LEFT);
				
			}
			
		}
		return $value;
	}
	
	/**
	 * Clears the saved value for a given
	 * @param string $filter_id
	 */
	public function clearSavedFilterValue($filter_id)
	{
		unset($_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id]);
		unset($_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id.'_year']);
		unset($_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id.'_month']);
		unset($_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id.'_day']);
		
		
		
	}
	
	/**
	 * Saves a value for a given filter ID
	 * @param string $filter_id
	 * @param string $value
	 */
	public function saveFilterValue($filter_id, $value)
	{
		if($this->useFilterSaving())
		{
			@$_SESSION['searchable_list_values'][$this->save_filter_id][$filter_id] = $value;
		}
	}
	
	/**
	 * Clears all the saved filter values
	 */
	public function clearSavedFilterValues()
	{
		unset($_SESSION['searchable_list_values'][$this->save_filter_id]);
	}
	
	
	public function processUrlSectionFilters()
	{
		$this_url = $_SERVER['REQUEST_URI'];
		
		if(TC_isTungstenView())
		{
			// All these URLs start with /admin/<module>/do/<url_target/
			// So we can trim the slashes, explode and  skip the first 4
			
			$num_to_trim = 4;
			$current_url_target = TC_currentURLTarget();
			if($current_url_target instanceof TSm_ModuleURLTarget)
			{
				// If an instance is required, we need to go one step further
				if($current_url_target->modelInstanceRequired())
				{
					$num_to_trim++;
				}
			}
			
			// Remove any trailing slashes since they get in the way
			$this_url = trim($this_url, '/');
			
			$parts = explode("/",$this_url);
			$parts = array_slice($parts, $num_to_trim);
		}
		else
		{
			$current_page = TC_activeModelWithClassName('TMm_PagesMenuItem');
			if($current_page instanceof TMm_PagesMenuItem)
			{
				// Remove the path that we know about
				$this_url = str_replace($current_page->pathToFolder(), '', $this_url);
			}
			
			// Remove any trailing slashes since they get in the way
			$this_url = trim($this_url, '/');
			
			// Generate the parts array
			$parts = explode("/",$this_url);
			
			// If the page requires a model ID, shift one more
			if($current_page && $current_page->requiresActiveModel())
			{
				array_shift($parts);
			}
			
		}
		
		// Generate the filters based on the parts
		$filters = [];
		for($index = 0; $index < count($parts); $index = $index+2)
		{
			if(isset($parts[$index]) && isset($parts[$index+1]))
			{
				if(!isset($this->pre_filters[$parts[$index]]))
				{
					$this->setPreFilter($parts[$index], $parts[$index+1]);
				}
			}
		}
	}
	
	/**
	 * Sets a pre-filter on this list, which must match up with one of the filters on the list. Setting this value
	 * will remove that filter from the visible area and hard-code in that restriction.
	 * @param string $name The name of the filter to be set
	 * @param string $value The value to be set
	 * @param bool $update_title_suffix Indicates if the title suffix of the page should be updated
	 * @return void
	 */
	public function setPreFilter($name, $value, $update_title_suffix = true)
	{
		$this->pre_filters[$name] = ['value' => $value, 'update_title_suffix' => $update_title_suffix];
	}
	
	/**
	 * Returns the form for the filter
	 * @return TCv_Form
	 */
	public function filterForm()
	{
		
		$filter_form = new TCv_Form('filter_form_'.$this->id());
		$filter_form->addClass('filter_form');
		$filter_form->setAction('#');
		$filter_form->setUseDivs();
		$filter_form->hideSubmitButton();
		$filter_form->addClass('skip_tungsten_loading');
		
		//$filter_form->addClass('num_filters_'.sizeof($this->filters));
//
//		$icon = new TCv_View('filter_icon');
//		$icon->setTag('i');
//		$icon->addClass('fa-filter');
//		$icon->addClass('fa-fw');
//
//		$filter_form->attachView($icon);
		
		// FIND PRE-SET URL FILTERS
		$this->processUrlSectionFilters();
		
		$filter_group = new TCv_FormItem_Group('filter_group');
		$num_in_group = 0;
		foreach($this->filters as $index => $filter)
		{
			// Detect if a URL filter is set
			// If so, generate a hidden field with that value and get out
			if(isset($this->pre_filters[$filter->id()]))
			{
				// Save the title
				$this->pre_filters[$filter->id()]['field_title'] = $filter->title();
				
				// Calculate the field value
				$selected_filter_value = $this->pre_filters[$filter->id()]['value'];
				$filter_field_value = $selected_filter_value;
				if($filter instanceof TCv_FormItem_Select || $filter instanceof TCv_FormItem_SearchSelect)
				{
					$filter_field_value = $filter->optionTitle($selected_filter_value);
				}
				$this->pre_filters[$filter->id()]['field_value'] = $filter_field_value;
				
				// Replace the filter with a hidden filter
				$hidden_filter = new TCv_FormItem_Hidden($filter->id(),'');
				$hidden_filter->setValue($selected_filter_value);
				$hidden_filter->addClass('filter_item');
				
				// Manually set the saved value to override anything that is set
				if($this->useFilterSaving())
				{
					$_SESSION['searchable_list_values'][$this->save_filter_id][$filter->id()] = $selected_filter_value;
				}
				
				
				// Update the viewed page title
				if($this->pre_filters[$filter->id()]['update_title_suffix'])
				{
					$current_page = TC_activeModelWithClassName('TMm_PagesMenuItem');
					if($current_page)
					{
						$current_page->addTitleSuffix(
							'<span class="prefilter"><span class="prefilter_title">' .$filter->title() . '</span>'
							.'<span class="prefilter_value">' .$this->pre_filters[$filter->id()]['field_value'] . '</span></span>');
						
						
						
					}
				}
				
				// Update the filter value to be used in this script
				$filter = $hidden_filter;
				$this->filters[$index] = $filter;
				
				// Don't do anything else
				//continue;
			}
			
			// Test for a new group
			if($filter instanceof TCv_FormItem_Group)
			{
				// Add the count to the current group and attach
				$filter_group->addClass('num_filters_'.$num_in_group);
				$filter_form->attachView($filter_group);
				
				// Start using the new group
				$filter_group = $filter;
				$num_in_group = 0; // reset the count
				
			}
			elseif($filter instanceof TCv_FormItem )
			{
				// If we're saving, then we need to set the saved value
				if($this->useFilterSaving())
				{
					if($filter instanceof TCv_FormItem_SelectDateTime)
					{
						$saved_value = str_pad($this->filterValue($filter->id() . '_year'), 4, '0', STR_PAD_LEFT);
						$saved_value .= '-' . str_pad($this->filterValue($filter->id() . '_month'), 2, '0', STR_PAD_LEFT);
						$saved_value .= '-' . str_pad($this->filterValue($filter->id() . '_day'), 2, '0', STR_PAD_LEFT);
						$saved_value .= ' ' . str_pad($this->filterValue($filter->id() . '_hour'), 2, '0', STR_PAD_LEFT);
						$saved_value .= ' ' . str_pad($this->filterValue($filter->id() . '_minute'), 2, '0', STR_PAD_LEFT);
						$saved_value .= ' ' . str_pad($this->filterValue($filter->id() . '_second'), 2, '0', STR_PAD_LEFT);
					}
					else
					{
						$saved_value = $this->filterValue($filter->id());
					}
					
					if($saved_value != '' && !is_null($saved_value))
					{
						$filter->setSavedValue($saved_value);
					}
					
					// If the session value isn't set, we'll need it possible for static initial load
					// We use the filter saved value, since it's possible that it's being set on creation
					if(!isset($_SESSION['searchable_list_values'][$this->save_filter_id][$filter->id()]))
					{
						$this_saved_value = $filter->savedValue();
						if(is_null($this_saved_value))
						{
							$this_saved_value = $filter->defaultValue();
						}
						$_SESSION['searchable_list_values'][$this->save_filter_id][$filter->id()] = $this_saved_value;
						
					}
					
					
				}
				
				// Attach them all
				$filter_group->attachView($filter);
				
				// Only up the count if we can see them
				if( !($filter instanceof TCv_FormItem_Hidden))
				{
					$num_in_group++;
				}
				
			}
			
			
			
		}
		
		$filter_group->addClass('num_filters_'.$num_in_group); // add the filter count
		$filter_form->attachView($filter_group);
		
		return $filter_form;
		
	}
	
	
	/**
	 * Function that is called which sets up the list of filters used. By default,it includes the search field and nothing else.
	 *
	 * This method can be overridden to define other filters
	 */
	public function defineFilters()
	{
		$this->addSearchFilterFormItem();
		
		
	}
	
	/**
	 * Returns the values associated with the filters for this item
	 * @return array
	 */
	public function filterValues()
	{
		$values = array();
		foreach($this->filters() as $filter)
		{
			$values[$filter->id()] = $this->filterValue($filter->id());
		}
		
		if(isset($values['form_filter_search']))
		{
			$values['search'] = $values['form_filter_search'];
		}
		
		return $values;
	}
	
	//////////////////////////////////////////////////////
	//
	// PAGINATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the number of items per page
	 * @param ?int $num_per_page The number on each page. If set to null, then it will show all the values
	 */
	protected function setPagination(?int $num_per_page) : void
	{
		$this->num_per_page = $num_per_page;
		
	}
	
	
	/**
	 * Creates the pagination controls and returns the view to represent them.
	 * @param int $current_page
	 * @return TCv_View
	 */
	public function paginationControls(int $current_page)
	{
		// pagination is not set, skip
		if(!$this->num_per_page)
		{
			return new TCv_View();
			
		}
		
		// CREATE CONTROLS
		$controls = new TCv_View();
		$controls->addClass('pagination_controls');
		
		$total_num_pages = ceil($this->total_models/$this->num_per_page);
		
		$class_name = $this->model_class;
		/** @var TCm_Model $class_name */
		if(class_exists($class_name))
		{
			$title = $class_name::modelTitlePlural();
			if($this->total_models == 1)
			{
				$title = $class_name::$model_title;
			}
			
		}
		else
		{
			$title = TC_localize('Items', "Items");
			if($this->total_models == 1)
			{
				$title = TC_localize('Item', 'Item');
			}
			
			
		}
		
		
		$item_count = new TCv_View();
		$item_count->addClass('item_count_box');
		$item_count->addText($this->total_models.' '.$title);
		$controls->attachView($item_count);
		
		
		$item_count_container = new TCv_View();
		$item_count_container->addClass('item_count_container');
		
		
		
		$page_num_start = 1;
		$page_num_stop = $total_num_pages;
		if($total_num_pages > 9)
		{
			if($current_page <= 4)
			{
				$page_num_stop = 9;
			}
			elseif($current_page >= $total_num_pages - 3)
			{
				$page_num_start = $total_num_pages - 8;
				$page_num_stop = $total_num_pages;
				
			}
			else
			{
				$page_num_start = $current_page - 4;
				$page_num_stop = $current_page + 4;
				
			}
		}
		
		
		if($current_page > 1)
		{
			if($total_num_pages > 9 && $page_num_start > 1)
			{
				
				$page_link = new TCv_Link();
				$page_link->addClass('page_link');
				$page_link->addText('1');
				$page_link->setURL('#');
				$page_link->addDataValue('target',1);
				$item_count_container->attachView($page_link);
			}
			
			$page_link = new TCv_Link();
			$page_link->addClass('page_link');
			$page_link->setIconClassName('fa fa-angle-left ');
			$page_link->setURL('#');
			$page_link->addDataValue('target',$current_page-1);
			$item_count_container->attachView($page_link);
		}
		
		for($page_num = $page_num_start; $page_num <= $page_num_stop; $page_num++)
		{
			$page_link = new TCv_Link();
			$page_link->addClass('page_link');
			$page_link->addClass('arrow previous');
			$page_link->addText($page_num);
			$page_link->setURL('#');
			$page_link->setTitle('Page '.$page_num);
			$page_link->addDataValue('target',$page_num);
			if($page_num == $current_page)
			{
				$page_link->addClass('current_page');
			}
			
			$item_count_container->attachView($page_link);
		}
		
		if($current_page < $total_num_pages)
		{
			$page_link = new TCv_Link();
			$page_link->addClass('page_link');
			$page_link->setIconClassName('fa fa-angle-right');
			$page_link->addClass('arrow next');
			
			$page_link->setURL('#');
			$page_link->addDataValue('target',$current_page+1);
			$item_count_container->attachView($page_link);
			
			// Very last one, only if there are a lot
			if($total_num_pages > 9 && $page_num_stop < $total_num_pages)
			{
				$page_link = new TCv_Link();
				$page_link->addClass('page_link');
				$page_link->addText($total_num_pages);
				$page_link->setURL('#');
				$page_link->addDataValue('target',$total_num_pages);
				$item_count_container->attachView($page_link);
			}
			
		}
		
		$controls->attachView($item_count_container);
		
		return $controls;
		
	}
	
	/**
	 * Hides the footer pagination if used
	 */
	public function hideFooterPagination()
	{
		$this->show_footer_pagination = false;
	}
	
	/**
	 * Determines the correct model list value, given a set of list session values. The searchable
	 * model list can generate from various model lists and methods. This will often be a TCm_ModelList,
	 * however if we're loading from another model, then the model and method could be different.
	 *
	 * This is a static method, because the value for this value is sometimes passed in as a parameter for lists
	 * @param array $list_session_values
	 * @return TCm_Model
	 */
	public static function modelListForSessionValues(array $list_session_values) : TCm_Model
	{
		if($list_session_values['model_id'] != '')
		{
			$model_id = htmlentities($list_session_values['model_id']);
			return ($list_session_values['model_list_class'])::init($model_id); // purposely don't load models
		}
		else
		{
			return ($list_session_values['model_list_class'])::init(false); // purposely don't load models
		}
	}
	
	/**
	 * Processes the values provided via the ajax searching script and returns the values to be returned
	 * @param array $params
	 *
	 * @return array
	 */
	public function processFilterValues(array $params) : array
	{
		$model_list = static::modelListForSessionValues($params);
		
		// Deal with form_filter_search
		// This was a rewrite and lots of time we use `search`, so we copy it over
		// The filter can't fight against other search on a page, so we use a different one
		if(isset($params['form_filter_search']))
		{
			$params['search'] = $params['form_filter_search'];
		}
		
		$this->handleWorkflowColumn(); // calls the workflow handler to include the column if necessary
		//	$this->addConsoleDebug('processFilterValues');
		//	$this->addConsoleDebug($params);
		$filter_method = $_SESSION['searchable_list_values'][$this->save_filter_id]['filter_method_name'];
		$view_class_for_model = $_SESSION['searchable_list_values'][$this->save_filter_id]['view_class_for_model'];
		
		// Set the view_model_for_class
		if($view_class_for_model != '')
		{
			$this->setViewClassForModel($view_class_for_model);
		}
		
		// Process for Date/Time
		foreach($this->filters() as $filter)
		{
			if($filter instanceof TCv_FormItem_SelectDateTime)
			{
				$select_date_time_value = str_pad(@$params[$filter->id().'_year'], 4, '0', STR_PAD_LEFT);
				$select_date_time_value .= '-'.str_pad(@$params[$filter->id().'_month'], 2, '0', STR_PAD_LEFT);
				$select_date_time_value .= '-'.str_pad(@$params[$filter->id().'_day'], 2, '0', STR_PAD_LEFT);
				$select_date_time_value .= ' '.str_pad(@$params[$filter->id().'_hour'], 2, '0', STR_PAD_LEFT);
				$select_date_time_value .= ' '.str_pad(@$params[$filter->id().'_minute'], 2, '0', STR_PAD_LEFT);
				$select_date_time_value .= ' '.str_pad(@$params[$filter->id().'_second'], 2, '0', STR_PAD_LEFT);
				
				$_GET[$filter->id()] = $select_date_time_value;
			}
			
		}
		
		// Process the filters with the provided POST VALUES
		$values = $model_list->$filter_method($params);
		
		// NEW METHOD where we pass the query and db values, but don't run it
		// this lets us append limits, to perform more efficient queries
		if(is_array($values) && isset($values['db_query']) && isset($values['db_params']))
		{
			$query = $values['db_query'];
			
			// Detect if we have multiple queries and they should be unioned
			if(is_array($query))
			{
				$query = implode(' UNION ', $query);
			}
			if(!str_contains(strtoupper($query), 'ORDER BY'))
			{
				$query .= static::generateQueryOrderStringForFilters($params);
			}
			
			// Ensure the value is passed through
			$params['num_per_page'] = $this->num_per_page;
			
			// Runs the searchable one so it's extendable
			if(is_null($this->total_models))
			{
				$this->total_models = $this->modelCount($model_list, $values);
			}
			$values['models'] = $model_list->getModelsForFilteredQuery($this->model_class,
																	   $query,
																	   $values['db_params'],
																	   $params);
			// Pass through for calculations
			$values['page_num'] = $params['page_num'];
		}
		else
		{
			$values = $this->postFilterObjects($values, $params);
			
			// OLD WAY required the returnValueForFilterResult to be called internally. Awkward
			// NEW WAY calls it here and just accepts a result which can be an array of object or a PDOStatement. It won't care.
			if($values instanceof PDOStatement || !isset($values['num_results']))
			{
				// Pass in the same filter values
				$this->addConsoleWarning('<code style="font-family: monospace;">'.get_class($model_list).'->'.$params['filter_method_name'].'()</code> uses the older return value process for filtering. Consider upgrading.');
				$values = $this->returnValueForFilterResult($params, $values);
			}
			$this->total_models = $values['num_results'];
		}
		
		
		$return_values = array();
		$return_values['list_html'] = '';
		$return_values['list_css'] = '';
		$return_values['pagination_html'] = '';
		
		// save values to the session variable
		// must happen before the view class is initialized to ensure it pulls the correct values if necessary
		if($this->useFilterSaving())
		{
			// Individually add the values to avoid wiping existing ones
			foreach($params as $name => $value)
			{
				$_SESSION['searchable_list_values'][$params['view_list_id']][$name] = $value;
			}
			
		}
		
		// Create a view list, add the model, process the items
		$this->addModels($values['models']);
		$this->processListItems();
		
		$collector_view = new TCv_View();
		
		// RETURN
		foreach($this->rows() as $row)
		{
			$return_values['list_html'] .= $row->html();
			$collector_view->attachCSSFromView($row);
		}
		
		$return_values['list_css'] = $collector_view->headCSSTags();
		
		$return_values['pagination_html'] = $this->paginationControls($values['page_num'])->html();
		
		
		// Deal with Google Analytics
		if(!is_null($this->gtag_list_id ))
		{
			$values = $this->generateGTagForModels();
			$return_values['list_html'] .= TSv_GoogleAnalytics::trackEvent('view_item_list',$values, true);
			
		}
		
		return $return_values;
	}
	
	
	
	/**
	 * Passthrough method to calculate the number of models. Extendable for specific cases if necessary
	 * @param TCm_ModelList $model_list
	 * @param array $filter_values The list of filter values that should be used for this count
	 * @return int
	 */
	public function modelCount($model_list, array $filter_values = []) : int
	{
		if($model_list instanceof TCm_ModelList)
		{
			return $model_list->modelCount($filter_values);
		}
		else // use the process functionality
		{
			return TCm_ModelList::modelCountForClassWithFilters($model_list, $filter_values);
		}
		return -1;
	}
	
	
	/**
	 * A function that can be called by extended classes to additionally filter the list after the first setting.
	 * This method accepts an array of models and must return an array of models
	 * @param TCm_Model[] $objects
	 * @param array $params The list of parameters that are also passed into the filter function
	 * @return TCm_Model[]
	 */
	protected function postFilterObjects($objects, $params)
	{
		return $objects;
	}
	
	
	/**
	 * Returns a pre-formatted set of results for the provided filter values for a TCv_SearchableModelList.
	 *
	 * @param array $filter_values The provided filter values
	 * @param array|PDOStatement $result The results for the filter which can be an array of strings or TCm_Models or
	 * it can be a `PDOStatement` with results.
	 * @return array A specifically formatted array that is used
	 */
	public function returnValueForFilterResult($filter_values, $result) : array
	{
		// Cast to int to avoid any calc errors in PHP 8.1
		$filter_values['page_num'] = (int)$filter_values['page_num'];
		$filter_values['num_per_page'] = (int)$_SESSION['searchable_list_values'][$this->save_filter_id]['num_per_page'];
		$model_class_name = $_SESSION['searchable_list_values'][$this->save_filter_id]['model_class'];
		$return_values = array(); // init the return values
		$return_values['num_results'] = 0; // filled in later
		$return_values['page_num'] = $filter_values['page_num'];
		$return_values['models'] = array(); // contains the models to be actually shown
		
		// process num_per_page
		$return_values['num_per_page'] = 0;
		if($filter_values['num_per_page'] > 0)
		{
			$return_values['num_per_page'] = $filter_values['num_per_page'];
		}
		
		
		
		// ensure we have an array of rows
		if($result instanceof PDOStatement)
		{
			$result = $result->fetchAll();
		}
		
		// Save first item and last item indices
		$first_item = 1;
		$last_item = sizeof($result);
		if($filter_values['num_per_page'] > 0)
		{
			$first_item = ($filter_values['page_num']-1) * $filter_values['num_per_page'] + 1;
			$last_item = $first_item + $filter_values['num_per_page'] - 1;
		}
		
		// Running tally of how many will be shown
		$count = 0;
		
		// SHORTCUT TO ONLY LOAD RELEVANT MODELS
		if($this->accessValidationDisabled())
		{
			foreach($result as $row)
			{
				$count++;
				if($count >= $first_item && $count <= $last_item)
				{
					list($model_index, $model) = $this->modelForRowData($model_class_name, $row);
					if($model_index == null)
					{
						$return_values['models'][] = $model;
					}
					else
					{
						$return_values['models'][$model_index] = $model;
					}
				}
				
			}
		}
		else // Model validations are required on `userCanView`
		{
			// Process an array of rows from a table
			foreach($result as $row)
			{
				list($model_index, $model) = $this->modelForRowData($model_class_name, $row);
				
				// A model is found, check if it can be seen
				if($model)
				{
					// Some solutions return an array, so we shouldn't worry about userCanView, since there is no model
					if(is_array($model) || $model->userCanView() )
					{
						$count++; // only increase the count if it's something we can see
						if($count >= $first_item && $count <= $last_item)
						{
							if($model_index == null)
							{
								$return_values['models'][] = $model;
							}
							else
							{
								$return_values['models'][$model_index] = $model;
							}
						}
					}
				}
			}
			
			
		}
		
		// Pass the count back
		$return_values['num_results'] = $count;
		
		return $return_values;
		
		
		
	}
	
	/**
	 * Returns a model for the provided row data
	 * @param string $model_class_name The name of the model class that we're expecting to instantiate
	 * @param array $row
	 * @return array An array of values where the first index is the model index and second is the model value
	 */
	protected function modelForRowData($model_class_name, $row)
	{
		// check if we have an array of models already
		if($row instanceof TCm_Model)
		{
			$model = $row;
			$model_index = $model->contentCode();
		}
		
		// Model class name exists, so we instantiate the model
		elseif($model_class_name != '')
		{
			$model = $model_class_name::init($row);
			$model_index = $model->contentCode();
		}
		
		// A custom class name is provided with the row
		elseif(isset($row['process_filter_class_name'])) // custom processing when returning multiple items
		{
			$class_name = $row['process_filter_class_name'];
			$model = $class_name::init($row);
			$model_index = $row['process_filter_class_name'].'--'.$model->id();
		}
		
		// An array of values, no model at all
		else
		{
			$model_index = null; // auto count, handled by the code that
			$model = $row;
		}
		
		return array($model_index, $model);
	}
	
	//////////////////////////////////////////////////////
	//
	// BARCODE SCANNING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Enables a feature which will properly handle barcodes being entered via keyboard mode. This requires some
	 * additional event tracking, so it must be enabled manually.
	 */
	protected function enableBarcodeScanningMode()
	{
		$this->updateJSClassInitValues([ 'use_barcode_scanning' => true ]);
		
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// BULK UPDATE
	//
	// Bulk updates refer to the option to have checkboxes inserted into list as the first column which and then
	// filter forms that are processed whenever they trigger a change/update. In this way, we can select multiple
	// items from the list and have the IDs (comma-separated list) sent to a specific `TSm_ModuleURLTarget` as which to
	// process the values.
	//
	// The URL target must accept two parameters using POST
	// The first parameter is the new ID that was selected. Corresponds to `$filter_field_id`
	// The second parameter is a comma-separated list of IDs that are being altered. These correspond to the IDs for
	// the objected being listed with this view.
	//
	// This specific line must be defined for the URL Target
	// This corresponds to the values that will be submitted via JS
	// `$target->setPassValuesIntoActionMethodType('POST','new_item_id','target_ids');`
	//
	// If the resulting action for the bulk update is to redirect to a new page then the method that is called to
	// process the bulk-update must output JSON to the screen with a values set for `url`
	//
	// Example:
	//
	// $response = array('url' => '/admin/module/do/view-name/');
	// echo json_encode($response);
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Adds a field that is meant to have bulk updates. This method was updated to require the form field as the
	 * first value.
	 *
	 * It no longer requires a set of rows to be created.
	 *
	 * @param TCv_FormItem $filter_field The field being added
	 * @param string $url_target_name The name of the URL target that accepts an array of IDs as a parameter
	 * @param string|null $module_folder optional module folder name if it is different than the one the view
	 * @param TCm_Model|null $model The optional model that is required for processing the URL target
	 * belongs to
	 */
	public function addBulkUpdateFilterField(TCv_FormItem $filter_field,
											 string $url_target_name,
														  $module_folder = null,
														  $model = null)
	{
		
		if(! ($filter_field instanceof TCv_FormItem_Select) )
		{
			TC_triggerError('Filter field provided must a select form item');
		}
		
		
		$filter_field_id = $filter_field->id();
		
		// Process the bulk field value
		//$module_folder = $filter_values['module_folder'];
		//$url_target_name = $filter_values['url_target_name'];
		
		if($module_folder === null) // fixed deprecated issue with old values
		{
			$module = $this->moduleForThisClass();
		}
		else
		{
			$module = TSm_Module::init($module_folder);
		}
		if(!($module instanceof TSm_Module))
		{
			TC_triggerError('Module with folder "'.$module_folder.'" is not found');
		}
		
		$url_target = $module->URLTargetWithName($url_target_name);
		if(!($url_target instanceof TSm_ModuleURLTarget))
		{
			TC_triggerError('URL Target "'.$url_target_name.'" is not found in module '.$module->title());
		}
		
		if($model instanceof TCm_Model)
		{
			$url_target->setModel($model);
		}
		
		
		// Add to the parameters
		$filter_field->addClass('bulk_field');
		$filter_field->addFormElementCSSClass('bulk_field');
		
		
		// Save the values
		$this->bulk_filter_fields[$filter_field_id] = [
			
			'field' => $filter_field,
			'url_target_name' => $url_target_name,
			'module_folder' => $module_folder,
			'model' => $model,
			'url_target' => $url_target->url(),
		];
		
	}
	
	
	/**
	 * Returns the form for the filter
	 * @return TCv_Form
	 */
	public function bulkUpdateForm()
	{
		
		$form = new TCv_Form('bulk_update_form', false);
		$form->setAction('#');
		//->addClass('filter_form'); // match styling
		$form->setUseDivs();
		$form->setButtonText('Apply');
		$form->addClass('skip_tungsten_loading');
		$form->removeClass('stacked');
		$form->setSubmitButtonID('bulk_submit');
		
		// Wrap in a group for consistent styling
		$bulk_group = new TCv_FormItem_Group('bulk_group');
		
		foreach($this->bulk_filter_fields as $id => $values)
		{
			$bulk_group->attachView($values['field']);
		}

//		// Attach the submit button to the group, instead of to the main body
//		$submit_button = $form->submitButton();
//		$bulk_group->attachView($submit_button);
//		$form->hideSubmitButton();
//
		$form->attachView($bulk_group);
		
		
		return $form;
	}
	
	public function bulkUpdateContainer()
	{
		$container = new TCv_View();
		$container->addClass('bulk_update_container');
		
		$heading = new TCv_View();
		$heading->addClass('heading');
		$heading->addText('Bulk actions');
		$container->attachView($heading);
		
		$container->attachView($this->bulkUpdateForm());
		
		return $container;
	}
	
	
	public function enableBulkEditMode()
	{
		if($this->bulk_column_added === false)
		{
			$column = new TCv_ListColumn('bulk');
			$column->setWidthAsPixels(20);
			$column->setTitle('<a class="bulk_column_title" ><i class="fas fa-check-square"></i></a>');
			$column->setAlignment('center');
			$column->setContentUsingListMethod('bulkCheckboxColumn');
			
			$this->addTCListColumn($column);
			
			$this->bulk_column_added = true;
		}
	}
	
	
	
	
	/**
	 * Returns the rearrange handle view
	 * @param TCm_Model $model A model that could be passed in.
	 * @return TCv_View
	 */
	public function bulkCheckboxColumn($model)
	{
		$checkbox = new TCv_View();
		$checkbox->addClass('bulk_box');
		$checkbox->addDataValue('model-id', $model->id());
		$checkbox->setTag('input');
		$checkbox->setAttribute('type', 'checkbox');
		$checkbox->setAttribute('name', htmlspecialchars($this->id()));
		$checkbox->setAttribute('value','1');
		
		return $checkbox;
		
	}
	
	/**
	 * Reorders the columns using the IDs provided. If a column is found with the provided ID, it will be included in
	 * that order. Any column NOT shown will be excluded from the updated order.
	 * @param string[] $order
	 */
	public function reorderColumns($order)
	{
		// Extend the functionality to ensure the bulk column is added
		if($this->bulk_column_added)
		{
			array_unshift($order,'bulk');
		}
		
		parent::reorderColumns($order);
		
	}
	
	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Manually turns off the loading using async. In that case, the list of models must be manually added. Useful when
	 * overriding a Searchable class or adding a searchable list to a dashboard.
	 */
	public function disableLoadUsingAsync()
	{
		$this->load_using_async = false;
		$this->setPagination(false);
		$this->filters = array();
		
	}
	
	/**
	 * @return void
	 * @deprecated Use disableLoadUsingAsync instead
	 */
	public function disableLoadUsingJQuery()
	{
		$this->disableLoadUsingAsync();
	}
	
	
	public function appliedFiltersBox() : ?TCv_View
	{
		$applied_filters = new TCv_View();
		$applied_filters->addClass('applied_filter_container');
		
		$inside = new TCv_View();
		$inside->addClass('applied_filters_list');
		
		$num_possible_filters = 0;
		$num_clearable_filters = 0;
		
		// Loop through each filter, determining if they are shown and clearable
		foreach($this->filters as $filter)
		{
			// Ignore bulk fields at the start
			if(isset($this->bulk_filter_fields[$filter->id()]))
			{
				continue;
			}
			
			
			// Default values for if it appears and if it is clearable
			$show_applied_filter = false;
			$clearable = true;
			$is_prefilter = false;
			$filter_title = $filter->title();
			$filter_value = '';
			
			// Determine the default value for actual form items
			if($filter instanceof TCv_FormItem)
			{
				$show_applied_filter = $filter->appearsInSearchableAppliedFilters();
				$clearable = $filter->clearableInSearchableAppliedFilters();
			}
			
			
			// Check for pre-filters, don't show those
			if(isset($this->pre_filters[$filter->id()]))
			{
				$clearable = false;
				$show_applied_filter = true;
				$is_prefilter = true;
				$filter_title = $this->pre_filters[$filter->id()]['field_title'];
				$filter_value = $this->pre_filters[$filter->id()]['field_value'];
				
			}
			
			// Not showing, just skip to the next one
			if(!$show_applied_filter)
			{
				continue;
			}
			
			
			$container = new TCv_View();
			$container->addClass('applied_filter');
			$container->addClass($filter->id());
			$container->addDataValue('name',$filter->id());
			
			$title_span = new TCv_View();
			$title_span->setTag('span');
			$title_span->addClass('title');
			$title_span->addText($filter_title);
			
			if($filter instanceof TCv_FormItem_Checkbox)
			{
				$container->attachView($title_span);
			}
			else
			{
				$title_span->addText(' : ');
				$container->attachView($title_span);
				
				$value_span = new TCv_View();
				$value_span->setTag('span');
				$value_span->addClass('value');
				$value_span->addText($filter_value);
				$container->attachView($value_span);
				
			}
			
			if($is_prefilter)
			{
				$container->addClass('prefilter');
			}
			
			
			if($clearable)
			{
				$num_clearable_filters++;
				$container->addClass('clearable');
				$clear = new TCv_Link();
				$clear->setTitle(TC_localize('remove_filter', 'Remove filter'));
				$clear->setIconClassName('fa-times');
				$container->attachView($clear);
			}
			//if($filter->hasClass)
			$num_possible_filters++;
			$inside->attachView($container);
		}
		
		$applied_filters->attachView($inside);
		
		
		if($num_clearable_filters > 0)
		{
			$clear = new TCv_Link();
			$clear->addClass('clear_button');
			$clear->setIconClassName('fas fa-times');
			$clear->addText(TC_localize('clear_filters', 'Clear filters'));
			$applied_filters->attachView($clear);
		}
		
		// Don't bother if there are none
		if($num_possible_filters == 0)
		{
			return null;
		}
		
		return $applied_filters;
	}
	
	/**
	 * Method to load the initial list models that render on first load. This uses the existing filter function and passes
	 * in values to populate the list
	 * @return void
	 */
	protected function loadInitialListModels() : void
	{
		// Initial Load, get models
		if(!$this->load_first_async )
		{
			
			$list_session_values = $_SESSION['searchable_list_values'][$this->save_filter_id];
			$list_session_values['view_list_id'] = $this->save_filter_id;
			if(!isset($list_session_values['page_num']))
			{
				$list_session_values['page_num'] = 1;
			}
			
			$this->processFilterValues($list_session_values);
		}
		
	}
	
	/**
	 * Returns the HTML for the view
	 
	 */
	public function render()
	{
		// Ensure async load the first one if we don't have a model
		// Can't be having a model list running
		if(!$this->model_class && !class_exists($this->model_class))
		{
			$this->forceFirstLoadToUseAsync();
		}
		$this->defineFilters();
		
		$this->addClassCSSFile('TCv_SearchableModelList');
		
		if($this->disable_filtering)
		{
			parent::render();
			return;
		}
		
		// Generate the filter form
		// This must happen before saving session variables, but we need the session variables right after
		// In order to generate the content properly
		$filter_form = null;
		$applied_filters = null;
		if($this->usesFilterForm())
		{
			$filter_form = $this->filterForm();
			$applied_filters = $this->appliedFiltersBox();
		}
		
		// Save the session config, happens before JS init
		$this->saveSessionConfig();
		
		
		if($this->load_using_async)
		{
			// Handle if a content item is associated with this view
			$traits = class_uses($this);
			if(isset($traits['TMt_PagesContentView']))
			{
				$content_item = $this->contentItem();
				if($content_item instanceof TMm_PagesContent)
				{
					$this->updateJSClassInitValues(['content_item_id' => $content_item->id()]);
				}
			}
			
			if($this->bulk_column_added)
			{
				$bulk_urls = [];
				foreach($this->bulk_filter_fields as $field_id => $field_values)
				{
					$bulk_urls[$field_id] = $field_values['url_target'];
				}
				
				$this->updateJSClassInitValues(['bulk_update_fields' => $bulk_urls]);
			}
			
			
			$this->addClassJSInit();
			
			$this->loadInitialListModels();
		}
		else
		{
			$this->models = array(); // avoid an unnecessary load
		}
		
		// RENDER ELEMENTS, EVERYTHING BEFORE NEEDS TO RUN TO CONFIGURE DATA
		// Rearranging occurred on Nov 20, 2024 to account for model counts
		// due to loading the list without an async call
		
		$search_controls = new TCv_View();
		$search_controls->addClass('search_controls header_controls');
		$show_controls = false;
		
		if($this->usesFilterForm())
		{
			$show_controls = true;
			$search_controls->attachView($filter_form);
			
		}
		
		if($this->bulk_column_added)
		{
			$show_controls = true;
			$search_controls->attachView($this->bulkUpdateContainer());
		}
		
		if($this->num_per_page > 0)
		{
			$show_controls = true;
			$pagination_holder = new TCv_View();
			$pagination_holder->addClass('pagination_holder');
			$pagination_holder->attachView($this->paginationControls(1));
			$search_controls->attachView($pagination_holder);
		}
		
		if($this->usesFilterForm())
		{
			$search_controls->attachView($applied_filters);
		}
		
		if($show_controls)
		{
			$this->attachView($search_controls);
		}
		
		
		
		
		
		parent::render();
		
		// Insert post-list controls
		if($this->num_per_page > 0 && $this->show_footer_pagination)
		{
			$search_controls = new TCv_View();
			$search_controls->addClass('search_controls footer_controls');
			
			$pagination_holder = new TCv_View();
			$pagination_holder->addClass('pagination_holder');
			$pagination_holder->attachView($this->paginationControls(1));
			$search_controls->attachView($pagination_holder);
			$this->attachView($search_controls);
		}
		
	}
	
	protected function saveSessionConfig() : void
	{
		$_SESSION['searchable_list_values'][$this->save_filter_id]['model_class'] = $this->model_class;
		
		$model_id = '';
		if($this->filter_model instanceof TCu_Item)
		{
			$model_id = $this->filter_model->id();
		}
		
		if(isset($_SESSION['searchable_list_values'][$this->save_filter_id]['token']))
		{
			$token = $_SESSION['searchable_list_values'][$this->save_filter_id]['token'];
		}
		else
		{
			$token = bin2hex(random_bytes(32));
		}
		
		// Save the token as a JS value
		$this->addJSClassInitValue('token', $token);
		
		$_SESSION['searchable_list_values'][$this->save_filter_id]['token'] = $token;
		$_SESSION['searchable_list_values'][$this->save_filter_id]['model_id'] = $model_id;
		$_SESSION['searchable_list_values'][$this->save_filter_id]['model_list_class'] = $this->model_list_class_name;
		$_SESSION['searchable_list_values'][$this->save_filter_id]['view_list_class'] = get_class($this);
		$_SESSION['searchable_list_values'][$this->save_filter_id]['filter_method_name'] = $this->filter_method_name;
		$_SESSION['searchable_list_values'][$this->save_filter_id]['view_class_for_model'] = $this->view_model_for_class;
		$_SESSION['searchable_list_values'][$this->save_filter_id]['num_per_page'] = $this->num_per_page;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// GOOGLE ANALYTICS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets this list as being a google item list which shows items that are being presented
	 * @param $id
	 * @param $name
	 * @return void
	 */
	protected function setAsGoogleAnalyticsItemList($id, $name)
	{
		$this->gtag_list_id = $id;
		$this->gtag_list_name = $name;
	}
	
	/**
	 * Generates an array of values to be converted into a Gtag for analytics.
	 * @see https://developers.google.com/analytics/devguides/collection/ga4/ecommerce?client_type=gtag#select_an_item_from_a_list
	 * @return array
	 */
	protected function generateGTagForModels()
	{
		$values = [
			'item_list_id' =>$this->gtag_list_id,
			'item_list_name' => $this->gtag_list_name,
		];
		$values['items'] = [];
		
		foreach($this->models as $model)
		{
			if(method_exists($model, 'price'))
			{
				$item = TSv_GoogleAnalytics::itemValuesForModel($model);
				$item['item_list_id'] = $this->gtag_list_id;
				$item['item_list_name'] = $this->gtag_list_name;
				
				$values['items'][] = $item;
			}
		}
		
		
		return $values;
	}
}
