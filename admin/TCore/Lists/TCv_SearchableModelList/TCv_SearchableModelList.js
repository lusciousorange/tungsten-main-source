class TCv_SearchableModelList {
	element = null;
	pagination_holders = [];

	filter_form = null;
	page_num = 1;
	fetch_abort_controller = null;
	filter_text_all_fields = '.filter_form #form_filter_search, ' +
		'.filter_form select, ' +
		'.filter_form .TCv_FormItem_Hidden.filter_item,' +
		'.filter_form input[type="date"]';
	update_target = null;
	search_timeout = null;
	options = {
		content_item_id 		: false,
		bulk_update_fields 		: {},
		view_list_id 			: null,
		filter_update_target	: '',
		sort_column 			: null,
		sort_order 				: 'asc',
		token 					: null,
		load_first_async        : false,

		// BARCODE SCANNING
		use_barcode_scanning 	: false,


	}
	/**
	 * @param {Element} element
	 * @param {Object} values
	 */
	constructor(element, values = {}) {
		this.element = element;
		this.filter_form = this.element.querySelector('.filter_form');
		this.pagination_holders = this.element.querySelectorAll('.pagination_holder');

		// Save config settings
		Object.assign(this.options, values);
		//console.log('Saved Options:', values);
		//this.focusOnSearch(); // Created unexpected results, especially on pages that have multiple


		this.addFilterFormEventListeners();
		this.addSortEventListeners();
		this.addPaginationEventListeners();
		this.addAppliedFilterListeners();
		this.addBulkActionsEventListeners();

		this.determineUpdateTarget();

		this.updateBulkContainerVisibility();

		// Detect default values in URL
		this.processURLParamsDefaults();

		if(this.options.load_first_async)
		{
			// Very last thing, trigger the update
			this.updateWithFilters(true);
		}
		else
		{
			// Still need to deal with the applied filters separately
			this.processAppliedFilters();
		}

	}

	/**
	 * Determines what the target should be for any update requests
	 */
	determineUpdateTarget() {
		// Determine what element to update
		// If not set, then it defaults to the tbody for the list
		if (this.options.filter_update_target == '') {
			this.update_target = this.element.querySelector(".table_body");
		}
		else {
			this.update_target = this.element.querySelector(this.options.filter_update_target);
		}
	}

	/**
	 * Focuses on the search field if it exists
	 */
	focusOnSearch() {
		let search_field = this.element.querySelector('#form_filter_search');
		if(search_field)
		{
			search_field.focus();
		}
	}

	/**
	 * Adds the event listeners to the filter form
	 */
	addFilterFormEventListeners() {
		if(this.filter_form)
		{
			this.filter_form.addEventListener('submit', (e) => this.filterFormSubmit(e));

			let filter_search = this.filter_form.querySelector('#form_filter_search');
			if(filter_search) {
				filter_search.addEventListener('input', (e) => {this.searchFieldChanged(e)});

				this.filter_form.querySelector('.clear_search_link').addEventListener('click', (e) => {
					e.preventDefault();

					// Clear and trigger change
					filter_search.value = '';
					this.searchFieldChanged(e);
				});
			}


			this.filter_form.querySelectorAll('select, input[type="date"]').forEach(el => {
				el.addEventListener('change', (e) => this.selectFieldChanged(e));
			});

			this.filter_form.querySelectorAll('input[type="checkbox"], input[type="radio"]').forEach(el => {
				el.addEventListener('change', (e) => this.checkboxFieldChanged(e));
			});


		}
	}

	/**
	 * Adds the sort event listeners and sets the first one to be the set field. This function also detects if a default is set
	 * via the PHP method setSortDefaultColumn() and if not, it chooses the first column and does ascending order
	 */
	addSortEventListeners() {
		// Detect a default
		if(this.options.sort_column !== null)
		{
			let sort_button = this.element.querySelector('.sort_col_button[data-sort="'+this.options.sort_column +'"]');

			// select that button
			sort_button.classList.add(this.options.sort_order, 'active');
		}
		else // detect the first
		{
			// Find the first one and set it as the default asc
			let first_sort_button = this.element.querySelector('th a.sort_col_button');
			if(first_sort_button)
			{
				first_sort_button.classList.add('asc', 'active');
				this.options.sort_column = first_sort_button.getAttribute('data-sort');
				this.options.sort_order = 'asc';
			}
		}
		this.element.querySelectorAll('th a.sort_col_button').forEach(el => {
			el.addEventListener('click', (e) => this.sortChanged(e))
		});
	}

	/**
	 * Handles when a sort event happens
	 * @param event
	 */
	sortChanged(event) {

		let button = event.currentTarget;
		let sort = button?.getAttribute('data-sort');
		let button_svg = button.querySelector('svg');

		// Matches sort, flip the order
		if(sort === this.options.sort_column)
		{
			if(this.options.sort_order === 'asc')
			{
				this.options.sort_order = 'desc';
				button.classList.remove('asc');
				button.classList.add('desc');

			}
			else
			{
				this.options.sort_order = 'asc';
				button.classList.add('asc');
				button.classList.remove('desc');
			}


		}
		else // change columns
		{
			this.options.sort_column = sort;
			this.options.sort_order = 'asc';

			let current_column_button = this.element.querySelector('a.sort_col_button.active');
			if(current_column_button)
			{
				current_column_button.classList.remove('asc','desc','active');
			}

			button.classList.add('asc','active');


		}



		this.updateWithFilters(true);


	}


	/**
	 *
	 * @param {SubmitEvent} event
	 */
	filterFormSubmit(event) {
		//console.log('filterFormSubmit', event);
		event.stopPropagation();
		event.preventDefault();
	}

	/**
	 * Tracks if the search field changed, which uses a timeout
	 * @param event
	 */
	searchFieldChanged(event) {
		if (this.search_timeout != null) {
			clearTimeout(this.search_timeout);
		}

		this.search_timeout = setTimeout(() => this.updateWithFilters(), 500);
		this.page_num = 1; // reset whenever changing values

	}

	/**
	 * Event listener for when a select field changes
	 * @param event
	 */
	selectFieldChanged(event) {
		this.processFilterFieldChange(event);
	}

	/**
	 * Event listener for when a checkbox changes
	 */
	checkboxFieldChanged(event) {
		this.processFilterFieldChange(event);
	}



	processFilterFieldChange(event) {
		this.page_num = 1; // reset the count
		this.updateWithFilters(true);
	}

	addTungstenLoading() {
		this.element.classList.add('tungsten_loading');
	}

	removeTungstenLoading() {
		this.element.classList.remove('tungsten_loading');
	}

	/**
	 * Function to deal with all the applied filters on items. Styling, formatting, etc
	 */
	processAppliedFilters() {
		// Text Fields
		this.element.querySelectorAll(this.filter_text_all_fields).forEach((el) => {
			if(!el.closest('.filter_item').classList.contains('bulk_field')) {
				if (el.value != '') {
					el.classList.add('filter_active');
				}
				else {
					el.classList.remove('filter_active');
				}

				this.updateAppliedFilterForItem(el);
			}
		});

		// Checkboxes
		this.element.querySelectorAll('.filter_form input[type="checkbox"]').forEach( (el) => {
			this.updateAppliedFilterForCheckbox(el);
		});

		// Radio buttons
		this.element.querySelectorAll('.filter_form .TCv_FormItem_RadioButtons').forEach((el) => {
			let field_name = el.getAttribute('data-row-id');
			let checked_field = el.querySelector('input[name='+field_name+']:checked');
			if(checked_field)
			{
				this.updateAppliedFilterForRadioItem(checked_field);
			}

		});
	}

	/**
	 * Updates the list with filters
	 * @param clear_pagination
	 */
	updateWithFilters(clear_pagination ) {
		if(clear_pagination) {
			this.element.querySelectorAll('.pagination_controls').forEach((el) => {el.innerHTML = '';});
		}
		this.addTungstenLoading();

//		this.$list.find('.list_container').addClass('loading');

		let params = new URLSearchParams({
			view_list_id 		: this.options.view_list_id,
			page_num			: this.page_num,
			sort_column			: this.options.sort_column,
			sort_order			: this.options.sort_order,
			token				: this.options.token,

		});

		if (this.options.content_item_id !== false)
		{
			params.append('content_item_id', this.options.content_item_id);

		}

		// Deal with applied filters separately
		this.processAppliedFilters();

		// GET PARAMETERS

		// Text Fields
		this.element.querySelectorAll(this.filter_text_all_fields).forEach((el) => {
			if(!el.closest('.filter_item').classList.contains('bulk_field')) {
				params.append(el.getAttribute('id'), el.value);
			}
		});

		// Checkboxes
		this.element.querySelectorAll('.filter_form input[type="checkbox"]').forEach( (el) => {
			params.append(el.getAttribute('id'), el.checked ? 1 : 0);
		});


		// Radio buttons
		this.element.querySelectorAll('.filter_form .TCv_FormItem_RadioButtons').forEach((el) => {
			let field_name = el.getAttribute('data-row-id');
			let checked_field = el.querySelector('input[name='+field_name+']:checked');
			if(checked_field)
			{
				params.append(field_name, checked_field.value);
			}

		});

		// Handle abort controller
		if (this.fetch_abort_controller !== null)
		{
			this.fetch_abort_controller.abort();
		}
		this.fetch_abort_controller = new AbortController();

		// SEND THE REQUEST
		this.sendUpdateRequest(params);

	}

	sendUpdateRequest(params) {

		// Generate the URL
		let url = '/admin/TCore/Lists/TCv_SearchableModelList/ajax_update_searchable_list.php?';
		url += params; // auto-call the toString function

		// Send Request
		fetch(url ,{
			method : 'GET',
			signal : this.fetch_abort_controller.signal, // pass the abort controller signal
		})	.then(response => response.json())
			.then(response =>{

				if(response.bad_token || response.no_list_value)
				{
					location.reload();
				}

				let empty_message = this.element.querySelector('.empty_message');
				if(empty_message)
				{
					empty_message.remove();
				}

				// UPDATE MAIN CONTENT
				if('list_html' in response)
				{
					this.update_target.innerHTML = response.list_html;
				}

				// UPDATE CSS:  the list css if it's set
				processAsyncCSSLinks(response.list_css);


				// UPDATE PAGINATION
				if('pagination_html' in response)
				{
					this.element.querySelectorAll('.pagination_holder').forEach((el) =>
					{
						el.innerHTML = response.pagination_html;
					});
				}

				// RESET BULK FIELDS
				for(const field_id in this.options.bulk_update_fields)
				{

					this.element.querySelector('#'+field_id).value = '';
				}

				this.hook_processUpdateResponse(response);


			})
			.catch((error) => {
				console.log(error)
			})
			.finally(() =>{
				this.fetch_abort_controller = null; // disable the abort controller
				this.removeTungstenLoading();
			});
	}

	/**
	 * A hook method to allow programmers to deal with the response coming back if necessary
	 * @param {Object} response
	 */
	hook_processUpdateResponse(response) {
		// Extend and override this function if you need to process data from the response in an extended class.
	}

	//////////////////////////////////////////////////////
	//
	// PAGINATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds the event listeners related to the pagination filters
	 */
	addPaginationEventListeners() {
		this.pagination_holders.forEach(el => {
			el.addEventListener('click',(e) => {this.paginationClick(e)});
		});
	}


	/**
	 * Handles when a page link is clicked
	 * @param event
	 */
	paginationClick(event) {
		event.preventDefault();

		let button = event.target.closest('.page_link');

		if(button)  {

			// Save the page number, then update
			this.page_num = button.getAttribute('data-target');
			this.updateWithFilters(false);
		}
	}
	//////////////////////////////////////////////////////
	//
	// APPLIED FILTERS
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds the event listeners related to the applied filters
	 */
	addAppliedFilterListeners() {
		// Only bother if we have it
		let container = this.element.querySelector('.applied_filter_container');
		if(container)
		{
			let clear_button = container.querySelector('.clear_button');
			if(clear_button)
			{
				clear_button.addEventListener('click', (e) => {this.clearFilters(e)});
			}

			container.querySelectorAll('.applied_filter a').forEach(el => {
				el.addEventListener('click', (e) => this.clearAppliedFilterHandler(e));
			});
		}
	}


	/**
	 * Updates the applied filter values for a given item
	 * @param item
	 */
	updateAppliedFilterForItem(item) {
		let filter_text_value = item.value;

		let applied_filter = this.element.querySelector('.applied_filter.' + item.getAttribute('id'));
		if(applied_filter && !applied_filter.classList.contains('prefilter'))
		{
			let show_filter = item.value !== '';

			// SELECT FIELD
			if(item.nodeName == 'SELECT')
			{
				filter_text_value = item.options[item.selectedIndex].text;
			}
			else
			{
				filter_text_value = item.value;
			}


			// Update the value
			if(filter_text_value)
			{
				applied_filter.querySelector('.value').textContent = filter_text_value;
			}

			// Update the visibility
			if(show_filter)
			{
				applied_filter.classList.remove('hidden');
			}
			else
			{
				applied_filter.classList.add('hidden');

			}

			this.cleanUpAppliedFilters();
		}
	}

	/**
	 * Updates the applied filter values for a given item
	 * @param {HTMLInputElement} item
	 */
	updateAppliedFilterForCheckbox(item) {
		let applied_filter = this.element.querySelector('.applied_filter.' + item.getAttribute('id'));
		if(applied_filter)
		{
			if(item.checked)
			{
				applied_filter.classList.remove('hidden');
			}
			else
			{
				applied_filter.classList.add('hidden');
			}

		}
		this.cleanUpAppliedFilters();
	}

	/**
	 * Updates the applied filter values for a given item
	 * @param {HTMLInputElement} item
	 */
	updateAppliedFilterForRadioItem(item) {
		let applied_filter = this.element.querySelector('.applied_filter.' + item.getAttribute('name'));
		if(applied_filter)
		{

			let selector = 'label[for=' + item.getAttribute('id') + '] .button_text';
			let label = this.element.querySelector(selector);
			applied_filter.querySelector('.value').textContent = label.textContent;
		}
		this.cleanUpAppliedFilters();
	}

	cleanUpAppliedFilters() {
		let container = this.element.querySelector('.applied_filter_container');
		let applied_elements = container.querySelectorAll('.applied_filter:not(.hidden)');
		let num_prefilter = container.querySelectorAll('.applied_filter.prefilter').length;
		if(applied_elements.length == 0)
		{
			container.classList.add('empty');
			this.element.classList.remove('filters_applied');
		}
		else
		{
			container.classList.remove('empty');
			this.element.classList.add('filters_applied');

		}

		// Only prefilters are shown, hide the clear button
		let clear_button = container.querySelector('.clear_button');
		if(clear_button)
		{
			if (applied_elements.length == num_prefilter)
			{
				clear_button.hide();
			}
			else
			{
				clear_button.show('flex');
			}
		}


	}

	/**
	 * Triggered when the filter is cleared
	 * @param event
	 */
	clearFilters(event) {
		event.preventDefault();

		// Clear fields
		this.element.querySelectorAll('.applied_filters_list .clearable').forEach(el => {
			let field_id = el.getAttribute('data-name');
			this.clearFieldWithID(field_id);

		});

		// Update the filters
		this.updateWithFilters(true);

	}

	/**
	 * Clears a field with a particular ID
	 * @param {string} field_id
	 */
	clearFieldWithID(field_id) {
		let field = this.element.querySelector('#'+field_id);
		if(field) {
			field.value = '';
		}
		else {
			//	console.error('field not found: '+ field_id);
		}


		// TODO: HOw do we clear checkboxes and radio buttons
	}

	/**
	 * Clears an applied filter, based on an event click
	 * @param event
	 */
	clearAppliedFilterHandler(event) {
		event.preventDefault();

		// Get the field ID and clear it
		let field_name = event.currentTarget.parentElement.getAttribute('data-name');
		this.clearFieldWithID(field_name);

		// Update the filters
		this.updateWithFilters(true);
	}


	//////////////////////////////////////////////////////
	//
	// BULK ACTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds the event listeners for all the bulk field actions
	 */
	addBulkActionsEventListeners() {

		// Only bother if we have update fields
		if(Object.keys(this.options.bulk_update_fields).length > 0)
		{
			// Listen on the form
			let bulk_form = this.element.querySelector('.bulk_update_form');
			if(bulk_form)
			{
				bulk_form.addEventListener('submit', (e) => { this.bulkUpdateSubmit(e) })
			}

			this.element.addEventListener('change', (e) => this.handleBulkBoxChange(e));

			// select all button
			let button = this.element.querySelector('a.bulk_column_title');
			if(button)
			{
				button.addEventListener('click',  (e) => this.toggleBulkSelectAll(e));
			}


		}


	}

	/**
	 * Delegation listener, for all change events, then finds the ones that are bulk_boxes
	 * @param {Event} event
	 */
	handleBulkBoxChange(event) {
		if(event.target.nodeName === 'INPUT' && event.target.classList.contains('bulk_box'))
		{
			this.updateBulkContainerVisibility();

		}
	}

	updateBulkContainerVisibility() {
		if(Object.keys(this.options.bulk_update_fields).length > 0)
		{
			let checked_fields = this.element.querySelectorAll('.bulk_box:checked');

			// At least one thing is unchecked, so check all
			if(checked_fields.length > 0)
			{
				this.element.querySelector('.bulk_update_container').slideDown();
			}
			else
			{
				this.element.querySelector('.bulk_update_container').slideUp();

			}
		}
	}

	bulkUpdateSubmit(event) {
		event.preventDefault();

		let changed_bulk_fields = [];

		// Loop through the fields that could be considered. If they aren't empty, run it.
		// Make sure to find them in the value column since bulk_field is applied to a bunch of stuff
		let bulk_fields = this.element.querySelectorAll('.bulk_update_form .value_column .bulk_field');

		bulk_fields.forEach((element)=> {
			// Save the bulk update values
			if(element.value !== '')
			{
				changed_bulk_fields.push(element);
			}
		});

		// Run the updates
		this.performBulkActions(changed_bulk_fields);

	}

	/**
	 * Performs the bulk actions for all the fields that have been selected
	 * @param {Array} fields
	 */
	performBulkActions(fields) {
		//console.log('performBulkActions', fields);

		// Generate the config that is sent for all of them
		let fetch_config = {
			method : 'POST',
		};

		// Get the array of selected IDs
		let selected_ids = [];
		this.element.querySelectorAll('.bulk_box:checked').forEach(el => {
			selected_ids.push(el.getAttribute('data-model-id'))
		});


		// Generate an array of fetches
		let fetches = [];
		fields.forEach(field => {

			// Create the form data to send
			let form_data = new FormData();
			form_data.append('target_ids', selected_ids.join(',') );
			form_data.append('new_item_id',field.value);

			// Generate the URL
			let url = this.options.bulk_update_fields[field.getAttribute('id')];

			fetch_config['body'] = form_data;
			fetches.push( fetch(url, fetch_config));
		});

		// Use promise.all to generate all the fetches at once
		// @see https://gomakethings.com/waiting-for-multiple-all-api-responses-to-complete-with-the-vanilla-js-promise.all-method/
		Promise.all(fetches)
			.then(function (responses){
				//console.log(responses);
			}).catch((error) => {
			// if there's an error, log it
			console.log(error);
		}).finally(() => {
			this.updateWithFilters(false);
			this.element.querySelector('.bulk_update_container').slideUp();
		});


	}

	toggleBulkSelectAll(event) {
		let unchecked = this.element.querySelectorAll('.bulk_box:not(:checked)');

		let new_value = false;

		// At least one thing is unchecked, so check all
		if(unchecked.length > 0)
		{
			new_value = true;
		}
		this.element.querySelectorAll('.bulk_box').forEach(el => {
			el.checked = new_value;
		});


		this.updateBulkContainerVisibility();

	}

	//////////////////////////////////////////////////////
	//
	// URL DEFAULTS
	//
	//////////////////////////////////////////////////////

	/**
	 * Handles processing URL values that are passed as values in the URL as key/value pairs which are acquired, set then
	 * the history is corrected.
	 */
	processURLParamsDefaults() {
		// save the url for modification
		let new_url = window.location.href;

		this.element.querySelectorAll(this.filter_text_all_fields).forEach((field) => {
			let field_id = field.getAttribute('id').replace(/[\[\]]/g, '\\$&');


			// Find the value for this field if set
			let regex = new RegExp( '[?&]' + field_id + '=([^&#]*)', 'i' );
			let value = regex.exec(window.location.href);
			value = value ? value[1] : null;




			// We've been passed a URL Value
			if(value != null)
			{
				// Update the field
				// Special case for "search", remove the spaces
				if(field_id === 'form_filter_search')
				{
					field.value = value.replace('+',' ');
				}
				else
				{
					field.value = value;
				}


				// Update URL
				let url_string = field_id + "=" + value;
				new_url = new_url.replace('?' + url_string,'?');
				new_url = new_url.replace('&' + url_string,'');
			}
		});

		// Trim an empty ? character
		new_url = new_url.replace(/\?\s*$/,'');

		// replace the history state for a clean URL
		history.replaceState( {} , 'foo', new_url );


	}

}