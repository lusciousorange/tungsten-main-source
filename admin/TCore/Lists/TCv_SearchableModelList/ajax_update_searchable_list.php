<?php
	$skip_tungsten_authentication = true; // skip authentication to allow for loading from public pages
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");
	set_time_limit(0);
	ob_start("ob_gzhandler");
	
	if(!isset($_SESSION['searchable_list_values'][$_GET['view_list_id']]))
	{
		print json_encode(['no_list_value' => 'no_list_value']);
		exit();
	}
	
	$list_session_values = $_SESSION['searchable_list_values'][$_GET['view_list_id']];
	
	// Save any of the newly passed in values
	// They will get saved and updated when the filter is applied
	foreach($_GET as $key => $value)
	{
		$list_session_values[$key] = $value;
	}
	
	// VALIDATE TOKEN
	if(@$_GET['token'] != $list_session_values['token'])
	{
		print json_encode(['bad_token' => 'Token not found']);
		exit();
	}
	
	// VALIDATE IF THE VIEW LIST CAN BE LOADED WITHOUT BEING LOGGED IN
	$traits = class_uses($list_session_values['view_list_class']);
	// @TODO: Test if we need to test for TC_isTUngstenView instead
	if(!isset($traits['TMt_PagesContentView']))
	{
		// In here, means we're loading a searchable list from a view not coming from a Page Content View
		// We must authenticate
		TC_website()->authenticateSession(); // authenticate to ensure they can view this page
	}
	
	// 2022-06-01 : Paulo
	// Disabling the public view mode, which incorrectly shows items
	// Instead the lists should always be respecting userCanView()
	
	// Instantiate Model and View List Class
	if($list_session_values['model_id'] != '')
	{
		// We're passing in a model with an ID, commonly used when we have a subset list that is based on another model
		// Eg: show all the items, but just for this one thing
		$model_list = TCv_SearchableModelList::modelListForSessionValues($list_session_values);
		$view_list = ($list_session_values['view_list_class'])::init($model_list); // pass it in either way
	}
	else // Just a normal list that will pull in the TCm_ModelList when the time comes
	{
		$view_list = ($list_session_values['view_list_class'])::init();
		
	}
	
	// Deals with a potentially passed in content_item_id to ensure content variables are passed through
	// This is only a concern for the async load because the main load would already handle it as part of rendering
	if(isset($list_session_values['content_item_id']) && isset($traits['TMt_PagesContentView']))
	{
		$content_item = TMm_PagesContent::init($list_session_values['content_item_id']);
		if($content_item instanceof TMm_PagesContent)
		{
			$view_list->setPagesContentItem($content_item);
		}
	}
	
	// Calls the processor to get a proper set of return values
	$return_values = array();
	if($view_list instanceof TCv_SearchableModelList)
	{
		// Set the filter ID
		if(isset($list_session_values['view_list_id']))
		{
			$view_list->setSaveFilterID($list_session_values['view_list_id']);
		}
		
		$return_values = $view_list->processFilterValues($list_session_values);
		$return_values['is_empty'] = false;
		
		if($return_values['list_html'] == '')
		{
			$return_values['list_html'] = $view_list->emptyMessageView()->html();
			$return_values['is_empty'] = true;
		}
	}
	
	TMm_BenchmarkEntry::instance()?->trackRenderTimes();
	print json_encode($return_values);
	TMm_BenchmarkEntry::instance()?->trackPrintTimes();
	ob_end_flush();
