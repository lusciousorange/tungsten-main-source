<?php

/**
 * Class TCv_ListHeadingCell
 */
class TCv_ListHeadingCell extends TCv_ListCell
{
	/**
	 * TCv_ListHeadingCell constructor.
	 * @param $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		$this->setTag('th');
		
	}


	/**
	 * @return string
	 */
	public function html()
	{
		return parent::html();
	}
	
}
?>