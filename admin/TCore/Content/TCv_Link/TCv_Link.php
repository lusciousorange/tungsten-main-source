<?php
/**
 * Class TCv_Link
 */
class TCv_Link extends TCv_View
{
	protected $use_dialog = false; // [bool] = 
	protected $dialog_type = false; // [string]= The type of dialog being shown
	protected $url = false;
	protected $title = false;
	protected $backup_title = '';
	protected $open_in_new_window = false;
	protected $link_icon_code = false;
	protected $link_icon_code_symbol = false;
	protected $icon_view = false;
	protected $wrap_icon_in_container = false;
	/**
	 * Creates a new link
	 * TCv_Link constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		//$this->show_id = false; // default to not showing ID
		$this->setTag('a');
		$this->setHTMLCollapsing(false); // turn off for all the menu settings used throughout the system
	}
	
	/**
	 * Sets the title for the link
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * Returns the title for the link
	 *
	 * @return bool|string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Sets the url for the link
	 *
	 * @param string $url
	 */
	public function setURL($url)
	{
		$this->url = $url;
	}
	
	/**
	 * Sets the url for the link
	 *
	 * @param string $url
	 */
	public function setHREF($url)
	{
		$this->setURL($url);
	}
	
	/**
	 * Appends a string to the end of the existing URL value
	 * @param string $append
	 */
	public function appendToURL($append)
	{
		$this->url .= $append;
	}
	
	/**
	 * Returns the current URL for the link
	 * @return bool|string
	 */
	public function url()
	{
		return $this->url;
	}
	

	/**
	 * Extends the function to deal with backup titles
	 * @param string $text
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 */
	public function addText($text, $disable_template_parsing = false)
	{
		if(is_null($text))
		{
			$text = '';
		}
		parent::addText($text,$disable_template_parsing);
		$this->backup_title = strip_tags($text);
	}
	
	/**
	 * Adds a hidden title to the button which is commonly used with icon-only buttons to ensure accessibility
	 * @param string $title
	 */
	public function addHiddenTitle($title)
	{
		$span = new TCv_View();
		$span->setTag('span');
		$span->addText($title);
		$span->setAttribute('hidden','');
		$this->attachView($span);
		
		$this->setAttribute('aria-label', $title);

	}
	
	/**
	 * Reverts this link into a span. this removes the attributes that don't belong, changes the tag to a span, and
	 * essentially de-links this view.
	 * @return void
	 */
	public function revertToSpan() : void
	{
		$this->setTag('span');
		$this->unsetAttribute('href');
		
	}

	//////////////////////////////////////////////////////
	//
	// ICONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets this link to have an icon which will be prepended to the start of the link text in an <i> tag.
	 *
	 * @param string $icon
	 * @param bool|string $symbol_id The ID for this symbol
	 */
	public function setIconClassName($icon, $symbol_id = false)
	{
		$this->link_icon_code = $icon;
		$this->link_icon_code_symbol = $symbol_id;
	}
	
	
	/**
	 * Sets this link to have an icon which will be prepended to the start of the link text in an <i> tag.
	 *
	 */
	public function removeIconClassName() : void
	{
		$this->link_icon_code = null;
		$this->link_icon_code_symbol = null;
	}
	
	
	/**
	 * Returns if the content in this link is currently only an icon. This will check if all the attached views
	 *
	 * @return bool
	 */
	public function contentIsOnlyIcon()
	{
		foreach($this->attachedViews() as $view)
		{
			if($view instanceof TCv_View)
			{
				if(!$view->hasClass('fa'))
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Indicates that we should wrap the icon in a container. This applies a span element with the class provided,
	 * allowing for better styling of the link.
	 * @param string $class_name The name of the CSS class to wrap the link in
	 */
	public function wrapIconInContainer(string $class_name = 'icon_box')
	{
		$this->wrap_icon_in_container = $class_name;
	}
	
	
	/**
	 * Returns the icon view.
	 * This method also handles any SVG symbols associated with the icon
	 * @return TCv_View
	 */
	public function iconView()
	{

		if($this->link_icon_code_symbol)
		{
			$icon_view = new TSv_FontAwesomeSymbol($this->link_icon_code_symbol, $this->link_icon_code);
		}
		else
		{
			$icon_view = new TCv_View();
			$icon_view->setTag('i');
			$icon_view->addClass($this->link_icon_code);

		}
		$icon_view->addClass('link_icon');

		if($this->wrap_icon_in_container !== false)
		{
			$container = new TCv_View();
			$container->setTag('span');
			$container->addClass($this->wrap_icon_in_container);
			$container->attachView($icon_view);
			return $container;
		}
		
		return $icon_view;	
	}
	
	/**
	 * Manualy set the icon view
	 * @param TCv_View $view
	 */
	public function setIconView($view)
	{
		if($view instanceof TCv_View)
		{
			$this->icon_view = $view;	
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// DIALOG
	//
	//////////////////////////////////////////////////////


	public function useDialog($question, $type = 'fa-exclamation-triangle', $color = 'orange', $full_screen = false)
	{
 		$this->addDataValue('dialog-content', $question);
 		$this->addClass('TCv_Dialog_button'); // enable auto-detect
 		
 		$this->setDialogIcon($type);
 		$this->setDialogColor($color);
 		$this->setDialogFullscreen($full_screen);
 	
 	}
 	
 	/**
 	 * Sets the icon code for the dialog
 	 * @param string $icon
 	 */
 	public function setDialogIcon(string $icon)
 	{
 		$this->addDataValue('dialog-icon', $icon);
 	}
 	
 	/**
 	 * Sets the color for the dialog
 	 * @param string $color
 	 */
 	public function setDialogColor(string $color)
 	{
 		$this->addDataValue('dialog-color', $color);
 	}
 	
 	/**
 	 * Sets if the dialog is fullscreen
 	 * @param bool $is_fullscreen
 	 */
 	public function setDialogFullscreen(bool $is_fullscreen)
 	{
 		if($is_fullscreen)
 		{
 			$this->addDataValue('dialog-fullscreen', 1);
 		}
 	}
 	
 	/**
 	 * Sets the text for the cancel button. Setting this to an empty string will hide the button
 	 * @param string $text
 	 */
 	public function setDialogCancelText(string $text) : void
 	{
 		$this->addDataValue('dialog-cancel-text', $text);
 	}
 	
 	/**
 	 * Sets the text for the ok button. Setting this to an empty string will hide the button
 	 * @param string $text
 	 */
 	public function setDialogOKText(string $text) : void
 	{
 		$this->addDataValue('dialog-ok-text', $text);
 	}
 	

	
	/**
	 * TUrns on the target="_blank" property for the link which means this link opens in a new window or tab
	 */
	public function openInNewWindow()
	{
		$this->setAttribute('target', '_blank');
		
		// Adding noopener to avoid access to window object
		// https://web.dev/external-anchors-use-rel-noopener/?utm_source=lighthouse&utm_medium=devtools
		$this->setAttribute('rel', 'noopener');
		$this->open_in_new_window = true;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// AJAX
	//
	//////////////////////////////////////////////////////


	
	/**
	 * Returns the HTML for the link
	 * @return string
	 */
	public function html()
	{
		if(!$this->render_called)
		{
			$this->render();
			$this->render_called = true;
		}

		if($this->icon_view)
		{
			// prepends the icon
			$this->attachView($this->icon_view, true);
			
		}
		elseif($this->link_icon_code)
		{
			// prepends the icon
			$this->attachView($this->iconView(), true);
				
		}
		
		if($this->url)
		{
			// Any URL should be processed for pageview links
			$url = $this->replacePageViewCodesWithURL($this->url);
			
			// Remove any encoding, just in case it creeps in
			$url = str_replace('&amp;','&',$url);
			
			$this->setAttribute('href', $url);
			
		
		
		}
		if($this->title)
		{
			$this->setAttribute('title', strip_tags($this->title));
		}
		
		

		return parent::html();
	}
}

?>