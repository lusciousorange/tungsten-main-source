<?php
/**
 * Class TCu_Text
 *
 * Class used to manipulate a string of text. It also contains various class methods to alter a generic text item as well.
 */
class TCu_Text
{
	use TSt_ConsoleAccess;

	private $original_text; // [string] = The original text provided
	private $text; // [string]= The text that is modified at each step
	
	private $template_has_been_parsed = false;

	
	/**
	 * TCu_Text constructor.
	 * @param string $text
	 * @param bool $trim (Optional) Default true.
	 */
	public function __construct($text, $trim = true)
	{
		if($trim)
		{
			$this->original_text = trim($text);
		}
		else
		{
			$this->original_text = $text;
		}
		
		$this->text = $this->original_text;
		
	
	}

	/**
	 * Turns off template parsing for this item
	 */
	public function disableTemplateParsing()
	{
		$this->template_has_been_parsed = true;
	}

	/**
	 * Resets any changes to the text and reverts it ot the original value
	 */
	public function resetText()
	{
		$this->text = $this->original_text;
		$this->template_has_been_parsed = false;
	}
	
	/**
	 * Returns the text
	 * @return string
	 */
	public function getText()
	{
		return $this->text();
	}
	
	/**
	 * Returns the text
	 *
	 * @return string
	 */
	public function text()
	{
		$this->parseTemplating();
		return $this->text;
	}
	
	/**
	 * Removes accents from the text to provide an ascii safe version, commonly used for URLs
	 * @return string
	 */
	public function removeAccents()
	{
		$table = array(
			'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj','đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
			'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
			'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
			'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
			'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
			'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
			'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y',
			'Ŕ'=>'R', 'ŕ'=>'r',
		);
		
		$this->text = strtr($this->text, $table);
		return $this->text;
	}
	
	
	
	/**
	 * Returns the original text
	 * @return string
	 */
	public function getOriginalText()
	{
		return $this->original_text;
	}

	/**
	 * Parses the text one time for any templating strings setup similar to {{ parse code }}
	 *
	 * Usage:
	 * \\ (double-bar) to indicate options and to use the first one that works. If there are multiple terms then the first
	 * one that can be successfully found will be shown
	 *
	 * ClassName.method() to indicate a a class with method to call on an object. This approach will load the active model
	 * with TC_activeModelWithClassName(). No IDs can be provided for models.
	 *
	 * TMm_PagesTemplateSetting.setting_name to indicate a pages template setting option. These can be found in the Pages module.
	 *
	 * module_folder.setting_name to pull a variable from a module setting form. Every module can have module settings.
	 *
	 * @return void
	 * @see TMm_PagesTemplateSetting
	 * @see TSv_ModuleSettingsForm
	 * @see TSm_Module::variable()
	 * @see TC_activeModelWithClassName()
	 *
	 */
	public function parseTemplating() : void
	{
		if(!$this->template_has_been_parsed)
		{
			// Find all the strings that have {{something}} and parse them

			$pattern = '/{{(.*?)}}/';
			$matches = [];
			if(!is_null($this->text))
			{
				preg_match_all($pattern, $this->text, $matches);
				
			}
			
			// only bother there is at least one
			if(count($matches) > 0 && sizeof($matches[1]) > 0)
			{
				// loop through each one and parse/replace
				foreach($matches[1] as $match)
				{
					//$is_cat = strpos($match,'cat') > 0;
					$original_string = $match;
					$match = trim($match);

					// Note we can't trim the match be
					// Break up each one by the OR code to render options
					$parse_options = explode('||',$match);
					$parse_option_used = false;
					foreach($parse_options as $match_option)
					{
						// trim just in case
						// de-convert special characters
						$match_option = trim(htmlspecialchars_decode($match_option));

						// If we haven't found one yet, try the next option
						// Often there is only one option
						if(!$parse_option_used)
						{

							// Option 1 : Plain Text String
							// Starts and ends with a double-quote "
							if(substr($match_option, 0,1) == '"' && substr($match_option, -1) == '"')
							{
								// REPLACE THE TEXT
								$match_option = htmlspecialchars(trim($match_option, '"'));
								$this->text = str_ireplace("{{" . $original_string  . "}}", $match_option, $this->text);
								$parse_option_used = true;

							}
							elseif(strpos($match_option,'.') > 0)
							{

								$code_parts = explode('.', $match_option);

								$class_name = trim($code_parts[0]);
								$method_name = trim($code_parts[1]);

								$ds = DIRECTORY_SEPARATOR;
								$possible_folder = $_SERVER['DOCUMENT_ROOT'].$ds.'admin'.$ds.$class_name;

								// SPECIAL CASE – Page Template Settings
								if($class_name == 'pages_template_setting')
								{
									$setting = TMm_PagesTemplateSetting::init($method_name);
									if($setting instanceof TMm_PagesTemplateSetting)
									{
										
										$this->text = str_ireplace(
											"{{" . $original_string  . "}}",
											$setting->value(),
										    $this->text);
										$parse_option_used = true;
									}
								}

								// SPECIAL CASE – Module Settings
								elseif(is_dir($possible_folder))
								{
									/** @var TSm_Module $module */
									$module = TSm_Module::init($class_name);

									if($module->variableExists($method_name))
									{
										$this->text = str_ireplace("{{" . $original_string  . "}}", $module->variable($method_name), $this->text);
										$parse_option_used = true;
									}
									elseif(method_exists($module, $method_name))
									{
										$new_value = $module->$method_name();
										$this->text = str_ireplace("{{" . $original_string  . "}}", $new_value, $this->text);
										$parse_option_used = true;
										
									}
									
									
									
								}

								// If we found a class name and a method, we call that method and
								// replace the value
								elseif ($class = TC_activeModelWithClassName($class_name))
								{
									if (method_exists($class, $method_name))
									{
										$result = call_user_func(array($class, $method_name));

										// REPLACE THE TEXT
										$this->text = str_ireplace("{{" . $original_string  . "}}", $result, $this->text);
										$parse_option_used = true;
									}
								}
							}
						}
					}



				}
			}

			$this->template_has_been_parsed = true;
		}
	}

	/**
	 * Evaluates a condition string as a boolean value
	 * @return bool
	 */
	public function evaluateTemplateConditionAsBoolean() : bool
	{
		$condition_values = array();
		// can't call text(). will parse template values
		$original_condition = str_replace(array('{{','}}',''), '', $this->text);
		//$conditions = explode(' AND ', $condition);
		$conditions = preg_split('/ AND | OR /',$original_condition,-1, PREG_SPLIT_NO_EMPTY);
		
		foreach($conditions as $condition)
		{


			$negate = false; // flag to track if we're negating the value

			@list($class_name, $method_name) = explode('.', $condition);
			$class_name = trim($class_name);
			$method_name = trim(str_ireplace('()', '', $method_name));

			// CHECK FOR NOT or ! AND NEGATE IF NECESSARY THEN TRIM
			if(substr(strtoupper($class_name), 0, 3) == 'NOT')
			{
				$negate = true;
				$class_name = trim(substr($class_name, 3));
			}
			elseif(substr($class_name, 0, 1) == '!')
			{
				$negate = true;
				$class_name = trim(substr($class_name, 1));

			}
			
			$current_condition_value = null;
			
			// OPTION 2 : CURRENT USER SPECIAL CASE
			if($class_name == 'current-user' || $class_name == 'current_user')
			{
				$current_user = TC_currentUser();
				if($current_user)
				{
					$current_condition_value = $current_user->$method_name();
				}
				else // hard exit, asked for current user and they don't exist
				{
					$current_condition_value = false;
					
				}
			}
			
			// OPTION 3 : CLASS NAME
			elseif(class_exists($class_name))
			{
				// ensure we're using the loaded class and not the one provided
				$class_name = $class_name::classNameForInit($class_name);
				// We have a method to call, must be correct context
				if(method_exists($class_name, $method_name))
				{
					try
					{
						$method_checker = new ReflectionMethod($class_name, $method_name);

						if($method_checker->isStatic())
						{
							// Call the static method
							$current_condition_value = call_user_func(array($class_name, $method_name));
						}

						// Try and init the active model with the provided class name
						elseif($active_model = TC_activeModelWithClassName($class_name))
						{
							$current_condition_value = $active_model->$method_name();

						}
						else
						{
							$current_condition_value = false;
							$this->addConsoleWarning('Active class of "' . $class_name . '" not found .');

						}
					}
					catch(ReflectionException $e)
					{
						$current_condition_value = false;

					}


				}
				else // We have a class and a method, but it doesn't exist
				{
					// Hard Fail if we got here. Throw a console error
					$current_condition_value = false;
					$this->addConsoleError('Method "' . $method_name . '" not found in class "' . $class_name
										   . '".');

				}


			}

			// OPTION 4 : METHOD NAME
			else
			{
				$method_name = trim($method_name);
				/** @var TSm_Module $module */
				$module = TSm_Module::init($class_name);
				if($module)
				{
					if($module->variableExists($method_name))
					{
						$current_condition_value = $module->variable($method_name);
					}
					else
					{
						$this->addConsoleWarning('Module Setting with name "' . $method_name . '" not found.');
					}
				}
				else
				{
					$this->addConsoleWarning('Module with folder "' . $class_name . '" not found.');
				}
			}

			// IF we have a value, deal with potential negation and add to the list
			if(!is_null($current_condition_value))
			{
				if($negate)
				{
					$current_condition_value = !$current_condition_value;
				}

				$condition_values[$condition] = $current_condition_value;

			}

		}

		if(count($condition_values ) > 0 )
		{
			$eval_string = $original_condition;
			foreach($condition_values as $condition_string => $value)
			{
				$eval_string = str_replace($condition_string, ($value ? '1' : '0'), $eval_string);
			}
			
			// Combine the various responses into something that can be evaluated to a boolean
			$eval_string = str_replace(' AND ', ' && ', $eval_string);
			$eval_string = str_replace(' OR ', ' || ', $eval_string);
			$response = 0;
			eval('$response = '.$eval_string.';');
			return $response;
		}


		// failsafe
		return false;
	}


	/**
	 * Encodes the string with unicode values
	 *
	 * @return string
	 */
	public function encode()
	{
		$result = '';
		for($i=0 ; $i<strlen($this->text) ; $i++)
		{
			$result .= '&#'.ord(substr($this->text,$i,1)).';';
		}
		$this->text = $result;
		return $this->text;
	}
	
	/**
	 * Checks if the text is validly formatted as an email
	 * @return bool
	 */
	public function checkIfValidEmailFormat()
	{
		if (!filter_var($this->text, FILTER_VALIDATE_EMAIL)) 
		{
			return false;
		}	
		return true;
	}

	//////////////////////////////////////////////////////
	//
	// HASHING
	//
	// Methods related to hashing
	//
	//////////////////////////////////////////////////////

	/**
	 * Hashes the text with sha256 and an optional random salt
	 *
	 * @param null|string $salt (Optional) Default false. The salt that should be used. A value of false will randomly generate a salt.
	 * @return string
	 */
	public function hash(?string $salt = null)
	{
		$password = $this->original_text;
		if(!$salt)
		{
			$salt = hash('sha256', uniqid(mt_rand(), true).strtolower($password));
		}
			
		$hash = $salt.$password;
		for ( $i = 0; $i < 100000; $i ++ )
		{
		    $hash = hash('sha256', $hash);
		}
		$this->text = $salt.$hash; // 128 character hash. first 64 are the salt, next 64 are the hash
		
		return $this->text;
	}
	
	/**
	 * Checks if the text matches the existing hash provided.
	 * @param string $existing_hash
	 * @return bool
	 */
	public function verifyMatchesHash($existing_hash)
	{
		$salt = substr($existing_hash, 0, 64);
		$new_hash = $this->hash($salt);
			
		return $new_hash == $existing_hash;
						
	}
	
	
	
	
	/**
	 * Strips any HTML tags and trims the content to a specified length. If the content was actually trimmed, an ellipsis is inserted at the end.
	 * @param int $length (Optional) Default 150. The number of characters to trim to
	 * @param bool $show_ellipsis (Optional) Default true. Indicates if the ellipsis character should be shown
	 * @return string
	 */
	public function trimExcess($length = 150, $show_ellipsis = true)
	{
		// strip any tags
		$this->plainText();
		
		// check if trimming necessary
		if(strlen($this->text) > $length)
		{
			$this->text = substr($this->text,0,$length).($show_ellipsis ? '&hellip;' : '');
		}
		
		return $this->text;
	}
	
	/**
	 * Strips any HTML tags and returns the plain text
	 *
	 * @return string
	 */
	public function plainText()
	{
		// strip any tags
		$this->text = strip_tags($this->text);
		
		return $this->text;
	}
	
	
	
	/**
	 * Converts any url starting with ftp, http or https into a clickable link. It also converts any email address into a clickable link
	 */
	public function clickableLinks()
	{
		$this->text =  preg_replace(
	     array(
	       '/(?(?=<a[^>]*>.+<\/a>)
	             (?:<a[^>]*>.+<\/a>)
	             |
	             ([^="\']?)((?:https?|ftp|bf2|):\/\/[^<> \n\r]+)
	         )/iex',
	       '/<a([^>]*)target="?[^"\']+"?/i',
	       '/<a([^>]+)>/i',
	       '/(^|\s)(www.[^<> \n\r]+)/iex',
	       '/(([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-]+)
	       (\\.[A-Za-z0-9-]+)*)/iex'
	       ),
	     array(
	       "stripslashes((strlen('\\2')>0?'\\1<a href=\"\\2\">\\2</a>\\3':'\\0'))",
	       '<a\\1',
	       '<a\\1 target="_blank">',
	       "stripslashes((strlen('\\2')>0?'\\1<a href=\"http://\\2\">\\2</a>\\3':'\\0'))",
	       "stripslashes((strlen('\\2')>0?'<a href=\"mailto:\\0\">\\0</a>':'\\0'))"
	       ),
	       $this->text); 
	 	// REGEX OBTAINED FROM STACK OVERFLOW http://stackoverflow.com/questions/1188129/replace-urls-in-text-with-html-links/. It's one of the lower answers.
	}
	
	/**
	 * Converts the entire text item to a cut down version of html. All tags are stripped and then any links are wrapped
	 * in anchors and line breaks are turned into break tags.
	 * @param bool $convert_links (Optional) Default true. Flag if links should be converted to anchor tags
	 * @return string
	 */
	public function toBasicHTML($convert_links = true)
	{
		$this->text = strip_tags($this->text); // get rid of any tags that they may have entered
		if($convert_links)
		{	
			$this->clickableLinks(); // make links clickable
		}
		$this->text = nl2br($this->text); // convert line breaks to break tags
		
		return $this->text;
	
	}
	
	/**
	 * Remove any bad HTML items to only result in basic stuff that is deamed valuable.
	 * @return string
	 */
	public function cleanHTML()
	{
		$content = $this->text;
		$allowed_tags = '<p><a><img><ul><li><ol><div><table><tbody><tr><td><th><span><em><i><b><strong>';
		$content = strip_tags($content, $allowed_tags);
		// remove any inline styles
		$content = preg_replace('/\s*(style=".*?")/i', '', $content);
		
		// change divs to paragraphs
		$content = preg_replace('/<div/i', '<p', $content);
		$content = preg_replace('/<\/div/i', '</p', $content);
		
		// avoid extra spaces
		$content = preg_replace('/&nbsp;/i', ' ', $content);
		
		// remove microsoft MsONormal classes
		$content = preg_replace('/\s*class="MsoNormal"/i', '', $content);
				
		// remove empty pargraphs
		$content = preg_replace('/<p>\s*<\/p>/i', '', $content);
		
		
		
		return $content;
	}
	

	/**
	 * Generate a random password made entirely of numbers and letters with at least one zero
	 * @param int $length (Optional) Default 8.
	 * @return string
	 */
	public static function generatePassword($length = 8)
	{
		$password = "";
	
		// define possible characters
		$possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
		
		// set up a counter
		$i = 0; 
		
		// add random characters to $password until $length is reached
		while ($i < $length-1) 
		{ 
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		
			// we don't want this character if it's already in the password
			if (!strstr($password, $char)) 
			{ 
				$password .= $char;
				$i++;
			}
		}
		// Add a number to the end
		$numbers = "0123456789"; 
		$char = substr($numbers, mt_rand(0, strlen($numbers)-1), 1);
		$password .= $char; // append a zero so that there is always a number
		
		return $password;
	}
	
	/**
	 * Function to handle whether a word should be singular or plural. It sounds strange but it comes up ALL the time and
	 * it really should be encapsulated. The default case involves the standard 's' for plural or '' for singular.
	 * The two forms can be overridden.
	 * @param int $num The number of items
	 * @param string $singular (Optional) Default ''. The singular form to be returned
	 * @param string $plural (OPtional) Default 's'. The plural form
	 * @return string
	 */
	public static function getPlural($num, $singular = '', $plural = 's')
	{
		if($num == 1)
		{
			return $singular;
		}
		else
		{
			return $plural;
		}
	}
	
	/**
	 * Performs a check to see if the text is formatted to be an email address
	 * @param bool $ignore_empty (Optional) Default false.
	 * @return bool
	 */
	public function checkValidEmail($ignore_empty = false)
	{
		$email = $this->text;
		
		
		if($email == '' && $ignore_empty) // blank is acceptable. If required, that validation will handle blanks
		{
			return true;
		}
		
		
		
		$isValid = true;
		$atIndex = strrpos($email, "@");
		if (is_bool($atIndex) && !$atIndex)
		{
			$isValid = false;
		}
		else
		{
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			if ($localLen < 1 || $localLen > 64)
			{
				// local part length exceeded
				$isValid = false;
			}
			else if ($domainLen < 1 || $domainLen > 255)
			{
				// domain part length exceeded
				$isValid = false;
			}
			else if ($local[0] == '.' || $local[$localLen-1] == '.')
			{
				// local part starts or ends with '.'
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $local))
			{
				// local part has two consecutive dots
				$isValid = false;
			}
			else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
			{
				// character not valid in domain part
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $domain))
			{
				// domain part has two consecutive dots
				$isValid = false;
			}
			else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
			{
				// character not valid in local part unless 
				// local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local)))
				{
					$isValid = false;
				}
			}
			
		}
		return $isValid;
	}

	/**
	 * Returns true if $haystack starts with the $needle string
	 * @param string $haystack
	 * @param string $needle
	 * @param bool $ignore_case (Optional) Default true.
	 * @return bool
	 */
	public static function startsWith($haystack, $needle, $ignore_case = true )
	{
	    
	    if ( $needle === '' ) 
	    {
	    	return false;
	    }

	    if ( $ignore_case === true ) 
	    {
	    	return strripos($haystack, $needle, -strlen($haystack)) !== false;
	    } 
	    else 
	    {
	    	return strrpos($haystack, $needle, -strlen($haystack)) !== false;
	    }

	}
	

	/**
	 * Returns true if $haystack ends with the $needle string
	 * @param string $haystack
	 * @param string $needle
	 * @param bool $ignore_case (Optional) Default true.
	 * @return bool
	 */
	public static function endsWith($haystack, $needle, $ignore_case = true )
	{
		if ( $needle === '' ) {
			return false;
		}

		$offset = strlen($haystack) - strlen($needle);

		if ( $ignore_case === true ) 
		{
			return ($offset >= 0 && stripos($haystack, $needle, $offset) !== false);
		} 
		else 
		{
			return ($offset >= 0 && strpos($haystack, $needle, $offset) !== false);
		}

	}

	/**
	 * Sets up the magic method to allow returning the object ot get what we really want. The string.
	 * @return string
	 */
	public function __toString ()
	{
		return $this->text();
	}
	
	//////////////////////////////////////////////////////
	//
	// PHONE NUMBERS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Cleans a  phone number
	 * @param string $number
	 * @return string
	 */
	public static function cleanPhoneNumber($number)
	{
		// Remove any special characters we can find
		$number = str_ireplace(array(' ','(',')','-','.',','),'' , trim($number));
		
		// Deal with a 1 or +1 being added
		$number = ltrim($number,"1");
		$number = str_ireplace('+1','',$number);
		return $number;
	}
	
	/**
	 * Validates a phone number to ensure it at least "seems" valid
	 * @param string $number
	 * @return bool
	 */
	public static function validatePhoneNumber($number)
	{
		$number = static::cleanPhoneNumber($number); // strip away the junk
		
		$length = strlen($number);
		if($length < 10
			|| $length == 11 // likely typo
			|| $length == 12 // likely typo
		)
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * Cleans and formats the phone number for consistent output using (234) 567-8910
	 * @param string $number
	 * @param string $missing (Default "Missing"). Indicates what is said if the number is missing
	 * @return string
	 */
	public static function formatPhoneNumber($number, $missing = "")
	{
		
		$n = static::cleanPhoneNumber($number);
		
		if(strlen($n) > 0)
		{
			// fix manitoba number with no area code
			if(strlen($n) == 7)
			{
				$n = '204'.$n;
			}
			
			// International numbers
			if(substr($n, 0,1) == '+')
			{
				// +234 235 876543...
				$n = substr($n,0,4).' '.substr($n,4,3).' '.substr($n,7,3).' '.substr($n,10);
			}
			else
			{
				$n = '('.substr($n,0,3).') '.substr($n,3,3).'-'.substr($n,6,4).' '.substr($n,10);;
				
				// Add the space back in before the extension
				$n = str_ireplace('ext', 'ext ', $n);
			}
		}
		else
		{
			$n = $missing;
		}
		
		return $n;
	}
	
	/**
	 * @param string $color A color string.
	 * @return bool True if the given color is a string in CSS hex, rgb, or rgba format.
	 */
	public static function isValidColor(string $color) : bool
	{
		$is_hex =
			$color[0]==='#'
			&& ctype_xdigit(substr($color, 1))
			&& (strlen($color)==7 || strlen($color)==4);
		
		// I sincerely apologize for the assault on the eyes that these regexes are
		
		$rgb_regex_pass =
			(bool) preg_match("/^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/", $color, $rgb_match);
		$is_rgb =
			$rgb_regex_pass
			&& ((0 <= $rgb_match[1]) && ($rgb_match[1] <= 255))
			&& ((0 <= $rgb_match[2]) && ($rgb_match[2] <= 255))
			&& ((0 <= $rgb_match[3]) && ($rgb_match[3] <= 255));
		
		$rgba_regex_pass =
			(bool) preg_match("/^rgba\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3}),\s*(\d*(?:\.\d+)?)\)$/", $color, $rgba_match);
		$is_rgba =
			$rgba_regex_pass
			&& ((0 <= $rgba_match[1]) && ($rgba_match[1] <= 255))
			&& ((0 <= $rgba_match[2]) && ($rgba_match[2] <= 255))
			&& ((0 <= $rgba_match[3]) && ($rgba_match[3] <= 255))
			&& ((0 <= $rgba_match[4]) && ($rgba_match[4] <= 1));
		
		return $is_hex || $is_rgb || $is_rgba;
	}
	
}

?>