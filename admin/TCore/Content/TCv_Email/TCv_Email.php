<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\OAuth;

use League\OAuth2\Client\Provider\Google;

use Pelago\Emogrifier\CssInliner;

/**
 * Class TCv_Email
 */
class TCv_Email extends TCv_View
{
	/** @var bool|PHPMailer $mailer */
	protected $mailer = false;

	protected $user = false;
	protected $show_in_console = false;

	/** @var bool|TCv_View $styled_view  */
	protected $styled_view = false;
	
	protected $css = array(); // [array] = The css for this email
	protected $embed_css_files = true;
	
	protected $html = false; // [string] = The html is only calculated once
	
	protected $email_testing_target = false;

	protected $http_prefix = '';

	protected $boundary = '----=_Boundary_';

	const FORCE_SEND = true;
	/**
	 * TCv_Email constructor.
	 * @param bool|TMm_User $user
	 */
	public function __construct($user)
	{
		$this->mailer = new PHPMailer();
		$this->mailer->isHTML(true); // Set email format to HTML
		$this->mailer->CharSet = 'UTF-8';
		$this->configureForSMTP();

		//$this->headers = array();
		$this->http_prefix = $this->fullDomainName();

		$sender_name = $_SERVER['HTTP_HOST'];
		if(TC_getModuleConfig('system', 'password_reset_name'))
		{
			$sender_name = TC_getModuleConfig('system', 'password_reset_name');
		}
		
		$sender_email = 'admin@'.$_SERVER['HTTP_HOST'];
		if(TC_getModuleConfig('system', 'password_reset_email'))
		{
			$sender_email = TC_getModuleConfig('system', 'password_reset_email');
		}
		
		$this->setSender($sender_name, $sender_email);

		if($user instanceof TMm_User)
		{
			$this->user = $user;
			$this->addRecipient($user);
		
	
		}
		
		parent::__construct('email');

		// Email Frame View
		$email_frame_view = 'TCv_View';
		$system_frame_view_class_name = TC_getModuleConfig('system', 'email_frame_view');
		if($system_frame_view_class_name != '')
		{
			$email_frame_view = $system_frame_view_class_name;
		}
		
		
		$this->styled_view = new $email_frame_view();
		$this->addClass('email_frame_view');

		if($reply_to = TC_getModuleConfig('system', 'email_reply_to_address'))
		{
			$this->addReplyTo($reply_to,$sender_name);
		}




	}

	/**
	 * Sets the "Reply To" for this email, which will adjust where the email comes from.
	 *
	 * This is a passthrough function for PHPMailer's addReplyTo
	 * @param string $email
	 * @param string $name
	 * @see PHPMailer::addReplyTo()
	 */
	public function addReplyTo($email, $name)
	{
		$this->mailer->addReplyTo($email, $name);
	}

	/**
	 * Turns on testing mode for the email. Any emails will be sent to the address provided and any previous
	 * recipients will be removed.
	 * @param string $email
	 */
	public function enableTestingModeWithEmail($email)
	{
		$this->email_testing_target = $email;
	}

	public function enableVerboseDebugging()
	{
		$this->mailer->SMTPDebug = 2; // Enable verbose debug output
	}

	/**
	 * Pulls the SMTP settings from the System module settings and configures the mailer appropriately.
	 *
	 * --- GMAIL ---
	 * You will need to either allow "less secure apps" or use the OAuth2
	 * Less Secure Apps : My Account > Security > Toggle switch at the bottom
	 * OAuth2 : https://github.com/PHPMailer/PHPMailer/wiki/Using-Gmail-with-XOAUTH2
	 */
	protected function configureForSMTP()
	{
		if(TC_getModuleConfig('system', 'use_smtp'))
		{
			//Server settings
			$this->mailer->isSMTP(); // Set mailer to use SMTP
			$this->mailer->Host = TC_getModuleConfig('system', 'smtp_host'); // Specify main and backup SMTP servers
			$this->mailer->SMTPAuth = true; // Enable SMTP authentication

			//$this->mailer->SMTPDebug = 2;

			$auth_type = TC_getModuleConfig('system', 'smtp_auth_type');

			if($auth_type == 'name_pass')
			{
				$this->mailer->Username = TC_getModuleConfig('system', 'smtp_username');
				$this->mailer->Password = TC_getModuleConfig('system', 'smtp_password');
			}
			else
			{
				$this->mailer->AuthType = 'XOAUTH2';
			
				$client_id = TC_getModuleConfig('system', 'smtp_oauth_client_id');
				$client_secret = TC_getModuleConfig('system', 'smtp_oauth_client_secret');

				$provider = false;
				if($auth_type == 'oauth2_google')
				{
					$provider = new Google(
						[
							'clientId' => $client_id,
							'clientSecret' => $client_secret
						]
					);
				}

				if(!$provider)
				{
					TC_triggerError('No Provider Found');
				}

				$this->mailer->setOAuth(
					new OAuth(
						[
							'provider' => $provider,
							'clientId' => $client_id,
							'clientSecret' => $client_secret,
							'refreshToken' => TC_getModuleConfig('system', 'smtp_oauth_refresh_token'),
							'userName' => TC_getModuleConfig('system', 'smtp_username'),
						]
					)
				);

			}

			// Connection type and Port
			$this->mailer->SMTPSecure = TC_getModuleConfig('system', 'smtp_connection_mode');
			$this->mailer->Port = TC_getModuleConfig('system', 'smtp_port');

		}
	}

	/**
	 * Override of the normal attachView to ensure all the views are attached to the inner "styled_view" object which
	 * will hold them all.
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return void
	 */
	public function attachView($view, $prepend = false)
	{
		$this->styled_view->addText("\n");
		$this->styled_view->attachView($view);
	}


	/**
	 * Override of the normal addText() to ensure all the views are attached to the inner "styled_view" object which
	 * will hold them all.
	 * @param string $text
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 * @return void
	 */
	public function addText($text, $disable_template_parsing = false)
	{
		$this->styled_view->addText($text, $disable_template_parsing);
	}

	/**
	 * Takes a string of emails and separates them, removing blanks and returning an array of emails.
	 * @param string $email_string
	 * @return string[]
	 */
	protected function separateEmails(string $email_string) : array
	{
		$emails = [];
		$parts = explode(',', $email_string);
		foreach($parts as $index => $email)
		{
			if(trim($email) != '')
			{
				$emails[] = $email;
			}
		}

		return $emails;
	}

	/**
	 * Adds a recipient to the email as either a string or a User object
	 * @param string|TMm_User $email_or_user
	 */
	public function addRecipient($email_or_user)
	{
		if($email_or_user instanceof TMm_User)
		{
			if($this->user === false)
			{
				$this->user = $email_or_user;
			}

			$email_string = $email_or_user->email();
			if($email_string != '')
			{
				$success = $this->mailer->addAddress($email_string, $email_or_user->fullName());
				$this->handleAddEmailResponse($success, $email_string);
			}
			
		}
		else
		{
			foreach($this->separateEmails($email_or_user) as $email_string)
			{
				$success = $this->mailer->addAddress($email_string);
				$this->handleAddEmailResponse($success, $email_string);
			}
		}
	}
	
	/**
	 * Adds a recipient as a CC on the email
	 * @param string|TMm_User $email_or_user
	 */
	public function addRecipientCC($email_or_user)
	{
		if($email_or_user instanceof TMm_User)
		{
			$email_string = $email_or_user->email();
			if($email_string != '')
			{
				$success = $this->mailer->addCC($email_string);
				$this->handleAddEmailResponse($success, $email_string);
				
			}
		}
		else
		{
			foreach($this->separateEmails($email_or_user) as $email_string)
			{
				$success = $this->mailer->addCC($email_string);
				$this->handleAddEmailResponse($success, $email_string);
			}
		}
	}
			
	/**
	 * Adds a BCC recipient
	 * @param string|TMm_User $email_or_user
	 */
	public function addRecipientBCC($email_or_user)
	{
		if($email_or_user instanceof TMm_User)
		{
			if($email_or_user->email() != '')
			{
				$email_string = $email_or_user->email();
				$success = $this->mailer->addBCC($email_string);
				$this->handleAddEmailResponse($success, $email_string);
				
			}
		}
		else
		{
			foreach($this->separateEmails($email_or_user) as $email_string)
			{
				$success = $this->mailer->addBCC($email_string);
				$this->handleAddEmailResponse($success, $email_string);
			}
		}
	}
	
	/**
	 * Internal helper method to check for a failure on adding an email and put something in the console.
	 * @param bool $success
	 * @param string $email
	 */
	protected function handleAddEmailResponse($success, $email)
	{
		if(!$success)
		{
			$this->addConsoleWarning('Email Error: ' . $email . ' bad formatting or duplicate');
		}
	}
	
	
	
	/**
	 * Removes all recipients from the email
	 */
	public function clearRecipients()
	{
		$this->mailer->clearAddresses();
		$this->mailer->clearCCs();
		$this->mailer->clearBCCs();

	}
	
	/**
	 * Clears all the reply-to email addresses
	 */
	public function clearReplyTos()
	{
		$this->mailer->clearReplyTos();
	}
			
	/**
	 * Sets the subject of the email
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->mailer->Subject = $subject;
	}
			
	/**
	 * Sets the sender of the email with a name and email.
	 * @param string $name
	 * @param string $email
	 */
	public function setSender($name, $email)
	{
		$email = trim($email);
		
		// Detect comma-separated
		if(strpos($email,',') > 0)
		{
			$parts = explode(',',$email);
			$email = trim($parts[0]);
		}

		// Detect spaces
		if(strpos($email,' ') > 0)
		{
			$parts = explode(' ',$email);
			$email = trim($parts[0]);
		}
		$this->mailer->setFrom($email, $name);
	}


	//////////////////////////////////////////////////////
	//
	// CSS
	//
	// Functions related to CSS for HTML emails
	//
	//////////////////////////////////////////////////////


	/**
	 * Turns on the functionality which embeds CSS file content into the email
	 * @param bool $embed
	 */
	public function setEmbedCSSFiles($embed)
	{
		$this->embed_css_files = $embed;
		
	}
	
	/**
	 * Extends the existing functionality to parse the file and attach it to the email.
	 * @param string $id
	 * @param string $url
	 * @param bool $media
	 */
	public function addCSSFile($id, $url, $media = false, bool $defer_load = false)
	{
		if($this->embed_css_files)
		{
			$this->convertLinkedCSSToEmbedded($url);
		}
		else
		{
			parent::addCSSFile($id, $url);	
		}
	}

	


	/**
	 * Adds CSS to the email, which saves it as the text rather than linking to the file
	 * @param string $css
	 */
	public function addCSS($css)
	{
		$this->css[] = $css;
	}
	
//	/**
//	 * Works through the attached views and adds the CSS to the email
//	 */
//	public function addCSSFromAttachedViews()
//	{
//		foreach($this->cssFiles() as $id => $css_data)
//		{
//			if(is_array($css_data))
//			{
//				$url = $css_data['url'];
//			}
//			else
//			{
//				$url = $css_data;
//			}
//
//			$this->processCSSFile($url);
//
//		}
//
//		foreach($this->cssRuntimes() as $id => $css_data)
//		{
//			$this->addCSS($css_data);
//		}
//	}

	/**
	 * Extends the functionality of this method to also handle any relative links
	 * @return string
	 */
	public function htmlForAttachedViews()
	{
		$html = parent::htmlForAttachedViews();

		$html = str_ireplace('href="/', 'href="'.$this->http_prefix.'/', $html);
		$html = str_ireplace('src="/', 'src="'.$this->http_prefix.'/', $html);
		$html = str_ireplace('</tr>', "</tr>\n", $html);
		$html = str_ireplace('<div>', "\n<div>", $html); // create sufficent line breaks

		return $html;
	}

	/**
	 * A view that shows the email is a test and it shows who the recipients are
	 * @return TCv_View
	 */
	public function testingView()
	{
		$testing_block = new TCv_View('testing_info_block');
		$testing_block->setAttribute('style','border:4px solid #900; padding:10px; margin-top:10px;');
		$testing_block->addText('<h2>Testing</h2>');
		//$testing_block->addText('<p>Recipients: '.implode(', ', $this->recipients).'</p>');
		//$testing_block->addText('<p>Cc: '.implode(', ', $this->recipients_cc).'</p>');
		//$testing_block->addText('<p>Bcc: '.implode(', ', $this->recipients_bcc).'</p>');
		
		return $testing_block;
	}
	
	/**
	 * Takes the styled view and builds out the html email
	 * @return bool|string
	 */
	public function html()
	{
		if(!$this->render_called)
		{
			$this->render();
			$this->render_called = true;
		}
		
		$this->styled_view->attachView($this->emailFooter());
		// Add the main view's styles to this email styled view. Ensuring CSS calls work as expected
		$this->styled_view->addClassArray($this->classes());
		parent::attachView($this->styled_view);
		
		$this->addClassCSSFile('TCv_Email');
		if($this->html === false)
		{
			// Attaches it all as css calls
			
			$html = new TCv_View();
			$html->setTag('html');
			
			$head = new TCv_View();
			$head->setTag('head');
				
			$body = new TCv_View();
			$body->setTag('body');
				
			//$head->addText('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
			$head->addText("\n\t".'<style type="text/css">');
			$head->addText($this->embeddedCSS());
			$head->addText("\n\t".'</style>');
			
			
			$body->addText("\n\t".'<table id="email_main_table" class="outer" cellpadding="0" cellspacing="0" border="0" >');
			$body->addText("\n\t\t".'<tbody><tr><td>');
			
			// ADD CONTENT
			$body->addText($this->htmlForAttachedViews());
			
			if($this->email_testing_target)
			{
				
				$body->attachView($this->testingView());
			}
			
			$body->addText("\n\t\t".'</td></tr></tbody>');
			$body->addText("\n\t".'</table>');
			
			$html->attachView($head);
			$html->attachView($body);
			
			
			$this->html = '<!DOCTYPE html>'.$html->html();
		}
		
		
		return $this->html;
	}
	
	/**
	 * Adds a header line to the email
	 * @param string $name
	 * @param string $value
	 * @deprecated No longer used since PHPMailer handles it all
	 */
	public function addHeader($name, $value)
	{
		// NO ACTION
		//$this->headers[$name] = $value;
	}
	
	/**
	 * Returns a summary view of the email, useful for debugging
	 * @return TCv_View
	 */
	public function summary()
	{
		$summary = new TCv_View();
		//$summary->addText('Subject: '.$this->subject);
		
		//$summary->addText('<br />Sender: '. $this->sender_name.' "'.$this->sender_email.'"');
		//$summary->addText('<br />Recipients: '.implode(',', $this->recipients));
		//$summary->addText('<br />CC: '.implode(',', $this->recipients_cc));
		//$summary->addText('<br />BCC: '.implode(',', $this->recipients_bcc));
		//$summary->addText('<br />From:',$this->headers['From']);
		//$summary->addText('<br />Content: <br /><br />'.htmlspecialchars($this->html()));
		return $summary;
	}

	/**
	 * Sets the value to indicate if it should show in the console
	 * @param bool $value (Optional) Default = true
	 */
	public function setShowInConsole($value = true)
	{
		$this->show_in_console = $value;
	}
	
	/**
	 * Sends the email using PHP's mail() function
	 * @param bool $force_send (Optional) Default false. Set to true if you want this email to attempt sending,
	 * regardless of any other settings related to restricting emails.
	 */
	public function send($force_send = false)
	{
		$html = $this->html(); // process HTML
		// Convert all styles into inline styles
		// I can't believe it's 2020 and I'm typing this
		$html = CssInliner::fromHtml($html)->inlineCss()->render();
		
		// Remove the style tag entirely
		$html = preg_replace('/<style(.*?)style>/s', '',$html);
//		// handle email testing setting
		if($this->email_testing_target)
		{
			// clear recipients to only have the testing email
			$this->clearRecipients();
			$this->addRecipient($this->email_testing_target);
		}

		$this->mailer->Body = $html;
		
		// Remove the style tag altogether
		$html = preg_replace('/<style(.*?)style>/s', '',$html);
		$this->mailer->AltBody = strip_tags($html);


		if($force_send)
		{
			$emails_enabled = true;
		}
		else
		{
			$emails_enabled = TC_getConfig('email','enabled');
		}
		
		// Disabled, console warning and get out
		if(!$emails_enabled)
		{
			$this->addConsoleWarning('Emails Disabled: '.$this->mailer->Subject);
			return;
		}
		
		try
		{
			$success = $this->mailer->send();
			
			if($this->show_in_console)
			{
				$console_values = array(
					'EMAIL SENT' => '',
					'Subject' => $this->mailer->Subject,
				);
				
				$addresses = $this->mailer->getToAddresses();
				if(is_array($addresses) && count($addresses) > 0)
				{
					$console_values['Recipients'] = array();
					foreach($addresses as $values)
					{
						$console_values['Recipients'][] = $values[0];
					}
				}
				
				$addresses = $this->mailer->getCcAddresses();
				if(is_array($addresses) && count($addresses) > 0)
				{
					$console_values['CC'] = array();
					foreach($addresses as $values)
					{
						$console_values['CC'][] = $values[0];
					}
				}
				
				$addresses = $this->mailer->getBccAddresses();
				if(is_array($addresses) && count($addresses) > 0)
				{
					$console_values['BCC'] = array();
					foreach($addresses as $values)
					{
						$console_values['BCC'][] = $values[0];
					}
				}

				$console_values['http_prefix'] = $this->http_prefix;
				$this->addConsoleDebug($console_values);
			}
			
			if(!$success)
			{
				$this->addConsoleError('Sending Failed');
			}
			
		} catch(Exception $e)
		{
			//echo 'Message could not be sent.';
			$this->addConsoleError('Mail Did Not Send');
			$this->addConsoleError($this->mailer->ErrorInfo);
		}
	

	}
	
	/**
	 * A footer view attached to all emails sent by the server. This ensures that legal information is associated
	 * with every piece of communication coming from the website.
	 * @return TCv_View
	 */
	protected function emailFooter()
	{
		$email_footer = new TCv_View('email_footer');
		
		$h2 = new TCv_View();
		$h2->addClass('company_title');
		$h2->addText(TC_getModuleConfig('pages', 'website_title'));
		
		$email_footer->attachView($h2);
		
		$template_value = TMm_PagesTemplateSetting::init( 'address');
		if($template_value instanceof TMm_PagesTemplateSetting)
		{
			$address = new TCv_View();
			$address->setTag('p');
			$address->addClass('header_address');
			$address->addText($template_value->value());
			
			$template_value = TMm_PagesTemplateSetting::init( 'phone_number');
			if($template_value instanceof TMm_PagesTemplateSetting)
			{
				$address->addText('<br />'.$template_value->value());
				
			}
			$email_footer->attachView($address);
			
		}
		
		
		// WEBSITE
		$website = new TCv_Link('footer_website');
		$website->addText($_SERVER['HTTP_HOST']);
		$website->setURL($this->fullDomainName());
		$email_footer->attachView($website);
		
		return $email_footer;
	}
	
}

?>