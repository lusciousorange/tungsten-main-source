<?php

/**
 * Class TCv_FilePreview
 */
class TCv_FilePreview extends TCv_View
{
	protected $file = false; // [TCm_File] = The file being provided to this view

	protected $show_close_link = false;

	/**
	 * TCv_FilePreview constructor.
	 * @param TCm_File $file
	 */
	public function __construct($file)
	{
		parent::__construct();
		$this->file = $file;
	}

	public function showCloseLink()
	{
		$this->show_close_link = true;
	}
	
	/**
	 * The html for the view which will show a preview of the file by default
	 * @return string
	 */
	public function html()
	{
		if($this->file->exists())
		{
			$this->addDataValue('filename', $this->file->filename());
			$this->addDataValue('extension', $this->file->extension());
			
			$filetype = $this->file->filetype();
			if($filetype == 'image' && $this->file->publiclyVisible())
			{
				$file_view = new TCv_Image($this->file->id().'_view', $this->file);
				$file_view->setViewBox(120,120);
			}
			elseif($this->file->extension() == 'svg')
			{
				$file_view = new TCv_View();
				$file_view->addText(file_get_contents($this->file->filenameFromServerRoot()));
			}
			else
			{
				$file_view = new TCv_View();
				$file_view->setTag('i');
				if($filetype == 'file')
				{
					$file_view->addClass('fa-file');
				}
				else
				{
					$file_view->addClass('fa-file-'.$filetype.'');
				}
				
				
			}	

			$file_link = new TCv_View();
			$file_link->addClass('file_icon_link');
			$file_link->setTag('span');
			$file_link->attachView($file_view);
			$this->attachView($file_link);

			

			
			$explanation = new TCv_View();
			$explanation->addClass('file_explanation');
			
			if($this->file->publiclyVisible())
			{
				$file_link = new TCv_Link();
				$file_link->openInNewWindow();
				$file_link->setURL($this->file->filenameFromSiteRoot());
				$file_link->addText($this->file->filename());
				$explanation->attachView($file_link);
			}
			else
			{
				$explanation->addText(''.$this->file->filename()).'';
			}
			
			
			//$explanation->addText('<br />Filetype: '.$this->file->extension());
			$explanation->addText('<br />Size: '.$this->file->filesizeString());
			if($file_view instanceof TCv_Image)
			{
				$explanation->addText('<br />Dimensions: '.$file_view->actualWidth().' x '.$file_view->actualHeight());
				
				// Detect if there's crop values
				//$crop_values = $this->file->cropValues();
				if(is_array($this->file->cropValues()))
				{
					list($full_width, $full_height) = @getimagesize($this->file->filenameFromServerRoot());
				//	$this->file->
					$explanation->addText('<br />Original: '.$full_width.' x '.$full_height);
					
				}
			}
			
			
			
			
			
			$this->attachView($explanation);
			
			if($this->show_close_link)
			{
				$close_link = new TCv_Link();
				$close_link->setURL('#');
				$close_link->addClass('remove_file_button');
				$close_link->setIconClassName('fa-times');
				$close_link->addDataValue('filename', $this->file->filename());
				$this->attachView($close_link);
			}
		
		}
		else
		{
			$explanation = new TCv_View();
			$explanation->addClass('file_explanation');
			$explanation->addText('No File');
			$this->attachView($explanation);
			
		}
		

		return parent::html();
	}
	
	
}

?>