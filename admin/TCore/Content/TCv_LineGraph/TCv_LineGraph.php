<?php

/**
 * @deprecated Use TCv_Graph
 */
class TCv_LineGraph extends TCv_View
{



	/**
	 * TCv_LineGraph constructor.
	 * @param string|bool $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);

		$this->addText('TCv_LineGraph removed. Contact your developer.');
	}
	

}