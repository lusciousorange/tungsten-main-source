<?php
/**
 * This script work through all the possible items that have images that need re-caching and goes through the models
 * to process them all.
 */
require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

ini_set('memory_limit', '1024M');
set_time_limit(0);

error_reporting(E_ALL);
ini_set( 'display_errors','1');

$model_name = 'TMm_PagesContent';
$image_file_column_name = 'value';

$processor = new TCm_Model(false);

// Flag to skip processing page content each time
// Once run, probably want to focus on other models
$process_pages = true;

//////////////////////////////////////////////////////
//
// Pages Content – TMv_Photo
//
//////////////////////////////////////////////////////
if($process_pages)
{
	
	$query = "SELECT c.* FROM pages_content_variables v INNER JOIN pages_content c USING(content_id)
	           WHERE v.variable_name = 'image_file' AND view_class = 'TMv_Photo' AND v.value != ''";
	$result = $processor->DB_Prep_Exec($query);
	while($row = $result->fetch())
	{
		$content_item = new TMm_PagesContent($row);
		$renderer = $content_item->renderer();
		$view_class = $content_item->viewClass();
		
		/** @var TMt_PagesContentView $content_view */
		$content_view = new $view_class();
		$content_view->setPagesContentItem($content_item);
		$content_view->render();
		$processor->addConsoleTimer($content_item->contentCode());
		
	}

//////////////////////////////////////////////////////
//
// Pages Content – Section backgrounds
//
//////////////////////////////////////////////////////
	
	$query = "SELECT c.* FROM pages_content_variables v INNER JOIN pages_content c USING(content_id)
           WHERE v.variable_name = 'background_image_file' AND v.value != ''";
	$result = $processor->DB_Prep_Exec($query);
	while($row = $result->fetch())
	{
		$content_item = new TMm_PagesContent($row);
		$renderer = $content_item->renderer();
		$view_class = $content_item->viewClass();
		
		/** @var TMt_PagesContentView $content_view */
		$content_view = new $view_class();
		$content_view->setPagesContentItem($content_item);
		$content_view->backgroundImageView();
		$processor->addConsoleTimer($content_item->contentCode());
	}

//////////////////////////////////////////////////////
//
// Pages Renderer Content – TMv_Photo
//
//////////////////////////////////////////////////////
	
	$query = "SELECT c.* FROM pages_renderer_content_variables v INNER JOIN pages_renderer_content c USING(content_id)
           WHERE v.variable_name = 'image_file' AND view_class = 'TMv_Photo' AND v.value != ''";
	$result = $processor->DB_Prep_Exec($query);
	while($row = $result->fetch())
	{
		$content_item = new TMm_PageRendererContent($row);
		
		$renderer = $content_item->renderer();
		$view_class = $content_item->viewClass();
		
		/** @var TMt_PagesContentView $content_view */
		$content_view = new $view_class();
		$content_view->setPagesContentItem($content_item);
		$content_view->render();
		$processor->addConsoleTimer($content_item->contentCode());
		
	}


//////////////////////////////////////////////////////
//
// Pages Renderer Content – Section backgrounds
//
//////////////////////////////////////////////////////
	
	$query = "SELECT c.* FROM pages_renderer_content_variables v INNER JOIN pages_renderer_content c USING(content_id)
           WHERE v.variable_name = 'background_image_file' AND v.value != ''";
	$result = $processor->DB_Prep_Exec($query);
	while($row = $result->fetch())
	{
		$content_item = new TMm_PageRendererContent($row);
		$renderer = $content_item->renderer();
		$view_class = $content_item->viewClass();
		
		/** @var TMt_PagesContentView $content_view */
		$content_view = new $view_class();
		$content_view->setPagesContentItem($content_item);
		$content_view->backgroundImageView();
		$processor->addConsoleTimer($content_item->contentCode());
	}
	
} // END of process_pages flag

//////////////////////////////////////////////////////
//
// OTHER FILES
//
// We use an array of model names and DB columns
// Acquiring and processing them all
//
// For each of these we can provide additional widths, which will be run thorugh
// the image process to generate the cache images
// It must be another array of values for the width, type, and function_width and function_height
//
//////////////////////////////////////////////////////
$items_to_process = [
//	[
//		'model_name' => 'TMm_NewsPost',
//		'db_column' => 'header_photo',
//		'additional_widths' => [
//
//			// News admin list
//			['target_width' => 100, 'type' => 'scale'],
//
//			// Newsroom 1/3 column, with gap
//			['target_width' => 290, 'type' => 'crop', 'function_width' => 1000, 'function_height' => 500],
//
//			// Full width on page
//			['target_width' => TC_getConfig('content_width'), 'type' => 'crop', 'function_width' => 1000, 'function_height' => 500],
//
//			// Stacked breakpoint
//			['target_width' => TC_getConfig('content_single_column_width'), 'type' => 'crop', 'function_width' => 1000, 'function_height' => 500],
//
//			// Showcase
//			['target_width' => 745, 'type' => 'crop', 'function_width' => 1000, 'function_height' => 500],
//
//		],
//
//	],

];

foreach($items_to_process as $values)
{
	$model_name = $values['model_name'];
	$db_column = $values['db_column'];
	$table_name = $model_name::tableName();
	$upload_folder = $model_name::uploadFolder();
	$query = "SELECT * FROM ".$table_name." WHERE $db_column != '' ";
	$result = $processor->DB_Prep_Exec($query);
	while($row = $result->fetch())
	{
		$file = new TCm_File(
			false,
			$row[$db_column],
			$model_name::uploadFolder(),
			TCm_File::PATH_IS_FROM_SERVER_ROOT
		);
		
		if($file->exists() && $file->isImage())
		{
			$image = new TCv_Image(false, $file);
			$image->html();
			
			foreach($values['additional_widths'] as $additional_width)
			{
				if(is_array($additional_width)
					&& isset($additional_width['target_width'])
					&& isset($additional_width['type'])
				)
				{
					$target_width = $additional_width['target_width'];
					$type = $additional_width['type'];
					if($type == 'crop')
					{
						$image->crop($additional_width['function_width'], $additional_width['function_height']);
						
						// Generate the cache image
						$image->createDefaultCacheForWidth($target_width);
						
					}
					else
					{
						$scale_height = 10000;
						if(isset($additional_width['function_height']))
						{
							$scale_height = $additional_width['function_height'];
						}
						
						$image->scaleInsideBox($target_width, $scale_height);
						$image->createCacheImage();
						
					}
					
				}
			}
		}
	}
}


Print 'Processing complete';
exit();