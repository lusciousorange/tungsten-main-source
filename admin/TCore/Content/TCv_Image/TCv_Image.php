<?php

/**
 * Class TCv_Image
 *
 * A view of an image that handles caching, resizing, and saving.
 */
class TCv_Image extends TCv_File
{
	protected string $cache_filename = '';
	
	/** @var bool Indicates if the cached image should be deleted and refreshed */
	protected bool $refresh_cache = false;
	protected int $jpeg_quality = 95;
	protected string $cache_folder_path = '/cache/';
	protected string $cache_prefix = '';
	protected bool $include_full_domain_in_src = false;
	
	/** @var bool indicate if filters have been applied but not saved */
	protected bool $image_filters_not_applied = false;
	
	// FILTERS
	//protected $colorize = false;
	//protected $grayscale = false;
	protected $filters = array();
	
	protected ?TCm_File $original_file = null;
	protected ?TCm_File $cropped_file = null;
	
	protected $alt_text = '';
	
	// Original Image Properties
	// Pulled from the image, possibly null if the image can't be found
	protected ?int $actual_width = 0;
	protected ?int $actual_height = 0;
	
	// Target Image Properties, Essentially the "destination" when dealing with image creation functions
	protected int $target_width = 0; // [int] = The resized width for this image
	protected int $target_height = 0; // [int] = The resized height for this image
	protected int $target_x = 0; // [int] = The destination x value for the point
	protected int $target_y = 0; // [int] = The destination y value for the point
	
	// Source Image Properties. These are  only used for determining 
	protected int $src_width = 0; // [int] = The width that should be used for the source, normally the real full value
	protected int $src_height = 0; // [int] = The height that should be used for the source, normally the real full
	protected int $src_x = 0; // [int] = The source x value for the point
	protected int $src_y = 0; // [int] = The source y value for the point
	
	protected $scale_type = false; // [string] = The type of scaling the was performed
	protected $use_scaling = true; // [bool] = Indicates if scaling shoudl be used for this image.
	
	protected $output_image_type = false; // [string] = The output image type. If false, it will use the same type as the image provided
	
	// Indicates if this image applies the lazy loading attribute
	protected bool $lazy_loading = true;
	
	protected ?string $multiply_file_url = null;
	
	protected array $responsive_image_sizes = []; // DEPRECATED
	
	/** @var ?int The rendered column width that this image is displayed at */
	protected ?int $rendered_column_width = null;
	
	/**
	 * These are codes used when generating filenames on filters. The filters change the rendered filename, and we
	 * want abbreviated ones to avoid VERY long names.
	 * @var array|string[]
	 */
	protected static array $filter_filename_codes = [
		'grayscale'   	=> 'grs',
		'colorize'    	=> 'clz',
		'emboss'        => 'emb',
		'blur'  		=> 'blr',
		'brightness'    => 'brt',
	
	];
	
	/**
	 * Indicates if we should use Image Magick instead of GD
	 * @var bool|null
	 */
	protected static ?bool $use_image_magick = null;
	
	/**
	 * TCv_Image constructor.
	 * @param bool|string $id
	 * @param ?|TCm_File $file
	 */
	public function __construct($id = false, $file = null)
	{
		// Backwards compatibility if someone passes in false
		if($file === false) { $file = null; }
		
		parent::__construct($id, $file);
		
		$this->setTag('img'); // image tag
		
		ini_set('memory_limit', '1024M');
		
		// save the original, regardless
		$this->original_file = $this->file;
		
		if($this->file instanceof TCm_File )
		{
			$this->determineCacheFolderPath();
			
			if($this->file()->extension() == 'svg')
			{
				
				// Do Nothing
				$this->processFileProperties();
			}
			else
			{
				$this->processFileProperties();
				$this->generateCroppedImageFile();
				
			}
		}
		else
		{
			TC_triggerError('File provided to TCv_Image is empty.');
		}
		
		$this->alt_text = $this->id;
		
	}
	
	/**
	 * Returns if we should use ImageMagick instead of GD.
	 * @return bool
	 */
	public static function useImageMagick() : bool
	{
		// Not set
		if(is_null(static::$use_image_magick))
		{
			$extension_loaded = extension_loaded('imagick');
			$class_exists = class_exists('Imagick');
			static::$use_image_magick = extension_loaded('imagick') && class_exists('Imagick');
		}
		
		return static::$use_image_magick;
	}
	
	/**
	 * Processes the file properties for the image provided
	 */
	public function processFileProperties() : void
	{
		if($this->file->filename() != '')
		{
			
			if(file_exists($this->file->filenameFromServerRoot()))
			{
				// Find the width and height based on the viewBox
				if($this->file->extension() == 'svg')
				{
					$svg_code = file_get_contents($this->file->filenameFromServerRoot());
					$view_box_start = strpos($svg_code, 'viewBox="')+9;
					$view_box_end = strpos($svg_code, '"', $view_box_start);
					
					$view_box = substr($svg_code, $view_box_start, ($view_box_end - $view_box_start));
					
					list($x, $y, $end_x, $end_y) = explode(' ', $view_box);
					$this->actual_width = $end_x - $x;
					$this->actual_height = $end_y - $y;
					
				}
				else
				{
					list($this->actual_width, $this->actual_height) = @getimagesize($this->file->filenameFromServerRoot());
					
				}
				
			}
			else
			{
				$this->addConsoleWarning('File Not Found : '.$this->file->filenameFromServerRoot());
				
				
			}
			
			
			
		}
		
		
		// no file found
		if($this->actual_width < 1)
		{
			// replace with missing image
			if(!isset($_SERVER['missing_TCv_Image']))
			{
				// can't use getTCInstance… since there are 3 values
				$_SERVER['missing_TCv_Image'] = new TCm_File('missing_TCv_Image', 'empty_pixel.png', $this->classFolderFromRoot('TCv_Image').'/');
			}
			$this->addClass('missing_image');
			
			// save new values
			$this->file = $_SERVER['missing_TCv_Image'];
			list($this->actual_width, $this->actual_height) = getimagesize($this->file->filenameFromServerRoot());
			
			
		}
		// Save the default width and height values
		$this->src_width = $this->actual_width;
		$this->src_height = $this->actual_height;
		
		$this->scaleInsideBox($this->actual_width, $this->actual_height);
		
		$this->output_image_type = $this->file->extension();
		$this->output_image_type = str_ireplace('jpeg', 'jpg', $this->output_image_type); // avoid jpeg, jpg issue
		
	}
	
	/**
	 * Generates the cropped image file if it exists for the crop values associated with the file.
	 *
	 * The crop values are stored with the filename itself as as set of values correlating to the X,Y, WIDTH AND HEIGHT of
	 * the source cropping settings. When a TCm_File is created with those values in the filename, they are parsed out
	 * and stored.
	 *
	 * This method will always use the original file for the TCm_File as the reference.
	 *
	 * @param ?int $target_width The width that is wanted for the cropping. By default the value is 1200 but
	 * providing a value ensures that the proper width is set.
	 * @param ?int $target_height The height for cropping. This is only used if no crop values are found
	 *
	 * @uses TCm_File::cropValues()
	 * @uses TCv_Image::cropCustom()
	 * @uses TCv_Image::createCacheImage()
	 * @uses TCv_Image::setToCroppedVersion()
	 */
	public function generateCroppedImageFile(?int $target_width = null, ?int $target_height = null) : void
	{
		$crop_values = $this->file->cropValues();
		if($crop_values)
		{
			// Deal with loading original file AT THE SAME SIZE
			// In this case, we skip the cropped image entirely and just reference the original
			// The file is already set to it, so just get out and use that photo
			if(is_null($target_width) && is_null($target_height) && $this->original_file->fileSetToCropFullSize())
			{
				return;
			}
			
			// Deal with crop values that don't match the ratio we need
			if(is_int($target_width) && is_int($target_height))
			{
				$expected_ratio = $target_width / $target_height ;
				$actual_ratio = $crop_values['target_width'] /$crop_values['target_height'];
				
				// Bad Ratio, Auto-Correct the $crop_values height
				if($expected_ratio != $actual_ratio)
				{
					
					$crop_values['src_height'] = round($crop_values['src_width'] / $expected_ratio);
					
				}
				
			}
			
			//$this->file = $this->original_file;
			if($target_width)
			{
				if(!$target_height)
				{
					$target_height = round($crop_values['target_height'] * $target_width / $crop_values['target_width']);
				}
				
			}
			elseif(isset($crop_values['target_width']))
			{
				$target_width = $crop_values['target_width'];
				$target_height = $crop_values['target_height'];
			}
			else
			{
				
				$target_width = $crop_values['src_width'];
				$target_height = round($crop_values['src_height'] * $target_width / $crop_values['src_width']);
			}
			
			$this->cropCustom(
				$crop_values['src_width'], $crop_values['src_height'],
				$crop_values['src_x'], $crop_values['src_y'],
				$target_width, $target_height
			);
			$this->createCacheImage();
			
			$file = new TCm_File($this->id() . 'file', $this->cacheFilename(), $this->cacheFolderPath());
			$this->cropped_file = $file;
			$this->setToCroppedVersion();
		}
		
		
	}
	
	
	/**
	 * Sets the image to use the cropped version if it exists.
	 */
	public function setToCroppedVersion() : void
	{
		if($this->cropped_file)
		{
			$this->file = $this->cropped_file;
			$this->processFileProperties();
		}
	}
	
	/**
	 * Sets the image to use the un-cropped, full-size version if it exists.
	 */
	public function setToUnCroppedVersion() : void
	{
		$this->file = $this->original_file;
		$this->processFileProperties();
	}
	
	/**
	 * The file that is being shown
	 * @return ?TCm_File
	 */
	public function file() : ?TCm_File
	{
		if($this->image_filters_not_applied)
		{
			$this->useCacheAsPrimaryFile();
			$this->image_filters_not_applied = false;
		}
		return $this->file;
	}
	
	
	/**
	 * Adds a file to multiply against the photo
	 * @param string $url The url from the site root, not the server root
	 */
	public function addMultiplyFileURL(string $url) : void
	{
		$this->multiply_file_url = $url;
		$this->calculateCacheFilename();
	}
	
	//////////////////////////////////////////////////////
	//
	// PROPERTY SETTINGS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the actual width of the photo being viewed
	 * @return int
	 */
	public function actualWidth() : int
	{
		return $this->actual_width;
	}
	
	/**
	 * Returns the actual height of the photo being viewed
	 * @return int
	 */
	public function actualHeight() : int
	{
		return $this->actual_height;
	}
	
	/**
	 * Sets if scaling should be used for output. If set to false, the image will be output at the natural resolution without resizing
	 * @param bool $use_scaling
	 */
	public function setUseScaling(bool $use_scaling) : void
	{
		$this->use_scaling = $use_scaling;
	}
	
	/**
	 * The view width of the image
	 * @return int
	 */
	public function viewWidth() : int
	{
		return $this->view_width;
	}
	
	/**
	 * The view height of the image
	 * @return int
	 */
	public function viewHeight() : int
	{
		return $this->view_height;
	}
	
	/**
	 * Sets the JPEG quality from 0 to 100
	 * @param int $quality
	 */
	public function setJPEGQuality(int $quality) : void
	{
		$this->jpeg_quality = $quality;
	}
	
	//////////////////////////////////////////////////////
	//
	// DIMENSION SETTINGS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Override standard function to undo the scale_type settings.This will force the view to check that the image was scaled in some way prior to output.
	 * @param int $width
	 * @param int $height
	 */
	public function setViewBox(int $width, int $height) :void
	{
		$this->scale_type = false;
		parent::setViewBox($width, $height);
	}
	
	/**
	 * Scales the image within a specified box. If the dimensions are not exactly lined up, It will scale the image to
	 * ensure that it proportionally fits inside the box provided. This will usually produce an image that will have a
	 * width or height that is smaller than the box dimensions provided.
	 *
	 * @param int $width
	 * @param int $height
	 */
	public function scaleInsideBox(int $width, int $height) : void
	{
		$this->setViewBox($width,$height);
		
		$scale_width = $this->view_width / $this->actual_width; // percentage change in width
		$scale_height = $this->view_height / $this->actual_height; // percentage change in height
		$percent = min($scale_width, $scale_height); // take lower of the two changes
		
		
		$this->target_width = round($this->actual_width * $percent);
		$this->target_height = round($this->actual_height * $percent);
		$this->scale_type = 's'; // scale
		
		// SAVE VALUES
		$this->src_width = $this->actual_width;
		$this->src_height = $this->actual_height;
		$this->src_x = 0;
		$this->src_y = 0;
		$this->target_x = 0;
		$this->target_y = 0;
		
		
		// CHANGE VIEW BOX TO AVOID MESS
		$this->view_width = $this->target_width;
		$this->view_height = $this->target_height;
	}
	
	/**
	 * Scales the photo centered inside of a box of a certain width and height. The remaining space will be transparent.
	 * @param int $width
	 * @param int $height
	 */
	public function scaleCenteredInsideBox(int $width, int $height) : void
	{
		
		$this->setViewBox($width,$height);
		
		$width_ratio = $this->view_width / $this->actual_width; // percentage change in width
		$height_ratio = $this->view_height / $this->actual_height; // percentage change in height
		$scaling = min($width_ratio, $height_ratio); // take lower of the two changes
		
		
		$this->target_width = round($this->actual_width * $scaling); // full width
		$this->target_height = round($this->actual_height * $scaling);
		
		$this->setOutputImageType('png');
		
		if($width_ratio > $height_ratio) // vertical image
		{
			$this->target_y = 0;
			$this->target_x = round(($this->view_width - $this->target_width)/2);
		}
		else // horizontal image
		{
			$this->target_x = 0;
			$this->target_y = round(($this->view_height - $this->target_height)/2);
		}
		
		$this->scale_type = 'c'; // center
		
		$this->src_width = $this->actual_width;
		$this->src_height = $this->actual_height;
		$this->src_x = 0;
		$this->src_y = 0;
		
		
	}
	
	/**
	 * Scales the image to a close proximity to a given area of pixels
	 * @param int $area The total area in pixels
	 */
	public function scaleToArea(int $area) : void
	{
		//$this->setViewBox($width,$height);
		
		
		$this->target_width = sqrt($area*$this->actual_width/$this->actual_height);
		$this->target_height = round($area/$this->target_width);
		$this->target_width = round($this->target_width);
		
		// CHANGE VIEW BOX TO AVOID MESS
		$this->view_width = $this->target_width;
		$this->view_height = $this->target_height;
		
		$this->src_width = $this->actual_width;
		$this->src_height = $this->actual_height;
		$this->src_x = 0;
		$this->src_y = 0;
		
		
	}
	
	/**
	 * Changes the destination dimensions of this image to match a particular width and height. It will grab the largest
	 * possible image in the center of the source image that will fit the dimensions given.
	 * @param int $width
	 * @param int $height
	 */
	public function crop(int $width, int $height) : void
	{
		$this->setViewBox($width, $height); // update the view box
		
		// Change the destinations values to ensure square
		$this->target_width = $width;
		$this->target_height = $height;
		
		// Use ratios to determine which orientation to use
		$target_ratio = $this->target_height / $this->target_width;
		$actual_ratio = $this->actual_height / $this->actual_width;
		
		// COMPARE RATIOS
		if($target_ratio > $actual_ratio)
		{
			// Indicates extra space on the sides, draw it out :)
			// Scale the destination to find the largest possible width
			$this->src_width = round($this->target_width * $this->actual_height / $this->target_height);
			$this->src_x = round(($this->actual_width - $this->src_width) / 2);
		}
		else // do the opposite
		{
			$this->src_height = round($this->target_height * $this->actual_width / $this->target_width);
			$this->src_y = round(($this->actual_height - $this->src_height) / 2);
		}
		
		$this->scale_type = 'cp'; // crop
		
	}
	
	/**
	 * Returns the crop values that are created when a file is cropped
	 * @return array
	 */
	public function cropValues() : array
	{
		$values = [
			'x' => $this->src_x,
			'y' => $this->src_y,
			'width' => $this->src_width,
			'height' => $this->src_height,
		];
		
		if($this->target_width > 0)
		{
			$values['target_width'] = $this->target_width;
		}
		
		return $values;
	}
	
	/**
	 * Checks if the file is set to crop to the full size.
	 * @param ?TCm_File $file The file to be used. Null indicates use the file stored for this image.
	 * @return bool
	 */
	public function fileSetToCropFullSize(?TCm_File $file = null) : bool
	{
		if(is_null($file))
		{
			$file = $this->file;
		}
		// Only bother if this is an image with crop values
		list($actual_width, $actual_height) = @getimagesize($file->filenameFromServerRoot());
		
		if($this->src_x == 0 && $this->src_y == 0
			&& $this->src_width == $actual_width
			&& $this->src_height == $actual_height)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	/**
	 * Crops the photo to match a specific ratio. This will find the center based on the ratio and this photo's
	 * dimensions
	 * @param float $ratio
	 */
	public function cropWithRatio(float $ratio) : void
	{
		$width = $this->actualWidth();
		$height = $this->actualHeight();
		$actual_ratio = $width/$height;
		
		// taller than the cropping, use the full width
		if($actual_ratio < $ratio)
		{
			$crop_width = $width;
			$crop_height = round($crop_width / $ratio);
		}
		else // wider than taller
		{
			$crop_height = $height;
			$crop_width = round($crop_height * $ratio);
		}
		
		$this->crop($crop_width, $crop_height);
	}
	
	/**
	 * Performs a custom crop in which the 4 values for the source are provided and the resulting image is cropped directly from those values.
	 * @param int $src_width
	 * @param int $src_height
	 * @param int $src_x
	 * @param int $src_y
	 * @param int $dest_width
	 * @param int $dest_height
	 */
	public function cropCustom(int $src_width,
							   int $src_height,
							   int $src_x,
							   int $src_y,
							   int $dest_width,
							   int $dest_height) : void
	{
		$this->setViewBox($dest_width, $dest_height); // update the view box
		
		$this->src_width = $src_width;
		$this->src_height = $src_height;
		$this->src_x = $src_x;
		$this->src_y = $src_y;
		
		$this->target_width = $dest_width;
		$this->target_height = $dest_height;
		
		$this->scale_type = 'cc'; // crop custom
	}
	
	//////////////////////////////////////////////////////
	//
	// FILTERING
	//
	// Used to apply filters to the image
	// http://php.net/manual/en/function.imagefilter.php
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Applies a filter, based on GD image filter names.
	 * @param string $filter_name The name of the Imagick function to be called
	 * @param array $args The params to be passed into Imagick
	 * @return void
	 */
	public function applyFilter(string $filter_name, array $args) : void
	{
		$this->filters[] = [
			'function' => $filter_name,
			'args' => $args];
		
		// Flag image as having filters not applied and needed reprocessing
		$this->image_filters_not_applied = true;
		
		// Need to update the filename for the cache
		$this->calculateCacheFilename();
		
	}
	
	/**
	 * Applies a filter to an Imagick image, which requires parsing the values and calling the correct methods. Any
	 * filter that we want to implement, must be handled here
	 * @param Imagick $image
	 * @param string $filter_name
	 * @param array $args
	 * @return Imagick Returns the image that is being modified
	 */
	public function processFilterForImagickImage(Imagick $image, string $filter_name, array $args = []) : Imagick
	{
		// Go through the filter types
		if($filter_name == 'grayscale')
		{
			// Set it to grayscale first, desaturate completely, better results than setting to greyscale
			//$image->modulateImage(100,0,100);
			$image->setImageType(Imagick::IMGTYPE_GRAYSCALE);
		}
		elseif($filter_name == 'colorize')
		{
			$color_code = $args[0];
			
			// Check for a hex color with opacity, #rrggbbaa
			if(substr($color_code,0,1) == '#' && strlen($color_code) >= 7)
			{
				$color = substr($color_code,0,7);
				
				$opacity = 1.0;
				if(strlen($color_code) > 7)
				{
					$opacity = hexdec(substr($color_code,7))/255.0; // 0.0 to 1.0
				}
				
				$image->setImageType(Imagick::IMGTYPE_GRAYSCALE);
				
				
				
				
				
				
				// Colorize
				//	$this->addConsoleDebug('COLORIZE '. $color. ' opacity '. $opacity);
				$opacity_pixel = new \ImagickPixel("rgb(128, 128, 128, $opacity)");
				$color_pixel = new ImagickPixel($color);
				//	$image->tintImage($color_pixel, $opacity_pixel);
				$image->colorizeImage($color_pixel, $opacity_pixel);
			}
			else
			{
				$this->addConsoleError('colorize must be formatted with #rrggbbaa in config and pages_module_variables.image_filtering_options ');
				
			}
			
		}
		elseif($filter_name == 'emboss')
		{
			$image->embossImage(10,50);
		}
		elseif($filter_name == 'blur')
		{
			$radius = 10;
			if(isset($args[0]))
			{
				$radius = $args[0];
			}
			$sigma = 5;
			if(isset($args[1]))
			{
				$sigma = $args[1];
			}
			
			$image->gaussianBlurImage($radius, $sigma);
			
		}
		elseif($filter_name == 'brightness')
		{
			$brightness = 0;
			if(isset($args[0]))
			{
				$brightness = $args[0];
			}
			
			
			$image->brightnessContrastImage($brightness, 0);
			
		}
		
		return $image;
	}
	
	/**
	 * Sets the colorization for the image
	 * @param int $red from 0 to 255
	 * @param int $green from 0 to 255
	 * @param int $blue from 0 to 255
	 * @param int $alpha (Optional) Default 100. Percentage from 0 to 100 which impacts the overall opacity of the
	 * image being produced. 100% is fully opaque, 0 has no effect.
	 * @param int $darkness_offset (Optional) Default -30. Indicates how much the darkness should be increased.
	 * Corresponds to a value for brightness
	 * @param int $contrast (Optional) Default 0. Indicates the level of contrast shift from -100 to +100.
	 * Corresponds to IMG_FILTER_CONTRAST.
	 * @see http://php.net/manual/en/function.imagefilter.php
	 */
	public function colorize($red, $green, $blue , $alpha = 100, $darkness_offset = -30, $contrast = 0)
	{
		// convert alpha to 0 to 127
		
		$alpha = round((100-$alpha) * 127/100); // 0 to 127
		
		
		$avg = round(($red + $green + $blue) / 3);
		$this->applyFilter(IMG_FILTER_GRAYSCALE);
		
		$this->applyFilter(IMG_FILTER_CONTRAST,$contrast);
		
		$this->applyFilter(IMG_FILTER_COLORIZE, $red, $green, $blue, $alpha);
		
		$this->applyFilter(IMG_FILTER_BRIGHTNESS, $darkness_offset - $avg );
	}
	
	/**
	 * Sets the image to grayscale
	 */
	public function setGrayscale()
	{
		$this->applyFilter('grayscale');
	}

//	/**
//	 * Flips the photo horizontally
//	 */
//	public function flipHorizontally()
//	{
//		$this->applyFilter('imageflip',IMG_FLIP_HORIZONTAL);
//	}
	
	
	//////////////////////////////////////////////////////
	//
	// OUTPUT METHODS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Overrides the default image type to specify the type of image to be output
	 * @param string $type The type which can be jpg, png or gif
	 */
	public function setOutputImageType(string $type) : void
	{
		if($type != 'jpg' && $type != 'png' && $type != 'gif' && $type != 'webp')
		{
			TC_triggerError('Provided image type must be jpg, png, or gif');
		}
		$this->output_image_type = $type;
	}
	
	/**
	 * Triggers the view to delete any old cache for this image and recreate it
	 */
	public function refreshCache() : void
	{
		$this->refresh_cache = true;
	}
	
	/**
	 * A utility function that generates a default cache image for a specific width and returns the filename for the
	 * file that was created
	 * @param int $width
	 * @return string
	 */
	public function createDefaultCacheForWidth(int $width) : string
	{
		$old_width = $this->view_width;
		$old_height = $this->view_height;
		
		// CROP
		if($this->scale_type == 'cp')
		{
			// If we are cropping, we need to find the ratio and set the values based on that ratio
			// This means re-running the crop using proportional scale value, then run it again after to reset
			$old_target_width = $this->target_width;
			$old_target_height = $this->target_height;
			
			// Find the new height for the crop tool, based on scaling the crop values
			$cache_height = round($width * $old_target_height / $old_target_width);
			$this->crop($width, $cache_height);
			
			// Generate the cache AND the filename
			$filename = $this->createCacheImage();
			
			// Re-run the cropper to move all the values back
			$this->crop($old_target_width, $old_target_height);
			
			
		}
		// CENTER
		elseif($this->scale_type == 'c')
		{
			// If we are scaling center, we need to find the ratio and set the values based on that ratio
			// This means re-running the scale using proportional scale value, then run it again after to reset
			$old_target_width = $this->target_width;
			$old_target_height = $this->target_height;
			
			// Find the new height for the crop tool, based on scaling the crop values
			$cache_height = round($width * $old_target_height / $old_target_width);
			$this->scaleCenteredInsideBox($width, $cache_height);
			
			// Generate the cache AND the filename
			$filename = $this->createCacheImage();
			
			// Re-run the scaler, using the width and height of the view, which is what was originally passed in
			$this->scaleCenteredInsideBox($old_width, $old_height);
		}
		// Failsafe to default scale
		else
		{
			$this->scaleInsideBox($width, 5000);
			
			// Generate the image
			$filename = $this->createCacheImage();
		}
		
		// Revert the values
		$this->view_width = $old_width;
		$this->view_height = $old_height;
		
		// Return the filename
		return $filename;
	}
	
	/**
	 * Recalculates the cache filename and stores it internally. Used internally on any call that changes the
	 * potential filename of what is stored
	 * @return void
	 */
	protected function calculateCacheFilename() : void
	{
		$this->cache_filename = $this->cache_prefix.$this->file->filenameBase().'-'.$this->scale_type.'_'.$this->target_width.'x'.$this->target_height;
		if($this->scale_type == 'cc')
		{
			$this->cache_filename .= '_'.$this->src_x.'x'.$this->src_y.'y';
		}
		
		
		foreach($this->filters as $filter_values)
		{
			$filename_code = $filter_values['function'];
			if(isset(static::$filter_filename_codes[$filename_code]))
			{
				$filename_code = static::$filter_filename_codes[$filename_code];
			}
			
			$this->cache_filename .= '_'.$filename_code;
			if(count($filter_values['args']) > 0)
			{
				$vars = str_ireplace('#', '', implode('_', $filter_values['args']));
				
				$this->cache_filename .= '_'.$vars;
			}
			
		}
		
		if(!is_null($this->multiply_file_url))
		{
			$this->cache_filename .= '_MUL';
		}
		
		$this->cache_filename .= '.'.$this->output_image_type;
		
	}
	
	/**
	 * Creates the cached image that will actually be used to view the image.
	 * @param bool|string $filename (Optional) Defaults to having "-cache" included aftwards. The name of the file to be used.
	 * @param bool $cache_folder_from_server_root (Optional) Indicates if the cache folder comes from the server root
	 * @return string The filename that was created
	 */
	public function createCacheImage($filename = false, $cache_folder_from_server_root = false)
	{
		if(!$cache_folder_from_server_root)
		{
			$cache_folder_from_server_root = $_SERVER['DOCUMENT_ROOT'].$this->cache_folder_path;
		}
		if(!$filename)
		{
			$this->calculateCacheFilename();
		}
		else
		{
			$this->cache_filename = $filename;
		}
		
		if(TC_getConfig('use_webp'))
		{
			$this->cache_filename = str_ireplace(".".$this->output_image_type, '', $this->cache_filename).'.webp';
			$this->output_image_type = 'webp';
			
		}
		
		$cache_server_filename = $cache_folder_from_server_root .$this->cache_filename;
		$cache_server_filename_sans_type = str_ireplace(".".$this->output_image_type, '', $cache_server_filename);
		
		// Handle refreshed cache
		if($this->refresh_cache)
		{
			$cache_server_filename_sans_type = str_ireplace(".".$this->output_image_type, '', $cache_server_filename);
			foreach (glob($cache_server_filename_sans_type."*.".$this->output_image_type) as $filename)
			{
				unlink($filename);
			}
		}
		
		
		// Cache image exists, so get out
		if(file_exists($cache_server_filename))
		{
			return $this->cache_filename;
		}
		
		// ------------------------------------------------------------------------------------
		
		if(!file_exists($cache_folder_from_server_root))
		{
			mkdir($cache_folder_from_server_root );
		}
		
		
		// Generate the file
		$this->generateCacheImage($cache_server_filename);
		
		// Return the filename, so we can reliably interact with it
		return $this->cache_filename;
	}
	
	/**
	 * Creates the cached image that will actually be used to view the image.
	 * @param string $cache_server_filename A string will save it to the server. A value of NULL will buffer and return the contents
	 * @return string
	 */
	public function generateCacheImage($cache_server_filename)
	{
		// No image so create one
		ini_set('memory_limit', '512M');
		
		if(!$this->file->exists())
		{
			return false;
		}
		
		if(static::useImageMagick())
		{
			return $this->generateCacheImageImagick($cache_server_filename);
		}
		else
		{
			return $this->generateCacheImageGD($cache_server_filename);
		}
		
	}
	
	/**
	 * Generates an image cache file using Image Magick, for a given server filename.
	 * @param $cache_server_filename
	 * @return string|null
	 * @throws ImagickException
	 */
	protected function generateCacheImageImagick($cache_server_filename)
	{
		try
		{
			if($this->file->exists())
			{
				list($actual_width, $actual_height) = getimagesize($this->file->filenameFromServerRoot());
				
				$image = new Imagick($this->file->filenameFromServerRoot());
				
				// Set the image page so that it doesn't track the crop size
				// Avoid any issues by always setting this to the file's width and height
				$image->setImagePage($actual_width, $actual_height, 0,0);
				
				
			}
			else
			{
				return null;
			}
			
			
		}
		catch(Exception $e)
		{
			// Deal with a cache file issue
			// Cache files can be deleted just delete it, it will re-run
			$file_path = $this->file->filenameFromSiteRoot();
			if(substr($file_path,0,7) == '/cache/')
			{
				$this->file->delete();
			}
			return null;
		}
		
		
		// Output EXIF values
		//$this->addConsoleDebug($image->getImageProperties());
		
		// No cache filename, output buffer directly
		if(is_null($cache_server_filename))
		{
			ob_start();
		}
		
		// ----- BASIC CROP AND RESIZE -----
		
		// Crop
		// https://www.php.net/manual/en/imagick.cropimage.php
		if(!$this->fileSetToCropFullSize())
		{
			$image->cropImage($this->src_width,
							  $this->src_height,
							  $this->src_x,
							  $this->src_y);
		}
		// Set the quality, does nothing for PNGs
		// https://www.php.net/manual/en/imagick.setimagecompressionquality.php
		$image->setImageCompressionQuality(90);
		
		// Resize
		// https://www.php.net/manual/en/imagick.resizeimage
		$image->resizeImage($this->target_width, $this->target_height,Imagick::FILTER_LANCZOS,1 );
		
		// ----- MULTIPLY IMAGE -----
		if(!is_null($this->multiply_file_url))
		{
			// https://www.php.net/manual/en/imagick.compositeimage
			// https://stackoverflow.com/questions/3492249/simulate-photoshops-multiply-in-php-with-gd-or-imagemagick
			$overlay = new Imagick($_SERVER['DOCUMENT_ROOT'].$this->multiply_file_url);
			$image->compositeImage($overlay, Imagick::COMPOSITE_MULTIPLY, 0, 0);
		}
		
		// ----- FILTERS -----
		
		foreach($this->filters as $filter_values)
		{
			//	$this->addConsoleDebug('FILTER apply in TCv_image');
			//	$this->addConsoleDebug($filter_values);
//			$type = $filter_values['filter_type'];
//			$args = $filter_values['args'];
//
			$image = $this->processFilterForImagickImage($image, $filter_values['function'], $filter_values['args']);
			
			
		}
		
		// ----- OUTPUT -----
		
		// Detect webp
		if($this->output_image_type == 'webp')
		{
			$image->setImageFormat('webp');
			$image->setImageCompressionQuality(85);
			$image->setOption('webp:lossless', 'false');
		}
		
		// Output buffer
		if(is_null($cache_server_filename))
		{
			$final_image = $image->getImageBlob();
			ob_end_clean();
			
			return $final_image;
		}
		else
		{
			
			// Set the image page so that it doesn't track the crop size
			// This will always be the target we set
			$image->setImagePage($this->target_width, $this->target_height, 0,0);
			
			// Save the file
			$image->writeImage($cache_server_filename);
			
			// Destroy the image
			// https://www.php.net/manual/en/imagick.clear.php
			$image->clear();
			
			// Destroy any image resources
			$this->file->clearImageResource();
			
		}
		
		// ---------------- RETURN ----------------
		return null;
		
		
		
	}
	
	protected function generateCacheImageGD($cache_server_filename)
	{
		$source = $this->file->imageResource();
		$destination = imagecreatetruecolor($this->view_width, $this->view_height); // create the new destination image
		
		if(is_null($cache_server_filename))
		{
			ob_start();
		}
		
		
		// JPEG
		if($this->output_image_type == 'jpg')
		{
			imagecopyresampled($destination, $source,
							   $this->target_x, $this->target_y,
							   $this->src_x, $this->src_y,
							   $this->target_width, $this->target_height,
							   (int)$this->src_width, (int)$this->src_height);
			
		}
		
		// GIF
		elseif($this->output_image_type == 'gif')
		{
			imagecopyresampled($destination, $source,
							   $this->target_x, $this->target_y,
							   $this->src_x, $this->src_y,
							   $this->target_width, $this->target_height,
							   $this->src_width, $this->src_height);
		}
		
		// PNG
		elseif($this->output_image_type == 'png')
		{
			imagealphablending($destination, false); // setting alpha blending on
			imagesavealpha($destination, true); // save alphablending setting (important)
			
			
			// fill background with transparency
			$fill_file = new TCm_File('bg_fill', 'TCv_ImageTransparency.png', TC_getConfig('TCore_path') . '/Content/TCv_Image/');
			$bg_fill = imagecreatefrompng($fill_file->filenameFromServerRoot());
			imagecopyresampled($destination, $bg_fill,
							   0, 0,
							   0, 0,
							   $this->view_width, $this->view_height,
							   1, 1);
			
			// Fill in the image
			imagecopyresampled($destination, $source,
							   $this->target_x, $this->target_y,
							   $this->src_x, $this->src_y,
							   $this->target_width, $this->target_height,
							   $this->src_width, $this->src_height);
			
			
		}
		
		// ----- FILTERS -----


//		foreach($this->filters as $filter_values)
//		{
//			$type = $filter_values['filter_type'];
//			$args = $filter_values['args'];
//			$method = false;
//
//			if(is_int($type)) // 'IMG_FILTER_X' which are ints
//			{
//				array_unshift($args, $filter_values['filter_type']);
//				array_unshift($args, $destination);
//				$method = 'imagefilter';
//			}
//			elseif(substr($type,0,5) == 'image') // GD Method Call
//			{
//				// Loop through $args and convert any constants that are being passed in
//				foreach($args as $index => $value)
//				{
//					if(substr($value,0,4) == 'IMG_')
//					{
//						$args[$index] = constant($value);
//					}
//				}
//				array_unshift($args, $destination);
//				$method = $type;
//			}
//
//			// Call a method within TCv_Image
//			elseif(method_exists($this, $type))
//			{
//				array_unshift($args, $destination);
//				$method = array($this, $type);
//			}
//
//			// Call the filter with the values
//			if($method)
//			{
//				call_user_func_array($method,$args);
//			}
//
//
//		}
		
		// ----- MULTIPLY IMAGE -----
		if(!is_null($this->multiply_file_url))
		{
			$multiply_image = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'].$this->multiply_file_url);
			$destination = $this->multiplyImageGD($destination, $multiply_image);
		}
		
		
		// ----- OUTPUT -----
		
		if($this->output_image_type == 'jpg')
		{
			imagejpeg($destination, $cache_server_filename, $this->jpeg_quality); // save the thumbnail
		}
		// GIF
		elseif($this->output_image_type == 'gif')
		{
			imagegif($destination, $cache_server_filename); // save the thumbnail
		}
		// PNG
		elseif($this->output_image_type == 'png')
		{
			imagepng($destination, $cache_server_filename); // save the thumbnail
		}
		
		if(is_null($cache_server_filename))
		{
			$final_image = ob_get_contents();
			ob_end_clean();
			
			return $final_image;
		}
		else
		{
			// Destory destination object
			$this->file->clearImageResource();
			imagedestroy($destination); // release the memory
		}
		
		return null;
	}
	
	/**
	 * Multiplies two images together. Used for GD only. Imagick has it's own method for composition
	 * @param resource $bottom
	 * @param resource $top
	 * @return resource
	 */
	public function multiplyImageGD($bottom, $top)
	{
		$outer_width = imagesx($bottom);
		$outer_height = imagesy($bottom);
		
		$inv255 = 1.0 / 255.0;
		
		$c = imagecreatetruecolor($outer_width, $outer_height);
		for($x = 0; $x < $outer_width; ++$x)
		{
			for($y = 0; $y < $outer_height; ++$y)
			{
				$rgb_src = imagecolorsforindex($bottom, imagecolorat($bottom, $x, $y));
				$rgb_dst = imagecolorsforindex($top, imagecolorat($top, $x, $y));
				$r = $rgb_src['red'] * $rgb_dst['red'] * $inv255;
				$g = $rgb_src['green'] * $rgb_dst['green'] * $inv255;
				$b = $rgb_src['blue'] * $rgb_dst['blue'] * $inv255;
				$rgb = imagecolorallocate($c, $r, $g, $b);
				imagesetpixel($c, $x, $y, $rgb);
			}
		}
		//	exit();
		return $c;
	}
	
	/**
	 * @param int $iterations The number of iterations that the blur effect should be applied
	 */
	public function applyHeavyBlurFilter($iterations) : void
	{
		$this->applyFilter('processHeavyBlurFilter', $iterations);
	}
	
	/**
	 * Processes the heavy blur
	 * @param resource $destination
	 * @param int $iterations
	 */
	protected function processHeavyBlurFilter($destination, $iterations) : void
	{
		/* Create array with width and height of down sized images */
		
		$half_sizes = array(
			'w'=>intval($this->target_width/4	),
			'h'=>intval($this->target_height/4)
		);
		
		/* Scale by 25% and apply Gaussian blur */
		$small_image = imagecreatetruecolor($half_sizes['w'],$half_sizes['h']);
		imagecopyresampled($small_image, $destination,
						   0, 0, 0, 0,
						   $half_sizes['w'], $half_sizes['h'], $this->target_width, $this->target_height);
		
		
		/** @see https://stackoverflow.com/questions/42759135/php-best-way-to-blur-images */
		for ($x=1; $x <=$iterations; $x++)
		{
			
			imagefilter($small_image, IMG_FILTER_SMOOTH, 99);
			imagefilter($small_image, IMG_FILTER_BRIGHTNESS, 2);
			
			for ($gaus_loop=1; $gaus_loop <=$iterations*2; $gaus_loop++)
			{
				imagefilter($small_image, IMG_FILTER_GAUSSIAN_BLUR, 999);
			}
		}
		
		// Copy back to the full sized
		imagecopyresampled($destination, $small_image,
						   0, 0, 0, 0,
						   $this->target_width, $this->target_height, $half_sizes['w'], $half_sizes['h']);
		imagedestroy($small_image);
		
		// Soften Edges after scaling up
		for ($gaus_loop=1; $gaus_loop <=$iterations*2; $gaus_loop++)
		{
			imagefilter($destination, IMG_FILTER_GAUSSIAN_BLUR, 999);
		}
		
		
	}
	
	/**
	 * Sets this view to use the latest generated cache as the primary image. This will mean that any further interactions will
	 * happen with the cached file which may lead to another cache.
	 *
	 */
	public function useCacheAsPrimaryFile() : void
	{
		$this->createCacheImage();
		$this->file = $this->cacheFile();
		$this->cache_filename = false;
	}
	
	/**
	 * Returns the cache filename
	 * @return string
	 */
	public function cacheFilename() : string
	{
		return $this->cache_filename;
	}
	
	/**
	 * Sets the cache prefix
	 * @param string $prefix
	 */
	public function setCachePrefix(string $prefix) : void
	{
		$this->cache_prefix = $prefix;
	}
	
	/**
	 * Returns the full path to the cache file
	 * @return string
	 */
	public function cacheFileURL() : string
	{
		return $this->cache_folder_path.$this->cache_filename;
		
	}
	
	/**
	 * Returns the cache folder path
	 * @return string
	 */
	public function cacheFolderPath() : string
	{
		return $this->cache_folder_path;
		
	}
	
	/**
	 * Processes the file to determine what the folder path should be for caching. This detects if we're in a folder
	 * inside of /assets/ and uses the equivalent in /cache/
	 * @return void
	 */
	public function determineCacheFolderPath() : void
	{
		// Find if the file is stored in a folder in assets
		$parts = explode('/',$this->file->filenameFromSiteRoot());
		if($parts[1] == 'assets' && $parts[3] == $this->file->filename())
		{
			$this->cache_folder_path .= $parts[2].'/';
		}
		
	}
	
	/**
	 * Sets the cache path
	 * @param string $path
	 */
	public function setCacheFolderPath(string $path) : void
	{
		$this->cache_folder_path = $path;
	}
	
	/**
	 * Sets the cache folder to the same as the image location
	 */
	public function setCacheFolderToImageLocation() : void
	{
		$this->cache_folder_path = $this->file()->path();
	}
	
	/**
	 * Uses the caching with storing the cache files in the same folder and having the prefix "cache-" in front of it.
	 */
	public function useSameFolderCaching() : void
	{
		$this->setCacheFolderToImageLocation();
		$this->setCachePrefix('cache-');
	}
	
	
	/**
	 * Returns the cache file
	 * @return TCm_File
	 */
	public function cacheFile() : TCm_File
	{
		return new TCm_File('cache_file_'.$this->id(), $this->cacheFilename(), $this->cacheFolderPath());
		
	}
	
	
	/**
	 * Saves a scaled down version of the same image if necessary.
	 *
	 * This method will generate resized image in the same location as the current image, but scaled down to fix a maximum
	 * width if necessary. The new filename will be created with the same base but with the string "-TMv_Photo_resized" which
	 * is a legacy value that continues to be used.
	 * @param int $max_width The maximum width of the new photo
	 * @param bool $delete_original (Optional) Default true. Indicates if the original should be deleted. In this case the original filename is the one that persists and is saved
	 * @return TCm_File The newly created file or the original file if the file was too small to be resized
	 */
	public function saveScaledDownVersion(int $max_width, bool $delete_original = true) : TCm_File
	{
		if ($this->actualWidth() <= $max_width)
		{
			return $this->file();
		}
		
		// ensure we have a unique filename
		$original_filename = $this->file->filename();
		$new_filename = $this->file->filenameBase() . '-TMv_Photo_resized' . '.' . $this->file->extension();
		
		// If the file exists for whatever reason, we need to generate a new one
		$count = 1;
		while(file_exists($this->file->pathFromServerRoot().$new_filename))
		{
			$new_filename = $this->file->filenameBase() . '-'.$count.'-' . '-TMv_Photo_resized' . '.' .
				$this->file->extension();
			$count++;
		}
		
		$this->setJPEGQuality(100);
		$this->scaleInsideBox($max_width, 10000);
		$this->createCacheImage($new_filename, $this->file->pathFromServerRoot());
		
		if($delete_original)
		{
			$path = $this->file->pathFromServerRoot();
			// Swap names to use the original filename
			
			// delete the original
			unlink($this->file->filenameFromServerRoot());
			// Tests with saving the original under a new name
			//rename($path.$original_filename, $path.$original_filename.'.old.jpg');
			
			// Rename the new file to the original filename
			rename($path.$new_filename, $path.$original_filename);
			
			// Re-acquire the new file, with the same name
			$new_file = new TCm_File('file', $original_filename, $path, TCm_File::PATH_IS_FROM_SERVER_ROOT);
		}
		else
		{
			$path = $this->file->pathFromServerRoot();
			$new_file = new TCm_File('file', $new_filename, $path, TCm_File::PATH_IS_FROM_SERVER_ROOT);
		}
		
		
		
		
		return $new_file;
		
	}
	
	/**
	 * Generates a CSS line for background images based on an image, a specific width and the selector for that view
	 * @param int $width The width for this view
	 * @param string $selector The CSS selector that we'll use in our query
	 * @return string
	 */
	public function generateScaledBackgroundImageMediaCSS(int $width, string $selector) : string
	{
		// Content width version
		$this->scaleInsideBox($width, 10000);
		$this->createCacheImage();
		
		
		return  "\n".
			'@media only screen and (max-width : '.$width.'px) {'.
			$selector.' { background-image: url("'.$this->cacheFolderPath().$this->cacheFilename().'");}
				}';
	}
	
	/**
	 * Generates a CSS line for background images based on an image, a specific width and the selector for that view
	 * @param int $width The width for this view
	 * @param int|null $height The height for this view. If null is provided, it uses the original height of the image
	 * @param string $selector The CSS selector that we'll use in our query
	 * @param int $crop_adjustment The number of pixels that should be applied to the crop to adjust it to a specific size.
	 * This creates a zoomed out effect most likely, common when cropping.
	 * @return string
	 */
	public function generateCroppedBackgroundImageMediaCSS(int $width,
														   ?int $height,
														   string $selector,
														   int $crop_adjustment = 0) : string
	{
		if(is_null($height))
		{
			$height = $this->actualHeight();
		}
		// Content width version
		$this->crop($width+$crop_adjustment, $height);
		$this->createCacheImage();
		
		
		return  "\n".
			'@media only screen and (max-width : '.$width.'px) {'.
			$selector.' { background-image: url("'.$this->cacheFolderPath().$this->cacheFilename().'");}
				}';
	}
	
	
	
	/**
	 * Sets this view to include the full domain name in the src. Used mainly for referencing the image OUTSIDE of
	 * the site, such as in emails or other external systems.
	 * @return void
	 */
	public function includeFullDomainInSrc() : void
	{
		$this->include_full_domain_in_src = true;
	}
	
	/**
	 * Sets the alt text for the image
	 * @param string $text
	 */
	public function setAltText(string $text) : void
	{
		$this->alt_text = trim(strip_tags($text));
	}
	
	/**
	 * DE
	 * @param int $max_screen_size The max screen size breakpoint where this applies
	 * @param int $image_resize_width The width that the image should be cached at
	 * @deprecated see setColumnCount
	 */
	public function addResponsiveImageSize(int $max_screen_size, int $image_resize_width) : void
	{
		$this->responsive_image_sizes[$max_screen_size] = $image_resize_width;
	}
	
	/**
	 * Sets the number of columns that this image is being loaded in. This is used for responsive layouts to set the
	 * sizes on how it should be rendered. This enables responsive images indicating that we need to consider the
	 * rendered column but also other sizes.
	 * @param ?int $column_width The column width that this image is rendered at. Null indicates no value
	 * @return void
	 */
	public function setRenderedColumnWidth(?int $column_width) : void
	{
		$this->rendered_column_width = $column_width;
	}
	
	/**
	 * Turns off lazy loading for this image
	 * @return void
	 */
	public function disableLazyLoading() : void
	{
		$this->lazy_loading = false;
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		if($this->lazy_loading)
		{
			$this->setAttribute('loading','lazy');
		}
		
		$this->file(); // pre-process
		$this->setAttribute('alt', $this->alt_text);
		
		// DEAL WITH SVGs
		if($this->file()->extension() == 'svg')
		{
			
			//	$contents = file_get_contents($file->filenameFromServerRoot());)
			
			//	return parent::html();
		}
		
		// handle no scaling
		if(!$this->use_scaling)
		{
			$this->setAttribute('width', $this->actual_width);
			$this->setAttribute('height', $this->actual_height);
			$this->setAttribute('src', $this->file->filenameFromSiteRoot());
			
		}
		else // scaling
		{
			if($this->file()->extension() == 'svg')
			{
				$this->setAttribute('width', $this->view_width);
				$this->setAttribute('height', $this->view_height);
				$path = $this->file->filenameFromSiteRoot();
				
			}
			else
			{
				// Already handled by the
				if($this->scale_type == false)
				{
					$this->scaleInsideBox($this->view_width, $this->view_height);
					
				}
				
				// Don't scale to the exact same sizes
				if($this->view_width != $this->actual_width && $this->view_height != $this->actual_height)
				{
					$this->createCacheImage();
				}
				
				// No cache is created, so just load the original file
				$path = $this->cache_folder_path . $this->cache_filename;
				if($this->cache_filename == '')
				{
					$path = $this->file->path() . $this->file->filename();
				}
			}
			
			
			// Set constant properties
			$this->setAttribute('width', $this->view_width);
			$this->setAttribute('height', $this->view_height);
			$this->setAttribute('src', htmlspecialchars($path));
			
			if($this->view_width > $this->view_height)
			{
				$this->addClass('is_landscape');
			}
			elseif($this->view_height > $this->view_width)
			{
				$this->addClass('is_portrait');
			}
			else
			{
				$this->addClass('is_square');
			}
			
		}
		
		if($this->include_full_domain_in_src)
		{
			$src = $this->fullDomainName(). $this->getAttribute('src');
			$this->setAttribute('src',$src);
		}
		
		$this->processResponsiveTagAttributes();
		
		
		// RETURN
		return parent::html();
		
	}
	
	/**
	 * Processes the responsive sizes, based on the set column widths and gaps. This generates modern srcset and size
	 * attributes on the image based on values coming from TC_config. This code needs to know the content_width,
	 * content_column_gap and content_single_column_width, in order to work effectively. These should be set for the
	 * site, so these calculations can work properly. If they aren't set, it's ignored.
	 * @return void
	 */
	protected function processResponsiveTagAttributes() : void
	{
		// Don't bother with SVGs
		if($this->file()->extension() == 'svg')
		{
			return;
		}
		
		// If we have these two values in the config, we can process response
		// Only bother if we've been told rendered column widths, otherwise just load the one size we want
		if(TC_configIsSet('content_column_gap') && !is_null($this->rendered_column_width))
		{
			$srcset = [];
			$image_widths = [];
			$single_column_breakpoint = TC_getConfig('content_single_column_width');
			$full_width = TC_getConfig('content_width');
			$image_widths[$full_width] = $full_width;
			$image_widths[$single_column_breakpoint] = $single_column_breakpoint;
			//$image_widths['small_device_width'] = TC_getConfig('small_device_width');
			
			$sizes = [];
			// On smaller devices, takes up the full width
			// Account for different resolutions and DRPs
			$resolutions = [1,1.5];
			$small_device_width = $full_width/2;
			foreach($resolutions as $multiplier)
			{
				$dpr_value = $small_device_width*$multiplier;
				$image_widths[$dpr_value] = $dpr_value;
				$sizes[] = "(max-width: "
					. ($small_device_width * $multiplier) . "px and max-resolution: ".$multiplier."x) "
					. $dpr_value . "px";
			}
			// Detect if we have a rendered column width, and it's not full width
			if($this->rendered_column_width != $full_width )
			{
				// Save the width to load
				$image_widths[$this->rendered_column_width] = $this->rendered_column_width;
				
				// Sizes specific to dealing with greater than single column mode
				$sizes[] = "(min-width: ".$single_column_breakpoint."px) ".$this->rendered_column_width."px";
			}
			
			// Full width
			$sizes[]  = '100vw';
			
			// Generate caches for all the sizes
			foreach($image_widths as $image_resize_width)
			{
				$filename = $this->createDefaultCacheForWidth($image_resize_width);
				$filename = $this->cacheFolderPath().$filename;
				$srcset[] = $filename." ".str_replace('.','_',$image_resize_width).'w';
			}
			
			$this->setAttribute('sizes', implode(', ', $sizes));
			
			// Set the srcset attribute
			$this->setAttribute('srcset',implode(', ', $srcset));
			
			// Set the full-size
			$this->setAttribute('src', $this->original_file->filenameFromSiteRoot());
			
		}
		
	}
	
}
