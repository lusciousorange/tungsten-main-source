<?php

/**
 * A link that is used to toggle the visibilty of another target. This is a shortcut class that saves loading larger JS libraries.
 */
class TCv_ToggleTargetLink extends TCv_Link
{
	protected $visiblity_action = 'slideToggle';
	protected $toggle_visibility_targets = array();
	protected $toggle_classes = array();
	
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->url = '#'; // never perform an action
	}
	
	
	/**
	 * Turns off sliding and sliding changes
	 */
	public function disableVisiblityChanges()
	{
		$this->visiblity_action = false;
	}
	
	/**
	 * Turns on slide toggle as the visibility action
	 */
	public function setUseSlideToggle()
	{
		$this->visiblity_action = 'slideToggle';
	}
	
	/**
	 * Turns on the traditional toggle
	 */
	public function setUseStandardToggle()
	{
		$this->visiblity_action = 'toggle';
	}
	
	/**
	 * Adds a target that will be flipped when this button is clicked
	 * @param string $target The CSS identifier for modifying DOM elements
	 */
	public function addVisibilityTarget($target)
	{
		$this->toggle_visibility_targets[$target] = $target;
	}
	
	/**
	 * Adds a target that will be flipped when this button is clicked
	 *
	 * @param string $target The CSS identifier for modifying DOM elements
	 * @param string $class The class to be toggled
	 */
	public function addClassToggle($target, $class)
	{
		$this->toggle_classes[] = array('target' => $target, 'class' =>  $class);
	}
	
	public function html()
	{
		$command = '';
		
		if(sizeof($this->toggle_visibility_targets) > 0)
		{
			$command .= "document.querySelectorAll('".implode(', ', $this->toggle_visibility_targets) ."').forEach(el=>{";
			$command .=     "el.".$this->visiblity_action."();";
			$command .=     "el.dispatchEvent(new Event('visibility_toggle'));";
			$command .= "});";
		}
		
		foreach($this->toggle_classes as $class_values)
		{
			$command .= "document.querySelectorAll('".$class_values['target'] ."').forEach(el=>{";
			$command .=     "el.classList.toggle('".$class_values['class']."');";
			$command .=     "el.dispatchEvent(new Event('class_toggle'));";
			$command .= "});";
		}
		$command .= 'return false;'; // avoid any other actions
		
		$this->setAttribute('onclick', $command);
		
		
		return parent::html();	
	}	
	
}

?>