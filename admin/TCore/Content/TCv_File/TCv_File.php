<?php
/**
 * Class TCv_File
 *
 * A default view of a file
 */
class TCv_File extends TCv_View
{

	/** @var ?TCm_File The file being shown */
	protected ?TCm_File $file = null;
	
	protected int $view_width = 50; // [int] = The width that this viewer should show
	protected int $view_height = 50; // [int] = The height that this viewer should show
	
	protected static $extension_classes = array(
		'jpg' => 'TCv_Image',
		'jpeg' => 'TCv_Image',
		'gif' => 'TCv_Image',
		'png' => 'TCv_Image'
		
		);

	/**
	 * TCv_File constructor.
	 * @param string $id
	 * @param ?TCm_File $file
	 */
	public function __construct($id, $file)
	{
		parent::__construct($id);
		$this->file = $file;
	}
	
	
	/**
	 * Sets the width and height for the view
	 * @param int $width
	 * @param int $height
	 */
	public function setViewBox(int $width, int $height) : void
	{
		$this->view_width = $width;
		$this->view_height = $height;
	}
	
	/**
	 * Refreshes the cache
	 */
	public function refreshCache()
	{	
		// Does nothing in this instance
	}
	
	
	/**
	 * @return string
	 */
	public function html()
	{
		//$html = '<img class="'.$this->classesString().'" alt="'.htmlspecialchars($this->id()).'" src="'.TC_getConfig('TCore_path').'/System/TCm_File/icons/file.png" width="'.htmlspecialchars($this->view_width).'" height="'.htmlspecialchars($this->view_height).'" />';
		return parent::html();
	}


	//////////////////////////////////////////////////////
	//
	// STATIC METHODS
	//
	//////////////////////////////////////////////////////

	/**
	 * A factory method to return the appropriate viewer for a given file. In many cases, the viewer will be an instance of TCv_File
	 * @param string $id
	 * @param TCm_File $file
	 * @return TCv_File
	 */
	public static function viewForFile($id, $file)
	{
		TC_assertObjectType($file, 'TCm_File');
			
		// Check if the extenion for the file has a default class viewer
		// If so, instantiate an instance of that class using the normal properties
		// Otherwise provide a TCv_File instance
		
		if(isset(self::$extension_classes[$file->extension()]))
		{
			return new self::$extension_classes[$file->extension()]($id, $file);
		}
		return new TCv_File($id, $file);
	}	
	
	/**
	 * A means of setting an override for the default View class for handling a particular file extension. The class
	 * name provided must be a subclass of TCv_File. This method is a class method to allow for a single setting of
	 * this property for all viewers.
	 * @param string $extension
	 * @param string $class_name
	 */
	public static function setViewClassForExtension($extension, $class_name)
	{
		self::$extension_classes[$extension] = $class_name;
	}
	
	
}

?>