<?php
/**
 * Class TCv_HTMLTableCellHeading
 */
class TCv_HTMLTableCellHeading extends TCv_HTMLTableCell
{
	/**
	 * TCv_HTMLTableCellHeading constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->setTag('th');
	}
	
	
}

?>