<?php

class TCm_FileBlob extends TCm_File
{
	protected $blob_content = false;
	protected $image_resource = false;
	
	public function __construct($id, $filename, $blob)
	{
		parent::__construct($id, $filename, '');
		
		$this->setBLOBContent($blob);
		$this->path = false;
		
	}
		
	public function pathFromSiteRoot()
	{
		return false;
	}
	
	public function filenameFromSiteRoot()
	{
		return false;
	}
	
	public function pathFromServerRoot()
	{
		return false;
	}
	
	public function filenameFromServerRoot()
	{
		return false;
	}
	
	
	public function setBLOBContent($blob_content)
	{
		$this->blob_content = $blob_content;
	}
	
	public function content()
	{
		return $this->blob_content;
	}
	
	
	public function imageResource()
	{
		if($this->image_resource === false)
		{
			$this->image_resource = imagecreatefromstring($this->content());
		}
		return $this->image_resource;
	}
	
	/**
	 * Returns the filesize. By default it returns a value in the smallest 1024 unit such as KB, MB, or GB. An option can be set to return just bytes
	 * @return int
	 */
	public function filesize()
	{
		return strlen($this->blob_content);
	}		
	
	public function exists()
	{
		return true;
	}		

	public function createFolderPath()
	{
		return false;
	}		
	
	
}

?>