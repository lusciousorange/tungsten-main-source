<?php

/**
 * This is a graphing library built on ApexCharts that is used to render a range of graphs that can be used in
 * Tungsten or on public websites.
 */
class TCv_Graph extends TCv_View
{
	protected $chart_type;
	
	/** Class constants for simple chart types. */
	public const
		AreaType = 'area', LineType = 'line', BarType = 'bar', ScatterType = 'scatter', BubbleType = 'bubble',
		CandlestickType = 'candlestick', PieType = 'pie', DonutType = 'donut', RadialBarType = 'radialBar',
		RadarType = 'radar', HeatmapType = 'heatmap', RangeBarType = 'rangeBar', BoxplotType = 'boxPlot',
		TreemapType = 'treemap', PolarAreaType = 'polarArea';

	/** Class constants for mixed chart types. To get supported series types, explode with delimeter '/'. */
	public const
		LineBarAreaMixedType = self::LineType."/".self::BarType."/".self::AreaType,
		ScatterLineMixedType = self::ScatterType."/".self::LineType,
		CandlestickLineMixedType = self::CandlestickType."/".self::LineType,
		BoxplotScatterMixedType = self::BoxplotType."/".self::ScatterType;

	/** Class constants for every available type for x-values. */
	public const CategoryXVal = 'category', NumericXVal = 'numeric', DatetimeXVal = 'datetime';

	/** The number of dimensions a datapoint in each chart type should have. */
	protected const NumDimensionsInDatapoint = [
		2 => [self::AreaType, self::LineType, self::BarType, self::ScatterType, self::PieType,
				self::DonutType, self::RadialBarType, self::RadarType, self::HeatmapType,
				self::TreemapType, self::PolarAreaType],
		3 => [self::BubbleType, self::RangeBarType],
		5 => [self::CandlestickType],
		6 => [self::BoxplotType]
	];

	/**
	 * Will only be set if the chart is a mixed type; if set, it will be one of the class constants ending in `MixedType`.
	 * @var string
	 */
	protected $mixedType;

	/**
	 * This chart's data.
	 * @var array
	 */
	protected $series = [];

	/**
	 * The actual configuration for this chart; styles, layout, etc; everything except the data.
	 * @var array
	 */
	protected $config = [];

	/**
	 * A default grid color that gets set by {@see setGridColor()}. Used to apply the desired grid color to
	 * y-axes that are added with {@see addYAxis()} after the function has been called.
	 * @var string
	 */
	protected $gridColor;

	/**
	 * An array whose keys are series/chart types, and whose values are config objects of settings which
	 * should apply to all series of that type. NOTE: This is only used when working with mixed charts.
	 * @var array
	 */
	protected $configForSeriesType = [];

	/**
	 * Constructor
	 * @param string $id The ID for the chart. Must be unique in order to load multiple on the same page.
	 */
	public function __construct(?string $id)
	{
		parent::__construct($id);

		// Add JS, external and internal.
		$this->addJSFile('ApexCharts',"https://cdn.jsdelivr.net/npm/apexcharts");
		$this->addClassJSFile('TCv_Graph');

		// Get the initial, most general default settings we have.
		$this->updateConfig($this->getDefaultConfig('general_settings_template'));

		// Load the built-in locales, english and french
		$this->addLocale($this->getDefaultConfig('locale_fr'));
		$this->addLocale($this->getDefaultConfig('locale_en'), true);
	}

	public function render()
	{
		$this->setTag('figure');

		$chart_target = new TCv_View($this->id().'_target');
		$this->attachView($chart_target);

		$params = [
			'target' => $this->id().'_target',
			'config' => $this->buildConfigObject()
		];
		$this->addClassJSInit('TCv_Graph', $params);
	}
	
	/**
	 * Sets the height for the chart.
	 * @param string $height
	 * @return void
	 */
	public function setHeight(string $height) : void
	{
		$this->updateConfig([ 'chart' => [ 'height' => $height ] ] );
	}
	
	
	/**
	 * hides the legend for this graph
	 * @return void
	 */
	public function hideLegend() : void
	{
		$this->updateConfig([ 'legend' => [ 'show' => false ] ] );
	}
	
	/**
	 * Set a minimum value for the Y axis
	 * @param int $min
	 * @return void
	 */
	public function setYAxisMin(int $min) : void
	{
		$this->updateConfig([ 'yaxis' => [ 'min' => $min ] ] );
		
	}
	
	/**
	 * Set a maximum value for the y axis
	 * @param int $max
	 * @return void
	 */
	public function setYAxisMax(int $max) : void
	{
		$this->updateConfig([ 'yaxis' => [ 'max' => $max ] ] );
		
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// AVAILABLE CHART TYPES
	//
	//////////////////////////////////////////////////////

	/**
	 * A convenient method that lets you set chart type using one of the class constants; see constants ending
	 * in 'Type' and 'MixedType'.
	 * @param string $chart_type One of the class constants ending in 'Type' or 'MixedType'.
	 */
	public function setChartType(string $chart_type) : void
	{
		if (self::isStringSimpleChartType($chart_type))
		{
			$setAsXChart = 'setAs'.ucfirst($chart_type).'Chart';
		}
		elseif(self::isStringMixedChartType($chart_type))
		{
			$types = explode('/', $chart_type);
			$setAsXChart = 'setAsMixed';
			foreach ($types as $type)
				$setAsXChart .= ucfirst($type);
			$setAsXChart .= 'Chart';
		}
		else
		{
			$this->addConsoleError("Invalid chart type '{$chart_type}'. Chart type was not set.");
			return;
		}
		$this->$setAsXChart();
	}

	//// Axis Charts ////

	/**
	 * Sets up this chart as a line chart.
	 *
	 * Line charts are good for tracking a change in a number over some variable. Example: Changes in wildlife
	 * population over several years, where each series is a species.
	 *
	 * @link https://apexcharts.com/docs/chart-types/line-chart/ ApexCharts line chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/line-charts/ ApexCharts line chart demos
	 */
	public function setAsLineChart() : void
	{
		$this->chart_type = self::LineType;
		
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('line_template')
		);
	}

	/**
	 * Sets up this chart as a bar chart.
	 *
	 * Bar charts are good for comparing things in categories, and for tracking a change in a number over some
	 * variable. Examples: Music album sales categorized by band. Comparing profits in each quarter of a year.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/column-charts/ ApexCharts vertical bar chart demos
	 * @link https://apexcharts.com/javascript-chart-demos/bar-charts/ ApexCharts horizontal bar chart demos
	 */
	public function setAsBarChart() : void
	{
		$this->chart_type = self::BarType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('bar_template')
		);
	}

	/**
	 * Sets up this chart as an area chart.
	 *
	 * Area charts are good for tracking a change in a number over some variable. They're similar to line
	 * charts, but are often used when you want to emphasize the magnitude of a change, rather than focusing
	 * on specific numbers. Example: Music sales over several years, where each series is a genre.
	 *
	 * @link https://apexcharts.com/docs/chart-types/area-chart/ ApexCharts area chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/area-charts/ ApexCharts area chart demos
	 */
	public function setAsAreaChart() : void
	{
		$this->chart_type = self::AreaType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('area_template')
		);
	}

	/**
	 * Sets up this chart as a scatter chart.
	 *
	 * Scatter charts are good for showing the relation between two variables. Example: Life expectancy vs
	 * median household income for a group of cities.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/scatter-charts/ ApexCharts scatter chart demo
	 */
	public function setAsScatterChart() : void
	{
		$this->chart_type = self::ScatterType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			[ 'chart' => [ 'type' => self::ScatterType ] ]
		);
	}

	/**
	 * Sets up this chart as a bubble chart.
	 *
	 * Bubble charts are good for showing the relationship between three variables. Example: Life expectancy
	 * vs median household income vs population for a group of cities.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/bubble-charts/ ApexCharts bubble chart demos
	 */
	public function setAsBubbleChart() : void
	{
		$this->chart_type = self::BubbleType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('bubble_template')
		);
	}

	/**
	 * Sets up this chart as a candlestick chart.
	 *
	 * Candlestick charts are used for financial data - they track changes in price over time. Example: The
	 * daily change in Facebook's stock price over the past month.
	 *
	 * @link https://apexcharts.com/docs/chart-types/candlestick/ ApexCharts candlestick chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/candlestick-charts/ ApexCharts candlestick chart demos
	 */
	public function setAsCandlestickChart() : void
	{
		$this->chart_type = self::CandlestickType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			[ 'chart' => [ 'type' => self::CandlestickType ] ]
		);
	}

	/**
	 * Sets up this chart as a boxplot chart.
	 *
	 * Boxplot charts are often used to summarize data. A single box shows a high, low, median, second
	 * quartile, and third quartile of a data set. They're a little bit like a more detailed range bar chart,
	 * for some applications. Examples: A box showing the distribution of test scores for a class. Several
	 * boxes showing the distribution of test scores in each subject for a class.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/box-whisker-charts/ ApexCharts boxplot chart demos
	 */
	public function setAsBoxPlotChart() : void
	{
		$this->chart_type = self::BoxplotType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			[ 'chart' => [ 'type' => self::BoxplotType ] ]
		);
	}

	/**
	 * Sets up this chart as a range bar chart.
	 *
	 * Range bar charts are good for comparing data in categories and showing trends over some variable,
	 * whenever you want to show a range of values for each bin in your data. They're a little bit like a
	 * less-detailed boxplot chart, for some applications. When horizontal, they also work great for
	 * visualizing timelines or schedules. Examples: The range in daily temperature for the past week. A
	 * project timeline, where categories are different tasks and series are different employees.
	 *
	 * @link https://apexcharts.com/docs/chart-types/range-bar-chart/ ApexCharts rangebar chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/timeline-charts/ ApexCharts rangebar chart demos
	 */
	public function setAsRangeBarChart() : void
	{
		$this->chart_type = self::RangeBarType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('rangebar_template')
		);
	}

	//// Charts With Other Layouts ////

	/**
	 * Sets up this chart as a pie chart. This is a single-series chart.
	 *
	 * Pie charts are good for comparing data in categories when those categories sum up to a meaningful
	 * whole. They're intuitive to read and great for part-to-whole analysis. Examples: A breakdown of how a
	 * budget is being spent. The results of a poll, such as favorite movie genre.
	 *
	 * @link https://apexcharts.com/docs/chart-types/pie-donut/ ApexCharts pie chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/pie-charts/ ApexCharts pie chart demos
	 */
	public function setAsPieChart() : void
	{
		$this->chart_type = self::PieType;
		$this->updateConfig($this->getDefaultConfig('pie_template'));
	}

	/**
	 * Sets up this chart as a donut chart. This is a single-series chart.
	 *
	 * Donut charts are just pie charts with a whole in the middle, so they share all the strengths of pie
	 * charts. They're good for comparing data in categories when those categories sum up to a meaningful
	 * whole. They're intuitive to read and great for part-to-whole analysis. Examples: A breakdown of how a
	 * budget is being spent. The results of a poll, such as favorite movie genre.
	 *
	 * @link https://apexcharts.com/docs/chart-types/pie-donut/ ApexCharts donut chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/pie-charts/ ApexCharts donut chart demos
	 */
	public function setAsDonutChart() : void
	{
		$this->chart_type = self::DonutType;
		$this->updateConfig($this->getDefaultConfig('donut_template'));
	}

	/**
	 * Sets up this chart as a radial bar chart. This is a single-series chart.
	 *
	 * Radial bar charts are essentially percentage-based bar charts, wrapped around a circle. They're good
	 * for comparing things in categories and can also be used to make gauges. Examples: A comparison of the
	 * performance of several computers on some metric. A set of gauges showing a computer's CPU usage, RAM
	 * usage, and other stats.
	 *
	 * @link https://apexcharts.com/docs/chart-types/radialbar-gauge/ ApexCharts radial bar chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/radialbar-charts/ ApexCharts radial bar chart demos
	 */
	public function setAsRadialBarChart() : void
	{
		$this->chart_type = self::RadialBarType;
		$this->updateConfig($this->getDefaultConfig('radialbar_full_circle_template'));
	}

	/**
	 * Sets up this chart as a radar/spider chart.
	 *
	 * Radar charts are good for comparing different characteristics of things. Examples: Charting several
	 * stats about a soccer player, such as their goals, assists, etc this season (single series). Comparing
	 * several soccer players' stats for this season (multiple series). Comparing pain medicines based on
	 * characteristics like their efficacy, cost, prevalence of side effects, etc.
	 *
	 * @link https://apexcharts.com/docs/chart-types/radar/ ApexCharts radar chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/radar-charts/ ApexCharts radar chart demos
	 */
	public function setAsRadarChart() : void
	{
		$this->chart_type = self::RadarType;
		$this->updateConfig($this->getDefaultConfig('radar_template'));
	}

	/**
	 * Sets up this chart as a heatmap chart.
	 *
	 * Heatmaps are essentially a data table where each cell is given a different shade of colour based on its
	 * value. They're good at comparing data in categories and looking for patterns in data or correlations
	 * between two variables. Examples: Analyzing daily precipitation in a city over the past year - vertical
	 * axis is months, horizontal axis is bins for the average daily precipitation. Github's user contributions
	 * graph - each square is a day, coloured by the volume of that user's contributions that day.
	 *
	 * A NOTE on data input: Each series is a row in the data table, while x values are columns.
	 *
	 * @link https://apexcharts.com/docs/chart-types/heatmap-chart/ ApexCharts heatmap chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/heatmap-charts/ ApexCharts heatmap chart demos
	 */
	public function setAsHeatmapChart() : void
	{
		$this->chart_type = self::HeatmapType;
		$this->updateConfig($this->getDefaultConfig('heatmap_template'));
	}

	/**
	 * Sets up this chart as a treemap chart.
	 *
	 * Treemaps are useful for comparing the magnitude of different data points. Each rectangle in a treemap
	 * is sized proportionally to its value. Examples: Showing the difference in land area of Canada's
	 * provinces. Showing the proportion of how many people die to various causes of death.
	 *
	 * A NOTE on ApexCharts: Usually treemaps are meant to visualize hierarchical (tree-structured) data as a
	 * set of nested rectangles. Unfortunately, as of ApexCharts version 3.30.0 there isn't any nesting in
	 * treemap charts. At best, there is one level of "nesting" in that each series is technically a rectangle
	 * containing several other rectangles.
	 *
	 * @link https://apexcharts.com/docs/chart-types/treemap-chart/ ApexCharts treemap chart docs
	 * @link https://apexcharts.com/javascript-chart-demos/treemap-charts/ ApexCharts treemap chart demos
	 */
	public function setAsTreemapChart() : void
	{
		$this->chart_type = self::TreemapType;
		$this->updateConfig($this->getDefaultConfig('treemap_template'));
	}

	/**
	 * Sets up this chart as a polar area chart. This is a single-series chart.
	 *
	 * Sometimes called a "polar bar chart". While visually similar to a pie chart, polar area charts are more
	 * like bar charts plotted in a polar coordinate system. They are good at comparing data in categories and
	 * tracking changes in a number over some variable. They're often used to track changes over a cyclical
	 * variable, like days of the week or months in a year. Example: The number of page views a website has
	 * gotten in every month of the past year.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/polar-area-charts/ ApexCharts polar area chart demos
	 */
	public function setAsPolarAreaChart() : void
	{
		$this->chart_type = self::PolarAreaType;
		$this->updateConfig($this->getDefaultConfig('polararea_template'));
	}

	//// Mixed Chart Types ////

	/**
	 * Sets up this chart as a mixed chart of line, bar, and area series.
	 *
	 * Typically, the different kinds of series are used to visualize closely related data, or the same data
	 * in different ways. Examples: A bar series showing sum of profits and a line series showing sum of sales.
	 * A bar series showing wins, a bar series showing losses, and a line series showing cumulative "score".
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/mixed-charts/ ApexCharts line/bar/area chart demos
	 */
	public function setAsMixedLineBarAreaChart() : void
	{
		$this->chart_type = self::LineBarAreaMixedType;
		$this->mixedType = self::LineBarAreaMixedType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			$this->getDefaultConfig('line_template'),
			$this->getDefaultConfig('bar_template'),
			$this->getDefaultConfig('area_template'),
			[ 'chart' => [ 'type' => self::LineType ] ]
		);

		$this->updateConfigForSeriesType(self::LineType, $this->getDefaultConfig('mixed_line_series_template'));
		$this->updateConfigForSeriesType(self::BarType, $this->getDefaultConfig('mixed_bar_series_template'));
		$this->updateConfigForSeriesType(self::AreaType, $this->getDefaultConfig('mixed_area_series_template'));
	}

	/**
	 * Sets up this chart as a mixed chart of scatter and line series.
	 *
	 * Typically, the line series in this kind of chart serves as a line-of-best-fit for the scatter data.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/mixed-charts/line-scatter/ ApexCharts scatter/line chart demo
	 */
	public function setAsMixedScatterLineChart() : void
	{
		$this->chart_type = self::ScatterLineMixedType;
		$this->mixedType = self::ScatterLineMixedType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			//$this->getDefaultConfig('scatter_template'),
			$this->getDefaultConfig('line_template'),
			[ 'chart' => [ 'type' => self::LineType ] ]
		);

		$this->updateConfigForSeriesType(self::LineType, $this->getDefaultConfig('mixed_line_series_template'));
		$this->updateConfigForSeriesType(self::ScatterType, $this->getDefaultConfig('mixed_scatter_series_template'));
	}

	/**
	 * Sets up this chart as a mixed chart of candlestick and line series.
	 *
	 * The line series in these charts may be used to highlight something about the candlestick series' data,
	 * or they may be used to show related data. Examples: A candlestick tracking stock prices over some time
	 * period, with a line that shows the overall change in price by connecting the very first and very last
	 * prices. A candlestick series showing changes in stock price over a time period, a line series showing
	 * total revenue, and a line series showing net income.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/candlestick-charts/candlestick-with-line/ ApexCharts candlestick/line chart demo
	 */
	public function setAsMixedCandlestickLineChart() : void
	{
		$this->chart_type = self::CandlestickLineMixedType;
		$this->mixedType = self::CandlestickLineMixedType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			//$this->getDefaultConfig('candlestick_template'),
			$this->getDefaultConfig('line_template'),
			[ 'chart' => [ 'type' => self::LineType ] ]
		);
		$this->updateConfigForSeriesType(self::LineType, $this->getDefaultConfig('mixed_line_series_template'));
		$this->updateConfigForSeriesType(self::CandlestickType, $this->getDefaultConfig('mixed_candlestick_series_template'));
	}

	/**
	 * Sets up this chart as a mixed chart of boxplot and scatter series.
	 *
	 * In these charts, the scatter series is typically used to show outliers from the data each box is
	 * summarizing.
	 *
	 * @link https://apexcharts.com/javascript-chart-demos/box-whisker-charts/boxplot-scatter/ ApexCharts boxplot/scatter demo
	 */
	public function setAsMixedBoxPlotScatterChart() : void
	{
		$this->chart_type = self::BoxplotScatterMixedType;
		$this->mixedType = self::BoxplotScatterMixedType;
		$this->updateConfig(
			$this->getDefaultConfig('axis_chart_template'),
			//$this->getDefaultConfig('boxplot_template'),
			//$this->getDefaultConfig('scatter_template'),
			[ 'chart' => [ 'type' => self::BoxplotType ] ]
		);

		$this->updateConfigForSeriesType(self::ScatterType, $this->getDefaultConfig('mixed_scatter_series_template'));
		$this->updateConfigForSeriesType(self::BoxplotType, $this->getDefaultConfig('mixed_boxplot_series_template'));
	}


	//////////////////////////////////////////////////////
	//
	// SETTING UP THE DATA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Add one series of data to this chart. Strings, integers, and floats can all be used for x values; see
	 * {@see setXValueType()} for more detail. NOTE: If you want the x values on an axis chart to count down
	 * to 0, you have to provide negative x-values for every series; numeric x-axes only ever count up.
	 *
	 * All data in a series must share the same structure. Data can be structured associatively (x => y or
	 * x => [y,z]) or as an array-of-arrays ([x,y] or [x,y,z]). Both work with every chart type, but for some
	 * range bar charts array-of-arrays is preferable because 2+ datapoints can have the same x value, ie go
	 * into the same category. NOTE: All datapoints must have precisely the number of dimensions expected for
	 * this chart's type; see {@see NumDimensionsInDatapoint}.
	 *
	 * @param array $data An array of datapoints.
	 * @param string $name A name for this series which will display on the legend.
	 * @param string|null $type ONLY FOR MIXED CHARTS. Specify a type for the series; make sure it's one of the
	 * types in the chosen mix. If it isn't, a default will be picked out of the legal types.
	 */
	public function addSeries(array $data, string $name = '', string $type = NULL) : void
	{
		// If no name is provided, provide a default name.
		if($name == '')
		{
			$name = 'Series '.(count($this->series)+1);
		}

		// Add to series list
		$this->series[] = [
			'name' => $name,
			'data' => $data,
			'type' => $type
		];
	}
	
	/**
	 * Returns the number of series for this graph
	 * @return int
	 */
	public function numSeries(): int
	{
		return count($this->series);
	}
	
	
	/**
	 * An extension of the normal addSeries method which is used for Radial bar charts that only accept percentages
	 * which limits what we can do with values. This method accepts a maximum value first and then the data is the
	 * values to be compared against that maximum. Those values are all saved in the config for the chart to be
	 * accessed later. The values submitted to the chart are still percentages, which are required.
	 *
	 * @param float $max The maximum value for the chart.
	 * @param array $data An array of datapoints which are values to be compared against the max.
	 * @param string $name A name for this series which will display on the legend.
	 * @param string|null $type ONLY FOR MIXED CHARTS. Specify a type for the series; make sure it's one of the
	 * types in the chosen mix. If it isn't, a default will be picked out of the legal types.
	 
	 *
	 */
	public function addRadialBarChartSeriesValues(float $max, array $data, string $name = 'Series', string $type = NULL) : void
	{
		$this->updateConfig(['radialBarMax' => $max, 'radialBarValues' => $data]);
		
		$series_percentage_data = [];
		foreach($data as $value)
		{
			$series_percentage_data[] = $value/$max * 100;
		}
		
		// Add the series in the normal way
		$this->addSeries($series_percentage_data, $name, $type);;
	}
	
	/**
	 * Set the array of colours that series will be coloured by. Valid formats: hex, rgb, rgba.
	 *
	 * NOTE: Colours for Box Plot and Candlestick charts aren't affected by this. See docs for
	 * {@link https://apexcharts.com/docs/options/plotoptions/boxplot/ Boxplot plotOptions} and
	 * {@link https://apexcharts.com/docs/options/plotoptions/candlestick/ Candlestick plotOptions}.
	 *
	 * @param array $colors An array of colours (as strings) to draw the data series with.
	 */
	public function setSeriesColors(array $colors) : void
	{
		// Warn about invalid series colours
		foreach ($colors as $key => $color_value)
		{
			if (!TCu_Text::isValidColor($color_value))
			{
				$this->addConsoleError("Invalid series color {$key}. Please use a string in hex, rgb, or rgba format.");
			}
		}

		// Can't use updateConfig here because if the current colour array is longer than the new colour array
		// then updateConfig won't fully overwrite it.
		$this->config['colors'] = array_values($colors);
	}

	/**
	 * Tell the chart explicitly if the data's x-values are categorical, numeric, or datetimes. ApexCharts
	 * attempts to guess what type x values are if you do not specify, but if a chart is doing something
	 * unexpected with its x axis then make sure to set this.
	 *
	 * CategoryXVal: X values are typically strings. Can be used with all chart types. For chart types with
	 * 		visible axes, unless 'xaxis.tickAmount' is specified, the graph will try to display a label for
	 * 		every x value in the order given. With 'xaxis.tickAmount', an evenly spaced number of x values
	 * 		will appear as labels.
	 * NumericXVal: X values MUST be numbers. Some chart types may not render properly with this type; ex, pie
	 * 		charts. All data will be displayed in ascending order by x value. For chart types with visible
	 * 		axes, a number of labels between the min and max values are calculated automatically; you can
	 * 		override the number of labels generated by setting 'xaxis.tickAmount'. To change how numeric x
	 * 		values are formatted, you can refer to one of the formatter functions in `TCv_Graph.js` by name in
	 * 		the chart's 'xaxis.labels.formatter' option, or you can add your own formatter function to that
	 * 		file. See {@link https://apexcharts.com/docs/options/xaxis/#formatter ApexCharts docs} for the
	 * 		function signature of formatters.
	 * DatetimeXVal: X values can be EITHER: integer unix timestamps in milliseconds; or datetime strings that
	 * 		can be parsed by javascript's Date.parse(). Some chart types may not render properly with this
	 * 		type; ex, pie charts. For chart types with visible axes, a number of labels between the min and
	 * 		max values are calculated automatically. To change how datetimes are formatted, see the
	 * 		{@link https://apexcharts.com/docs/datetime/ ApexCharts docs} on datetimes.
	 * 		NOTE: 'xaxis.tickAmount' has NO EFFECT on charts with this x value type.
	 *
	 * @param string $type The type of your data's x-values. One of class constants CategoryXVal, NumericXVal,
	 * or DatetimeXVal.
	 */
	public function setXValueType(string $type) : void
	{
		if (!self::isStringValidXValueType($type))
		{
			$this->addConsoleError("Invalid type for x-values. Please use one of the class constants ending in 'XVal'.");
			return;
		}

		$this->updateConfig(['xaxis' => ['type' => $type]]);
	}
	
	public function applyAxisFormatter($axis, $formatter)
	{
		$this->updateConfig([$axis => ['labels' => ['formatter' => $formatter]]]);
		
	}
	public function setYAxisAsInteger()
	{
		$this->applyAxisFormatter('yaxis','formatter_NumberAsInteger');
	}
	
	public function setXAxisAsInteger()
	{
		$this->applyAxisFormatter('xaxis','formatter_NumberAsInteger');
	}
	
	public function setYAxisAsPercent()
	{
		$this->applyAxisFormatter('yaxis','formatter_NumberAsPercent');
	}
	
	public function setXAxisAsPercent()
	{
		$this->applyAxisFormatter('xaxis','formatter_NumberAsPercent');
	}
	
	


	
	//////////////////////////////////////////////////////
	//
	// COURTESY FUNCTIONS FOR COMMON AND/OR COMPLICATED
	// CHART CONFIG
	//
	//////////////////////////////////////////////////////

	/**
	 * This function helps simplify the process of adding multiple y-axes to a chart. See the
	 * {@link https://apexcharts.com/docs/options/yaxis/ ApexCharts y-axis docs } for the input structure.
	 * @param array $options JSON configuration for a y-axis.
	 */
	public function addYAxis(array $options = []) : void
	{
		// If a default grid colour was set with setGridColor(), and the options don't override it, use that
		// colour for this axis' line and ticks.
		if (isset($this->gridColor) && !isset($options['axisBorder']['color']))
		{
			$options['axisBorder']['color'] = $this->gridColor;
		}
		if (isset($this->gridColor) && !isset($options['axisTicks']['color']))
		{
			$options['axisTicks']['color'] = $this->gridColor;
		}

		// If the chart has a singular y-axis in its config already (ie the yaxis key is an array whose keys
		// are NOT sequential 0-based keys), put it in an array instead, because we are now multi-axis.
		if (isset($this->config['yaxis'])
			&& (array_values($this->config['yaxis']) !== $this->config['yaxis']) )
		{
			$this->config['yaxis'] = [$this->config['yaxis']];
		}

		// Add this y-axis to the main config.
		$index = count((array)$this->config['yaxis']);
		$this->updateConfig(['yaxis' => [$index => $options]]);
	}

	/**
	 * For axis charts. Sets the colour of grid lines and axis ticks. Valid formats: hex, rgb, rgba.
	 * @param string $color
	 */
	public function setGridColor(string $color) : void
	{
		if (!TCu_Text::isValidColor($color))
		{
			$this->consoleAddWarning("Invalid grid color. Please use a string in hex, rgb, or rgba format.");
		}

		// apply colour to grid lines, x-axis line, and x-axis tick marks
		$updates = [
			'grid' => [ 'borderColor' => $color ],
			'xaxis' => [
				'axisBorder' => [ 'color' => $color ],
				'axisTicks' => [ 'color' => $color ]
			]
		];
		// retroactively apply to all existing y-axes' lines and tick marks
		foreach ((array)$this->config['yaxis'] as $a) {
			$updates['yaxis'][] = [
				'axisBorder' => [ 'color' => $color ],
				'axisTicks' => [ 'color' => $color ]
			];
		}

		// set as default for y-axes added in the future
		$this->gridColor = $color;

		// finally, update the main config
		$this->updateConfig($updates);
	}

	/**
	 * Simplifies the process of enabling/disabling interactivity like zooming, panning, and allowing chart
	 * downloads, and simultaneously showing/hiding the appropriate toolbar buttons for all of those.
	 * NOTE: As of ApexCharts v3.29.0 there is a bug where, when both x and y zooming are enabled, panning on
	 * the y-axis doesn't work properly.
	 *
	 * @param boolean $canZoomX For axis charts. If true, users can zoom and pan on the x axis.
	 * @param boolean $canZoomY For axis charts. If true, users can zoom and pan on the y axis.
	 * @param boolean $canDragZoom For axis charts. If true, users can click and drag a selection box on the
	 * chart and it will zoom to that selection.
	 * @param boolean $canDownload If true, users have the option to download the chart as a png, svg, or csv.
	 */
	public function setToolbar(bool $canZoomX = false, bool $canZoomY = false,
			bool $canDragZoom = false, bool $canDownload = false) : void
	{
		$this->updateConfig(
			[
				'chart' => [
					'toolbar' => [
						'show' => ($canZoomX || $canZoomY || $canDragZoom || $canDownload),
						'tools' => [
							'download' => $canDownload,
							'zoom' => (($canZoomX || $canZoomY) && $canDragZoom),
							'zoomin' => ($canZoomX || $canZoomY),
							'zoomout' => ($canZoomX || $canZoomY),
							'pan' => ($canZoomX || $canZoomY),
							'reset' => ($canZoomX || $canZoomY)
						]
					],
					'zoom' => [
						'enabled' => ($canZoomX || $canZoomY),
						'type' => ($canZoomX ? 'x' : '').($canZoomY ? 'y' : '')
					]
				]
			]
		);
	}

	/**
	 * Set whether all the UI is hidden or not. Not recommended for use with charts that have multiple y-axes.
	 * @param bool $spark If true, all UI, axes, etc are hidden; only the actual series are drawn.
	 */
	public function setSparkline(bool $spark = true) : void
	{
		$this->updateConfig( [ 'chart' => [ 'sparkline' => [ 'enabled' => $spark ] ] ] );
	}

	/**
	 * The default chart language is english, and a french locale is built in by default. If you require a
	 * different language, you'll have to load JSON for it with {@see addLocale()}.
	 *
	 * See one of the {@link ./defaults/locale_en.json default locale files } for the structure of a new locale.
	 * ApexCharts has {@link https://cdn.jsdelivr.net/npm/apexcharts/dist/locales/ many ready-made locales }
	 * you can load in if needed.
	 *
	 * @param string $locale The name of the locale to use. "en" (english) and "fr" (french) are available by default.
	 */
	public function setLocale(string $locale) : void
	{
		if (in_array($locale, array_column($this->config['chart']['locales'], 'name')) )
		{
			$this->updateConfig(['chart' => ['defaultLocale' => $locale]]);
		}
		else
		{
			$this->addConsoleError("Locale '{$locale}' doesn't exist or isn't properly formatted.");
		}
	}

	/**
	 * Allows you to add a locale to this chart, and set it to the current locale if you want.
	 *
	 * See one of the {@link ./defaults/locale_en.json default locale files } for the structure of a new locale.
	 * ApexCharts has {@link https://cdn.jsdelivr.net/npm/apexcharts/dist/locales/ many ready-made locales }
	 * you can load in if needed.
	 *
	 * @param array   $settings The locale you wish to add - decoded json.
	 * @param boolean $setAsCurrentLocale If true, the locale you're adding will be set as the current/default locale for this chart.
	 */
	public function addLocale(array $settings, bool $setAsCurrentLocale = false) : void
	{
		// Avoid empty value
		if(!isset($this->config['chart']['locales']))
		{
			$this->config['chart']['locales'] = [];
		}
		
		if (!in_array($settings['name'], array_column((array)$this->config['chart']['locales'], 'name')) )
		{
			$index = count((array)$this->config['chart']['locales']);
			$this->updateConfig(['chart' => ['locales' => [$index => $settings]]]);

			if($setAsCurrentLocale)
			{
				$this->setLocale($settings['name']);
			}
		}
		else
		{
			$this->addConsoleError("Locale '{$settings['name']}' already exists; please use the existing "
				."locale or pick a different name for this one.");
		}

	}


	//////////////////////////////////////////////////////
	//
	// OVERRIDING DEFAULT SETTINGS
	//
	//////////////////////////////////////////////////////

	/**
	 * A FULL OVERRIDE of ALL config options. Does not affect data series. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param array $config A complete ApexCharts configuration; decoded JSON.
	 */
	public function setConfig(array $config) : void
	{
		$this->config = $config;
	}

	/**
	 * A FULL OVERRIDE of ALL config options. Does not affect data  series. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $filepath_from_root A path to a JSON file containing a complete ApexCharts configuration.
	 */
	public function setConfigFromJSONFile(string $filepath_from_root) : void
	{
		$this->setConfig($this->getConfigFromJSONFile($filepath_from_root));
	}

	/**
	 * Override specific config options. Does not affect data series. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param array $options,... One or more partial ApexCharts configuration objects; decoded JSON.
	 */
	public function updateConfig(array ...$options) : void
	{
		$this->config = array_replace_recursive($this->config, ...$options);
	}

	/**
	 * Override specific config options. Does not affect data series. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $filepath_from_root A path to a JSON file containing a partial ApexCharts configuration.
	 */
	public function updateConfigFromJSONFile(string $filepath_from_root) : void
	{
		$this->updateConfig($this->getConfigFromJSONFile($filepath_from_root));
	}

	/**
	 * Override specific config options for a specific series type IN MIXED CHARTS. Should match the general
	 * structure of a regular ApexCharts config object. Use single-series values only. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $type The series type to update; see class constants ending in 'Type'
	 * @param array $options,... One or more partial ApexCharts configuration objects; decoded JSON.
	 */
	public function updateConfigForSeriesType(string $type, array ...$options) : void
	{
		if (!self::isStringSimpleChartType($type))
		{
			$this->addConsoleError("In updateConfigForSeriesType: '{$type}' is not a valid series type.");
			return;
		}
		$old_series_config = $this->configForSeriesType[$type] ?? [];
		$this->configForSeriesType[$type] = array_replace_recursive($old_series_config, ...$options);
	}

	/**
	 * Override specific config options for a specific series type IN MIXED CHARTS. Should match the general
	 * structure of a regular ApexCharts config object. Use single-series values only. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $type The series type to update; see class constants ending in 'Type'
	 * @param string $filepath_from_root A path to a JSON file containing a partial ApexCharts configuration.
	 */
	public function updateConfigForSeriesTypeFromJSONFile(string $type, string $filepath_from_root) : void
	{
		$this->updateConfigForSeriesType($type, $this->getConfigFromJSONFile($filepath_from_root));
	}

	/**
	 * Override ALL config options for a specific series type IN MIXED CHARTS. Should match the general
	 * structure of a regular ApexCharts config object. Use single-series values only. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $type The series type to update; see class constants ending in 'Type'
	 * @param string $filepath_from_root A path to a JSON file containing a partial ApexCharts configuration
	 * object.
	 */
	public function setConfigForSeriesType(string $type, array $options) : void
	{
		if (!self::isStringSimpleChartType($type))
		{
			$this->addConsoleError("In updateConfigForSeriesType: '{$type}' is not a valid series type.");
			return;
		}
		$this->configForSeriesType[$type] = $options;
	}

	/**
	 * Override ALL config options for a specific series type IN MIXED CHARTS. Should match the general
	 * structure of a regular ApexCharts config object. Use single-series values only. See the
	 * {@link https://apexcharts.com/docs/ ApexCharts docs } for more info.
	 * @param string $type The series type to update; see class constants ending in 'Type'
	 * @param string $filepath_from_root A path to a JSON file containing a partial ApexCharts configuration
	 * object.
	 */
	public function setConfigForSeriesTypeFromJSONFile(string $type, string $filepath_from_root) : void
	{
		$this->setConfigForSeriesType($type, $this->getConfigFromJSONFile($filepath_from_root));
	}


	//////////////////////////////////////////////////////
	//
	// CREATING THE FINAL CONFIG OBJECT
	//
	//////////////////////////////////////////////////////

	/**
	 * Combines the current configuration with the chart's data series and any custom series configuration and
	 * returns a finished config object that can be passed to the javascript.
	 * @return array
	 */
	protected function buildConfigObject() : array
	{
		// Error Checking: Warn the user if they forgot to set a chart type. Set a default chart type of Line.
		if (!isset($this->config['chart']['type']))
		{
			$this->addConsoleWarning("Chart type was not set. Defaulting to line chart.");
			$this->setAsLineChart();
		}

		// Error Checking: Warn if you gave a single-series chart type multiple series
		if (self::isChartTypeSingleSeries($this->config['chart']['type']) && count($this->series) > 1)
		{
			$type = self::chartTypeName($this->config['chart']['type']);
			$num = count($this->series);
			$this->addConsoleWarning("{$type} charts are single series only; {$num} series given.");
		}

		$final_config = $this->config; // The config object we're building to pass to the js.
		$chart_data = []; // Partial config object to hold formatted data.

		// If this chart is a sparkline and we have toolbar options... we shouldn't have toolbar options.
		if (isset($final_config['chart']['sparkline']) && $final_config['chart']['sparkline']['enabled'])
		{
			unset($final_config['chart']['toolbar']);
			unset($final_config['chart']['zoom']);
		}

		// Restructure the data how ApexCharts wants it.
		foreach ($this->series as $s)
		{
			$dat = $this->restructureSeries($s);
			$chart_data = array_merge_recursive($chart_data, $dat);
		}

		// If no x value type set, try to guess which to use based on the type of the first x value.
		// NOTE: Decided to never guess datetime x vals, because they're a niche case with strange behaviour.
		if (!isset($final_config['xaxis']['type']))
		{
			$first_x_val = 0;
			if(isset($chart_data['series'][0]['data'][0]['x']))
			{
				$first_x_val = $chart_data['series'][0]['data'][0]['x'];
			}
			if (is_int($first_x_val) || is_float($first_x_val))
			{
				$final_config['xaxis']['type'] = self::NumericXVal;
			}
			elseif (is_string($first_x_val))
			{
				$final_config['xaxis']['type'] = self::CategoryXVal;
			}
		}

		// For multi-series charts: repair any series who are missing x values by inserting null values.
		if (!self::isChartTypeSingleSeries($this->config['chart']['type']) && count($this->series) > 1)
		{
			// Collect the x values of all series, and check if we actually need to do any repair work at the
			// same time. If all arrays of x values are the same, we don't have to do any work.
			$series_x_vals = [];
			$repair_is_needed = false;
			$index = 0;
			foreach ($chart_data['series'] as $s)
			{
				$series_x_vals[] = array_column($s['data'], 'x');

				if (!$repair_is_needed)
				{
					$repair_is_needed = ($series_x_vals[0] != $series_x_vals[$index]);
				}
				$index += 1;
			}

			// If repair work IS required...
			if ($repair_is_needed)
			{
				// Find the ordered list of x values that should be present. Numeric and datetime x values sort
				// ascending, category x values preserve their existing order as much as possible.
				switch ($final_config['xaxis']['type'])
				{
					case self::NumericXVal:
						$required_x_vals = array_unique(array_merge(...$series_x_vals));
						sort($required_x_vals);
						break;

					case self::DatetimeXVal:
						$required_x_vals = array_unique(array_merge(...$series_x_vals));
						usort(
							$required_x_vals,
							function($time1, $time2)
							{
								$t1 = (is_string($time1) ? strtotime($time1) : $time1);
								$t2 = (is_string($time2) ? strtotime($time2) : $time2);
								return $t2 <=> $t1;
							}
						);
						break;

					case self::CategoryXVal:
						$required_x_vals = self::mergeLists(...$series_x_vals);
						break;

					// If no x value type was set and we couldn't guess one, inform the developer and go the less
					// computationally intensive route.
					default:
						$this->addConsoleError('No x value type was set or guessed for series repair. Ensure that '
							.'all x values in the data are the same type (which must be either string, int, or '
							.'float). If this warning still appears, try using setXValueType().');
						$required_x_vals = array_unique(array_merge(...$series_x_vals));
						sort($required_x_vals);
						break;
				}

				// Backfill each series with null values at every missing x value.
				foreach ($required_x_vals as $new_x)
				{
					$empty_datapoint = ['x' => $new_x, 'y' => null];
					if ($this->config['chart']['type'] === self::BubbleType)
					{
						$empty_datapoint['z'] = null;
					}

					$count = count($chart_data['series']);
					for ($i=0; $i < $count; $i++)
					{
						$col = array_column($chart_data['series'][$i]['data'], 'x' );
						if (!in_array($new_x, $col))
						{
							$chart_data['series'][$i]['data'][] = $empty_datapoint;
						}
					}
				}

				// Sort each series according to the original ordering of the values.
				$count = count($chart_data['series']);
				for ($i=0; $i < $count; $i++)
				{
					usort(
						$chart_data['series'][$i]['data'],
						function($val1, $val2) use ($required_x_vals)
						{
							$priority1 = array_search($val1['x'], $required_x_vals);
							$priority2 = array_search($val2['x'], $required_x_vals);
							return $priority1 <=> $priority2;
						}
					);
				}

			} // end if

		} // end of series repair

		// For mixed charts: Apply any per-series-type configuration. Use regular chart config for default
		// values where possible.
		if (isset($this->mixedType))
		{
			foreach ($chart_data['series'] as $i => $series)
			{
				if (isset($this->configForSeriesType[$series['type']]))
				{
					self::applyConfigAtSeriesIndex(
						$i, $final_config, $this->configForSeriesType[$series['type']], $this->config
					);
				}
			}
		}

		// Combine the data and with the other settings and return it all as a finished config array.
		return array_replace_recursive($final_config, $chart_data);
	}

	/**
	 * Given a series of data, in either associative format or array-of-arrays format, restructures the data
	 * into a structure that ApexCharts will always accept.
	 * @param  array $series The series to restructure.
	 * @return array A partial config object containing the restructured series.
	 */
	protected function restructureSeries(array $series) : array
	{
		$result = [];
		$type = $this->config['chart']['type'];

		// If this is a mixed chart, use series type instead of chart type
		if (isset($this->mixedType))
		{
			$valid_series_types = explode('/', $this->mixedType);
			// If this series doesn't have a type, choose a default for it.
			if (!isset($series['type']) || !in_array($series['type'], $valid_series_types))
			{
				$this->addConsoleWarning("Series '{$series['name']}' has no type; defaulting to '{$valid_series_types[0]}'.");
				$series['type'] = $valid_series_types[0];
			}
			$type = $series['type'];
		}

		$dimensions = self::numDimensionsInChartType($type);

		// Determine data format. If invalid format, inform developer. We will return empty data later.
		$is_associative = self::isDataInAssociativeFormat($type, $series['data']);
		$is_array_of_arrays = self::isDataInArrayOfArraysFormat($type, $series['data']);
		if (!$is_associative && !$is_array_of_arrays)
		{
			$name = self::chartTypeName($type);
			$this->addConsoleError("Invalid data for series '{$series['name']}'. {$name} series need datapoints "
				."with exactly {$dimensions} dimensions. Please ensure all datapoints have the same structure "
				."and have exactly the required number of dimensions.");
		}

		switch ($type)
		{
			// Single-series chart types need a special format for their data.
			case self::PieType:
			case self::DonutType:
			case self::RadialBarType:
			case self::PolarAreaType:
				// deal with erroneously formatted data
				if (!$is_associative && !$is_array_of_arrays)
				{
					return ['labels' => [], 'series' => []];
				}
				// deal with properly formatted data
				foreach ($series['data'] as $k => $v)
				{
					if ($is_associative)
					{
						$result['labels'][] = $k;
						$result['series'][] = (is_array($v) ? $v[0] : $v);
					}
					else {
						$result['labels'][] = $v[0];
						$result['series'][] = $v[1];
					}
				}
				return $result;

			// Bubble charts get a { x: val, y: val, z: val } structure for datapoints
			case self::BubbleType:
				$map_associative = function($k, $v)
				{
					return ['x' => $k, 'y' => $v[0], 'z' => $v[1]];
				};
				$map_array_of_array = function($v)
				{
					return ['x' => $v[0], 'y' => $v[1], 'z' => $v[2]];
				};
				// Do NOT break or return! Must fall through to default case.

			// All other types get a {x: val, y: val} or {x: val, y: [val, val, ...]} structure for datapoints
			default:
				if (!isset($map_associative))
				{
					$map_associative = function($k, $v) use ($dimensions)
					{
						$yval = ($dimensions == 2 && is_array($v)) ? $v[0] : $v;
						return ['x' => $k, 'y' => $yval];
					};
					$map_array_of_array = function($v)
					{
						$xval = array_shift($v); // get first value and remove from array
						return ['x' => $xval, 'y' => $v[0]];
					};
				}

				// deal with erroneously formatted data
				if (!$is_associative && !$is_array_of_arrays)
				{
					return ['series' => [['name' => $series['name'], 'data' => []]]];
				}

				// deal with properly formatted data
				$result = ['name' => $series['name']];
				$result['data'] =
					($is_array_of_arrays)
					? array_map($map_array_of_array, $series['data'])
					: array_map($map_associative, array_keys($series['data']), array_values($series['data']));
				if (isset($this->mixedType))
				{
					$result['type'] = $series['type'];
				}
				return ['series' => [$result]];
		} // end switch
	}

	/**
	 * Recursively updates existing settings in a config array for the given series index with new settings
	 * in another config array. Any settings which lack a value for any particular series will be backfilled
	 * with a default setting from the provided default config array, or with null if there is no default.
	 * NOTE: Used to apply settings to each different series by type in mixed charts.
	 *
	 * Built-in PHP functions for working recursively with arrays wouldn't work here for roughly 3 reasons:
	 * we're using keys from two separate arrays to do work, most of the cases where we need to stop stepping
	 * deeper are not when we encountar a scalar value, and we need to do slightly different work for each case.
	 *
	 * @param int $seriesNum The series index the new settings will be put at.
	 * @param array $base A base config array to be updated. Passed by reference - the contents are updated
	 * directly, there is no return value on this function.
	 * @param array $new A partial config array of new settings to update $base with. Should match the general
	 * structure of a regular ApexCharts config object. Use single-series values.
	 * @param array $default OPTIONAL. A partial config array of default settings, which will be used to fill
	 * in empty/unset series indices in $base. Use single-series values. If this does not provide a default
	 * value for any particular setting, NULL is used instead.
	 */
	protected static function applyConfigAtSeriesIndex(int $seriesNum, &$base, $new, $default = NULL)
	{
		// Get keys from the config we're updating, and from the new settings being applied.
		$keys = array_unique(array_merge( array_keys((array)$base), array_keys((array)$new) ));

		foreach($keys as $k)
		{
			$backfill = (isset($default[$k]) ? $default[$k] : NULL);

			// If the current setting is an array of leaves:
			// - Update the current series index with the new setting, if there is a new setting.
			// - Backfill all unset series indices with the default, or null if there is no default.
			if (self::isValMultiSeriesSetting($base[$k]))
			{
				if (isset($new[$k]) && self::isValSingleSeriesSetting($new[$k]))
				{
					$base[$k][$seriesNum] = $new[$k];
				}

				$base[$k] = $base[$k] + array_fill_keys(range(0,max($seriesNum, ...array_keys($base[$k]))), $backfill);
				ksort($base[$k]);
			}
			// If we have a new setting to apply, AND the current setting is either unset or a single-series val:
			// Turn this setting into a multi-series value and update the series index we need to.
			// Fill in any blanks with the default value, or null if there is no default.
			elseif (self::isValSingleSeriesSetting($new[$k])
				&& (self::isValSingleSeriesSetting($base[$k]) || !isset($base[$k])))
			{
				$base[$k] = [$base[$k]];

				$base[$k][$seriesNum] = $new[$k];
				$base[$k] = $base[$k] + array_fill_keys(range(0,max($seriesNum, ...array_keys($base[$k]))), $backfill);
				ksort($base[$k]);
			}
			// If we didn't do any of that, and base[k] isn't a leaf, go deeper.
			elseif (!self::isValSingleSeriesSetting($base[$k]) && !self::isValMultiSeriesSetting($base[$k]))
			{
				self::applyConfigAtSeriesIndex($seriesNum, $base[$k], $new[$k], $default[$k]);
			}
		}
	}


	//////////////////////////////////////////////////////
	//
	// CONFIGURATION TEMPLATES
	//
	// The view has a series of default templates which can be loaded.
	// Additional templates can be loaded as well.
	//
	//////////////////////////////////////////////////////

	/**
	 * Loads a JSON file from a given path from the site root, and returns a decoded array. This is used in
	 * configuring the view.
	 * @param string $filepath_from_root The path to the file from the site root. Likely starts with /admin/
	 * @return array The contents of the file as decoded json
	 */
	protected function getConfigFromJSONFile(string $filepath_from_root) : array
	{
		$content = file_get_contents($_SERVER['DOCUMENT_ROOT'].$filepath_from_root);
		return json_decode($content, true);
	}

	/**
	 * Return a default template config based on the collection of json files located in this view's class
	 * folder. Only the filename is necessary.
	 * @param string $filename The name of the default file to load - must be within folder TCv_Graph/defaults
	 * @return array The contents of the default file as decoded json.
	 */
	protected function getDefaultConfig(string $filename) : array
	{
		if(strpos($filename,'.json') === false)
		{
			$filename .= '.json';
		}
		return $this->getConfigFromJSONFile($this->classFolderFromRoot('TCv_Graph').'/defaults/'.$filename);
	}


	//////////////////////////////////////////////////////
	//
	// ERROR CHECKING & OTHER HELPERS
	//
	//////////////////////////////////////////////////////

	/**
	 * @param  string $type A chart type. See class constants ending in 'Type' and 'MixedType'.
	 * @return string A human-friendly name for the given chart type.
	 */
	public static function chartTypeName(string $type) : string
	{
		if (self::isStringSimpleChartType($type))
		{
			return ucwords(implode(' ', preg_split('/(?=[A-Z])/',$type)));
		}
		elseif (self::isStringMixedChartType($type))
		{
			$arr = explode('/', $type);
			foreach ($arr as &$t)
			{
				$t = ucwords(implode(' ', preg_split('/(?=[A-Z])/',$t)));
			}
			return implode('/', $arr);
		}
		return '';
	}

	/**
	 * Merges any number of lists (ie arrays with sequential integer keys starting from 0) with overlapping
	 * items, preserving the order of the items. WARNING: If any list contains no elements which overlap with
	 * any other elements of any other list, those items will retain their relative order but will be inserted
	 * between other elements near the beginning of the returned list.
	 *
	 * The implementation follows several rules:
	 * Rule 1: If A comes before B in all lists where they occur together, then A must precede B in the output
	 * Rule 2: If A and B change their relative order in different lists, then their relative order in the
	 *         final list is taken from the first list they appear in together
	 * Rule 3: If A and B do not occur together in ANY list, their relative order is decided by who has the
	 *         earlier indice in the first list they each occur in
	 *
	 * @param  array $lists,... Any number of lists to merge.
	 * @return array A merged list with the ordering of the original lists preserved according to the rules in
	 * this function's long description
	 */
	protected static function mergeLists(array ...$lists) : array
	{
		// If there's only one list, there's no work to do...
		if (count($lists) === 1)
		{
			return $lists[0];
		}

		// Step 0: Define an anonymous function that's only really useful inside of this specific function.
		$get_preceding_items =
			function ($item, $list)
			{
				$index = array_search($item, $list);
				if ($index === false) // item was not in list
				{
					return [];
				}
				return array_slice($list, 0, $index);
			};

		// Step 1: Find the set of all unique items.
		$unique_items = array_unique( array_merge(...$lists) ); // list
		$item_predecessors = []; // dict
		$item_priorities = []; // dict

		// Step 2: For each item, find the set of all items which precede it.
		// The array_reverse makes sure our Rule 2 check works properly.
		foreach (array_reverse($unique_items) as $item)
		{
			// Find the set of items which precede this item, in all lists.
			$preceding_items = [];
			foreach($lists as $list)
			{
				$preceding_items = array_merge($preceding_items, $get_preceding_items($item, $list));
			}
			$preceding_items = array_unique($preceding_items);

			// Obeying Rule 2. For every preceding item p_item, if we have a list of predecessors for p_item
			// and that list contains item, then we must remove p_item from item's predecessor list; item
			// precedes p_item, so p_item cannot precede item.
			$item_predecessors[$item] =
				array_filter(
					$preceding_items,
					function($p_item) use ($item_predecessors, $item)
					{
						return !array_key_exists($p_item, $item_predecessors)
							|| !in_array($item, $item_predecessors[$p_item]);
					}
				);
		}

		// Step 3: For each item, calculate the item's priority in the final list ordering based on its predecessors.
		$unchecked_items = $unique_items;
		$max_loops = count($unique_items)*2;
		$counter = 0;
		while(!empty($unchecked_items) && $counter < $max_loops)
		{
			foreach ($unchecked_items as $item)
			{
				$predecessors = $item_predecessors[$item];
				// If item has NO predecessors, its priority is 0 (the highest priority)
				if (count($predecessors) === 0)
				{
					$item_priorities[$item] = 0;
				}
				// If item HAS predecessors, its priority is (max priority out of its predecessors) + 1.
				else
				{
					// (but only calculate this if we have a priority for ALL preceding items already)
					if (array_diff($unchecked_items, $predecessors) === $unchecked_items)
					{
						$max_pred_priority = -1;
						foreach ($predecessors as $p)
						{
							$max_pred_priority = max($max_pred_priority, $item_priorities[$p]);
						}
						$item_priorities[$item] = $max_pred_priority + 1;
					}
				}
			}
			// Update list of unchecked items
			$unchecked_items = array_diff($unique_items, array_keys($item_priorities));
			$counter++;
		}

		// Step 4: Sort the set of unique items by the priorities and return. If two items have equal
		// priority, they retain their current relative ordering.
		usort(
			$unique_items,
			function($val1, $val2) use ($item_priorities, $unique_items)
			{
				$order = $item_priorities[$val1] <=> $item_priorities[$val2];
				if ($order === 0)
				{
					return array_search($val1, $unique_items) <=> array_search($val2, $unique_items);
				}
				return $order;
			}
		);
		return $unique_items;
	}

	
	/**
	 * @param string $chart_type The type to check.
	 * @return bool True if the given string is a chart type; both simple and mixed types will pass. See class
	 * constants ending in 'Type' and 'MixedType'.
	 */
	public static function isStringChartType(string $chart_type) : bool
	{
		return self::isStringMixedChartType($chart_type)
			|| self::isStringSimpleChartType($chart_type);
	}

	/**
	 * @param string $chart_type The type to check.
	 * @return bool True if the given string is a simple (ie non-mixed) chart type. See class constants ending in 'Type'.
	 */
	public static function isStringSimpleChartType(string $chart_type) : bool
	{
		return in_array(
			$chart_type,
			[
				self::AreaType, self::LineType, self::BarType, self::ScatterType, self::BubbleType,
				self::CandlestickType, self::PieType, self::DonutType, self::RadialBarType,
				self::RadarType, self::HeatmapType, self::RangeBarType, self::BoxplotType,
				self::TreemapType, self::PolarAreaType
			],
			true //strict comparison
		);
	}

	/**
	 * @param  string $chart_type The type to check.
	 * @return bool True if the given string is a mixed chart type. See class constants ending in 'MixedType'.
	 */
	public static function isStringMixedChartType(string $chart_type) : bool
	{
		return in_array(
			$chart_type,
			[
				self::LineBarAreaMixedType, self::ScatterLineMixedType,
				self::CandlestickLineMixedType, self::BoxplotScatterMixedType
			],
			true //strict comparison
		);
	}

	/**
	 * @param string $chart_type The chart type this series is for. Required to determine the minimum number
	 * of dimensions each datapoint must have.
	 * @param array $data The data to check the format of.
	 * @return bool True if every datapoint in this data array matches the associative input format; x values
	 * are array keys, and all other values are in the array value. Ex: x => y, or x => [start, end].
	 */
	public static function isDataInAssociativeFormat(string $chart_type, array $data) : bool
	{
		$dimensions = self::numDimensionsInChartType($chart_type);
		return self::doesDataHaveCorrectDimensions($dimensions-1, $data); // the minus 1 is important.
	}

	/**
	 * @param string $chart_type The chart type this series is for. Required to determine the minimum number
	 * of dimensions each datapoint must have.
	 * @param array $data The data to check the format of.
	 * @return bool True if every datapoint in this data array matches the array-of-arrays input format; each
	 * datapoint is an array where the first index is the x dimension. Ex: [x, y] or [x, start, end].
	 */
	public static function isDataInArrayOfArraysFormat(string $chart_type, array $data) : bool
	{
		$dimensions = self::numDimensionsInChartType($chart_type);
		return (self::doesDataHaveCorrectDimensions($dimensions, $data) && array_values($data) === $data);
	}

	/**
	 * @param int $number_of_dimensions The number of dimensions the value of each datapont should have.
	 * @param array $data The data array to check the datapoints of.
	 * @return bool True if all datapoints in the given data array have EXACTLY the given number of dimensions.
	 */
	protected static function doesDataHaveCorrectDimensions(int $number_of_dimensions, array $data) : bool
	{
		$is_data_valid = true;
		foreach ($data as $key => $val) {
			// valid values are: null; a scalar value (iff only one dimension); or an array with length == dimensions
			if ($val !== NULL
				&& !(!is_array($val) && $number_of_dimensions == 1)
				&& !(is_array($val) && count($val) == $number_of_dimensions) )
			{
				$is_data_valid = false;
				break;
			}
		}
		return $is_data_valid;
	}

	/**
	 * Determine what the required number of dimensions in a datapoint is for a specific chart type. Essentially
	 * just flips the class constant NumDimensionsInDatapoint to make it easier to search by type.
	 * @param string $chart_type The type to check. One of the class constants ending in 'Type'
	 * @return int The number of dimensions each datapoint should have in a chart of this type, or 0 if the
	 * given type was not actually a type.
	 */
	protected static function numDimensionsInChartType(string $chart_type) : int
	{
		foreach (self::NumDimensionsInDatapoint as $min => $chart_types)
		{
			if (in_array($chart_type, $chart_types))
			{
				return $min;
			}
		}
		//$this->addConsoleError("Can't find required number of dimensions for invalid chart type '{$chart_type}'.");
		return 0;
	}

	/**
	 * Checks if a chart type can only support a single series. Single-series chart types require a different
	 * final structure for their data than all other chart types - see {@see restructureSeries()}.
	 * @param string $type A chart type. See class constants ending in 'Type'.
	 * @return bool True if the given string is a single-series chart type, false otherwise.
	 */
	public static function isChartTypeSingleSeries(string $type) : bool
	{
		return in_array($type, [self::PieType,self::DonutType,self::RadialBarType,self::PolarAreaType]);
	}

	/**
	 * @param string $type An x value type. See class constants ending in 'XVal'.
	 * @return bool True if the given string is a valid x value type.
	 */
	public static function isStringValidXValueType(string $type) : bool
	{
		return in_array($type, [self::CategoryXVal,self::NumericXVal,self::DatetimeXVal]);
	}

	/**
	 * For the purposes of recursion in {@see applyConfigAtSeriesIndex()}, we consider something in a config
	 * object to be a setting-value for a single series if it's either a scalar value, or a list (ie, array
	 * whose keys are sequential, numeric, and start from 0).
	 * @param  mixed $val A fragment of a config array to test.
	 * @return bool
	 */
	protected static function isValSingleSeriesSetting($val) : bool
	{
		return isset($val) && (!is_array($val) || (array_values($val) === $val) );
	}

	/**
	 * For the purposes of recursion in {@see applyConfigAtSeriesIndex()}, we consider something in a config
	 * object to be a setting-value for multiple series if it's an array whose keys are ALL numeric, and whose
	 * members are either null or pass {@see isValSingleSeriesSetting()}.
	 * @param  mixed $val A fragment of a config array to test.
	 * @return bool
	 */
	protected static function isValMultiSeriesSetting($val) : bool
	{
		$member_is_null_or_singleseries =
			function ($member)
			{
				return is_null($member) || self::isValSingleSeriesSetting($member);
			};

		return
			is_array($val)
			&& count(array_filter(array_keys($val), 'is_string')) === 0
			&& count(array_filter($val, $member_is_null_or_singleseries)) === count($val);
	}
	
	
	
	public function setAnimate(bool $animate = true)
	{
		$this->updateConfig(['chart' => [ 'animations' => [ 'enabled' => $animate ]]]);
		
		
	}
	
	
	
	/*
		   /_)
		>O(__)}}-
		   \_)

	Oh dear, there's a bug in this code!
	Don't worry though. This one is friendly.
	*/
}
