/**
 * This is a JS class that corresponds to the view with the same
 * name. It is automatically added to the <head> and instantiated.
 * The values come in via the constructor.
 *
 * Everything else is done inside this view.
 */
class TCv_Graph {

	constructor(element, values = {}) {
		this.element = element;
	    this.target = document.querySelector("#"+values.target);
	    this.config = values.config;

		// Apply any custom formatters this class has that were requested.
		this.applyFormatters(this.config);

		if(this.target && this.config)
		{
			// Render the chart in the target element
			this.chart = new ApexCharts(this.target, this.config);
			this.chart.render();
		}

	}

	//////////////////////////////////////////////////////
	//
	// FORMATTERS
	//
	// Various custom formatter functions that an
	// ApexCharts chart can use to format values.
	//
	//////////////////////////////////////////////////////

	/**
	 * Recursively searches the given object for properties called "formatter" that have a string value that
	 * is the name of a formatter function in this class, and swaps out the string name for the actual
	 * function. Intended to help with setting custom formatter functions for anything that will take them.
	 * @param  {object} obj The object to apply formatters to.
	 * @return {void}
	 */
	applyFormatters(obj) {
		for (let k in obj) {
			if (obj[k] !== null && typeof obj[k] == "object" && k !== "series"){
				this.applyFormatters(obj[k]);
			}
			else if (k == "formatter") {
				if (typeof obj[k] == "string" && obj[k].substring(0, 10) == "formatter_"
						&& typeof this[obj[k]] == "function" ){
					obj[k] = this[obj[k]];
				} else {
					obj[k] = undefined;
				}
			}
		}
	}

	/**
	 * Formats a number as a percentage with two decimal points.
	 * @param val The value to format.
	 * @return {string} The formatted value.
	 */
	formatter_NumberAsInteger(val) {
		return val.toFixed(0);
	}
	/**
	 * Formats a number as a percentage with two decimal points.
	 * @param val The value to format.
	 * @return {string} The formatted value.
	 */
	formatter_NumberAsPercent(val) {
		return parseFloat(val).toFixed(2) + '%';
	}

	/**
	 * Returns the average of all series totals formatted as a percentage with two decimal points. This only
	 * exists because radialbar charts had ridiculous looking numbers for their total/average label.
	 * @param w
	 * @return {string} The formatted average.
	 */
	formatter_SeriesAverageAsPercent(w) {
		const avg = w.globals.seriesTotals.reduce((a, b) => { return a + b }, 0) / w.globals.series.length;
		return parseFloat(avg).toFixed(2) + '%';
	}

	/**
	 * Returns the average of all series totals for a radial bar. This requires the use of addRadialBarChartSeriesValues()
	 *
	 * This function uses the series values, not the percentages which are what's passed.
	 * @param w
	 * @return {string} The formatted average.
	 */
	formatter_RadialBarSeriesValueAverage(w) {
		// Use reduce() to add up all the values in the array
		const avg = w.config.radialBarValues.reduce((a, b) => { return a + b }, 0) / w.globals.series.length;

		return Math.round(avg);
	}

	/**
	 * Returns the average of all series totals for a radial bar. This requires the use of addRadialBarChartSeriesValues()
	 *
	 * This function uses the series values, not the percentages which are what's passed.
	 * @param w
	 * @return {string} The formatted average.
	 */
	formatter_RadialBarNumber(w) {
		return w;
	}



}
