<?php

/**
 * Class TCv_HTMLTable
 *
 * A view that represents a normal HTML table
 */
class TCv_HTMLTable extends TCv_View
{
	protected $current_table_body;
	protected $table_header;
	protected $table_footer;
	protected $column_classes = array();

	/**
	 * TCv_HTMLTable constructor.
	 * @param bool|string $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->setTag('table');
		$this->setHTMLCollapsing(false);
		
		$this->createNewTableBody();

		$this->table_header = new TCv_View();
		$this->table_header->setTag('thead');
		
		$this->table_footer = new TCv_View();
		$this->table_footer->setTag('tfoot');
		
	}
	
	/**
	 * Creates a new table body for this table
	 */
	public function createNewTableBody()
	{
		if(isset($this->current_table_body))
		{
			parent::attachView($this->current_table_body);
		}
		$this->current_table_body = new TCv_View();
		$this->current_table_body->setTag('tbody');
		
	
	}
	
	/**
	 * This function deals with all the columns that may need to be assigned to the cells.
	 *
	 * @param TCv_HTMLTableRow $table_row
	 */
	public function handleColumnClassesForTableRow($table_row)
	{
		$current_column = 1;
		foreach($table_row->attachedViews() as $table_cell)
		{
			if($table_cell instanceof TCv_HTMLTableCell)
			{
				if(isset($this->column_classes[$current_column]))
				{
					if(is_array($this->column_classes[$current_column])) // chekc if we actually have classes
					{
						$table_cell->addClassArray($this->column_classes[$current_column]);
					}
					
				}
				
				$current_column += $table_cell->colspan(); // jump that many columns. Usually 1.
			}
		}
	}
		

	/**
	 * Overrides the attachView method to handle adding content to the tbody element
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return bool|string|TCv_View|void
	 */
	public function attachView($view, $prepend = false)
	{
		if($view instanceof TCv_View)
		{
			if($view->tag() == 'tbody')
			{
				if(isset($this->current_table_body))
				{
					parent::attachView($this->current_table_body);
					unset($this->current_table_body);
				}
				parent::attachView($view);
				$this->createNewTableBody();
			}
			elseif($view instanceof TCv_HTMLTableRow)
			{
				$this->handleColumnClassesForTableRow($view);
				$this->current_table_body->attachView($view);
			}
		}
	}

	/**
	 * Attaches a row to the table head
	 * @param TCv_View $view
	 */
	public function attachHeadRow($view)
	{
		$this->table_header->attachView($view);
	}
	
	/**
	 * Attaches a row to the table head
	 * @param TCv_View $view
	 */
	public function attachHeaderRow($view)
	{
		$this->table_header->attachView($view);
	}
	
	/**
	 * Attaches a row to the footer head
	 * @param TCv_View $view
	 */
	public function attachFooterRow($view)
	{
		$this->table_footer->attachView($view);
	}
	
	/**
	 * Sets a class to be added to every column that is attached to this table for a given row
	 * @param int $column_number
	 * @param string $class
	 */
	public function addColumnClass($column_number, $class)
	{
		$this->column_classes[$column_number][] = $class;
	}
		
		
	/**
	 * Override HTML to deal with attaching the table body and header
	 * @return string
	 */
	public function html()
	{
		parent::attachView($this->table_header);
		parent::attachView($this->current_table_body);
		parent::attachView($this->table_footer);
		
		
		return parent::html();

	}
	
}

?>