<?php
/**
 * Class TCv_HTMLTableRow
 */
class TCv_HTMLTableRow extends TCv_View
{
	/**
	 * Creates a table row that can be added to a TCv_HTMLTable
	 * TCv_HTMLTableRow constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->setTag('tr');
		$this->setHTMLCollapsing(false);
	}
	
	
	/**
	 * Shortcut method to create a table cell heading with the content provided.
	 * @param string|TCv_View $content
	 * @param string|bool $classes (Optional) Default false. A string of CSS classes or false if none are to be applied.
	 */
	public function createHeadingCellWithContent($content, $classes = false)
	{
		$table_cell = new TCv_HTMLTableCellHeading();
		if($content instanceof TCv_View)
		{
			$table_cell->attachView($content);
		}
		else
		{
			$table_cell->addText($content);
		}

		if($classes)
		{
			$table_cell->addClass($classes);
		}
		$this->attachView($table_cell);
	}
	
	/**
	 * Shortcut method to create a table cell  with the content provided.
	 * @param string|TCv_View $content
	 * @param string|bool $classes (Optional) Default false. A string of CSS classes or false if none are to be
	 * applied.
	 */
	public function createCellWithContent($content, $classes = false)
	{
		$table_cell = new TCv_HTMLTableCell();
		if($content instanceof TCv_View)
		{
			$table_cell->attachView($content);
		}
		else
		{
			$table_cell->addText($content);
		}
		if($classes)
		{
			$table_cell->addClass($classes);
		}
		$this->attachView($table_cell);
	}
	
	/**
	 * Extends the attachView Method to ensure that items are either td or th elements
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return void
	 */
	public function attachView($view, $prepend = false)
	{
		if($view->tag() != 'td' && $view->tag() != 'th')
		{
			$view->setTag('td'); // for to be a td if nothing else
		}
		
		parent::attachView($view, $prepend);
	}
	
}

?>