<?php
/**
 * Class TCv_HTMLTableCell
 */
class TCv_HTMLTableCell extends TCv_View
{
	protected $colspan = 1;
	
	/**
	 * TCv_HTMLTableCell constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->setTag('td');
	}
	
	/**
	 * Sets the number of colspan for this cell
	 * @param int $span
	 */
	public function setColumnSpan($span)
	{
		$this->colspan = $span;
		$this->setAttribute('colspan',$span);
	}
	
	/**
	 * returns the colspan
	 * @return int
	 */
	public function colspan()
	{
		return $this->colspan;
	}
}

?>