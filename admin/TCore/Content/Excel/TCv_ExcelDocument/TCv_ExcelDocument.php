<?php
/**
 * Class TCv_ExcelDocument
 */
class TCv_ExcelDocument extends TCv_View
{
	protected $excel = false;
	protected $sheet = false;
	protected $current_row = 1;
	protected $current_sheet_num = -1;
	
	protected $filename = 'excel_export';
	
	/**
	 * TCv_ExcelDocument constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		// Create new PHPExcel object
		$this->excel = new PHPExcel();
		
		
		// Set properties
		$this->excel->getProperties()
			->setCreator("Tungsten")
			->setLastModifiedBy("Tungsten");
		$this->excel->removeSheetByIndex(0);
		
		
	}
	
	/**
	 * Returns the main PHPExcel Object
	 * @return bool|PHPExcel
	 */
	public function excel()
	{
		return $this->excel;
	}
	
		
	/**
	 * Moves the pointer to the next sheet
	 * @param $title
	 */
	public function nextSheet($title)
	{
		$title = substr($title, 0, 31); // maximum 31 characters
		$this->current_sheet_num++;
		$this->excel->createSheet(NULL, $this->current_sheet_num);
		$this->sheet = $this->excel->setActiveSheetIndex($this->current_sheet_num);
		$this->current_row = 1;		
		$this->sheet->setTitle($title);
						 

	}
	
	/**
	 * Adds a sheet to the page using a Model List View (TCv_ModelList)
	 * @param $list_view
	 */
	public function addSheetUsingList($list_view)
	{
		
		$title = substr('Not Set', 0, 31); // maximum 31 characters
		$this->current_sheet_num++;
		$this->excel->createSheet(NULL, $this->current_sheet_num);
		$this->sheet = $this->excel->setActiveSheetIndex($this->current_sheet_num);
		$this->current_row = 1;		
		$this->sheet->setTitle($title);
		
						 

	}
	
	/**
	 * Adds a worksheet to the document
	 * @param TCv_ExcelWorksheet $worksheet
	 */
	public function addWorksheet($worksheet)
	{
		$this->excel->addExternalSheet($worksheet->sheet());
		
	}
	
	/**
	 * Sets the filename, no need to pass extension
	 * @param string $filename
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;
	}
	
	/**
	 * generates the contents of this excel file to the screen. This also sets the headers for hte content type
	 */
	public function html()
	{
		$filename = $this->filename . ".xlsx";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename.'"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		$objWriter->save('php://output');
		exit();
	}
	
	
	


}
?>