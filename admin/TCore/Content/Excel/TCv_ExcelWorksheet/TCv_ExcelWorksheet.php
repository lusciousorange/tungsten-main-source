<?php
/**
 * Class TCv_ExcelWorksheet
 */
class TCv_ExcelWorksheet extends TCv_View
{
	protected $current_row = 1;
	protected $current_column = 'A';
	protected $furthest_column = 'A';
	
	protected $sheet_name = 'sheet';
	protected $cell_formats = array();
	protected $sheet = false;
	protected $title = false;
	protected $model_methods = array();
	protected $column_titles = array();
	
	protected $column_letter = array();
	
	protected $use_autosize = true;
	
	/**
	 * TCv_ExcelWorksheet constructor.
	 * @param string $title
	 */
	public function __construct($title = '')
	{
		parent::__construct();
		
		$excel = new PHPExcel();
		//include_once($_SERVER['DOCUMENT_ROOT'].'/admin/TCore/Content/Excel/PHPExcel/PHPExcel/Worksheet.php');
		if($title == '')
		{
			$title = 'Worksheet';
		}
		$this->title = $title;
		//$this->sheet = new PHPExcel_Worksheet($excel, $this->title);
		$this->sheet =  $excel->createSheet();
		$this->sheet->setTitle($this->title);
		
		$column_letter = 'A';
		for($i = 1; $i<50; $i++)
		{
			$this->column_letter[$i] = $column_letter;
			$column_letter++;
		}
		$this->addCellFormat('percentage', array('numberformat' => array('code' => '0%')));
		
		$this->addCellFormat('heading_1', array(
			'font' => array(
				'bold' => true,
				'size' => 18
				)
			)
		);
		
		$this->addCellFormat('centered', array(
			'alignment' => array(
				 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				)
			)
		);
		
		
		$this->addCellFormat('heading_2', array(
			'font' => array(
				'bold' => true,
				'size' => 16
				),
			'borders' => array(
				'bottom' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			
			)
		);
		
		$this->addCellFormat('heading_3', array(
			'font' => array(
				'bold' => true,
				'size' => 14
				),
			
			)
		);
		
		$this->addCellFormat('column_heading', array(
			'font' => array(
				'bold' => true,
				'size' => 12
				)
			)
		);
		
		if($this->use_autosize)
		{
			$this->sheet->getColumnDimension('A')->setAutoSize(true);
		}	
		
	}
	
	/**
	 * Turns off the autosize
	 */
	public function disableAutoSize()
	{
		$this->use_autosize = false;
	}

	/**
	 *	Returns the current sheet that is being worked on
	 * @return PHPExcel_Worksheet
	 */
	public function sheet()
	{
		return $this->sheet;
	}
	
	
	/**
	 * Sets a cell format for the sheet
	 * @param string $name The name of the cell format
	 * @param array $array The values for the format
	 */
	public function addCellFormat($name, $array)
	{
		$this->cell_formats[$name] = $array;
	}
	
	/**
	 * Returns the current row
	 * @return int
	 */
	public function row()
	{
		return $this->current_row;
	}
	
	/**
	 * Returns the current column name
	 * @return string
	 */
	public function column()
	{
		return $this->current_column;
	}
	
	/**
	 * Move the point to the next row
	 */
	public function nextRow()
	{
		$this->current_row++;
		$this->current_column = 'A';
	}
	
	/**
	 * Move the pointer to the next column
	 */
	public function nextColumn()
	{
		$this->current_column++;
		if($this->current_column > $this->furthest_column)
		{
			if($this->use_autosize)
			{
				$this->sheet->getColumnDimension($this->current_column)->setAutoSize(true);
			}
			$this->furthest_column = $this->current_column;
		}
	}
	
	/**
	 * Returns the current cell name
	 * @return string
	 */
	public function currentCell()
	{
		return $this->column().$this->row();
	}
	
	/**
	 * Adds content to the current cell in the sheet
	 * @param string $content The content to be added to the cell
	 * @param bool|string $format The name of the format (Optional) Default false for no format.
	 */
	public function addContentToCurrentCell($content, $format = false)
	{
		if(is_array($content))
		{
			TC_triggerError('content is array');
		}
		$this->sheet->setCellValue($this->currentCell(), strip_tags($content));
		if($format)
		{
			$this->sheet->getStyle($this->currentCell())->applyFromArray($this->cell_formats[$format]);
		
		}
	}
	
	/**
	 * Adds content to the current cell in the sheet then move to the next column
	 *
	 * @param string $content The content to be added
	 * @param bool|string $format The name of the format (Optional) Default false for no format.
	 *
	 * @uses TCv_ExcelWorksheet::addContentToCurrentCell()
	 * @uses TCv_ExcelWorksheet::nextColumn()
	 */
	public function addContentToCurrentCellAndMove($content, $format = false)
	{
		$this->addContentToCurrentCell($content, $format);
		$this->nextColumn();
	}
	
	
	/**
	 * Adds a sheet to the page using a Model List View (TCv_ModelList)
	 * @param TCv_ModelList $list_view The list view that will be used for data
	 */
	public function addValuesUsingListView($list_view)
	{
		foreach($list_view->columns() as $column)
		{
			$this->applyFormatToCurrentCell('column_heading');
			$this->addContentToCurrentCell($content);
			$this->nextColumn();
			
		}
		foreach($list_view->models() as $model)
		{
			foreach($list_view->columns() as $column)
			{
				$content = contentForColumnWithModel($column, $model);
				$this->addContentToCurrentCellAndMove($content);
			}
			$this->nextRow();
			
		}
		//$title = substr('Not Set', 0, 31); // maximum 31 characters
		
	
	}
	

	/**
	 * Applies a format to a given number of cells, starting with the current one
	 * @param string $format_name The name of the format, which must follow the cell formatting rules for arrays in PHP Excel and then be added using addCellFormat()
	 * @param int $total_number_of_cells (Optional) Default 1. The number of cells that this style is applied it. This
	 * won't move the "cursor" but it will apply those styles.
	 *
	 */
	public function applyFormatToCurrentCell($format_name, $total_number_of_cells = 1)
	{
		// apply the normal style
		$this->sheet->getStyle($this->currentCell())->applyFromArray($this->cell_formats[$format_name]);
		
		if($total_number_of_cells > 1)
		{
			$column = $this->column();
			for($column_num = 1; $column_num < $total_number_of_cells; $column_num++)
			{
				$column++;
				$cell = $column.$this->row();
				$this->sheet->getStyle($cell)->applyFromArray($this->cell_formats[$format_name]);
			}
		}	
	}

	/**
	 * Applies a format to a cell range
	 * @param string $format_name The name of the format to be applied
	 * @param string $cell_range The cell range such as "A1:A5"
	 */
	public function applyFormatToCells($format_name, $cell_range)
	{
		$this->sheet->getStyle($cell_range)->applyFromArray($this->cell_formats[$format_name]);
	}

	/**
	 * Returns the number of defined columns in this worksheet
	 * @return int
	 */
	public function numColumns()
	{
		if(sizeof($this->column_titles) > 0)
		{
			return sizeof($this->column_titles);
		}
		return 1;
	}
	
	/**
	 * Adds a column heading title to the list of column headings. This function won't output anything but it will save the name of the title for the column.
	 * @param $title
	 */
	public function defineColumnTitle($title)
	{
		$this->column_titles[] = $title;
	}
	
	/**
	 * Adds the column headings that are set using the defineRowUsingModelMethods() method.
	 * @param string $style (Optional) Default "heading_2". The style  name to be used
	 */
	public function addModelColumnHeadings($style = 'heading_2')
	{
		if($this->column() != 'A')
		{
			$this->nextRow();
		}
		
		$this->applyFormatToCurrentCell('column_heading', $this->numColumns());
		foreach($this->column_titles as $title)
		{
			$this->addContentToCurrentCellAndMove($title);
		}
	}

	/**
	 * Adds a row to the sheet which will create a merged row of all the defined columns and style it using the provided style.
	 * @param string $content
	 * @param string $style
	 */
	public function addStyledHeadingRow($content, $style)
	{
		if($this->column() != 'A')
		{
			$this->nextRow();
		}
		// find the range starting from the current cell to the number of columns
		$range = $this->column().$this->row().':'.$this->column_letter[$this->numColumns()].$this->row();
		
		// merge the cells
		$this->sheet->mergeCells($range);
		
		// add the content
		$this->addContentToCurrentCell($content);
	
		// style the cell
		$this->applyFormatToCurrentCell($style,$this->numColumns());
		
		// next row
		$this->nextRow();	
	}
	

	/**
	 * A method to identify how the columns for this sheet should be labelled and identified.
	 *
	 * This is useful there is a list of models and methods are definded within that model to return values that align with columns in the sheet.
	 * @param array $values A list of key=>value pairs in which the Key is the title of the column and the Value is the method name that should be called for the model.
	 */
	public function defineRowUsingModelMethods($values)
	{
		foreach($values as $column_title => $method_name)
		{
			$this->defineColumnTitle($column_title);
			$this->defineModelMethodAsColumnValue($method_name);
		}
	}

	/**
	 * Adds a row for a given model.
	 *
	 * This function will call the method names for the various column names already setup and add those values returned
	 * from those functions. This function will move to the next line if mid-entry.
	 * @param TCm_Model $model THe model
	 */
	public function addRowForModel($model)
	{
		if($this->column() != 'A')
		{
			$this->nextRow();
		}
			
		foreach($this->model_methods as $method_name)
		{
			$this->addContentToCurrentCellAndMove(strip_tags($model->$method_name()));
		}
		
		$this->nextRow();		
		

	}
	
	/**
	 * Sets up the worksheet to use a model method as what should be called and added as the content for a column. The order in which you add the methods will correlate to the column order.
	 * @param string $method_name
	 */
	public function defineModelMethodAsColumnValue($method_name)
	{
		$this->model_methods[] = $method_name;
	}
	
	/**
	 * Sets up the worksheet to use a blank value for the column value. The order in which you add the methods will correlate to the column order.
	 */
	public function addBlankAsColumnValue()
	{
		$this->model_methods[] = false;
	}
	
	
	/**
	 * This function can be called on an individual worksheet, instead of adding the worksheet to a larger document.
	 * IF the worksheet is added to a Excel Document, then this function is never called.
	 * @return string|void
	 */
	public function html()
	{
		
		$document = new TCv_ExcelDocument();
		$document->setFilename($this->title);
		$document->addWorksheet($this);
		return $document->html();

	}
	

}
?>