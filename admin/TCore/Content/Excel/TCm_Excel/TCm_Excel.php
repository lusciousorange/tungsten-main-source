<?php
/**
 * Class TCm_Excel
 */
class TCm_Excel extends PHPExcel
{
	use TCt_DatabaseAccess, TCt_FactoryInit, TSt_ConsoleAccess;
	
	protected $excel = false; // legacy support
	protected $sheet = false;
	protected $current_row = 1;
	protected $current_sheet_num = 0;
	protected $last_column_used = 'A';
	protected $last_column_value = 1; // tracks the value of the last column used
	
	protected $filename = 'filename';
	
	/**
	 * TCm_Excel constructor.
	 * @param array|int $id Legacy support, no longer used
	 */
	public function __construct($id)
	{
		parent::__construct();
		
		// Legacy support
		$this->excel = $this;
		
		// Set properties
		$this->getProperties()
			->setCreator("Luscious Orange")
			->setLastModifiedBy("Luscious Orange");
		
		
		// Save the sheet
		$this->sheet = $this->setActiveSheetIndex($this->current_sheet_num);
		
		
		
		
	}
	
	/**
	 * Returns the main PHPExcel Object
	 * @return PHPExcel
	 */
	public function excel()
	{
		return $this->excel;
	}
	
	/**
	 * Returns the current row
	 * @return int
	 */
	public function row()
	{
		return $this->current_row;
	}
	
	/**
	 * Move the pointer to the next row
	 */
	public function nextRow()
	{
		$this->current_row++;
	}
	
	/**
	 * Move the pointer to a new sheet with the provided title
	 * @param string $title The title of the new sheet
	 */
	public function nextSheet($title)
	{
		$title = substr($title, 0, 31); // maximum 31 characters
		$this->current_sheet_num++;
		$this->createSheet(NULL, $this->current_sheet_num);
		$this->sheet = $this->excel->setActiveSheetIndex($this->current_sheet_num);
		$this->current_row = 1;		
		$this->sheet->setTitle($title);
		
		$this->last_column_used = 'A';
						 

	}
	
	/**
	 * Sets the value for a given cell with
	 * @param string $cell
	 * @param string $value
	 * @param string|bool $style (Optional) Default false. the style to be applied
	 */
	public function setCellValue($cell, $value, $style = false)
	{
		$this->sheet->setCellValue($cell, $value);
		
		if($style)
		{
			$this->setCellStyle($cell, $style);
		}
		
		$letters = preg_replace('/[0-9]+/', '', $cell);
		$column_value = $this->columnValue($letters);
		// Deal with finding the last column used
		if($column_value > $this->last_column_used)
		{
			$this->sheet->getColumnDimension($letters)->setAutoSize(true);
			$this->last_column_used = $letters;
			$this->last_column_value = $column_value;
			
		}
		
	}
	
	/**
	 * Sets the styles for cells
	 * @param string $cell_range The cell range or single cell value
	 * @param string|array $style
	 * @throws PHPExcel_Exception
	 */
	public function setCellStyle($cell_range, $style)
	{
		if(is_string($style))
		{
			$this->sheet->getStyle($cell_range)->getNumberFormat()->setFormatCode($style);
		}
		elseif(is_array($style))
		{
			$this->sheet->getStyle($cell_range)->applyFromArray($style);
		}
	}
	
	/**
	 * Sets the filename
	 * @param string $filename The filename without an extension
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;
	}
	
	/**
	 * Generates the contents of this excel file to the screen. This also sets the headers for hte content type
	 */
	public function generate()
	{
		$filename = $this->filename . ".xlsx";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename.'"');
		header('Cache-Control: max-age=0');
	
		$objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
		$objWriter->save('php://output');
	}
	
	
	/**
	 * Internal function to calculate the value of a column for comparison
	 * @param string $cell The cell name such as 'A5' or 'ZZ4'
	 * @return int The "value" of that column
	 * @see https://stackoverflow.com/questions/8849477/how-can-i-calculate-the-sum-of-letter-numbers-in-a-string
	 */
	protected function columnValue($cell)
	{
		$count = 0;
		
		// remove non letters, spaces, then move to uppercase
		$cell = preg_replace('/[0-9]+/', '', $cell);
		$cell = strtoupper(trim($cell));
		$num_letters = strlen($cell);
		
		for($i = 0; $i < $num_letters; $i++)
		{
			$count += ord($cell[$i]) - 64;
		}
		
		return $count;
	}
	
	/**
	 * @param TCm_File $excel_file
	 *
	 */
	public static function initFromFile(TCm_File $excel_file)
	{
		if($excel_file->exists())
		{
			$filename = $excel_file->filenameFromServerRoot();
			
			//  Read your Excel workbook
			try
			{
				// @see https://stackoverflow.com/questions/9695695/how-to-use-phpexcel-to-read-data-and-insert-into-database
				$inputFileType = PHPExcel_IOFactory::identify($filename);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($filename);
				
				return $objPHPExcel;
				
			}
			catch(Exception $e)
			{
				die('Error loading file "'.pathinfo($filename,PATHINFO_BASENAME).'": '.$e->getMessage());
			}
		}
		else
		{
			TC_triggerError('Excel file not found');
		}
		
	}

}
?>