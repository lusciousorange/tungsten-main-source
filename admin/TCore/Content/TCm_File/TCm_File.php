<?php
/**
 * Class TCm_File
 *
 * A file that represents a physical file sitting somewhere on the server.
 */
class TCm_File extends TCm_Model
{
	protected $filename; // [string] = THe name of the file 
	protected $path; // [string] = The path to the file from teh server root
	protected $server_path;
	protected $extension; // [string] = The extension on the file
	protected $filename_base; // [string = The string that is the base of the filename
	protected $image_resource = false;
	protected $crop_values = null;

	protected $pdf_extensions = array('pdf','eps','ps','ai');
	protected $archive_extensions = array('zip', 'sit','tar.gz','tgz','tlz','sitx');
	protected $image_extensions = array('jpg','gif','jpeg','png','tiff','psd','tif');
	protected $video_extensions = array('mkv','flv','vob','avi','mov','qt','wmv','mp4','m4p','m4v','mpg','mpeg','m2v','mpv','mp2','mpe','3gp');
	protected $audio_extensions = array('mp3','wav','aac','ac3','act','aif','aiff','band','dcf','flp','mp1', 'mpa','mpga');
	protected $word_extensions = array('doc','docx','docm','dotx','dotm','docm','dot');
	protected $excel_extensions = array('xls','xlt','xlm','xlsx','xlsm','xltx','xlsb','xia');
	protected $powerpoint_extensions = array('pptx','ppt','keynote');
	protected $code_extensions = array('htm','html','php','asp','aspx','vb','js','css','c','h','asp','cpp','cxx','dll','hxx','java','jsp','pm','sbl','src');
	
	const PATH_IS_FROM_SERVER_ROOT = true;
	
	/**
	 * TCm_File constructor.
	 * @param bool|string $id The ID for this file
	 * @param string $filename The name of the file being loaded
	 * @param string $path The path to the file (Optional) Default "/assets/".
	 * @param bool $path_is_from_server_root (Optional) Default false. Indicates if the path provided is from the server root. Otherwise it means that it's from the site root.
	 */
	public function __construct($id, $filename, $path = '/assets/', $path_is_from_server_root = false)
	{
		parent::__construct($id);
		
		// Save the path, avoid any double-slashes
		$this->path = str_replace('//','/', $path);
		
		
		if($filename)
		{
			// deal with cropping
			// a string formatted like {CROP, crop_x, crop_y, crop_width, crop_height}
			// or {CROP, crop_x, crop_y, crop_width, crop_height, target_width, target_height}
			// may be appended to the end of filenames
			// it must be saved then ignored
			$crop_position = strpos($filename, '{CROP');
			if($crop_position  > 0)
			{
				$crop_values = str_ireplace(array('{','}'), '', substr($filename,$crop_position ));
				$crop_values = explode(',', $crop_values);
				array_shift($crop_values);

				$this->crop_values = array();
				$this->crop_values['src_x'] = intval($crop_values[0]);
				$this->crop_values['src_y'] = intval($crop_values[1]);
				$this->crop_values['src_width'] = intval($crop_values[2]);
				$this->crop_values['src_height'] = intval($crop_values[3]);
				
				
				// Detect bad crop values, and disable crop
				if($this->crop_values['src_width'] == 0 || $this->crop_values['src_height'] == 0)
				{
					$this->crop_values = null;
				}
				else
				{
					
					// If the next value is set, then there's a target width
					// If it's not set, then it uses 1200 as the default width
					//$crop_width = 1200;
					if(isset($crop_values[4]))
					{
						$crop_width = round($crop_values[4]);
						$this->setCropOutputWidth($crop_width);
					}
					
					
					
					
				}
				
				

				$filename = substr($filename,0, $crop_position);
			}

			$this->setFilename($filename);
		}
		
		if($path_is_from_server_root)
		{
			$this->server_path = $this->path;

			// Correct the path so that it can be used as well
			$this->path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $this->server_path);
		}
		else // PATH IS FROM SITE ROOT
		{
			$string = $_SERVER['DOCUMENT_ROOT'].$this->path.'/';
			$string = str_ireplace('//', '/', $string);
			$this->server_path = htmlspecialchars($string);

		}
		// Check for site-wide switch to /assets/pages/ instead of /assets
		if($this->path == '/assets/pages/' &&  !file_exists($this->server_path.$this->filename()))
		{
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/'.$this->filename()))
			{
				$this->addConsoleMessage('Moving Pages File : '.$this->filename());
				rename($_SERVER['DOCUMENT_ROOT'].'/assets/'.$this->filename(), $this->server_path.$this->filename());

			}


		}
		
		// Ensure it ends with a slash
		if(substr($this->server_path, -1) != '/')
		{
			$this->server_path .= '/';
		}
		
		// Ensure it ends with a slash
		if(substr($this->path, -1) != '/')
		{
			$this->path .= '/';
		}
		$this->createFolderPath();
		
	
	
	}

	/**
	 * Sets the output crop width that should be saved as part of this file.
	 *
	 * @param int $crop_width The width of the crop output file in pixels
	 */
	public function setCropOutputWidth(int $crop_width) : void
	{
		if(!is_null($this->crop_values))
		{
			// Save the crop width and height
			$this->crop_values['target_width'] = $crop_width;
			$target_height = round($this->crop_values['src_height'] / $this->crop_values['src_width'] * $crop_width);
			$this->crop_values['target_height'] = $target_height;
		}
	}

	/**
	 * Returns the crop values for this file.
	 *
	 * These are values that are associated ith the filename in the DB and parsed out each time. These values
	 * are often used for final display but it is up to the developer to use them as necessary.
	 *
	 * The formatting for the values is as follows:
	 *
	 * src_x
	 * src_y
	 * source_width
	 * source_height
	 * target_width
	 * target_height
	 *
	 * @return array|null Returns false if no crop values are found.
	 */
	public function cropValues() : ?array
	{
		return $this->crop_values;

	}
	
	/**
	 * Checks if the file is set to crop to the full size.
	 * @return bool
	 */
	public function fileSetToCropFullSize() : bool
	{
		// Only bother if this is an image with crop values
		if($this->isImage() && is_array($this->crop_values))
		{
			list($actual_width, $actual_height) = @getimagesize($this->filenameFromServerRoot());
			
			if($this->crop_values['src_x'] == 0 && $this->crop_values['src_y'] == 0
				&& $this->crop_values['src_width'] == $actual_width
				&& $this->crop_values['src_height'] == $actual_height)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		// Defaults to true since no crop values indicates full size
		return true;
	}
	
	/**
	 * Turns off any crop values set for the view
	 */
	public function disableCropValues()
	{
		$this->crop_values = null;
	}

	/**
	 * Function to parse the filename given for this file. Saves the extension and basename
	 */
	protected function parseFileName()
	{
		$file_path = explode(".",$this->filename); // separate the path into an array
		$this->extension = $file_path[sizeof($file_path) - 1]; // find the extension 
		$this->filename_base = str_replace(".".$this->extension, '', $this->filename); // get rid of the extension
		$this->extension = strtolower($this->extension); // lower string extension
	}
	
	/**
	 * Function to parse the filename given for this file. Saves the extension and basename
	 *
	 * @return string
	 */
	public function filenameFromSiteRoot()
	{
		return htmlspecialchars($this->path.$this->filename);
	}
	
	/**
	 * Returns the path from the site root to the file
	 * @return string
	 */
	public function pathFromSiteRoot()
	{
		return htmlspecialchars($this->path);
	}
	
	
	/**
	 * Returns the path from the server root to the file
	 * @return string
	 */
	public function filenameFromServerRoot()
	{
		return $this->server_path.$this->filename;
	}
	
	/**
	 * Returns the path from the server root to the file
	 * @return string
	 */
	public function pathFromServerRoot()
	{
		return $this->server_path;
	}
	
	/**
	 * Returns the path for the file
	 * @return string
	 */
	public function path()
	{
		return $this->path;
	}		
	
	
	/**
	 * Returns the extension for this file
	 * @return string
	 */
	public function extension()
	{
		return $this->extension;
	}

	/**
	 * Returns if this file is publicly visible by being within the document root.
	 * @return bool
	 */
	public function publiclyVisible()
	{
		return strpos($this->pathFromServerRoot(),$_SERVER['DOCUMENT_ROOT']) !== false;

	}
	
	/**
	 * Returns the path for the file.
	 *
	 * Possible responses are "image", "pdf", "archive", "video", "word", "excel", "powerpoint", "code", "audio" or "file" if not found.
	 * @return string
	 */
	public function filetype()
	{
		if(in_array($this->extension, $this->image_extensions))
		{
			return 'image';
		}
		elseif(in_array($this->extension, $this->pdf_extensions))
		{
			return 'pdf';
		}
		elseif(in_array($this->extension, $this->archive_extensions))
		{
			return 'archive';
		}
		elseif(in_array($this->extension, $this->video_extensions))
		{
			return 'video';
		}
		elseif(in_array($this->extension, $this->word_extensions))
		{
			return 'word';
		}
		elseif(in_array($this->extension, $this->excel_extensions))
		{
			return 'excel';
		}
		elseif(in_array($this->extension, $this->powerpoint_extensions))
		{
			return 'powerpoint';
		}
		elseif(in_array($this->extension, $this->code_extensions))
		{
			return 'code';
		}
		elseif(in_array($this->extension, $this->audio_extensions))
		{
			return 'audio';
		}
		
		return 'file';
	}
	
	/**
	 * Returns if the extension correlates to an image format
	 * @return bool
	 */
	public function hasImageExtension()
	{
		return $this->isImage();
	}
	
	/**
	 * Returns if the extension correlates to an image format
	 * @return bool
	 */
	public function isImage()
	{
		return $this->filetype() == 'image';
	}
	
	/**
	 * Returns if the extension correlates to an image format
	 * @return bool|resource
	 */
	public function imageResource()
	{
		if($this->image_resource === false)
		{
			
			if($this->exists())
			{
				$file_source = $this->filenameFromServerRoot();
			
				// Create the Source
				if($this->extension() == 'jpeg' || $this->extension() == 'jpg')
				{
					$this->image_resource = imagecreatefromjpeg($file_source);
				}
				elseif($this->extension() == 'gif')
				{
					$this->image_resource = imagecreatefromgif($file_source);
				}
				elseif($this->extension() == 'png')
				{
					$this->image_resource = imagecreatefrompng($file_source);
					
				}	
			
			}
			
		}
		
		return $this->image_resource;
		
	}
	
	/**
	 * Clears the image resource which includes destroying the image
	 * @return void
	 */
	public function clearImageResource()
	{
		if($this->image_resource !== false)
		{
			imagedestroy($this->image_resource);
			$this->image_resource = false;
		}
		
	}
	

	/**
	 * Returns the filename for this file
	 *
	 * @return string
	 */
	public function filename()
	{
		return $this->filename;
	}
	
	/**
	 * Sets the filename to be associated with this file.
	 * @param string $filename
	 * @uses TCm_File::parseFileName()
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;
		$this->parseFileName();
		
	}
	
	
	/**
	 * Returns the base for the filename which means no extension or path.
	 * @return string
	 */
	public function filenameBase()
	{
		return $this->filename_base;
	}		
	
	/**
	 * Returns the filesize in bytes.
	 * @return int
	 */
	public function filesize()
	{
		return filesize($this->filenameFromServerRoot());
	}
	
	/**
	 * Returns the filesize as a human-readable string. By default it returns a value in the smallest 1024 unit such as KB, MB, or GB. An option can be set to return just bytes.
	 * @param bool $return_bytes (Optional) Default false. Indicates if the value should force bytes
	 * @param int $num_decimals (Optional) Default 1. The number of decimals to be used
	 * @return string
	 */
	public function filesizeString($return_bytes = false, $num_decimals = 1)
	{
		$byte_values = array(
			'TB' => 1099511627776,
			'GB' => 1073741824,
			'MB' => 1048576,
			'KB' => 1024,
			);
		
		$bytes = $this->filesize();
		
		if($return_bytes || $bytes < 1024)
		{
			return $bytes.' B';
		}
		
		foreach($byte_values as $unit => $limit)
		{
			// more bytes than the limit, means to use this unit
			if($bytes >= $limit)
			{
				$rounded_value = $bytes / $limit;
				return round($rounded_value, $num_decimals).' '.$unit;
			}
		}
		
		return $bytes;
	}		
	
	/**
	 * Returns if the file exists
	 * @return bool
	 */
	public function exists()
	{
		return file_exists($this->filenameFromServerRoot()) && $this->filename != '';
	}		

	/**
	 * Creates the necessary folders to the path of this file. This only works for files inside the document root.
	 */
	public function createFolderPath()
	{

		if(!is_dir($this->pathFromServerRoot()))
		{
			$folders = explode('/', $this->pathFromServerRoot()); // break the path into it's folders
			$full_path = '/';
			foreach($folders as $folder_name) // loop thorugh each folder
			{
				if($folder_name != '') // avoid blanks
				{
					$full_path .= $folder_name.'/';
					// if not a directory then create it
					if(!is_dir($full_path))
					{
						mkdir($full_path,0755);
						$this->addConsoleMessage('Creating Folder: '.$full_path);
					}
				}	
			}
		}

	}		
	
	
	/**
	 * Returns a name that doesn't conflict with the current name provided.
	 *
	 * @param bool $use_random_filename (Optional) Default false.
	 * @return string
	 */
	public function uniqueFilename($use_random_filename = false)
	{
		$temp_file_name = $this->filename;
		$file_name = explode(".",$temp_file_name); // separate the base and extension
		$last_index = sizeof($file_name) - 1; // get the extension index
		$extension = $file_name[$last_index];

		// create the new filename subtracting the dot '.' and the extension
		$file_name = substr($temp_file_name, 0 , strlen($temp_file_name) - 1 - strlen($extension));
		
		if($use_random_filename)
		{
			//$non_valid_chars = array('.','#','<','>','@','#','%','$','!');
			$file_name = substr(md5(trim($file_name)), 0, 20); // create a hash of the filename
		}
		else // not hashed 
		{
			$non_valid_chars = array('<','>','#','%','&','+','/',"'",'"',',','(',')','{','}');
			$file_name = str_replace($non_valid_chars, '', $file_name); // get rid of multiple dots
			$file_name = str_replace(' ', '_', $file_name); // get rid of spaces
		}
		
		$usable_name = false;
		$count = 1;
		$temp_file_name = $file_name.'.'.$extension; // save the new temp
		while(!$usable_name)
		{	
			if(file_exists($this->pathFromServerRoot().$temp_file_name))
			{
				$count++; // add one to the count
				$temp_file_name = $file_name.'-'.$count.'.'.$extension; // change image name to be name-#.jpg
			}
			else
			{
				$usable_name = true;
			}
		}
		return $temp_file_name;
	}

	/**
	 * Static function that returns a safe filename in a given folder.
	 * @param string $original_filename
	 * @param string $folder The path to the folder that is being used. The path must be from the server root
	 * @return string
	 */
	public static function safeFilenameInFolder($original_filename, $folder)
	{
		$temp_file_name = $original_filename;
		$file_name = explode(".",$temp_file_name); // separate the base and extension
		$last_index = sizeof($file_name) - 1; // get the extension index
		$extension = $file_name[$last_index];

		// create the new filename subtracting the dot '.' and the extension
		$file_name = substr($temp_file_name, 0 , sizeof($temp_file_name) - 2 - strlen($extension));

			$non_valid_chars = array('<','>','#','%','&','+','/',"'",'"');
			$file_name = str_replace($non_valid_chars, '', $file_name); // get rid of multiple dots
			$file_name = str_replace(' ', '_', $file_name); // get rid of spaces

		$usable_name = false;
		$count = 1;
		$temp_file_name = $file_name.'.'.$extension; // save the new temp
		while(!$usable_name)
		{
			if(file_exists($folder.$temp_file_name))
			{
				$count++; // add one to the count
				$temp_file_name = $file_name.'-'.$count.'.'.$extension; // change image name to be name-#.jpg
			}
			else
			{
				$usable_name = true;
			}
		}
		return $temp_file_name;
	}


	/**
	 * Deletes the file
	 * @param string $action_verb
	 */
	public function delete($action_verb = 'deleted')
	{
		
		unlink($this->filenameFromServerRoot());
		$message = 'DELETE FILE : '.$this->filename;
		$this->addConsoleMessageWithType($message, TSm_ConsoleQuery_Delete);
		
		// Old approach that would put cache in front of names
		//exec('rm '.$_SERVER['DOCUMENT_ROOT'].$this->path.'cache-'.$this->filename_base.'*');
		
		// New approach, checks the cache folder
		exec('rm '.$_SERVER['DOCUMENT_ROOT'].'/cache/'.$this->filename_base.'*');
			
	}
	
	/**
	 * Output the file to the screen
	 */
	public function outputToScreen()
	{
		if ($this->exists())
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$this->fileName().'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . $this->filesize());
			readfile($this->filenameFromServerRoot());
		    exit();
		}
	}
	
	
}

?>