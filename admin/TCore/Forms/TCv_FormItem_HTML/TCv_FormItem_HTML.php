<?php

class TCv_FormItem_HTML extends TCv_FormItem
{
	protected string $html = ''; // The html to be shown
	
	/**
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title = false)
	{
		parent::__construct($id, $title);
		$this->setSaveToDatabase(false); // don't add to db, that would be silly
		$this->uses_container = true;
		$this->hideInSearchableAppliedFilters();
		
		
	}
	
	
	/**
	 * Returns the html for this field
	 * @return string
	 */
	public function html()
	{
		return parent::html();
	}
	
	/**
	 * @param $html
	 * @return void
	 * @deprecated Use addText()
	 */
	public function setHTML($html)
	{
		$this->addText($html);
	}
	
}
?>