<?php
/**
 * Class TCv_FormItem_GoogleRecaptcha
 *
 * A Google Recaptcha check that can be added to a form and will show up as long as there is a working key in settings
 *
 * This system uses v2, NOT v3.
 *
 * This requires an API key pair, which can be configured on http://www.google.com/recaptcha/admin
 *
 * @see https://developers.google.com/recaptcha/docs/display
 */
class TCv_FormItem_GoogleRecaptcha extends TCv_FormItem
{
	protected ?string $site_key = null;
	protected ?string $secret_key = null;
	
	/**
	 * @var string
	 * The url where the JS library from Google is stored
	 */
	protected string $js_url = "https://www.google.com/recaptcha/api.js";
	
	/**
	 * TCv_FormItem_GoogleRecaptcha constructor
	 *
	 * @param string $id Optional: in order to customize the ID used for the form item
	 * @param string|false $title Optional: The option to set a title that is visible in title column of form
	 */
	public function __construct ($id = 'google_recaptcha_service', $title = false)
	{
		parent::__construct($id, $title);
		$this->setSaveToDatabase(false); // We are not saving to DB, because recaptcha is for validation only
		
		// Test if feature is enabled
		if(!TC_getConfig('use_google_recaptcha'))
		{
			$this->addText('Google Recaptchas are not enabled for this website');
			$this->disable();
			return;
		}
		
		$this->acquireRecaptchaKeys();// Attempt to get a site key from the system that is needed for accessing Google

		if (!empty($this->site_key) && !empty($this->secret_key))
		{
			$this->attachHTMLCaptchaBox();

			$this->addJSFile(
				'google_recaptcha_js',
				'<script src='.$this->js_url.' async defer></script>'
			);

			// Add the process method, so it always gets added
			$this->addProcessMethodCall('validateGoogleCaptcha',
				TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
				TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
				false);
		}
		else
		{
			$this->addText('You must configure your Google Recaptcha site keys in system settings');
			$this->disable();
		}
	}

	/**
	 * The HTML required to show the Google reCaptcha form item
	 */
	public function attachHTMLCaptchaBox() : void
	{
		$view = new TCv_View('google_recaptcha');
		$view->setAttribute('data-sitekey', $this->site_key);
		$view->addClass('g-recaptcha');
		$this->attachView($view);

	}

	/**
	 * Attempt to get the required site key from the system settings
	 */
	public function acquireRecaptchaKeys() : void
	{
		$this->site_key = TC_getModuleConfig('system','recaptcha_site_key');
		$this->secret_key = TC_getModuleConfig('system','recaptcha_secret_key');
	}

	/**
	 * The validation required for detecting if google recaptcha was valid or not
	 * @return TCm_FormItemValidation
	 */
	public function validateGoogleCaptcha() : TCm_FormItemValidation
	{
		$validation = new TCm_FormItemValidation($this->id, 'Captcha');
		// Perform your tests, fail the validation if it doesn't work
		// Return the validation in the end
		$verify_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$this->secret_key.'&response='.$_POST['g-recaptcha-response']);
		$response_data = json_decode($verify_response);
		// check was the response successfully checked by Google
		if(!$response_data->success)
		{
			$this->addConsoleDebug('validation should have failed');
			$validation->failWithMessage('Captcha response is not valid.');
		}

		return $validation;
	}

}
