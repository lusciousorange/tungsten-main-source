<?php 
	/**********************************************
	 
	 This is a general process script that will deal with 
	 any TCv_FormWithModel that needs processing. This script is
	 called by default by the TCv_FormWithModel class but it can
	 be overridden easily as necessary. 
	 
	 It should also be noted that this script loads the tungsten_header.php
	 file which means that if you are attempting to process a form
	 from a public site, you will need to use set the value of 
	 $skip_tungsten_authentication to true for that script and then process the 
	 script manually. You can copy this script as a starting point.
	 
	 **********************************************/
	
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_process_header.php");

	$form_processor = TCc_FormProcessorWithModel::init();
	$form_processor->run();
	
?>
