<?php

/**
 * Class TCc_FormProcessorWithModel
 */
class TCc_FormProcessorWithModel extends TCc_FormProcessor
{
	/** @var bool|TCm_Model $model */
	protected $model = false;
	protected $original_model = false;

	protected $form_name, $model_name;
	protected $models = array();

	/**
	 * TCc_FormProcessorWithModel constructor.
	 */
	public function __construct()
	{
		// Create the model 
		//$this->form_name = $_POST['tracker_form_name'];
		
		parent::__construct();
		$this->model_name = $this->form_tracker->modelName();
		
		
	}
	
	/**
	 * Triggers the run after init which handles the hook method for configuring the process. That is used in any
	 * number of cases, but most notably switching to mirror tables in publishing. It also configures the model.
	 * @return void
	 */
	public function runAfterInit() : void
	{
		// Calls the form's config method
		$this->form_class_name::customFormProcessor_initConfig($this);
		
		$this->configureModel();
		
	}
	
	/**
	 * Override the internal DB query function for that form processors always use the model class's database
	 * connection. Most of the time, that's the main database, however for sharded models, we need to respect them.
	 * @param $query
	 * @param $values
	 * @param $bind_param_values
	 * @param ?TCm_Database $connection
	 * @return PDOStatement
	 */
	public function DB_Prep_Exec($query, $values = null, $bind_param_values = [], ?TCm_Database $connection = null)
	{
		if(is_null($connection))
		{
			$connection = $this->DB();
		}
		return static::DB_RunQuery($query,
		                           $values,
		                           $bind_param_values,
		                           $connection);
	}
	
	/**
	 * The database connection for a form processor should always use the database for the model. In regular systems,
	 * it will always be true.
	 * @return TCm_Database|null
	 */
	public function DB(): ?TCm_Database
	{
		return ($this->modelName())::DB_Connection();
	}
	
	
	
	/**
	 * Configures the processor to deal with all the values associated with the model for the form.
	 */
	public function configureModel()
	{
		if(trim($this->model_name) == '')
		{
			return;
		}

		$primary_value = $this->form_tracker->primaryValue();

		//print 'primary value '.$this->form_tracker->primaryValue();
		if($primary_value >= 0 && trim($primary_value) != '')
		{
			$class_name = $this->model_name;
			$this->model = $class_name::init($this->form_tracker->primaryValue());
			$this->original_model = clone($this->model);

		}
		else // not an editor
		{
			$this->model = $this->model_name;
		}
		
		$this->addTrackerValueToConsole('Model Name', $this->model_name);
		
		//$model = $this->model;
		
		
	}
	
	/**
	 * Returns the model for this processor. If this process was run multiple times, this will only return the most
	 * recently modified. see models() for all possibly created/edited models.
	 * @return TCm_Model
	 */
	public function model()
	{	
		return $this->model;
	}	
	
	/**
	 * Returns all the models that were created. If this form processor ran more than once, than  model for this processor
	 * @return TCm_Model[]
	 */
	public function models()
	{	
		return $this->models;
	}	
	
	/**
	 * Sets the model for this processor
	 * @param TCm_Model $model
	 */
	public function setModel($model)
	{	
		$this->model = $model;
	}	
	
	/**
	 * Returns a model regardless of if the form processor has access to a model and regardless of if it is an editor or not.
	 * @return TCm_Model
	 */
	public function modelForProcessingFunctionCalls()
	{
		if($this->model() instanceof TCm_Model)
		{
			return $this->model();
		}
		
		$model_name = $this->model_name;
		return new $model_name(0);
	}
	
	
	/**
	 * The name of the model for this processor
	 * @return class-string<TCm_Model>
	 */
	public function modelName() : string
	{	
		return $this->model_name;
	}	
	
	
	/**
	 * Verifies that the tracker values provided match those that were originally setup with the form. If the verification
	 * fails, the processor immediately return the user to the form page.
	 * @deprecated No longer used
	 */
	public function verifyTracker()
	{	

	}
	
	/**
	 * Performs actions that should occur immediately after an update, before any post method calls are done.
	 */
	protected function saveModelAfterUpdate()
	{
		
		// Clear any previously saved versions of the model
		TC_clearMemoryForClass($this->model_name, $this->databasePrimaryValue());
		
		// Detect if we're using a model where the primary ID is editable and possibly not an auto-increment value
		$primary_key_name = $this->model_name::$table_id_column;
		if(isset($this->database_values[$primary_key_name]))
		{
			$this->setDatabasePrimaryValue($this->database_values[$primary_key_name]['value']);
		}
		
		
		// EDITING : Remove any cache for this model
		// Must happen before we re-instantiate
		if($this->model instanceof TCm_Model)
		{
			$this->model->clearCache();
		}

		// save the model, always grabbing a fresh one
		$this->model = ($this->model_name)::init($this->databasePrimaryValue());
		$this->models[] = $this->model; // save mutliple if they exist
		// Save the model to the form tracker
		if(isset($this->form_tracker) && $this->model)
		{
			$this->form_tracker->setPrimaryModel($this->model);
		}

		if($this->isCreator())
		{
			if($this->model instanceof TCm_Model)
			{
				// If we're creating, need to still clear the all cache
				$this->model->clearCache();
				
				/**
				 * Workflows creation
				 * If we are using workflows AND there is a new model using the Workflow Item trait,
				 * then we will need to create default workflows
				 * @author Katie Overwater
				 */
				if(
					TC_getConfig('use_workflow')
					&& TC_classUsesTrait($this->model, 'TMt_WorkflowItem')
				)
				{
					$this->model->generateDefaultWorkflows();
				}
			}
		}
	}
	
	/**
	 * Checks if we have just created a model that is a primary model for horizontal sharding. If so, we need to
	 * generate the tables for it.
	 * @return void
	 */
	protected function handleInstallOfHorizontalShardPrimary() : void
	{
		// Deal with horizontal sharding
		// We just created a model that is a primary shard, we need to install all the tables for it
		if($this->isCreator() && $this->isValid() && is_subclass_of($this->model_name, 'TCm_ModelPrimaryHorizontalShard'))
		{
			$new_model = $this->model();
			$new_model->installShardTables();
		}
	}
	
	/**
	 * Compiles the list of database query snippets.
	 * @uses TCc_FormProcessor::addDatabaseValue()
	 */
	protected function processDatabaseQuerySnippets()
	{
		parent::processDatabaseQuerySnippets();
		
		// Validate the model against it's scheme
		// Requires the database snippets to exist
		$this->validateDatabaseValuesAgainstModelSchema();
		
		// Detect model changes
		// Deals with skipping queries
		if($this->isEditor())
		{
			$values = [];
			foreach($this->database_values as $column_name => $array)
			{
				$values[$column_name] = $array['value'];
			}
			if(!$this->model()->checkIfUpdateValuesTriggerChange($values))
			{
				$this->no_db_changes_detected = true;
			}
		}
		
	}
	
	/**
	 * Extends the existing functionality of the updateDatabase method to handle the model settings
	 * @param bool $force_insert
	 */
	public function updateDatabase(bool $force_insert = false)
	{
		// handle adding user_id when creating new items
		if($this->isCreator() || $force_insert)
		{
			// if this class saves the user_id and it isn't already set
			$model_name = $this->model_name;
			if(class_exists($model_name))
			{
				if($model_name::$store_user_id_when_creating && !isset($this->database_values['user_id']))
				{
					if($user = TC_currentUser())
					{
						$this->addDatabaseValue('user_id', $user->id());
					}
				}
			}
			else
			{
				$this->addConsoleWarning('Class "' . $model_name . '" does not exist');
			}
		}

		parent::updateDatabase($force_insert);

	
	}

	/**
	 * A hook method that lets us perform actions after the update has occurred but before the "post" actions happen.
	 */
	protected function performActionsBetweenUpdateAndPostActions()
	{
		// Always save the latest model
		$this->saveModelAfterUpdate();
		
		// Check for horizontal sharding and install the tables
		$this->handleInstallOfHorizontalShardPrimary();
		
	}

	/**
	 * Returns the orginal model that was passed in before any changes were made
	 * @return TCm_Model|bool
	 */
	public function originalModel()
	{
		return $this->original_model;

	}

	/**
	 * Extends the functionality for processing form values to handle a pairing table for use Multiple
	 * @param TCv_FormItem $form_item The form item that is being processed
	 * @param bool|string|array $form_value The form value provided
	 * @return array|string
	 */
	public function getFormValuesForField($form_item, $form_value)
	{
		if(is_array($form_value) && $form_item->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE)
		{
			// return blank since we aren't dealing with the form value in the normal processing
			return array();
		}


		return parent::getFormValuesForField($form_item, $form_value);
	}
	
	
	/**
	 * Validates the model against the schema which is defined in the static method schema for each model
	 */
	public function validateDatabaseValuesAgainstModelSchema()
	{
		/** @var string|TCm_Model $model_name */
		$model_name = $this->model_name;
		
		if($this->isEditor())
		{
			$mode = 'update';
			$model = $this->model();
		}
		else
		{
			$mode = 'create';
			$model = null;
		}
		
		// Compile the values from the database_values that are going to be submitted
		$values = [];
		foreach($this->database_values as $name => $db_values)
		{
			$values[$name] = $db_values['value'];
		}
		
		$validation = $model_name::validateSubmittedValuesAgainstSchema($values, $mode, $model);
		
		// errors found in the schema validation, work through them and fail the corresponding form fields
		if(is_array($validation))
		{
			foreach($validation as $values)
			{
				// Use the error processing functionality which is caught later and provides consistent feedback
				TC_trackProcessError("Validation: ".$values['message'], 'error', $values['field_id']);
			}
		}
	}
	
	
	
	/**
	 * Runs the processor
	 * @param bool $redirect (Optional) Default true. Indicates if the processor redirects
	 */
	public function run($redirect = true)
	{
		/** @var TCv_FormWithModel|string $form_class */
		$form_class = $this->formClassName();
		
		// HANDLE MESSENGER
		/** @var string|TCm_Model $model_name */
		$model_name = $this->modelName();
		if(!$model_name || $model_name == '')
		{
			$this->fail('Processing Error: Model Not Found');
			$this->finish($redirect);
			return;
		}
		
		$title = $model_name::$model_title;
		
		if($form_class::$show_form_success_message)
		{
			TC_messageConditionalTitle($title.' was successfully '.($this->isEditor() ? 'updated' : 'created'),true);
		}
		if($form_class::$show_form_failure_message)
		{
			TC_messageConditionalTitle($title.' could not be '.($this->isEditor() ? 'updated' : 'created'), false);
		}
		
		// VALIDATE
		$this->processFormItems();
		$form_class::customFormProcessor_Validation($this); // validation should always run
		
		// UPDATE DB
		if($this->isValid())
		{
			$form_class::customFormProcessor_performPrimaryDatabaseAction($this);
		}
		
		// After Database, check again since valid might change
		if($this->isValid())
		{
			$form_class::customFormProcessor_afterUpdateDatabase($this);
		}
		
		$this->finish($redirect);
	}
	
	/**
	 * @param array $form_item_values
	 * @param array $process_values
	 * @return TCm_FormItemValidation
	 */
	public function processFunctionCall_CLASS_STATIC_METHOD($form_item_values, $process_values)
	{
		// Override the function from TCc_FormProcessor to validate with static class methods
		
		$model_class_name = ($this->modelName())::classNameForInit();
		
		
		// Define the values for the function call
		// This matches historical setup in the past
		$args = $process_values['args'];
		$method_name = $process_values['method_name'];
		
		// Modify the args provided to ensure they appear in the correct order
		$field_id = array_shift($args);
		
		// Combine them in the order, with user-defined args at the end
		$args = array_merge([$field_id,$form_item_values['submitted_value'],  $this->model()], $args);
		
		if(!method_exists($model_class_name,$method_name))
		{
			TC_triggerError('Process methods "'.$method_name.'" does not exist in class '.$model_class_name);
		}
		
		return call_user_func_array(array($model_class_name, $method_name),$args);

	}
	
	/**
	 * Extend to deal with models that aren't traditional such as not auto incrementing
	 * @return void
	 */
	protected function saveCreateModelInsertedID() : void
	{
		$primary_key_name = $this->model_name::$table_id_column;
		if(isset($this->database_values[$primary_key_name]))
		{
			$this->setDatabasePrimaryValue($this->database_values[$primary_key_name]['value']);
		}
		else
		{
			parent::saveCreateModelInsertedID();
		}
	}
}
?>