class TCv_FormItem_TextBox {

	char_counter = null;
	options = {
		max_length : -1
	};

	/**
	 * Constructor for the field
	 * @param element
	 * @param params
	 */
	constructor(element, params) {
		this.element = element;
		this.element.addEventListener('input', (e) => this.fieldChanged(e));
		Object.assign(this.options, params);

		this.char_counter = this.element.parentElement.querySelector('.char_counter');

		this.fieldChanged();
	}

	/**
	 * Event handler every time the field is changed. Updates the char count
	 * @param event
	 */
	fieldChanged(event) {
		// Update the char counter if we have a max length and there's a counter to fill in
		if(this.options.max_length > 0 && this.char_counter)
		{
			let num_remaining = this.options.max_length -  this.element.value.length;
			this.char_counter.querySelector('.number').textContent = num_remaining;
		}
	}
}