<?php

/**
 * Class TCv_FormItem_TextBox
 */
class TCv_FormItem_TextBox extends TCv_FormItem
{
	protected int $max_length = -1; // The number of characters that can be entered. False indicates no limit
	protected bool $show_num_remaining = false;
	/**
	 * TCv_FormItem_TextBox constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		
	}

	//////////////////////////////////////////////////////
	//
	// CHARACTER LIMIT
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets a max length on the field
	 * @param int $max_length The maximum length
	 * @param bool $show_num_remaining Indicate if we should show the number remaining
	 * @return void
	 */
	public function setMaxLength(int $max_length, bool $show_num_remaining = false)
	{
		$this->max_length = $max_length;
		$this->show_num_remaining = $show_num_remaining;
	}
	
	/**
	 * Sets a limit on the number of characters that can be entered into the box
	 *
	 * @param int $limit
	 */
	public function setCharacterLimit(int $limit)
	{
		if($limit > 0)
		{
			$this->max_length = $limit;
			$this->show_num_remaining = true;
			
		
		}
	}
	
	/**
	 * Performs a validation to check if the number of characters is above the maximum amount permitted.
	 *
	 * @return TCm_FormItemValidation
	 */
	public function validateCharacterLimit()
	{
		$validation = new TCm_FormItemValidation($this->id, 'Character Limit');
		
		if(strlen(trim($_POST[$this->id])) > $this->max_length)
		{
			$validation->failWithMessage(''.htmlspecialchars($this->title).' cannot be more than '.$this->max_length.' characters.');
		}
		
		return $validation;
	}

	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the HTML
	 * @return string
	 */
	public function html()
	{
		$textarea = new TCv_View(htmlspecialchars($this->id));
		$textarea->setTag('textarea');
		$textarea->addClassArray($this->classes());
		$textarea->setAttribute('name', htmlspecialchars($this->fieldName()));
		$textarea->addText(htmlspecialchars($this->currentValue()), true);
		$textarea->addClassArray($this->form_element_classes);
		$textarea->addDataValueArray($this->form_element_data_values);
		
		if($this->max_length > 0)
		{
			$textarea->setAttribute('maxlength', $this->max_length);
		}
		
		$this->attachView($textarea);
		
		
		// Deal with the extra stuff we add
		if($this->max_length > 0 && $this->show_num_remaining)
		{
			$this->addClassJSFile('TCv_FormItem_TextBox');
			$this->addClassJSInit('TCv_FormItem_TextBox', ['max_length' => $this->max_length]);

			$char_count = new TCv_View();
			$char_count->addClass('char_counter');
			$inside = new TCv_View();
			$inside->addClass('number');
			$inside->setTag('span');
			$inside->addText($this->max_length);
			$char_count->attachView($inside);
			$char_count->addText('/'.$this->max_length.' '. TC_localize('text_limit_characters_remaining','Remaining'));
			
			$this->attachView($char_count);
			
		}
		
		
		return parent::html();
	}
	
	
	
}
?>