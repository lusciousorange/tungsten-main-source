<?php

/**
 * Class TCv_FormItem_Heading
 */

class TCv_FormItem_Heading extends TCv_FormItem
{
	//$heading_views_attached = array();
	protected ?TCv_View $heading = null;
	
	/**
	 * TCv_FormItem_Heading constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		$this->setSaveToDatabase(false); // don't add to db, that would be silly
		$this->setShowTitleColumn(false); // hides the title column and just shows the heading
		$this->setShowTitleColumnContent(false); // failsafe to not show the contents of the title column either
		$this->uses_container = false;
		//$this->setTag('h2');
		
		$this->heading = new TCv_View();
		$this->heading->setTag('h2');
		$this->heading->addText($this->title);
		
	}
	
	/**
	 * Sets the heading tag, allowing us to change how the heading is rendered
	 * @param string $tag
	 * @return void
	 */
	public function setHeadingTag(string $tag) : void
	{
		$this->heading->setTag($tag);
	}
	
	
	/**
	 * Extends the functionality of attach view to add it to the heading
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 * @return bool|string|TCv_View|void
	 */
	public function attachView($view, $prepend = false)
	{
		$this->heading->attachView($view, $prepend);
	}
	
	/**
	 * Access to the heading tag if it needs to be styled or modified
	 * @return TCv_View
	 */
	public function headingTag(): TCv_View
	{
		return $this->heading;
	}
	/**
	 * Adds text to this headings string or html to this view.
	 *
	 * @param string $text The text or HTML to be added to this view.
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 * @uses TCu_Text
	 */
	public function addText($text, $disable_template_parsing = false)
	{
		$this->heading->addText($text,$disable_template_parsing);
	}
	
	
	/**
	 * Headings don't have a label
	 * @return TCv_View
	 */
	public function label()
	{
		return null;
	}
	/**
	 * @return string
	 */
	public function html()
	{
		parent::attachView($this->heading, true);
		
		if($this->helpText() != '')
		{
			$help_text = new TCv_View();
			$help_text->setTag('p');
			$help_text->addClass('form_help_text');
			$help_text->addText($this->helpText());
			parent::attachView($help_text);
			
		}
		
		
		return parent::html();
	}
	
	
}
