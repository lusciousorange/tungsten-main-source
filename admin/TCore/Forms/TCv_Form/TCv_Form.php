<?php
/**
 * Class TCv_Form
 */
class TCv_Form extends TCv_View
{
	
	// properties
	protected string $method = 'post'; // [string] = The method for sending information. Default to POST, or you can use GET
	protected string $action = '#'; // [string] = The action that will be taken upon submitting
	protected bool $is_multipart = true; // [bool] = Indicates if the form should send its content encoded as multi-part, normally used when sending files
	protected bool $is_editor = false; // [bool][string] = Indicates if this form is an editor. This is a generic flag useful for extended classes
	protected array $group_form_items_to_add = array();
	protected bool $show_submit = true;
	protected $submit_button = false;
	protected string $button_text = 'Submit';
	protected ?string $submit_button_id = 'submit';
	protected bool $disable_all_title_columns = false;
	protected bool $form_error = false;
	
	protected bool $attach_submit_to_form_root = false;
	
	protected ?string $submit_button_icon = null;
	
	/** @var bool Indicates if the JS for the class should load when finally rendered */
	protected bool $load_class_JS = true;
	
	
	// form items
	protected array $form_items = array(); // [array] = The array of form items that are added to this form. The indices are the respective IDs for the form items.
	
	
	// form tracking
	protected ?TCm_FormTracker $form_tracker = null;
	protected bool $load_form_tracking = true;

	protected bool $use_divs = true; // [bool] = Form will use divs instead of tables.
	
	protected array $containers = array();
	protected $form_table = false;
	protected array $form_root_form_items = array();
	
	
	protected array $passthrough_values = array();
	
	protected bool $submit_attached = false;
	
	protected $success_url = false;
	
	public static $DISABLE_FORM_TRACKING = false;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_Form');
	
	
	/** @var bool|string The ID of the form item that will get the focus  */
	protected $focus_load_id = false;
	
	/**
	 * TCv_Form constructor.
	 * @param bool|string $id
	 * @param bool $load_form_tracking (Optional) Default true;
	 */
	public function __construct($id, $load_form_tracking = true)
	{
		parent::__construct($id);

		$this->load_form_tracking = $load_form_tracking;
		if($this->load_form_tracking)
		{
			$this->loadFormTracker($id);
		}
		
		if(TC_isTungstenView())
		{
			$this->useTungstenFormatting();
		}
		else
		{
			$this->addClassCSSFile('TCv_Form');
		}
		$this->setTag('form');

		//$this->setUseFormTracking(false);

		$this->form_table = new TCv_View($this->id() . '_table');
		$this->form_table->setTag($this->useDivValue('table'));
		$this->form_table->addClass('form_table');
		$this->form_table->setHTMLCollapsing(false);


		// default to the default process script
		$this->setAction(TC_getConfig('TCore_path') . '/Forms/TCc_FormProcessor/process_form_tungsten.php');
		
		$this->addClassJSFile('TCv_Form');
		
		if(TC_getConfig('use_stacked_forms'))
		{
			$this->setAsStacked();
		}
	}
	
	/**
	 * Turns off the functionality that loads the class JS for the form. Simple forms, or those in loops might not want or
	 * need this.
	 * @return void
	 */
	public function disableFormClassJS() : void
	{
		$this->load_class_JS = false;
	}
	
	
	/**
	 * Sets this form to render as stacked
	 * @return void
	 */
	public function setAsStacked()
	{
		$this->addClass('stacked');
	}
	
	/**
	 * An function to enable the default to use Tungsten's formatting rather than the default that comes with
	 * TCv_From.
	 */
	public function useTungstenFormatting() : void
	{
		$this->removeCSSFile('TCv_Form');
		
		// Using Tungsten 8, then load that CSS. Tungsten 9 loads it via css_partials
		if(!TC_getConfig('use_tungsten_9'))
		{
			$this->addClass('TCv_Form_Tungsten');
			$this->addCSSFile('TCv_Form_Tungsten', '/admin/TCore/Forms/TCv_Form/TCv_Form_Tungsten.css');
		}
		$this->setAsStacked();
	}
	
	//////////////////////////////////////////////////////
	//
	// FORM TRACKING
	//
	//////////////////////////////////////////////////////

	/**
	 * Loads a form tracker for this form.
	 * @param string $id
	 * @see TCm_FormTracker
	 */
	public function loadFormTracker(string $id) : void
	{
		$this->form_tracker = TCm_FormTracker::configure($id);
	}
	
	/**
	 * Manually set the primary table name and key
	 * @param string $table
	 * @param string $key
	 * @see TCm_FormTracker
	 */
	public function setPrimaryTableAndKey($table, $key)
	{
		if($this->load_form_tracking)
		{
			$this->form_tracker->setPrimaryTableAndKey($table, $key);// needs to be saved for other form calls
		}
	}
	
	/**
	 * Sets the primary value for the form
	 * @param int|string $value
	 * @see TCm_FormTracker
	 */
	public function setPrimaryValue($value) : void
	{
		if($this->load_form_tracking)
		{
			$this->form_tracker->setPrimaryValue($value);
		}
	}
		
	
	/**
	 * Returns the form tracker for this form
	 * @return null|TCm_FormTracker
	 * @see TCm_FormTracker
	 */
	public function formTracker() : ?TCm_FormTracker
	{
		return $this->form_tracker;
	}
	
	/**
	 * Returns the primary value for this form
	 * @return bool|string
	 * @see TCm_FormTracker
	 */
	public function primaryValue()
	{
		if($this->load_form_tracking)
		{
			return $this->form_tracker->primaryValue();
		}
	}
	
	/**
	 * Returns the primary table for this form
	 * @return string
	 * @see TCm_FormTracker
	 */
	public function primaryTable()
	{
		if($this->load_form_tracking)
		{
			return $this->form_tracker->primaryTable();
		}
	}
	/**
	 * Returns the primary key for this form
	 * @return bool|int|string
	 * @see TCm_FormTracker
	 */
	public function primaryKey()
	{
		if($this->load_form_tracking)
		{
			return $this->form_tracker->primaryKey();
		}
	}
	
	/**
	 * resets all the values in the form tracker for this form
	 * @see TCm_FormTracker
	 */
	public function resetValues()
	{
		$this->form_tracker = TCm_FormTracker::configure(null, true); // second value indicates a clear on the form tracker
	}


	/**
	 * Returns the current form value for the provided ID. Due to how forms are loaded, an ID's value can't be acquired until
	 * it is attached to the form.
	 * @param string $id The ID of the form item
	 * @return mixed
	 */
	public function currentFormValueForID($id)
	{
		// Not set. Throws error because it could lead to false data operations if anything else was returned
		if(!isset($this->form_items[$id]))
		{
			TC_triggerError('Form Item with ID "<em>' . $id . '</em>" is not set. Cannot call TCv_Form->currentFormValueForID() until that form item is added to the form.');
		}

		/** @var TCv_FormItem $form_item */
		$form_item = $this->form_items[$id];
		
		if($this->load_form_tracking)
		{

			if($this->form_tracker->hasSavedValues())
			{
				$form_item->setSavedValue($this->form_tracker->submittedValueForFormItemID($form_item->id()));
			}
		}

		return $this->form_items[$id]->currentValue();
	}
	//////////////////////////////////////////////////////
	//
	// IS EDITOR
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets if this form is an editor
	 * @param bool $value (Optional) Default true
	 */
	public function setIsEditor($value = true)
	{
		$this->is_editor = $value;
	}
	
	/**
	 * Returns if this form is an editor
	 * @return bool
	 */
	public function isEditor()
	{
		return $this->is_editor;
	}
	
	/**
	 * Returns if this form is a creator, which means not an editor
	 * @return bool
	 */
	public function isCreator()
	{
		return !$this->is_editor;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FORM ITEMS
	//
	//////////////////////////////////////////////////////

	/**
	 * Function where your form items should be instantiated. This class must be extended in order to make use of this feature.
	 */
	public function configureFormElements()
	{
			
	}
	
	/**
	 * Returns the form item with the provided ID
	 * @param string $id
	 * @return bool|TCv_FormItem|TCv_View
	 */
	public function formItemWithID($id)
	{
		if(isset($this->form_items[$id]))
		{
			return $this->form_items[$id];
		}
		return false;
	}
	
	/**
	 * Returns the list of form items
	 * @return TCv_FormItem[]
	 */
	public function formItems()
	{
		return $this->form_items;
	}
	
	/**
	 * Removes the form item with a particular ID
	 * @param string $id
	 */
	public function removeFormItemWithID($id)
	{
		foreach($this->containers as $container)
		{
			$container->detachViewWithID($id);
		}

		foreach($this->formItems() as $form_item)
		{
			if($form_item instanceof TCv_FormItem_Group)
			{
				$form_item->removeFormItemWithID($id);
				
			}
		}
		// Handle if being removed before being attached
		unset($this->form_items[$id]);
	}

	//////////////////////////////////////////////////////
	//
	// PROPERTY SETTINGS
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the form method usually POST or GET
	 * @param string $method
	 */
	public function setMethod($method)
	{
		$method = strtolower($method);
		if($method != 'post' && $method != 'get')
		{
			TC_triggerError("setMethod parameter must be 'post' or 'get'");
		}
		
		if($method == 'get')
		{
			$this->is_multipart = false;
		}
		$this->method = $method;
	}
	
	/**
	 * Sets the action for the form
	 * @param string $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}
	
	/**
	 * Sets the ID of the item that should get the focus when the form loads
	 * @param string $form_item_id
	 */
	public function setOnLoadFocusItem($form_item_id)
	{
		$this->focus_load_id = $form_item_id;
	}

	//////////////////////////////////////////////////////
	//
	// OUTPUT METHODS
	//
	//////////////////////////////////////////////////////

	/**
	 * Performs a check to see if the output should use divs or regular tables. If divs are used then a div is returned. otehrwise a
	 * @param string $tag
	 * @return string
	 */
	public function useDivValue($tag)
	{
		return ($this->use_divs ? 'div' : $tag);
	}
	
	/**
	 * Sets this form to output using div elements rather than a table
	 */
	public function setUseDivs()
	{
		$this->use_divs = true;
	}

	/**
	 * Old method for attaching form items
	 * @param $form_item
	 * @deprecated use attachView() instead
	 * @see TCv_Form::attachView()
	 */
	public function addFormItem($form_item)
	{
		$this->attachView($form_item);
		
	}
	
	/**
	 * Overrides the default attachView to force all items to stay within a container
	 * @param bool|TCv_View|TCv_FormItem $form_item The form item or view being attached
	 * @param bool $prepend No affect
	 * @return void
	 */
	public function attachView($form_item, $prepend = false)
	{
		// Views could be false, which we should just return blank
		if($form_item === false)
		{
			return;
		}
		// Attaching non-TCv_FormItems
		if(!( $form_item instanceof TCv_FormItem))
		{
			foreach($form_item->attachedViews() as $view)
			{
				if(is_string($view) && strpos($view, 'TCv_FormItem'))
				{
					TC_triggerError('Attempting to attach a regular view to a form that contains TCv_FormItems
					inside of it. This is not permitted since the form items will not be processed. Use TCv_FormItem_Group instead.');
				}
			}
			
		}
		
		if($prepend)
		{
			array_unshift($this->form_items, $form_item);
		}
		else
		{
		    if($form_item->id() == '')
            {
                $this->form_items[] = $form_item;
            }
            else
            {
                $this->form_items[$form_item->id()] = $form_item;
            }
		}

	}
	
	/**
	 * This method handles all the necessary items for when an item is attached
	 * @param TCv_FormItem $form_item The form item being attached
	 */
	protected function processFormItemAttached(&$form_item)
	{
		if($this->load_form_tracking)
		{

			// track any saved values from the form tracker
			if($this->form_tracker->hasSavedValues())
			{
				if($form_item instanceof TCv_FormItem)
				{
					$form_item->setSavedValue($this->form_tracker->submittedValueForFormItemID($form_item->id()));
				}

			}
		}

		if($this->disable_all_title_columns)
		{
			$form_item->setShowTitleColumn(false);
		}
		
		if($form_item instanceof TCv_FormItem)
		{
			$form_item->setForm($this);
		}
		
		if($this->load_form_tracking)
		{
			// Handle flagging
			if($this->form_tracker->isInvalidForID($form_item->id()))
			{
				$form_item->markAsInvalid();
			}

		}
				
		// legacy save
		//$this->form_items[$form_item->id()] = $form_item;

	}

	/**
	 * Disables all the title columns for this entire form
	 */
	public function disableAllTitleColumns()
	{
		$this->disable_all_title_columns = true;
	}
	
	/**
	 * Enables all the title columns for this entire form
	 */
	public function enableAllTitleColumns()
	{
		$this->disable_all_title_columns = false;
	}
	
	
	/**
	 * Renders a single form item and returns the completed view if possible
	 * @param TCv_FormItem|TCv_View $form_item
	 * @return bool|TCv_View|TCv_FormItem
	 */
	public function renderFormItem($form_item)
	{
		// manually adding a submit button
		if ($form_item instanceof TCv_FormItem_SubmitButton)
		{
			$this->show_submit = false; // turn off duplicate form showing
		}


		if ($form_item instanceof TCv_FormItem)
		{
			// attach the form item
			$this->processFormItemAttached($form_item);

			// DEAL WITH SPECIAL CASES
			if ($form_item->isSetToAttachToFormRoot())
			{
				$this->form_root_form_items[] = $form_item;
			}
			else
			{
				$table_row = $this->tableRowView($form_item);
				return $table_row;
			}


		}
		elseif ($form_item instanceof TCv_FormItem_Group)
		{
			foreach ($form_item->formItems() as $group_form_item)
			{
				
				// attach the form item

				$this->group_form_items_to_add[$group_form_item->id()] = $group_form_item;
				$this->processFormItemAttached($group_form_item);

				if($group_form_item instanceof TCv_FormItem)
				{
					// DEAL WITH SPECIAL CASES
					if ($group_form_item->isSetToAttachToFormRoot())
					{
						$this->form_root_form_items[] = $group_form_item;
					}
					else
					{
						$table_row = $this->tableRowView($group_form_item);
						$form_item->attachView($table_row, true);
					}
					
				}
				

			}

			// attach the completed view
			return $form_item;
		}
		else // not a form item being added. make sure it fits within the form framework
		{
			// CREATE ROW
			return $form_item;
		}

		return false;
	}

	/**
	 * This method is called when rendering to finally attach all the form items. Until this point, the form items
	 * are stored in the class but not rendered
	 */
	protected function renderFormItems()
	{
		foreach($this->formItems() as $form_item)
		{
			$rendering = $this->renderFormItem($form_item);
			if($rendering)
			{
				$this->form_table->attachView($rendering);
			}
		}
		// Add them at the end for consistency
		foreach($this->group_form_items_to_add as $id => $form_item)
		{
			$this->form_items[$id] = $form_item;
		}

	}


		/**
	 * Creates a table row for a given form item
	 * @param TCv_View|TCv_FormItem $form_item
	 * @return TCv_View
	 */
	public function tableRowView($form_item)
	{
		// CREATE ROW
		$table_row = new TCv_View(htmlspecialchars($form_item->id()).'_row');
		$table_row->addClass('table_row');
		$table_row->setAttribute('data-row-id',$form_item->id());
		$table_row->setTag($this->useDivValue('tr'));
		$table_row->addClass($form_item->classesString());
		$table_row->addClass(htmlspecialchars($form_item->id()).'_row');
		if($form_item->isRequired())
		{
			$table_row->addClass('required');
		}
		// TITLE COLUMN
		if($form_item->showTitleColumn())
		{
			$table_row->attachView($this->titleColumnView($form_item));
		}
		else // include hidden label
		{
			if($label = $form_item->label())
			{
				$label->setAttribute('hidden', '');
				$table_row->attachView($label);
			}
		}
		
		// VALUE COLUMN
		$table_row->attachView($this->valueColumnView($form_item));

		// Must come after to acquire any classes from handle attaching
		$table_row->addClassArray($form_item->rowClasses());
		$table_row->addDataValueArray($form_item->rowDataValues());

		return $table_row;
	}		
	
	
	
	
	/**
	 * Returns the title column view for the form item
	 * @param TCv_FormItem $form_item
	 * @return TCv_View
	 */
	public function titleColumnView($form_item)
	{
		$td = new TCv_View();
		$td->setTag($this->useDivValue('td'));
		$td->addClass('title_column');
		
		if($form_item->showTitleColumnContent())
		{
			if($form_item->isRequired())
			{
				$span = new TCv_View();
				$span->setTag('span');
				$span->addClass('required_icon');
				$span->addText('*');
				$td->attachView($span);
			}
			
			$td->attachView($form_item->label());
			
			
			$td->attachView($this->helpTextView($form_item));
			
		}
		
		return $td;
	}
	
	public function helpTextView($form_item)
	{
		if(trim($form_item->helpText()) != '')
		{
			
			
			$container = new TCv_View();
			$container->addClass('help_text_container');
			
			$link = new TCv_Link();
			$link->addClass('help_icon_button');
			$link->setAttribute('tabindex', '-1'); // avoid tabbing on help icons
			$link->setURL('#');
			$link->setIconClassName('fas fa-info-circle');
			$link->setTitle('Learn more');
			
			$container->attachView($link);
			
			$text = new TCv_View();
			$text->addClass('form_info_text ');
			$text->addText($form_item->helpText());
			$container->attachView($text);
			
			return $container;
		}
		return null;
	}
	
	/**
	 * Returns the value column view for the form item
	 * @param TCv_FormItem $form_item
	 * @return TCv_View
	 */
	public function valueColumnView($form_item)
	{
		$td = new TCv_View();
		$tag_type = $this->useDivValue('td');
		$td->setTag($tag_type);
		$td->addClass('value_column');
		if(!$form_item->showTitleColumn() && $tag_type == 'td')
		{
			$td->setAttribute('colspan','2');
			
		}
		
		// Add Form Content
		
		if($icon = $form_item->iconCode())
		{
			$icon_view = new TSv_FontAwesomeSymbol($icon, $icon);
			$icon_view->addClass('field_icon');
			$td->attachView($icon_view);
		}
		
		
		
		$td->attachView($form_item);
		
			
		return $td;
	}
	
	/**
	 * Attaches all the items that are meant to be attached to the form root. This includes hidden fields but also
	 * any fields that have been set to render that way.
	 */
	public function attachFormRootViews() : void
	{
		foreach($this->form_root_form_items as $form_item)
		{
			// Hidden fields just need the form
			if($form_item instanceof TCv_FormItem_Hidden)
			{
				$rendering = $form_item;
			}
			else
			{
				// Manually generating the table row
				$rendering = $this->tableRowView($form_item);
				
			}
			parent::attachView($rendering);
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// SUBMIT BUTTON
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * @return TCv_FormItem_SubmitButton
	 */
	public function submitButton()
	{
		$submit = new TCv_FormItem_SubmitButton($this->submit_button_id, $this->button_text);
		return $submit;
	}
	
	/**
	 * Sets the submit button ID. Useful if there are multiple forms on the page
	 * @param string $id
	 */
	public function setSubmitButtonID($id)
	{
		$this->submit_button_id = $id;
	}


	/**
	 * Sets the button text for this form. This overrides the item name value if set.
	 * @param string $text
	 */
	public function setButtonText($text)
	{
		$this->button_text = $text;
	}
	
	/**
	 * Sets the submit button for this form. This will override any other button settings and use the provided button.
	 *
	 * @param string $button
	 */
	public function setSubmitButton($button)
	{
		$this->submit_button = $button;
	}
	
	/**
	 * Makes it so that the submit button isn't generated. This can be overridden later by resetting the submit button to a nother value
	 */
	public function hideSubmitButton()
	{
		$this->show_submit = false;
	}
	
	/**
	 * Makes it so that the submit button isn't generated. This can be overridden later by resetting the submit button to another value
	 */
	public function showSubmitButton()
	{
		$this->show_submit = true;
	}
	
	/**
	 * Returns if this form has a submit button
	 * @return bool
	 */
	public function hasSubmitButton()
	{
		return $this->submit_button !== false;
	}
	
	/**
	 * Calculates the item name from the model values and if the form is an editor. This function is automatically called
	 * when the form is instantiated and can be overidden with a new name or a new submit button altogether.
	 */
	public function calculateButtonText()
	{
		$this->button_text = ($this->isEditor() ? TC_localize('Update', 'Update') : TC_localize('Create','Create'));
	}
	
	/**
	 * Sets the name of the icon for the submit button
	 * @param string $code
	 */
	public function setSubmitButtonIconCode(string $code)
	{
		$this->submit_button_icon = $code;
	}
	
	/**
	 * Returns the icon code for submit button this form
	 * @return string
	 */
	public function submitButtonIconCode()
	{
		return $this->submit_button_icon;
	}
	
	/**
	 * Adds the submit button to the form at this exact location. This function respects all the settings related to the submit button.
	 */
	public function attachSubmitButton()
	{
		if($this->show_submit && !$this->submit_attached)
		{
			if(!$this->submit_button)
			{
				$this->submit_button = $this->submitButton();
			}

			if($this->disable_all_title_columns)
			{
				$this->submit_button->setShowTitleColumn(false);
			}
			
			if($this->attach_submit_to_form_root)
			{
				$this->submit_button->setToAttachToFormRoot();
			}
			
			
			$this->attachView($this->submit_button);
			
//			// manually add submit button
//			$table_row = $this->tableRowView($this->submit_button);
//			$this->attachView($table_row);
			$this->submit_attached = true;

		}
			
	}
	
	/**
	 * Sets the submit button to attach to the root
	 */
	public function setSubmitToAttachToRoot() : void
	{
		$this->attach_submit_to_form_root = true;
	}

	//////////////////////////////////////////////////////
	//
	// PROCESSOR INTEGRATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Manually override the success url which is passed to the processor
	 * @param string $url  The URL or a URL Target name. The system will try to find a URL Target for the value provied and if it cannot, it will use the normal URL value.
	 * @param TCm_Model|bool $model (Optional) Default false. The model to be included as part of the URL target
	 * used if a URL target is found
	 */
	public function setSuccessURL($url, $model = false)
	{
		if($model != false && !($model instanceof TCm_Model) )
		{
			TC_triggerError('setSuccessURL must pass a model as a second parameter');
		}
		
		// try to find a URL Target name
		// If there's a slash or colon, we know it's not a URL Target
		if(strpos($url, '/') === false && strpos($url, ':') === false)
		{
			$url_target = TC_URLTargetFromModule(false, $url, true); // ignore errors
			if($url_target)
			{
				if($model)
				{
					$url_target->setModel($model);
				}
				$this->success_url = $url_target->url();
				return;
			}
		}
		$this->success_url = $url;
		
	}
	
	/**
	 * Returns the success url
	 * @return bool|string
	 */
	public function successURL()
	{
		return $this->success_url;
		
	}
	
	/**
	 * This function is run before anything occurs with processing and provides a means to deal with values at the
	 * very start of the processing process for this form. The form model is not even set yet, as this might be one
	 * of the things it can alter or tweak. see mirror tables
	 * @param TCc_FormProcessor $form_processor
	 * @return void
	 */
	public static function customFormProcessor_initConfig(TCc_FormProcessor $form_processor) : void
	{
	
	}
	
	/**
	 * A function call that returns a boolean indicating if the form should skip the authentication process ensuring
	 * someone is logged in. WARNING: using this disables the user authentication on the process script for this form.
	 * It should only be used for forms which legitimately can't be processed when a user is not logged in such as the
	 * login form or "forgot my password" form.
	 * @return bool
	 */
	public static function customFormProcessor_skipAuthentication()
	{
		return false;
	}
	
	
	/**
	 * A function call that performs the primary database action. If this class is overrridden, then it should perform
	 * whatever action necessary and call $form_processor->updateDatabase() if the primary action should still occur.
	 * If the primary action shouldn't occur, then return false.
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// No database action by default. 
		//$form_processor->updateDatabase();
	}
	
	
	/**
	 * This function is called after the traditional form processor processFormItems() but before the updateDatabase()
	 * function. A form instance can override this function to do custom form processing that doesn't fit within the
	 * standard operating procedure for many Tungsten forms.
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
	}
	
	/**
	 * This function is called after the traditional form processor updateDatabase() function. A form instance can override
	 * this function to do custom form processing that doesn't fit within the standard operating procedure for many Tungsten forms.
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
	}


	//////////////////////////////////////////////////////
	//
	// MAIN HTML
	//
	//////////////////////////////////////////////////////


	/**
	 * Starts up the form with necessary items relevant to this form
	 */
	public function formStartup()
	{
		$this->setAttribute('action', htmlspecialchars($this->action));
		$this->setAttribute('accept-charset', 'UTF-8');
		$this->setAttribute('method', $this->method);
		if($this->is_multipart)
		{
			$this->setAttribute('enctype', 'multipart/form-data');
		
			$hidden = new TCv_FormItem_Hidden('MAX_FILE_SIZE','Max File Size');
			$hidden->setValue('7340032');
			$hidden->setSaveToDatabase(false);
			$hidden->setShowID(false);
			$this->attachView($hidden);
		}

		if ($this->load_form_tracking)
		{


			$hidden = new TCv_FormItem_Hidden('tracker_name', 'Tracker Name');
			$hidden->setvalue(htmlspecialchars($this->form_tracker->trackerName()));
			$hidden->setSaveToDatabase(false);
			$hidden->setShowID(false);
			$this->attachView($hidden);

			if($this->isEditor())
			{
				$hidden = new TCv_FormItem_Hidden('tracker_item_id', 'Tracker Item ID');
				$hidden->setValue(htmlspecialchars($this->primaryValue()));
				$hidden->setSaveToDatabase(false);
				$hidden->setShowID(false);
				$hidden->setAsPrivateValue();
				$this->attachView($hidden);
			}
		}
		if($this->success_url)
		{
			$hidden = new TCv_FormItem_Hidden('success_url','Success URL');
			$hidden->setValue(htmlspecialchars($this->success_url));
			$hidden->setSaveToDatabase(false);
			$hidden->setShowID(false);
			$this->attachView($hidden);
		}

	}
	
		
	/**
	 * The end of the form output. This handles any cleanup for the form.
	 * @deprecated Use saveFormTrackerValues() instead
	 */
	protected function formFinish()
	{
		$this->saveFormTrackerValues();
	}
	
	/**
	 * Saves the form tracker values
	 */
	public function saveFormTrackerValues()
	{
		if($this->load_form_tracking)
		{
			
			
			// save all the form items to the form tracker
			$this->form_tracker->clearFormItems();
			foreach($this->formItems() as $form_item)
			{
				if($form_item instanceof TCv_FormItem)
				{
					$this->form_tracker->trackFormItem($form_item);
				}

			}

			$this->form_tracker->saveForProcessing();
		}
	}
	
	/**
	 * The html for this form. This class is meant to be a generic form setup so there is little formatting other than
	 * line breaks. To format the form to a specific layout, extend this class and overwrite the html() method.
	 * @param bool $show_form
	 * @return string
	 */
	public function html($show_form = true)
	{
		if($this->form_error)
		{
			return parent::html();
		}
		if($show_form)
		{
			
			
			if ($this->load_form_tracking)
			{
				if (!$this->form_tracker)
				{
					$this->loadFormTracker('no_tracker_' . rand(100, 100000));
				}
				// Generate verification code
				$tracker_verification = new TCv_FormItem_Hidden('tracker_verification', 'Tracker Verification');
				$tracker_verification->setvalue($this->form_tracker->generateVerificationCode());
				$tracker_verification->setSaveToDatabase(false);
				$tracker_verification->setShowID(false);
				$tracker_verification->setAsPrivateValue();
				$this->attachView($tracker_verification);
			}

			$this->configureFormElements();
			
			// Handle focus loading
			if($this->focus_load_id)
			{
				$form_item = $this->formItemWithID($this->focus_load_id);
				if($form_item)
				{
					$form_item->focusOnLoad();
				}
			}
		

			if ($this->load_form_tracking)
			{
				// FORM ITEMS FOR TRACKER
				$tracker_form_class = new TCv_FormItem_Hidden('tracker_form_class', 'Form Class');
				$tracker_form_class->setvalue(get_called_class());
				$tracker_form_class->setSaveToDatabase(false);
				$tracker_form_class->setShowID(false);
				$this->attachView($tracker_form_class);
			}

			$this->formStartup();
			
			// SUBMIT BUTTON SETTINGS
			$this->attachSubmitButton();
			
			
		}
		
		// attach all containers
		$this->renderFormItems();

		$this->attachToFormRoot($this->form_table);
		
		if($show_form)
		{
			
			$this->attachFormRootViews();
			
			$this->saveFormTrackerValues();
			
			// Load JS, init values will be pulled from getClassJSInitValues()
			// This happens last so that calls that occur during form setup are processed
			if($this->load_class_JS)
			{
				$this->addClassJSInit(false);
			}
			
			
		}
		return parent::html();	
	}
	
	/**
	 * Attaches the view to the form root, instead of inside the form table
	 * @param TCv_View $view
	 */
	public function attachToFormRoot($view)
	{
		parent::attachView($view);
	}

	/**
	 * Hides the form with the provided error message
	 * @param string $message
	 */
	protected function hideFormWithError($message)
	{
		$this->form_error = true;
		$access_denied = TSv_AccessDenied::init('form_editor_no_editing_permitted');
		$access_denied->setMainTitle($message);
		parent::attachView($access_denied);

	}


}

	
?>