/**
 * A class to contain common code related to all forms.
 */
class TCv_Form {

	toggle_fields = [];
	loading_listener;

	constructor(element, options) {
		this.element = element;
		if(this.element)
		{
			// Must detect on the entire document, in order to detect outside cancel clicks
			this.element.addEventListener('click', this.handleHelpText.bind(this));

			this.configureVisualToggleFields();

			this.determineFocusLoadField();

			this.enableLoadingIndicatorOnSubmit();
		}
		else
		{
			console.warn('Form object not found');
		}

	}

	/**
	 * Enables the loading indicator on submit, which is enabled in the constructor by default. If you don't want the loading
	 * indicator trigger, you need to disable it with the paired method.
	 *
	 */
	enableLoadingIndicatorOnSubmit() {
		this.loading_listener = this.addTungstenLoading.bind(this);
		// Clicking submit on any form, should trigger the tungsten loading on that form
		this.element.addEventListener('submit', this.loading_listener );
	}

	/**
	 * Disables the loading indicator for this form. Calling this removes the event handler for the loading trigger
	 * so that it won't show apply the tungsten_loading class
	 */
	disableLoadingIndicatorOnSubmit() {

		// Clicking submit on any form, should trigger the tungsten loading on that form
		this.element.removeEventListener('submit', this.loading_listener );
	}

	/**
	 * Method that adds the tungsten_loading class to the form
	 */
	addTungstenLoading() {
		this.element.classList.add('tungsten_loading');
	}

	/**
	 * Determines if a form item has been set that the focus element for loading
	 */
	determineFocusLoadField() {
		let focus_row = this.element.querySelector('.load_focus');
		if(focus_row)
		{
			let field_id = focus_row.getAttribute('data-row-id');
			let field = this.element.querySelector('#'+field_id);

			if(field)
			{
				field.focus();
			}
		}
	}

	/**
	 * Hides a field row with a particular name
	 * @param {string|string[]} names
	 * @param {boolean} use_slide Indicates if a slider should be used
	 */
	hideFieldRows(names,use_slide = false) {

		// Convert single value to array for consistent processing
		if(typeof names === 'string')
		{
			// class selector, get all of them
			if(names.substring(0,1) === '.')
			{
				names = this.element.querySelectorAll(names);
			}
			else
			{
				names = [names];
			}
		}

		for(const value of names)
		{
			let row = null;
			if(value instanceof HTMLElement)
			{
				row = value;
			}
			else if(value.substring(0,1) === '.')
			{
				row = this.element.querySelector(value);
			}
			else
			{
				row = this.element.querySelector('.'+ value + '_row');
			}

			if(row)
			{
				if(use_slide)
				{
					row.slideUp();
				}
				else
				{
					row.hide();
				}
			}

			// If not, try and find a group
			else if(value.substring(0,1) != '.')
			{
				let group = this.element.querySelector('.TCv_FormItem_Group#'+ value + '');
				if(group)
				{
					if(use_slide)
					{
						group.slideUp();
					}
					else
					{
						group.hide();
					}
				}
			}
		}

	}

	/**
	 * A wrapper function that deals with a common issue of a boolean value used to determine if a field should be shown or not.
	 * @param is_show
	 * @param names
	 * @param use_slide
	 */
	showHideFieldRows(is_show, names, use_slide) {
		if(is_show)
		{
			this.showFieldRows(names, use_slide);
		}
		else
		{
			this.hideFieldRows(names, use_slide);
		}
	}




	/**
	 * Shows a field row with a particular name
	 * @param {string|string[]} names or class selector
	 * @param {boolean} use_slide Indicates if a slide animation should be used
	 */
	showFieldRows(names, use_slide = false) {

		// Convert single value to array for consistent processing
		if(typeof names === 'string')
		{
			// class selector, get all of them
			if(names.substring(0,1) === '.')
			{
				names = this.element.querySelectorAll(names);
			}
			else
			{
				names = [names];
			}
		}

		for(const value of names)
		{
			let row = null;
			if(value instanceof HTMLElement)
			{
				row = value;
			}
			else if(value.substring(0,1) === '.')
			{
				row = this.element.querySelector(value);
			}
			else
			{
				row = this.element.querySelector('.'+ value + '_row');
			}

			if(row)
			{
				if(use_slide)
				{
					row.slideDown(500, 'flex');
				}
				else
				{
					row.show('flex');
				}

			}

			// If not, try and find a group
			else if(value.substring(0,1) != '.')
			{
				let group = this.element.querySelector('.TCv_FormItem_Group#'+ value + '');
				if(group)
				{
					if(use_slide)
					{
						group.slideDown(500, 'block');
					}
					else
					{
						group.show('block');
					}
				}
			}
		}
	}


	/**
	 * Adds an event listener to a field on the form. This will setup a standard event listener for the element on the form.
	 * The option exists to trigger_on_run which will immediately call the listener, passing in a mock event with a target
	 * @param {string} field_id The name of the fie
	 * @param {string} event_name The name of the event to be listened for
	 * @param {function} callback The callback function, which should normally be a function of this class
	 * @param {boolean} trigger_on_run Indicate if it should trigger after immediately
	 * @param {object} trigger_values Additional values to be passed in. Combined with the target being set as the field.
	 */
	addFieldEventListener(field_id, event_name, callback, trigger_on_run = false, trigger_values = {}) {
		// add crop ratio listener
		let field = this.element.querySelector('#' + field_id);
		if(field)
		{
			let bound_callback = callback.bind(this);
			field.addEventListener(event_name, bound_callback);
			if(trigger_on_run)
			{
				bound_callback(Object.assign({target:field},trigger_values));
			}
		}
		else
		{
			// detect select date boxes
			let select_date_row = this.element.querySelector('#' + field_id + '_row.TCv_FormItem_SelectDateTime');
			if(select_date_row)
			{
				// Add event listeners to all the select fields
				let bound_callback = callback.bind(this);
				select_date_row.querySelectorAll('select').forEach(el => {
					el.addEventListener(event_name, bound_callback);
				});
			}
			else
			{
				//	console.error('Field with ID ', field_id, 'not found');
			}

		}

	}

	/**
	 * Returns the field value for a field with a specified ID
	 * @param {string} field_id
	 * @return {string}
	 */
	fieldValue(field_id) {
		return this.element.querySelector('#' + field_id).value;
	}

	/**
	 * Sets the field value for a given field ID
	 * @param {string} field_id
	 * @param {string} value
	 * @return {string}
	 */
	setFieldValue(field_id, value) {
		this.element.querySelector('#' + field_id).value = value;
	}

	/**
	 * Delegation event handler for help text clicks
	 * @param event
	 */
	handleHelpText(event) {
		let button = event.target.closest('a.help_icon_button');

		if(button)
		{
			// We're on a button
			event.preventDefault();

			// Button clicked is already showing
			if(button.parentElement.classList.contains('showing'))
			{
				button.parentElement.classList.remove('showing');
			}
			else
			{
				// Remove all of them, then add this one
				let selected = document.querySelector('.help_text_container.showing');
				if(selected)
				{
					selected.classList.remove('showing');
				}

				button.parentElement.classList.add('showing');
			}
		}

		// Clicked inside help text container
		else if(event.target.closest('.help_text_container'))
		{
			// Do nothing
			event.preventDefault();

		}

		// Hiding handled by singular handler at the end of this doc
	}


	//////////////////////////////////////////////////////
	//
	// VISIBILITY TOGGLING
	//
	// These are automated solutions for showing/hiding fields
	// based on the value of another field
	//
	//////////////////////////////////////////////////////


	configureVisualToggleFields() {
		// Find all the data-vis fields
		this.element.querySelectorAll('div[data-vis-toggle-field]').forEach(row => {

			let field_id = row.getAttribute('data-row-id');
			// Groups we just want the ID of the item itself
			if(row.classList.contains('TCv_FormItem_Group'))
			{
				field_id = row.getAttribute('id');
			}

			let source_field_id = row.getAttribute('data-vis-toggle-field');

			let values = {
				source_field_id	 	: source_field_id,
				source_field_value 	: row.getAttribute('data-vis-toggle-show'),
				field_id 			: field_id,
			}

			let field = this.element.querySelector('#' + field_id);
			if(field)
			{
				// track if the affected field is required to maintain that status
				// Groups don't have a required value, so it can vary
				values.required = field.required;
			}

			this.toggle_fields.push(values);

			// Add the event listener for the field, triggering the toggle functionality
			this.addFieldEventListener(source_field_id,'input', this.formVisibilityToggle.bind(this), true);

		});
	}

	/**
	 * Event handler for any fields that are set to trigger other visibility settings.
	 * @param event
	 */
	formVisibilityToggle(event) {
		let source_field_value = event.target.value;
		let source_field_id = event.target.getAttribute('id');

		// Loop through all the set toggle fields, find the ones that match the source field id
		this.toggle_fields.forEach(values => {

			// We have a match, update the visibility of the row of the associated field
			if(values.source_field_id ===  source_field_id)
			{
				let show_fields = false;

				// Test for the case where we look for a non-empty value
				if(values.source_field_value === '!empty')
				{
					show_fields = source_field_value !== null && source_field_value != '';
				}
				else // compare to the value we're looking for
				{
					show_fields = values.source_field_value === source_field_value;
				}

				// values match, show the field rows
				if(show_fields)
				{
					this.showFieldRows(values.field_id, true);
					if(values.required)
					{
						this.element.querySelector('#'+values.field_id).setAttribute('required','required');
					}
				}
				else // hide the row, always disable required
				{
					this.hideFieldRows(values.field_id, true);
					this.element.querySelector('#'+values.field_id).removeAttribute('required');
				}

			}
		});

	}

}

/**
 * Single event handler for clicks that detects if we are clicking OFF of a help text, which needs to happen once to avoid
 * conflicts with multiple forms loading on a page.
 */

document.addEventListener('click', (event) => {
	let help_container = event.target.closest('.help_text_container');

	if(help_container === null)
	{
		let selected = document.querySelector('.help_text_container.showing');
		selected?.classList.remove('showing');
	}

});