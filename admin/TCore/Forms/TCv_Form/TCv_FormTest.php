<?php
class TCv_FormTest extends TC_ViewTestCase
{
	protected function setUp () : void
	{
		$this->view = new TCv_Form('test_view');
	}
	
	public function testSetPrimaryValue()
	{
		$this->view->setPrimaryValue('123');
		$this->assertEquals('123', $this->view->primaryValue());
	}
	
	
}

?>