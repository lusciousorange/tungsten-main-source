<?php

/**
 * Class TCv_FormItem_Hidden
 */
class TCv_FormItem_Hidden extends TCv_FormItem
{
	protected bool $is_private = false;
	
	/**
	 * TCv_FormItem_Hidden constructor.
	 * @param bool|string $id
	 * @param ?string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'hidden';
		$this->uses_container = false;
		$this->hideInSearchableAppliedFilters();
		$this->setToAttachToFormRoot();
		
	}
	
	public function html()
	{
		if($this->isPrivate())
		{
			return '';
		}
		$this->setTag('input');
		$this->setAttribute('type', 'hidden');
		$this->addClassArray($this->form_element_classes);
		$this->addDataValueArray($this->form_element_data_values);
		
		if($this->use_multiple)
		{
			$this->setAttribute('name', htmlspecialchars($this->fieldName()).'[]');
		}
		else
		{
			$this->setAttribute('name', htmlspecialchars($this->fieldName()));
		}
		
		$value = $this->currentValue();
		if(is_array($value))
		{	
			$value = implode(',', $value);
		}
		
		
		$this->setAttribute('value', htmlspecialchars($value));
		
		return parent::html();
	}
	

	/**
	 * Returns the value for the string
	 * @return null|string
	 */
	public function value()
	{
		return $this->currentValue();
	}
	
	/**
	 * Sets the value for the form item
	 * @param $value
	 */
	public function setValue($value)
	{
		$this->setDefaultValue($value);
	
	}
	
	//////////////////////////////////////////////////////
	//
	// PRIVATE VALUES
	//
	// There are times when values need to be passed to
	// the form tracker but we don't want to expose them
	// to the form on the site since they can be manipulated.
	// Private values are passed via the TCm_FormTracker and
	// not shown on the site and can't be manipulated.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Configures this hidden value to be a private value that is passed without any output in the HTML
	 */
	public function setAsPrivateValue()
	{
		$this->is_private = true;
	}
	
	/**
	 * returns if this hidden item is a private value
	 * @return bool
	 */
	public function isPrivate()
	{
		return $this->is_private;
	}
}
?>