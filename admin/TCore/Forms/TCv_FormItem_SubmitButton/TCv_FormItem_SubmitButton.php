<?php

/**
 * Class TCv_FormItem_SubmitButton
 */
class TCv_FormItem_SubmitButton extends TCv_FormItem
{
	protected $button_classes = array(); // 
	protected $use_button_tag = false;
	
	/**
	 * TCv_FormItem_SubmitButton constructor.
	 * @param string $id The id for the form field
	 * @param string $title The title for the form
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'button';
		$this->save_to_database = false;
		$this->setShowTitleColumnContent(false);
	}
	
	/**
	 * Adds a class to the button itself. Used instead of adding it to the row
	 * @param string} $class_name
	 */
	public function addButtonClass($class_name)
	{
		$this->button_classes[] = $class_name;
	}
	
	/**
	 * Sets the submit button to use the <button> tag instead of <input>
	 */
	public function setUseButtonTag()
	{
		$this->use_button_tag = true;
	}
	
	/**
	 * Returns the view for the field
	 * @return TCv_View
	 */
	public function fieldView()
	{
		$id = false;
		if($this->show_id && $this->attributeID() !== false)
		{
			$id = $this->attributeID();
		}
		
		$input = new TCv_View($id);
		if($this->use_button_tag)
		{
			$input->setTag('button');
			$input->addText($this->title);
			$input->setAttribute('aria-label',strip_tags($this->title));
			
		}
		else
		{
			$input->setTag('input');
			$input->setAttribute('value', htmlspecialchars($this->title));
			
		}
		$input->addClassArray($this->button_classes);
		$input->addClassArray($this->form_element_classes);
		
		// Common attributes for both
		$input->setAttribute('type', 'submit');
		
		// Buttons shouldn't have names, no need to submit
		//$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		
		foreach($this->attributes as $name => $value)
		{
			$input->setAttribute($name,$value);
		}
		
		return $input;
	}
	
	
	/**
	 * @return string
	 */
	public function html()
	{
		$this->attachView($this->fieldView());
		
		return parent::html();
	}
	
	
}
?>