<?php
/**
 * Class TCv_FormItem_CheckboxList
 *
 * A list of checkboxes
 */
class TCv_FormItem_CheckboxList extends TCv_FormItem
{
	protected $submitted_value = false; // tracked for later
	protected ?TCv_View $fieldset = null;

	// The Checkbox IDs, must be public so they are grabbed for form processing
	public array $checkbox_ids = [];


	/**
	 * TCv_FormItem_CheckboxList constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		$this->setHTMLCollapsing(false);
		$this->setUseMultiple(','); // Default to putting them all into one row
		$this->addClass('checkbox_list_item');
		
		// Generate the fieldset
		$this->fieldset = new TCv_View($this->id());
		$this->fieldset->setTag('fieldset');
		$legend = new TCv_View();
		$legend->setTag('legend');
		$legend->addClass('sr-only');
		$legend->addText($this->title());
		$this->fieldset->attachView($legend);

		// Saves the checkbox IDs so that it is passed to the processor
		// Ensures the other side has a full list of all the IDs that are being shown
		$this->addSavedProcessorClassProperty('checkbox_ids');
	}

	//////////////////////////////////////////////////////
	//
	// Setting Values
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the values for this checkbox list from two arrays. One array is an array of objects that will be shown.
	 * The array is the list of values that are currently checked off.
	 *
	 * @param TCm_Model[] $option_models An array of objects which will make up the list of items to be shown. The
	 * class must have the methods id() and title() declared in order to work properly. The id() will be the id
	 * associated with the value attribute on the checkbox. The title() will be what is displayed.
	 * @param bool|TCm_Model[] $selected_models (Optional) Default false. An array of objects that indicates the
	 * current set of options that are selected.
	 */
	public function setValuesFromObjectArrays($option_models, $selected_models = false)
	{	
		$selected_values = array(); // array of ids
		if(is_array($selected_models))
		{
			foreach($selected_models as $object)
			{
				if($object instanceof TCm_Model)
				{
					$selected_values[] = $object->id();
				}
				else
				{
					$this->addConsoleError('Bad value of "<em>'.$object.'</em>" passed to Checkbox List ');
				}

			}
		}	
		
		foreach($option_models as $option_model)
		{
			if(!method_exists($option_model, 'title'))
			{
				TC_triggerError('Object provided in setValuesFromObjectArrays() does not have the method <em>title()</em>. This method is required in order to properly display the list of checkbox items.');
			}
			if(!method_exists($option_model, 'id'))
			{
				TC_triggerError('Object provided in setValuesFromObjectArrays() does not have the method <em>id()</em>. This method is required in order to properly display the list of checkbox items.');
			}
			
			$is_checked = in_array($option_model->id(), $selected_values);
			
			
			
			
			$this->addCheckbox($option_model->id(), $option_model->title(), $is_checked);
		}
		
		
	
	}

	//////////////////////////////////////////////////////
	//
	// DISPLAY SETTINGS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Interject to detect checkboxes and ensure they are in the fieldset
	 * @param $view
	 * @param $prepend
	 * @return bool|string|TCv_View
	 */
	public function attachView($view, $prepend = false)
	{
		if($view instanceof TCv_FormItem_Checkbox)
		{
			$this->fieldset->attachView($view);
			return $view;
		}
		else
		{
			return parent::attachView($view, $prepend);
		}
	}
	
	/**
	 * Adds a checkbox to the list with an ID and title.
	 * @param string $id The ID, which will be prefixed by the main view's ID
	 * @param string $title The title for the checkbox
	 * @param bool $default_is_checked (Optional) Default false.
	 * @param bool $disabled (Optional) Default true.
	 */
	public function addCheckbox($id, $title, $default_is_checked = false, $disabled = false)
	{
		$checkbox = $this->createCheckbox($id, $title, $default_is_checked, $disabled);
	
		$this->fieldset->attachView($checkbox);
		
		
	}
	
	/**
	 * Adds a checkbox to the list with an ID and title.
	 * @param string $id The ID, which will be prefixed by the main view's ID
	 * @param string $title The title for the checkbox
	 * @param bool $default_is_checked (Optional) Default false.
	 * @param bool $disabled (Optional) Default true.
	 * @return TCv_FormItem_Checkbox
	 */
	public function createCheckbox($id, $title, $default_is_checked = false, $disabled = false)
	{
		$this->checkbox_ids[] = $id;

		$checkbox = new TCv_FormItem_Checkbox($this->id().'_'.$id, $title);
		$checkbox->addClass('checkbox_list_item');
		$checkbox->setLabel($title);
		$checkbox->setFormValue($id);
		if($default_is_checked > 0)
		{
			$checkbox->setDefaultValue(true);
		}
	
		if($disabled)
		{
			$checkbox->disable();
		}
		// copy save to DB settings
		$checkbox->setSaveToDatabase($this->saveToDatabase());
		
		return $checkbox;
	}


	//////////////////////////////////////////////////////
	//
	// OVERRIDES
	//
	//////////////////////////////////////////////////////

	
	/**
	 * Returns the form items that are checkboxes
	 * @return TCv_FormItem_Checkbox[]
	 */
	public function checkboxFormItems()
	{
		$checkboxes = $this->attached_views;
		foreach($checkboxes as $index => $view)
		{
			if($view instanceof TCv_FormItem_Checkbox)
			{
				// If we're using numbered columns, then we want the value to pass through
				// Otherwise we assume the main view will pass along the array of values
				// Last option is custom programming that connect to a pairing table, which also means false
				// Also, if the main item is set to not save to the database, then neither should the checkboxes
				if($this->useMultipleSetting() != TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS || $this->saveToDatabase() === false)
				{
					$view->setSaveToDatabase(false);
				}
			}
			else
			{
				unset($checkboxes[$index]);

			}

		}

		return $checkboxes;
	}
	
	/**
	 * Returns the submitted value for the form which is an array of values that were selected
	 * @return array
	 */
	public function submittedValue()
	{
		// Lazy loading to avoid duplicate efforts of finding other values
		if($this->submitted_value === false)
		{
			$this->submitted_value = array();

			// The checkbox IDs are saved in the form tracker so we can access it here
			// Only necessary for numbered columns. The pairing tables work differently
			if($this->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS)
			{
				foreach($this->checkbox_ids as $id)
				{
					$field_name = $this->id() . '_' . $id;
					$value = $_POST[$field_name];
					$this->submitted_value[$field_name] = trim($value);

				}
			}
			else // PAIRING TABLE OR OTHERS, just the ones we care about
			{
				foreach($_POST as $field_name => $value)
				{
					// Find fields that are related to this one, but not this one
					if(str_starts_with($field_name, $this->id().'_'))
					{
						$this->submitted_value[$field_name] = trim($value);
					}
				}
			}


		}
		return $this->submitted_value;
	}
	
	/**
	 * Returns the list of checkbox form items to ensure they are tracked during the form processing.
	 * @return TCv_FormItem_Checkbox[]
	 */
	public function additionalFormItemsToTrack()
	{
		return $this->checkboxFormItems();
	}
	
	public function html()
	{
		// Add the counts to the fieldset
		$num_options = count($this->checkboxFormItems());
		$this->fieldset->addDataValue('num-options', $num_options);
		if($num_options < 8)
		{
			$this->fieldset->addClass('range_less_than_8');
		}
		elseif($num_options < 16)
		{
			$this->fieldset->addClass('range_8_to_16');
		}
		else
		{
			$this->fieldset->addClass('range_16_plus');
		}
		
		$this->attachView($this->fieldset);
		
		
		
		foreach($this->checkboxFormItems() as $checkbox)
		{
			if(isset($this->saved_value[$checkbox->id()]))
			{
				$checkbox->setSavedValue(true);
			}
			elseif($this->saved_value_set) // values were set, avoid false values since it would load the default if no saved is provided
			{
				$checkbox->setSavedValue(false);
				
			}
		}
		
		return parent::html();
	}
}
?>