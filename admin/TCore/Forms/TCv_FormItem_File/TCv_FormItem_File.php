<?php

class TCv_FormItem_File extends TCv_FormItem
{
	protected $perform_upload_to_folder = true;
	protected $show_preview = false; // [bool] = Flag to indicate if the preview of the file should be shown.
	
	protected $hash_filename = false; // [bool] = Indicates if the filename should be turned into a hash 
	
	protected $uploaded_filename = false; // [string] = The name of the file that was uploaded to the server
	protected $remove_url = ''; // [string] =The url for removing the file
	
	protected $blob_field = false;
	protected $model_class = false;
	
	/**
	 * @param $id
	 * @param $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		$this->addProcessMethodCall('validateFileBlackList', 
			TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO);
		
	}
	
	/**
	 * Override the standard function for getting the submitted value. This function returns the name of the file that was provided.
	 * @return array|mixed|string
	 */
	public function submittedValue()
	{
		return $_FILES[$this->id]['name'];
	}
	
	/**
	 * @return bool|mixed|null
	 */
	public function databaseValue()
	{
		$value = $this->uploaded_filename;
		if($this->emptyValuesAsNull() && $value == '')
		{
			return NULL;
		}
		return $value;

	}
	
	/**
	 * @param string $class_name
	 * @return void
	 */
	public function setModelClassName($class_name)
	{
		$this->model_class = $class_name;
	}
		
	/**
	 * Indicate that the uploaded file will be saved to a blob in the database rather than to the server. In this scenario, the database table in question must be setup to save blob files. When saving to blob, the filename will still be saved to the actual database unless the form field is set to not save to database.
	 * @param string $blob_field_name
	 * @return void
	 */
	public function setSaveToBLOB($blob_field_name)
	{
		$this->blob_field = $blob_field_name;
		$this->perform_upload_to_folder = false;
	}

	/**
	 * A method that will save the blob file
	 * @param string $blob_field_name
	 * @param $form_processor
	 * @return void
	 */
	public function saveBlobFile($blob_field_name, $form_processor)
	{
		// save the uploaded filename, which is pulled by the DB
		$this->uploaded_filename = $_FILES[$this->id()]['name'];

		// open the file, save it to the database
		$file_object = fopen($_FILES[$this->id()]['tmp_name'], 'rb');
		$form_processor->addDatabaseValue($blob_field_name, $file_object, PDO::PARAM_LOB);
	}
	
	/**
	 * Performs a check to see if the file provided is on the black list of filetypes that are not permitted to be uploaded.
	 * @return TCm_FormItemValidation
	 */
	public function validateFileBlackList()
	{
		$validation = new TCm_FormItemValidation($this->id(), 'File Type - Black List');
		
		$filename = $_FILES[$this->id()]['name'];
		if($filename != '')
		{
			$extension = explode(".",$filename); 
			$extension = strtolower($extension[sizeof($extension)-1]);
			
			$black_list = array('exe','app','php','asp','aspx'); // extensions that aren't permitted
			if(in_array($extension, $black_list))
			{
				$validation->failFieldWithMessage($this->title(),' is of a filetype that is not permitted to be uploaded.');
				$validation->setDetails('Extension = '.htmlspecialchars($extension));
			}
		}	
		else
		{
			$validation->setIsSkipped();
			$validation->setDetails('No file provided');
		}
		
		return $validation;
	
	}
	
	/**
	 * Sets the form item to confirm that the file uploaded is of a particular type
	 * @param array $permitted_extensions
	 * @return void
	 */
	public function setValidateFileType($permitted_extensions)
	{
		$this->addProcessMethodCall('validateFileType', 
			TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
			$permitted_extensions);	
	
	}
	
	/**
	 * Performs a check to see if the file provided matches a set of type specifications.
	 * @param $permitted_extensions
	 * @return TCm_FormItemValidation
	 */
	public function validateFileType($permitted_extensions)
	{
		$validation = new TCm_FormItemValidation($this->id(), 'Validate File Type');
		$filename = $_FILES[$this->id()]['name'];
		if($filename != '')
		{
			$extension = explode(".",$filename); 
			
			$extension = strtolower($extension[sizeof($extension)-1]);
			// Check against a blacklist
			// not found in the list
			if(array_search($extension, $permitted_extensions) === false) // returns index so must check for false and not 0
			{
				$validation->failFieldWithMessage($this->title(),' does not match the permitted list of file types. The list of permitted file types are '.implode(', ', $permitted_extensions).'.');
				$validation->setDetails('Extension = '.htmlspecialchars($extension));
			}
			
		}
		else
		{
			$validation->setIsSkipped();
			$validation->setDetails('No file provided');
		}
		return $validation;
	
	}
	
	/**
	 * Sets the form item to validate an image to be an exact file size
	 * @param $width
	 * @param $height
	 * @return void
	 */
	public function setValidateExactImageDimensions($width, $height)
	{
		$this->addProcessMethodCall('validateExactImageDimensions', 
			TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
			$width, 
			$height);	
	
	}
	
	/**
	 * Performs a check to see if the file provided matches a set of type specifications.
	 * @param int $width
	 * @param int $height
	 * @return TCm_FormItemValidation
	 */
	public function validateExactImageDimensions($width, $height)
	{
		$validation = new TCm_FormItemValidation($this->id(), 'Image - Exactly '.$width.' x '.$height.'');
		
		$filename = $_FILES[$this->id()]['name'];
		if($filename != '')
		{
			list($image_width, $image_height) = @getimagesize($_FILES[$this->id()]['tmp_name']);
		
			if($image_width == 0 || $image_height == 0)
			{
				$validation->failFieldWithMessage($this->title(),' does not appear to an image.');
			}
			elseif($image_width != $width || $image_height != $height)
			{
				$validation->failFieldWithMessage($this->title(),' Invalid image size. Requires '.$width.' x '.$height.' pixels. Provided: '.$image_width.' x '.$image_height.' pixels');
				$validation->setDetails('Image Dimensions = '.$image_width.' x '.$image_height);
			}
		}
		else
		{
			$validation->setIsSkipped();
			$validation->setDetails('No file provided');
		}
		return $validation;
	
	}
	
	/**
	 * Sets the form item to validate an image to be a minimum width and height
	 * @param int $width
	 * @param int $height
	 * @return void
	 */
	public function setValidateMinimumImageDimensions($width, $height)
	{
		$this->addProcessMethodCall('validateMinumumImageDimensions', 
			TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
			$width, 
			$height);	
	
	}
	
	/**
	 * Performs a check to see if the file provided matches a set of type specifications.
	 * @param $width
	 * @param $height
	 * @return TCm_FormItemValidation
	 */
	public function validateMinumumImageDimensions($width, $height)
	{
		$validation = new TCm_FormItemValidation($this->id(), 'Image - Minimum of '.$width.' x '.$height.'');
		
		$filename = $_FILES[$this->id()]['name'];
		if($filename != '')
		{
			list($image_width, $image_height) = @getimagesize($_FILES[$this->id()]['tmp_name']);
		
			if($image_width == 0 || $image_height == 0)
			{
				$validation->failFieldWithMessage($this->title(),' does not appear to an image.');
			}
			elseif($image_width < $width || $image_height < $height)
			{
				$validation->failFieldWithMessage($this->title(),' Invalid image size. Minimum '.$width.' x '.$height.' pixels. Provided: '.$image_width.' x '.$image_height.' pixels');
				$validation->setDetails('Image Dimensions = '.$image_width.' x '.$image_height);
			}
		}
		else
		{
			$validation->setIsSkipped();
			$validation->setDetails('No file provided');
		}
		return $validation;
	
	}

	
	/**
	 * A passthrough function to save the submitted upload path.
	 * @param $class_name
	 * @return void
	 */
	public function saveModelClassName($class_name)
	{
		$this->model_class = $class_name;
	}
	
	/**
	 * A FormProcessor action. This method is called during the processing to upload the file to the appropriate folder.
	 * @return TCm_FormItemValidation
	 */
	public function uploadFile()
	{
		$model_class = $this->model_class;
		$upload_folder_from_server_root = $model_class::uploadFolder();
		
		$validation = new TCm_FormItemValidation($this->id(), 'File Upload');
		$filename = $_FILES[$this->id()]['name'];
		// Not ideal,but currently the only way to check if an error system-wide occured
		$this->messenger = TCv_Messenger::instance();
		
		
		if(!$this->messenger->isSuccess())
		{
			$validation->setIsSkipped();
			$validation->setDetails('Form item failed previous validations.');
		}
		elseif($filename == '')
		{
			$validation->setIsSkipped();
			$validation->setDetails('No file provided');
			$this->setSaveToDatabase(false); // blank filenames shouldn't get sent to DB
		}
		else
		{
			// Get unique filename
			$this->uploaded_filename = $this->uniqueFilename();
			// Copy the file to the new location
			$filepath = $upload_folder_from_server_root.$this->uploaded_filename;
			
			if(copy($_FILES[$this->id()]['tmp_name'], $filepath))  // upload the file			
			{	
				chmod($filepath, 0644); // change permissions to allow others to read
			}
			$details = 'Filename: '.$this->uploaded_filename;
			$details .= '<br />Location: '.$upload_folder_from_server_root;
			
			$details .= '<br />File Size: '.$this->bytesToUnits(filesize($filepath));
			$validation->setDetails($details);
		}	
		
		
		return $validation;
	}
	
	/**
	 * Returns the name that the file will be uploaded as. This function checks to ensure that an existing file with that name does not already exist.
	 * @return string
	 */
	protected function uniqueFilename()
	{
		$model_class = $this->model_class;
		$upload_folder_from_server_root = $model_class::uploadFolder();
		
		
		$temp_file_name = $_FILES[$this->id]['name'];
		$file_name = explode(".",$temp_file_name); // separate the base and extension
		$last_index = sizeof($file_name) - 1; // get the extension index
		$extension = $file_name[$last_index]; 
		
		// create the new filename subtracting the dot '.' and the extension
		$file_name = substr($temp_file_name, 0 , strlen($temp_file_name) - 2 - strlen($extension));
		
		if($this->hash_filename)
		{
			//$non_valid_chars = array('.','#','<','>','@','#','%','$','!');
			$file_name = substr(md5(trim($file_name)), 0, 20); // create a hash of the filename
		}
		else // not hashed 
		{
			$non_valid_chars = array('<','>','#','%','&','+','/',"'",'"');
			$file_name = str_replace($non_valid_chars, '', $file_name); // get rid of multiple dots
			$file_name = str_replace(' ', '_', $file_name); // get rid of spaces
		}
		
		$usable_name = false;
		$count = 1;
		$temp_file_name = $file_name.'.'.$extension; // save the new temp
		while(!$usable_name)
		{	
			if(@file_exists($upload_folder_from_server_root.$temp_file_name))
			{
				$count++; // add one to the count
				$temp_file_name = $file_name.'-'.$count.'.'.$extension; // change image name to be name-#.jpg
				
				//if($use_hash)
				//{
				//	$temp_file_name = $file_name = substr(md5(trim($file_name.'-'.$count)), 0, 20).'.'.$extension;
				//}
			}
			else
			{
				$usable_name = true;
			}
		}
		return $temp_file_name;
	}

	/**
	 * Sets the form item to save the name of the file as a hash instead of the actual filename. This is useful for hiding the actual name of the file.
	 * @param $use_hash
	 * @return void
	 */
	public function setHashFilename($use_hash = true)
	{
		$this->hash_filename = $use_hash;
	}
	
	
	/**
	 * Sets the link for removing the image. When activated, an 'x' will appear to provide the option to remove the file
	 * @param $url
	 * @return mixed
	 */
	public function setRemoveURL($url)
	{
		return $this->remove_url = $url;
	}
	
	/**
	 * Sets the show preview value which indicates if a file preview is shown below the form field
	 * @param $show
	 * @return mixed|true
	 */
	public function setShowPreview($show = true)
	{
		return $this->show_preview = $show;
	}
	
	/**
	 * Returns the html for the text field.
	 * @return string
	 */
	public function html()
	{
		$input = new TCv_View(htmlspecialchars($this->id));
		$input->setTag('input');
		$input->addClassArray($this->classes());
		$input->setAttribute('type','file');
		$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		$input->addClassArray($this->form_element_classes);
		$input->addDataValueArray($this->form_element_data_values);
		
		$this->attachView($input);
		
		if($this->currentValue() != '')
		{
			$file_previewer = new TCv_View();
			$file_previewer->addClass('form_file_preview');
			
			if($this->remove_url != '' && $this->show_preview)
			{
				$remove_link = new TCv_Link();
				$remove_link->setURL(htmlspecialchars($this->remove_url));
				$remove_link->addClass('delete_button');
				$remove_link->addText('x');
				$file_previewer->attachView($remove_link);
				
			}
			
			$model_class = $this->model_class;
			$file = new TCm_File($this->id().'_file', $this->currentValue(),  $model_class::uploadFolder(), TCm_File::PATH_IS_FROM_SERVER_ROOT);
			if($file->exists())
			{	
				$file_view = TCv_File::viewForFile($this->id().'_view', $file);
				$file_view->refreshCache();
				$file_view->setViewBox(40,40);
				
				$file_link = new TCv_Link();
				$file_link->openInNewWindow();
				$file_link->setURL($file->filenameFromSiteRoot());
				$file_link->attachView($file_view);
				$file_link->addText('File: '.$file->filename());
				$file_link->addText('<br />Size: '.$file->filesize());
				if($file_view instanceof TCv_Image)
				{
					$file_link->addText('<br />Dimensions: '.$file_view->actualWidth().' x '.$file_view->actualHeight());
				}
				
				
				$file_previewer->attachView($file_link);
				$this->attachView($file_previewer);
			
			}
			
			
		}
		
		return parent::html();
	}
	
	
		
	/**
	 * Extend functionality to test if the form is an editor. If so, then undo the need for validate blank.
	 * @param $form
	 * @return void
	 */
	public function setForm(&$form)
	{
		if(!($form instanceof TCv_Form))
		{
			TC_triggerError('Provided form was not of type TCv_Form');
		}
		
		// Handle if we're being added to the form and this value hasn't been set yet
		if($this->model_class == '')
		{
			if($form instanceof TCv_FormWithModel)
			{
				$this->model_class = $form->modelName();
			}
		}
		
		if($this->model_class == '')
		{
			TC_triggerError('TCv_FormItem_FileDrop requires a model class to be set using the method setModelClassName(). That class must implement a <em>static</em> method called uploadFolder() which returns the absolute server path for where the file should be put. ');
		}
		
		if(!method_exists($this->model_class, 'uploadFolder'))
		{
			TC_triggerError('The class "'.$this->model_class.'" must implement a <em>static</em> method called uploadFolder() which returns the absolute server path for where the file should be put. ' );
		}
		
		$this->form = $form;
	}
	
	/**
	 * Override of the handleAttachedToView method that checks to see that an upload folder has been set. This method also adds the uploadFile method call to the process list to ensure that the file upload is the last validation/action that is attempted.
	 * @return void
	 */
	public function handleAttachedToView()
	{
		$model_class = $this->model_class;
		$upload_folder_from_server_root = $model_class::uploadFolder();
		
		
		// if editor, remove the method call for it
		// a bit of a special case, but here we go
		if($this->form()->isEditor())
		{
			foreach($this->processMethodCalls() as $index => $method_call_values)
			{
				if($method_call_values['method_name'] == 'validateIsBlank')
				{
					unset($this->process_calls[$index]);
				}
			}
		}
		
		if($this->perform_upload_to_folder)
		{
			// Add this during the adding to form to ensure it is the last method call
			$this->addProcessMethodCall('saveModelClassName()', 
				TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
				TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
				TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
				$this->model_class);	
		
		
			$this->addProcessMethodCall('uploadFile()', 
				TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
				TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
				TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO);

		}
		
		
		// Handle blob field settings to 
		if($this->blob_field != false)
		{
			$this->addProcessMethodCall(
				'saveBlobFile()', // The method to call
				TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD, // Indicate it's a method of this form item
				TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE, // Do this after the DB update occurs, separate interaction
				TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO, // No need to pass in the form field ID
				$this->blob_field // A value to be passed in as a parameter
				);	
				

		}
		
	}
	
	
}
?>