<?php
require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

// Grab the ID, if it's not an int, then false
$id = $_POST['dynamic_call_model_id'];

$model = null;
if($id === 'static') // Static call
{
	$model = $_POST['dynamic_call_class_name'];
}
elseif(is_int($id)) // Model provided
{
	$model = ($_POST['dynamic_call_class_name'])::init($id);
}
else // singleton such as a model list
{
	$model = ($_POST['dynamic_call_class_name'])::init();
}


$model2 = TCm_Model::init(false);


// We have a model to work with
if(!is_null($model))
{
	$method_name = $_POST['dynamic_call_method'];
	//$results = $model->$method_name($_POST['search'], $_POST['current_value']);
	
	$results = call_user_func_array([$model, $_POST['dynamic_call_method']], [$_POST['search']]);
	
	// Determine the title method
	if(substr( $_POST['response_model_class_title_method'],0,5) == 'title'
		&& method_exists($_POST['response_model_class_name'], $_POST['response_model_class_title_method']))
	{
		$title_method_name = $_POST['response_model_class_title_method'];
	}
	else
	{
		$title_method_name = 'title';
	}
	
	// Match the response format for TomSelect
	$response_value = [];
	foreach($results as $id => $title)
	{
		// If we get a model, just return the title instead
		if($title instanceof TCm_Model)
		{
			$id = $title->id(); // save the ID first
			$title = $title->$title_method_name();
			
		}
		$response_value[] = [
			'value' => $id,
			'text' => $title
		];
	}
	
	echo json_encode($response_value);
}


