<?php
/**
 * Class TCv_FormItem_Select
 * A <select> form item used in a form
 */
class TCv_FormItem_Select extends TCv_FormItem
{
	protected $options = []; // The list of options to be shown for the select field
	protected $option_attributes = [];
	
	protected $use_filtering = false; // [bool] = Indicate if filtering shoudl be included for when they type.
	
	
	/**
	 * TCv_FormItem_Select constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		
		$this->addJSClassInitValue('field_id', $this->id());
	}


	//////////////////////////////////////////////////////
	//
	// FORM ELEMENT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the view for the select field
	 * @return TCv_View
	 */
	public function fieldView()
	{
		// Ensure we only do this once
		if($this->use_filtering)
		{
			$this->addCSSFile('tom_select', "https://cdn.jsdelivr.net/npm/tom-select@2.2.2/dist/css/tom-select.css");
			$this->addJSFile('tom_select', "https://cdn.jsdelivr.net/npm/tom-select@2.2.2/dist/js/tom-select.complete.min.js");

			$this->configureCurrentValueJSInitValues();
			
			$this->addClassJSFile('TCv_FormItem_Select');
			$this->addClassJSInit('TCv_FormItem_Select');


		}
		
		$id = false;
		if($this->show_id && $this->attributeID() !== false)
		{
			$id = $this->attributeID();
		}
		
		$select = new TCv_View($id);
		$select->setTag('select');
		$select->setAttribute('data-default-value', $this->currentValue());
		$select->addClassArray($this->form_element_classes);
		$select->addDataValueArray($this->form_element_data_values);
		
		if($this->use_multiple)
		{
			$select->setAttribute('multiple', 'multiple');
			$select->setAttribute('name', htmlspecialchars($this->fieldName()).'[]');
		}
		else
		{
			$select->setAttribute('name', htmlspecialchars($this->fieldName()));
		}

		if($this->disabled)
		{
			$select->setAttribute('disabled','disabled');
		}
		
		if($this->is_required == 1) // value of 2 doesn't require it, just shows the *
		{
			$select->setAttribute('required', 'required');
		}
		
		foreach($this->attributes as $name => $value)
		{
			$select->setAttribute($name,$value);
		}
		
		$current_option_group = false;
				
		foreach($this->options as $value => $title)
		{
			if(substr($value,0,11) == 'optgroupend')
			{
				if($current_option_group !== false)
				{
					$select->attachView($current_option_group);
				}
				$current_option_group = false;
			}
			
			// STARTING AN OPTION GROUP
			elseif(substr($value,0,8) == 'optgroup')
			{
				if($current_option_group !== false)
				{
					$select->attachView($current_option_group);
				}
				$current_option_group = new TCv_View(substr($value,9));
				$current_option_group->setTag('optgroup');
				$current_option_group->setAttribute('label', htmlspecialchars($title));
			}
			else
			{
				$option = new TCv_View(htmlspecialchars($this->id).'_'.htmlspecialchars($value));
				$option->setTag('option');
				$option->setAttribute('value', htmlspecialchars($value));
				$option->addText(htmlspecialchars($title));
				
				$current_type = gettype($this->currentValue());
				
				if($current_type == 'boolean')
				{
					if($this->currentValue() == $value)
					{
						$option->setAttribute('selected', 'selected');
					}
				}
				elseif(is_array($this->currentValue()))
				{
					
					foreach($this->currentValue() as $selected_value)
					{
						if(strcmp($selected_value, $value) == 0)
						{
							$option->setAttribute('selected', 'selected');
						}
						
					}
				}
				else
				{
					if(strcmp($this->currentValue(), $value) == 0)
					{
						$option->setAttribute('selected', 'selected');
					}
					
				}
				
				// Attributes
				foreach($this->option_attributes[$value] as $attribute_name => $attribute_value)
				{
					$option->setAttribute($attribute_name, $attribute_value);
				}
				
				
				// Add to Option Group
				if($current_option_group !== false)
				{
					$current_option_group->attachView($option);
				}
				else
				{
					$select->attachView($option);
				}
			}
			
		}
		
		if($current_option_group !== false)
		{
			$select->attachView($current_option_group);
		}




		return $select;

	}
	
	/**
	 * Static method to get the symbol for the arrow for selects
	 * @return TSv_FontAwesomeSymbol
	 */
	public static function fontAwesomeArrowSymbol() : TSv_FontAwesomeSymbol
	{
		// Tungsten 9 uses a different arrow for selects the icon for the caret for the drop-down
		if(TC_getConfig('use_tungsten_9') && TC_isTungstenView())
		{
			return new TSv_FontAwesomeSymbol('TCv_Form9_select_arrow', 'fas fa-chevron-down');
		}
		
		// Return the caret down which was the default
		return  new TSv_FontAwesomeSymbol('select_caret', 'fas fa-caret-down');
		
	}
	
	/**
	 * Returns the html, attaching the field view
	 * @return string
	 */
	public function html()
	{

		// Wrap field in container and add the caret
		$container = new TCv_View();
		$container->addClass('select_container');

		$container->attachView($this->fieldView());
		
		$container->attachView(static::fontAwesomeArrowSymbol());
		
		
		
		$this->attachView($container);

		return parent::html();

	}
	
	

	//////////////////////////////////////////////////////
	//
	// OPTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the options set for this array
	 * @return array
	 */
	public function options()
	{
		return $this->options;
	}
	
	/**
	 * Returns value for an option based on teh value
	 * @param string $value
	 * @return string
	 */
	public function optionTitle($value)
	{
		return $this->options[$value];
	}
	
	
	/**
	 * Overrides any existing values and sets the options to be the provided array
	 * @param array $options Must be associative array with indices being the values
	 */
	public function setOptions($options)
	{
		foreach($options as $value => $title)
		{
			$this->addOption($value, $title);
		}
	}
	
	/**
	 * Adds an option to the select box
	 * @param string/int $value
	 * @param string $title
	 */
	public function addOption($value, $title)
	{
		if( is_null($value) )
		{
			$this->setEmptyValuesAsNull();
			$value = '';
		}
		
		// There are no options yet, this is the default
		// Don't include option groups
		if(count($this->options) == 0
			&& $this->defaultValue() == '' // Don't bother if it's been set already
			&& !str_starts_with($value,'optgroup_'))
		{
			$this->setDefaultValue($value);
		}
		
		$this->options[$value] = $title;
		$this->option_attributes[$value] = [];
	}
	
	/**
	 * Adds an option attribute which be attached to the html of that particular option inside the element.
	 * @param string/int $option_value
	 * @param string $name
	 * @param string $value
	 */
	public function addOptionAttribute($option_value, $name, $value)
	{
		$this->option_attributes[$option_value][$name] = $value;
	}

	/**
	 * Disables an option value
	 * @param string $option_value
	 */
	public function disableOption($option_value)
	{
		$this->addOptionAttribute($option_value, 'disabled','disabled');
	}

	/**
	 * Turns on the feature which hides disabled options.
	 */
	public function hideDisabledOptions()
	{
		$this->addClass('hide_disabled');
	}
	
	/**
	 * Returns if this select box has the option indicated
	 * @param string $value
	 * @return bool
	 */
	public function hasOption($value)
	{
		return isset($this->options[$value]);
	}
	
	/**
	 * Sets the options using a number range. For decending order, put the higher number first
	 * @param int $first
	 * @param int $last
	 */
	public function setOptionsWithNumberRange($first, $last)
	{
		if($last > $first) // ascending
		{
			for($i = $first; $i <= $last; $i++)
			{
				$this->addOption($i, $i);
			}
		}
		else // descending
		{
			for($i = $first; $i >= $last; $i--)
			{
				$this->addOption($i, $i);
			}
		}
	}
	
	/**
	 * Sets the options. Same as addOptions
	 * @param array $values
	 * @deprecated See addOptions()
	 * @see TCv_FormItem_Select::addOptions()
	 */
	public function setOptionsWithArray($values)
	{
		$this->addOptions($values);
	}
	
	/** Adds options to the select by providing an array
	 * @param $values
	 */
	public function addOptions($values)
	{
		foreach($values as $index => $title)
		{
			$this->addOption($index, $title);
		}
	}
	
	
	/**
	 * Removes an option from the view
	 * @param string/int $value
	 */
	public function removeOption($value) : void
	{
		unset($this->options[$value]);
		unset($this->option_attributes[$value]);
	}
	
	
	/**
	 * Adds an option group
	 * @param string $value
	 * @param string $title
	 */
	public function startOptionGroup($value, $title)
	{
		$this->addOption('optgroup_'.$value, $title);
	}
	
	/**
	 * Ends an option group, allowing the items to fall back to the main entry
	 */
	public function endOptionGroup()
	{
		$this->addOption('optgroupend_'.microtime(true), '');
	}
	
	//////////////////////////////////////////////////////
	//
	// TCv_SearchableModelList
	//
	//////////////////////////////////////////////////////
	
	public function clearableInSearchableAppliedFilters(): bool
	{
		// If there is no empty-string option, then we can't clear it no matter what
		if(!$this->hasOption(''))
		{
			return false;
		}
		
		return parent::clearableInSearchableAppliedFilters();
	}
	
	//////////////////////////////////////////////////////
	//
	// FILTERING
	//
	//////////////////////////////////////////////////////

	/**
	 * Turns on a feature in which the available options narrow down as the user types. This switches the standard
	 * select box into a box with a field to be typed.
	 *
	 * This method has an option to indicate that dynamic loading is used. In that scenario, the field will do a
	 * server call whenever the value is changed. The response from the server is JSON with the IDs and Title for the
	 * values to be shown.
	 *
	 * The method will receive two values. The first is the search string, the second is the current value. It is up
	 * to the method being called to properly return the current value.
	 *
	 * @param null|TCm_Model|string $dynamic_load_model (Optional) Default null. The model that should be called when
	 * loading dynamically. If it is a static method, you should pass the string name of the model
	 * @param null|String $dynamic_load_method (Optional) Default null. The method called on the model that returns
	 * the appropriate values for the select box.
	 * @param null|String $dynamic_load_response_class_name (Optional) The class name for the item being loaded
	 * @param string $dynamic_load_model_title_method The name of the method to be called on the returned model to
	 * show on the site. It must begin with 'title' in order to function. This is a security restriction to prevent
	 * insertion.
	 */
	public function useFiltering($dynamic_load_model = null,
	                             $dynamic_load_method = null,
	                             $dynamic_load_response_class_name = null,
	                             $dynamic_load_model_title_method = 'title')
	{
		$this->use_filtering = true;
		
		// Add a default which is required when using filtering since this avoids the first one being auto-selected
		$this->addOption('','Select an item');
		
		if(!is_null($dynamic_load_model) && !is_null($dynamic_load_method))
		{
			if(is_object($dynamic_load_model))
			{
				$this->addJSClassInitValue('dynamic_call_class_name', get_class($dynamic_load_model));
				$this->addJSClassInitValue('dynamic_call_model_id', $dynamic_load_model->id());
			}
			else // Static method name
			{
				$this->addJSClassInitValue('dynamic_call_class_name', $dynamic_load_model);
				$this->addJSClassInitValue('dynamic_call_model_id', 'static');
			}
			
			$this->addJSClassInitValue('dynamic_call_method', $dynamic_load_method);
			
			
			$this->addJSClassInitValue('response_model_class_name', $dynamic_load_response_class_name);
			
			if(substr($dynamic_load_model_title_method,0,5) != 'title')
			{
				TC_triggerError('Title method for filtering MUST being with "title". ');
			}
			$this->addJSClassInitValue('response_model_class_title_method', $dynamic_load_model_title_method);
			
		}
		
		
	}
	
	/**
	 * Configures the field to have a value to start
	 * @return void
	 */
	protected function configureCurrentValueJSInitValues()
	{
		$current_value = $this->currentValue();
		
		// Dynamic loading
		if(isset($this->js_init_values['response_model_class_name']) && $current_value > 0)
		{
			
			// Ensure we have an array for the current value
			// We loop through them and add them regardless
			if(!is_array($current_value))
			{
				$current_value = [$current_value];
			}
			
			$response_class_name = $this->js_init_values['response_model_class_name'];
			$response_method_name = $this->js_init_values['response_model_class_title_method'];
			
			$values = [];
			foreach($current_value as $id)
			{
				// If we have a current value, we can look up the title if we were given a response class name
				if(class_exists($response_class_name))
				{
					$current_model = ($response_class_name)::init($id);
					if($current_model)
					{
						$values[$id] = $current_model->$response_method_name();
					}
					else
					{
						$values[$id] = 'Model '.$this->id();
					}
					
				}
				else // no class exists
				{
					$values[$id] = $id;
				}
			}
			
			// Add all these current values as proper options with the response method being called
			foreach($values as $id => $title)
			{
				$this->addOption($id, $title);
			}
			
			$this->addJSClassInitValue('current_value', $this->currentValue());
			
		}
	}
	
	
}


