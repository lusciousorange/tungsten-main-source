class TCv_FormItem_Select {
	element;
	timeout = null;
	fetch_abort_controller = null;
	input_field = null;
	use_dynamic_loading = false;

	options = {
		current_value : '',
		field_id : null,

		// The name of the class that is going to call a method to get values
		dynamic_call_class_name : null,

		// The ID of the class_name above
		dynamic_call_model_id 	: null,

		// The name of the method called on that class+id object instantiated above
		dynamic_call_method 	: null,

		// The name of the class that we're loading that come in the response
		response_model_class_name 	: null,

		response_model_class_title_method 	: 'title',


	};

	constructor(element, params) {
		this.element = element;

		// Save the options
		Object.assign(this.options, params);


		// Save if we're using dynamic loading
		this.use_dynamic_loading =
			this.options.dynamic_call_method !== null
			&& this.options.dynamic_call_class_name !== null;

		// Create the load function if we're using dynamic loading
		let load_function = null;
		if(this.use_dynamic_loading)
		{
			load_function = (query, callback) => {this.updateDynamicSearchResults(query, callback)};
		}

		// Configure the TomSelect for search and dropdowns
		this.element.tom_select = new TomSelect(element, {
			searchResultLimit : false,
			duplicateItemsAllowed : false,
			load : load_function,
			maxOptions : null, // No max on the options that are shown

			// Function to run to initialize the selector
			onInitialize : () => {
				this.input_field = this.element.parentElement.querySelector('.ts-control input');

			},
			onChange : (value) => { this.changeHandler(value);},

			plugins: {

				// Adds the remove button to each item
				// @see https://tom-select.js.org/plugins/remove-button/
				remove_button:{
					title:'Remove this item',
				}
			},
		});

	}

	/**
	 * Handles whenever the value changes for the tom select at all
	 * @param value The value of the field, as it exists after being changed
	 */
	changeHandler(value) {

		// Single select, blur after change
		if(!this.element.multiple)
		{
			let text_field = this.element.parentElement.querySelector('input[type="select-one"]');
			if(text_field)
			{
				text_field.blur();
			}
		}
		else // multiple
		{
			// empty the field, refresh the options
			this.input_field.value = '';
			this.element.tom_select.refreshOptions();

		}
	}



	/**
	 * Makes an AJAX call to the server to acquire the results and update the view
	 */
	updateDynamicSearchResults(query, callback) {

		// Create the form data to send
		let form_data = new FormData();

		form_data.append('current_value', this.options.current_value);

		// Handle abort controller
		if(this.fetch_abort_controller !== null)
		{
			this.fetch_abort_controller.abort();
		}
		this.fetch_abort_controller = new AbortController();


		form_data.append('search', query);
		form_data.append('dynamic_call_class_name', this.options.dynamic_call_class_name);
		form_data.append('dynamic_call_model_id', this.options.dynamic_call_model_id);
		form_data.append('dynamic_call_method', this.options.dynamic_call_method);
		form_data.append('response_model_class_name', this.options.response_model_class_name);
		form_data.append('response_model_class_title_method', this.options.response_model_class_title_method);

		// Generate the URL
		let url = "/admin/TCore/Forms/TCv_FormItem_Select/ajax_filter_select_list.php";

		// Send Request
		fetch(url, {
			method : 'POST',
			body : form_data, // pass the form data
			signal : this.fetch_abort_controller.signal, // pass the abort controller signal
		}).then(response => response.json())
			.then(response =>
			{
				// Run the TomSelect callback, which handles the updates
				callback(response);

			})
			.catch((error) =>
			{
				console.log(error);
			})
			.finally(() =>
			{
				this.fetch_abort_controller = null; // disable the abort controller
			});



	}
}