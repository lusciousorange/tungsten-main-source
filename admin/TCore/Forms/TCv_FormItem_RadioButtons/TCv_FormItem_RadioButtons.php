<?php

class TCv_FormItem_RadioButtons extends TCv_FormItem
{
	protected array $options = []; // The list of options to be shown for the radio field
	protected array $option_attributes = [];
	protected array $options_disabled  = [];
	
	protected string $checked_icon_code = 'fa-circle';
	
	
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		
		$this->addClassCSSFile('TCv_FormItem_RadioButtons');
		
	}
	
	/**
	 * Sets the icon code use when indicating which item is checked.
	 * @param string $icon_code
	 * @return void
	 */
	public function setCheckedIconCode(string $icon_code) : void
	{
		$this->checked_icon_code = $icon_code;
	}
	
	
	
	public function optionView($value, $title)
	{
		$option_id = htmlspecialchars($this->id.'_'.$value);
		
		$container = new TCv_View($option_id.'_box');
		$container->setTag('span');
		$container->addClass('radio_button_option_box');
		
		// Attributes
		foreach($this->option_attributes[$value] as $attribute_name => $attribute_value)
		{
			
			$container->setAttribute($attribute_name, $attribute_value);
		}
		

		$radio = new TCv_View($option_id);
		$radio->setTag('input');
		$radio->setAttribute('type', 'radio');
		$radio->setAttribute('name', htmlspecialchars($this->fieldName()));
		$radio->setAttribute('value', htmlspecialchars($value));
		if(htmlspecialchars($this->currentValue()) == $value)
		{
			$radio->setAttribute('checked', 'checked');
		}
		if(isset($this->options_disabled[$value]))
		{
			$radio->setAttribute('disabled', 'disabled');
			
		}
		$container->attachView($radio);
		

		$label = new TCv_View();
		$label->setTag('label');
		$label->setAttribute('for', $option_id);
		$label->setAttribute('role','button'); // labels are buttons for radios
		$label->addClass('TCv_FormItem_RadioButtons_option_title');

		$styling_target = new TCv_View();
		$styling_target->setTag('span');
		$styling_target->addClass('button_restyle');
			$icon = new TCv_View();
			$icon->addClass('checkmark');
			$icon->addClass('fas '.$this->checked_icon_code);
			$styling_target->attachView($icon);
		$label->attachView($styling_target);

		$styling_target = new TCv_View();
		$styling_target->setTag('span');
		$styling_target->addClass('button_text');
		$styling_target->addText($title);
		$label->attachView($styling_target);

		$container->attachView($label);


		return $container;
	}
	
	
	/**
	 * @return array
	 */
	public function options() : array
	{
		return $this->options;
	}
	
	/**
	 * @param $value
	 * @param $title
	 * @return void
	 */
	public function addOption($value, $title) : void
	{
		if($title instanceof TCv_View)
		{
			$this->attachCSSFromView($title);
			$title = $title->html();
		}
		
		if( is_null($value) )
		{
			$this->setEmptyValuesAsNull();
			$value = '';
		}
		
		
		$this->options[$value] = $title;
		$this->option_attributes[$value] = array();
	
		// This was attaching before current values were ever set. Cart/Horse
		//$this->attachView($this->optionView($value, $title));
	}
	
	/**
	 * Disables a particular option
	 * @param $value
	 * @return void
	 */
	public function disableOption($value) : void
	{
		$this->options_disabled[$value] = true;
	}
	
	/**
	 * Adds an array of options
	 * @param $options
	 * @return void
	 */
	public function addOptions(array $options) : void
	{
		foreach($options as $value => $title)
		{
			$this->addOption($value, $title);
		}
	}
	
	/**
	 * Adds an attribute to a specific option
	 * @param $option_value
	 * @param $name
	 * @param $value
	 * @return void
	 */
	public function addOptionAttribute($option_value, $name, $value)
	{
		$this->option_attributes[$option_value][$name] = $value;
	}
	
	
	public function html()
	{
		$fieldset = new TCv_View($this->id());
		$fieldset->setTag('fieldset');
		$legend = new TCv_View();
		$legend->setTag('legend');
		$legend->addClass('sr-only');
		$legend->addText($this->title());
		$fieldset->attachView($legend);
		foreach($this->options as $value => $title)
		{
			$fieldset->attachView($this->optionView($value, $title));
		}
		
		// Add the counts to the fieldset
		$num_options = count($this->options);
		$fieldset->addDataValue('num-options', $num_options);
		if($num_options < 8)
		{
			$fieldset->addClass('range_less_than_8');
		}
		elseif($num_options < 16)
		{
			$fieldset->addClass('range_8_to_16');
		}
		else
		{
			$fieldset->addClass('range_16_plus');
		}
		
		$this->attachView($fieldset);
		
		return parent::html();
	}
	

	
	
	
}
?>