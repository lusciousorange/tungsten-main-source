<?php

/**
 * @deprecated Just use regular filtering on the TCv_FormItem_Select. All the functionality would exist within that
 */
class TCv_FormItem_SearchSelect extends TCv_FormItem_Select //TCv_FormItem
{
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		
		$this->addClass($this->id());
		$this->addOption('', 'Search');
		
		
	}
	
	
	
	
	
}
?>