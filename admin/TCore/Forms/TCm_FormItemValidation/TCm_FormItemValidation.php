<?php

/**
 * The utility class used to return the result of a form validation. Each form validation item is the result from a single validation
 */
class TCm_FormItemValidation extends TCm_Model
{
	protected $is_valid = true; // [bool] = Indicates if the validation succeeded
	protected $is_skipped = false; // [bool] = Indicates if the validation was skipped for whatever reason
	protected $form_item_id = false; 
	
	protected $message = ''; // [string] = The message that resulted from the validation
	protected $title = ''; // [string] = The title of the validation being performed
	protected $details = ''; // [string] = The details of this validation
	
	protected $log_in_console = true;
	
	/**
	 * @param string $form_item_id The id of the form item that is being validated
	 * @param string $title The title of the validation being performed
	 */
	public function __construct($form_item_id, $title)
	{
		$this->form_item_id = $form_item_id;
		
		parent::__construct($form_item_id.'_'.strtolower(str_ireplace(array(' ',':'),'_',$title)));
		$this->title = $title;
	}
	
	
	
	/**
	 * Returns the if this validation item is valid
	 * @return bool
	 */
	public function isValid()
	{
		return $this->is_valid;
	}
	
	/**
	 * Sets the is_valid value for this validation
	 *
	 * @param bool $is_valid
	 */
	public function setIsValid($is_valid)
	{
		$this->is_valid = $is_valid;
	}
	
	/**
	 * Fails the validation
	 */
	public function fail()
	{
		$this->is_valid = false;
	}
	
	/**
	 * Fails the validation with a pre-defined message
	 * @param string $message The message to be shown for this validation failure
	 */
	public function failWithMessage($message)
	{
		$this->message = htmlspecialchars($message);
		$this->fail();
	}
	
	/**
	 * Fails the validation with a pre-defined message for a given field
	 * @param string $title The title of the field
	 * @param string $message The message to be shown for this validation failure
	 */
	public function failFieldWithMessage($title, $message)
	{
		$this->message = '<span class="field_title">'.htmlspecialchars($title).'</span> : '.htmlspecialchars($message);
		$this->fail();
	}
	
	/**
	 * A wrapper function for the PDO execute function
	 *
	 * @param PDOStatement $statement
	 * @param false $values
	 * @param array $bind_param_values
	 * @return PDOStatement
	 */
	public function DB_Exec(PDOStatement $statement, $values = false, $bind_param_values = array())
	{
		$statement = parent::DB_Exec($statement, $values, $bind_param_values);
		if($statement->errorCode() != '00000')
		{
			$this->failWithMessage('There was an error with a database query. This is a programming error, please contact a system administrator.');

		}
		
		return $statement;
		
	}
	
	/**
	 * Returns if this validation item is_skipped
	 *
	 * @return bool
	 */
	public function isSkipped()
	{
		return $this->is_skipped;
	}
	
	/**
	 * Sets the is_skipped value for this validation
	 *
	 * @param bool $is_skipped
	 */
	public function setIsSkipped($is_skipped = true)
	{
		$this->is_skipped = $is_skipped;
	}
	
	/**
	 * Returns ID of the form item that is being edited
	 *
	 * @return bool|string
	 */
	public function formItemID()
	{
		return $this->form_item_id;
	}
	
	/**
	 * Returns the title of this validation
	 *
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Returns the message of this validation
	 *
	 * @return string
	 */
	public function message()
	{
		return $this->message;
	}
	
	/**
	 * Returns the details of this validation
	 *
	 * @return string
	 */
	public function details()
	{
		return $this->details;
	}
	
	
	/**
	 * Method to provide additional details for this validation
	 *
	 * @param string $details
	 */
	public function setDetails($details)
	{
		$this->details = $details;
	}
	
	/**
	 * Returns if this validation should be logged in the console
	 * @return bool
	 */
	public function isConsoleLogging()
	{
		return $this->log_in_console;
	}
	
	/**
	 * Disables the console logging for this particular validation
	 */
	public function disableConsoleLogging()
	{
		$this->log_in_console = false;
	}
	
}
