<?php
/**
 * Form field for a password.
 */
class TCv_FormItem_Password extends TCv_FormItem_TextField
{
	
	/**
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		$this->field_type = 'password';
		
		// ALways turn off the saving and autocomplete 
		$this->setSavedValue(false);
		$this->disableAutoComplete();
			
	}
	
	/**
	 * Returns the value that will be entered into the database. For any password field, the value will be a sha256 hash of the provided password
	 * @return string
	 */
	public function databaseValue()
	{
		$value = $this->submittedValue();
		if($this->emptyValuesAsNull() && $value == '')
		{
			return NULL;
		}
		$text = new TCu_Text($this->submittedValue());
		return $text->hash();
	}
	
	
	/**
	 * Sets the form item to perform a verification on the text submitted to ensure it meets requirements for a valid password
	 * @return void
	 */
	public function setValidatePasswordFormat()
	{
		$this->addProcessMethodCall('validatePasswordFormat',
		                            TCv_FormItem::PROCESSING_IS_CLASS_STATIC_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES,
		                            $this->isRequired());
	}

	/**
	 * Indicates that this form item should check the history of the user to see if they have previously used this email.
	 * This only works if the user history is enabled and only if they have the feature turned on.
	 * @return void
	 */
	public function setValidateUniqueHistory() : void
	{
		if(TC_getConfig('use_track_model_history') && TC_getConfig('password_prevent_reuse'))
		{

			$this->addProcessMethodCall(
				'validatePasswordReuse',
				TCv_FormItem::PROCESSING_MODEL_CLASS_INSTANCE_METHOD,
				TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
				TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES,
				TCv_FormItem::PROCESSING_PARAM_SUBMITTED_VALUE);

		}
	}

	/**
	 * Sets the form item to perform a verification on the model instance for the form
	 * @return void
	 */
	public function setValidateUserPassword()
	{
		
		$this->addProcessMethodCall(
			'validatePassword', 
			TCv_FormItem::PROCESSING_MODEL_CLASS_INSTANCE_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
			TCv_FormItem::PROCESSING_PARAM_SUBMITTED_VALUE);
	
	}

	/**
	 * Sets the password field to check that the value matched the value of the provided field
	 *
	 * @param string $field_id
	 * @return void
	 */
	public function setValidateMatchField($field_id)
	{
		$this->addProcessMethodCall(
			'validateMatchingValues', 
			TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
			TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES,
			$field_id); // We pass in the OTHER ID to be compared against this one
	
	}

	
	
}
?>