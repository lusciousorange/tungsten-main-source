<?php
/**
 * Class TCm_FormTracker
 * A class to track the values returning from a processed form. This class is saved to a session variable and passed back
 * to the form to handle all the data management associated with the form.
 */
class TCm_FormTracker
{
	// form items
	protected array $form_items = array(); // [array] = The array of form items that are added to this form. The indices are the respective IDs for the form items.
	protected bool $has_saved_values = false;
	protected string $tracker_name = 'tracker';
	protected $primary_value = false; // [string] = The id of the primary item being edited. This is normally the id of the item or false
	protected $primary_table = false; // [string] = THe name of the primary table for the form
	protected $primary_key = false; // [string] = The name of the primary key for the form
	protected array $invalid_fields = [];
	protected bool $invalid = false;

	protected $verification_code = false;
	
	// MODEL BASED VALUES
	protected $model_name = false;
	
	/**
	 * TCm_FormTracker constructor.
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		$this->tracker_name = $id;
		
	}
	
	/**
	 * Returns the name for this tracker. The name is saved in the ID value
	 * @return array|int|string
	 */
	public function trackerName()
	{
		return $this->tracker_name;
	}
	
	/**
	 * @return string|bool
	 */
	public function modelName()
	{
		return $this->model_name;
	}
	
	/**
	 * SEts the model name
	 * @param string $model_name
	 */
	public function setModelName(string $model_name)
	{
		$this->model_name = $model_name;
	}

	//////////////////////////////////////////////////////
	//
	// FORM VIEW SAVING
	//
	//////////////////////////////////////////////////////

	/**
	 * Tracks a form item to be processed. This is done when a form is laid out on the screen. The system will save the
	 * relevant values about the form item for processing.
	 * @param TCv_FormItem $form_item
	 */
	public function trackFormItem(TCv_FormItem &$form_item)
	{
		if(!isset($this->form_items[$form_item->id()]))
		{
			foreach($form_item->formFieldIDs() as $field_id)
			{
				$properties = array(
					'id' => $field_id,
					'view_class_name' => get_class($form_item),
					'title' => $form_item->title(),
					'process_calls' => $form_item->processMethodCalls(),
					'submitted_value' => $form_item->savedValue(),
					'processor_class_properties' => $form_item->processor_class_properties
					);

				// Grab the properties defined in the form item. These may vary for different form items, but some
				// basic properties such as required, use_multiple, etc are always passed
				foreach($form_item->processor_class_properties as $class_property)
				{
					$properties[$class_property] = $form_item->$class_property;
				}

				$this->form_items[$field_id] = $properties;
				
			}


		}
		// already set, update the process calls. They may change per loading.
		else
		{
			$this->form_items[$form_item->id()]['process_calls'] = $form_item->processMethodCalls();
		}
		
		// Handle Private Hidden Values
		if($form_item instanceof TCv_FormItem_Hidden && $form_item->isPrivate())
		{
			// If a hidden private value is used, we always save the "Default" value which avoids any possible
			// saving issues from the form processor.
			// @see TCm_FormTracker::saveSubmittedValue()
			$this->form_items[$form_item->id()]['private_value'] = $form_item->defaultValue();
		}
		
		
		// recursive call to track any other form items.
		foreach($form_item->additionalFormItemsToTrack() as $additional_form_item)
		{
			$this->trackFormItem($additional_form_item);
		}
	}

	/**
	 * Removes the form item with the ID
	 * @param string $id
	 */
	public function removeFormItemWithID(string $id)
	{
		unset($this->form_items[$id]);
	}

	/**
	 * Returns the properties for the form
	 * @param string $id
	 * @return mixed
	 */
	public function propertiesForFormItemID(string $id)
	{
		return $this->form_items[$id];
	}
	
	/**
	 * Returns the list of form items associated with the form
	 * @return TCv_FormItem[]
	 */
	public function formItems()
	{
		return $this->form_items;
	}
	
	/**
	 * Adds an array of form items to the tracker
	 * @param TCv_FormItem[] $form_items
	 */
	public function addFormItemsAsArray(array $form_items)
	{
		foreach($form_items as $id => $form_item)
		{
			if($form_item instanceof TCv_FormItem)
			{
				$this->trackFormItem($form_item);
			}
		}
	}
	
	/**
	 * Removes all existing form items
	 */
	public function clearFormItems()
	{
		$this->form_items = array();
	}
	
	//////////////////////////////////////////////////////
	//
	// VERIFICATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Generates a verification code
	 * @return string
	 */
	public function generateVerificationCode()
	{
		$verification_code = new TCu_Text(gettimeofday(true).''); // generate the password
		$verification_code = substr($verification_code->hash(),0,40);
		
		$this->verification_code = $verification_code;
		return $this->verification_code;
	}
		
	/**
	 * Returns the verification code
	 * @return string
	 */
	public function verificationCode()
	{
		return $this->verification_code;
	}
	
	//////////////////////////////////////////////////////
	//
	// PROPERTIES
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the primary model for this form tracker
	 * @param TCm_Model $model
	 */
	public function setPrimaryModel(TCm_Model $model)
	{
		$this->setPrimaryTableAndKey($model::tableName(), $model::$table_id_column);// needs to be saved for other form calls
	
	}

	//////////////////////////////////////////////////////
	//
	// SUBMITTED VALUES
	//
	//////////////////////////////////////////////////////

	/**
	 * Saves the submitted value for a provided ID of a form item
	 * @param string $id
	 * @param mixed $value
	 */
	public function saveSubmittedValue(string $id,  $value)
	{
		// Check for a private value which overrides whatever value we have and uses it instead.
		// This is part of TCv_FormItem_Hidden that allows for values to be passed without exposing them in HTML
		if(isset($this->form_items[$id]['private_value']))
		{
			$value = $this->form_items[$id]['private_value'];
		}
		$this->form_items[$id]['submitted_value'] = $value;
		$_POST[$id] = $value;
	}
	
	/**
	 * Returns the submitted value for a provided ID
	 * @param string $id
	 * @return string|null
	 */
	public function submittedValueForFormItemID($id)
	{
		if(isset($this->form_items[$id]['submitted_value']))
		{
			return $this->form_items[$id]['submitted_value'];
		}
		
		return NULL;
	}

	/**
	 * Returns if this form tracker has saved values from the form
	 * @return bool
	 */
	public function hasSavedValues()
	{
		return $this->has_saved_values;
	}

	//////////////////////////////////////////////////////
	//
	// PRIMARY VALUES
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the primary table and key
	 *
	 * @param string $table
	 * @param string $key
	 */
	public function setPrimaryTableAndKey(string $table, string $key)
	{
		$this->primary_table = $table;
		$this->primary_key = $key;
	}
	
	/**
	 * Returns the primary table
	 * @return string
	 */
	public function primaryTable()
	{
		return $this->primary_table;
	}
	
	/**
	 * Returns the primary key
	 * @return string
	 */
	public function primaryKey()
	{
		return $this->primary_key;
	}
	
	/**
	 * Sets the primary value
	 * @param string|bool $value
	 */
	public function setPrimaryValue($value)
	{
		$this->primary_value = $value;
	}
	
	/**
	 * Returns the primary value
	 * @return bool
	 */
	public function primaryValue()
	{
		return $this->primary_value;
	}

	//////////////////////////////////////////////////////
	//
	// FIELD FLAGGING
	//
	//////////////////////////////////////////////////////

	/**
	 * Marks a field as having an error
	 * @param string $id
	 */
	public function setInvalidField($id)
	{
		@$this->invalid_fields[$id] = true;
		$this->invalid = true;
	}
	
	/**
	 * Indicates if a particular field is invalid
	 * @param null|string $id
	 * @return bool
	 */
	public function isInvalidForID(?string $id) : bool
	{
		return isset($this->invalid_fields[$id]);
	}
	
	/**
	 * Removes all invalid markers from all fields
	 */
	public function resetInvalidFields()
	{
		$this->invalid_fields = array();
		$this->invalid = false;
		$this->has_saved_values = true;
	}

	//////////////////////////////////////////////////////
	//
	// VALIDATIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Takes a TCm_FormItemValidation and uses it to apply the appropriate form tracking values
	 * @param TCm_FormItemValidation $validation
	 */
	public function addValidation(TCm_FormItemValidation $validation)
	{
		// Add any debuggers here
		if(!$validation->isValid())
		{
			$this->setInvalidField($validation->formItemID());
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// SAVING AND ACCESSING
	//
	//////////////////////////////////////////////////////

	/**
	 * A method to save this formTracker to a session variable so that it can be used for form processing
	 */
	public function saveForProcessing()
	{
		$_SESSION['TCm_FormTracker'][$this->trackerName()] = $this;
	}

	/**
	 * A form tracker will delete itself from the session memory
	 * @param bool|string $verb
	 */
	public function delete($verb = false)
	{
		unset($_SESSION['TCm_FormTracker'][$this->trackerName()]);
	}


	//////////////////////////////////////////////////////
	//
	// CLASS METHODS
	//
	//////////////////////////////////////////////////////

	/**
	 * A class method used to configure the form tracker. This function will return an instance of TCm_FormTracker.
	 * If an existing instance was saved in a session variable, then it will be returned. Otherwise a new form
	 * tracker will be created and returned.
	 * @param null|string $override_tracker_name The name of the tracker to be used instead of the default.
	 * @param bool $clear Indicates if the old form tracker should be discared and start fresh
	 * @return TCm_FormTracker
	 */
	public static function configure(?string $override_tracker_name = null, bool $clear = false) : TCm_FormTracker
	{
		// check for a tracker name override.
		if(isset($override_tracker_name) && $override_tracker_name !== false)
		{
			$tracker_name = $override_tracker_name;
		}
		else // no override
		{	 
			$tracker_name = '';
			$url_split = explode("/",$_SERVER['PHP_SELF'].$_SERVER['QUERY_STRING']);
			array_shift($url_split);
			$tracker_name = implode('_',$url_split);
			$tracker_name = str_ireplace('&', '_', $tracker_name);
			
		}
		
		// Handle clearing old tracker values
		if($clear)
		{
			unset($_SESSION['TCm_FormTracker'][$tracker_name]);
		}
		
		if(isset($_SESSION['TCm_FormTracker'][$tracker_name])) // session variable is not set
		{	
			$form_tracker = $_SESSION['TCm_FormTracker'][$tracker_name]; // save it to the variable
		}
		else  // session var exists and still on the same page
		{	
			$form_tracker = new TCm_FormTracker($tracker_name); // create the class
		}
		return $form_tracker;
	}
	
	
}
?>