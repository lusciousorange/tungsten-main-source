<?php

/**
 * A form component to add a date and time selector
 */
class TCv_FormItem_SelectDateTime extends TCv_FormItem
{
	protected $selected_full_date = false;
	protected $use_blank_starting_values = false;
	
	protected $use_exact_month_array = false;
	protected $use_exact_day_array = false;
	protected $use_exact_hour_array = false;
	protected $use_exact_minute_array = false;
	protected $use_exact_second_array = false;
	
	protected $use_date_calendar = false;
	
	protected $year_values = array(); // The list of values to be shown for the year select field
	protected $month_values = array(); // The list of values to be shown for the year select field
	protected $month_values_full = array(
		1  => 'January',
		2  => 'February',
		3  => 'March',
		4  => 'April',
		5  => 'May',
		6  => 'June',
		7  => 'July',
		8  => 'August',
		9  => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December'
	);
	
	public $view_order = 'YMDhm'; // saved processor class
	protected $view_order_functions = array(
		'Y' => 'yearView',
		'M' => 'monthView',
		'D' => 'dayView',
		'h' => 'hourView',
		'm' => 'minuteView',
		't' => 'hourAsTimeView',
		's' => 'secondView',
		//':' => 'timeSeparatorView',
		' ' => 'spacerView'
	);
	protected $view_order_class = 'view_order_';
	
	protected $hours_values = array(); // [array] = The hours that are shown 
	protected $minutes_values = array(); // [array] = The minutes that are shown 
	protected $seconds_values = array(); // [array] = The seconds that are shown
	
	/**
	 * TCv_FormItem_SelectDateTime constructor.
	 * @param $id
	 * @param $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		$this->setYearValues(date('Y')+1, date('Y') - 20); // save initial values
		$this->setHoursValues(range(0,23));
		$this->setMinutesValues(range(0,59));
		$this->setSecondsValues(range(0,59));
		$this->setFullMonthNames();
		
		$this->addSavedProcessorClassProperty('view_order');
		
		$this->addClassCSSFile('TCv_FormItem_SelectDateTime');
		
		
	}
	
	/**
	 * Configures the "selected full date" which ensures we always have a value that is formatted as YYYY-MM-DD hh:mm:ss
	 * to use in our processing of the form view.
	 */
	public function configureSelectedFullDate()
	{
		$this->selected_full_date = htmlspecialchars(trim($this->currentValue()));
		
		// Year and month only
		if (preg_match("/^[0-9]{4}-[0-1][0-9]$/",$this->selected_full_date))
		{
			$date = $this->selected_full_date.'-00';
			$time = '00:00:00';
		}
		elseif (preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9](.*)/",$this->selected_full_date))
		{
			$date = substr($this->selected_full_date, 0, 10);
			$time = substr($this->selected_full_date,11);
		}
		else
		{
			$date = '0000-00-00';
			$time = $this->selected_full_date;
		}
		
		// Clear away any spaces
		$time = trim($time);
		
		// Fill in the time correctly
		// Check for only two values
		if($time == '')
		{
			$time = '00:00:00';
		}
		elseif (preg_match("/^[0-2][0-9]:[0-5][0-9]$/",$time))
		{
			$time .= ':00';
		}
		elseif (preg_match("/^[0-2][0-9]$/",$time))
		{
			$time .= ':00:00';
		}
		
		$this->selected_full_date = $date.' '.$time;
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	//  HTML AND VIEW SETTINGS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Sets the view order for this display. It is set as a single string. Year (Y), Month (M), Day (D), hours (h),
	 * minutes (m), seconds(s) and you can also include a colon (:) to include a separator between the times.
	 * @param string $view_order
	 */
	public function setViewOrder(string $view_order)
	{
		$this->view_order = $view_order;
	}
	
	/**
	 * Indicates that blank values should be included
	 */
	public function includeBlankStartingValues()
	{
		$this->use_blank_starting_values = true;
	}
	
	
	
	/**
	 * @return string
	 */
	public function html()
	{
		// Date calendars actually just use a text input set to date and lets the browser do all the heavy work
		if($this->use_date_calendar)
		{
			$this->renderDateCalendar();
			return parent::html();
		}
		
		if(!$this->use_blank_starting_values)
		{
			
			$this->addProcessMethodCall('validateYMDValue', TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD, TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			                            false, $this->view_order);
		}
		
		$this->configureSelectedFullDate();
		//$selected_full_date = htmlspecialchars(trim($this->currentValue()));
		$options_showing_class = 'showing_';
		$container = new TCv_View();
		$view_options = str_split($this->view_order);
		$num_showing = 0;
		
		$show_date_box = false;
		$date_box = new TCv_View();
		$date_box->addClass('date_select_YMD_box');
		
		$show_time_box = false;
		$time_box = new TCv_View();
		$time_box->addClass('date_select_hms_box');
		
		// Loop through each view code, generating the view for the particular field
		foreach($view_options as $view_code)
		{
			$function_name = $this->view_order_functions[$view_code];
			if($function_name != '' && isset($function_name))
			{
				// Generate the view
				$view_to_attach = $this->$function_name();
				if($view_to_attach->tag() == 'select')
				{
					$wrapper = new TCv_View();
					$wrapper->setTag('span');
					$wrapper->addClass('date_select_wrapper');
					$wrapper->addClass('view_type_' . $view_code);
					$wrapper->attachView($view_to_attach);
					
					// Add the icon for the caret for the drop-down
					$icon_view = new TSv_FontAwesomeSymbol('select_caret', 'fas fa-caret-down');
					$wrapper->attachView($icon_view);
					
					
					$view_to_attach = $wrapper;
				}
				
				// Handle YEAR / MONTH / DAY
				if($view_code == 'Y' || $view_code == 'M' || $view_code == 'D')
				{
					$show_date_box = true;
					$date_box->attachView($view_to_attach);
					$options_showing_class .= $view_code;
					$num_showing++;
				}
				
				// Handle HOUR / MINUTE / SECOND
				elseif($view_code == 'h' || $view_code == 'm' || $view_code = 's')
				{
					$show_time_box = true;
					$time_box->attachView($view_to_attach);
					
					$options_showing_class .= $view_code;
					$num_showing++;
				}
				
			}
		}
		
		if($show_date_box)
		{
			$icon = new TCv_View();
			$icon->setTag('i');
			$icon->addClass('date_icon');
			$icon->addClass('fa-calendar-alt');
			$date_box->attachView($icon);
			
			$container->attachView($date_box);
		}
		if($show_time_box)
		{
			$icon = new TCv_View();
			$icon->setTag('i');
			$icon->addClass('time_icon');
			$icon->addClass('fa-clock');
			$time_box->attachView($icon);
			$container->attachView($time_box);
		}
		
		$container->addClass($options_showing_class);
		$container->addClass('num_showing_'.$num_showing);
		$this->attachView($container);
		
		
		return parent::html();
	}
	
	//////////////////////////////////////////////////////
	//
	// DATE VIEWS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the view for the year.
	 * @return TCv_View
	 */
	public function yearView()
	{
		$selected_value = substr($this->selected_full_date, 0,4);
		return $this->selectBoxView('year', $this->year_values, $selected_value);
	}
	
	/**
	 * Returns the view for the month.
	 * @return TCv_View
	 */
	public function monthView()
	{
		$selected_value = substr($this->selected_full_date, 5,2);
		return $this->selectBoxView('month', $this->month_values, $selected_value);
	}
	
	/**
	 * Returns the view for the day.
	 * @return TCv_View
	 */
	public function dayView()
	{
		$selected_value = substr($this->selected_full_date, 8,2);
		$padded_array = range(1,31);//$this->padDigitArray(range(1,31));
		$values = array_combine($padded_array, $padded_array);
		return $this->selectBoxView('day', $values, $selected_value);
		
	}
	
	/**
	 * Returns the view for the hour.
	 * @return TCv_View
	 */
	public function hourView()
	{
		if($this->use_exact_hour_array)
		{
			$values = $this->hours_values;
		}
		else
		{
			$padded_array = $this->padDigitArray($this->hours_values);
			$values = array_combine($padded_array, $padded_array);
			
		}
		
		$selected_value = substr($this->selected_full_date, 11,2);
		return $this->selectBoxView('hour', $values, $selected_value);
		
		
	}
	
	/**
	 * Returns the view for the time which is minutes and seconds together. This is a customized view that requires
	 * the values for the hours be set using the HH:mm format.
	 * @return TCv_View
	 */
	public function hourAsTimeView()
	{
		if($this->use_exact_hour_array)
		{
			$values = $this->hours_values;
		}
		else
		{
			TC_triggerError('Cannot use time view unless times manually set');
			
		}
		
		// Grab minutes and seconds
		$selected_value = substr($this->selected_full_date, 11,5);
		return $this->selectBoxView('hour', $values, $selected_value);
		
		
	}
	
	/**
	 * Returns the view for the minute.
	 * @return TCv_View
	 */
	public function minuteView()
	{
		if($this->use_exact_minute_array)
		{
			$values = $this->minutes_values;
		}
		else
		{
			$padded_array = $this->padDigitArray($this->minutes_values);
			$values = array_combine($padded_array, $padded_array);
			
		}
		
		$selected_value = substr($this->selected_full_date, 14,2);
		return $this->selectBoxView('minute', $values, $selected_value);
		
		
	}
	
	/**
	 * Returns the view for the second.
	 *
	 * @return TCv_View
	 */
	public function secondView()
	{
		if($this->use_exact_second_array)
		{
			$values = $this->seconds_values;
		}
		else
		{
			$padded_array = $this->padDigitArray($this->seconds_values);
			$values = array_combine($padded_array, $padded_array);
		}
		
		$selected_value = substr($this->selected_full_date, 17,2);
		return $this->selectBoxView('second', $values, $selected_value);
	}
	
	/**
	 * @return string|TCv_View
	 */
	public function timeSeparatorView()
	{
		return '';
//		$span = new TCv_View();
//		$span->addClass('time_separator');
//		$span->setTag('span');
//		$span->addText(':');
//		return $span;
	}
	
	/**
	 * @return string|TCv_View
	 */
	public function spacerView()
	{
		return '';
//		$span = new TCv_View();
//		$span->addClass('spacer');
//		$span->setTag('span');
//
//		return $span;
	}
	
	
	/**
	 * Returns the view for the a single select field.
	 * @param string $sub_id The sub ID for this select box. The main id will be prepented to this sub-id
	 * @param array $values The list of values for this select box
	 * @param string $selected_value The value that is currently selected
	 * @return TCv_View
	 */
	protected function selectBoxView($sub_id, $values, $selected_value = '')
	{
		if($sub_id == 'cal')
		{
			$select = new TCv_FormItem_TextField($this->id.'_'.$sub_id, $sub_id);
			//	$select->setAttribute('readonly', 'readonly');
			$select->setPlaceholderText(TC_localize('choose date', 'Choose date'));
			if($this->isRequired())
			{
				$select->setIsRequired();
			}
			
			// Disable auto complete
			$select->disableAutoComplete();
		}
		else
		{
			$select = new TCv_FormItem_Select($this->id.'_'.$sub_id, $sub_id);
		}
		
		$select->setUsesContainer(false);// avoid wrapping each in a div
		if($sub_id == 'year' || $sub_id == 'month' || $sub_id == 'day')
		{
			$selected_value = intval( $selected_value );
		}
		$select->setEditorValue($selected_value); // plus 0 to convert to int
		
		if($select instanceof TCv_FormItem_Select)
		{
			if($this->use_blank_starting_values)
			{
				$select->addOption('',ucwords($sub_id));
			}
			$select->addOptions($values);
			
		}
		
		// Add the form element classes
		foreach($this->form_element_classes as $class_name)
		{
			$select->addFormElementCSSClass($class_name);
		}
		
		
		// Wrap field in container and add the caret
		$container = new TCv_View();
		$container->addClass('select_container');
		
		$select_view = $select->fieldView();
		$select_view->addClass('SelectDateTime_SelectBox');
		$select_view->addClass('SelectDateTime_'.$sub_id);
		$select_view->setAttribute('aria-label', $sub_id);

		$container->attachView($select_view);
		
		// Add the icon for the caret for the drop-down
		if($select instanceof TCv_FormItem_Select)
		{
			$container->attachView(TCv_FormItem_Select::fontAwesomeArrowSymbol());
		}
		
		return $container;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// CUSTOM VALUE SETTING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the values for the year select box using a range of integers. The first and last values should be whole
	 * numbers. If the first number is greater than the last number, the count will descend
	 *
	 * @param int $first
	 * @param int $last
	 */
	public function setYearValues($first, $last)
	{
		$this->year_values = array();
		if($last > $first) // ascending
		{
			for($i = $first; $i <= $last; $i++)
			{
				$this->year_values[$i] = $i;
			}
		}
		else // descending
		{
			for($i = $first; $i >= $last; $i--)
			{
				$this->year_values[$i] = $i;
			}
		}
	}
	
	/**
	 * Sets the values for the months.
	 * @param array $values Either an array of integers or an associative array that is passed through with the
	 * second parameter set.
	 * @param bool $use_exact_array (Optional) Default. Indicate if the exact values should be passed into the field.
	 * Unaltered
	 */
	public function setMonthValues($values, $use_exact_array = false)
	{
		$this->month_values = $values;
		$this->use_exact_month_array = $use_exact_array;
		
	}
	
	
	/**
	 * Sets the values for the hour select box.
	 * @param array $values Either an array of integers or an associative array that is passed through with the
	 * second parameter set.
	 * @param bool $use_exact_array (Optional) Default. Indicate if the exact values should be passed into the field.
	 * Unaltered
	 */
	public function setHoursValues($values, $use_exact_array = false)
	{
		$this->hours_values = $values;
		$this->use_exact_hour_array = $use_exact_array;
	}
	
	/**
	 * Sets the values for the hour select to show AM and PM
	 */
	public function setHoursAs_AM_PM()
	{
		$new_hours = array();
		foreach($this->hours_values as $hour => $text)
		{
			if($hour < 12)
			{
				if($hour == 0)
				{
					$new_hours[$hour] = '12 am';
				}
				else
				{
					$hour_padded = str_pad($hour, 2, '0', STR_PAD_LEFT);
					$new_hours[$hour_padded] = $hour.' am';
				}
				
			}
			else // PM
			{
				if($hour > 12)
				{
					$new_hours[$hour] = ($text-12).' pm';
				}
				else
				{
					$new_hours[$hour] = $text.' pm';
				}
			}
		}
		
		$this->setHoursValues($new_hours, true);
	}
	
	/**
	 * Sets the values for the hour select box.
	 * @param array $values Either an array of integers or an associative array that is passed through with the
	 * second parameter set.
	 * @param bool $use_exact_array (Optional) Default. Indicate if the exact values should be passed into the field.
	 * Unaltered
	 */
	public function setMinutesValues($values, $use_exact_array = false)
	{
		$this->minutes_values = $values;
		$this->use_exact_minute_array = $use_exact_array;
	}
	
	/**
	 * Sets the values for the hour select box.
	 * @param array $values Either an array of integers or an associative array that is passed through with the
	 * second parameter set.
	 * @param bool $use_exact_array (Optional) Default. Indicate if the exact values should be passed into the field.
	 * Unaltered
	 */
	public function setSecondsValues($values, $use_exact_array = false)
	{
		$this->seconds_values = $values;
		$this->use_exact_second_array = $use_exact_array;
	}
	
	/**
	 * Sets the month listing to show the full names of the months
	 */
	public function setFullMonthNames()
	{
		$this->month_values = $this->month_values_full;
	}
	
	/**
	 * Sets the month listing to show the abbreviated names of the months
	 */
	public function setAbbreviatedMonthNames()
	{
		$values = array();
		foreach($this->month_values as $index => $name)
		{
			$values[$index] = substr($name, 0, 3);
		}
		
		$this->month_values = $values;
	}
	
	/**
	 * Sets the month listing to show the digits of the months
	 */
	public function setDigitMonthNames()
	{
		$values = array();
		foreach($this->month_values as $index => $name)
		{
			$values[$index] = $index;
		}
		
		$this->month_values = $values;
	}
	
	/**
	 * Shortcut to set the default date to today.
	 */
	public function setDefaultDateToToday()
	{
		parent::setDefaultValue(date('Y-m-d H:i:s'));
	}
	
	/**
	 * Sets the default date
	 * @param $string
	 */
	public function setDefaultDate($string)
	{
		parent::setDefaultValue(date('Y-m-d H:i:00', strtotime($string)));
	}
	
	/**
	 * Sets the default value
	 * @param string $string
	 */
	public function setDefaultValue($string)
	{
		$this->setDefaultDate($string);
	}
	
	
	/**
	 * Extends the existing set editor function but doesn't bother if the values are all zeros. AVoids aggressive
	 * overriding with setting default values
	 * @param $value
	 */
	public function setEditorValue($value)
	{
		if($value != '0000-00-00 00:00:00')
		{
			parent::setEditorValue($value);
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// SPECIAL FUNCTIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Utility function to pad the select values to have 2 digits always
	 * @param $array
	 * @return mixed
	 */
	protected function padDigitArray($array)
	{
		foreach($array as $index => $digit)
		{
			$array[$index] = str_pad($digit, 2, '0', STR_PAD_LEFT);
		}
		return $array;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// OVERRIDE SUBMITTED VALUE FUNCTION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the value that was submitted to the form.
	 * @return array|string
	 */
	public function submittedValue()
	{
		// Identify if the value was previously saved to the post field
		if(isset($_POST[$this->id]))
		{
			return trim($_POST[$this->id]);
		}
		
		// We can't let bad values in. We either care about them or we don't. If we don't, then we put in the first available
		// number which is 1 for dates and 0 for times
		$year = '0001';
		$month = '01';
		$day = '01';
		$hour = '00';
		$minute = '00';
		$second = '00';
		
		// CHECK IF EACH VALUE IS PASSED IN THE POST
		if(isset($_POST[$this->id.'_year']))
		{
			$year = str_pad($_POST[$this->id.'_year'],4,'0',STR_PAD_LEFT);
		}
		
		if(isset($_POST[$this->id.'_month']))
		{
			$month = str_pad($_POST[$this->id.'_month'],2,'0',STR_PAD_LEFT);
		}
		
		if(isset($_POST[$this->id.'_day']))
		{
			$day = str_pad($_POST[$this->id.'_day'],2,'0',STR_PAD_LEFT);
		}
		
		if(isset($_POST[$this->id.'_hour']))
		{
			$hour = $_POST[$this->id.'_hour'];
			
			// Detect `t` format with hour having minutes in it
			// String is already H:m format as a string, so we need to explode it
			// So both values are saved correctly and then we still process them
			if(strpos($hour,':') >= 0)
			{
				list($hour, $minute) = explode(':', $hour);
				
				// Clean up the minute while we're here
				$minute = str_pad($minute,2,'0',STR_PAD_LEFT);
			}
			$hour = str_pad($hour,2,'0',STR_PAD_LEFT);
		}
		
		if(isset($_POST[$this->id.'_minute']))
		{
			$minute = str_pad($_POST[$this->id.'_minute'],2,'0',STR_PAD_LEFT);
		}
		
		if(isset($_POST[$this->id.'_second']))
		{
			$second = str_pad($_POST[$this->id.'_second'],2,'0',STR_PAD_LEFT);
		}
		
		// RETURN THE COMPLETED STRING
		return $year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.$second;
	}
	
	/**
	 * Sets the field to only show the time values. Equivalent to calling setViewOrder('YMD')
	 */
	public function setAsDateOnly()
	{
		$this->setViewOrder('YMD');
	}
	
	/**
	 * Sets the field to only show the time values. Equivalent to calling setViewOrder('hm')
	 */
	public function setAsTimeOnly()
	{
		$this->setViewOrder('hm');
		
	}
	
	/**
	 * @param string $sub_id
	 * @param array $values
	 * @param string $selected_value
	 * @return string
	 * @deprecated Use views rather than HTML
	 * @see TCv_FormItem_SelectDateTime:selectBoxView()
	 */
	protected function htmlForSelectBox($sub_id, $values, $selected_value = '')
	{
		return $this->selectBoxView($sub_id, $values, $selected_value)->html();
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowYears($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowYears() has no effect and is deprecated. ');
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowMonths($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowMonths() has no effect and is deprecated. ');
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowDays($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowDays() has no effect and is deprecated. ');
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowHours($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowHours() has no effect and is deprecated. ');
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowMinutes($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowSeconds() has no effect and is deprecated. ');
	}
	
	/**
	 * @param bool $show
	 * @deprecated Has no functionality.
	 * @see TCv_FormItem_SelectDateTime::setViewOrder()
	 */
	public function setShowSeconds($show = true)
	{
		$this->addConsoleError('Method TCv_FormItem_SelectDateTime::setShowSeconds() has no effect and is deprecated. ');
	}
	
	//////////////////////////////////////////////////////
	//
	// DATE VALIDATION
	//
	//////////////////////////////////////////////////////
	
	
	public function validateYMDValue($var)
	{
		$validation = new TCm_FormItemValidation($this->id, 'Validate Year-Month-Day');
		
		// Only bother if we're dealing with all 3 values
		if(substr($var,0,3) == 'YMD')
		{
			$date = new DateTime($_POST[$this->id]);
			//	$stop_date = new DateTime($form_processor->formValue('stop_date'));
			
			
			if($date->format('Y-m-d') != trim(substr($_POST[$this->id()],0,10)))
			{
				$validation->failWithMessage('' . htmlspecialchars($this->title) . ' is not a valid date.');
			}
			
		}
		
		return $validation;
	}
	
	public function setIsRequired($disable_validation = false)
	{
		if(!$disable_validation && !$this->use_date_calendar)
		{
			$this->addProcessMethodCall('validateNonBlankDates',
			                            TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
			                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
			                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO);
			
		}
		parent::setIsRequired($disable_validation);
	}
	
	/**
	 * Returns the value that will be entered into the database. The default operation for this method is return the value
	 * provided by submittedValue(). If a new value is to be provided, this method should be overridden to provide the
	 * proper value to be entered into the database.
	 * @return string
	 */
	public function databaseValue()
	{
		$value = $this->submittedValue();
		if($this->emptyValuesAsNull())
		{
			$is_empty = true;
			
			// Calendars or any other empty string, should be null
			if(trim($value) == '')
			{
				return null;
			}
			
			// YEARS
			if(str_contains($this->view_order,'Y') && substr($value, 0,4) != '0000')
			{
				$is_empty = false;
			}
			
			// MONTHS
			if(str_contains($this->view_order,'M') && substr($value, 5,2) != '00')
			{
				$is_empty = false;
			}
			
			// DAY
			if(str_contains($this->view_order,'D') && substr($value, 8,2) != '00')
			{
				$is_empty = false;
			}
			
			// If the view mode includes dates and those dates are zero'd out, we have a null value
			if($is_empty)
			{
				return null;
			}
			
		}
		return $value;
	}
	
	/**
	 * Validates that the dates are not blank based on the View Order for the model
	 * @return TCm_FormItemValidation
	 */
	public function validateNonBlankDates()
	{
		$validation = new TCm_FormItemValidation($this->id, 'Validate Not Blank Select Date');
		//$this->addConsoleDebug('Use Calendar : '.($this->use_date_calendar ? 'yes': 'no'));
		//$this->addConsoleDebug($this->view_order);
		if($this->use_date_calendar)
		{
		
		}
		else
		{
			$fields = array();
			$fields['Y'] = 'year';
			$fields['M'] = 'month';
			$fields['D'] = 'day';
			$fields['h'] = 'hour';
			$fields['m'] = 'minute';
			$fields['s'] = 'second';
			
			
			$failed = false;
			
			foreach($fields as $code => $name)
			{
				if(strpos($this->view_order,$code) !== false)
				{
					if( @$_POST[$this->id.'_'.$name] == '')
					{
						$failed = true;
					}
				}
				
			}
		}
		
		
		
		if($failed)
		{
			$validation->failWithMessage('' . htmlspecialchars($this->title) . ' is not a valid date.');
			
		}
		return $validation;
	}
	
	//////////////////////////////////////////////////////
	//
	// DATE CALENDAR
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Enables the date picker tool which is default by the browser. This replaces the views with an HTML input
	 * [type="date"] that handles the date picking.
	 */
	public function setUseDateCalendar()
	{
		$this->use_date_calendar = true;
		$this->removeProcessMethodCall('validateNonBlankDates');
	}
	
	/**
	 * Handles the attaching of the date picker JS. This is called during the rendering of the view to adjust what is
	 * rendered.
	 */
	protected function renderDateCalendar()
	{
		// Ensure we only have the first 10 values, just in case there's time on it
		$current_value = substr(trim($this->currentValue()),0,10);
		
		$input = new TCv_FormItem_TextField($this->id(), $this->title());
		$input->setFieldType('date');
		$input->setAttribute('value', $current_value);
		$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		
		
		if($this->is_required == 1)
		{
			$input->setIsRequired();
		}
		if($this->disabled)
		{
			$input->setAttribute('disabled','disabled');
		}
		
		$input->addClassArray($this->form_element_classes);
		$input->addDataValueArray($this->form_element_data_values);
		
		foreach($this->attributes as $name => $value)
		{
			$input->setAttribute($name,$value);
		}
		
		// attach the field to view this item
		$this->attachView($input->fieldView());
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TCv_SearchableModelList
	//
	//////////////////////////////////////////////////////
	
	public function clearableInSearchableAppliedFilters(): bool
	{
		// If we're not using the date calendar and there are no blank starting values, then we can't be clearable
		
		if(!$this->use_date_calendar && !$this->use_blank_starting_values)
		{
			return false;
		}
		
		return parent::clearableInSearchableAppliedFilters();
	}
	
	public function appearsInSearchableAppliedFilters(): bool
	{
		if(!$this->use_date_calendar && !$this->use_blank_starting_values)
		{
			return false;
		}
		return parent::appearsInSearchableAppliedFilters();
	}
	
	
}
?>