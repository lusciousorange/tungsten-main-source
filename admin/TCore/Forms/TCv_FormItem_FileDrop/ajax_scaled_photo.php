<?php
	$skip_tungsten_authentication = true; // called from various spots, not always from admin
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");
	

	$return_filename = $_GET['filename'];
	$class_name = $_GET['model_class'];

	$upload_folder = $class_name::uploadFolder();
	
	if(isset($_GET['upload_folder']))
	{
		$upload_folder = htmlentities($_GET['upload_folder']);
	}
	
	$file = new TCm_File('scale_file', $return_filename, $upload_folder, TCm_File::PATH_IS_FROM_SERVER_ROOT);
	

	// Deal with header
	$ctype = '';
	switch( $file->extension() ) {
		case "gif": $ctype="image/gif"; break;
		case "png": $ctype="image/png"; break;
		case "jpeg":
		case "jpg": $ctype="image/jpeg"; break;
		case "svg": $ctype="image/svg+xml"; break;
		default:
	}

	header('Content-type: ' . $ctype);
	
	$contents = file_get_contents($file->filenameFromServerRoot());
	echo $contents;
	exit();

	
?>