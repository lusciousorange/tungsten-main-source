class TCv_FormItem_FileDrop {

	// The field ID that this form dropper is associated with. Pull from the element passed in
	field_id = null;
	cropper_value_field = null;
	form_item_container = null;
	field_target = null;
	preview_container = null;
	first_load = true;
	field_row = null;

	dropzone_params = {};

	cropper = null;
	cropper_image = null; // reference to the cropper image

	change_event = new Event('change');

	options = {
		mode : 'single', // 'multiple', 'cropper'
		upload_script : '',
		upload_folder : '',
		model_class : '',
		preview_class_name : false,
		permitted_mime_types : null,
		minimum_width : 0,
		minimum_height : 0,
		save_outside_root_folder : false,
		cropper_init_data : false,
		cropper_ratio : null,

		// The specific width and height set, if provided
		cropper_width : null,
		cropper_height : null,

	};


	// CROPPER SETTINGS
	is_svg = false;

	/**
	 * @param {Element} element
	 * @param {Object} values
	 */
	constructor(element, values = {}) {
		this.form_item_container = element;
		this.field = element.querySelector('.file_drop_field_value');
		this.field_row = element.closest('.TCv_FormItem_FileDrop');
		this.field_target = this.form_item_container.querySelector('.file_drop_target');
		this.preview_container = this.form_item_container.querySelector('.preview_container');

		//console.log(this.field);
		// Trigger the file status update
		this.updateHasFileStatus();

		// Save config settings
		Object.assign(this.options, values);

		// Save the field ID
		this.field_id = this.field.getAttribute('id');

		//console.log('constructor', this.field);
		//console.log(values);


		this.initFileDropper();

		// Deal with remove links. Not shown with cropper mode
		if(this.preview_container)
		{
			this.preview_container.addEventListener('click', this.handleRemoveFile.bind(this));
		}

		if(this.isCropperMode()) {

			// Save the cropper image, add eventual listeners
			this.cropper_image = this.form_item_container.querySelector('.cropper_image');
			this.cropper_image.addEventListener('ready', () => this.cropperBuilt());
			this.cropper_image.addEventListener('cropend', () => this.cropperChanged() );

			// check if an existing file exists
			if(this.field.value === '')
			{
				// avoid failing the first time if there's no value
				this.first_load = false;
			}

			// Event listeners
			this.form_item_container.querySelector('.aspect_ratio_value').addEventListener('change', (e) => this.aspectRatioChanged(e));
			this.form_item_container.querySelector('.recrop_link').addEventListener('click', (e) => this.showCropper(e));
			this.form_item_container.querySelector('.cancel_crop_link').addEventListener('click', (e) => this.cancelCropper(e));
			this.form_item_container.querySelector('.confirm_crop_link').addEventListener('click', (e) => this.confirmCropper(e));


			this.cropper_value_field = this.form_item_container.querySelector('#' + this.field_id + '_cropper_values');


		}
		else if(this.isMultipleMode())
		{
			// Loop through each <ul>, configuring a sortable with the group

			new Sortable(this.preview_container, {
				handle : '.file_preview_box',
				animation : 150,
				fallbackOnBody : true,
				swapThreshold : 0.65,
				direction : 'vertical',
				onSort : (e) =>
				{
					this.updateMultipleOrder(e);
				},
			});
		}

	}

	/**
	 * Function called to update the field value, which triggers the change event, but also adds some consistency in how to
	 * behave whenever we update the field value.
	 * @param new_value
	 */
	updateFieldValue(new_value) {
		//console.log("UPDATE VALUE: " + new_value);
		this.field.value = new_value;
		this.field.dispatchEvent(this.change_event);

		this.updateHasFileStatus();
	}

	/**
	 * Updates the status of the container to indicate if this item has an existing file
	 */
	updateHasFileStatus() {

		if(this.field.value === '')
		{
			this.form_item_container.classList.remove('has_file');
			this.form_item_container.classList.add('no_file');
		}
		else
		{
			this.form_item_container.classList.add('has_file');
			this.form_item_container.classList.remove('no_file');
		}
	}

	/**
	 * Returns if this is set to single mode
	 * @returns {boolean}
	 */
	isSingleMode() {
		return this.options.mode === 'single';
	}

	/**
	 * Returns if this is set to multiple mode
	 * @returns {boolean}
	 */
	isMultipleMode() {
		return this.options.mode === 'multiple';
	}

	/**
	 * Returns if this is set to cropper mode
	 * @returns {boolean}
	 */
	isCropperMode() {
		return this.options.mode === 'cropper';
	}


	initFileDropper() {

		// Determine preview class name, not always necessary
		let preview_class_name = this.options.preview_class_name;



		this.dropzone_params =  {
			'model_class'			: this.options.model_class,
			'upload_folder' 		: this.options.upload_folder,
			'save_outside_root_folder' : this.options.save_outside_root_folder ? 1 : 0,
			'minimum_width'			: this.options.minimum_width,
			'minimum_height'		: this.options.minimum_height,
			'preview_class_name'	: preview_class_name,
			'cropper_ratio'			: this.options.cropper_ratio,
			'cropper_width'			: this.options.cropper_width,
			'cropper_height'		: this.options.cropper_height,
		};

		// @see https://docs.dropzone.dev/configuration/basics/configuration-options
		let dropzone_options = {
			url: this.options.upload_script,

			// Set the click target to be the drop target
			clickable : this.field_target,

			// Use our own previews
			disablePreviews : true,

			createImageThumbnails : false,


			params :this.dropzone_params,
		};


		if(this.options.permitted_mime_types != null)
		{
			dropzone_options.acceptedFiles = this.options.permitted_mime_types.join(', ');
		}

		//console.log(dropzone_options);

		// Create the dropzone
		let myDropzone = new Dropzone(this.form_item_container, dropzone_options);

		// Event listeners
		myDropzone.on('addedfile', (file) => { this.handleFileAdded(file) });
		myDropzone.on('success', (file, response) => { this.handleUploadSuccess(file, response) });
		myDropzone.on('error', (file, response) => this.handleUploadError(file, response));



	}

	/**
	 * Handler to deal with when the file is initially added.
	 * @param file
	 */
	handleFileAdded(file) {
		let error_box = this.form_item_container.querySelector('.error_box');

		if(error_box)
		{
			error_box.remove();
		}

		this.field_target.classList.add('tungsten_loading');
	}

	/**
	 * The DropZone handler for when the upload is a success
	 * @param {File} file
	 *@param {string} response
	 * */
	handleUploadSuccess(file, response) {
		//console.log('handleUploadSuccess', file);
		this.field_target.classList.remove('tungsten_loading');

		response = JSON.parse(response);
		//console.log('response', response);


		// Single and cropper behave the same way
		if(this.isSingleMode() || this.isCropperMode())
		{
			// Replace the values entirely
			this.preview_container.innerHTML = response.preview;
			this.updateFieldValue(response.filename);

			if(response.extension == 'svg')
			{
				this.field_row.classList.add('is_svg');
			}
			else
			{
				this.field_row.classList.remove('is_svg');
			}

		}
		else if(this.isMultipleMode())
		{
			// Append everything to the existing list
			this.preview_container.innerHTML += response.preview;

			// Break up the string, recombine with the new value
			let parts = this.field.value.split(',');
			if(this.field.value ==="")
			{
				parts = [];
			}
			parts.push(response.filename);
			this.updateFieldValue(parts.join(','));

		}

		if(this.isCropperMode())
		{
			if(response.publicly_visible)
			{
				// use the file path value that came back from the upload
				this.cropper_image.setAttribute('src',response.filepath);
			}
			else // not publicly visible, use the script instead
			{
				var url_data = [];
				url_data.push('filename=' + encodeURI(response.filename));
				url_data.push('model_class=' + encodeURI(this.options.model_class));
				url_data.push('upload_folder=' + encodeURI(this.options.upload_folder));

				let image_source = "/admin/TCore/Forms/TCv_FormItem_FileDrop/ajax_scaled_photo.php";
				image_source += '?' + url_data.join('&');
				this.cropper_image.setAttribute('src',image_source);
			}

			this.configureCropper();

		}




		// This will create a container that is numbered for each file. When uploading, it will put the status in that
		// box. Once it's complete, it will put the preview there instead.

		//file.number = this.file_number;
		//this.file_number++;
	}

	/**
	 * The DropZone handler for when the upload is an error
	 * @param {File} file
	 * @param {string} response
	 */
	handleUploadError(file, response) {
		//console.log('handleUploadError');
		this.field_target.classList.remove('tungsten_loading');

		let error_box = document.createElement('div');
		error_box.classList.add('error_box');
		error_box.textContent = response;

		this.form_item_container.prepend(error_box);

		//console.error('error', file, response);
	}

	handleRemoveFile(event) {
		event.preventDefault(); // disable the click

		// check if it's a remove button
		if(event.target.closest('.remove_file_button'))
		{
			// console.log(event);
			// console.log(this);

			// delete preview DOM element
			let box = event.target.closest('.file_preview_box');
			let filename = box.getAttribute('data-filename');
			box.remove();


			// remove value
			let new_value = '';
			if(this.isMultipleMode())
			{
				// Break up the string, recombine with the new value
				let parts = this.field.value.split(',');

				for(const i in parts)
				{
					if(parts[i] === filename)
					{
						parts.splice(i,1);
					}
				}
				new_value = parts.join(',');
			}

			this.updateFieldValue(new_value);
			this.saveCropperValues();

		}
	}

	/**
	 * Takes in an array of values and rounds them off for even math
	 * @param {Array} values
	 * @returns {*}
	 */
	roundCropperValues(values) {
		values.x = Math.round(values.x);
		values.y = Math.round(values.y);
		values.width = Math.round(values.width);
		values.height = Math.round(values.height);
		return values;
	}

	/**
	 * Returns the current cropper values cleaned for saving and usage
	 * @returns {*}
	 */
	getCleanedCropperValues() {
		let data = this.cropper_image.cropper.getData();
		let right_x 	= data.x + data.width;
		let bottom_y 	= data.y + data.height;
		let image_width = this.cropper_image.cropper.getImageData().naturalWidth;
		let image_height= this.cropper_image.cropper.getImageData().naturalHeight;


		// WIDTH/HEIGHT bigger than the possible
		if(data.width > image_width)
		{
			data.width = image_width;
			right_x = data.x + data.width;
		}
		if(data.height > image_height)
		{
			data.height = image_height;
			bottom_y = data.y + data.height;
		}


		// OVER THE LEFT EDGE
		if(data.x < 0)
		{
			data.x = 0;
			right_x = data.x + data.width;
		}
		if(data.y < 0)
		{
			data.y = 0;
			bottom_y = data.y + data.height;
		}

		// Over the right edge, move left edge back by the difference
		if(right_x > image_width)
		{
			data.x -= right_x - image_width;
		}

		if(bottom_y > image_height)
		{
			data.y -= bottom_y - image_height;
		}


		// Round off values
		data = this.roundCropperValues(data);

		return data;

	}

	/**
	 * Trigger when the cropping dimensions have changed. We mainly need to just clean the values
	 *
	 */
	cropperChanged() {
		// Ensure values are always rounded and avoid spilling over
		let clean_values = this.getCleanedCropperValues();
		this.cropper_image.cropper.setData(clean_values);
	}

	/**
	 * Called each time when the cropper is built
	 */
	cropperBuilt() {
		if(this.isCropperMode())
		{
			// First load grabs values from the initial data and loads it in
			if (this.first_load)
			{
				this.first_load = false;
				// Only bother if we're getting real init data
				// All zeros should be ignored so that the default interaction occurs
				if (this.options.cropper_init_data.width > 0 && this.options.cropper_init_data.height > 0)
				{


					// Convert values to integers
					for(let val in this.options.cropper_init_data)
					{
						this.options.cropper_init_data[val] = parseInt(this.options.cropper_init_data[val]);
					}

					// save the initial crop value
					this.cropper_image.cropper.setData(this.options.cropper_init_data);

				}
			}
			// else // subsequent builds need to trigger that the cropper has changed, to update the values
			// {
			// 	this.cropperChanged();
			//
			//
			// }

			// Save the values
			this.saveCropperValues();


			// Remove loading
			this.form_item_container.classList.remove('tungsten_loading');

		}

	}

	/**
	 * Takes the current cropper values that are in place and saves them
	 */
	saveCropperValues() {
		if(this.isCropperMode())
		{

			let new_value = '';
			if(this.field.value != '')
			{
				// current values
				let cv = this.roundCropperValues(this.cropper_image.cropper.getData());
				new_value = cv.x + ',' + cv.y + ',' + cv.width + ',' + cv.height;

				if(this.options.cropper_width > 0)
				{
					new_value += ',' + this.options.cropper_width;
				}

			}
			this.cropper_value_field.value = new_value;
		}
	}

	/**
	 * Gets the current aspect ratio value
	 * @returns {string}
	 */
	currentAspectRatio() {
		return this.form_item_container.querySelector('.aspect_ratio_value').value;
	}

	/**
	 * Triggered whenever the aspect ratio is changed
	 * Note: since this is a hidden field, the JS that performs the change must all call .trigger('change') on the field
	 */
	aspectRatioChanged() {
		if(this.isCropperMode())
		{
			let aspect_ratio = this.currentAspectRatio();
			this.cropper_image.cropper.setAspectRatio(aspect_ratio);

			if(this.field.value !== '')
			{
				this.cropperChanged();

				// Detect if we aren't showing the cropper, therefore need to recrop it manually
				if(!this.form_item_container.classList.contains('show_cropper'))
				{
					// Only bother if it's NOT free form
					if(aspect_ratio !== '')
					{
						this.confirmCropper();
					}

				}
			}
		}
	}



	/**
	 * Configures the cropper  the cropper to the default values which are reset each time it is initialized or a new file is uploaded.
	 *
	 */
	configureCropper() {

		// Don't do anything for SVGs
		let field_value = this.field.value;
		this.is_svg = field_value.substr(-4).toLowerCase() === '.svg';

		if(this.is_svg)
		{
			this.first_load = false;
			return
		}

		if(this.isCropperMode())
		{
			// Destroy the old one
			if(this.cropper)
			{
				this.cropper_image.cropper.destroy();
			}

			let cropper_config = {
				zoomable		: false,
				moveable		: false,
				scalable		: false,
				autoCropArea	: 1.0, // Start with max space,
				aspectRatio		: this.currentAspectRatio(),
				scaleX			: 1,
				scaleY			: 1,
				rotate			: 0,
			};

			// If photo exists
			if(this.field.value !== '')
			{
				this.cropper = new Cropper(this.cropper_image, cropper_config);
			}
		}

	}

	/**
	 * Hides the cropper and shows the preview image
	 *
	 * @param {Event} event
	 */
	showCropper(event) {
		//console.log('showCropper');
		event.preventDefault();
		if(this.is_svg)
		{
			return;
		}

		this.form_item_container.classList.add('show_cropper');
		this.configureCropper();

		this.triggerWindowResizeEvent();
	}

	/**
	 * Hides the cropper and shows the preview image
	 *
	 * @param {Event} event
	 */
	cancelCropper(event) {
		//console.log('cancelCropper');
		event.preventDefault();

		this.form_item_container.classList.remove('show_cropper');

		// set the cropper to use them
		let cropper_data = this.cropper_image.cropper.getData();
		let last_saved = this.cropper_value_field.value.split(',');

		cropper_data.x = Math.round(last_saved[0]);
		cropper_data.y = Math.round(last_saved[1]);
		cropper_data.width = Math.round(last_saved[2]);
		cropper_data.height = Math.round(last_saved[3]);

		this.cropper_image.cropper.setData(cropper_data);

		this.triggerWindowResizeEvent();
	}

	confirmCropper(event = null) {
		//console.log('confirmCropper');
		if(event)
		{
			event.preventDefault();
		}
		this.form_item_container.classList.add('tungsten_loading');

		// Save the current data set
		this.saveCropperValues();

		// Disable showing the cropper
		this.form_item_container.classList.remove('show_cropper');


		let form_data = new FormData();
		for(let name in this.dropzone_params)
		{
			form_data.append(name, this.dropzone_params[name]);
		}

		// Add values for re-crop
		form_data.append('filename', this.field.value);
		form_data.append('crop', this.cropper_value_field.value);


		// Trigger the call
		let url = '/admin/TCore/Forms/TCv_FormItem_FileDrop/process_recrop.php';
		let init = {
			method: 'POST',
			headers: {},
			body : form_data
		};
		fetch(url, init)
			.then(response => response.json())
			.then(response =>{
				//console.log(response);

				// Replace the values entirely
				this.preview_container.innerHTML = response.preview;
			}).finally(() =>{
				this.form_item_container.classList.remove('tungsten_loading');
			});

		this.triggerWindowResizeEvent();
	}

	/**
	 * Triggers the resize event for a window, which helps keep thing aligned
	 */
	triggerWindowResizeEvent() {
		window.dispatchEvent(new Event('resize'));
	}

	/**
	 * Updates the order whenever multiple items are rearranged.
	 */
	updateMultipleOrder() {
		let filenames_reordered = [];

		// Find all the children that are file preview boxes
		for(let child of this.preview_container.querySelectorAll('.file_preview_box'))
		{
			let filename = child.getAttribute('data-filename');
			if(filename !== '')
			{
				filenames_reordered.push(filename);
			}
		}

		// save the value, avoid bad values with no arrays
		if(filenames_reordered.length > 0)
		{
			this.updateFieldValue(filenames_reordered.join(','));

		}
		else
		{
			this.updateFieldValue('');
		}

	}



}