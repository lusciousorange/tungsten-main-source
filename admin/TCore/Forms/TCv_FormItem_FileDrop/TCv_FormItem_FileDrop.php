<?php

/**
 * Class TCv_FormItem_FileDrop
 *
 * A form item that allows for files to be drag and dropped into the view but also uses a cropper to allow for photo
 * cropping.
 */
class TCv_FormItem_FileDrop extends TCv_FormItem
{
	protected $mode = 'single'; // 'cropper', 'multiple'
	public    $model_class = false; // must be public so it can be passed to the processor effectively
	protected $current_file = null;
	
	// UPLOAD SETTINGS
	protected $upload_script = ''; // The folder where the file should be put
	public    $upload_folder = false;
	
	// PREVIEWS
	protected $preview_class_name = 'TCv_FilePreview';
	
	// CROPPER
	protected $cropper_values = false;
	protected $cropper_ratio = false;
	protected $cropper_width = null;
	protected $cropper_height = null;
	
	protected $cropper_field = false;
	protected $save_outside_root_folder = false;

	protected $minimum_width = null;
	protected $minimum_height = null;
	
	// MIME TYPES
	protected ?array $permitted_mime_types = null;
	protected $mime_type_conversions  = [
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'png' => 'image/png',
		'svg' => 'image/svg+xml',
		'mp4' => 'video/mp4',
		'webm'=> 'video/webm',
		'pdf' => 'application/pdf',
		
		// MS OFFICE
		'doc' => 'application/msword',
		'docx' => 'application/application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'xls' => 'application/vnd.ms-excel',
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	
	];
	
	/**
	 * TCv_FormItem_FileDrop constructor.
	 *
	 * @param bool $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
			
		
		$this->upload_script = $this->classFolderFromRoot('TCv_FormItem_FileDrop').'/process_upload.php';
		
		// Load DropZone
		$this->addJSFile('DropZone',$this->classFolderFromRoot('TCv_FormItem_FileDrop', true).'/DropZone/dropzone.min.js');
		$this->addCSSFile('DropZone',$this->classFolderFromRoot('TCv_FormItem_FileDrop', true).'/DropZone/dropzone.min.css');
		// Class files
		$this->addClassJSFile('TCv_FormItem_FileDrop');
		
		$this->addClassCSSFile('TCv_FormItem_FileDrop');
		
		
		
		
	}
	
	
	/**
	 * Returns the mode for this file dropper. One of three values of 'single', 'multiple', or 'cropper'
	 * @return string
	 */
	public function mode()
	{
		return $this->mode;
	}
	
	
	public function isCropperMode()
	{
		return $this->mode() == 'cropper';
	}
	
	public function isMultipleMode()
	{
		return $this->mode() == 'multiple';
	}
	
	public function isSingleMode()
	{
		return $this->mode() == 'single';
	}
	
	/**
	 * Sets the upload folder. This is usually acquired by the Model class called the class method uploadFolder(). This
	 * is an override that is based on the site root.
	 * @param string $upload_folder
	 */
	public function setUploadFolder($upload_folder)
	{
		// Detect a backwards folder. Can't have that
		if(strpos($upload_folder,'..') !== false)
		{
			$parts = explode('/',$upload_folder);
			$index = 0;
			$index_to_delete = null;
			for($place = 0; $place < count($parts); $place++)
			{
				if($parts[$place] == '..')
				{
					if($place > 1) // we're not at the front, 0/1/..
					{
						$index_to_delete = $place-1;
					}
				}
			}
			
			if($index_to_delete)
			{
				unset($parts[$index_to_delete]); // Delete the previous one
				unset($parts[$index_to_delete+1]); // Delete the ..
				
			}
			
			$upload_folder = implode('/', $parts);
			
		}
		
		$this->upload_folder = $upload_folder;
	}
	
	/**
	 * Indicates that this file should be saved outside the website root, at which point the script will use the same
	 * path but use folders that are one level inside the site root and therefore unobtainable.
	 *
	 * Using this feature requires manual dealing with the file during processing.
	 */
	public function setSaveOutsideRootFolder()
	{
		$this->save_outside_root_folder = true;
	}
	
	/**
	 * Turns off the preview for this filedroppers
	 * @deprecated No longerused
	 */
	public function disablePreview()
	{
	
	}

	public function setUseMultiple($db_handling = TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS,
								   $pairing_table_name = false,
								   $paired_column_name = false,
								   $editor_model = false)
	{
		$this->setAsMultipleFiles();

	}

	/**
	 * Turns on the ability for multiple files to be uploaded. This will disable the cropper feature if it is enabled.
	 */
	public function setAsMultipleFiles()
	{
		$this->mode = 'multiple';
		$this->addJSFile('SortableJS','https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js');
		
	}

	/**
	 * Turns on the ability for multiple files to be uploaded that also permits the order of items to be rearranged. This will disable the cropper feature if it is enabled.
	 * @deprecated This does nothing since all of the multiple files setups are rearrangable by default
	 */
	public function setAsMultipleFilesWithRearranging()
	{
		$this->setAsMultipleFiles();
	
	}


	/**
	 * Sets the name of the view that will be used for previews
	 * @param string $class_name
	 */
	public function setPreviewClassName($class_name)
	{
		// Previews must be a subclass of TCv_FilePreview for functional consistency
		if($class_name != 'TCv_FilePreview' && !is_subclass_of($class_name,'TCv_FilePreview'))
		{
			TC_triggerError('Preview class must be a subclass of TCv_FilePreview');
		}
		$this->preview_class_name = $class_name;
		$this->addClassCSSFile($class_name);

	}
	
	/**
	 * Sets the model class name
	 * @param string $class_name The name of the class
	 */
	public function setModelClassName($class_name)
	{
		$this->model_class = $class_name;
		$this->upload_folder = false; // using a model class turns off the upload folder
	}
	
	/**
	 * Turns on the "close" button for previews. The settings require a URL target that a close button should be added
	 * to the previews of an editor. This requires a URL target that must be called that requires a model of the same
	 * type that is being edited.
	 *
	 * @param string $module_name The name of the module that should be called
	 * @param string $url_target_name The name of the URL target in that module
	 * @deprecated use setRemoveFileURLTarget instead
	 * @see TCv_FormItem_FileDrop::setRemoveFileURLTarget()
	 */
	public function setEditorPreviewShowURLTarget($module_name, $url_target_name)
	{
		$this->setRemoveFileURLTarget($module_name, $url_target_name);
	}

	/**
	 * Sets the values for the URL target that is asyncronously called when the remove button is clicked.
	 * The settings require a URL target that a close button should be added
	 * to the previews of an editor. This requires a URL target that must be called that requires a model of the same
	 * type that is being edited.
	 *
	 * @param string $module_name The name of the module that should be called
	 * @param string $url_target_name The name of the URL target in that module
	 * @deprecated No longer performs any functionality. Form must be updated
	 */
	public function setRemoveFileURLTarget($module_name, $url_target_name)
	{
	}

	/**
	 * Sets teh upload script
	 * @param string $upload_script
	 */
	public function setUploadScript($upload_script)
 	{
		$this->upload_script = $upload_script;
 	}

	//////////////////////////////////////////////////////
	//
	// FILETYPE SETTINGS
	//
	// It's possible to resrict the uploader to only permit
	// certain filetypes
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the uploader to only accept certain filetype extensions for upload. This will only work if it finds the
	 * common MIM
	 *
	 * @param array $permitted_extensions The list of extensions that are permitted
	 * @param null $message Deprecated value
	 * @deprecated use addPermittedMimeType or other MIME Type functions
	 */
	public function setPermittedExtensions($permitted_extensions, $message = null)
	{
		foreach($permitted_extensions as $index => $extension)
		{
			$extension = strtolower($extension);
			
			if(isset($this->mime_type_conversions[$extension]))
			{
				$this->permitted_mime_types[$this->mime_type_conversions[$extension]] =
					$this->mime_type_conversions[$extension];
			}
			
			
			
		}
	

	}
	
	/**
	 * Adds a new MIME type to the accepted types
	 *
	 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
	 * @param string $mime_type
	 * @return void
	 */
	public function addPermittedMimeType(string $mime_type) : void
	{
		$this->permitted_mime_types[$mime_type] = $mime_type;
	}
	
	/**
	 * Allow PDFs
	 * @return void
	 */
	public function setAllowPDFs() : void
	{
		$this->addPermittedMimeType($this->mime_type_conversions['pdf']);
	}
	
	/**
	 * Allow Word Docs
	 * @return void
	 */
	public function setAllowWordDocs() : void
	{
		$this->addPermittedMimeType($this->mime_type_conversions['doc']);
		$this->addPermittedMimeType($this->mime_type_conversions['docx']);
	}
	
	/**
	 * Allow Excel Docs
	 * @return void
	 */
	public function setAllowExcelDocs() : void
	{
		$this->addPermittedMimeType($this->mime_type_conversions['xls']);
		$this->addPermittedMimeType($this->mime_type_conversions['xlsx']);
	}
	
	
	/**********************************************/
	/*										   	  */
	/*        IMAGE DIMENSIONS - MINIMUM          */
	/*										   	  */
	/**********************************************/
	// ! ----- IMAGE DIMENSIONS - MINIMUM -----



	/**
	 * Sets the form item to validate an image to be a minimum width and height
	 *
	 * @param int $width
	 * @param int $height
	 */
	public function setMinimumImageDimensions($width, $height) : void
	{
		$this->minimum_width = $width;
		$this->minimum_height = $height;
	}


	
	//////////////////////////////////////////////////////
	//
	// CROPPER SETTINGS
	//
	// The methods related to having a photo cropper enabled
	// for this drop item
	//
	//////////////////////////////////////////////////////

	/**
	 * Enables the cropper for this file drop view.
	 *
	 * The cropper requires a few additional parameters to be set, which can be overridden if necessay after this method call.
	 * * multiples_files is turned off since it can only handle one image
	 * * the permitted extensions are set to jpg, jpeg and gif
	 *
	 * @param float|string|bool|array $ratio The ratio that the photos will be limited to, which must be a float, often calculated by
	 * dividing the width by the height such as 3/2 would lead to a ratio of 1.5 where the width is 1.5 times the height.
	 * A ratio can also be provided as a string with a colon such as '5:2'
	 * IF an array is provided, then it is the actual maximum width and height to be used [$width, $height)
	 *
	 * If only the first value in the array is set, then it sets the crop width without any forced ratio
	 *
	 * @param bool $allow_svg (Optional) Default false. Indicate if svgs should be included.
	 * @uses TCv_FormItem_FileDrop::setPermittedExtensions()
	 * @uses TCv_FormItem_FileDrop::setScalePhotoWidth()
	 * @uses TCv_FormItem_FileDrop::disablePreview()
	 *
	 */
	public function setUseCropper($ratio = false, $allow_svg = false)
	{
		$this->mode = 'cropper';
		//$this->disablePreview();
		
		if(is_array($ratio) && sizeof($ratio) == 2)
		{
			list($photo_width, $photo_height) = $ratio;
			
			
			if(!is_null($photo_height) && $photo_height > 0)
			{
				// Save the image dimensions
				$this->setMinimumImageDimensions($photo_width, $photo_height);
				$ratio = $photo_width / $photo_height;
				$this->cropper_height = $photo_height;
			}
			else
			{
				$ratio = false;
				
				// Save the image dimensions,but need a stupid high height, since there's no real limit
				$this->setMinimumImageDimensions($photo_width, 0);
			}
			
			// Sets the cropper width
			$this->cropper_width = $photo_width;
			
			
		}
		$this->cropper_ratio = $ratio;

		$this->addPermittedMimeType('image/jpeg');
		$this->addPermittedMimeType('image/png');
		$this->addPermittedMimeType('image/gif');
		
		if($allow_svg)
		{
			$this->addPermittedMimeType('image/svg+xml');
		}
		
	}


	/**
	 * Returns the data values for the cropper
	 * @return array
	 */
	public function cropperDataValues()
	{
		if($this->cropper_values === false)
		{
			$values = array();
			$values['x'] = 0;
			$values['y'] = 0;
			$values['width'] = 0;
			$values['height'] = 0;

			// No set in JS
			//$values['scaleX'] = 1;
			//$values['scaleY'] = 1;
			//$values['rotate'] = 0;

			if($this->currentFile())
			{
				if ($crop_values = $this->currentFile()->cropValues())
				{
					$values['x'] = $crop_values['src_x'];
					$values['y'] = $crop_values['src_y'];
					$values['width'] = $crop_values['src_width'];
					$values['height'] = $crop_values['src_height'];

				}

				// Deal with no values in an old file or with no settings
				// Use the full width and height
				if(!$crop_values || $crop_values['src_width'] == 0)
				{
					$image = new TCv_Image('', $this->currentFile());
					$values['width'] = $image->actualWidth();
					$values['height'] = $image->actualHeight();

				}

			}

			$this->cropper_values = $values;

		}

		return $this->cropper_values;
	}

	/**
	 * Sets the cropper values to a string provided
	 * @param $string
	 */
	public function setCropperValuesWithString($string)
	{
		list($x, $y, $width, $height ) = explode(',', $string);
		$this->cropper_values = array(
			'x' => $x,
			'y' => $y,
			'width' => $width,
			'height' => $height,

		);

		// No crop value
		if(strpos($this->currentValue(),'{CROP') === false)
		{
			$this->setSavedValue($this->currentValue().'{CROP,'.$string.'}');
		}
	}



	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the current file if it exists. Possibly returns an array of files
	 * @return bool|TCm_File|TCm_File[]
	 */
	public function currentFile()
	{
		if($this->current_file === null)
		{
			$model_class = $this->model_class;
			if (class_exists($model_class))
			{
				// Multiple files returns an array
				if($this->isMultipleMode())
				{
					$this->current_file = [];
					$values = explode(',', $this->currentValue());
					foreach($values as $index => $filename)
					{
						$file = new TCm_File($this->id() . '_file_'.$index, $filename, $this->upload_folder,
											 TCm_File::PATH_IS_FROM_SERVER_ROOT);
						if($file->exists())
						{
							$this->current_file[] = $file;
						}
						else
						{
							$this->addConsoleWarning('File Not Found : '.$filename);
						}
					}

				}
				else
				{
					$file = new TCm_File($this->id() . '_file', $this->currentValue(), $this->upload_folder,
										 TCm_File::PATH_IS_FROM_SERVER_ROOT);
					if($file->exists())
					{
						$this->current_file = $file;
					}
				}

			}
		}
		
		return $this->current_file;
	}
	
	/**
	 * Returns the submitted files that are provided during form processing. These are already saved to the proper
	 * location upon upload.
	 * @return TCm_File[]
	 */
	public function submittedFiles()
	{
		$files = array();
		
		$model_class = $this->model_class;
		if (class_exists($model_class))
		{
			// Multiple files returns an array
			if($this->isMultipleMode())
			{
				foreach($this->submittedValue() as $index => $filename)
				{
					$file = new TCm_File($this->id() . '_file_'.$index, $filename, $this->upload_folder,
										 TCm_File::PATH_IS_FROM_SERVER_ROOT);
					if($file->exists())
					{
						$files[] = $file;
					}
					else
					{
						$this->addConsoleWarning('File Not Found : '.$filename);
					}
				}
				
			}
			else
			{
				$file = new TCm_File($this->id() . '_file', $this->submittedValue(), $this->upload_folder,
									 TCm_File::PATH_IS_FROM_SERVER_ROOT);
				if($file->exists())
				{
					$files[] = $file;
				}
			}
			
		}
		
		return $files;
	}

	/**
	 * @return string
	 */
	public function render()
	{
		// CONFIGURE INIT VALUES
		
		// Set the upload folder
		$model_class = $this->model_class;
		if($this->upload_folder == false && method_exists($model_class , 'uploadFolder'))
		{
			$this->setUploadFolder($model_class::uploadFolder());
		}
		
		$init_values = array(
			'mode' => $this->mode(),
			"upload_script" => $this->upload_script,
			"upload_folder" => ($this->upload_folder ? $this->upload_folder : '') ,
			'save_outside_root_folder' => $this->save_outside_root_folder,
			"model_class" => $this->model_class,
			'preview_class_name' => $this->preview_class_name,
			"minimum_width" => $this->minimum_width,
			"minimum_height" => $this->minimum_height,
		);
		
		
		
		if($this->permitted_mime_types)
		{
			$init_values['permitted_mime_types'] = array_values($this->permitted_mime_types);
			
		}
		
		if($this->isCropperMode())
		{
			$init_values['cropper_ratio'] = $this->cropper_ratio;
			if(!is_null($this->cropper_width))
			{
				$init_values['cropper_width'] = $this->cropper_width;
			}
			if(!is_null($this->cropper_height))
			{
				$init_values['cropper_height'] = $this->cropper_height;
			}
		}
		
		
		
		
		$container = new TCv_View($this->id().'_drop_target');
		$container->addDataValue('field-id', $this->id());
		$container->addClass('file_drop_target');
		$container->addClassArray($this->classes());
		$container->addText('Drop a file here, or click to browse');
		$this->attachView($container);
		
		$model_class = $this->model_class;
		
		$preview_container = new TCv_View();
		$preview_container->addClass('preview_container');
		
		if(class_exists($model_class))
		{
			if($this->isMultipleMode())
			{
				$files = $this->currentFile();
			}
			else
			{
				$files = array();
				$files[] = $this->currentFile();
			}
			
			foreach($files as $file)
			{
				if ($file && $file->exists())
				{
					
					$class_name = $this->preview_class_name;
					
					/** @var TCv_FilePreview $preview */
					$preview = $class_name::init($file);
					$preview->addClass('file_preview_box');
					$preview->addDataValue('filename', $file->filename());
					$preview->showCloseLink();
					
					
					$preview_container->attachView($preview);
				}
			}
		}
		$this->attachView($preview_container);
		
		
		
		$upload_script = new TCv_FormItem_Hidden($this->id().'_filedrop_upload_script', false);
		$upload_script->setValue($this->upload_script);
		$this->attachView($upload_script);
		
		
		
		if($this->isCropperMode())
		{
			$this->attachCropper();
		}
		
		// The field for the value, which needs to be a text field that is hidden, so that the form validation triggers
		$field_value = new TCv_FormItem_TextField($this->id(), false);
		$field_value->addFormElementCSSClass('file_drop_field_value');
		
		if($this->isMultipleMode())
		{
			$field_value->setEditorValue($this->currentValue());
		}
		elseif($file = $this->currentFile())
		{
			$field_value->setEditorValue($file->filename());
		}
		else // add the value nonetheless
		{
			$field_value->setEditorValue($this->currentValue());
		}
		
		// Just the field, not the entire setup
		$this->attachView($field_value->fieldView());
		
		
		// VERY LAST THING, INIT THE JS
		$this->updateJSClassInitValues($init_values);
		$this->addClassJSInit('TCv_FormItem_FileDrop');
		
	}
	
	
	
	/**
	 * Extend functionality to test if the form is an editor. If so, then undo the need for validate blank.
	 * @param TCv_Form $form
	 */
	public function setForm(&$form)
	{
		if(!($form instanceof TCv_Form))
		{
			TC_triggerError('Provided form was not of type TCv_Form');
		}
		
		// Handle if we're being added to the form and this value hasn't been set yet
		if($this->model_class == '')
		{
			if($form instanceof TCv_FormWithModel)
			{
				$this->model_class = $form->modelName();
			}
		}
		
		if($form instanceof TCv_FormWithModel)
		{
			
			if($this->model_class == '')
			{
				TC_triggerError('TCv_FormItem_FileDrop requires a model class to be set using the method setModelClassName(). That class must implement a <em>static</em> method called uploadFolder() which returns the absolute server path for where the file should be put. ');
			}
			
			if(!method_exists($this->model_class, 'uploadFolder'))
			{
				TC_triggerError('The class "'.$this->model_class.'" must implement a <em>static</em> method called uploadFolder() which returns the absolute server path for where the file should be put. ' );
			}
		}
		
		$this->addSavedProcessorClassProperty('model_class');
		$this->addSavedProcessorClassProperty('upload_folder');
		
		$this->form = $form;
	}


	/**
	 * Override of the helpText function to return the help text for the file restrictions
	 */
	public function helpText()
	{
		$text = $this->help_text;
		if($this->isCropperMode() && $this->cropper_ratio)
		{
			$cropper_ratio = $this->cropper_ratio;
			if(!strpos($cropper_ratio,':') > 0)
			{
				$cropper_ratio .= ':1';
			}

			if($text != '')
			{
				$text .= '<br /><br/>';
			}
			else
			{
				$text .= '<br />';
			}
			
			// Cropper dimensions removed because they provided a false sense of sizing
		}

//		if($this->permitted_extensions !== false)
//		{
//			$text .= '<br /><br/><strong>Permitted Filetypes</strong><br />'.implode(', ', $this->permitted_extensions);
//
//		}
		return $text;
	}
	

	protected function attachCropper()
	{

		// Add the flag to this form row
		$this->addCSSClassForRow('use_cropper');
		
		// Load necessary JS and CSS for cropper
		$this->addCSSFile('cropper_css', 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.min.css');
		$this->addJSFile('cropper_js', 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.min.js');
		
		$cropper_ratio_float = $this->cropper_ratio;

		if($this->cropper_ratio != false)
		{
			if(strpos($cropper_ratio_float, ':') > 0)
			{
				$parts = explode(':',$cropper_ratio_float);
				$cropper_ratio_float = $parts[0] / $parts[1];
			}
			$this->updateJSClassInitValues(['aspect_ratio' => $cropper_ratio_float]);
		}

		// Aspect Ratio Hidden Field
		$field_value = new TCv_FormItem_Hidden($this->id().'_aspect_ratio', false);
		$field_value->setValue($cropper_ratio_float);
		$field_value->addClass('aspect_ratio_value');
		$this->attachView($field_value);


		// Deal with editing values
		if($this->form())
		{
			$value = $this->form()->formTracker()->submittedValueForFormItemID($this->id().'_cropper_values');
			if($value != '')
			{
				$this->setCropperValuesWithString($value);
			}
		}

		// Cropper Values Hidden Field
		$cropper_current_data_values = $this->cropperDataValues();
		$this->cropper_field= new TCv_FormItem_Hidden($this->id().'_cropper_values', false);
		$this->cropper_field->setValue(implode(',',$cropper_current_data_values));
		$this->cropper_field->setSaveToDatabase(false);
		$this->attachView($this->cropper_field);


		// CREATE THE CROPPER CONTROLS
		$cropper_controls = new TCv_View();
		$cropper_controls->addClass('cropper_controls');

			$this->addCSSClassForRow('existing_file');
			$link = new TCv_Link();
			//$link ->addClass('existing_file_action');
			$link->addClass('recrop_link');
			//$link->addClass('initial_button');
			//$link->addClass('showing');
			$link->setIconClassName('fa-crop');
			$link->addText('Adjust Cropping');
			$link->setURL('#');
			//$link->addDataValue('field-id', $this->id());
			$cropper_controls->attachView($link);

			
			$active_cropper_buttons  = new TCv_View();
			$active_cropper_buttons->addClass('active_buttons');
			
			
			$link = new TCv_Link();
			//$link ->addClass('existing_file_action');
			$link->addClass('cancel_crop_link');
			$link->setIconClassName('fa-times');
			$link->addText('Cancel cropping');
			$link->setURL('#');
			//$link->addDataValue('field-id', $this->id());
			$active_cropper_buttons->attachView($link);
		
			
			$link = new TCv_Link();
			//$link ->addClass('existing_file_action');
			$link->addClass('confirm_crop_link');
			$link->setIconClassName('fa-check');
			$link->addText('Confirm Cropping');
			$link->setURL('#');
			//$link->addDataValue('field-id', $this->id());
			$active_cropper_buttons->attachView($link);
			
			$cropper_controls->attachView($active_cropper_buttons);

		$this->attachView($cropper_controls);

		// Currently cropped image
		if($file = $this->currentFile())
		{
			$cropper_current_data_values = $this->cropperDataValues();

		}


			// Container box required to ensure a clean cropping container
		$cropper_image_container = new TCv_View();
		$cropper_image_container->addClass('cropper_container_box');

		$cropper_image = new TCv_View($this->id().'_cropper_image');
		$cropper_image->setTag('img');
		$cropper_image->addClass('cropper_image');

		if($file = $this->currentFile())
		{
			if($file->publiclyVisible())
			{
				$image_source = $file->filenameFromSiteRoot();
			}
			else // use script to pull the file
			{
				$image_source = "/admin/TCore/Forms/TCv_FormItem_FileDrop/ajax_scaled_photo.php";
				$image_source .= '?filename='.urlencode($file->filename())
					.'&model_class='.$this->model_class
					.'&upload_folder='.urlencode($this->upload_folder);
				
			}
			
			$cropper_image->setAttribute('src', $image_source);
			
			if($file->extension() == 'svg')
			{
				$this->addCSSClassForRow('is_svg');
			}
			
		}

		$cropper_image_container->attachView($cropper_image);



		$this->attachView($cropper_image_container); // added afterwards
		//$this->attachView($crop_values_field );

		$this->updateJSClassInitValues(['cropper_init_data' => $this->cropperDataValues()]);
		
		
		
		
		
		
	}

	public function databaseValue()
	{
		$filename = parent::submittedValue();

		// If there are cropper values and the filename isn't blank
		if(isset($_POST[$this->id().'_cropper_values']) && $filename != '')
		{
			// Detect invalid values
			$cropper_values = $_POST[$this->id().'_cropper_values'];
			if(count(explode(',', $cropper_values)) >= 4) // could be 4 or 5
			{
				//	$this->addConsoleDebug(explode(',', $cropper_values));
				$filename .= '{CROP,' . htmlentities($cropper_values) . '}';
			}
			
		}
		return $filename;
	}
	
	/**
	 * Extend the editor value setting to clean up trailing commas on multiple file settings. This resulted from a
	 * transition where the old setup had a trailing comma that created problems.
	 *
	 * @param string $value
	 */
	public function setEditorValue($value)
	{
		$value = trim($value.'', ',');
		
		// Multiple files returns an array
		if($this->isMultipleMode())
		{
			if(strpos($value, '{CROP') > 0)
			{
				// cropper values slipped into multiple mode
				$value = preg_replace('/\{CROP.*\}/', '', $value);
			}
		}
		parent::setEditorValue($value);
	}
	
	/**
	 * Returns the cropper value to track properly when editing
	 * @return TCv_FormItem[]
	 */
	public function additionalFormItemsToTrack()
	{
		if($this->cropper_field)
		{
			return array($this->cropper_field);
		}
		return array();

	}
	
	//////////////////////////////////////////////////////
	//
	// VALUE PROCESSING
	//
	// Functions related to processing an uploaded file
	//
	//////////////////////////////////////////////////////
	
	public static function detectFileSizeExceeded()
	{
		// Super-globals error usually means filesize exceeded
		if(sizeof($_POST) == 0)
		{
			header("HTTP/1.0 400 Bad Request");
			echo "File size upload limit exceeded. Reduce the filesize and try again.";
			exit();
		}
	}
	
	/**
	 * Determined the upload folder from the values provided, and returns it
	 * @return string
	 */
	public static function determinePostUploadFolder()
	{
		$upload_folder = '';
		if(isset($_POST['model_class']) && class_exists($_POST['model_class']))
		{
			$upload_folder = trim($_POST['model_class']::uploadFolder());
		}
		if(isset($_POST['upload_folder']) && $_POST['upload_folder'] !== '')
		{
			// We have something different than what we get from the class folder, which will just work
			// This bypasses the potential math and confusion. The upload folder matches what hte class says
			// We know the class  provides absolute links, so we can just skip it all. It's a good path.
			if($upload_folder == '' || ($upload_folder != $_POST['upload_folder']))
			{
				
				// Check if we're outside the main document root, at which point it determines how we store files
				
				$doc_root_excluding_site = explode(DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']);
				array_pop($doc_root_excluding_site);
				$doc_root_excluding_site = implode(DIRECTORY_SEPARATOR, $doc_root_excluding_site);
				
				// If we're not referencing the document root, make sure we do
				if(strpos($_POST['upload_folder'], $doc_root_excluding_site) === false)
				{
					$upload_folder = $_SERVER['DOCUMENT_ROOT'] . '/' . htmlentities($_POST['upload_folder']);
				}
				else
				{
					$upload_folder = htmlentities($_POST['upload_folder']);
				}
			}
		}
		
		
		if(isset($_POST['save_outside_root_folder']) &&  $_POST['save_outside_root_folder'])
		{
			$path = explode(DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']);
			$last_element = array_pop($path);
			$upload_folder = str_ireplace(
				DIRECTORY_SEPARATOR.$last_element.DIRECTORY_SEPARATOR,
				DIRECTORY_SEPARATOR,
				$upload_folder);
			
		}
		
		// Remove any double-slashes that might have slipped in
		$upload_folder = str_replace('//','/', $upload_folder);
		
		if($upload_folder == '')
		{
			header("HTTP/1.0 400 Bad Request");
			echo "Upload folder not set.";
			exit();
		}
		
		return $upload_folder;
	}
	
	/**
	 * Determines the filename for a posted file
	 * @param string $upload_folder
	 * @return string
	 */
	public static function determinePostFilename($upload_folder)
	{
		
		// Regular multipart/form-data upload.
		// @see https://docs.dropzone.dev/getting-started/setup/server-side-implementation
		$filename = $_FILES['file']['name'];
		
		// This isn't really used, but is left here in case backwards compatibility issues arise
		if(isset($_POST['prefix']))
		{
			$filename = $_POST['prefix'].$filename;
		}
		
		// Go ahead to find out the unique filename for the future
		$future_file = new TCm_File(false, $filename, $upload_folder, TCm_File::PATH_IS_FROM_SERVER_ROOT);
		return $future_file->uniqueFilename();
	}
	
	/**
	 * validates the posted image sizes
	 * @return void
	 */
	public static function validatePostMinimumImageSizes()
	{
		$model = new TCm_Model(false);
		if(isset($_POST['minimum_width']) && isset($_POST['minimum_height']))
		{
			try
			{
				$min_width = (int)$_POST['minimum_width'];
				$min_height = (int)$_POST['minimum_height'];
				
				list($image_width, $image_height) = getimagesize($_FILES['file']['tmp_name']);
				if($image_width > 0 && $image_height > 0
					&& $min_width > 0 && $min_height > 0
				)
				{
					if($image_width < $min_width || $image_height < $min_height)
					{
						header("HTTP/1.0 400 Bad Request");
						
						$message = "Image too small. Must be at least " . $min_width.'px wide';
						if($_POST['minimum_height'] > 0)
						{
							$message .= ' and '. $min_height.'px tall';
							
						}
						echo $message;
						exit();
						
					}
				}
			}
			catch(Exception $e)
			{
			
			}
			
		}
	}
	
	/**
	 * // Moves a posted tmp file into its new home
	 * @param string $upload_folder
	 * @param string $filename
	 * @return TCm_File
	 */
	public static function movePostTmpFile($upload_folder, $filename)
	{
		if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_folder.$filename))
		{
			$file = new TCm_File(false, $filename, $upload_folder, TCm_File::PATH_IS_FROM_SERVER_ROOT);
			if(!$file)
			{
				header("HTTP/1.0 400 Bad Request");
				echo "File upload error. File not created.";
				exit();
				
			}
			
			return $file;
		}
		else
		{
			header("HTTP/1.0 400 Bad Request");
			echo "File could not be moved saved.";
			exit();
			
		}
		
	}
	
	/**
	 * Handles photo scaling with a post value
	 * @param TCm_File $file
	 * @return TCm_File
	 */
	public static function handlePostPhotoScaling($file)
	{
		// Only turns on if there is a scaling value and it's an image
		if($file->hasImageExtension())
		{
			
			$photo = new TCv_Image('original_photo', $file);
			
			// Generate a scaled-down version and use it from now on
			$file = $photo->saveScaledDownVersion(TC_getConfig('upload_resize_width'));
			//$return_filename = $new_file->filename();
			$photo->addConsoleMessage('File Scaled Down ');
			
			
		}
		
		return $file;
	}
	
	/**
	 * Handles the post cropping of a photo, using a file provided
	 * @param TCm_File $file
	 * @return TCm_File
	 */
	public static function handlePostCropping($file)
	{
		// Only turns on if there is a cropping value and it's an image
		if($file->hasImageExtension() && isset($_POST['cropper_ratio']))
		{
			$ratio = (float)$_POST['cropper_ratio'];
			
			if($ratio > 0)
			{
				// Use TCv_Image to generate the cropped image, including the cache
				$photo = new TCv_Image('original_photo', $file);
				
				$cropper_width = (int)$_POST['cropper_width'];
				$cropper_height = (int)$_POST['cropper_height'];
				
				if($cropper_width > 0 && $cropper_height > 0)
				{
					$photo->crop($cropper_width, $cropper_height);
					
				}
				else
				{
					$photo->cropWithRatio($ratio);
					
				}
				
				// Generate the cropped filename
				$cropped_filename = $file->filename() . '{CROP,' . implode(',', $photo->cropValues()) . '}';
				
				// Update the file
				$file = new TCm_File('file', $cropped_filename, $file->pathFromServerRoot(), TCm_File::PATH_IS_FROM_SERVER_ROOT);
			}
		}
		
		return $file;
	}
	
	/**
	 * Generates the preview based on a post value
	 * @param TCm_File $file
	 * @return TCv_FilePreview
	 */
	public static function generatePostPreview($file)
	{
		$preview_class = 'TCv_FilePreview';
		if( isset($_POST['preview_class_name'])
			&& $_POST['preview_class_name'] != $preview_class
			&& class_exists($_POST['preview_class_name']))
		{
			$preview_class = $_POST['preview_class_name'];
		}
		
		/** @var TCv_FilePreview $preview */
		$preview = ($preview_class)::init($file);
		$preview->addClass('file_preview_box');
		$preview->addClass('ui-sortable-handle'); // add it just in case we're doing sorting
		$preview->showCloseLink();
		
		
		return $preview;
	}
	
	/**
	 * Generates the response array
	 * @param TCm_File $file
	 * @param TCv_FilePreview $preview
	 * @return array
	 */
	public static function generatePostOutputArray($file, $preview)
	{
		
		if($file->publiclyVisible())
		{
			$filepath = $file->filenameFromSiteRoot();
		}
		else
		{
			$filepath = $file->filenameFromServerRoot();
		}
		
		
		return [
			'filename' => $file->filename(),
			'extension' => $file->extension(),
			'publicly_visible' => $file->publiclyVisible(),
			'filepath' => $filepath,
			'preview' => $preview->html(),
		
		];
	}
}
?>