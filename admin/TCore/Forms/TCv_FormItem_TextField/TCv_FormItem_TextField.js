class TCv_FormItem_TextField {

	element = null;
	element_row = null;
	numbers_strings = ['0','1','2','3','4','5','6','7','8','9'];


	constructor(element, values) {

		this.element = element;
		this.element_row = element.closest('.table_row');

		this.element.addEventListener('keydown', (e => {this.keyPressed(e)}));
		this.element.addEventListener('paste', (e => {this.pasteHandler(e)}));
	}

	/**
	 * Event handler for the keypress event, which deals with the different scenarios for processing values.
	 * @param event
	 */
	keyPressed(event) {


		// Ignore anything that is a system command like copy/paste/refresh
		if(event.metaKey)
		{
			return;
		}


		// Detect Control and action keys and always ignore them
		let control_keys = [8,0,9,13,37,38,39,40,16,17,18,91 ];
		if(control_keys.indexOf(event.which) >= 0)
		{
			return;
		}

		// Handle if we require an integer
		if(this.element_row.classList.contains('integer'))
		{
			let number = event.which;

			// Main numbers 48 (0) through 57 (9)
			if(number >= 48 && number <= 57)
			{
				return;
			}

			// Deal with the number pad, which is 96 (0) through 105 (9)
			else if(number >= 96 && number <= 105)
			{
				return;
			}
			else // not a number, don't let the key event happen and get out
			{
				event.preventDefault();
				return;
			}


		}

		// Don't allow spaces to be typed
		if(this.element_row.classList.contains('no_spaces'))
		{
			if(event.which === 32) // spacebar
			{
				event.preventDefault();
				return;
			}

		}

		if(this.element_row.classList.contains('numbers_spaces'))
		{
			// only permit spacebar and numbers
			if(event.key !== ' ' && ! ( event.key >= 0 && event.key <= 9) )
			{
				event.preventDefault();
				return;
			}
		}


	}


	/**
	 * Detects paste events and ensure only the correct valeus get pasted
	 * @param event
	 */
	pasteHandler(event) {
		let pasted_data = event.clipboardData.getData('text');

		if(this.element_row.classList.contains('integer'))
		{
			pasted_data = pasted_data.replace(/\D/g, ''); // remove non-numbers
			this.element.value = pasted_data;
			event.preventDefault();
			return;
		}

		if(this.element_row.classList.contains('no_spaces'))
		{
			pasted_data = pasted_data.replace(/ /g, ''); // remove spaces
			this.element.value = pasted_data;
			event.preventDefault();
			return;
		}



	}
}