<?php
/**
 * Class TCv_FormItem_TextField
 *
 * A text field form item which creates a <input type="text"> field
 */
class TCv_FormItem_TextField extends TCv_FormItem
{
	protected $auto_complete = true; // [bool] = Indicate if autocomplete should be used
	protected $max_length = false; // [bool] = Indicate if autocomplete should be used
	protected $field_type = 'text'; 
	protected $js_required = false;
	/**
	 * TCv_FormItem_TextField constructor.
	 * @param string $id The ID for the field
	 * @param string $title The title shown for the field
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);

		$this->type = 'normal';


	}
	
	/**
	 * Sets the field type, which overrides the default value of "text"
	 * @param string $type
	 */
	public function setFieldType($type)
	{
		$this->field_type = $type;
	}
	
	/**
	 * Returns the view for the field, which is separate from the full container for this form item
	 * @return TCv_View
	 */
	public function fieldView()
	{
		$id = false;
		if($this->show_id && $this->attributeID() !== false)
		{
			$id = $this->attributeID();
		}
		
		$input = new TCv_View($id);
		$input->setTag('input');
		$input->setAttribute('type', $this->field_type);
		$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		$input->setAttribute('value', $this->currentValue());
		$input->addClassArray($this->form_element_classes);
		$input->addDataValueArray($this->form_element_data_values);
		
		if($this->is_required == 1)
		{
			$input->setAttribute('required', 'required');
		}
		
		if($this->placeholder_text)
		{
			$input->setAttribute('placeholder', $this->placeholder_text);
		}
		
		if(!$this->auto_complete)
		{
			$input->setAttribute('autocomplete', 'off');
		}
		
		if($this->max_length > 0)
		{
			$input->setAttribute('maxlength', htmlspecialchars($this->max_length));
		}

		if($this->disabled)
		{
			$input->setAttribute('disabled','disabled');
		}

		foreach($this->attributes as $name => $value)
		{
			$input->setAttribute($name,$value);
		}


		return $input;
	}
	
	/**
	 * Extend functionality to ensure js is triggered
	 */
	public function setIsInteger()
	{
		parent::setIsInteger();
		$this->js_required = true;
		$this->setFieldType('number');
		$this->setAttribute('step','1');
	}
	
	/**
	 * Restricts the input to only have numbers and spaces
	 */
	public function setAsNumbersAndSpacesOnly()
	{
		$this->js_required = true;
		$this->addClass('numbers_spaces');
		$this->addProcessMethodCall('validateIsNumbersAndSpaces',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		
	}
	
	/**
	 * Restricts the input to only permit URL safe characters
	 */
	public function setAsRequiresURLSafe()
	{
		$this->js_required = true;
		//$this->addClass('numbers_spaces');
		$this->addProcessMethodCall('validateAsURLSafe',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		
	}
	
	/**
	 * Returns the HTML
	 * @return string
	 */
	public function html()
	{
		$this->attachView($this->fieldView());

		if($this->js_required)
		{
			$this->addClassJSFile('TCv_FormItem_TextField');
			$this->addClassJSInit('TCv_FormItem_TextField');
		}
		return parent::html();
	}
	
	
	/**
	 * Disables the auto complete for the text field
	 */
	public function disableAutoComplete()
	{
		$this->auto_complete = false;
	}
	
	/**
	 * Sets a maximum length for this field
	 * @param $length
	 */
	public function setMaxLength($length)
	{
		$this->max_length = $length;
	}

	/**
	 * Extends the no spaces validation to actively process the field
	 * @param bool $force_remove_on_processing Indicate if any spaces in the values should be forcibly removed before
	 * processing. This will handle any spaces that make it through to the server-side processing and ensure they
	 * don't trigger an error, but just get deleted first.
	 */
	public function setNoSpaces($force_remove_on_processing = false)
	{
		// Turns on the required jS
		$this->js_required = true;

		// Add the remove spaces process call first
		if($force_remove_on_processing)
		{
			$this->addProcessMethodCall('deleteSpaces',
										TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
										TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
										TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		}

		// Calls the parent to enforce no spaces that get through still trigger an error
		parent::setNoSpaces();


	}

	/**
	 * Sets this field to be an email which enables a server-side validation to do basic format checking
	 */
	public function setIsEmail()
	{
		$this->setFieldType('email');
		$this->addProcessMethodCall('validateEmailFormat',
									TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
									TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
									TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO);
	}
	
	/**
	 * Validates if an email being provided is a valid one to use. This method is called during the validation
	 * process of a form. It checks if anyone other than the this user has this email address. If so, then using this
	 * email won't work.
	 *
	 * @return TCm_FormItemValidation
	 */
	public function validateEmailFormat()
	{
		$validation = new TCm_FormItemValidation($this->id(), 'Validate Email Format');
		
		$text = new TCu_Text($this->submittedValue());
		$ignore_empty = true;
		if(!$text->checkValidEmail($ignore_empty))
		{		
			$validation->failFieldWithMessage($this->title(), 'Email must be a properly formatted.');
		}
		
		return $validation;
		
	}
	
}
?>