<?php
/**
 * Class TCv_FormItem_Checkbox
 */
class TCv_FormItem_Checkbox extends TCv_FormItem
{
	protected $form_value = '1'; // [string] = The value that is assigned to this checkbox. The default value is '1' which translates to a yes/no equivalent
	protected $description = false; //[string] = The label to be attached to this checkbox
	
	protected $checkbox_label = false; //[string] = The label to be attached to this checkbox
	protected $show_label = false;
	
	/**
	 * TCv_FormItem_Checkbox constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		$this->type = 'normal';
		
		// change defaults for all the values to false
		$this->default_value = false;
		
		$this->checkbox_label = new TCv_View();
		$this->checkbox_label->setTag('label');
		$this->checkbox_label->addClass('checkbox_label');
		$this->checkbox_label->setAttribute('for',htmlspecialchars($this->id()));
		
		// Adding button restyle
		$styling_target = new TCv_View();
		$styling_target->setTag('span');
		$styling_target->addClass('button_restyle');
		
		$icon = new TCv_View();
		$icon->addClass('checkmark');
		$icon->addClass('fas fa-check');
		$styling_target->attachView($icon);
		$this->checkbox_label->attachView($styling_target);
		
	}
	
	//////////////////////////////////////////////////////
	//
	// FORM ELEMENT
	//
	//////////////////////////////////////////////////////


	/**
	 * @return string
	 */
	public function html()
	{
		$input = new TCv_View($this->id());
		$input->setTag('input');
		//$input->addClassArray($this->classes());
		$input->setAttribute('type', 'checkbox');
		$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		$input->setAttribute('value', htmlspecialchars($this->formValue()));
		$input->addClassArray($this->form_element_classes);
		$input->addDataValueArray($this->form_element_data_values);
		
		if($this->disabled)
		{
			$input->setAttribute('disabled','disabled');
		}
		
		if($this->isRequired())
		{
			$input->setAttribute('required','required');
			
		}
		
		// deal with checked indicator
		if($this->currentValue())
		{
			$input->setAttribute('checked', 'checked');
		}
		$this->attachView($input);
		
		// label
		//if($this->show_label)
		//{
			$this->attachView($this->checkbox_label);
		//}
		
		return parent::html();
	}


	//////////////////////////////////////////////////////
	//
	// FORM VALUE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the value for this form item
	 *
	 * @return string
	 */
	public function formValue()
	{
		return $this->form_value;
	}
	
	/**
	 * Sets the value for this form item. This is the value that is passed to the form, and not the current state of checked or unchecked.
	 * @param string|mixed $value
	 */
	public function setFormValue($value)
	{
		$this->form_value = $value;
	}


	//////////////////////////////////////////////////////
	//
	// LABEL
	//
	//////////////////////////////////////////////////////


	/**
	 * Switches the setup to use the title for the label on this form. In this scenario, the title is set to blank
	 * and then used as a label next to the checkbox.
	 */
	public function useTitleForLabel()
	{
		$this->setLabel($this->title());
		$this->setShowTitleColumnContent(false);
	}
	
	/**
	 * Returns the checkbox label for this form item, which is different than the label that is generated as part of
	 * the form layout.
	 *
	 * @return bool|TCv_View
	 */
	public function checkboxLabel()
	{
		return $this->checkbox_label;
	}
	
	/**
	 * Sets the label for this form item
	 * @param string|TCv_View $label
	 */
	public function setLabel($label)
	{
		$styling_target = new TCv_View();
		$styling_target->setTag('span');
		$styling_target->addClass('button_text');
		
		if($label instanceof TCv_View)
		{
			$styling_target->attachView($label);
		}
		else
		{
			$styling_target->addText($label);
		}
		
		
		$this->checkbox_label->attachView($styling_target);
		$this->show_label = true;
	}
	
	/**
	 * Attaches a view to the label
	 * @param TCv_View $view
	 */
	public function attachViewToLabel($view)
	{
		$this->checkbox_label->attachView($view);
		$this->show_label = true;
	}
	
	/**
	 * Extends the addText function to add to the label instead of the view itself
	 *
	 * @param string $label
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 */
	public function addText($label, $disable_template_parsing = false)
	{
		$this->checkbox_label->addText($label, $disable_template_parsing);
		$this->show_label = true;
	}

	//////////////////////////////////////////////////////
	//
	// DESCRIPTION
	//
	//////////////////////////////////////////////////////

	/**
	 * returns the description
	 * @return bool|string
	 */
	public function description()
	{
		return $this->description;
	}
	
	/**
	 * Sets the description for this form item
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	//////////////////////////////////////////////////////
	//
	// SUBMITTED VALUE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the value that was submitted to the form. If a form item consists of a more complicated layout such as
	 * multiple form elements, this function should be overridden to ensure that the proper values are indicated as
	 * the submitted value.
	 *
	 * @return int
	 */
	public function submittedValue()
	{
		if(isset($_POST[$this->id]) && $_POST[$this->id] > 0)
		{
			return 1;
				
		}
		else
		{
			return 0;
		}

	}
	
}
?>