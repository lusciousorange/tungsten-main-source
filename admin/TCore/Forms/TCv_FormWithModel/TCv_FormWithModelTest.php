<?php
class TCv_FormWithModelTest extends TC_FormWithModelTestCase
{
	
	
	public function testAttachView()
	{
		$form = $this->resetFormWithModel('TCm_Model');
		$num_views = count($form->formItems());
		
		
		$form_item = new TCv_FormItem_Hidden('hidden','Hidden');
		$form_item->setValue('no');
		$form->attachView($form_item);
		
		
		$this->assertEquals($num_views+1, count($form->formItems()));
		
		
	}
	
	
}
