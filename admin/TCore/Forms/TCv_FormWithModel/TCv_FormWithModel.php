<?php
/**
 * Class TCv_FormWithModel
 *
 * A form that is based on a model. A regular form has no easy way of being processed. This form is based on a model,
 * which provides some structure on how the data is saved.
 */
class TCv_FormWithModel extends TCv_Form
{
	protected $model = false;
	protected $model_name = '';
	protected $ignore_model_edit_validation = false;
	
	public static $show_form_success_message = true;
	public static $show_form_failure_message = true;
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_FormWithModel');
	
	
	/**
	 * TCv_FormWithModel constructor.
	 * @param TCm_Model|string $model Providing a model indicates an editor for that model. Providing a model name
	 * indicates that it's a creator for that model. Providing an empty string will work, but only if the required
	 * model name is set using `formRequiredModelName()`.
	 * @param bool $load_form_tracking (Optional) Default true;
	 */
	public function __construct($model, $load_form_tracking = true)
	{
		$this->validateFormInput($model);
		
		// Pass in false to avoid triggering the form tracking, we manually set it to avoid a double load
		parent::__construct($this->model_name, false);
		$this->load_form_tracking = $load_form_tracking;
		
		if($this->model_name == '')
		{
			// Exit before attempting to load anything else
			// Bad data or a blank instantiation
			return;
		}
		
		// If we have a model, then we load the form tracker through setModel()
		if($model instanceof TCm_Model)
		{
			$this->setModel($model);
		}
		elseif($this->load_form_tracking)
		{
			$this->loadFormTracker(get_called_class() . '-' . $this->model_name); // form tracker
		}

		// Update table values using the model
		if($model instanceof TCm_Model)
		{
			$this->setPrimaryTableAndKey($model::tableName(), $model::$table_id_column);// needs to be saved for other form calls
			$this->setPrimaryValue($model->id());
		}
		else
		{
			$model_name = $this->model_name;

			$this->setPrimaryTableAndKey($model_name::tableName(), $model_name::$table_id_column);// needs to be saved for other form calls
			$this->setPrimaryValue(false);
		}
		$this->formTracker()->setModelName($this->model_name);
		
		// deal with the button
		$this->calculateButtonText();
		
		// default to the default process script
		$this->setAction(TC_getConfig('TCore_path').'/Forms/TCc_FormProcessorWithModel/process_form_with_model_tungsten.php');
		
	}
	
	/**
	 * Function to validate the form input for all the scenarios that can arise. Anything that doesn't pass the
	 * validation will trigger an error. If successful, it will also save the model name value for the form.
	 *
	 * This takes into account the required model name, possible values, etc
	 * @param $input
	 * @return void
	 */
	protected function validateFormInput($input)  : void
	{
		// Get the required form model, used in error messages as well as validation
		$form_required_model_name = static::formRequiredModelName();
		$has_required_model_name = is_string($form_required_model_name);
		
		// Validate that the required model name exists
		if($has_required_model_name)
		{
			if(!class_exists($form_required_model_name))
			{
				$error_message = "Bad form configuration: ";
				$error_message .= '<em>`'.get_called_class().'`</em> requires a model of type ';
				$error_message .= ' of type <em>`'.$form_required_model_name.'`</em> which DOES NOT EXIST';
				TC_triggerError($error_message);
			}
		}
		
		// 1. EDITOR MODE
		// Model provided, so it's an editor
		if($input instanceof TCm_Model)
		{
			// Save the model name
			$this->model_name = get_class($input);
			
			if($has_required_model_name)
			{
				// Validate the model. We have one but it's the wrong class
				if(!($input instanceof $form_required_model_name))
				{
					$error_message = "Bad form configuration (Editor): ";
					$error_message .= '<em>`'.get_called_class().'`</em> expects a model of type ';
					$error_message .= '<em>`'.$form_required_model_name.'`</em>. Input model was a ';
					$error_message .= '<em>`'.$this->model_name.'`</em>.';
					
					TC_triggerError($error_message);
				}
			}
		}
		
		// 2. CREATOR MODE
		// This is a string, but only certain values are acceptable
		elseif(is_string($input))
		{
			$input = trim($input);
			
			// Option 1, we have a class name, which is how Tungsten has set creators for years
			if(class_exists($input))
			{
				// Save the model name
				$this->model_name = $input;
				
				// Additional validation
				if($has_required_model_name)
				{
					if($input != $form_required_model_name && !is_subclass_of($input, $form_required_model_name))
					{
						$error_message = "Bad form input: ";
						$error_message .= '<em>`'.get_called_class().'`</em> expects string or a model';
						$error_message .= ' of type <em>`'.$form_required_model_name.'`</em>';
						
						$error_message .= '. Input type was an invalid string class name of <em>`'.htmlentities($input)
							.'`</em>';
						TC_triggerError($error_message);
					}
				}
			}
			elseif($input == '' && $has_required_model_name)
			{
				// Creator mode, no issue here, just need to set the model name
				$this->model_name = $form_required_model_name;
			}
			else // Bad input string
			{
				$error_message = "Bad form input: ";
				$error_message .= '<em>`'.get_called_class().'`</em> expects string or a model';
				$error_message .= ' of type <em>`'.$form_required_model_name.'`</em>';
				
				$error_message .= '. Input type was an invalid string <em>`'.htmlentities($input).'`</em>';
				TC_triggerError($error_message);
			}
		}
		
		// 3. BAD INPUT : Not a string, nor a model
		else
		{
			$error_message = "Bad form input: ";
			$error_message .= '<em>`'.get_called_class().'`</em> expects string or a model';
			if($has_required_model_name)
			{
				$error_message .= ' of type <em>`'.$form_required_model_name.'`</em>';
			}
			$error_message .= '. Input type was <em>`'.gettype($input).'`</em>';
			TC_triggerError($error_message);
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// MODEL
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the name of the model for this form. If set, this is enforced and saves the effort of having to pass
	 * in strings into the constructors. Instead the models can be null or a string.
	 * @return class-string<TCm_Model>|null The name of the class that for this form
	 */
	public static function formRequiredModelName(): ?string
	{
		return null;
	}

	/**
	 * Sets the model for this form. This is often done through the contructor, however it can be re-applied if ever necessary.
	 * @param TCm_Model $model
	 */
	public function setModel($model)
	{
		if($model instanceof TCm_Model)
		{
			// generate new form tracker for this ID
			$this->loadFormTracker(get_called_class().'_'.$model->id());
			$this->setIsEditor(true);
			$this->model = $model;
		}
		
	}
	
	/**
	 * Turns off a validation the checks that when editing a mdoel, that the userCanEdit() method rturns true. By default
	 * this is always turned on, and turning this off can expose data to editing that shoudlnt'be.
	 */
	public function ignoreModelEditValidation()
	{
		$this->ignore_model_edit_validation = true;
		
	}
	
	/**
	 * Returns the model for this form
	 * @return bool|TCm_Model
	 */
	public function model()
	{
		return $this->model;
	}
	
	/**
	 * Returns the model name for this form
	 * @return string
	 */
	public function modelName()
	{
		return $this->model_name;
	}
	
	
	/**
	 * Returns the main model for this form
	 * @return TCm_Model
	 */
	public function primaryModel()
	{
		return $this->model();
	}
	
	/**
	 * Extend functionality of the attachView method to set the editor value using the model values.
	 * @param bool|TCv_FormItem|TCv_View $form_item
	 * @param bool $prepend
	 */
	public function attachView($form_item, $prepend = false)
	{
		if($form_item === false || is_null($form_item))
		{
			return;
		}
		TC_assertObjectType($form_item, 'TCv_View');
		
		$views_to_add = array();
		$views_to_add[] = $form_item;
		
		
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			if($form_item instanceof TCv_FormItem)
			{
				$localized_fields = $this->localizedFieldsForFormItem($form_item);
				$views_to_add = array_merge($views_to_add, $localized_fields);
			}
			elseif($form_item instanceof TCv_FormItem_Group)
			{
				$form_item->processLocalizationFieldsForForm($this);
			}
		}
		
		
		// after we have a full set, process them all
		foreach($views_to_add as $form_item_view)
		{
			if($this->isEditor())
			{
				if( $form_item_view instanceof TCv_FormItem )
				{
					$model_value = $this->model()->valueForProperty($form_item_view->id());
					if($model_value == '')
					{
						$form_item_view->setEditorValue( $model_value );
					}
					else
					{
						
						// If the item uses multiple, we handle it here since it might need
						// values from multiple tables
						if ($form_item_view->useMultipleSetting() !== false)
						{
							// this means we need to pass in an array
							if ($form_item_view->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS)
							{
								$values = array();
								$i = 1;
								
								while($this->model()->propertyExists($form_item_view->id().'_'.$i))
								{
									$value = $this->model()->valueForProperty($form_item_view->id().'_'.$i);
									$values[$i] = trim($value);
									$i++;
								}
								
								$form_item_view->setEditorValue($values);
							}
							else
							{
								
								$form_item_view->setEditorValue($model_value);
							}
						}
						else
						{
							$form_item_view->setEditorValue($model_value);
						}
					}
					
				}
				elseif($form_item_view instanceof TCv_FormItem_Group)
				{
					foreach($form_item_view->formItems() as $group_form_item)
					{
						if($group_form_item instanceof TCv_FormItem)
						{
							$group_form_item->setEditorValue( $this->model()->valueForProperty($group_form_item->id()) );
						}
						
					}
				}
			}
			
			parent::attachView($form_item_view, $prepend);
			
			
		}
		
	}
	
	/**
	 * Returns an array of localized views for a form item, which is how localization handles it.
	 * @param TCv_FormItem $form_item
	 * @return array
	 */
	public function localizedFieldsForFormItem(TCv_FormItem $form_item) : array
	{
		$views = [];
		$model = $this->modelName();
		$db_field = $form_item->id();
		if($model && is_string($db_field) && $model::schemaFieldIsLocalized($db_field))
		{
			// Go through the non-default languages and clone the fields with other language values
			$localization = TMm_LocalizationModule::init();
			foreach($localization->nonDefaultLanguages() as $language => $language_settings)
			{
				
				$form_item_language = clone $form_item;
				$form_item_language->setID($form_item_language->id().'_'.$language);
				$form_item_language->setTitle($form_item_language->title().' ('.$language_settings['title'].')');
				$form_item_language->addClass('localization');
				$form_item_language->addClass('localization_'.$language);
				
				
				if($default_value_localized = $form_item->defaultValueLocalized($language))
				{
					$form_item_language->setDefaultValue($default_value_localized);
				}
				
				// handle localized require
				if(!TC_getModuleConfig('localization','enforce_required'))
				{
					$form_item_language->disableRequired();
				}
				
				$views[] = $form_item_language;
				
				
				
				
			}
		}
		
		return $views;
	}
	
	/**
	 * Calculates the item name from the model values and if the form is an editor. This function is automatically called
	 * when the form is instantiated and can be overridden with a new name or a new submit button altogether.
	 * @return bool|string
	 */
	public function calculateButtonText()
	{
		$name = $this->model_name;
		if($this->isEditor())
		{
			$this->button_text = TC_localize('Update','Update');
		}
		else
		{
			$this->button_text = TC_localize('Create','Create');
		}
		
		$this->button_text .= ' '.$name::modelTitleSingularLower();
		return $this->button_text;
	}
	
	
	/**
	 * Performs the priary data action, which means it calls updateDatabase on the form processor
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @uses TCc_FormProcessorWithModel::updateDatabase()
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// Perform the database update
		$form_processor->updateDatabase();
	}

	/**
	 * @param bool $show_form
	 * @return string
	 */
	public function html($show_form = true)
	{
		
		$tracker_form_name = new TCv_FormItem_Hidden('tracker_form_name','Tracker Form');
		$tracker_form_name->setvalue(get_called_class());
		$tracker_form_name->setSaveToDatabase(false);
		$tracker_form_name->setShowID(false);
		$this->attachView($tracker_form_name);
		
		// enforce no editing
		if($this->isEditor())
		{
			// only perform this check if we're logged in	
			if(TC_currentUser() && !$this->model()->userCanEdit() && !$this->ignore_model_edit_validation)
			{
				$show_form = false;
				$model_name = $this->modelName();
				//$model_title = $model_name::$model_title;
				$access_denied = TSv_AccessDenied::init('form_editor_no_editing_permitted');
				$access_denied->setMainTitle('Permission to edit this item is denied');
				//$access_denied->setExplanation('You do not have permission to edit this item.');
				$this->hideSubmitButton();
				parent::attachView($access_denied);

			}
		}
		return parent::html($show_form);
	}

	
	
}


define('TCv_FORM_EDITOR', true);
define('TCv_FORM_NOT_EDITOR', false);
	
?>