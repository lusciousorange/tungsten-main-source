/**
 * A function to clean the TinyMCE content that is pasted in.
 * @see https://www.tiny.cloud/docs/tinymce/6/copy-and-paste/#paste_postprocess
 * @see https://www.tiny.cloud/blog/remove-style/
 */
function tungstenTinyMCEPasteProcessor(editor, args) {

	// Run the content through an element to process
	let container = document.createElement('div');
	container.innerHTML = args.content;

	let elements = container.querySelectorAll('*');
	elements.forEach(node => {

		let attributes_to_delete = []; // track separately due to HTML updating and skipping

		for(let node_attribute of node.attributes)
		{
			let attribute_name =node_attribute.name;
			if(attribute_name !== 'src' && attribute_name !== 'href')
			{
				attributes_to_delete.push(attribute_name);
			}
		}

		// Loop through and delete the attributes
		attributes_to_delete.forEach(attribute_name => {
			node.removeAttribute(attribute_name);
		});

	});

	// Remove tag names that we don't want. Specifically divs and sections
	let content = container.innerHTML;
	content = content.replaceAll('<div>','');
	content = content.replaceAll('<\/div>','');
	content = content.replaceAll('<section>','');
	content = content.replaceAll('<\/section>','');

	// Replace BAD characters non-breaking spaces
	// double-spaces in editors get converted into nbsp, which get saved as unicode
	// Replace them with regular spaces
	content = content.replace(/(&nbsp;|\u00A0|\xA0| )/g, ' ');
	content = content.replaceAll('“','"');
	content = content.replaceAll('”','"');

	// Save the content back
	args.content = content;

}