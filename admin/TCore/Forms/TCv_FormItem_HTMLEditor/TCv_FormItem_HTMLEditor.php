<?php

/**
 * Class TCv_FormItem_HTMLEditor
 */
class TCv_FormItem_HTMLEditor extends TCv_FormItem
{
	protected $value = '';
	
	/**
	 * THe config array for tiny_mce, which is then passed into an init function, but this can be modified for each
	 * @var array
	 */
	protected $tiny_mce_config = [
		'selector'          => 	".tinymce_init textarea",
		'theme'             => 	"silver",
		'plugins'           => 	"paste fullscreen anchor table code image  media charmap link lists advlist ",

		'block_formats'     =>  'Paragraph=p; Heading 2=h2; Heading 3=h3; Heading 4=h4;',
		'toolbar1'          =>  "formatselect | bold italic underline strikethrough superscript subscript | alignleft aligncenter alignright | table bullist numlist | link image | code fullscreen",
		'statusbar'         =>  false,
		'menubar'           =>  false,
		'browser_spellcheck'=> 	true,


		// Paste settings to not allow drag-and-drop and default as text
		'paste_block_drop'  => true,
	//	'paste_as_text'     =>  true,
		
		// paste_preprocess set in configureSelectedTinyMCE() function since it must pass a function name
	//	'paste_postprocess'  => 'tungstenTinyMCEPastePostProcessor',

		// File Manager
		'external_filemanager_path'=> "/admin/TCore/Forms/TCv_FormItem_HTMLEditor/filemanager/",
		'filemanager_title' => "File Manager" ,
		'external_plugins'  => [
					"filemanager" =>  "/admin/TCore/Forms/TCv_FormItem_HTMLEditor/filemanager/plugin.min.js"],
		'relative_urls'     =>  false,
		'link_list'         =>  "/admin/pages/do/menu-list-tinyMCE",
		'entity_encoding'   =>  'raw', // ensure all entities are stored in their non-entity form
	];
	/**
	 * TCv_FormItem_HTMLEditor constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
		
		// This must be enabled for the HTML editor since it obviously needs to show HTML
		$this->setAsAllowsHTML();
		
		$this->type = 'normal';
		
		$this->addJSFile('tiny_mce_functions',$this->classFolderFromRoot('TCv_FormItem_HTMLEditor',true).'/tinymce_functions.js');
		
		$this->addClassCSSFile('TCv_FormItem_HTMLEditor');
		
		$this->addJSFile('tinymce', '/vendor/tinymce/tinymce/tinymce.min.js?v'.TCv_Website::gitVersion());
		
		$this->addClass('tiny_mce');
		
		$folders = array(
			'/assets/',
			'/assets/content/',
			'/assets/content_thumbs/',
			);
			
		foreach($folders as $folder_path)
		{
			if(!is_dir($_SERVER['DOCUMENT_ROOT'].$folder_path))
			{
				mkdir($_SERVER['DOCUMENT_ROOT'].$folder_path,0755);
			}
			
		}

		$this->addProcessMethodCall('cleanupHTML', TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD,
									TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE, TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO);


	}
	
	/**
	 * Sets the init path for TinyMCE. The path must point to a JS file that properly inititializes TinyMCE with the necessary settings
	 *
	 * @param string $path
	 * @param string|bool $mce_class_name The classname to differentiate the different types of initializers needed.
	 * This must match the selector options in the init code
	 * @deprecated No longer sets files to load, instead modifies the base
	 */
	public function setTinyMCEInitPath($path, $mce_class_name = false)
	{
		//No longer used. Modify or use one of the presets
	}

	/**
	 * Changes the editor to use more basic settings that disables things like image uploads, headings, and alignment.
	 */
	public function useBasicEditor()
	{
		$this->tiny_mce_config['height'] = 150;
		$this->tiny_mce_config['plugins'] = "paste advlist autolink lists link table code fullscreen";
		$this->tiny_mce_config['toolbar1'] ="formatselect | bold italic underline superscript subscript | bullist
		numlist | link code fullscreen ";

	}
	
	
	/**
	 * Changes the editor to use more basic settings that disables things like image uploads, headings, and alignment.
	 */
	public function useSuperBasicEditor()
	{
		$this->tiny_mce_config['height'] = 150;
		$this->tiny_mce_config['plugins'] = "advlist autolink lists link paste";
		$this->tiny_mce_config['toolbar1'] ="bold italic | link | bullist numlist";
		
	}
	
	/**
	 * Changes the editor to use the editor but with no photo and no expand option
	 */
	public function useTextEditor()
	{
		
		$this->tiny_mce_config['plugins'] = "fullscreen anchor table paste code image lists media charmap link advlist";
		$this->tiny_mce_config['toolbar1'] ="formatselect | bold italic underline strikethrough superscript subscript | table bullist numlist | link | code";
		
	}
	
	/**
	 * Sets a value in the TinyMCE config which can vary. This allows for one-off overrides of particular settings
	 * that we might want to add or remove.
	 * @param string $key
	 * @param $value
	 * @return void
	 */
	public function setTinyMCEConfig(string $key, $value): void
	{
		$this->tiny_mce_config[$key] = $value;
	}
	
	/**
	 * Sets the height for the editor.
	 * @param int $height
	 * @return void
	 */
	public function setHeight(int $height) : void
	{
		$this->setTinyMCEConfig('height', $height);
	}
	
	
	/**
	 * Manually sets the toolbar string for the editor
	 * @param string $toolbar_string
	 * @return void
	 */
	public function setTinyMCEToolbar(string $toolbar_string) : void
	{
		$this->setTinyMCEConfig('toolbar1', $toolbar_string);
	}
	
	/**
	 * Override the focus method to use tinyMCE methods
	 *
	 * @see https://stackoverflow.com/questions/23522824/perform-action-after-tinymce-rendered-in-dom
	 */
	public function focusOnLoad()
	{
		// Convoluted solution to focus on a HTML Editor when loading
		// Doc Ready event -> add editor event -> PostRender event
		$this->addJSLine($this->id() . '_focus',
		                 'tinyMCE.on("addeditor", function( event ){
		                 event.editor.on("PostRender", function(ed){this.focus()});}, true ); ', true);
		
	}
	
	////////////////////////////////////////////////
	//
	// STYLE CLEANING
	//
	////////////////////////////////////////////////

	/**
	 * Sets the processor to clear any inline styles
	 * @deprecated
	 * @see TCV_FormItem_HTMLEditor::cleanupHTML
	 */
	public function setClearInlineStyles()
	{
	}
	

	/**
	 * Clears any inline styles of the submitted value.
	 * @return TCm_FormItemValidation
	 */
	public function cleanupHTML()
	{
		$validation = new TCm_FormItemValidation($this->id, 'Cleanup HTML');

		$text = $_POST[$this->id];
		
		// Remove HTML Comments, which also catches Word XML
		$text = preg_replace('/<!--(.*)-->/Uis', '', $text);
		
		// Remove Common copy/paste properties
		$text = preg_replace('/mso-.+;/Ui', "", $text); // mso styles. Must go first to avoid bad regex
		$text = preg_replace('/font-family:.+;/Ui', "", $text);;
		$text = preg_replace('/font-size:.+;/Ui', "", $text);;
		$text = preg_replace('/text-autospace:.+;/Ui', "", $text);;
		$text = preg_replace('/color:.+;/Ui', "", $text);;
		$text = preg_replace('/background:.+;/Ui', "", $text);;
		$text = preg_replace('/class="Mso.+"/Ui', "", $text);;
		
		// Replace non-breaking spaces plus single and double quotes
		$text = str_replace('&nbsp;',' ', $text);
		$text = str_replace('&rsquo;',"'", $text);
		$text = str_replace('&lsquo;',"'", $text);
		$text = str_replace('&rdquo;','"', $text);
		$text = str_replace('&ldquo;','"', $text);
		
		$pattern = '/<([^>\s]+)[^>]*>(?:\s*(?:&nbsp;|&thinsp;|&ensp;|&emsp;|&#8201;|&#8194;|&#8195;)\s*)*<\/\1>/';
		$_POST[$this->id] = preg_replace($pattern, '',$text);

		
		return $validation;

	}


	////////////////////////////////////////////////
	//
	// RETURN
	//
	////////////////////////////////////////////////
	
	/**
	 * Method to apply the final selected TinyMCE configuration that has been set
	 */
	protected function configureSelectedTinyMCE()
	{
		// Generates a single line for instantiating the editor
		$this->addJSLine('js_tinymce_clear_old'.$this->id(),
		                 "if(window.tinymce.editors['".$this->id()."']) { window.tinymce.editors['".$this->id()."'].remove();}", true);
		
		// Update the selector to this one field
		// Done at this stage to account for ID changes that may occur
		$this->tiny_mce_config['selector'] = 'textarea#'.$this->id();
		
		$json = json_encode($this->tiny_mce_config);
		$json = rtrim($json, '}').',"paste_preprocess" : tungstenTinyMCEPasteProcessor}';
		$this->addJSLine('js_tinymce_init_'.$this->id(),
		                 'tinymce.init('.$json.');', true);
		
	}
	
	
	public function render()
	{
		// Deal with the set tiny_mce_name
		$this->configureSelectedTinyMCE();
		
		// Add the actual textarea element
		$input = new TCv_View(htmlspecialchars($this->id()));
		$input->setTag('textarea');
		$input->addClassArray($this->classes());
		$input->setAttribute('type', 'checkbox');
		$input->setAttribute('name', htmlspecialchars($this->fieldName()));
		$input->setAttribute('value', htmlspecialchars($this->value));
		$input->addText($this->currentValue(), true);
		$this->attachView($input);
	}
	
	
}
?>