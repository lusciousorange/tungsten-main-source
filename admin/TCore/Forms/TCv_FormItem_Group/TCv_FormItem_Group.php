<?php
/**
 * Class TCv_FormItem_Group
 *
 * A view used to group form items together for styling. This has no functional effect on the operation of the form.
 */
class TCv_FormItem_Group extends TCv_View
{
	protected $form_items = array();
	protected $title;
	/**
	 * TCv_FormItem_Group constructor.
	 * @param $id
	 * @param string $title
	 */
	public function __construct($id, $title = 'Group')
	{
		parent::__construct($id);
		
		$this->title = $title;
		// saves the views attached, re-adds them at the end
		$this->setHTMLCollapsing(false);
	}
	
	//////////////////////////////////////////////////////
	//
	// TITLE
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the title for this form item
	 *
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Sets the title for this form item
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	
	/**
	 * Returns the form items for this group
	 * @return TCv_View[]|TCv_FormItem[]
	 */
	public function formItems()
	{
		return $this->form_items;
	}
	
	public function removeAllFormItems()
	{
		$this->form_items = array();
	}
	
	/**
	 * Removes the form item with a particular ID
	 * @param string $id
	 */
	public function removeFormItemWithID($id)
	{
		unset($this->form_items[$id]);
	}
	
	/**
	 * Sets if the title column should be shown
	 *
	 * @param bool $show
	 */
	public function setShowTitleColumn($show)
	{
		// do nothing
	}

	/**
	 * Overrides the default attachView to force all items to stay within a container
	 * @param bool|TCv_View $form_item
	 * @param bool $skip_form_handling If true, it will actually attach the form. THis is usually automatically done
	 * by the TCv_Form that it's being attached to.
	 * @return bool|string|TCv_View|void
	 */
	public function attachView($form_item, $skip_form_handling = false)
	{
		// Handle a false form_item, which happens if there's nothing to show
		if(!$form_item)
		{
			return false;
		}
		if($form_item instanceof TCv_FormItem)
		{
			if($skip_form_handling)
			{
				parent::attachView($form_item);
			}
			else
			{
				$this->form_items[$form_item->id()] = $form_item;
			}
		}
		else
		{
			$this->form_items[$form_item->id()] = $form_item;
			parent::attachView($form_item);	
		}
		
		
	}
	
	/**
	 * Hqndles the localization of the fields in this group. This must be called from TCv_Form since this view doesn't know
	 * what form it belongs to. This will inject the fields into the list of form items.
	 *
	 * @param TCv_FormWithModel $form The form that is loading it, which requires a model since that's how localization is loaded.
	 * @return void
	 */
	public function processLocalizationFieldsForForm(TCv_FormWithModel $form) : void
	{
		// Start a fresh array that is built sequentially. If we find localized fields, then thse get added at the same
		// time, ensuring they sit below their counterparts
		$new_form_items = [];
		foreach($this->form_items as $id => $form_item)
		{
			$new_form_items[$id] = $form_item;
			$views = $form->localizedFieldsForFormItem($form_item);
			foreach($views as $form_item)
			{
				$new_form_items[$form_item->id()] = $form_item;
			}
			
		}
		
		$this->form_items = $new_form_items;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// VISIBILITY TOGGLING
	//
	// These are automated solutions for showing/hiding fields
	// based on the value of another field
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Enables the functionality that will show or hide this field based on the value of another field.
	 * @param string $field_id The field ID that will be used to trigger the showing/hiding of the visibility
	 * @param mixed $show_value The value that correlate to a "show value". Any other value will hide the
	 * field
	 */
	public function setVisibilityToFieldValue($field_id, $show_value)
	{
		$this->addDataValue('vis-toggle-show',$show_value);
		$this->addDataValue('vis-toggle-field', $field_id);
		
	}
}
?>