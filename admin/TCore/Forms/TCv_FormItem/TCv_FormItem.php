<?php
/**
 * Class TCv_FormItem
 *
 * A Tungsten Core Form item. Each form item is a specific type of element along with the associated information that is associated with a form element.
 */
abstract class TCv_FormItem extends TCv_View
{
	protected $title = 'Title'; // [string] = The title for this form item
	protected $form_field_ids = array();
	public 	  $empty_values_as_null = false;

	protected $help_text = ''; // [string] = The instructional text that each form item may have
	public 	  $is_required = false; // [bool] = Indicates if the item is required
	protected $placeholder_text = false; // [bool] = The placeholder text for this form
	protected $secondary_html = ''; // [string] = HTML that is associated with the form item
	protected $type = 'none'; // [string] = The type for this form item
	protected $form = false; // [TCv_Form] = The form that containe this item
	
	// VALUES
	protected $default_value = ''; // [string] = the default value for this item
	protected $default_values_localized = array();
	protected $editor_value = NULL; // [string] = The editor value assigned to this item
	protected $saved_value = NULL; // [string] = The saved value assigned to this item
	protected $saved_value_set = false; // [bool] = Tracks if the saved value has actuall been set
	protected $disabled = false;
	protected $field_name = null;
	
	// APPEARANCE
	protected $show_title_column = true; // [bool] = Indicates if the title column should be shown for this form item
	protected $show_title_column_content = true; // [bool] = Indicates if the content in the title column should be shown
	protected $is_valid = true; // [bool] = Indicates if there were any validation errors on this item
	protected $override_database_field_name = false; // [string= THe name used to override the filed name. Separate from the id
	protected $row_classes = array();
	// Database Saving Settings
	public    $save_to_database = true; // [bool] = Indicates if this item should be saved to the database if the
	// validation was successful
	
	protected $process_calls = array(); // [array] = The array of process calls that are required for this form item. Each entry in this array must be a function called during the validation process.
	protected $update_calls = array(); // [array] = The array of post update calls. 
	
	protected $views_after = array(); // [array] = The views to be added after the normal setup.
	protected $uses_container = true;

	protected $form_element_classes = array();
	protected $form_element_data_values = array();

	public $use_multiple = false;
	
	protected $row_data_values = array();
	
	protected bool $attach_outside_form_table = false;
	
	
	/** @var bool Indicates if this field should appear in searchable applied filters. Only has an effect in that
	 * particular interface.
	 */
	protected bool $show_in_searchable_applied_filters = true;
	
	/** @var bool Indicates if this field is clearable in searchable applied filters */
	protected bool $clearable_in_searchable_applied_filters = true;
	
	/**
	 * @var null|string $field_icon_code
	 * The code for the icon for this field is used
	 */
	protected $field_icon_code = null;

	/**
	 * The columns that are saved to pass to the form processor for consistency in how the form item is processed.
	 * The developer can manually add more to the list for each form_item by using `addSavedProcessorClassProperty()`.
	 *
	 */
	public $processor_class_properties = array(
		'use_multiple',
		'is_required' ,
		'save_to_database' ,
		'empty_values_as_null' ,

	);

	const DISABLE_VALIDATION = true;
	const USE_MULTIPLE_NUMBERED_COLUMNS = 'numbered_columns';
	const USE_MULTIPLE_PAIRING_TABLE = 'pairing_table';


	// calling a function that exists in a TCc_FormProcessor.
	const PROCESSING_IS_FORM_PROCESSOR_METHOD = 0;
	//define('TCv_FormItem_FORM_PROCESSOR_METHOD', 0);

	// calling a function that is a static method. Useful if it must be called even when there is no instance. ie: form creators
	const PROCESSING_IS_CLASS_STATIC_METHOD = 1;
	//define('TCv_FormItem_CLASS_STATIC_METHOD', 1);

	// calling a fuction that is a normal method of a class
	const PROCESSING_MODEL_CLASS_INSTANCE_METHOD = 2;
	//define('TCv_FormItem_MODEL_CLASS_INSTANCE_METHOD', 2);

	// calling a function within the actual form item view
	const PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD = 3;
	//define('TCv_FormItem_FORM_ITEM_INSTANCE_METHOD', 3);

	const PROCESSING_IS_AFTER_UPDATE = 1;
	//define('TCv_FormItem_AFTER_UPDATE', 1);

	const PROCESSING_IS_BEFORE_UPDATE = 0;
	//define('TCv_FormItem_BEFORE_UPDATE', 0);

	// indicate that the form field ID needs to be passed into the process call methdod
	const PROCESSING_INCLUDE_FORM_FIELD_ID_YES = 1;
	//define('TCv_FormItem_INCLUDE_FORM_FIELD_ID_YES', 1);

	// no need to pass in the form field ID
	const PROCESSING_INCLUDE_FORM_FIELD_ID_NO = 0;
	//define('TCv_FormItem_INCLUDE_FORM_FIELD_ID_NO', 0);

	// Special value to indicate that you can pass in the provided values from the form item into a processing param
	const PROCESSING_PARAM_SUBMITTED_VALUE = '::processing_param_value::';
	
	
	// Special value to indicate that the parameter to pass in should be the form model itself. Null if it's a creator
	const PROCESSING_PARAM_FORM_MODEL = '::processing_param_form_model::';
	
	
	/**
	 * TCv_FormItem constructor.
	 *
	 * @param string $id The unique ID fo the form ite
	 * @param string $title THe title of the form item
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id);
		$this->title = $title;
		$this->addFormFieldID($id);
		$this->field_name = $id;
		
		// By default every form field rejects HTML
		$this->setNoHTML();
	}
	
	
	/**
	 * Sets the form that is containing this item. Useful any time a form item must know about the form that it is a part of.
	 *
	 * @param TCv_Form $form
	 */
	public function setForm(&$form)
	{
		if(!($form instanceof TCv_Form))
		{
			TC_triggerError('Provided form was not of type TCv_Form');
		}
	
		$this->form = $form;
	}
	
	/**
	 * Overrides the default functional for setting the ID.
	 * @param string  $field_id
	 */
	public function setID($field_id)
	{
		$this->form_field_ids = array();
		$this->addFormFieldID($field_id);
		parent::setID($field_id);
		$this->field_name = $field_id;
	}
	
	public function setFieldName($name)
	{
		$this->field_name = $name;
	}
	
	public function fieldName()
	{
		return $this->field_name;
	}
	
	/**
	 * Returns the list of form field IDs for this form item. In most cases, there is one field per field item.
	 *
	 * It's possible to create more complex form items which have multiple form fields in place.
	 * @return string[]
	 */
	public function formFieldIDs()
	{
		return $this->form_field_ids;
	}
	
	/**
	 * Adds a form field ID to be tracked and processed.
	 * @param string $form_field_id
	 */
	public function addFormFieldID($form_field_id)
	{
		$this->form_field_ids[$form_field_id] = $form_field_id;
	}
	
	/**
	 * Returns the form that this form item is attached to
	 *
	 * @return TCv_Form|bool
	 */
	public function form()
	{
		return $this->form;
	}
	
	/**
	 * Overrides the default option indicating if this view wraps all the content in a container.
	 * @param $uses_container
	 * @return mixed
	 */
	public function setUsesContainer($uses_container)
	{
		return $this->uses_container = $uses_container;
	}
	
	/**
	 * Returns if this form item is required
	 * @return bool
	 */
	public function isRequired()
	{
		return $this->is_required;
	}
	
	/**
	 * Returns the if this form_item is valid
	 * @return bool
	 */
	public function isValid()
	{
		return $this->is_valid;
	}
	
	/**
	 * Marks the form item as invalid
	 */
	public function markAsInvalid()
	{
		$this->addClass('invalid');
	}
	
	/**
	 * Disables this form item
	 */
	public function disable()
	{
		$this->disabled = true;
	}
	
	/**
	 * Sets this form to attach the item outside of the form table, which can be useful for controlling the
	 * styling and scrolling of the interface.
	 */
	public function setToAttachToFormRoot() : void
	{
		$this->attach_outside_form_table = true;
	}
	
	/**
	 * Returns if this item is meant to be attached outside the form table
	 * @return bool
	 */
	public function isSetToAttachToFormRoot() : bool
	{
		return $this->attach_outside_form_table;
	}
	//////////////////////////////////////////////////////
	//
	// ICONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the icon code for this field which uses Font-Awesome
	 * @param string $field_icon_code
	 */
	public function setIconCode($field_icon_code)
	{
		$this->field_icon_code = $field_icon_code;
	}
	
	/**
	 * Returns the icon code
	 * @return string|null
	 */
	public function iconCode()
	{
		return $this->field_icon_code;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// VALUE SETTING
	//
	// Default values are what is shown when the form is loaded, assuming no other values are available
	// Editor values are what are provided when editing a form. This is common when altering an object that has existing values
	// Saved values are those that have been saved when the form was submitted and returned back for the person to change something.
	// Current value is the actual value that should be shown given all the information above
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the default value for this form. This is a wrapper function around setCurrentValue() which is what actually does all the heavy lifting.
	 *
	 * @param string $value
	 */
	public function setDefaultValue($value)
	{
		$this->default_value = $value;
	}
	
	/**
	 * A method to assign a default value on fields which are localized. This is useful if using the localizaiton moduel
	 * and the default value needs to vary depending on the language. If setting the value for english, use the normal setDefaultValue() method.
	 *
	 * @param string $language
	 * @param string $value
	 */
	public function setDefaultValueLocalized($language, $value)
	{
		$this->default_values_localized[$language] = $value;
	}
	
	/**
	 * Returns the default value for a localization language. Returns false if it was not set.
	 * @param string $language
	 * @return bool|mixed
	 */
	public function defaultValueLocalized($language)
	{
		if(isset($this->default_values_localized[$language]))
		{
			return $this->default_values_localized[$language];
		}
		return false;
	}
	
	/**
	 * Returns the default value
	 *
	 * @return string
	 */
	public function defaultValue()
	{
		return $this->default_value;
	}
	
	/**
	 * Sets the editor value for this form item.
	 *
	 * @param string $value
	 */
	public function setEditorValue($value)
	{

		if($this->editor_value == NULL)
		{
			// Need to explode an array of values
			if (!is_bool($this->useMultipleSetting()) &&
				$this->useMultipleSetting() !== TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS &&
				$this->useMultipleSetting() !== TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE)
			{
				// Not numbered columns, so implode with the value provided
				if(!is_array($value) && !is_null($value))
				{
					$values = explode($this->useMultipleSetting(), $value);
					
					foreach($values as $index => $new_value)
					{
						$values[$index] = trim($new_value);
					}
					
				}
				else
				{
					$values = $value;
				}
				$this->editor_value = $values; // pass in the array of avlues

			}
			else
			{
				$this->editor_value = $value;
			}

		}

	}
	
	/**
	 * Returns the editor value
	 *
	 * @return null|string
	 */
	public function editorValue()
	{
		return $this->editor_value;
	}
	
	/**
	 * Sets the saved value for this form item.
	 *
	 * @param string $value
	 */
	public function setSavedValue($value)
	{
		$this->saved_value = $value;
		$this->saved_value_set = true;
	}
	
	/**
	 * Returns the saved value
	 *
	 * @return null|string
	 */
	public function savedValue()
	{
		return $this->saved_value;
	}
	
	/**
	 * Returns the current value calculated from the possible form values available.
	 *
	 * @return null|string
	 */
	public function currentValue()
	{
		if($this->savedValue() !== NULL)
		{
			return $this->savedValue();
		}
		
		if($this->editorValue() !== NULL)
		{
			return $this->editorValue();
		}
		
		return $this->defaultValue();
		
		
	}

	/**
	 * Indicates that empty values should be passed in as null
	 */
	public function setEmptyValuesAsNull()
	{
		$this->empty_values_as_null = true;
	}

	public function emptyValuesAsNull()
	{
		return $this->empty_values_as_null ;
	}

	//////////////////////////////////////////////////////
	//
	// TITLE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the title for this form item
	 *
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Sets the title for this form item
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}


	//////////////////////////////////////////////////////
	//
	// HELP TEXT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the help text for this form item
	 *
	 * @return string
	 */
	public function helpText()
	{
		return $this->help_text;
	}

	/**
	 * Sets the help text for this form item
	 *
	 * @param string $help_text
	 */
	public function setHelpText($help_text)
	{
		$this->help_text = $help_text;
	}

	/**
	 * Adds help text to the existing text
	 * @param string $help_text
	 */
	public function addHelpText($help_text)
	{
		if($help_text instanceof TCv_View)
		{
			$this->help_text .= $help_text->html();
		}
		else
		{
			$this->help_text .= $help_text;
		}

	}

	//////////////////////////////////////////////////////
	//
	// TYPE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the type for this form item
	 *
	 * @return string
	 */
	public function type()
	{
		return $this->type;
	}
	
	/**
	 * Sets the type for this form item
	 *
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}


	//////////////////////////////////////////////////////
	//
	// PLACEHOLDER
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the placeholder text for this form item
	 *
	 * @return bool|string
	 */
	public function placeholderText()
	{
		return $this->placeholder_text;
	}
	
	/**
	 * Sets the placeholder text for this form item
	 *
	 * @param string $placeholder_text
	 */
	public function setPlaceholderText($placeholder_text)
	{
		$this->placeholder_text = $placeholder_text;
	}

	//////////////////////////////////////////////////////
	//
	// LOADING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets this item to have the focus when loaded
	 */
	public function focusOnLoad()
	{
		$this->addClass('load_focus');

	}
	
	//////////////////////////////////////////////////////
	//
	// TITLE COLUMN
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns if the title column should be shown
	 *
	 * @return bool
	 */
	public function showTitleColumn()
	{
		return $this->show_title_column;
	}
	
	/**
	 * Sets if the title column should be shown
	 *
	 * @param bool $show
	 */
	public function setShowTitleColumn($show)
	{
		$this->show_title_column = $show;
		if($show)
		{
			$this->removeClass('no_title_column');
		}
		else
		{
			$this->addClass('no_title_column');
		}
		
	}
	
	/**
	 * Returns if the title column content should be shown
	 *
	 * @return bool
	 */
	public function showTitleColumnContent()
	{
		return $this->show_title_column_content;
	}
	
	/**
	 * Sets if the title column content should be shown
	 *
	 * @param bool $show
	 */
	public function setShowTitleColumnContent($show)
	{
		$this->show_title_column_content = $show;
	}
	
	/**
	 * Generates the label for this item
	 * @return TCv_View
	 */
	public function label()
	{
		$title = $this->title();
		if(is_null($title))
		{
			return null;
		}
		
		$label = new TCv_View();
		$label->setTag('label');
		$label->addClass('field_title');
		
		if($title instanceof TCv_View)
		{
			$label->attachView($title);
		}
		else
		{
			$label->addText($title);
			
		}
		$label->setAttribute('for', $this->attributeID());
		
		
		return $label;
	}
	
	/**
	 * Sets this form item to never stack
	 * @param bool $left_aligned_titles Indicates if the titles should be left-aligned
	 * @return void
	 */
	public function preventLayoutStacking($left_aligned_titles = true)
	{
		$this->addClass('no_stack');
		if($left_aligned_titles)
		{
			$this->addClass('left_title');
		}
		else
		{
			$this->addClass('right_title');
		}
	}
	
	/**
	 * Sets this form item to be half-width. This only has an effect on stacked forms
	 * @return void
	 */
	public function setAsHalfWidth()
	{
		$this->addClass('half_width');
	}
	
	//////////////////////////////////////////////////////
	//
	// USE MULTIPLES
	//
	//////////////////////////////////////////////////////

	/**
	 * Indicates that multiple values are permitted. This value controls how form items that pass values as arrays
	 * will handle the response.
	 *
	 * If the filtering option is wanted, it must be enabled separately.
	 *
	 * @param bool|string $db_handling (Optional) Default TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS.
	 *
	 * TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS will add each of the values to a column called `field_id_X` where
	 * X is a numerical increasing value starting at 1.
	 *
	 * TCv_FormItem:USE_MULTIPLE_PAIRING_TABLE (Only works with `TCv_FormWithModel` since it requires knowledge
	 * about the primary model that is being created/edited. This feature will use a third table to store these
	 * values. This is a very specific subset of functionality that is common for tables that store one-to-many
	 * values such as users and user_group  * pairings. Using this functionality will update that table with the
	 * values, erasing any previous values for the form model.
	 *
	 * true (boolean) : as a value enables the multiple values, but the processing is likely manual and at the
	 * discretion of
	 * the developer.
	 *
	 * `string` any other string value provided will concatenate the values with that value. The commonly accepted
	 * value to use is a comma.
	 *
	 * @param string|bool $pairing_table_name The name of the table that will be used if the db_handling is set to
	 * TCv_FormItem:USE_MULTIPLE_PAIRING_TABLE.
	 * @param string|bool $paired_column_name The name of the paired columns where the IDs will be inserted
	 * to.
	 * @param TCm_Model|bool $editor_model The editor model that indicates what is currently being edited in the form
	 * . Normally just passing in $form->model() works as expected.
	 * @see TCv_FormItem_Select::useFiltering()
	 */
	public function setUseMultiple($db_handling = TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS,
								   $pairing_table_name = false,
								   $paired_column_name = false,
									$editor_model = false)
	{
		if($db_handling === TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE
			&& ($pairing_table_name === false || $paired_column_name === false))
		{
			TC_triggerError('You must provide a value for `$pairing_table_name` and `$paired_column_name` when using TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE in setUseMultiple');
		}

		$this->use_multiple = $db_handling;

		// Add the pairing table
		if($db_handling === TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE)
		{
			$this->setSaveToDatabase(false);
			//updatePairingTable($table_name, $paired_model_name, $new_values)
			$this->addProcessMethodCall('updatePairingTable',
										TCv_FormItem::PROCESSING_MODEL_CLASS_INSTANCE_METHOD,
										TCv_FormItem::PROCESSING_IS_AFTER_UPDATE,
										TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_NO,
										$pairing_table_name,
										$paired_column_name,
										TCv_FormItem::PROCESSING_PARAM_SUBMITTED_VALUE
										);
			if($editor_model)
			{
				$current_values = $editor_model->currentValuesFromPairingTable($pairing_table_name, $paired_column_name);
				$this->setEditorValue($current_values);
			}

		}
	}

	/**
	 * Returns the use_multiple setting to indicate how array values are handled
	 * @return bool|string
	 */
	public function useMultipleSetting()
	{
		return $this->use_multiple;
	}

	//////////////////////////////////////////////////////
	//
	// PROCESS CALLS
	//
	//////////////////////////////////////////////////////


	/**
	 * Adds a method name to the list that should be called during the processing for this form item. This method handles
	 * all the options for adding a process call including if it is a TCm_Model static method or if the call should happen
	 * before or after the main update.
	 *
	 * Additional parameters can be passed in after the first 4 properties to have those pass through to the method
	 * call. If you turned on $include_form_field_id, then the next parameter must be the field ID.
	 *
	 * If you want to pass in the submitted value, then set that parameter to be
	 * TCv_FormItem::PROCESSING_PARAM_SUBMITTED_VALUE and the processor will pass that in instead.
	 *
	 * @param string $method_name The name of the method to be called
	 * @param bool $is_model_class_method Indicates if the method being called is a class method that should be called using the
	 * @param bool $wait_for_update Indicates that this method call should only occur after the main update to the
	 * database for the model. If the call has a verification and it happens before the update, a failed verification
	 * will result in the update not happening.
	 * @param bool $include_form_field_id Indicates if the function to be called requires the form ID to be passed in.
	 * This will be true for most methods, however when calling an instance method of model or a TCv_FormItem, the
	 * field id isn't required. If the value is to YES, then the first argument passed in, must be the field id.
	 *
	 *
	 */
	public function addProcessMethodCall($method_name, $is_model_class_method, $wait_for_update, $include_form_field_id )
	{
		$method_name = str_ireplace('()', '', $method_name);
		
		$args = func_get_args();
		array_shift($args);
		array_shift($args);
		array_shift($args);
		array_shift($args);
		$this->process_calls[$method_name] = array(
			'wait_for_update' => $wait_for_update, 
			'is_model_class_method' => $is_model_class_method, 
			'method_name' => $method_name, 
			'include_form_field_id' => $include_form_field_id, 
			'args' => $args);
	}
	
	/**
	 * Removes a process call for a provided method name
	 * @param string $method_name
	 */
	public function removeProcessMethodCall($method_name)
	{
		$method_name = str_ireplace('()', '', $method_name);
		unset($this->process_calls[$method_name]);
	}
	
	/**
	 * Returns the process method calls
	 *
	 * @return array
	 */
	public function processMethodCalls()
	{
		return $this->process_calls;	
	}

	/**
	 * Adds a property for this form item that will be saved during form processing and passed to form processor for
	 * continuity. Any property added must be a public property on the class.
	 * @param string $property_name
	 */
	public function addSavedProcessorClassProperty($property_name)
	{
		$this->processor_class_properties[] = $property_name;
	}

	//////////////////////////////////////////////////////
	//
	// VALIDATIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Indicates that this form item is required.
	 * @param bool $disable_validation (Optional) Default false. If true, the field will be marked but no validation will happen.
	 */
	public function setIsRequired($disable_validation = false)
	{
		if($disable_validation)
		{
			$this->is_required = 2; // use a value of 2 so that boolean tests pass, but we can discern disabled
			// validation from fully required items
		}
		else
		{
			$this->is_required = 1;
			$this->addProcessMethodCall('validateIsBlank',
										TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
										TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
										TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		}
	}

	/**
	 * Turns off the required setting for this form field
	 */
	public function disableRequired()
	{
		$this->is_required = 0;
		$this->removeProcessMethodCall('validateIsBlank');
	}
	
	/**
	 * Adds the float validation
	 */
	public function setIsFloat()
	{
		$this->addClass('float');
		$this->addProcessMethodCall('validateIsFloat',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
									TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
									TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
	}
	
	/**
	 * Adds the integer validation
	 */
	public function setIsInteger()
	{
		$this->addClass('integer');
		$this->addProcessMethodCall('validateIsInteger',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
	}
	
	/**
	 * Adds the alpha-numeric validation
	 */
	public function setIsAlphaNumericUnderscore()
	{
		$this->addClass('alphanumberic_underscore');
		$this->addProcessMethodCall('validateIsAlphaNumericUnderscore',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
	}
	
	/**
	 * Adds the no spaces validation
	 */
	public function setNoSpaces()
	{
		$this->addClass('no_spaces');
		$this->addProcessMethodCall('validateNoSpaces',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		
	}
	
	/**
	 * Adds the no HTML validation
	 */
	public function setNoHTML()
	{
		$this->addProcessMethodCall('validateNoHTML',
		                            TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD,
		                            TCv_FormItem::PROCESSING_IS_BEFORE_UPDATE,
		                            TCv_FormItem::PROCESSING_INCLUDE_FORM_FIELD_ID_YES);
		
	}
	
	/**
	 * Enables HTML to be submitted via this form.
	 *
	 * WARNING: This should only be used when absolutely necessary. Extra care must be taken to avoid site injection
	 * vulnerabilities when enabling HTML inputs.
	 */
	public function setAsAllowsHTML()
	{
		$this->removeProcessMethodCall('validateNoHTML');
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FORM TRACKING
	//
	//////////////////////////////////////////////////////

	/**
	 * This is a function that can be overwritten to force the form tracker to track other fields which aren't the default
	 * for the form item. It won't be used often, but it is used in cases when a form item may instantiate multiple form
	 * items within itself. An example can be seen in TCv_FormItem_CheckboxList.
	 *
	 * @return array
	 */
	public function additionalFormItemsToTrack()
	{
		return array();
		
	}

	//////////////////////////////////////////////////////
	//
	// DATABASE
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the value that was submitted to the form. If a form item consists of a more complicated layout such as
	 * multiple form elements, this function should be overridden to ensure that the proper values are indicated as the submitted value.
	 * @return string|array
	 */
	public function submittedValue()
	{
		$value = @$_POST[$this->id];
		
		if($value == NULL)
		{
			return '';
		}
		
		if(is_array($value))
		{
			foreach($value as $index => $array_value)
			{
				$value[$index] = trim($array_value);
			}
		}
		else
		{
			$value = trim($value);
		}


		
		return $value;
	}
	
	/**
	 * Returns the value that will be entered into the database. The default operation for this method is return the value
	 * provided by submittedValue(). If a new value is to be provided, this method should be overridden to provide the
	 * proper value to be entered into the database.
	 * @return string
	 */
	public function databaseValue()
	{
		$value = $this->submittedValue();
		if($this->emptyValuesAsNull() && $value == '')
		{
			return NULL;
		}
		return $value;
	}
		
	
	/**
	 * Sets the flag indicating if the value for this form item should be saved to the database.
	 * @param bool $save
	 */
	public function setSaveToDatabase($save = true)
	{
		$this->save_to_database = $save;
	}
	
	/**
	 * Returns if this form item is saved to the databse
	 *
	 * @return bool
	 */
	public function saveToDatabase()
	{
		return $this->save_to_database;
	}
	
	//////////////////////////////////////////////////////
	//
	// VISIBILITY TOGGLING
	//
	// These are automated solutions for showing/hiding fields
	// based on the value of another field
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Enables the functionality that will show or hide this field based on the value of another field.
	 * @param string $field_id The field ID that will be used to trigger the showing/hiding of the visibility
	 * @param mixed $show_value The value that correlate to a "show value". Any other value will hide the
	 * field
	 */
	public function setVisibilityToFieldValue(string $field_id, $show_value) : void
	{
		$this->addDataValueForRow('vis-toggle-show',$show_value);
		$this->addDataValueForRow('vis-toggle-field', $field_id);
		
		// rely on browser validation only
		$this->removeProcessMethodCall('validateIsBlank');
	}
	
	/**
	 * Enables the functionality that will set the visibility for this field based on if another field has a value.
	 * This checks for a non-null non-empty-string value and then adjusts the visibility accordingly.
	 * @param string $field_id The field ID that will be used to trigger the showing/hiding of the visibility
	 * field
	 */
	public function setVisibilityToFieldValueIsSet(string $field_id) : void
	{
		// pass in a special value to test for
		$this->setVisibilityToFieldValue($field_id, '!empty');
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// VIEW ATTACHING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Adds a css class to the form element itself.
	 * @param string $class THe CSS class name
	 */
	public function addFormElementCSSClass($class)
	{
		$this->form_element_classes[] = $class;
	}
	
	/**
	 * Adds a unique attribute commonly used by parsing systems that follow the pattern `data-$name="$value"`.
	 *
	 * This method is used to easily add data values to the tag. The convention is to have dash-separated-words for
	 * data attributes.
	 * @param string $name The name of the attribute *without* the "data-" string at the start.
	 * @param string $value THe value for the data value
	 */
	public function addFormElementDataValue($name, $value)
	{
		$this->form_element_data_values[$name] = $value;
	}
	
	/**
	 * Add a CSS class that is specific fo
	 *
	 * @param string $class
	 */
	public function addCSSClassForRow($class)
	{
		$this->row_classes[] = $class;
	}
	
	/**
	 * Returns the row classes
	 * @return array
	 */
	public function rowClasses()
	{
		return $this->row_classes;
	}
	
	/**
	 * Add a data value that is specific for the row
	 *
	 * @param string $name
	 * @param string $value
	 */
	public function addDataValueForRow($name, $value)
	{
		$this->row_data_values[$name] = $value;
	}
	
	/**
	 * Returns the row data values
	 * @return array
	 */
	public function rowDataValues()
	{
		return $this->row_data_values;
	}
	
	/**
	 * Attaches a view to this form item view which appears before the form items.
	 *
	 * @param TCv_View $view
	 */
	public function attachViewBefore($view)
	{
		parent::attachView($view); // gets added beforehand by default
	}
	
	/**
	 * Attaches a view to this form item view after all the other content is added This function saves all these views
	 * until the output to ensure any view are added first..
	 * @param TCv_View $view
	 */
	public function attachViewAfter($view)
	{
		$this->views_after[] = $view;
	}
	
	/**
	 * Removes any of the attached views after
	 */
	public function clearViewsAfter()
	{
		$this->views_after = [];
	}
	
	/**
	 * Returns the html for the text field.
	 *
	 * @return string
	 */
	public function formElementHTML()
	{
		return $this->html();
	}
	
	//////////////////////////////////////////////////////
	//
	// TCv_SearchableModelList filters
	//
	// Functions related to how form fields behaving in
	// searchable model list filters
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this form items should appear in searchable model lists applied filter results.
	 * @return bool
	 * @see TCv_SearchableModelList
	 */
	public function appearsInSearchableAppliedFilters() : bool
	{
		return $this->show_in_searchable_applied_filters;
	}
	
	/**
	 * Hides this form item in any searchable applied lists. This only has an effect when this is a form item
	 * in a searchable model list filter
	 * @return void
	 * @see TCv_SearchableModelList
	 */
	public function hideInSearchableAppliedFilters() : void
	{
		$this->show_in_searchable_applied_filters = false;
	}
	
	/**
	 * A function that returns if something is clearable in the searchable applied filters. NO
	 * @return bool
	 */
	public function clearableInSearchableAppliedFilters() : bool
	{
		return $this->clearable_in_searchable_applied_filters;
	}
	
	/**
	 * Turns of the ability to clear this field in a searchable model list filters. Only has an effect if it appears
	 * @return void
	 * @see TCv_FormItem::appearsInSearchableAppliedFilters()
	 * @see TCv_SearchableModelList
	 */
	public function disableClearingInSearchableAppliedFilters() : void
	{
		$this->clearable_in_searchable_applied_filters = false;
	}
	
	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Override of the normal html() function to include any of the views that are added after the normal content.
	 * @return string
	 */
	public function html()
	{
		if($this->disabled)
		{
			$this->setAttribute('disabled','disabled');
		}
		
		foreach($this->views_after as $view)
		{
			parent::attachView($view);
		}
		
		// deal with box having attributes that match the input
		if($this->uses_container)
		{
			$this->setIDAttribute($this->id.'_container');
			$this->clearClasses();
			$this->addClass('form_item_container');
		}
		
		return parent::html();
	}
	
	
}

//////////////////////////////////////////////////////
//
// CONSTANTS - DEPRECATED
//
// USE the TCv_FormItem constants defined at the top of this file
//
//////////////////////////////////////////////////////

define('TCv_FormItem_FORM_PROCESSOR_METHOD', 0); // calling a function that exists in a TCc_FormProcessor. Default Utilities.
define('TCv_FormItem_CLASS_STATIC_METHOD', 1); // calling a function that is a static method. Useful if it must be called even when there is no instance. ie: form creators
define('TCv_FormItem_MODEL_CLASS_INSTANCE_METHOD', 2); // calling a fuction that is a normal method of a calss
define('TCv_FormItem_FORM_ITEM_INSTANCE_METHOD', 3); // calling a function within the actual form item view

define('TCv_FormItem_AFTER_UPDATE', 1);
define('TCv_FormItem_BEFORE_UPDATE', 0);

define('TCv_FormItem_INCLUDE_FORM_FIELD_ID_YES', 1); // indicate that the form field ID needs to be passed into the process call methdod
define('TCv_FormItem_INCLUDE_FORM_FIELD_ID_NO', 0); // no need to pass in the form field ID


?>