<?php 
	/**********************************************
	 
	 This is a general process script that will deal with 
	 any TCv_FormWithModel that needs processing. This script is
	 called by default by the TCv_FormWithModel class but it can
	 be overridden easily as necessary. 
	 
	 It should also be noted that this script loads the tungsten_header.php
	 file which means that if you are attempting to process a form
	 from a public site, you will need to use set the value of 
	 $skip_tungsten_authentication to true for that script and then process the 
	 script manually. You can copy this script as a starting point.
	 
	 **********************************************/
	 
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_process_header.php"); 
	
	/**********************************************/
	/*										   	  */
	/*             PROCESSOR STARTUP              */
	/*										   	  */
	/**********************************************/
	$form_processor = TCc_FormProcessor::init();
	$form_class = $form_processor->formClassName();
	
	/**********************************************/
	/*										   	  */
	/*            CUSTOMIZE MESSENGER             */
	/*										   	  */
	/**********************************************/
	//TC_messageConditionalTitle('Success', true);
	//TC_messageConditionalTitle('Failed', false);


	//////////////////////////////////////////////////////
	//
	// VALIDATE
	//
	//////////////////////////////////////////////////////
	$form_processor->processFormItems();
	$form_class::customFormProcessor_Validation($form_processor); // validation should always run

	//////////////////////////////////////////////////////
	//
	// UPDATE DATABASE
	//
	// DB updates happen with models which correlate to tables
	// DB updates don't happen automatically for regular forms
	//
	//////////////////////////////////////////////////////
	if($form_processor->isValid())
	{
		$form_class::customFormProcessor_performPrimaryDatabaseAction($form_processor);
	}
		
	if($form_processor->isValid())
	{
		$form_class::customFormProcessor_afterUpdateDatabase($form_processor);
	}
	
	/**********************************************/
	/*										   	  */
	/*            FINISH AND REDIRECT             */
	/*										   	  */
	/**********************************************/
	$form_processor->finish();	
	
?>
