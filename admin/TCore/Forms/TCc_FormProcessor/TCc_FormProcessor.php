<?php

/**
 * Class TCc_FormProcessor
 */
class TCc_FormProcessor extends TCu_Processor
{
	// form items
	protected $form_items = array(); // [array] = The array of form items that are added to this form. The indices are the respective IDs for the form items.
	protected $form_item_models = array(); //
	protected $form_item_views = array();

	/** @var bool|TCv_Form  */
	protected $form_view = false; // [TCv_Form] = An instance of the form being processed.
	protected $form_class_name = false; // [string] = The name of the form class being viewed
	protected $model = false; // Pass-through value that wont' have any significance in regular form processors
	protected $is_editor = false;
	
	// form tracking
	/** @var TCm_FormTracker $form_tracker  */
	protected $form_tracker = false;
	protected $form_tracker_session_name = ''; // [string] = Name of the session variable that the form tracker is saved in
	protected $form_tracker_always_save = false;

	protected $form_items_processed = false; // [bool] = Flag if the form item validations have already been added
	protected $set_date_added = false; // [bool] = Flag to indicate if the date_added property should be included in the DB
	protected $post_updates_called = false;
	
	
	// Process URLS
	protected $failure_url = ''; // [string] = The url that the processor will be directed to if the processing fails
	protected $success_url = ''; // [string] = The url that the processor will be directed to if the processing succeeds
	
	/**
	 * TCc_FormProcessor constructor.
	 */
	public function __construct()
	{
		parent::__construct(get_called_class().'processor');
		
		// FORM TRACKER
		$this->findFormTracker();
		
		
		// SAVES VALUES OF THE TRACKER TO THE FORM ITEMS
		$this->saveFormValues();
		
		$this->verifyFormMatch();
		
		if(isset($_POST['success_url']))
		{
			if($_POST['success_url'] == 'referer' || $_POST['success_url'] == 'referrer')
			{	
				$this->setSuccessURL($_SERVER['HTTP_REFERER'] ?? '');
			}
			else
			{
				$this->setSuccessURL($_POST['success_url']);		
			}
			
		}
		
		$this->form_class_name = $_POST['tracker_form_class'];

	}
	
	/**
	 * The name of the form class that is being processed.
	 * @return string
	 */
	public function formClassName()
	{	
		return $this->form_class_name;
	}

	//////////////////////////////////////////////////////
	//
	// FORM TRACKER
	//
	//////////////////////////////////////////////////////


	/**
	 * Handles the finding of the form tracker given the different ways it can come to the processor
	 */
	public function findFormTracker()
	{
		// INITIATE FORM TRACKER
		$this->form_tracker_session_name = $_POST['tracker_name'];

		// Form tracker found in a session variable. use that one.
		if(isset($_SESSION['TCm_FormTracker'][$this->form_tracker_session_name]))
		{
			$this->form_tracker = $_SESSION['TCm_FormTracker'][$this->form_tracker_session_name];
		}
		// Tracker name, but no tracker provided
		elseif(isset($this->form_tracker_session_name))
		{
			$this->form_tracker = TCm_FormTracker::configure($this->form_tracker_session_name);
		}
		
		
		// Nothing else is found. Just create a form tracker
		if(!isset($this->form_tracker))
		{
			// ! TODO: Find a safer method to simply return rather than throw an error
			TC_triggerError('Attempting to process a form without a valid tracker. In order to avoid incorrect processing of data, the code has been halted. Please contact your web developer.');
		}
		
		$this->processDatabaseSettings();
		
		$this->form_tracker->resetInvalidFields();
		
		
	}
	
	/**
	 * Verifies that the form being processed matches the one that is being sent
	 */
	public function verifyFormMatch()
	{
		if($this->form_tracker->submittedValueForFormItemID('tracker_verification') != $this->form_tracker->verificationCode())
		{
			//$this->fail('Form Validation Unsuccessful');
		}
	}
	
	/**
	 * Handles all the database settings from the form tracker
	 */
	public function processDatabaseSettings()
	{
		// table name and primary key
		$this->setDatabaseTableValues($this->form_tracker->primaryTable(), $this->form_tracker->primaryKey());
		
		if($this->form_tracker->primaryValue() !== false)
		{
			$this->setDatabasePrimaryValue($this->form_tracker->primaryValue());
			$this->is_editor = true;
		}
		
		
	}

	//////////////////////////////////////////////////////
	//
	// ACCESSORS
	//
	//////////////////////////////////////////////////////



	/**
	 * takes a field ID and returns all teh submitted values as an associative array. In most cases, it will be a single array
	 * however it's possible for a field to return multiple values.
	 * @param string $field_id
	 * @param bool $use_database_value Indicates if the database value should be returned instead of submitted
	 * @return array
	 */
	public function valuesForField($field_id, $use_database_value = false)
	{
		$form_item = $this->form_item_views[$field_id];

		if($use_database_value)
		{
			$form_value = $form_item->databaseValue();

		}
		else
		{
			$form_value = $this->formValue($field_id);
		}



		return $this->getFormValuesForField($form_item, $form_value);
	}

	/**
	 * A function for processing a form value for a field and returning the appropriate value.
	 * @param TCv_FormItem $form_item The form item that is being processed
	 * @param bool|string|array $form_value The form value provided
	 * @return array
	 */
	public function getFormValuesForField($form_item, $form_value)
	{
		$field_id = $form_item->id();
		$values = array();
		if(is_array($form_value))
		{
			if($form_item->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE)
			{
				TC_triggerError('TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE requires a `TCv_FormWithModel` to be used when setting setUseMultiple() of a form item.');
			}
			elseif($form_item->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS)
			{
				// NOTE : 2024-08-28
				// Was ignoring checklists, which just broke them
				// Commit 99bfb253d – August 2022
				// Not sure why it was added, but it was a bad idea
				//if(!$form_item instanceof TCv_FormItem_CheckboxList)
				//{
				foreach ($form_value as $index => $value)
				{
					$values[$index] = $value;
				}
				//}
			}
			else
			{
				$values[$field_id] = implode($form_item->useMultipleSetting(), $form_value);
			}
		}
		else
		{
			$values[$field_id] = $form_value;
		}

		return $values;
	}

	/**
	 * Returns an array of the submitted form values. Unfiltered or validated.
	 * @return array
	 */
	public function submittedValues()
	{
		$values = array();
		foreach($this->form_item_views as $id => $form_item)
		{
			$values = array_merge($values, $this->submittedValuesForField($id));
		}
		return $values;
	}

	/**
	 * Returns an array of the submitted form values that are tied to a visible form value. Unfiltered or validated.
	 * @return array
	 */
	public function submittedVisibleValues()
	{
		$values = array();
		foreach($this->form_item_views as $id => $form_item)
		{
			if(!$form_item instanceof TCv_FormItem_Hidden)
			{
				$values = array_merge($values, $this->submittedValuesForField($id));
			}
		}
		return $values;
	}

	/**
	 * Returns an array of the submitted form values that are set to be saved to the database.
	 *
	 * @return array
	 */
	public function submittedSavableValues()
	{
		$values = array();
		foreach($this->form_item_views as $id => $form_item)
		{
			if($form_item->saveToDatabase())
			{
				$submitted_value = $form_item->submittedValue();
				if(is_array($submitted_value))
				{
					if($form_item->useMultipleSetting() == TCv_FormItem::USE_MULTIPLE_NUMBERED_COLUMNS)
					{
						$count = 1;
						foreach($submitted_value as $value)
						{
							$values[$id . '_' . $count] = $value;
							$count++;
						}
					}
					else
					{
						$values[$id ] = implode($form_item->useMultipleSetting() ,$submitted_value);

					}

				}
				else
				{
					$values[$id] = $submitted_value;
				}

			}
		}
		return $values;
	}


	/**
	 * Returns if the field with the given ID is set.
	 * @param string $id
	 * @return bool
	 */
	public function fieldIsSet($id)
	{
		return isset($this->form_item_views[$id]);
	}

	/**
	 * Returns the form item with the given ID
	 * @param string
	 * @return bool|TCv_FormItem
	 */
	public function formItemWithID($id)
	{
		
		if(isset($this->form_item_views[$id]))
		{
			return $this->form_item_views[$id];
		}
		
		return false;
		
	}
	
	/**
	 * Returns the form value with the given ID
	 * @param string $id
	 * @return string|int
	 */
	public function formValue($id)
	{
		if($this->formItemWithID($id))
		{
			if($this->form_tracker)
			{
				return $this->form_tracker->submittedValueForFormItemID($id);
			}

			return $this->formItemWithID($id)->submittedValue();
		}

		// failsafe to posted value, just in case
		return @$_POST[$id];
		
	}
	
	/**
	 * Returns the value to be added to the database for the provided form item ID
	 * @param string $id
	 * @return string
	 */
	public function databaseValue($id)
	{
		if(isset($this->database_values[$id]))
		{
			return $this->database_values[$id]['value'];
		}
		if($this->formItemWithID($id))
		{
			return $this->formItemWithID($id)->databaseValue();
		}
		else
		{
			TC_triggerError('Form Item with ID "'.htmlspecialchars($id).'", does not exist.');

		}

		return '';
	}
	
	
	/**
	 * Returns an array of values that gives the ID, title, and value submitted for each TCv_FormItem
	 * @return array
	 */
	public function submittedFormValues()
	{
		if(!$this->form_items_processed)
		{
			TC_triggerError('Cannot acquire submitted form items until after processFormItems() as been called.');
		}
		
		$values = array();
		foreach($this->form_item_views as $id => $form_item_view)
		{
			$values[] = array(
				'form_item_id' => $id, 
				'class_name' => get_class($form_item_view), 
				'title' => $form_item_view->title(), 
				'submitted_value' => $form_item_view->submittedValue()
				
				);	
		}
		
		return $values;
	}
	
	
	/**
	 * Returns if the processor is an editor
	 * @return bool
	 */
	public function isEditor()
	{
		return $this->is_editor;
	}
	
	/**
	 * Returns if the processor is handling the creation of a new item
	 * @return bool
	 */
	public function isCreator()
	{
		return !$this->is_editor;
	}
	

	/**
	 * Loops through each form item, calls the submittedValue() function for that item and saves the value to the
	 * form_tracker. This function is automatically called from the constructor for this class.
	 */
	protected function saveFormValues()
	{
		foreach($this->form_tracker->formItems() as $id => $form_item_values)
		{
			$class_name = $form_item_values['view_class_name'];

			/** @var TCv_FormItem $form_item_view */
			$form_item_view = new $class_name($form_item_values['id'], $form_item_values['title']);
			
			// Save relevant values to the form. Ensures that the form class process calls work as expected
			//$form_item_view->setSaveToDatabase($form_item_values['save_to_database']);

			// Save the "use multiple setting
			//$form_item_view->setUseMultiple($form_item_values['use_multiple'],
			//$form_item_values['use_multiple_pairing_table_name']);

			foreach($form_item_values['processor_class_properties'] as $property_name)
			{
				$form_item_view->$property_name = $form_item_values[$property_name];
			}

			// Save the view
			$this->form_item_views[$form_item_values['id']] = $form_item_view;
			
			$submitted_value = $form_item_view->submittedValue();

			$this->form_tracker->saveSubmittedValue($id, $submitted_value);

			
			$this->trackFormItemInConsole($id);
		}
		
	
	}
	
	/**
	 * Updates the value of a form tracker to a new value. This is a pass-through function for the form tracker.
	 * @param string $id
	 * @param string $value
	 * @uses TCm_FormTracker::saveSubmittedValue()
	 */
	public function updateSavedValue($id, $value)
	{
		$this->form_tracker->saveSubmittedValue($id, $value);
	}
	
	/**
	 * This function should be only once per form processor action. It will call any validations or actions that are set
	 * for each form item. If this function is called more than once, it will ignore the subsequent calls.
	 */
	public function processFormItems()
	{
		if(!$this->form_items_processed)
		{

			$this->validations = array();
		
			// loop through each of the form items and performs the validation on them
			foreach($this->form_tracker->formItems() as $id => $form_item_values)
			{
				if(sizeof($form_item_values['process_calls']) > 0)
				{
					// loop through the process calls for this item
					foreach($form_item_values['process_calls'] as $index => $process_values)
					{
						// perform function calls that are meant to happen before the DB update
						if(!$process_values['wait_for_update'])
						{
							$this->processFunctionCall($form_item_values, $process_values);
						}
					}
				}
				
				$this->validateNoInjectionFlags($form_item_values);
				
				
				
			}
			
			
		}
		
		$this->form_items_processed = true;
		
	}

	//////////////////////////////////////////////////////
	//
	// PROCESS FUNCTIONS / VALIDATION
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns a model regardless of if the form processor has access to a model and regardless of if it is an editor or not.
	 *
	 * @return TCm_Model
	 */
	public function modelForProcessingFunctionCalls()
	{
		return new TCm_Model(0);
	}
	
	/**
	 * Processes a function call on a view that is meant to be run during the processing of the form.
	 *
	 * @param array $form_item_values
	 * @param array $process_values
	 */
	public function processFunctionCall($form_item_values, $process_values)
	{
		// Attach the form_processor as the last argument to the function
		$process_values['args'][] = $this;
	
		// If returns validation, then start with form_field_id
		if($process_values['include_form_field_id'])
		{
			array_unshift($process_values['args'], $form_item_values['id']);
		}
		
		foreach($process_values['args'] as $index => $value)
		{
			if($value == TCv_FormItem::PROCESSING_PARAM_SUBMITTED_VALUE)
			{
				$process_values['args'][$index] = $form_item_values['submitted_value'];
			}
			
			elseif($value == TCv_FormItem::PROCESSING_PARAM_FORM_MODEL)
			{
				if($this->isCreator())
				{
					$process_values['args'][$index] = null;
				}
				// Only works on FormProcessorWithModel
				else
				{
					$process_values['args'][$index] = $this->modelForProcessingFunctionCalls();
				}
			}
			
			
		}

		// ----- METHOD IN THE FORM PROCESSOR -----
		if($process_values['is_model_class_method'] == TCv_FormItem::PROCESSING_IS_FORM_PROCESSOR_METHOD)
		{
			$this->processFunctionCall_FORM_PROCESSOR_METHOD($form_item_values, $process_values);
		}
		
		// ----- STATIC CLASS METHOD -----
		elseif($process_values['is_model_class_method'] == TCv_FormItem::PROCESSING_IS_CLASS_STATIC_METHOD)
		{
			$validation_result = $this->processFunctionCall_CLASS_STATIC_METHOD($form_item_values, $process_values);
			
		}
		
		// ----- INSTANCE CLASS METHOD -----
		elseif($process_values['is_model_class_method'] == TCv_FormItem::PROCESSING_MODEL_CLASS_INSTANCE_METHOD)
		{
			$validation_result = $this->processFunctionCall_CLASS_INSTANCE_METHOD($form_item_values, $process_values);
		}
		
		// ----- FORM ITEM INSTANCE CLASS -----
		elseif($process_values['is_model_class_method'] == TCv_FormItem::PROCESSING_IS_FORM_ITEM_INSTANCE_METHOD)
		{
			$validation_result = $this->processFunctionCall_FORM_ITEM_INSTANCE_METHOD($form_item_values, $process_values);
		}
		
		// Method within a formitem view class
		
		// Deal with optional returned validations
		if(isset($validation_result))
		{
			if($validation_result instanceof TCm_FormItemValidation)
			{
				if(!$validation_result->isValid())
				{
					$this->is_valid = false;
				}
				
				$this->addValidation($validation_result);
			}	
		}
		
	}
	
	/**
	 * @param array $form_item_values
	 * @param array $process_values
	 * @return TCm_FormItemValidation
	 */
	public function processFunctionCall_FORM_PROCESSOR_METHOD($form_item_values, $process_values)
	{
		
		//$validation_result = call_user_func_array(array($this, $process_values['method_name']), $process_values['args']);
		
		// Calling internal form proceessor methods handle the validation results immeidately.
		return call_user_func_array(array($this, $process_values['method_name']), $process_values['args']);
		
	}
	
	/**
	 * @param array $form_item_values
	 * @param array $process_values
	 * @return TCm_FormItemValidation
	 */
	public function processFunctionCall_CLASS_STATIC_METHOD($form_item_values, $process_values)
	{
		// DO NOTHING. THERE IS NO STATIC MODEL TO REFERENCE
		
		// This shouldn't happen
		
		// USE TCc_FormProcessorWithModel
		$validation = new TCm_FormItemValidation('no_id', 'Static validation skipped');
		$validation->setIsSkipped(true);
		$validation->setDetails('Static validation used, on non-model based processor');
		return $validation;
	
	}
	
	/**
	 * @param array $form_item_values
	 * @param array $process_values
	 * @return TCm_FormItemValidation
	 */
	public function processFunctionCall_CLASS_INSTANCE_METHOD($form_item_values, $process_values)
	{
		$model = $this->modelForProcessingFunctionCalls();
		
		// Ensure the submitted value is passed to the method. This is the default action since it requires the
		// value in order to interact with it properly.
		// Previous attempts to comment this out on 2019-06-05 `3ddb704f` broke this functionality
		array_unshift($process_values['args'], $form_item_values['submitted_value']);
		
		// If returns validation, then start with form_field_id
		if($process_values['include_form_field_id'])
		{
			array_unshift($process_values['args'], $form_item_values['id']);
		}
		
		
		// calls the instance method of the $model with the name $process_values['method_name']
		// it then passes in the arguments starting with the form item ID, then the value submitted, then the $model
		return call_user_func_array(array($model, $process_values['method_name']), $process_values['args']);
	}
	
	/**
	 * @param array $form_item_values
	 * @param array $process_values
	 * @return TCm_FormItemValidation
	 */
	public function processFunctionCall_FORM_ITEM_INSTANCE_METHOD($form_item_values, $process_values)
	{
		$field = $this->form_item_views[$form_item_values['id']];
		return call_user_func_array(array($field, $process_values['method_name']), $process_values['args']);
	}
	
	/**
	 * Performs update calls that occur after the update database has occurred
	 */
	protected function performPostUpdateMethodCalls()
	{
		// loop through each of the form items and performs the validation on them
		foreach($this->form_tracker->formItems() as $id => $form_item_values)
		{
			if(sizeof($form_item_values['process_calls']) > 0)
			{
				// loop through the process calls for this item
				foreach($form_item_values['process_calls'] as $index => $process_values)
				{
					// perform function calls that are meant to happen before the DB update
					if($process_values['wait_for_update'])
					{
						$this->processFunctionCall($form_item_values, $process_values);
					}
				}
			}
		}
		
	}
	
	/**
	 * Adds a validation to the processor. This function takes a TCm_FormItemValidation and combines the results from
	 * that validation to all the other validations in the processor. If this method is being used outside of this class'
	 * validate() method, then it must be called "after" validate().
	 *
	 * @param TCm_FormItemValidation $validation
	 */
	public function addValidation($validation)
	{
		parent::addValidation($validation);
		
		// ADD VALIDATION TO FORM TRACKER
		$this->form_tracker->addValidation($validation);
	}
	
	/**
	 * Forces a failure of this processor indicating that a particular form item is to blame.
	 * @param string $form_item_id
	 * @param bool $message
	 */
	public function failFormItemWithID($form_item_id, $message = false)
	{
		$this->form_tracker->setInvalidField($form_item_id);
		$this->fail($message);
	}
	
	/**
	 * Sets the messenger title which likely overrides whatever setting was previously set
	 * @param string $title
	 */
	public function setMessengerTitle($title)
	{
		$messenger = TCv_Messenger::instance();
		$messenger->setFinalTitle($title);
	}

	//////////////////////////////////////////////////////
	//
	// BUILT-IN PROCESS FUNCTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Performs a match that checks that two fields have the same value
	 * @param string $field_id_1
	 * @param string $field_id_2
	 */
	public function validateMatchingValues($field_id_1, $field_id_2)
	{
		$field_1 = $this->form_item_views[$field_id_1];
		TC_assertObjectType($field_1, 'TCv_FormItem');
		$field_2 = $this->form_item_views[$field_id_2];
		TC_assertObjectType($field_2, 'TCv_FormItem');
		

		// VALIDATE MATCHING PASSWORDS, REQUIRES 2 FIELDS TO PERFORM
		$validation = new TCm_FormItemValidation($field_id_1, 'Validate Matching Values : '.$field_1->title().' and '.$field_2->title());
	
		if($field_1->submittedValue() != $field_2->submittedValue())
		{
			$this->form_tracker->setInvalidField($field_id_1);
			$this->form_tracker->setInvalidField($field_id_2);
			$validation->failWithMessage('The values for '.$field_1->title().' and '.$field_2->title().' did not match');
		}
		
		// Add them directly
		$this->addValidation($validation);
		
	}
	
	/**
	 * Performs a match that checks that two fields have different values
	 * @param string $field_id_1
	 * @param string $field_id_2
	 */
	public function validateNonMatchingValues($field_id_1, $field_id_2)
	{
		
		$field_1 = $this->form_item_views[$field_id_1];
		TC_assertObjectType($field_1, 'TCv_FormItem');
		$field_2 = $this->form_item_views[$field_id_2];
		TC_assertObjectType($field_2, 'TCv_FormItem');
		
		// VALIDATE MATCHING PASSWORDS, REQUIRES 2 FIELDS TO PERFORM
		$validation = new TCm_FormItemValidation($field_id_1, 'Validate Non-Matching Values : '.$field_1->title().' and '.$field_2->title());
	
		if($field_1->submittedValue() == $field_2->submittedValue())
		{
			$this->form_tracker->setInvalidField($field_id_1);
			$this->form_tracker->setInvalidField($field_id_2);
			$validation->failWithMessage('The values for '.$field_1->title().' and '.$field_2->title().' cannot be the same');
		}

		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Validates if the value provided is a single string that is alphanumeric with an option for an underscore
	 * @param string $field_id
	 */
	public function validateIsAlphaNumericUnderscore($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is Alpha-Num-_');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		if(preg_match('/\W/', $field->submittedValue()))
		{
			$validation->failFieldWithMessage($field->title(), 'Must be a single word container only letters, numbers, or the underscore (_) character.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to check if the value is blank.
	 * @param $field_id
	 */
	public function validateIsBlank($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is Blank');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		if(is_array($field->submittedValue()))
		{
			if(sizeof($field->submittedValue()) == 0)
			{
				$validation->failFieldWithMessage($field->title(), 'You must enter a value.');	
			}
		}
		elseif(trim($field->submittedValue()) == '')
		{
			$validation->failFieldWithMessage($field->title(), 'You must enter a value.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to check if this form item is a single word.
	 * @param string $field_id
	 */
	public function validateNoSpaces($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Has Spaces');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		if(preg_match('/\s/', $field->submittedValue()))
		{
			$validation->failFieldWithMessage($field->title(), 'No spaces allowed.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}

	/**
	 * Performs a validation to check if this form item is a single word.
	 * @param string $field_id
	 */
	public function deleteSpaces($field_id)
	{
		$_POST[$field_id] = str_replace(' ','',$_POST[$field_id]);
		$field = $this->form_item_views[$field_id];
		$this->form_tracker->saveSubmittedValue($field_id, $field->submittedValue());

		$validation = new TCm_FormItemValidation($field_id, 'Deleting Spaces');
		$this->addValidation($validation);
	}

	/**
	 * Performs a validation to ensure that the text provided is an integer. The only permitted values are numeric values.
	 * @param string $field_id
	 */
	protected function validateIsInteger($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is Integer');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		if( preg_match( '/[^\d]+/', $field->submittedValue() ) )
		{		
			$validation->failFieldWithMessage($field->title(), 'Must be an integer. The text can only contain numbers (0 to 9)');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to ensure that the text provided is a float. The only permitted values are 0 to 9 and a decimal.
	 *
	 * @param string $field_id
	 */
	protected function validateIsFloat($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is Float');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		if(!preg_match('/^(?:\d+|\d*\.\d+)$/', $field->submittedValue()) && trim($field->submittedValue()) != '')
		{		
			$validation->failFieldWithMessage($field->title(), 'Must be a float. The text can only contain numbers (0 to 9) and a single decimal.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to ensure that the text provided only contains numbers and spaces
	 * @param string $field_id
	 */
	protected function validateIsNumbersAndSpaces($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is Numbers+Spaces');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		if( preg_match( '/[^01234567890 ]+/', $field->submittedValue() ) )
		{
			$validation->failFieldWithMessage($field->title(), 'Must only contain numbers and spaces.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to ensure that the text provided only contains numbers and spaces
	 * @param string $field_id
	 */
	protected function validateAsURLSafe($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate Is URL Safe');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		$value =$field->submittedValue();
		
		// Allow a value of homepage otherwise, just URL safe
		if($value != '/' && !preg_match( '/^[a-zA-Z0-9_-]*$/',$value  ) )
		{
			$validation->failFieldWithMessage($field->title(), 'Must only contain numbers, basic letters, dashes, and underscores.');
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * Performs a validation to ensure that the text provided has no HTML.
	 * @param string $field_id
	 */
	protected function validateNoHTML($field_id)
	{
		$validation = new TCm_FormItemValidation($field_id, 'Validate No HTML');
		$field = $this->form_item_views[$field_id];
		TC_assertObjectType($field, 'TCv_FormItem');
		
		$submitted_value = $field->submittedValue();
		
		// Turn them into arrays
		if(!is_array($submitted_value))
		{
			$submitted_value = [$submitted_value];
		}
		
		foreach($submitted_value as $test_value)
		{
			if($test_value != strip_tags($test_value))
			{
				$validation->failFieldWithMessage($field->title(), 'Value cannot include HTML');
			}
		}
		// Add them directly
		$this->addValidation($validation);
	}
	
	/**
	 * A validation that is run outside of the normal values to check if it contains malicious code. This function
	 * contains a growing list of items that we look for and reject on absolutely ANY input.
	 * @param array $values
	 */
	protected function validateNoInjectionFlags($values)
	{
		$field_id = $values['id'];
		$validation = new TCm_FormItemValidation($field_id, 'Validate malicious code');
		$validation->disableConsoleLogging();
		
		$flags = [
			'base64_decode',
			'eval('
		];
		
		$field = $this->form_item_views[$field_id];
		$submitted_value = $field->submittedValue();
		if(!is_array($submitted_value))
		{
			$submitted_value = [$submitted_value];
		}
		
		foreach($flags as $bad_code)
		{
			foreach($submitted_value as $test_value)
			{
				if(strpos($test_value,$bad_code) !== false)
				{
					
					$validation->failFieldWithMessage($field->title(), 'Value contains potentially malicious code. ');
				}
			}
			
		}
		
		// Add them directly
		$this->addValidation($validation);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// DATABASE
	//
	//////////////////////////////////////////////////////

	/**
	 * Updates the database with all the relevant information provided
	 * @param bool $force_insert  A flag to force the update to perform an insert, regardless of what the processor settings seem to indicate.
	 * @uses TCc_FormProcessorWithModel::processDatabaseQuerySnippets()
	 * @uses TCc_FormProcessor::performPostUpdateActions()
	 * @uses TCc_FormProcessor::performPostUpdateMethodCalls()
	 */
	public function updateDatabase(bool $force_insert = false)
	{
		$this->processDatabaseQuerySnippets();
		
		// Just in case, validate one last time
		if($this->isValid())
		{
			parent::updateDatabase($force_insert);
		}
		
		if($this->isValid())
		{
			$this->performActionsBetweenUpdateAndPostActions();
		}
		
		if($this->isValid() && !$this->post_updates_called) // ONLY RUN THEM IF VALID, AND ONLY ONCE
		{
			$this->post_updates_called = true;
			$this->performPostUpdateActions();
			$this->performPostUpdateMethodCalls();
		}
		
	}

	/**
	 * A hook method that lets us perform actions after the update has occurred but before the "post" actions happen.
	 */
	protected function performActionsBetweenUpdateAndPostActions()
	{

	}
	
	/**
	 * Compiles the list of database query snippets.
	 * @uses TCc_FormProcessor::addDatabaseValue()
	 */
	protected function processDatabaseQuerySnippets()
	{
		// Get Snippets from each form item
		foreach($this->form_tracker->formItems() as $id => $form_item_values)
		{
			$form_item_view = $this->form_item_views[$form_item_values['id']];
			if($form_item_view->saveToDatabase() && $id != 'db_primary_value')
			{
				foreach($this->valuesForField($id, true) as $value_id => $value)
				{
					if(!isset($this->database_values[$value_id]))
					{
						$this->addDatabaseValue($value_id, $value);
					}
				}
			}
		}
	}
	
	
	/**
	 * Performs actions that should occur immediately after an update, before any post method calls are done.
	 */
	protected function performPostUpdateActions()
	{
		// Save the ID of the new created item to the tracker.
		if(isset($this->form_tracker))
		{
			$this->form_tracker->setPrimaryValue($this->databasePrimaryValue());
		}
		
	}


	/**
	 * Forces the form process to always save the values for this form.
	 *
	 * WARNING : this should only be used in debugging and testing. This prevents a form from being successfully
	 * wiped after being used.
	 */
	public function forceSaveFormValues()
	{
		$this->form_tracker_always_save = true;
	}

	/**
	 * Completes the processor
	 * @param bool $redirect (Optional) Default true. Indicates if the processor redirects
	 */
	public function finish($redirect = true)
	{
		
		// Fail the processor if we detected any errors along the way
		if(TC_numProcessErrors() > 0)
		{
			$this->fail();
			// Track any failed field IDs that might have been identified
			foreach(TC_processErrors() as $error_values)
			{
				// if the field ID is set AND we have a form item with that field ID
				if(isset($error_values['field_id']) && $this->formItemWithID($error_values['field_id']) )
				{
					// Flag the field ID
					$this->form_tracker->setInvalidField($error_values['field_id']);
				}
			
			}
		}
		
		if($this->isValid() && !$this->form_tracker_always_save)
		{
			unset($_SESSION['TCm_FormTracker'][$this->form_tracker_session_name]);
		}
		else
		{
			$_SESSION['TCm_FormTracker'][$this->form_tracker_session_name] = $this->form_tracker;
		}
		
		parent::finish($redirect);
	
	}

	/**
	 * Removes a database value from the processor
	 *
	 * @param string $column The database column
	 */
	public function removeDatabaseValue($column)
	{
		parent::removeDatabaseValue($column);
		$this->form_tracker->removeFormItemWithID($column);

	}





	//////////////////////////////////////////////////////
	//
	// CONSOLE
	//
	//////////////////////////////////////////////////////

	/**
	 * Tracks the form item in the console
	 * @param string $form_item_id
	 */
	protected function trackFormItemInConsole($form_item_id)
	{
		$values = $this->form_tracker->propertiesForFormItemID($form_item_id);
		
		$message_column_1 = $values['id'];
		
		
		
		$submitted_value = $values['submitted_value'];
		if(is_array($submitted_value))
		{
			$submitted_value = implode('<br />', $submitted_value);
		}
		else
		{
			$submitted_value = htmlspecialchars($submitted_value);
		}
		
		// Hide passwords
		if($values['view_class_name'] == 'TCv_FormItem_Password')
		{
			if(strlen($submitted_value) >0)
			{
				$submitted_value =  $submitted_value[0] .str_pad('', strlen($submitted_value)-2, '*'). substr($submitted_value,-1);
			}
			
		}
		$message_column_2 = $submitted_value;
		
		$details = 'Submitted Value : <em>'.$submitted_value.'</em>';
		$details .= '<br />Save to Database : <em>'.($values['save_to_database'] ? 'Yes' : 'No').'</em>';
		
		$process_calls = array();
		foreach($values['process_calls'] as $function_values)
		{
			$process_calls[] = $function_values['method_name'];
		}
		if(sizeof($process_calls) > 0)
		{
			$details .= '<br />Process Calls : <em>'.implode(', ' , $process_calls).'</em>';
		}
		
		$console_item = new TSm_ConsoleItem('', TSm_ConsoleFormValue);
		$console_item->setClassName($values['view_class_name']);
		$console_item->setThreeColumnMessage($message_column_1, $message_column_2, '' );
		
		$console_item->setDetails($details);
		
		$this->addConsoleItem($console_item);
		
	}	
		
	
	
	
}
?>