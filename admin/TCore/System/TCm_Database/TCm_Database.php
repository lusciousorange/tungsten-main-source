<?php
/**
 * Class TCm_Database
 *
 * This is an extension of PHP's PDO class which allows for a few features necessary for Tungsten.
 *
 */
class TCm_Database extends PDO
{
	protected string $database_name = '';
	private $transaction_id = 0;
	private $has_error = false;
	private $inserted_id = 0;
	private $last_error_message;
	private static $connection = NULL;
	
	public static $installed_tables = null;
	
	/**
	 * The array of connections that are open, indexed by database_name
	 * @var TCm_Database[]
	 */
	public static array $connections = [];
	
	public function __construct(string $dsn, ?string $username = null, ?string $password = null, ?array $options = null)
	{
		parent::__construct($dsn, $username, $password, $options);
		
		// Extract the Database name
		// Can't use config, the entire point to deal with multiple connections
		$dsn_parts = explode(';', $dsn);
		foreach($dsn_parts as $part)
		{
			if(str_starts_with($part,'dbname'))
			{
				$this->database_name = substr($part, 7);
			}
		}
		
	}
	
	/**
	 * Returns the database name for this connection
	 * @return string
	 */
	public function databaseName(): string
	{
		return $this->database_name;
	}
	
	
	/**
	 *
	 */
	private function __clone()
	{
	
	}
	
	/**
	 * Extends the existing functionality of PDO to track transaction IDs.
	 * @param string $query
	 * @param array $options
	 * @return PDOStatement
	 */
	public function prepare(string $query, array $options = []) : PDOStatement
	{
		$statement = parent::prepare($query, $options);
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		
		return $statement;
		
	}
	
	/**
	 * Sets the inserted ID which ignores console inserts
	 * @param int $id
	 */
	public function setInsertedID($id) : void
	{
		$this->inserted_id = $id;
	}
	
	/**
	 * Returns the inserted ID
	 * @return int
	 */
	public function insertedID()
	{
		return $this->inserted_id;
	}
	
	/**
	 * Extended to track the transaction ID
	 */
	public function beginTransaction() : bool
	{
		
		$this->transaction_id++;
		return parent::beginTransaction();
	}
	
	/**
	 * Returns the transaction ID
	 * @return int
	 */
	public function transactionID() : int
	{
		if($this->inTransaction())
		{
			return $this->transaction_id;
		}
		return 0;
	}
	
	
	/**
	 * Returns if an error was found in the processing
	 * @return bool
	 */
	public function errorFound() : bool
	{
		return $this->errorCode() != '00000' || $this->has_error;
	}
	
	/**
	 * Sets this database to have an error.
	 * @param string|bool $error_message (Optional) Default false. The error message
	 * @return bool
	 */
	public function setHasError($error_message = false)
	{
		if($error_message)
		{
			$this->last_error_message = $error_message;
		}
		return $this->has_error = true;
		
	}
	
	/**
	 * Returns if this database has an error.
	 * @return bool
	 */
	public function hasError()
	{
		return $this->has_error;
	}
	
	/**
	 * Returns the last error message.
	 * @return string
	 */
	public function lastErrorMessage()
	{
		return $this->last_error_message;
	}
	
	/**
	 * Extends the PDO rollback() method to deal with console messages
	 * @param null|string $message
	 * @return bool TRUE on success or FALSE on failure.
	 */
	public function rollback(?string $message = null) : bool
	{
		$this->setHasError();
		
		$model = new TCm_Model(false);
		$console_item = new TSm_ConsoleItem('Database Rollback', TSm_ConsoleMessage, TSm_ConsoleIsError);
		$console_item->setClassName(get_class($this));
		if($message)
		{
			$console_item->setDetails($message);
		}
		$model->addConsoleItem($console_item);
		try
		{
			return parent::rollback();
		}
		catch(Exception $e)
		{
			return false;
		}
		
	}
	
	/**
	 * Resets the connection to the DB.
	 *
	 * @see TCm_Database::connection()
	 */
	public static function resetConnection()
	{
		self::$connection = NULL;
		static::connection();
	}
	
	public static function openConnection($host, $database, $user, $pass, $port = null, $type = 'mysql') : ?TCm_Database
	{
		// Existing connection not found in our list of connections
		if(!array_key_exists($database, static::$connections))
		{
			//print '==================> OPEN: '.$database."\n";
			if($type == 'mysql')
			{
				$connection_string = "mysql:host=" . $host . ";dbname=" . $database . ";charset=utf8";
				if($port)
				{
					$connection_string .= ';port=' . $port;
				}
				try
				{
					
					static::$connections[$database] = new TCm_Database($connection_string, $user, $pass);
					
					// Set names to UTF-8
					$query = "SET NAMES utf8";
					$statement = static::$connections[$database]->prepare($query);
					$statement->execute();
					
				} catch(Exception $e)
				{
					// Detect the maintenance file if it exists
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/maintenance.html'))
					{
						header("Location: /maintenance.html");
						exit();
					}
					// Don't mess around. Dump the error and exit.
					print 'Database Connection Failure';
					exit();
				}
				
				// Try timezones, but don't let that stop the DB from running
				try
				{
					// Set the time zone
					$timezone = TC_getConfig('DB_timezone');
					if(is_string($timezone))
					{
						$query = "SET time_zone = '" . $timezone . "'";
						$statement = static::$connections[$database]->prepare($query);
						$statement->execute();
					}
					
					// Breaks in PHP 8
//					$query = "SET GLOBAL max_allowed_packet=524288000";
//					$statement =  static::$connections[$database]->prepare($query);
//					$statement->execute();
//
				} catch(Exception $e)
				{
				}
				
				
			}
			
			if(static::$connections[$database])
			{
				static::$connections[$database]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			
		}
		
		// Return the value no matter what
		return static::$connections[$database];
		
	}
	
	/**
	 * Closes all the connections by destroying the PDO objects themselves
	 * @return void
	 */
	public static function closeAllConnections() : void
	{
		//print "xxxxxxxxxxxx Closing all connections\n";
		foreach(static::$connections as $index => $connection)
		{
			static::$connections[$index] = null;
		}
		
		// Reset the values so they aren't found
		static::$connections = [];
		self::$connection = null;
	}
	
	/**
	 * Returns a connection to the database. This uses the default values in the config to trigger and calls the more
	 * generic openConnection() method to ensure we're saving all of these properly.
	 *
	 * This is a singleton method that will return the connection if it exists, otherwise it will create the connection
	 * using the values in the config.
	 * @return null|TCm_Database
	 */
	public static function connection()
	{
		// Maintains the original self::$connection
		self::$connection = static::openConnection(TC_getConfig('DB_hostname'),
		                                           TC_getConfig('DB_database'),
		                                           TC_getConfig('DB_username'),
		                                           TC_getConfig('DB_password'),
		                                           TC_getConfig('DB_port')
		);
		
		return self::$connection;
	}
	
	/**
	 * Returns the tables that are installed
	 * @return string[]|null
	 */
	public static function installedTables()
	{
		if(static::$installed_tables == null)
		{
			
			
			$statement = self::$connection->prepare('SHOW TABLES');
			$statement->execute();
			
			while($row = $statement->fetch())
			{
				$table_name = reset($row);
				static::$installed_tables[$table_name] = $table_name;
				
			}
		}
		
		return static::$installed_tables;
	}
	
	/**
	 * Returns if the given table is installed
	 * @param string $table_name
	 * @return bool
	 */
	public static function tableInstalled(string $table_name)
	{
		return isset(static::installedTables()[$table_name]);
	}
	
}
