<?php
	session_start();
	header('Content-type: text/javascript');
	ob_start("compress");
	
	function compress($buffer) 
	{
		/* remove comments */
		//$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		/* remove tabs, spaces, newlines, etc. */
		//$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '	', '		', '		'), '', $buffer);
		return $buffer;
	}
	
	
	// GRAB FILES
	$js_calls = $_SESSION['compress_js_'.htmlspecialchars($_GET['id'])];
	$output = '';
	
	if(is_array($js_calls))
	{
		foreach($js_calls as $id => $url)
		{
			$output .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$url);
		}
	}
	print $output;
	
	
	ob_end_flush();
?>