<?php

/**
 * Class TCv_Website
 *
 * The primary class for representing a website view. This is an extension of the TCv_View class which adds in the functionality
 * for generating the entire html for the site.
 */
class TCv_Website extends TCv_View
{
	protected $domain; // [string] The the domain of the webpage
	protected ?array $sections; // The list of folders for the page being viewed
	protected string $section_name = '';
	// in the root that is being viewed
	protected bool $is_beta_site = false; // [bool] = Indicates if the website being viewed is a beta website
	protected bool $is_admin_site = false;
	protected bool $system_installed = false;
	protected array $plugins = [];
	
	protected $meta_tags = array(); // [array] = The list of meta tags
	
	// HEAD settings
	protected string $description = 'Description'; // [string] = The description that appears in the head
	protected string $favicon = '/images/favicon.ico'; // [string] = The location of the favicon. Defaults to /images/favicon.ico
	protected $title = false; // [string] = The title of the page
	protected $title_locked = false;
	protected $title_prefix = false;
	protected ?TMm_User $user = null;
	
	// DATABASES
	protected $database_handle = false;
	
	// CONTENT
	protected array $body_content = array(); // [array] = List of html content that is to be shown
	protected bool $messenger_attached = false;
	
	// CONSOLE
	protected $timer = false;
	
	// MAIN CONTENT ITEMS
	protected $html_tag = false;
	protected $head_tag = false;
	protected $body_tag = false;
	
	protected array $installed_plugins = array();
	
	protected bool $performance_tracking = false;
	
	protected static ?TCv_Website $website = NULL;
	
	public static $table_id_column = false;
	public static $table_name = false;
	public static $model_title = false;
	
	protected static string $commit_version = '';
	
	/**
	 * TCv_Website constructor.
	 * @param string $id (Optional) Default "website"
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		
		$this->addJSFile('Tungsten_js','/admin/TCore/System/Tungsten.min.js?v'.TCv_Website::gitVersion());
		$this->addJSLine('window.W_init','window.W = {}; // Tungsten storage object');
		
		$this->domain = $_SERVER['HTTP_HOST'] ?? '';
		$this->sections = explode("/",$_SERVER['PHP_SELF']); // split the requested url
		array_shift($this->sections); // remove domain
		if(isset($this->sections[0]))
		{
			$this->section_name = $this->sections[0]; // 0 is the domain
		}
		
		ini_set('default_charset','UTF-8');
		ini_set('memory_limit', '512M');
	
		
		// By default run JQuery
		//$this->addJSJQueryLibrary();
		
		$this->cleanSession();
		
		$this->html_tag = new TCv_View();
		$this->html_tag->setTag('html');
		$this->html_tag->setAttribute('lang', 'en');
		
		$this->head_tag = new TCv_View();
		$this->head_tag->setTag('head');
		
		$this->body_tag = new TCv_View($this->id());
		$this->body_tag->setTag('body');
		$this->configureBodyTag();
		
		$this->addCSSFile('reset', '/admin/TCore/System/TCv_Website/reset.css');
		
		
		if($this->section_name != 'index.php')
		{
			$this->body_tag->addClass('site_section_'.htmlspecialchars($this->section_name));
		}
		
		$this->addMetaTag('viewport', '<meta name="viewport" content="width=device-width, initial-scale=1">');
		
		try 
		{
        	$db = TCm_Database::connection();
        	$result = $db->query("SELECT 1 FROM modules LIMIT 1");
        	$this->system_installed = true;
    	} 
    	catch (Exception $e) 
    	{
        	$this->system_installed = false;
    	}
    	
		
		
		// CONFIGURE BETA SETTINGS
		if($this->system_installed)
		{
			$this->is_beta_site = TC_getConfig('is_staging_domain');
		}
		
		//$this->configureErrorDisplaySettings($this->is_beta_site);
		
		
		if(TC_getConfig('is_localhost'))
		{
			$this->addClass('localhost_website');
		}
		
		if(TC_getConfig('is_staging_domain'))
		{
			$this->addClass('staging_website');
		}	
		
		$this->loadPlugins(false); // don't wait for rendering
		
		// Load Console List to clear old ones
		$console_list = new TSm_ConsoleItemList();
		
		// Load TSv_ModuleURLTargetLink JS file
		$this->addClassJSFile('TSv_ModuleURLTargetLink');
		
		
	}
	
	/**
	 * Saves the version of git for this website. If not possible or not found, then a manual TC_config value called
	 * 'site_version' can be used to increase the value
	 */
	protected static function saveGitVersion() : void
	{
		if(static::$commit_version == '')
		{
			// Version.txt File which is created by the cloudflare sync
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/version.txt'))
			{
				static::$commit_version = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/version.txt'));
				return;
			}
			
			// Version number in TC config
			$local_version = TC_getConfig('site_version');
			if(is_string($local_version) && $local_version != '')
			{
				static::$commit_version = trim($local_version);
				return;
			}
			
			// Get a version ID from the repo itself
			$rev = exec('git rev-parse --short HEAD');
			if(substr($rev,0,5) != 'fatal' && $rev != '')
			{
				static::$commit_version = trim($rev);
			}
			
		}
		
	}
	
	/**
	 * Returns the git version for this website
	 * @return string
	 */
	public static function gitVersion() : string
	{
		// Try using memcached to store this and save some processing grief
		$mem_key = 'git_version';
		
		$mem_value = TC_Memcached::get($mem_key);

		// If it's null OR if it's an empty string, we need to get the latest
		if(is_null($mem_value) || $mem_value == '')
		{
			// Not found, save for the next 5 minutes
			static::saveGitVersion();
			$mem_value = static::$commit_version;
			TC_Memcached::set($mem_key, $mem_value, 300);
		}
		
		return $mem_value;
	}
	
	//////////////////////////////////////////////////////
	//
	// SINGLETON SETUP
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A singleton method that always returns the current instance of the website.
	 * @return TCv_Website|bool
	 */
	public static function website()
	{
	    if (!self::$website)
		{
			self::$website = static::init();
	
			// load localization as the first item if this is what we need
			if(TC_getConfig('use_localization') && class_exists('TMm_LocalizationModule'))
			{
				$settings = TMm_LocalizationModule::init();
			}
			TC_saveActiveModel(self::$website);
			
		}
		return self::$website;
    }
	
	/**
	 * Returns if the install is complete. This is indicated by the system module being installed, but the "install" module is not.
	 * @return bool
	 */
	public function installComplete()
	{
		$module_list = TSm_ModuleList::init();
		return $module_list->installComplete();
	}
	
	/**
	 * Singleton to return the instance of the website class
	 */
	public static function checkForUpgrade()
	{
		$console_setting = TC_getConfig('console_saving');
		TC_setConfig('console_saving', false);
		
		// Don't bother if we aren't done installing yet
		$module_list = TSm_ModuleList::init();
		if(!$module_list->installComplete())
		{
			return;
		}
		

		
		include_once($_SERVER['DOCUMENT_ROOT'].'/admin/system/config/system_version.php');
		
		// System Version is not up to date
		if(TC_getModuleConfig('system','system_version') != $admin_version)
		{
			foreach($module_list->modules() as $module)
			{
				if($module->requiresUpgrade())
				{
					$module->install();	
				}
				
			}
			TC_setModuleConfig('system','system_version', $admin_version);
		}
		
		TC_setConfig('console_saving', $console_setting);
		
	}

	//////////////////////////////////////////////////////
	//
	// USERS
	//
	// Methods related to a logged in user.
	//
	//////////////////////////////////////////////////////


	/**
	 * @param ?int $id The ID of the user that is currently logged in
	 */
	public function setUserID(?int $id) : void
	{
		// Don't bother if we have a null or zero value. That's not a user
		if(is_null($id) || $id === 0)
		{
			return;
		}
		
		// Detect impersonation and swap to the new ID
		if(TC_getConfig('use_impersonate_user') && isset($_SESSION['impersonation_id']))
		{
			$id = $_SESSION['impersonation_id'];
			$this->addClass('impersonation');
		}

		if($id > 0 && $user = TMm_User::init($id))
		{
			$this->user = $user;

			// save the current model
			// Disabled because it messes with every other instance of viewing a user
			// Current user now has a special case of 'current-user' as hte class name
			//TC_saveActiveModel(TC_currentUser());
			
	
		
		
		}	
	}	
	
	/**
	 * Returns the currently logged in user. Null if not logged in
	 * @return ?|TMm_User
	 */
	public function user() :?TMm_User
	{
		return $this->user;
	}	
	
	/**
	 * Returns the id of the user that is currently logged in. Null if not logged in
	 * @return ?int
	 */
	public function userID() : ?int
	{
		if($this->user)
		{
			return $this->user->id();
		}
		return null;
	}	
	
	/**
	 * Returns if there is currently a user logged in.
	 * @return bool
	 */
	public function isLoggedIn() : bool
	{
		return $this->user instanceof TMm_User;
	}

	/**
	 * Cancels the impersonation
	 */
	public static function cancelPublicImpersonation()
	{
		unset($_SESSION['impersonation_id']);
	}

	//////////////////////////////////////////////////////
	//
	// BETA SITES
	//
	// Methods related to beta website flagging
	//
	//////////////////////////////////////////////////////

	/**
	 * Indicates if the website being viewed is a beta website
	 * @param bool $is_beta
	 */
	public function setIsBetaSite($is_beta)
	{
		$this->is_beta_site = $is_beta;
	}
	
	/**
	 * Indicates if the website being viewed is a beta website
	 * @return bool
	 */
	public function isBetaSite()
	{
		return $this->is_beta_site;
	}
	
	/**
	 * A view that shows the message if the site is in a staging or localhost mode.
	 * @return TCv_View|bool
	 * @deprecated
	 */
	public function stagingMessageBar()
	{

		$warning = new TCv_View();
		$warning->addClass('staging_message');
		$warning->addText('<i class="fa fa-exclamation-triangle"></i> ');
		
		if(TC_getConfig('is_localhost'))
		{
			$warning->addText('<strong>Localhost</strong> – You are currently viewing a localhost domain, not the live website.');
			$warning->addClass('localhost_message');
			return $warning;
		}
		elseif(TC_getConfig('is_staging_domain'))
		{
			$warning->addText('<strong>Staging</strong> – You are currently viewing a staging domain, not the live website.');
			return $warning;
		}	
		return false;
		
	}

	//////////////////////////////////////////////////////
	//
	// SESSION MAINTENANCE
	//
	//////////////////////////////////////////////////////

	/**
	 * Performs maintenance on the session to ensure the website continues to load quickly
	 */
	public function cleanSession()
	{
		foreach($_SESSION as $id => $name)
		{
			if(is_string($name) && $name == '__PHP_Incomplete_Class')
			{
				unset($_SESSION[$id]);
			}
		}
		

		
	
	}

	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	//////////////////////////////////////////////////////

	/**
	 * Takes any messenger information that is stored in the messenger, outputs a messenger and then wipes the messenger to avoid repeated output.
	 */
	public function addMessengerResultsToContent()
	{
		$messenger = $this->messenger();
		if($messenger instanceof TCv_Messenger)
		{
			$this->attachView($messenger);
			
		}
	}
	
	/**
	 * Returns the messenger in use
	 * @return bool|TCv_Messenger
	 */
	public function messenger()
	{
		if($this->messenger_attached)
		{
			return false;
		}
		
		$this->messenger_attached = true;
		return TCv_Messenger::instance();
	}



	//////////////////////////////////////////////////////
	//
	// DOMAIN AND URL
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the domain of the website
	 * @return string
	 */
	public function domain()
	{
		return $this->domain;
	}
	
	/**
	 * Returns the sections of the url which are divided by the backslash "/"
	 * @return string[]
	 */
	public function URLSections()
	{
		return $this->sections;
	}
	
	/**
	 * Returns the text in a particular url section
	 * @param int $index
	 * @return string
	 */
	public function URLSection($index)
	{
		return @$this->sections[$index];
	}
	
	/**
	 * Returns the section name for this page. This is the string associated with the URL sections, ie folders.
	 * @return string
	 */
	public function sectionName()
	{
		return $this->section_name;
	}
	
	/**
	 * Set the title prefix that will appear in the meta tag only and subsequently in the window title. The "prefix"
	 * is actually the suffix now as it's best to include it after.
	 * @param string $prefix
	 */
	public function setTitlePrefix(string $prefix)
	{
		$this->title_prefix = str_ireplace('-','', $prefix);
	}
	
	/**
	 * Returns false since we use page titles to deal with this
	 * @return bool
	 */
	public function isUsableAsPageTitle()
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// PLUGINS
	//
	//////////////////////////////////////////////////////


	/**
	 * Loads the installed plugins
	 * @param bool $wait_for_rendering Indicates if the plugins to be loaded are those that should wait for rendering
	 */
	public function loadPlugins($wait_for_rendering)
	{
		$plugins = TSm_PluginList::init();
		foreach($plugins->installedPlugins() as $plugin_model)
		{
			if($wait_for_rendering == $plugin_model->waitsForRendering())
			{
				/** @var TSm_Plugin $class_name */
				$class_name = $plugin_model->className();

				$view_class = $class_name::init($plugin_model);
				if($view_class->determineIfShown($this->isBetaSite(), $this->is_admin_site))
				{
					$this->attachView($view_class);
					$this->plugins[$plugin_model->className()] = $plugin_model;
				}
				else
				{
					$view_class->processNotLoaded();
				}
			}
		}
	}
	
	/**
	 * Returns if a given plugin is installed
	 * @param string $plugin_class_name The name of the plugin
	 * @return bool
	 */
	public function pluginInstalled($plugin_class_name)
	{
		$plugins = TSm_PluginList::init();
		return $plugins->pluginIsInstalled($plugin_class_name);
	}
	
	/**
	 * Return the plugin with the given name, if it's installed
	 * @param string $plugin_class_name
	 * @return bool|TSm_Plugin
	 */
	public function pluginModelWithName($plugin_class_name)
	{
		$plugins = TSm_PluginList::init();
		foreach($plugins->installedPlugins() as $plugin_model)
		{
			if($plugin_model->className() == $plugin_class_name)
			{
				return $plugin_model;
			}
		}
		return false;
	}
	
	/**
	 * Returns the plugin view with the given class name
	 * @param string $plugin_class_name
	 * @return bool|TSv_Plugin
	 */
	public function pluginViewWithName($plugin_class_name)
	{
		$plugin_model = $this->pluginModelWithName($plugin_class_name);
		/** @var TSm_Plugin $class_name */
		$class_name = $plugin_model->className();

		$view_class = $class_name::init($plugin_model);
		return $view_class;
		
	}

	//////////////////////////////////////////////////////
	//
	// SOCIAL MEDIA
	//
	//////////////////////////////////////////////////////

	/**
	 * Includes the code that lets twitter integration happen
	 */
	public function addTwitter_API()
	{
		$this->addJSLine('twitter', 'window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));', true);
	
	}
	
	/**
	 * Sets the open graph title
	 * @param string $title
	 */
	public function setOpenGraphTitle($title)
	{
		$this->addMetaTag('og:title', array('property' => 'og:title', 'content' => $title));
	}
	
	/**
	 * Sets the open graph type
	 * @param string $type
	 */
	public function setOpenGraphType($type)
	{
		$this->addMetaTag('og:type', array('property' => 'og:type', 'content' => $type));
	}
	
	/**
	 * Sets the open graph image
	 * @param string $image
	 */
	public function setOpenGraphImage($image)
	{
		$this->addMetaTag('og:image', array('property' => 'og:image', 'content' => $image));
	}
	
	/**
	 * Sets the open graph url
	 * @param string $url
	 */
	public function setOpenGraphURL($url)
	{
		$this->addMetaTag('og:url', array('property' => 'og:url', 'content' => $url));
	}

	/**
	 * Sets the open graph site name
	 * @param string $site_name
	 */
	public function setOpenGraphSiteName($site_name)
	{
		$this->addMetaTag('og:site_name', array('property' => 'og:site_name', 'content' => $site_name));
	}

	/**
	 * Sets the open graph facebook admin ids
	 * @param string $ids
	 */
	public function setOpenGraphFacebookAdminIDs($ids)
	{
		$this->addMetaTag('og:admins', array('content' => $ids));
	}
	
	/**
	 * Sets the open graph title description
	 * @param string $description
	 */
	public function setOpenGraphDescription($description)
	{
		$this->addMetaTag('og:description', array('property' => 'og:description', 'content' => $description));
	}


	//////////////////////////////////////////////////////
	//
	// CONTENT AND VIEWS
	//
	//////////////////////////////////////////////////////


	/**
	 * Adds HTML content that will shown in the order that it is provided
	 * @param string $id The ID for this content item
	 * @param string $html THe html to be added
	 * @param bool|int $position (Optional) Default false. The position in the series of content where this content should be added.
	 */
	public function addHTMLContent($id, $html, $position = false)
	{
//		if(isset($this->body_content[$id]))
//		{
//		//	TC_triggerError('Duplicate Content ID:  "'.htmlspecialchars($id).'" has already been assigned');
//		}
		
		if($position !== false)
		{
			$new_content = array();
			$index = 0;
			foreach($this->body_content as $item_id => $item_html)
			{
				if($index == $position)
				{
					$new_content[$id] = $html; 
				}
				
				// add the existing one regardless
				$new_content[$item_id] = $item_html;
				$index++;
			}
			
			// handle blank page with only the header setup
			if(sizeof($this->body_content) < 1)
			{
				$new_content[$id] = $html; 
			}
			
			$this->body_content = $new_content;
		}
		else
		{
			$this->body_content[$id] = $html;
		}
	}
	
	/**
	 * Overrides the default operation of attachView. This method calls the parent's attachView to perform all the normal
	 * operation but it also grabs the html from the view and adds it to the website using addHTMLContent
	 * @param bool|TCv_View $view The view to be added
	 * @param bool|int $position (Optional) Default false. The position in terms of the view items that it should be added.
	 * @return void
	 */
	public function attachView($view, $position = false)
	{
		if($view === false || is_null($view))
		{
			return;
		}
	
		// deal with just adding strings
		if(is_string($view))
		{
			$this->addHTMLContent($view, $view, $position);
			return;
		}
		
		
		$view_html = parent::attachView($view);
		
		if($view_html instanceof TCv_View)
		{
			$view_html = $view_html->html();
		}
		
		if($view->id() == '' || $view->id() == false)
		{
			$id_num = @$_SESSION['num_blank_objects_attached']++;
			$id = 'TCv_Website_non_id_item_'.$id_num;
			$view->setID($id);
		}
		// Grab the HTML, this may call additional attach views
		$this->addHTMLContent($view->id(), $view_html, $position);
		
	}
	
	/**
	 * Removes all views from this website
	 */
	public function detachAllViews()
	{
		$this->body_content = array();
		parent::detachAllViews();
	}

	/**
	 * Sets the description value for this website
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}


	/**
	 * Returns the doctype for this website
	 * @return string
	 */
	public function doctype()
	{
		return '<!doctype html>';
	}

	/**
	 * Changes the title of the page
	 * @param string $title
	 */
	public function setTitle($title)
	{
		if(!$this->title_locked)
		{
			$this->title = $title;
		}
	}

	/**
	 * Locks the title to avoid any future title changes.
	 */
	public function lockTitle()
	{
		$this->title_locked = true;
	}

	/**
	 * Changes the favicon of the page
	 * @param string $favicon
	 */
	public function setFavicon($favicon)
	{
		$this->favicon = $favicon;
	}

	/**
	 * Adds direct HTML to the head of the document.
	 * @param string $html
	 * @deprecated This performs no action anymore
	 */
	public function addHeadHTML($html)
	{
		//$this->headHTML[] = $html;
	}

	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION
	//
	//////////////////////////////////////////////////////

	/**
	 * Check that the email and password provided equate to a real person in the Users system. It will check that the
	 * credentials provided match a user in the system.  It will also check that the user is a member of one of the groups
	 * that are permitted to vie at least one module in Tungsten.
	 * @param string $email
	 * @param string $password
	 * @return bool|TMm_User
	 */
	public function authenticateEmailPassword($email, $password)
	{

		if (!isset($email) || !isset($password))
	    {			
			return false;
		}
		
		if($user = TMm_User::classNameForInit()::userWithEmail($email))
		{
			if($user->authenticatePassword($password))
			{
				return $user;
			}
			else
			{
				return false;
			}
		}
			
		return false;
	
	}

	/**
	 * Logs the user out of the website
	 * @param string|bool $message The message to be shown on logout. If a value of null is passed, it will do the
	 * default action. If a value of `false` is passed, it will not show a message.
	 */
	public function logout($message = null)
	{
		$original_page = @$_SESSION['original_page'];
	  	// Destroy the session.
	  	
	  	session_destroy();
	  	session_start();
		session_regenerate_id(); // delete the old session id regenerate a new id
	
		
		// SILENT LOGOUT
		if($message === false)
		{
		
		}
		// custom value passed
		elseif($message != null)
		{
			TC_message($message, false);
		}
		elseif(TC_getConfig('use_localization'))
		{
			TC_message(TC_localize('logout_complete','Logout Complete'));
		}
		else
		{
			TC_message('Logout Complete');
		}
		
	  	$_SESSION['original_page'] = $original_page;
		TSv_GoogleAnalytics::trackEvent('logout');
		
	}



	//////////////////////////////////////////////////////
	//
	// BODY TAG
	//
	//////////////////////////////////////////////////////



	/**
	 * Add a class to the body of this website
	 * @param string $class_name
	 * @deprecated
	 * @see TCv_Website::addClass()
	 */
	public function addBodyClass($class_name)
	{
		$this->addClass($class_name);
	}
	
	/**
	 * Add a class to the body of this website
	 * @param ?string $class_name The CSS class anme to be added to the
	 *
	 */
	public function addClass(?string $class_name) : void
	{
		if($this->body_tag)
		{
			$this->body_tag->addClass($class_name);
		}	
		else
		{
			parent::addClass($class_name);
		}
	}
	
	/**
	 * Handle the basic configuration of the body tag
	 */
	public function configureBodyTag()
	{
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') > 0) { $this->body_tag->addClass('ie6'); }
		elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') > 0) { $this->body_tag->addClass('ie7'); }
		elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8') > 0) { $this->body_tag->addClass('ie8'); }
		elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9') > 0) { $this->body_tag->addClass('ie9'); }
		elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9') > 0) { $this->body_tag->addClass('ie10'); }
		
		$this->body_tag->addClass($this->classesString());
	}
	
	
	/**
	 * The html for all the body content
	 * @return string
	 */
	public function bodyContent()
	{
		$html = '';
		foreach($this->body_content as $id => $body_content_html)
		{
			$html .= "\n\t\t".$body_content_html; 
		}
		
		return $html;
	}
	
	/**
	 * This function processes the body tag and adds the relevant information to it such as the main content, etc. If
	 * modifications are meant to be done to the body tag on a larger scale, then it should occur within this function
	 * but be sure to be aware of if you are calling the parent::completeBodyTag function as well.
	 */
	public function completeBodyTag()
	{
		// Add SVG Symbols
		foreach($this->fontAwesomeSymbols() as $symbol_id => $code)
		{
			$this->body_tag->attachView($this->fontAwesomeTagForSymbol($symbol_id));
		}

		if(sizeof($this->svgIconSymbols()) > 0)
		{
			$view = new TCv_View();
			$view->setTag('svg');
			$view->setAttribute('style','display:none;');
			foreach($this->svgIconSymbols() as $symbol_id => $code)
			{
				//svg id ="asdfs"
				// Clean up code
				$code = preg_replace('/<!--(.*)-->/Uim', '', $code);
				$code = preg_replace('/id\s*=\s*"(.*)"/i', '', $code);
				$code = preg_replace('/style\s*=\s*"(.*)"/i', '', $code);
				$code = str_replace('<svg','<symbol id="'.$symbol_id.'"', $code);
				$code = str_replace('</svg','</symbol', $code);

				$view->addText($code);
			}
			$this->body_tag->attachView($view);

		}

		// add content to be shown, pretty much the last thing that happens
		$this->body_tag->addText($this->bodyContent(), true);

		
	}
	
	/**
	 * The title used in meta tags
	 * @return bool
	 */
	public function metaTagTitle()
	{
		return $this->title;
	}
	
	/**
	 * This function adds all the necessary elements to the head tag. This function can be overridden to add additional
	 * items as well as through other standard techniques as necessary.
	 */
	public function completeHeadTag()
	{
		$title = strip_tags($this->metaTagTitle());
		if($this->title_prefix)
		{
			$title = $title . ' - '. $this->title_prefix;
		}
		
		$this->head_tag->addText("\n\t".'<title>'.$title.'</title>');
		$this->head_tag->addText("\n\t".'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
		$this->head_tag->addText("\n\t".'<meta name="description" content="'.$this->description.'" />');
		foreach($this->meta_tags as $id => $tag_string)
		{
			$this->head_tag->addText("\n\t".$tag_string);
		}
		// Favicon
		$this->head_tag->addText("\n\t".'<link rel="icon" href="'.htmlspecialchars($this->favicon).'" type="image/x-icon" />');
		$this->head_tag->addText("\n\t".'<link rel="shortcut icon" href="'.htmlspecialchars($this->favicon).'" type="image/x-icon" />');
		
		$this->head_tag->addText($this->headCSS());
		
		$this->head_tag->addText($this->headJS());
		
	}
	
	/*
	 * Enables performance tracking functionality which we'll expand and grow over time.
	 */
	public function enablePerformanceTracking() : void
	{
		$this->performance_tracking = true;
	}
	
	public function attachPerformanceTrackingToConsole() : void
	{
		if($this->performance_tracking)
		{
			$this->addConsoleDebug('Memory: '.TC_currentMemoryUsage());
			
			if(isset($GLOBALS['TC_classes']['classes']))
			{
				$this->addConsoleDebug('<h3>GLOBAL CLASSES</h3>');
				$this->addConsoleDebug($GLOBALS['TC_classes']['classes']);
			}
			if(isset($_SESSION['TCm_FormTracker']))
			{
				$this->addConsoleDebug('<h3>FORM TRACKERS</h3>');
				$this->addConsoleDebug($_SESSION['TCm_FormTracker']);
			}
			
		}
	}
	
	/**
	 * Returns all the HTML for this item
	 * @return string
	 */
	public function html()
	{
		$this->render(); // call render in case it's needed
		
		$this->loadPlugins(true); // don't wait for rendering
		
		$this->attachPerformanceTrackingToConsole();
		
		// CLEAR CLASS MEMORY (Creates a slowdown otherwise)
		TC_clearMemoryForClass(-1);
		
		
		// HEAD
		$this->completeHeadTag();
		$this->html_tag->addText("\n");
		$this->html_tag->attachView($this->head_tag);
		
		// BODY
		$this->completeBodyTag();
		$this->html_tag->addText("\n");
		$this->html_tag->attachView($this->body_tag);
		
		
		
		// Return the doctype plus all the html
		$html = $this->doctype() . $this->html_tag->html();

		// Track Rendering
		$console_item = new TSm_ConsoleItem('Render', TSm_ConsolePageRendering);
		$console_item->setClassName(get_class($this));
		$this->addConsoleItem($console_item);
		
		// A variable used to track process errors that occur during the handling of scripts and non-rendered actions
		// This is reset each time a page is rendered, so that any future calls can safely fill it accordingly to
		// pass error messages
		TC_clearProcessErrors();
		
		TMm_BenchmarkEntry::instance()?->trackRenderTimes();
		
		return $html;

	}
	
	/**
	 * Returns the HTML that should be output for AJAX. This version of the output should be used when html is being
	 * generated and being displayed on a page that already has been loaded by tungsten.
	 * @param bool $load_css (Optional) Default true. Indicates if CSS should be included
	 * @param bool $load_js (Optional) Default true. Indicates if CSS should be included
	 * @return string
	 */
	public function htmlForAJAX($load_css = true, $load_js = true)
	{
		//$this->removeJSJQueryLibrary(); // avoid excess loading of JS
		
		$html = '';
		if($load_css)
		{
			$html .= $this->headCSS();
		}
		$html .= $this->bodyContent();
		if($load_js)
		{
			$html .= $this->headJS(false);
		}

		// Track Rendering
		$console_item = new TSm_ConsoleItem('Render', TSm_ConsolePageRendering);
		$console_item->setClassName(get_class($this));
		$this->addConsoleItem($console_item);


		return $html;
	}
	
	/**
	 * Returns a link to show the console if it is indeed meant to be shown. Otherwise returns false
	 * @return bool|TCv_Link
	 */
	protected function showConsoleLink()
	{
		// Hide if we aren't saving to the console
		if(!TC_getConfig('console_saving'))
		{
			return false;
		}
		$console_model = $this->pluginModelWithName('TSv_Console');
		
		if($console_model)
		{	
			if($console_model->determineIfShown($this->isBetaSite(), $this->is_admin_site))
			{
				$console_button = new TCv_Link();
				$console_button->setTitle('Console');
				$console_button->addText('Console'); 
				$console_button->addClass('TSv_ConsoleButton_show'); 
				$console_button->addClass('console_button'); 
				
				$console_button->setURL('#');
				return $console_button;
				
			}
		}
		
		return false;
	}	
	

	
	/**
	 * Returns the list of classes that can have a specific interface.
	 *
	 * This method will check all the installed modules and return a list of classes that implement the interface
	 * provided. Classes will only be returned if they are sorted in the proper hierarchy within the Tungsten infrastructure.
	 *
	 * @param string $interface_name The name of the interface that is being searched for
	 * @param bool $restrict_to_user_access Restricts to only traits that exist in modules that the user can see
	 * @return array
	 */
	public static function classesWithInterface($interface_name, $restrict_to_user_access = false)
	{
		$classes = array();
		/** @var TSm_ModuleList $module_list */
		$module_list = TSm_ModuleList::init();
		if($restrict_to_user_access)
		{
			$modules = $module_list->installedModulesForUser(TC_currentUser());
		}
		else
		{
			$modules = $module_list->modules();
		}

		foreach($modules as $module)
		{
			$potential_classes = $module->classNames();
			foreach($potential_classes as $class_name)
			{
				$interfaces = class_implements($class_name);
				if(isset($interfaces[$interface_name]))
				{
					// avoid duplicates with extended classes
					$base_class_name = $class_name::baseClass();
					
					// Already exists, ensure we have an extended version
					if(isset($classes[$base_class_name]))
					{
						if($base_class_name != $class_name)
						{
							$classes[$base_class_name] = $class_name;
						}
					}
					else
					{
						$classes[$base_class_name] = $class_name;
					}
					
					
					
				}
			}
		}

		return $classes;
	}

	/**
	 * Returns the list of classes that can have a specific trait.
	 *
	 * This method will check all the installed modules and return a list of classes that uses the trait
	 * provided. Classes will only be returned if they are sorted in the proper hierarchy within the Tungsten infrastructure.
	 *
	 * @param string $trait_name The name of the trait that is being searched for
	 * @param bool $restrict_to_user_access Restricts to only traits that exist in modules that the user can see
	 * @return array
	 */
	public static function classesWithTrait($trait_name, $restrict_to_user_access = false)
	{
		$classes = array();
		/** @var TSm_ModuleList $module_list */
		$module_list = TSm_ModuleList::init();
		if($restrict_to_user_access)
		{
			$modules = $module_list->installedModulesForUser(TC_currentUser());
		}
		else
		{
			$modules = $module_list->modules();
		}

		foreach($modules as $module)
		{
			$potential_classes = $module->classNames();
			foreach($potential_classes as $class_name)
			{
				$traits = class_uses($class_name);
				if(isset($traits[$trait_name]))
				{
					$classes[$class_name] = $class_name;
				}
			}
		}

		return $classes;
	}


}
?>