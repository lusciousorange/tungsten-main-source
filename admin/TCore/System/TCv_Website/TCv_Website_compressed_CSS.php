<?php
	session_start();
	header('Content-type: text/css');
	ob_start("compress");
	
	/**********************************************/
	/*										   	  */
	/*           COMPRESSION FUNCTION             */
	/*										   	  */
	/**********************************************/
	function compress($buffer) 
	{
		
		/* remove comments */
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		/* remove tabs, spaces, newlines, etc. */
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '	', '		', '		'), '', $buffer);
		return $buffer;
	}
	
	
	/**********************************************/
	/*										   	  */
	/*            CSS FILE INCLUDES               */
	/*										   	  */
	/**********************************************/
	$css_calls = $_SESSION['dynamic_css']['compress_css_'.htmlspecialchars($_GET['id'])];
	$output = '';
	if(is_array($css_calls))
	{
		foreach($css_calls as $id => $url)
		{
			// NEEDS TO HANDLE RELATIVE URL properties. I know it's bizarre
			
			// trim the end off the url
			$path = explode('/', $url);
			$filename = array_pop($path);
			$path = implode('/', $path).'/';
			
			// Grab file and convert url( or url(' to have absolute path
			$contents = file_get_contents( $_SERVER['DOCUMENT_ROOT'].$path.$filename);
			$output .=  preg_replace('/url\((\'|\")?(\w)/', 'url($1'.$path.'$2', $contents);
		}
	}
	
	
	/**********************************************/
	/*										   	  */
	/*          CSS RUNTIME INCLUDES              */
	/*										   	  */
	/**********************************************/
	$css_calls = $_SESSION['dynamic_css']['runtime_css_'.htmlspecialchars($_GET['id'])];
	
	if(is_array($css_calls))
	{
		foreach($css_calls as $id => $call)
		{
			$output .= $call;
		}
	}
	
	
	print $output;
	
	ob_end_flush();
	unset($_SESSION['dynamic_css']['compress_css_'.htmlspecialchars($_GET['id'])]);
	unset($_SESSION['dynamic_css']['runtime_css_'.htmlspecialchars($_GET['id'])]);
?>