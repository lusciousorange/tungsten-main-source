<?php

	//////////////////////////////////////////////////////
	//
	// CORE OBJECT INSTANCES
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the current instance of the website view that is running.
	 * @return TCv_Website
	 */
	function TC_website()
	{
	    return TCv_Website::website();
	}
	
	/**
	 * Returns the current instance of the user that is logged in at the moment. A value of false indicates that no
	 * user could be found.
	 *
	 * @return TMm_User|false
	 */
	function TC_currentUser()
	{
		if(TC_getConfig('use_impersonate_user') && isset($_SESSION['impersonation_id']))
		{
			return TMm_User::init( $_SESSION["impersonation_id"]);
		}

		if(isset($_SESSION["tungsten_user_id"]) && $_SESSION["tungsten_user_id"] > 0)
		{
			$user = TMm_User::init( $_SESSION["tungsten_user_id"]);
			if(!$user) // Found a user ID but no user instantiated. Log out and start over
			{
				TC_website()->logout('User not found');
			}
			return $user;
		}
		return false;
	}

	/**
	 * Returns if the current user that is logged-in, has the developer flag enabled.
	 * @return bool
	 */
	function TC_currentUserIsDeveloper() : bool
	{
		if(TC_currentUser())
		{
			return TC_currentUser()->isDeveloper();
		}
		return false;
	}

	/**
	 * Returns if the view being loaded is in a tungsten view. This method returns true if the view being rendered is
	 * within Tungsten. For scripts it will also check if the referrer is coming from inside or outside of Tungsten
	 * to ensure consistency with permissions.
	 * @return bool
	 */
	function TC_isTungstenView()
	{
		// Only bother once per load
		if(!TC_configIsSet('in_tungsten_view'))
		{
			$parts = explode('/', $_SERVER['REQUEST_URI'] ?? '');

			// Test if we're not in the admin folder
			if(count($parts) <= 1 || $parts[1] != 'admin')
			{
				TC_setConfig('in_tungsten_view', false);
			}
			else // admin folder, potentially referred to process script
			{
				if($referrer = @$_SERVER['HTTP_REFERER'])
				{
					$referrer = parse_url($referrer,PHP_URL_PATH);
					$referrer_parts = explode('/', $referrer);

					TC_setConfig('in_tungsten_view', $referrer_parts[1] == 'admin');
				}
				else // no referrer, definitely in admin
				{
					TC_setConfig('in_tungsten_view', true);

				}

			}
		}

		return TC_getConfig('in_tungsten_view');
	}

	//////////////////////////////////////////////////////
	//
	// CLASS INSTANTIATION
	//
	// Various methods related to instantiating classes within
	// Tungsten. These classes handle a large portion of the
	// caching and performance features.
	//
	// These classes also handle any `class posing` that
	// allows a developer to override the model that is loaded
	// for any give class name via the `TC_config.php` setup.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the name of the class that is used for a given class name.
	 *
	 * This method returns the name of the actual class that will be loaded for a given class name.
	 * This accounts for any class posing (overrides) that may be setup in TC_config.php.
	 *
	 * @param string $class_name The name of the class that is being checked for usage.
	 * @return string The name of the class that will be loaded
	 * @deprecated use TCu_Item::classNameForInit() instead.
	 * @see TCu_Item::classNameForInit()
	 */
	function TC_actualClassNameUsed($class_name)
	{
		/** @var TCu_Item $class_name */
		return $class_name::classNameForInit();
	}
	
	/**
	 * Return if there is a cached instance for a given class name and ID that has already been
	 * initialized somewhere else in the system.
	 * @param string $class_name The name of the class
	 * @param int|string $id The id for the class. For most `TCm_Model` classes, this would correlate to the id of that model.
	 * @return bool
	 */
	function TC_classInstanceExists($class_name, $id)
	{
		if(@isset($GLOBALS['TC_classes']['classes'][$class_name][$id]))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Instantiates a class with a given class name and ID if it does not already exist.
	 *
	 * This method will attempt to instantiate a class with an ID provided as well as handle any
	 * class posing or caching. If a given instance has previously been loaded, then this method will
	 * return that same cached instance instead of returning a new instance.
	 *
	 * This method is widely used to avoid creating multiple instances of the same object item.
	 *
	 * This method is designed the various types of instantiations than can occur within Tungsten, including:
	 *
	 * * `TCm_Models` with an integer $id
	 * * 'TCm_Models` with an array matching the values from its database row
	 * * Any `TCu_Item` that has a unique $id
	 * * Singletons when the $id is set to -1
	 * * Classes with no input parameters when the $id is set to -1
	 *
	 * @param string $class_name THe name of the class that is being instantiated. If a class uses class posing,
	 * then
	 * it is recommended to pass in the "base class name" instead of the posed class name. This is especially true for
	 * Tungsten Core classes that begin with "TC".
	 *
	 * @param int|array|bool $id (Optional) The id for the class that is being instantiated.
	 *
	 * An integer signifies the unique for that particular object. A value of `-1` indicates that no parameters.
	 *
	 * An array can be passed in for `TCm_Model` classes but it will only succeed if it is an associative array with
	 * one of the indices corresponding to `TCm_Model::$table_id_column` static property of that class. Any caching of
	 * `TCm_Model` instances is always indexed by their IDs.
	 *
	 * @param bool $id_2 A second parameter which will create a unique match with the first if provided.
	 *
	 * @return TCu_Item|bool (Optional) The model that was requested or *false if it does not exist*. If an instance returns a value
	 * of `false` for the method `exists()`, then this method also returns false.
	 *
	 * @see TCm_Model
	 * @see TCv_View
	 * @see TC_initClassNoSaving() Option that doesn't cache values
	 * @deprecated use static factory methods in TCu_Item instead
	 * @see TCu_Item::init()
	 */
	function TC_initClass($class_name, $id = -1, $id_2 = false)
	{
		// This code is a bridge between the old TC_initClass style and the new init() style

		if(!is_object($id) && !is_array($id) && $id == -1)
		{
			return $class_name::init();
		}
		/** @var TCu_Item $class_name */
		if($id_2)
		{
			return $class_name::init($id, $id_2);
		}

		return $class_name::init($id);

	}

	//RegEx: For future searching TC_initClass\(.*,{1}.*,{1}
	/**
	 * Initializes a class without saving. @see TC_initClass() for documentation on the parameters
	 * @param $class_name
	 * @param int $id
	 * @param bool $id_2
	 * @return bool
	 * @see TC_initClass()
	 * @deprecated use static factory methods in TCu_Item instead
	 * @see TCu_Item::initWithoutCache()
	 */
	function TC_initClassNoSaving($class_name, $id = -1, $id_2 = false)
	{
		/** @var TCu_Item $class_name */
		return $class_name::initWithoutCache($id, $id_2);
	}



	/**
	 * Initializes a class with the provided content code
	 *
	 * @param string $content_code
	 * @return bool|TCu_Item|TCm_Model
	 */
	function TC_initClassWithContentCode($content_code)
	{
		list($class_name, $id) = explode('--', $content_code);

		/** @var TCu_Item $class_name */
		return $class_name::init($id);
	}

	/**
	 * Clears any saved instance for a given class name with or without an $id.
	 *
	 * This method works in conjunction with `TC_initClass()`. That method will cache many instances of models and this
	 * method clears any saved values for those instances.
	 *
	 * @param string $class The name of the class to clear
	 * @param int|string|bool $id (Optional) The specific ID for that class. If this value is set to false (default)
	 * then it will clear all objects for that class name. If an $id is provided, then it will only attempt to clear
	 * instances with that particular ID.
	 *
	 * @deprecated use the clearFromMemory() or static clearAllModelsFromMemory() methods that are located as part of
	 * TCt_FactoryInit which every Tungsten model has.
	 *
	 */
	function TC_clearMemoryForClass($class, $id = false)
	{
		if($class == -1)
		{
			unset($GLOBALS['TC_classes']['classes']);
		}
		elseif($id === false)
		{
			$class = TC_actualClassNameUsed($class);
			unset($GLOBALS['TC_classes']['classes'][$class]);
		}
		else
		{
			$class = TC_actualClassNameUsed($class);
			unset($GLOBALS['TC_classes']['classes'][$class][$id]);
		}


	}

	/**
	 * Returns the current memory usage with an option to return the formatting.
	 * @param bool $use_formatting Default true. Indicates if formatting should be used to show KB, MB, etc. If set
	 * to false then the actual value is returned.
	 * @return string
	 */
	function TC_currentMemoryUsage(bool $use_formatting = true) : string
	{
		$bytes = memory_get_usage(TC_getConfig('console_show_real_memory'));
		
		if($use_formatting)
		{
			$unit = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
			$i = floor(log($bytes, 1024));
			return round($bytes / pow(1024, $i), 2) . ' ' . $unit[$i];
		}
		
		return $bytes;
	}
	
	
	/**
	 * A method that sorts any number of objects using the values from the $method as the index.
	 *
	 * This method will fail if any of the objects provided aren't models or if they can't have the $method called.
	 *
	 * @param array $objects The array of objects to be sorted
	 * @param string $method The name of the method to be called. This method cannot accept any parameters.
	 * @param bool $use_ascending (Optional) Default true. Indicates if it should sort ascending.
	 * @return array The newly arrange objects
	 */
	function sortObjectsUsingMethod($objects, $method, $use_ascending = true)
	{
		$new_order = array();
		
		foreach($objects as $object)
		{
			$new_order[$object->$method()] = $object;	
		}
		
		if($use_ascending)
		{
			ksort($new_order);
		}
		else
		{
			krsort($new_order);
		}
		
		return $new_order;
	}

	//////////////////////////////////////////////////////
	//
	// ACTIVE MODELS
	//
	// Active models are those instances of items that are
	// currently being viewed or used as an active item on a page.
	//
	// These are used by the `TCm_ModuleControllers` to track
	// which items are currently being worked on.
	//
	// Active models apply to specific instances of a model
	// in the system.
	//
	// For example, when viewing a user's profile, that user
	// would be saved as the active model since the view directly
	// relates to that model and would fail without it.
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the primary model for this page load.
	 *
	 * There can only be one primary model at any given time.
	 *
	 * @param TCm_Model|Object $model The model to be set as the primary. This is almost always a `TCm_Model`.
	 */
	function TC_setPrimaryModel($model)
	{
		$GLOBALS['TC_classes']['primary_model'] = $model;

	}
	
	/**
	 * Returns the primary model for this page load.
	 * @return TCm_Model|Object|null
	 */
	function TC_primaryModel()
	{
		if(isset($GLOBALS['TC_classes']['primary_model']) && $GLOBALS['TC_classes']['primary_model'] instanceof TCm_Model)
		{
			return $GLOBALS['TC_classes']['primary_model'];
		}
		return null;
		
	}
	
	/**
	 * Saves a model to the list of active models for this page load. There can be any number of active models saved
	 * but there can only be one per class name.
	 *
	 * @param TCm_Model|TCu_Item $model The model to be saved, usually a TCm_Model.
	 *
	 * @uses TCu_Item::modelsToLoadWhenActive() Called on the model and this method can be extended by parent classes
	 * if there are other models that should also be saved.
	 *
	 * @see TC_activeModelWithClassName()
	 */
	function TC_saveActiveModel($model)
	{
		if(is_object($model))
		{
			$GLOBALS['TC_classes']['active_models'][get_class($model)] = $model;
			if(method_exists($model,'modelsToLoadWhenActive'))
			{
				foreach($model->modelsToLoadWhenActive() as $other_active_model)
				{
					TC_saveActiveModel($other_active_model);
				}
			}
		}
		
	
	}
	
	/**
	 * Returns the active model for a given class name
	 * @param string $class_name The class name that is wanted.
	 *
	 * The $class_name parameter has a special override option if `"current_user"` is passed in as the value. In that
	 * instance the current user that is logged in is always returned instead.
	 *
	 * @return TCm_Model|TCu_Item|bool The active model for that class if found, otherwise false.
	 *
	 * @see TC_currentUser()
	 * @see TC_saveActiveModel()
	 */
	function TC_activeModelWithClassName($class_name)
	{
		// special case to always be able to call the current user as a saved model
		if($class_name == 'current-user' || $class_name == 'current_user')
		{
			return TC_currentUser();
		}

		if(class_exists($class_name))
		{
			/** @var TCm_Model $class_name */
			return $class_name::activeModel();

		}
		return false;
	}

	/**
	 * Returns all the active models that have been set
	 *
	 * @return array The array of all the active models
	 *
	 * @see TC_saveActiveModel()
	 */
	function TC_activeModels()
	{
		if(isset($GLOBALS['TC_classes']['active_models']))
		{
			return $GLOBALS['TC_classes']['active_models'];
		}
		return array();
	}

	/**
	 * Returns all the active models that have been set
	 * @param string|TCu_Item The class or classname for the model that should be removed from the active list
	 * @return array The array of all the active models
	 *
	 * @see TC_saveActiveModel()
	 */
	function TC_removeActiveModel($class)
	{
		if($class instanceof TCu_Item)
		{
			$class = get_class($class);
		}
		
		unset($GLOBALS['TC_classes']['active_models'][$class]);
	}

	//////////////////////////////////////////////////////
	//
	// CONFIG VALUES
	//
	// These are methods related to accessing the config
	// settings for the entire site as well as for
	// the modules settings.
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns a configuration setting that is defined in `TC_config.php`.
	 *
	 * @param string $variable_name
	 * @param string|null $second_index_name
	 * @return mixed
	 *
	 * @see TC_setConfig()
	 */
	function TC_getConfig($variable_name, ?string $second_index_name = null)
	{
		if(TC_configIsSet($variable_name))
		{
			if(is_null($second_index_name))
			{
				return $GLOBALS['TC_config'][$variable_name];
			}
			else
			{
				if(isset($GLOBALS['TC_config'][$variable_name][$second_index_name]))
				{
					return $GLOBALS['TC_config'][$variable_name][$second_index_name];
				}
				else
				{
					return null;
				}
			}
			
		}
		
		// Values that we ALWAYS want to return a value, so these are system-wide defaults that always hit
		$fallback_values = [
			'Tungsten_admin_folder_URL'     => 'admin',
			'content_width'                 => 1000,
			'content_single_column_width'   => 768,
		];
		
		if(isset($fallback_values[$variable_name]))
		{
			return $fallback_values[$variable_name];
		}
		
		
		return null;
	}

	/**
	 * Returns a config setting for one of the modules. These are defined separate for each module.
	 *
	 * @param string $module_name The name of the module
	 * @param string $variable_name The name of the variable/setting that is wanted
	 * @return mixed false if not found
	 *
	 * @uses TSm_Module
	 * @see TSv_ModuleSettingsForm
	 */
	function TC_getModuleConfig($module_name, $variable_name)
	{
		// Check if we've grabbed this already
		if( isset($GLOBALS['TC_module_configs'][$module_name][$variable_name] ) )
		{
			return $GLOBALS['TC_module_configs'][$module_name][$variable_name];
		}

		$module = TSm_Module::init($module_name);

		$return = false;
		if($module instanceof TSm_Module)
		{
			if($module->installed())
			{
				$return = $module->variable($variable_name);
			}
		}

		$GLOBALS['TC_module_configs'][$module_name][$variable_name] = $return;
		return $return;
	}
	
	/**
	 * Sets the value for a module's configuration variable.
	 *
	 * @param string $module_name The name of the module
	 * @param string $variable_name The name of the variable/setting.
	 * @param string $value The new value for the variable.
	 * @return string
	 *
	 * @uses TSm_Module::updateVariableWithName()
	 */
	function TC_setModuleConfig($module_name, $variable_name, $value)
	{
		$module = TSm_Module::init($module_name);
		if($module instanceof TSm_Module)
		{
			if($module->installed())
			{
				return $module->updateVariableWithName($variable_name, $value);
			}
			return false;
		}
		return false;
	}
	
	/**
	 * Sets a new value for a system configuration value for the rest of the page load.
	 *
	 * Note: This does not permanently set any values in the config. It is an override for the existing process
	 * that is being run.
	 * @param string $variable_name
	 * @param mixed $value
	 */
	function TC_setConfig($variable_name, $value)
	{
		$GLOBALS['TC_config'][$variable_name] = $value;
	}

	/**
	 * Returns if a given config value is set
	 * @param string $variable_name
	 * @return bool
	 */
	function TC_configIsSet($variable_name)
	{
		return isset($GLOBALS['TC_config'][$variable_name]);
	}



	/**
	 * Returns if a given module is installed
	 *
	 * @param string $module_name The name of the module
	 * @return bool
	 *
	 * @uses TSm_Module
	 * @see TSv_ModuleSettingsForm
	 */
	function TC_moduleWithNameInstalled($module_name)
	{
		$module = TSm_Module::init($module_name);
		if($module instanceof TSm_Module)
		{
			if($module->installed())
			{
				return true;
			}
		}

		// Must meet them all, otherwise false
		return false;
	}

	/**
	 * Returns the module for a provided class or class name.
	 *
	 * This method caches the values to avoid excessive ReflectionClass class.
	 *
	 * @param string|TCu_Item $class The name of a class
	 * @return TSm_Module
	 *
	 */
	function TC_moduleForClass($class)
	{
		// Deal with an object instead of a string
		if($class instanceof TCu_item)
		{
			$class = get_class($class);
		}

		// Check for caching
		if( isset($GLOBALS['TC_config']['class_modules'][$class]) )
		{
			return $GLOBALS['TC_config']['class_modules'][$class];
		}

		$reflection = new ReflectionClass($class);

		$path = $reflection->getFileName();
		$admin_position = strpos($path,DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR);

		// parse until we find the "admin" folder.
		$path = substr($path, $admin_position);

		$path_pieces = explode(DIRECTORY_SEPARATOR, $path);
		$module_name = $path_pieces[2];

		$module = TSm_Module::init($module_name);
		if($module)
		{
			$GLOBALS['TC_config']['class_modules'][$class] = $module;
		}
		else
		{
			TC_triggerError('Module not found for class "'.$class.'"');
		}

		return $GLOBALS['TC_config']['class_modules'][$class];

	}


	//////////////////////////////////////////////////////
	//
	// CONTROLLERS AND URL TARGETS
	//
	// These methods relate to the loading of the controller
	// and url targets for the site.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the current module controller.
	 *
	 * Every page load that happens via `/admin/` exists within a module and that module has a controller. In most
	 * cases that controller is overloaded for each module. This method only works when being loaded on a website that is
	 * on Tungsten's admin and the class `TSv_Tungsten` is being loaded.
	 *
	 * @return TSc_ModuleController|bool
	 *
	 * @uses TSv_Tungsten::currentModule()
	 */
	function TC_currentModuleController() : ?TSc_ModuleController
	{
		$website = TC_website();
		if($website instanceof TSv_Tungsten)
		{

			if ($module = $website->currentModule())
			{
				return $website->currentModule()->controller();
			}
			return new TSc_ModuleController(null);
		}
		return null;
	}
	
	/**
	 * Returns the current URL target.
	 *
	 * @return ?TSm_ModuleURLTarget The URL target that is currently being loaded. False if not found.
	 */
	function TC_currentURLTarget() : ?TSm_ModuleURLTarget
	{
		$website = TC_website();
		if($website instanceof TSv_Tungsten)
		{
			return TC_currentModuleController()->currentURLTarget();
		}
		return null;
	}

	/**
	 * Returns the current Tungsten module.
	 *
	 * @return TSm_Module|false The current module being loaded
	 */
	function TC_currentModule()
	{
		$website = TC_website();
		if(method_exists($website, 'currentModule'))
		{
			return $website->currentModule();
		}
		return false;
		
	}

	/**
	 * Returns a URL target that is correlated to a specific module and target name.
	 *
	 * There cannot be two URL Targets in the same module, so a module name and target name is sufficient to uniquely
	 * identify a URL target. The combination will look similar to `/admin/$module_name/do/$url_target_name/.
	 *
	 * @param string $module_name The folder name of the module
	 * @param string $url_target_name The name of the URL target
	 * @param bool $ignore_errors (Optional) Default false. Indicate if the errors of not finding a url target should be
	 * ignored. If set to true, then a missing url_target will return false.
	 * @return TSm_ModuleURLTarget|false
	 *
	 * @uses TSm_Module
	 * @uses TSc_ModuleController::URLTargetWithName()
	 */
	function TC_URLTargetFromModule($module_name , $url_target_name, $ignore_errors = false)
	{
		$controller = false;
		
		if($module_name)
		{
			$module = TSm_Module::init($module_name);
			if($module)
			{
				$controller = $module->controller();
			}

		}
		
		if(!$controller)
		{
			$controller = TC_currentModuleController();
		}
		
		if($controller)
		{
			$url_target = $controller->URLTargetWithName($url_target_name);

			if (!$url_target && !$ignore_errors)
			{
				TC_triggerError('Not Found: URL Target with name "<em>' . $url_target_name . '</em>" in module "<em>' . $module_name . '</em>", with controller "<em>' . get_class($controller) . '</em>".');
			}
			return $url_target;

		}

		return false;

	}

	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	// Methods related to the process messenger which is
	// used throughout the system to give user feedback
	// when something occurs on the website.
	//
	//////////////////////////////////////////////////////

	/**
	 * Generates a message that will be shown the next time a page loads.
	 *
	 * This function adds a message to the system's messenger, which provides feedback on actions within the system. The
	 * first message added will be the title for the messenger. Any additional messages are listed below in the list
	 * of message points.
	 *
	 * @param string $message The message to be shown to the user
	 * @param bool $successful (Optional) Default true. Indicates if the message was successful. A value of false will
	 * set the messenger to be in error and turn the entire messenger to "failed".
	 * @return void
	 *
	 * @uses TCv_Messenger::addMessage()
	 * @uses TCv_Messenger::fail()
	 */
	function TC_message($message, $successful = true)
	{
		$messenger = TCv_Messenger::instance();
		if(trim($message) != '')
		{
			$messenger->addMessage($message, $successful);
		}
		else // handle blank fails
		{
			if(!$successful)
			{
				$messenger->fail();
			}
		}
	}
	
	/**
	 * Sets the "conditional title" for the messenger.
	 *
	 * This function sets the conditional titles for the messenger. This function should be used to enforce a specific
	 * title for the messenger that will only appear if the messenger is either successful or failed. Any other
	 * TC_message() calls will be pushed down if this title is used.
	 *
	 * @param string $title The title to be conditionally set.
	 * @param bool $is_success Indicates if the conditional title should be shown when the messenger is succesful or not.
	 *
	 * @uses TCv_Messenger::setSuccessTitle()
	 * @uses TCv_Messenger::setFailureTitle()
	 */
	function TC_messageConditionalTitle($title, $is_success)
	{
		$messenger = TCv_Messenger::instance();
		if($is_success)
		{
			$messenger->setSuccessTitle($title);
		}
		else
		{
			$messenger->setFailureTitle($title);	
		}
		
		
	}
	
	/**
	 * Resets the messenger and removes any previous settings or messages that might been added.
	 *
	 * @return void
	 *
	 * @uses TCv_Messenger::resetMessenger()
	 */
	function TC_clearMessenger()
	{
		$messenger = TCv_Messenger::instance();
		$messenger->resetMessenger();
	}

	/**
	 * Tracks a process error which is a site-wide system for tracking if an error occurred. This is a lightweight
	 * structure meant to pass error information across the complex calls that can happen. It currently works in
	 * conjunction with the Form Processors and Form trackers which should be adapted to use this in the future.
	 * @param string $message The message associated with the error
	 * @param string $type (Default "error") Indicates the type of error, which is user-specified to provide context
	 * if needed. Some systems in Tungsten provide specific types
	 * @param string|null $field_id (Default null) the field id associated with this processing error.
	 */
	function TC_trackProcessError(string $message, string $type = 'error', string $field_id = null)
	{
		$_SESSION['process_errors'][] = [
			'message' => $message,
			'type'    => $type,
			'field_id'=> $field_id
		];
		
		// ERRORS for dbs are already logged in the console, don't need to double-dip
		if($type == 'db_error')
		{
			return;
		}
		
		$full_message = "[".$type;
		if($field_id)
		{
			$full_message .= " : ".$field_id;
		}
		$full_message .= "] ".$message."";
		
		// Add console error
		
		$message = new TSm_ConsoleItem($full_message,TSm_ConsoleMessage,true);
		$message->commit();
		
		
	}

	/**
	 * Returns the process errors that exist
	 * @return array[]
	 */
	function TC_processErrors()
	{
		if(!isset($_SESSION['process_errors']))
		{
			$_SESSION['process_errors'] = [];
		}
		return $_SESSION['process_errors'];
	}

	/**
	 * Returns how many process errors exist
	 * @return int
	 */
	function TC_numProcessErrors()
	{
		return count(TC_processErrors());
	}

	/**
	 * Clears the process errors that exist
	 */
	function TC_clearProcessErrors()
	{
		$_SESSION['process_errors'] = [];
	}

	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////

	// Detect if the localization init file exists. If not we need to replicate a simple function
	if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/admin/localization/config/init.php'))
	{
		
		/**
		 * The dummy replacement method for TC_localize which is potentially called from other modules that would
		 * lookup a value. Without the actual module, we should always be returning what is provided. If a default
		 * value exists, it returns that, otherwise it returns the code.
		 * @param string $string_code The code that is wanted
		 * @param string $default_primary_value (Default null) The default value to be used in the system if it doesn't
		 * exist. This helps to "auto correct" the system and insert the values into the localization to be translated
		 * @return string The localized string if found, otherwise it return $string_code.
		 */
		function TC_localize($string_code, $default_primary_value = null)
		{
			if($default_primary_value)
			{
				return $default_primary_value;
			}
			
			return $string_code;
		}
	}
	
	

	//////////////////////////////////////////////////////
	//
	// ERROR HANDLING
	//
	// Some basic error handling methods that provide
	// a little extra functionality for tests.
	//
	//////////////////////////////////////////////////////

	/**
	 * A general console call that doesn't
	 * @param mixed $message The message to add to the console
	 * @param int $type The type of console item, which is done through a series of constants
	 * @param bool $is_error Indicates if this is an error
	 * @return void
	 */
	function TC_addToConsole($message, int $type = TSm_ConsoleDebug, bool $is_error = false) : void
	{
		if(is_object($message))
		{
			$console_item = new TSm_ConsoleItem('', $type, $is_error);
			$console_item->setThreeColumnMessage($message->title(), get_class($message), $message->id() );
		}
		else
		{
			$console_item = new TSm_ConsoleItem($message, $type, $is_error);
			
		}
		
		$console_item->commit();
	}
	
	/**
	 * Triggers an error in PHP.
	 * @param string $message The message for the error
	 * @param int $level (Optional) Default `E_USER_ERROR`. The error level to be set.
	 *
	 * @uses trigger_error()
	 * @uses TC_backtrace()
	 */
	function TC_triggerError($message, $level = E_USER_ERROR)
	{
		TC_website()->addConsoleError('Fatal Error : '.$message);
		print '<h2>'.$message.'</h2>';
		print TC_backtrace();
		
		$trace = TC_backtrace(true);
		$last_line = array_pop($trace);
		
		trigger_error(strip_tags($message).' at '.$last_line['f'].':'.$last_line['l'], $level);
	}
	
	/**
	 * An assertion test that ensures that an object matches the type and class provided.
	 *
	 * @param TCu_Item $object The object to be tested
	 * @param string $expected_type The name of the class that the $object must match against in order to succeed.
	 */
	function TC_assertObjectType($object, $expected_type)
	{
		if(!@is_object($object))
		{
			print '<h2><strong>Invalid Object - </strong> Provided: <em>'.gettype($object).'</em>'.', Expected: <em>'.$expected_type.'</em></h2>';
			print TC_backtrace();
			
			trigger_error('', E_USER_ERROR);
	
		}
		
		if(!@class_exists($expected_type))
		{
			print '<h2><strong>Invalid Object - </strong> Class Name Does Not Exist: <em>"'.$expected_type.'"</em></h2>';
			print TC_backtrace();
			
			trigger_error('', E_USER_ERROR);
	
		}
		
		if(!($object instanceof $expected_type))
		{
			$type = ucwords(gettype($object));
			if($type == 'Object')
			{
				$type .= ' ['.get_class($object).']';
			}
			
			// must print, trigger_error truncates errors...
			print '<h2><strong>Invalid Object Type - </strong> Provided: <em>'.$type.'</em>'.', Expected: <em>'.$expected_type.'</em></h2>';
			print TC_backtrace();
			
		  trigger_error('', E_USER_ERROR);
	
		}
		
	   
	}
	
	/**
	 * Assertion test to check if the provided $object has the $trait.
	 * @param Object $object The object to be tested
	 * @param string $trait The name of the trait to be tested against.
	 *
	 * @uses class_uses()
	 */
	function TC_assertObjectTrait($object, $trait)
	{
		if(!TC_classUsesTrait($object, $trait))
		{
			// must print, trigger_error truncates errors...
			print '<h2><strong>Invalid Object Trait - </strong> Class <em>'.get_class($object).'</em> expected to have trait <em>'.$trait.'</em></h2>';
			print TC_backtrace();
			
		  trigger_error('', E_USER_ERROR);
	
		}
		
	   
	}

	/**
	 * A recursive test to see if this class or it's parents use a particular trait
	 * @param string|TCu_Item $class
	 * @param string $trait
	 */
	function TC_classUsesTrait($class, $trait)
	{
		// Ensure we have a class name
		if(is_object($class))
		{
			$class = get_class($class);
		}
		
		if(!isset($traits[$trait]))
		do
		{
			$traits = class_uses($class);
			if(isset($traits[$trait]))
			{
				return true;
			}
			$class = get_parent_class($class);
			
		} while($class);
	
		
		
	}

	/**
	 * Determines the array of model class names that use a particular trait.
	 * @param string $trait
	 * @return string[]
	 */
	function TC_modelsWithTrait(string $trait) : array
	{
		// Only do it once per trait
		if(isset($GLOBALS['trait_classes'][$trait]))
		{
			return $GLOBALS['trait_classes'][$trait];
		}
		
		// Find them
		$GLOBALS['trait_classes'][$trait] = [];
		
		// Loop through each folder
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			// Process the pages content folder for any content views that should be loaded
			$models_folder_path = $admin_root.$module_folder.'/classes/models/';
			// check if it is a directory and skip . and ..
			if(is_dir($models_folder_path))
			{
				// if here then we are looking at /admin/<module>/classes/
				$model_class_folder = dir($models_folder_path);
				while($model_name = $model_class_folder->read())
				{
					// ignore the two return links and anything that isn't a folder, OR the class doesn't exist
					if($model_name == '.' || $model_name == '..' || !class_exists($model_name))
					{
						continue;
					}
					
					if(TC_classUsesTrait($model_name, $trait))
					{
						// Avoid conflicting classes to load the correct one only
						$base_class = $model_name::baseClass();
						$active_class_name = $base_class::classNameForInit();
						$GLOBALS['trait_classes'][$trait][$active_class_name] = $active_class_name;
						
					}
				}
			}
			
			
		}
		return $GLOBALS['trait_classes'][$trait];
	}
	
	/**
	 * A debug function which provides a complete backtrace from where the call is made through to this method call.
	 * @param bool $return_as_array Indicates if the response should be returned as an array
	 * @return string|array
	 */
	function TC_backtrace(bool $return_as_array = false)
	{
		
		// Add backtrace details
		$calls = debug_backtrace();
		$calls = array_reverse($calls);
		$i = 1;
		$call_line = false; // it appears to be off by one
		$array_values = array();
		
		if(!$return_as_array)
		{
			$css = '<style>table { width:100%; margin-bottom:10px;} th {text-align:left; border-bottom:1px solid #000;} span.in_text {display:none;}</style>';
			
			$backtrace = $css . PHP_EOL . '<table>';
			$backtrace .= PHP_EOL . '<tr><th width="40">#</th><th width="30%">File</th><th width="80">Line</th><th width="*">Call</th></tr>';
		}
		
		foreach($calls as $call_info)
		{
			if(!isset($call_info['class']))
			{
				$call_info['class'] = '';
			}
			if(!isset($call_info['type']))
			{
				$call_info['type'] = '';
			}
			if(!isset($call_info['file']))
			{
				$call_info['file'] = '';
			}
			if(!isset($call_info['line']))
			{
				$call_info['line'] = '';
			}
			
			
			$file = $call_info['file'];
			$file = explode('/', $file);
			$filename = array_pop($file);
			$function_name = $call_info['function'];
			$track = true;
			if(
				($filename == 'index.php' && $function_name == 'require') // ignore require
				|| ($filename == 'basic_TC_functions.php' && $function_name == 'TC_backtrace') // ignore backtrace
				|| ($call_info['class'] == 'TSm_ConsoleItem')
				|| $function_name == 'TC_backtrace'
			)
			{
				$track = false;		
			}
			if($track)
			{
				
				$track_values = array(
					'f' => $filename,
					'l' => $call_info['line'],
					'd' => $call_info['class'].$call_info['type'].($function_name != '' ? $function_name.'()' : ''),
				
				);
				
				$array_values[] = $track_values;
				if(!$return_as_array)
				{
					$backtrace .= PHP_EOL . '<tr>';
					$backtrace .= '<td>' . $i++ . '</td>';
					
					$backtrace .= '<td>' . $track_values['f'] . '</td>';
					$backtrace .= '<td>' . $track_values['l'] . '</td>';
					$backtrace .= '<td>' . $track_values['d'] . '&nbsp;</td>';
					$backtrace .= '</tr>';
				}
				
			}
			
			
		}
		if(!$return_as_array)
		{
			$backtrace .= '</table>';
			return $backtrace;
		}
		else // array values
		{
			return $array_values;
		}
		
	}

	
	
?>