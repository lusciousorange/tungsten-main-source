<?php
/**
 * Class TCu_Processor
 * The class to handle the processing a generic script.
 */
class TCu_Processor extends TCm_Model
{
	// Validation and process calls
	protected $validations = array(); // [array] = An array of TCm_FormItemValidations that stores each validation that was performed on any TCv_FormItem
	protected $is_valid = true; // [bool] = Indicates if the form processor has come across any invalid fields
	
	// Process URLS
	protected $failure_url = ''; // [string] = The url that the processor will be directed to if the processing fails
	protected $success_url = ''; // [string] = The url that the processor will be directed to if the processing succeeds
	
	// Database settings
	protected $database_table = false; // [string] = The default table that this form processor will place it's values into
	protected $database_primary_key = false; // [string] = The primary key for the table
	protected $database_primary_value = false; // [int] = The ID of the item that is being edited. 
	protected $query_snippets = array();
	
	protected $database_values = array();
	
	protected bool $no_db_changes_detected = false;
	
	protected $user_id = false; // [int] The user ID for the processor
	
	/**
	 * TCu_Processor constructor.
	 *
	 * @param array|int $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		
		// Set default values for urls to the page that directed to this form processor
		$this->failure_url = @$_SERVER['HTTP_REFERER']; // Most cases this is what is wanted
		$this->success_url = @$_SERVER['HTTP_REFERER'];

	}
	
	
	/**
	 * Returns the array of validations that are associated with this processor
	 * @return array The array of validations
	 *
	 * @see TCm_FormItemValidation
	 */
	public function validations()
	{
		return $this->validations;
	}
	
	/**
	 * Returns a specific validation with a particular ID
	 *
	 * @param string $id The ID that is wanted
	 * @return TCm_FormItemValidation
	 */
	public function validationWithID($id)
	{
		return $this->validations[$id];
	}
	
	/**
	 * Adds a validation to the processor. This function takes a TCm_FormItemValidation and combines the results from
	 * that validation to all the other validations in the processor. If this method is being used outside of this class'
	 * validate() method, then it must be called "after" validate().
	 *
	 * @param TCm_FormItemValidation $validation The validation that is being added
	 */
	public function addValidation($validation)
	{
		// SAVE VALIDATION
		$this->validations[$validation->id()] = $validation;
		if(!$validation->isValid())
		{
			$this->fail();
		}
		
		if($validation->isConsoleLogging())
		{
			// ADD VALIDATION TO CONSOLE
			//$message = '<span class="form_value_validation_id">'.$validation->id().'</span><span class="form_value_variable">'.$validation->formItemID().'</span><span class="form_value_value">'.$validation->title().'</span>';
			$console_item = new TSm_ConsoleItem('', TSm_ConsoleFormValidation, !$validation->isValid());
			$console_item->setClassName(get_class($this));
			$console_item->setDetails($validation->details());
			$console_item->setIsSkipped($validation->isSkipped());
			$console_item->setThreeColumnMessage($validation->title(), $validation->formItemID(), '');
			
			$this->addConsoleItem($console_item);
		}
		// ADD VALIDATION TO MESSAGE
		if($validation->message() != '')
		{
			TC_message($validation->message(), $validation->isValid());
		}
	
		
		
	}

	
	/**
	 * Forces a failure of this processor. This is normally automatically triggered through the validations.
	 *
	 * @param string|bool $message The message to be shown when failing
	 *
	 * @see TC_message()
	 */
	public function fail($message = false)
	{
		$this->setAsInvalid();
		
		TC_message($message, false); // run either way to ensure it tracks the failure


	}
	
	/**
	 * Adds a message to the responses and this is a passthrough for consistency of TC_message()
	 *
	 * @param string|bool $message The message to be shown when failing
	 *
	 * @see TC_message()
	 */
	public function addMessage($message = false)
	{
		TC_message($message);
		
		
	}
	
	/**
	 * Returns if this processor is valid
	 * @return bool
	 */
	public function isValid()
	{
		return $this->is_valid;
	
	
	}
	
	/**
	 * Sets the processor to be invalid
	 */
	public function setAsInvalid()
	{
		$this->is_valid = false;
	}
	
	/**
	 * Sets the url to be directed to if the processing fails.
	 *
	 * @param string $url
	 */
	public function setFailureURL($url)
	{
		$this->failure_url = $url;
	}

	/**
	 * Sets the url to be directed to if the processing succeeds.
	 *
	 * @param string $url
	 */
	public function setSuccessURL($url)
	{
		$this->success_url = $url;
	}
	
	/**
	 * Returns the url to be directed to if the processing fails.
	 *
	 * @return string
	 */
	public function failureURL()
	{
		return $this->failure_url;
	}
	
	/**
	 * Returns the url to be directed to if the processing succeeds.
	 *
	 * @return string
	 */
	public function successURL()
	{
		return $this->success_url;
	}
	
	/**
	 * Sets the ID for the database item. This is the ID that is associated with a primary key for the main
	 * db table that this form is editing.
	 * @param int $item_id The ID for the primary value
	 */
	public function setDatabasePrimaryValue($item_id)
	{
		$this->addTrackerValueToConsole('Primary Value', $item_id);
		$this->database_primary_value = $item_id;
	}
	
	/**
	 * Sets the ID for the database item. This is the ID that is associated with a primary key for the main db table that this form is editing.
	 * @return int
	 */
	public function databasePrimaryValue()
	{
		return $this->database_primary_value;
	}
	
	/**
	 * Sets the values for the database table and primary key. These values are used to create a valid MySQL query to update the items be altered.
	 *
	 * @param string $table The name of the table that is being modified
	 * @param string $primary_key The primary key for the table that is being updated. Note that this does not necessarily
	 * have to be the actual primary key but it needs to be a column with unique values.
	 *
	 */
	public function setDatabaseTableValues($table, $primary_key)
	{
		$this->addTrackerValueToConsole('Primary Table', $table);
		$this->addTrackerValueToConsole('Primary Key', $primary_key);
		
		$this->database_table = $table;
		$this->database_primary_key = $primary_key;
	}


	/**
	 * Adds a value to be added to the database table.
	 *
	 * @param string $column The database column
	 * @param string $value The value for that column
	 * @param int|bool $param_type (Optional) Default false. The parameter type correlating to PDO bindParam
	 */
	public function addDatabaseValue($column, $value, $param_type = false)
	{
		if(!is_null($value))
		{
			$value = trim($value);
			
			// Replace any script tags, avoids injection of JavaScript
			$value = preg_replace('/<script/i', '<span class="script_replace"', $value);
			$value = preg_replace('/<\/script/i', '</span', $value);
			
		}
		
		
		$this->database_values[$column] = array('value' => $value, 'param_type' => $param_type);
	}

	/**
	 * Removes a database value from the processor
	 *
	 * @param string $column The database column
	 */
	public function removeDatabaseValue($column)
	{
		unset($this->database_values[$column]);
	}

	/**
	 * Same function as addDatabaseValue()
	 * @param $column
	 * @param $value
	 * @deprecated 8.0
	 * @uses TCu_Processor::addDatabaseValue()
	 */
	public function addQueryValue($column, $value)
	{
		$this->addDatabaseValue($column, $value);
	}

	
	/**
	 * A specialized function that allows a user_id to be added to the system. This is due to the fact that most
	 * queries will want to store the ID of the person that made the change. If this function is called without any
	 * parameters it will attempt to include the user ID of the logged in user in Tungsten. It can be overridden to include other IDs.
	 *
	 * @param int|bool $user_id (Optional) Default false will use the current logged in user iD.
	 */
	public function addUserID($user_id = false)
	{
		// try for Tungsten ID 
		if($user_id === false)
		{
			if(TC_currentUser())
			{
				$user_id = TC_currentUser()->id();	
			}
			
		}
		
		$this->addDatabaseValue('user_id', $user_id);
	}
	
	
	
	/**
	 * Performs a query on the database that will update or insert the new values into the table based on the settings.
	 *
	 * @param bool $force_insert (Optional) Default false. Indicates if the udpate should always insert new items.
	 *
	 * @see TCu_Processor::addDatabaseValue()
	 * @see TCu_Processor::setDatabasePrimaryValue()
	 */
	public function updateDatabase(bool $force_insert = false)
	{
		// Before the update, double-check if any external method calls triggered errors
		if(TC_numProcessErrors() > 0)
		{
			$this->fail(); // something went wrong, ensure the processor failed
			return; // stop anything else from happening
		}
		$is_update = $this->database_primary_value !== false;
		if($force_insert)
		{
			$is_update = false;
		}
		
		// BUILD THE QUERY
		$query = ($is_update ? 'UPDATE' : 'INSERT INTO').' `'.addslashes($this->database_table).'` SET ';
		$snippets = array();
		foreach($this->database_values as $column => $value_settings)
		{
			$value = $value_settings['value'];
			if(is_null($value))
			{
				$value = '<em>null</em>';
			}
			elseif($value == '')
			{
				$value = "<em>''</em>";
			}
			$console_item = new TSm_ConsoleItem('',TSm_ConsoleFormDatabaseValue);
			$console_item->setThreeColumnMessage($column, $value, '' );
			$console_item->setClassName(get_class($this));
			$this->addConsoleItem($console_item);
			$snippets[] = "`$column` = :$column ";
				
		}
		// combine the snippets
		$query .= implode(', ', $snippets);
		
		if(sizeof($snippets) == 0)
		{
			$this->addConsoleMessage('Query Skipped: No values to update in '.$this->database_table);
			return;
		}
		
		if($is_update)
		{
			$query .= ' WHERE '.$this->database_primary_key.' = :database_primary_value';
			$this->addDatabaseValue('database_primary_value', $this->database_primary_value);
			
		}
		else
		{
			$query .= ', date_added = now() ' ;
		}
		
		// BUILD THE QUERY EITHER WAY, NEED TO SHOW IT IF SKIPPED
		$data_values = array();
		$param_values = array();
		foreach($this->database_values as $column => $value_settings)
		{
			$data_values[$column] = $value_settings['value'];
			if($value_settings['param_type'] != false)
			{
				$param_values[$column] = $value_settings['param_type'];
			}
		}
		
		if($this->isValid() && !$this->no_db_changes_detected)
		{
			// Perform Query
			$result = $this->DB_Prep_Exec($query, $data_values, $param_values);
		
			if($result->errorCode() != '00000')
			{
				$this->fail('<p><strong>There was an unexpected database error</strong> <br />It must be corrected by a programmer and most likely, isn\'t your fault. Please help us resolve the issue by sending this message or preferably a screenshot of this message to the website administrator. This will aid in helping to address the problem quickly. We apologize for the inconvenience. Table: "'.$this->database_table.'", ID: '.$this->database_primary_value.'</p>');
			}
			elseif(!$is_update) 
			{
				$this->saveCreateModelInsertedID();
			}
		}
		else
		{
			
			$console_item = new TSm_ConsoleItem(htmlspecialchars(static::compositeQueryWithValues($query, $data_values)), TSm_ConsoleQuerySkipped, false);
			$console_item->setClassName(get_class($this));
			$console_item->setIsSkipped();
			if($this->no_db_changes_detected)
			{
				$console_item->setDetails("No changes to the database detected");
			}
			$this->addConsoleItem($console_item);
		
		

		}
			
	}
	
	/**
	 * Function to save the inserted ID when creating a new model
	 * @return void
	 */
	protected function saveCreateModelInsertedID() : void
	{
		$this->setDatabasePrimaryValue($this->DB()->insertedID());
	}
	
	
	/**
	 * Returns the processor to the success/failure page based on the results of the queries.
	 *
	 * @param bool $redirect (Optional) Default true. Indicates if the processor redirects
	 * @see header()
	 * @see TCu_Processor::setFailureURL()s
	 * @see TCu_Processor::setSuccessURL()
	 *
	 */
	public function finish($redirect = true)
	{
		if(TC_numProcessErrors() > 0)
		{
			$this->fail(); // something went wrong, ensure the processor failed
		}
		
		if($this->isValid())
		{
			$this->addConsoleMessage('Processing Successful');
			$url = $this->success_url;
		}
		else
		{
			$this->addConsoleWarning('Processing Failed');
			$url = $this->failure_url;
		}
		
		if($redirect)
		{
			header("Location: " . $url);
			exit();
		}
	}
	
	/**
	 * Adds a 3-column tracker value to the console that shows values that have been added to be processed.
	 * @param string $name The name of the tracker value
	 * @param string $value The tracker value
	 * @param string $extra_info (Optional) Default ''. Additional info to be added in the console
	 *
	 * @see TSm_ConsoleItem
	 */
	public function addTrackerValueToConsole($name, $value, $extra_info = '')
	{
		$console_item = new TSm_ConsoleItem('', TSm_ConsoleFormSetting);
		$console_item->setClassName(get_class($this));
		$console_item->setThreeColumnMessage($name, $value, $extra_info );
		$this->addConsoleItem($console_item);
	}
	

	
	
}

	define('TCu_Processor_ForceInsert', true);
	
?>