<?php

/**
 * A core level class to interact with Memcached and acquire the details
 */
class TC_Memcached
{
	static ?Memcached $mem = null;
	static ?string $prefix = null;
	
	
	/**
	 * Returns if memcached is enabled on this site
	 * @return bool
	 */
	public static function enabled() : bool
	{
		$config = TC_getConfig('memcached');
		if(is_array($config))
		{
			return $config['enabled'];
		}
		return false;
	}
	
	
	/**
	 * Generates the memory variable if it exists. Returning null means that it didn't work for whatever reason.
	 * @return null|Memcached
	 */
	public static function mem() : ?Memcached
	{
		// If not set, configure it once statically
		if(is_null(static::$mem))
		{
			$config = TC_getConfig('memcached');
			if($config['enabled'])
			{
				// Configuration is enabled, set the server and save the prefix
				static::$mem = new Memcached();
				static::$mem->addServer($config['server'],  $config['port']);
				static::$prefix = $config['prefix'];
			}
		}
		return static::$mem;
	}
	
	/**
	 * The function to get a value. This accounts for detecting if it was successful and also handling prefixing to avoid
	 * conflicts with multiple sites.
	 * @param string $key
	 * @return void
	 */
	public static function get(string $key)
	{
		// Get this first, so that the prefix is set and we have a config
		$mem = static::mem();
		
		// Don't do anything if there's no prefix
		if(is_null(static::$prefix))
		{
			return null;
		}
		
		$value = $mem?->get(static::$prefix.$key);
		
		// CONSOLE TESTING
		//$console_item = new TSm_ConsoleItem('GET: ' . static::$prefix . $key . ' -------- ' . gettype($value), TSm_ConsoleTimer, false);
		//$console_item->commit();
		
		return $value;
		
	}
	
	/**
	 * The function to set a value. This accounts for detecting if it was successful and also handling prefixing to avoid
	 * conflicts with multiple sites.
	 * @param string $key
	 * @param mixed $value
	 * @param int $expiration
	 * @return void
	 */
	public static function set(string $key, $value, int $expiration = 0) : void
	{
		// Get this first, so that the prefix is set and we have a config
		$mem = static::mem();
		
		// Don't do anything if there's no prefix
		if(is_null(static::$prefix))
		{
			return;
		}
		
		// CONSOLE TESTING
		// $console_item = new TSm_ConsoleItem('SET: '.static::$prefix.$key.' '.gettype($value), TSm_ConsoleTimer, false);
		// $console_item->commit();
		
		$mem?->set(static::$prefix.$key, $value, $expiration);
	}
	
	/**
	 * Deletes a key with a specific key
	 * @param string $key
	 * @return void
	 */
	public static function delete(string $key) : void
	{
		// Get this first, so that the prefix is set and we have a config
		$mem = static::mem();
		
		// Don't do anything if there's no prefix
		if(is_null(static::$prefix))
		{
			return;
		}
		
		// CONSOLE TESTING
		// $console_item = new TSm_ConsoleItem('DELETE: ' . $key, TSm_ConsoleTimer, false);
		// $console_item->commit();
		
		$mem?->delete(static::$prefix.$key);
	}
	
	/**
	 * Gets a saved memcached value and if it can't find it, run the function provided
	 * @param string $key
	 * @param  $run_function
	 * @param int $expiration Default 0 the number of seconds before it expires, a value of zero is never.
	 * @return int|null
	 */
	public static function getIntValueOrRun(string $key, $run_function, int $expiration = 0) : ?int
	{
		
		$mem_value = TC_Memcached::get($key);
		if(is_int($mem_value))
		{
			//	TC_addToConsole('FOUND IN MEMORY: '.$mem_value);
			return $mem_value;
		}
		
		$new_value = $run_function();
		
		TC_Memcached::set($key, $new_value, $expiration);
		return $new_value;
		
	}
	
	/**
	 * Gets a saved memcached value and if it can't find it, run the function provided
	 * @param string $key
	 * @param $run_function
	 * @param int $expiration Default 0 the number of seconds before it expires, a value of zero is never.
	 * @return array|null
	 */
	public static function getArrayValueOrRun(string $key, $run_function, int $expiration = 0) : ?array
	{
		$mem_value = TC_Memcached::get($key);
		if(is_array($mem_value))
		{
			return $mem_value;
		}
		
		// Call the callback function that gets the value and saves it to the memcache
		$new_value = $run_function();
		
		TC_Memcached::set($key, $new_value, $expiration);
		return $new_value;
		
	}
	
	/**
	 * Gets a saved memcached value and if it can't find it, run the function provided
	 * @param string $key
	 * @param $run_function
	 * @param int $expiration Default 0 the number of seconds before it expires, a value of zero is never.
	 * @return bool|null
	 */
	public static function getBoolValueOrRun(string $key, $run_function, int $expiration = 0) : ?bool
	{
		$mem_value = TC_Memcached::get($key);
		
		// Booleans are stored as 1 or 0, since returning a value of false means something different with the `get` function
		// that exists in memcached
		if(is_int($mem_value))
		{
			return (bool)$mem_value;
		}
		
		// Call the callback function that gets the value and saves it to the memcache
		// Cannot store booleans, so we must track them as 1/0
		$new_value = $run_function() ? 1 : 0;
		
		TC_Memcached::set($key, $new_value, $expiration);
		return (bool)$new_value;
		
	}
	
	
	/**
	 * Gets a saved memcached value and if it can't find it, run the function provided
	 * @param string $key
	 * @param $run_function
	 * @param int $expiration Default 0 the number of seconds before it expires, a value of zero is never.
	 * @return string|null
	 */
	public static function getStringValueOrRun(string $key, $run_function, int $expiration = 0) : ?string
	{
		$mem_value = TC_Memcached::get($key);
		if(is_string($mem_value))
		{
			return $mem_value;
		}
		
		// Call the callback function that gets the value and saves it to the memcache
		$new_value = $run_function();
		
		TC_Memcached::set($key, $new_value, $expiration);
		return $new_value;
		
	}
	
	
	
}