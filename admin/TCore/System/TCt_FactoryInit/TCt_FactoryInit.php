<?php

/**
 * Trait TCt_FactoryInit
 *
 * Trait that handles factory initialization within Tungsten.
 */
trait TCt_FactoryInit
{
	protected static $show_errors_on_class_init_failures = true;

	/**
	 * Returns the name of the class that is used for a given class name.
	 *
	 * This method returns the name of the actual class that will be loaded for a given class name.
	 * This accounts for any class posing (overrides) that may be setup in TC_config.php.
	 *
	 * @param string $class_name The name of the class that is being checked for usage.
	 * @return string The name of the class that will be loaded
	 */
	public static function overrideClassName($class_name)
	{
		if(isset($GLOBALS['TC_config']['class_overrides'][$class_name]))
		{
			$class_name = $GLOBALS['TC_config']['class_overrides'][$class_name];
		}

		return $class_name;
	}

	/**
	 * A static method that can be overridden for classes that have complex model structures. By default this method
	 * returns the called class name which accounts for override class names.
	 * @param mixed $args,... The arguments provided for the init
	 * @return string
	 * @see get_called_class()
	 * @see TCu_Item::overrideClassName()
	 */
	public static function classNameForInit(...$args)
	{
		return static::overrideClassName(get_called_class());
	}
	
	/**
	 * The value that should be used as a parameter for singletons. By default all singletons use a string called
	 * singleton which ensures they fall in the same category. It's possible to override that value.
	 * @return string|mixed
	 */
	public static function initSingletonIdentifier()
	{
		return 'singleton';
	}
	
	
	/**
	 * Returns if this class is a singleton. Any attempt to instantiate this model will always pass the same parameter.
	 * @return bool
	 */
	public static function isSingleton() : bool
	{
		return false;
	}
	
	
	/**
	 * Factory method to create an instance without caching
	 * @param mixed $args,... Parameters for the class
	 * @return static|null
	 */
	public static function initWithoutCache(...$args)
	{
		$class_name = static::classNameForInit(...$args);
		// https://stackoverflow.com/questions/2409237/how-to-call-the-constructor-with-call-user-func-array-in-php
		$model = new $class_name(...$args);

		// check if the exists() method is valid for the given class object and then returns instance as needed
		if(method_exists($model,'exists'))
		{
			if($model->exists())
			{
				return $model;
			}
			else
			{
				$arg_0 = @$args[0];
				
				if(is_object($arg_0))
				{
					$arg_0 = get_class($arg_0).' '.$arg_0->id();
				}
				
				if(static::$show_errors_on_class_init_failures)
				{
					TC_website()->addConsoleError(
						'Model of class <em>' . $class_name .
						'</em> could not be instantiated with ID <em>"' . (is_array($arg_0) ? '<array>' : $arg_0) . '"</em>');
				}
					return null;
			}
		}
		else
		{
			return $model;
		}


	}
	
	/**
	 * A hook method that allows for a class to trigger actions immediately after the init. This allows for the class
	 * to be fully instantiated before calling methods that may refer back to it.
	 */
	public function runAfterInit()
	{
	
	}
	

	/**
	 * Factory method to create an instance which uses caching in globals to avoid excess DB calls.
	 *
	 * Arrays
	 * If an array is passed in, it’s taken as the values for that model. This is a commonly used short-circuit,
	 * particularly when iterating through values from a database call. Unless using this in abnormal circumstances,
	 * the values passed in should always be those that match a real model in the system.
	 *
	 * This approach skips a step in the initialization because it doesn’t have to go query the DB for values.
	 *
	 * Integer
	 * When an integer is passed in, it treats it as the primary key ID. In this case, it only has an ID so it will
	 * query the database and find the corresponding database row for that ID and initializes with those values.
	 *
	 * @param mixed $args,... Parameters for the class
	 * @return static|null
	 */
	public static function init(...$args)
	{
		/** @var TCu_Item $class_name */
		$class_name = static::classNameForInit(get_called_class());
		$id = @$args[0];
		// Class is set to not, save skip ahead
		if(property_exists($class_name, 'disable_tungsten_caching'))
		{
			if($class_name::$disable_tungsten_caching == true)
			{
				$item = static::initWithoutCache(...$args);
				// After saving to globals, we can now run the after-init
				// Future calls will find the globals and return them
				if(is_object($item) && method_exists($item, 'runAfterInit'))
				{
					$item->runAfterInit();
				}
				return $item;
				
			}
		}


		if(!is_string($class_name))
		{
			TC_triggerError('Class name must be a string, provided was "'.gettype($class_name).'".');
		}


		if(!class_exists($class_name))
		{
			TC_triggerError('Class <em>"'.$class_name.'"</em> does not exist. Attempt to instantiate failed.');
		}

		// check if id is an array
		$param = $id;


		if(is_array($id)) // init array provided instead of int
		{
			$table_id_column = $class_name::$table_id_column;
			if($table_id_column != '')
			{
				$id = @$id[$table_id_column]; // id column provided
			}
			else
			{
				$id = -1; // turn off to avoid loading bad values that were pre-saved. Impossible...but...
			}
		}
		elseif($id instanceof TCm_Model)
		{
			$id = $id->id();
		}
		elseif(is_null($id)) // nothing passed in, therefore a singleton
		{
			$id = static::initSingletonIdentifier();
		}

		// Deal with the second ID
		if(isset($args[1]))
		{
			if($args[1] instanceof TCm_Model)
			{
				$id .= '_'.$args[1]->id();
			}
			elseif(is_object($args[1]))
			{
				$id .= '_'.get_class($args[1]);
			}
			else
			{
				$id .= '_'.$args[1];
			}

		}

		// Deal with 2nd param being passed int
		if(isset($args[2]))
		{
			if($args[2] instanceof TCm_Model)
			{
				$id .= '_'.$args[2]->id();
			}
			else
			{
				$id .= '_'.$args[2];
			}

		}
		
		// Deal with SHARDING, needs the shard ID value as well
		if(is_subclass_of($class_name, 'TCm_ModelWithHorizontalSharding'))
		{
			$current_model = $class_name::currentPrimaryShardModel();
			$id .= '_sh'.$current_model->id();
			$param = null; // avoid any double saving
		}
		
		
		// Special case for partially extended modules
		// Set it back to to the main module class so that it finds the saved values
		if(get_parent_class($class_name) == 'TSm_Module')
		{
			$class_name = 'TSm_Module';
		}
		
		if(!isset($GLOBALS['TC_classes']['classes'][$class_name]))
		{
			$GLOBALS['TC_classes']['classes'][$class_name] = [];
		}
		
		// get the tag
		if(isset($GLOBALS['TC_classes']['classes'][$class_name][$id]))
		{
			$item = $GLOBALS['TC_classes']['classes'][$class_name][$id];
		}
		// handle string values being passed, rare but it happens
		elseif(is_string($param) && isset($GLOBALS['TC_classes']['classes'][$class_name][$param]))
		{
			$item = $GLOBALS['TC_classes']['classes'][$class_name][$param];
		}
		else
		{
			// Get the object
			/** @var TCu_Item|TSm_Module $item */
			$item = static::initWithoutCache(...$args);

			if($item)
			{
				// save to the TC_config array for future use
				if($id === false || $id == 'singleton') // false equates to singleton
				{
					$GLOBALS['TC_classes']['classes'][$class_name]['singleton'] = $item;
					
					// Store the item by ID as well, just in case it's referenced manually
					if(method_exists($item, 'id') && $item->id() > 0)
					{
						$GLOBALS['TC_classes']['classes'][$class_name][$item->id()] = $item;
					}
					
				}
				elseif($id == -1)
				{
					$GLOBALS['TC_classes']['classes'][$class_name][-1] = $item;
				}
				else
				{
					$GLOBALS['TC_classes']['classes'][$class_name][$id] = $item;
				}

				// save for string parameters as well
				if(is_string($param))
				{
					$GLOBALS['TC_classes']['classes'][$class_name][$param] = $item;

				}
				
				// Handle modules since they are instantiated two different ways
				// Handle extended Module classes, which may be partial
				// Ensure the base calls also work to pull the same model
				if(is_object($item) &&
					($class_name == 'TSm_Module' || is_subclass_of($class_name, 'TSm_Module') )
				)
				{
					$GLOBALS['TC_classes']['classes']['TSm_Module'][$item->id()] = $item;
					$GLOBALS['TC_classes']['classes']['TSm_Module'][$item->folder()] = $item;
				}
				
				
				// After saving to globals, we can now run the after-init
				// Future calls will find the globals and return them
				if(is_object($item) && method_exists($item, 'runAfterInit'))
				{
					$item->runAfterInit();
				}
				
			}


		}

		// check if the exists() method is valid for the given class object and then returns instance as needed
		if($item && method_exists($item,'exists'))
		{
			if($item->exists())
			{
				return $item;
			}
			else
			{
				if(static::$show_errors_on_class_init_failures)
				{
					TC_website()->addConsoleError('Model of class <em>'.$class_name.'</em> could not be instantiated with ID <em>"'.$id.'"</em>');
				}
				return null;
			}
		}
		else
		{
			return $item;
		}
	}
	
	/**
	 * Clears this model from the memory
	 * @return void
	 */
	public function clearFromMemory()
	{
		$class_name = static::classNameForInit(get_called_class());
		$GLOBALS['TC_classes']['classes'][$class_name][$this->id()] = null;
		unset($GLOBALS['TC_classes']['classes'][$class_name][$this->id()]);
		gc_collect_cycles();
		
	}
	
	/**
	 * Clears all the models from memory for this class
	 * @return void
	 */
	public static function clearAllModelsFromMemory()
	{
		// Using null works immediately more often, still trigger garbage collection
		// https://stackoverflow.com/questions/10544779/how-can-i-clear-the-memory-while-running-a-long-php-script-tried-unset
		$class_name = static::classNameForInit(get_called_class());
		if(isset($GLOBALS['TC_classes']['classes'][$class_name]))
		{
			foreach($GLOBALS['TC_classes']['classes'][$class_name] as $id => $model)
			{
				$model = null;
				$GLOBALS['TC_classes']['classes'][$class_name][$id] = null;
				unset($GLOBALS['TC_classes']['classes'][$class_name][$id]);
			}
		}
		gc_collect_cycles();
	}
	
	
	
	/**
	 * Factory method to grab the active model
	 * @return static|null
	 */
	public static function activeModel()
	{
		$class_name = get_called_class();
		$class_overrides = TC_getConfig('class_overrides');
		if(is_array($class_overrides) && isset($class_overrides[$class_name]))
		{
			$class_name = $class_overrides[$class_name];
		}

		if(isset($GLOBALS['TC_classes']['active_models'][$class_name]))
		{

			return $GLOBALS['TC_classes']['active_models'][$class_name];
		}
		elseif(isset($GLOBALS['TC_classes']['active_models']))
		{
			// Handle referencing an extended parent class
			foreach($GLOBALS['TC_classes']['active_models'] as $active_model)
			{
				if(is_subclass_of($active_model, $class_name))
				{
					return $active_model;
				}
			}
			
		}
		return null;
	}
}