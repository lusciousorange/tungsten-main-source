<?php
use PHPUnit\Framework\TestCase;

/**
 * Extends the test case to deal with anything related to models in Tungsten
 * Class TC_ModelTestCase
 */
class TC_ModelTestCase extends TestCase
{
	use TCt_DatabaseAccess, TSt_ConsoleAccess;
	
	protected static $local_user;
	protected $reset_model_class_names = true;
	protected $reset_names = array();
	
	protected function setUp () : void
	{
		// Run the class name reset
		$this->resetModuleClassNames();
		
		// Clear the process errors which otherwise build up over the course of the tests
		TC_clearProcessErrors();
		
		// Add unit test console start
		$_SERVER['REQUEST_URI'] = $this->getName();
		$console_page_start = new TSm_ConsoleItem('UNIT TEST', TSm_ConsolePageStart);
		$console_page_start->commit();
		
		
		// Call the parent
		parent::setUp();
	}
	
	/**
	 * Turns on the debugger where console errors get displayed to the screen
	 * @deprecated This function no longer does anything.
	 */
	protected function enableDebugger()
	{
	}
	
	/**
	 * Disables the class name resets for this test case. This is commonly used when testing classes that extend the
	 * functionality for an existing module (store, pages, etc). In those instances, the tests for that module have
	 * class names resets to avoid the tests being contaminated by extended class code.
	 *
	 * Running this method in our test classes BEFORE you call the parent's setup, will ensure the actual classes get
	 * used and not the base classes for a given module.
	 */
	protected function disableClassNameResets()
	{
		$this->reset_model_class_names = false;
	}
	
	/**
	 * Adds one more more model names that should be added to the reset list
	 * @param string|string[] $names
	 */
	protected function addResetModelClassNames($names)
	{
		if(is_string($names))
		{
			$this->reset_names[] = $names;
		}
		elseif(is_array($names))
		{
			$this->reset_names = $this->reset_names + $names;
		}
		
	}
	
	/**
	 * Resets all the class names to use this modules original values and not the extended ones that might appear on
	 * this site.
	 */
	protected function resetModuleClassNames()
	{
		if($this->reset_model_class_names)
		{
			foreach($this->reset_names as $name)
			{
				$this->setOverrideClass($name, $name);
			}
		}
		
	}
	
	/**
	 * This method is called before the first test of this test class is run.
	 */
	public static function setUpBeforeClass(): void
	{
		//	Cannot clear data. Tests run simultaneously and leads to race conditions
		//	clearDataInTables();
	}
	
	/**
	 * Sets an override class which can be used to adjust what models are loaded during testing. This has two main
	 * functions. One is to provide a method to ensure base classes are tested, and not extended classes that might
	 * occur for a given project. The second one is to provide a mechanism to override classes related to 3rd-parties
	 * so we can create mimic classes to avoid triggering those interactions.
	 * @param string $original_class
	 * @param string $my_class
	 */
	public function setOverrideClass($original_class, $my_class)
	{
		$GLOBALS['TC_config']['class_overrides'][$original_class] = $my_class;
	}
	
	/**
	 * Returns the local test user if it exists in the system. These values are set in the
	 * `TC_config_env ['test_user_values]` and provide a consistent approach to referencing the user who is running
	 * the tests. This helps ensure that functionality related to emailing, texting and otherwise communicating with
	 * a human is limited to the human running the test.
	 * @return TMm_User|bool
	 */
	public function localTestUser()
	{
		// Previously Set
		if(static::$local_user instanceof TMm_User)
		{
			return static::$local_user;
		}
		
		$user_values = TC_getConfig('test_user_values');
		if(is_array($user_values))
		{
			static::$local_user = TMm_User::createWithValues($user_values);
			return static::$local_user;
		}
		
		TC_triggerError('Local User not configured, you must set $TC_Config["test_user_values"] in TC_config_env.php');
	}
	
	/**
	 * A function to deletes an model from the database and ignores permissions. This is used when we need to clear
	 * away memory in a consistent fashion.
	 * @param TCm_Model $model
	 */
	protected function deleteModelIgnorePermissions($model)
	{
		if($model instanceof TCm_Model)
		{
			$query = "DELETE FROM `" . $model::tableName() . "` WHERE " . $model::$table_id_column . " = :id LIMIT 1";
			$this->DB_Prep_Exec($query, array('id' => $model->id()));
		}
		unset($model);
	}
	
	/**
	 * A method that detects that there was a process error for a given type and field. This tests Tungsten's
	 * internal process errors to confirm the error was generated.
	 * @param string type The type of error
	 * @param string field_id The ID of the field
	 * @param string|false message The message to be shown
	 */
	public function assertTungstenProcessError($type, $field_id, $message = false)
	{
		$found = false;
		foreach(TC_processErrors() as $error_values)
		{
			if($error_values['type'] == $type && $error_values['field_id'] == $field_id)
			{
				$found = true;
			}
		}
		$this->assertTrue($found, $message);
		
	}
	
	/**
	 * A method that detects if there was a schema field validation error
	 * @param string field_id The ID of the field
	 * @param string|false message The message to be shown
	 * @uses TC_ModelTestCase::assertTungstenProcessError
	 */
	public function assertTungstenSchemaFieldValidationError($field_id, $message = false)
	{
		$this->assertTungstenProcessError('field_validation', $field_id, $message);
	}
	
	/**
	 * A method that detects if there were any process errors that occurred
	 */
	public function assertNoTungstenProcessError()
	{
		$this->assertTrue(TC_numProcessErrors() == 0,'Process error detected');
	}
	
	
	
	/**
	 * A method to assert that there was a Tungsten DB error
	 * @param string|false message The message to be shown
	 */
	public function assertTungstenDBError($message = false)
	{
		$this->assertTungstenProcessError('db_error','', $message);
		
	}
	
	/**
	 * Extend the assertion to handle
	 * @param $value
	 * @param  $constraint
	 * @param string $message
	 * @return void
	 */
	public static function assertThat($value, $constraint, string $message = ''): void
	{
		$value_text = $value;
		if(is_bool($value))
		{
			$value_text = $value ? '<em>true</em>' : '<em>false</em>';
		}
		if(is_null($value))
		{
			$value_text = '<em>null</em>';
		}
		if($value instanceof TCm_Model)
		{
			$value_text = $value->contentCode();
		}
		
		
		
		$console_item = new TSm_ConsoleItem('Assert: '.$value_text .' '.$constraint->toString(),
		                                    TSm_ConsoleUnitTest);
		
//		self::$count += count($constraint);
//
//		$success = $constraint->evaluate($value, $message, true);
//
		try {
			parent::assertThat($value, $constraint, $message);
		}
		catch(Exception $e)
		{
			// Catch, track and re-throw
			$console_item->setIsError();
			$console_item->setDetails('<i class="fas fa-times"></i> : '.$message);
			$console_item->commit();
			throw $e;
		}
		
		$console_item->commit();
	
	}
	
	
	
}