<?php

/**
 * Class TCc_Controller
 *
 * The generic abstract
 */
abstract class TCc_Controller extends TCu_Item
{
	
	/**
	 * TCc_Controller constructor.
	 * @param int|string $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	/**
	 * Model lists are singletons
	 * @return bool
	 */
	public static function isSingleton() : bool
	{
		return true;
	}
	
	
	
}
?>