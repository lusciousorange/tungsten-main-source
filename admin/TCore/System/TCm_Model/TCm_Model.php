<?php
	/**
	 * The main class that all models should extend.
	 *
	 * This class handles all the core functionality that comes with managing data.
	 *
	 * The TCm_Model bases a lot of the database interaction on the TCt_DatabaseAccess trait.
	 *
	 * For most models, the classes that extend TCm_Model will set the static properties for $table_id_column, $table_name,
	 * $model_title and possible $primary_table_sort. These properties will differ for each class that extends TCm_Model.
	 *
	 * @uses TCt_DatabaseAccess
	 *
	 */
	class TCm_Model extends TCu_Item
	{
		use TCt_DatabaseAccess;
		use TMt_ContentFlaggable;
		use TCt_MemcachedModel;
		
		
		protected ?string $date_added = '';
		protected bool $properties_set_from_table = false;
		
		protected ?string $date_modified = null;
		
		protected static bool $fails_initialization_with_id_0 = true;

		private $temp_init_array = false; // [array/false] = The row that was provided to the contructor. It is useful in saving instantiation calls to the DB
		public static $table_id_column = ''; // [string] = The id column for this class. if left as an empty string, attempts to instantiate a class using a value array will fail
		public static $table_name = ''; // [string] = The table for this class.
		public static $model_title = 'Model'; // [string] = The title that describes all these models
		public static $model_title_plural = false; // The title that describes all these models in plural form
		public static $primary_table_sort = false; // [string] = The title that describes all these models
		public static $store_user_id_when_creating = false;
		public static $table_use_auto_increment = true;
		public static ?string $mirror_table_suffix = null;
		
		
		// DELETION
		protected array $delete_tables = [];
		protected array $delete_file_fields = [];
		protected array $delete_queries = [];
		
		// The base classes that should be used for avoiding extended classes
		protected static $base_class_names = array('TCm_Model');

		public static $date_regex =
			'/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])) ((([01])[0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/';
		
		
		/**
		 * Returns the name of the table for this model
		 * @return string
		 */
		public static function tableName() : string
		{
			return static::$table_name;
		}
		
		/**
		 * Changes the stored table name for this class to use the mirror table name
		 * @return void
		 */
		public static function switchToMirrorTable() : void
		{
			if(is_string(static::$mirror_table_suffix))
			{
				$current_table_name = static::$table_name;
				
				// Avoid multiple moments when this could happen
				if(!str_ends_with($current_table_name, static::$mirror_table_suffix))
				{
					static::$table_name = $current_table_name . static::$mirror_table_suffix;
				}
			}
		}
		
		/**
		 * TCm_Model constructor.
		 *
		 * In most cases the model represents a single item in the database which corresponds to a single row in a single
		 * table. A large portion of how TCm_Models work and operate is based on that convention.
		 *
		 * The constructor changes it's interaction depending on if the $table_name static class property is set. A model may
		 * be a simple structure with no database relevance.
		 *
		 * @param int|array $id The id can be an integer or an array. An integer corresponds to unique ID for the model. An array is a set
		 * of values used to initialize the model. The array should almost always match the properties from the table and is
		 * a shortcut instantiation technique that skips a database query.
		 * @param bool $load_from_table (Optional) Default true. Indicates if the properties should be loaded from the
		 * MySQL table if a valid table_id_column and $table_name are set for the class.
		 */
		public function __construct($id, $load_from_table = true)
		{
			// handle possibility of array loading
			if(is_array($id))
			{
				$this->temp_init_array = $id; // save init array for later use
				// This value should not be called from classes
				// Instead call $this->setPropertiesFromTable(); to handle saving values to the object


				$id = @$id[static::$table_id_column];	// pull out the ID for this item
			}

			parent::__construct($id);

			if(static::tableName() != '' && static::$table_id_column != '')
			{
				$this->addDeleteTable(static::tableName());
			}

			if($load_from_table && static::$table_id_column != '' && static::tableName() != '')
			{
				$class_name = get_called_class();
				if(!$class_name::$fails_initialization_with_id_0 && $this->id() == 0 )
				{

					// do nothing
				}
				else // we need to try and find it, failing if it can't
				{
					$this->setPropertiesFromTable();
				}

			}
			elseif(is_array($this->temp_init_array)) // not loading from a table but an array is given
			{
				$this->convertArrayToProperties($this->temp_init_array);
				$this->temp_init_array = false;
			}
		}

		/**
		 * Returns the title for this model.
		 *
		 * This method is commonly overloaded for each class to return a relevant string for the object.
		 *
		 * @return string
		 */
		public function title()
		{
			return self::$model_title.' #'.$this->id();
		}
		
		/**
		 * Returns the model title
		 * @return string
		 */
		public static function modelTitleSingular()
		{
			$title = static::$model_title;
			return TC_localize($title, $title);
		}
		
		/**
		 * Returns the model title but lowercase. This by default will apply strtolower, but can be extended for
		 * specific models if necessary.
		 * @return string
		 */
		public static function modelTitleSingularLower() : string
		{
			return strtolower(htmlspecialchars(static::modelTitleSingular()));
		}
		
		/**
		 * Returns the plural title for the model
		 * @return bool|string
		 */
		public static function modelTitlePlural()
		{
			$title = '';
			$class_name = get_called_class();
			if(static::$model_title_plural !== false)
			{
				$title = static::$model_title_plural;
			}
			else
			{
				$last_letter_of_name = substr(static::$model_title, -1);
				if($last_letter_of_name == 'y')
				{
					$title = substr(static::$model_title, 0, strlen(static::$model_title) - 1).'ies';
				}
				else
				{
					$title = (static::$model_title).'s';
				}
			}
			
			
			return TC_localize($title, $title);

		}
		
		/**
		 * Returns the model title but lowercase. This by default will apply strtolower, but can be extended for
		 * specific models if necessary.
		 * @return string
		 */
		public static function modelTitlePluralLower() : string
		{
			return strtolower(htmlspecialchars(static::modelTitlePlural()));
		}
		
		
		
		//////////////////////////////////////////////////////
		//
		// DATE ADDED
		//
		// Most database entries will have a value for `date_added`
		// that represents the timestamp when the object was created.
		//
		//////////////////////////////////////////////////////

		/**
		 * Returns the date added formatted to a specific format.
		 * @param string $format The format to be used
		 * @return string
		 * @uses date()
		 * @uses strtotime()
		 */
		public function dateAddedFormatted($format = 'F j, Y \a\t g:ia')
		{
			if($this->date_added == '' || is_null($this->date_added))
			{
				return '';
			}

			return date($format, strtotime($this->date_added));
		}

		/**
		 * Returns the date added, usually formatted as `YYYY-MM-DD hh:mm:ss`.
		 * @return mixed
		 */
		public function dateAdded()
		{
			return $this->date_added;
		}
		
		/**
		 * Returns the date modified, usually formatted as `YYYY-MM-DD hh:mm:ss`.
		 * @return string|null
		 */
		public function dateModified()
		{
			// Store all the possible dates that we find
			// Faster than trying to do comparisons every time
			$possible_dates = [];
			
			// Try and find the date modified
			$possible_dates[$this->date_added] = $this->date_added;
			$possible_dates[$this->date_modified] = $this->date_modified;
			
			foreach($this->relatedModelFunctions() as $class_name => $function)
			{
				// Check if we have a class_name for more efficient lookups
				if(is_string($class_name))
				{
					$query = "SELECT date_added, date_modified FROM `".$class_name::tableName()."` WHERE ".static::$table_id_column.' = :id';
					$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
					while($row = $result->fetch())
					{
						$possible_dates[$row['date_added']] = $row['date_added'];
						$possible_dates[$row['date_modified']] = $row['date_modified'];
					}
					
					// Special case to make looking up variables on page content more efficient
					if(TC_classUsesTrait($this,'TMt_PageRenderer'))
					{
						$render_item_table_name = $this->renderItemTableName();
						$variable_table_name = $this->variablesTableName();
						
						// Get all the dates from all the variables for this item
						// MUCH faster than loading possibly thousands of models, when we just need dates
						$query = "SELECT v.date_added, v.date_modified FROM `$variable_table_name` v
						INNER JOIN `$render_item_table_name` USING(content_id)
							WHERE ".$this->rendererIDColumn()." = :id";
						$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
						while($row = $result->fetch())
						{
							$possible_dates[$row['date_added']] = $row['date_added'];
							$possible_dates[$row['date_modified']] = $row['date_modified'];
						}
					}
					
				}
				else // models need to be loaded
				{
					// Ensure the method exists
					if(method_exists($this, $function))
					{
						// Get the models, confirm it's an array
						$related_models = $this->$function();
						if(is_array($related_models))
						{
							// Loop through the models
							foreach($related_models as $model)
							{
								// Only bother if it's actually a Tungsten model
								if($model instanceof TCm_Model)
								{
									$date_added = $model->dateAddedFormatted('Y-m-d H:i:s');
									$possible_dates[$date_added] = $date_added;
									
									$date_modified = $model->dateModifiedFormatted('Y-m-d H:i:s');
									$possible_dates[$date_modified] = $date_modified;
									
									
									$model->clearFromMemory();
								}
							}
						}
					}
				}
			}
			
			// Sort all the dates, so that the newest is first
			// Grab that and make that the date we care about
			rsort($possible_dates);
			$last_date = $possible_dates[0];
			return date('c', strtotime($last_date));
		}
		
		/**
		 * Returns an array of function names that should be called, each of which provides an array of related
		 * `TCm_Model` items, however that is determined for this model. This is used primarily for calculating date_modified.
		 *
		 * Implementations should extend this functionality, especially if they use page builders
		 * @return array An array of function names to be called. If an index is set, it must be the name of the model that is being loaded. This provides
		 * * shortcuts for looking up dates and is generally useful information.
		 */
		public function relatedModelFunctions()
		{
			if(TC_classUsesTrait($this,'TMt_PageRenderer'))
			{
				return [$this->pageRenderItemClassName() => 'contentItems'];
			}
			
			if(TC_classUsesTrait($this,'TMt_PageRenderItem'))
			{
				return ['variableModels'];
			}
			
			return [];
		}
		
		/**
		 * Returns the date modified formatted to a specific format.
		 * @param string $format The format to be used
		 * @return string
		 * @uses date()
		 * @uses strtotime()
		 */
		public function dateModifiedFormatted($format = 'F j, Y \a\t g:ia')
		{
			return date($format, strtotime($this->dateModified()));
		}
		
		/**
		 * Validates if one time is after another. This performs a string comparison on the two dates to ensure that
		 * `$before` is before `$after`.
		 * @param string $before The first time
		 * @param string $after The second time, which should come after the `$before`
		 * @return true Indicates if they order is correct
		 */
		public static function validateDateOrder(string $before, string $after)
		{
			return $after > $before;
		}
		
		/**
		 * Validates if the date string contains a valid date
		 * @param string $date_string the string must be formatted as YYYY-MM-DD and any time values are ignored
		 */
		public static function validateDateString(string $date_string)
		{
			return checkdate(
				substr($date_string,5,2), // month
				substr($date_string,8,2), // day
				substr($date_string,0,4)  // year
			);
		}
		
		
		//////////////////////////////////////////////////////
		//
		// PROPERTY SETTINGS
		//
		// Most database entries will have a value for `date_added`
		// that represents the timestamp when the object was created.
		//
		//////////////////////////////////////////////////////

		/**
		 * Sets the internal properties for this model based on the static table properties.
		 *
		 * This method will parse the values provided when initialized if it was an array. OTherwise it will query the
		 * database for the appropriate values.
		 *
		 * In each case, every value provided will be saved as a property of this model.
		 *
		 * @param string|bool $table (Optional) Default false which will use the TCv_View::tableName() set for the model.
		 * @return bool Indicates if the properties were successfully set.
		 *
		 * @uses TCm_Model::convertArrayToProperties()
		 */
		protected function setPropertiesFromTable($table = false)
		{
			// Check if we're doing memcached
			// If we have it, get out, skip any queries
			if(static::cachePropertiesFromTable())
			{
				$model_values = TC_Memcached::get($this->cacheKeyName());
				if(is_array($model_values))
				{
					$this->convertArrayToProperties($model_values);
					$this->temp_init_array = false;
					return true;
				}
			}
			
			if($this->properties_set_from_table)
			{
				return true;
			}

			if(!$table)
			{
				$table = static::tableName();
			}
			if($this->temp_init_array)
			{
				if(is_array($this->temp_init_array))
				{
					$this->convertArrayToProperties($this->temp_init_array);
					$this->temp_init_array = false;
				}
				else
				{
					$this->properties_set_from_table = true;
					return parent::destroy();
				}
			}
			elseif(static::$table_id_column != '')
			{
				$table = trim($table);
				
				// Simple table name, we escape it, possibly more complicated with aliases, etc
				$query = "SELECT * FROM ";
				if(str_contains($table,' '))
				{
					$query .= "$table";
				}
				else
				{
					$query .= "`$table`";
				}
				$query .= " WHERE ".static::$table_id_column." = :id LIMIT 1";
				
				
				$values = array('id' => $this->id);

				if($this->id >= 0)
				{
					$statement = $this->DB_Prep_Exec($query, $values);

					if($statement->rowCount() > 0)
					{
						$row = $statement->fetch();
						$this->convertArrayToProperties($row);
					}
					else
					{
						$this->properties_set_from_table = true;
						return parent::destroy();
					}
				}
				else
				{
					$this->properties_set_from_table = true;
					return parent::destroy();
				}

			}
			else
			{
				$this->addConsoleWarning('Attempting to set properties from table '.$table.' without primary_id_column being set');
				$this->exists = false;
			}
			$this->properties_set_from_table = true;
			return true;
		}

		/**
		 * Converts an array of values into properties for this model.
		 * @param array $values The array of values to be converted.
		 */
		protected function convertArrayToProperties($values)
		{
			foreach($values as $index => $value)
			{
				$this->$index = $value;
			}
			
			// Deal with memcached
			if(static::cachePropertiesFromTable())
			{
				//if not already in the cache and must be returning an array
				$current_value = TC_Memcached::get($this->cacheKeyName());
				if(!is_array($current_value))
				{
					TC_Memcached::set($this->cacheKeyName(), $values);
				}
				
			}
			
			$this->properties_set_from_table = true; // avoid any potential double-calls

		}

		/**
		 * Returns a the value for a given property.
		 * @param string $property The name of the property
		 * @return mixed
		 */
		public function valueForProperty($property)
		{
			return @$this->$property;
		}

		/**
		 * Returns if the property exists
		 * @param string $property The name of the property
		 * @return bool
		 */
		public function propertyExists($property)
		{
			return isset($this->$property);
		}



		/**
		 * Updates a single value in the database as well as the corresponding value in this model. This method
		 * checks to see if the value actually changed, saving unnecessary DB calls.
		 * @param string $field_name The name of the field
		 * @param mixed $value The new value for the field
		 */
		protected function updateDatabaseValue($field_name, $value)
		{
			// Check if the value changed. Avoid unnecessary DB calls
			if($this->$field_name !== $value)
			{
				$query = "UPDATE `" . (static::tableName()) . "` SET " . $field_name . " = :value WHERE  " . (static::$table_id_column) . " = :id LIMIT 1";
				$this->DB_Prep_Exec($query, array('value' => $value, 'id' => $this->id()));
				$this->$field_name = $value;

				$this->clearCache();

			}

		}

		/**
		 * Updates the model with a set of values.
		 *
		 * This method will update the database with the values provided as well as the internal values in the model
		 *
		 * @param array $values An array of key=>value pairs that represent the values for this model.
		 * @param array $bind_param_values (Optional) Default empty array. The list of any bind parameter values. The indices
		 */
		public function updateWithValues(array $values, $bind_param_values = array())
		{
			// Run the validation against the schema
			// In the editor mode, we pass in the object, to ensure the validation has all the info
			$validation = static::validateSubmittedValuesAgainstSchema($values, 'update', $this);
			
			// errors found
			if(is_array($validation))
			{
				foreach($validation as $values)
				{
					TC_trackProcessError($values['message'],'field_validation', $values['field_id']);
				}
				return false;
			}
			
			// PERFORM THE UPDATE
			// Only bothers if it finds a change in the values
			$query = "UPDATE `".(static::tableName())."` SET ";
			$snippets = [];
			
			
			// Track values for updating the model
			$model_update_values = [];
			foreach($values as $name => $value)
			{
				if($value === "now()")
				{
					// Handle the now() value being passed for tracking
					$snippets[] = " $name = now() ";
					
					$model_update_values[$name] = date('Y-m-d H:i:s');
					unset($values[$name]); // remove it from the array of provided values
				}
				else
				{
					$snippets[] = " $name = :$name ";
					$model_update_values[$name] = $value;
				}
				
			}
			
			$query .= implode(', ', $snippets);
			$query .= " WHERE  ".(static::$table_id_column)." = :primary_model_id LIMIT 1";
		
			if($this->checkIfUpdateValuesTriggerChange($values))
			{
				$values['primary_model_id'] = $this->id();
				$statement = $this->DB_Prep_Exec($query, $values, $bind_param_values);
				if($statement->errorCode() == '00000')
				{
					foreach($model_update_values as $name => $value)
					{
						$this->$name = $value;
					}
				}
			}
			else // query skipped
			{
				$values['primary_model_id'] = $this->id();
				$console_item = new TSm_ConsoleItem(htmlspecialchars(static::compositeQueryWithValues($query, $values)),TSm_ConsoleQuerySkipped,false);
				$console_item->setIsSkipped();
				$console_item->setDetails("No changes to the database detected");
				$this->addConsoleItem($console_item);
			}

			$this->clearCache();


		}
		
		/**
		 * Tests if the provided update values will trigger a change on the model. This compares each entry against
		 * the current values of the model to determine if anything is different from what's stored in the model.
		 * @param array $update_values
		 * @return bool
		 */
		public function checkIfUpdateValuesTriggerChange(array $update_values) : bool
		{
			foreach($update_values as $column_name => $new_value)
			{
				// Detect a change, this must also check for type, since nulls and zeros need to be handled properly
				if($new_value !== $this->$column_name)
				{
					return true;
				}
			}
			
			return false;
		}

		/**
		 * Creates a new instance of this model using the values provided.
		 *
		 * This static method will accept the value parameters and attempt to insert them into the table for this model. The
		 * values for the table entirely depend on the name of the class used to call this static method.
		 *
		 * For example to create a new `TMm_User`, you would need to call `TMm_User::createWithValues($values)`
		 *
		 * @param array $values An array of key=>value pairs that represent the values for this model.
		 * @param array $bind_param_values (Optional) Default empty array. The list of any bind parameter values. The indices
		 * for the binding parameters should match those of the properties that those parameters belong to.
		 * @param bool $return_new_model (Optional) Default true. Indicates if the newly created object should be
		 * instantiated and returned. This is a heuristic option to speed up complex calls where the model isn't needed
		 * @return static|null|int Returns the newly created model or null if it did not succeed. If the model init
		 * was skipped but successful it will return the value 0. This will still not trigger.
		 * @todo remove the bad init of TCm_Model and call the DB class instead
		 *
		 * @see PDOStatement::bindParam()
		 */
		public static function createWithValues(array $values, $bind_param_values = array(), $return_new_model = true)
		{
			$table_name = static::tableName();

			// Run the validation against the schema
			$validation = static::validateSubmittedValuesAgainstSchema($values,'create');
			
			// errors found
			if(is_array($validation))
			{
				foreach($validation as $values)
				{
					TC_trackProcessError(get_called_class()." not created: ".$values['message'],
					                     'field_validation',
					                     $values['field_id']);
					
				}
				return null;
			}
			
			$new_model = null; // Null is what is returned when a model does not succeed
			try
			{
				$set_values = array();
				foreach($values as $index => $value)
				{
					$index = str_ireplace(':', '', $index);
					$set_values[$index] = "`".$index."` = :$index";
					
					if(is_object($value))
					{
						TC_triggerError('Objects cannot be passed as database values : '.$index);
					}
				}

				// if this class saves the user_id and it isn't already set
				if(static::$store_user_id_when_creating && !isset($set_values['user_id']))
				{
					if($user = TC_currentUser())
					{
						$set_values['user_id'] = 'user_id = :user_id';
						$values['user_id'] = $user->id();
					}
				}


				$query = "INSERT INTO $table_name SET ".implode(', ', $set_values);
				if(!isset($set_values['date_added']))
				{
					$query .= ', date_added = now()';
				}
				$result = static::DB_RunQuery($query, $values, $bind_param_values);
				
				if($result->errorCode() == '00000')
				{
					if($return_new_model)
					{
						// Test for an auto-incremented ID, which won't always be the case
						$id = static::DB_Connection()->insertedID();
						if($id > 0)
						{
							$new_model = static::init($id);
						}

						// detect if we've manually set the value for the ID
						// instantiate with that ID instead
						elseif(isset($values[static::$table_id_column]))
						{
							$new_model = static::init($values[static::$table_id_column]);
						}
					}
					else
					{
						$new_model = 0;
					}
				}
			}
			catch(Exception $e)
			{

			}

			/**
			 * Workflows creation
			 * If we are using workflows AND there is a new model using the Workflow Item trait,
			 * then we will need to create default workflows
			 * @var TMt_WorkflowItem $new_model
			 * @author Katie Overwater
			 */
			if(TC_getConfig('use_workflow') &&
				$new_model instanceof  TCm_Model && TC_classUsesTrait($new_model, 'TMt_WorkflowItem'))
			{
				$new_model->generateDefaultWorkflows();
			}

			return $new_model;
		}

		/**
		 * Returns the values that are used for duplication
		 * @param array|bool $override_values An array of values that are overridden in the duplication process
		 * @return array|bool
		 */
		public function duplicationValues($override_values = false)
		{

			$query = "SELECT * FROM `" . static::tableName() . "` WHERE " . static::$table_id_column . " = :id LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			if($item_row = $result->fetch())
			{
				unset($item_row[static::$table_id_column]); // unset the id column
				unset($item_row['date_added']); // unset the date added
				unset($item_row['date_modified']); // unset the date modified
				if(is_array($override_values))
				{
					foreach($override_values as $index => $value)
					{
						$item_row[$index] = $value;
					}
				}
				return $item_row;
			}
			return false;
		}

		/**
		 * Duplicates this item with the exact same values but a new ID
		 * @param array|bool $override_values An array of values that are overridden in the duplication process
		 * @return static|TCm_Model|bool
		 */
		public function duplicate($override_values = false)
		{
			if($values = $this->duplicationValues($override_values))
			{
				$new_item = static::createWithValues($values);

				return $new_item;
			}
			return false;
		}
		
		/**
		 * Checks if the values provide differ from what's currently set for this model
		 * @param array $values
		 * @return bool
		 */
		public function valuesDifferFromCurrent(array $values)
		{
			foreach($values as $name => $value)
			{
				// Shortcut if the property doesn't even exist, 100% differs
				if(!isset($this->$name))
				{
					return true;
				}
				
				if($this->$name != $value)
				{
					return true;
				}
			}
			return false;
		}

		//////////////////////////////////////////////////////
		//
		// DELETION
		//
		// The deletion of models almost always corresponds with
		// changes to the database.
		//
		// Ideally the database is setup with foreign keys to
		// handle the majority of the deletion interactions.
		//
		// These methods provide a secondary mechanism to deal
		// with scenarios that aren't as clear-cut.
		//
		//////////////////////////////////////////////////////


		/**
		 * Adds a table to this model that should be triggered if this item is ever deleted.
		 *
		 * The table must have a column that contains the primary_key for this model which is found in static::$table_id_column.
		 *
		 * @param string $table The name of the table that also requires a delete action.
		 * @param string|bool $column (Optional) Default false will insert static::$table_id_column. If provided, then this
		 * must be the name of the column in the table where the ID for this model can be found.
		 */
		public function addDeleteTable($table, $column = false)
		{
			if($column)
			{
				$this->delete_tables[$table] = $column;
			}
			elseif(static::$table_id_column != '')
			{
				$this->delete_tables[$table] = static::$table_id_column;
			}
			else
			{
				TC_triggerError('Attempting to add a delete table to '.get_called_class().' without defining static property $table_id_column');
			}
		}

		/**
		 * Adds a field that should be emptied when this object is deleted.
		 * @param string $field The name of the field in this model that is used to find the relevant filename
		 * @param string $path The path from teh site root where the file can be found
		 */
		public function addDeleteFileField($field, $path)
		{
			$this->delete_file_fields[$field] = $path;
		}

		/**
		 * Adds a query that should be run when this item is deleted.
		 * @param string $query The query string to be run
		 * @param array $values The array of values to be passed into the query
		 */
		public function addDeleteQuery($query, $values)
		{
			$this->delete_queries[] = array('query' => $query, 'query_values' => $values);
		}

		/**
		 * Deletes this model which runs any database actions necessary. If the delete action is successful, the
		 * `exists` flag is changed to fals which can be used to test the existence of the model.
		 * @param string $action_verb
		 * @see TCm_Model::addDeleteFileField()
		 * @see TCm_Model::addDeleteQuery()
		 * @see TCm_Model::addDeleteTable()
		 */
		public function delete($action_verb = 'deleted')
		{
			$values = array('id'=> $this->id());
			$class_name = get_called_class();

			$message_title = $class_name::$model_title;

			if($this->userCanDelete())
			{
				try
				{
					$this->DB()->beginTransaction();

					// DELETE any cache associated with this model
					$this->clearCache();

					foreach($this->delete_tables as $table_name => $id_column)
					{

						$query = "DELETE FROM $table_name WHERE $id_column = :id";
						$result = $this->DB_Prep_Exec($query, $values);
					}

					foreach($this->delete_file_fields as $field_name => $path)
					{
						$file = new TCm_File($field_name, $this->$field_name, $path);
						$file->delete();
					}

					foreach($this->delete_queries as $delete_query_settings)
					{
						$query = $delete_query_settings['query'];
						$query_values = $delete_query_settings['query_values'];
						$result = $this->DB_Prep_Exec($query, $query_values);

					}

					$this->DB()->commit();
					$this->exists = false; // set the exists flag

				}
				catch (Exception $e)
				{
					$this->DB()->rollBack($e->getMessage());
					$error_message = '<em>'.$this->title()."</em> could not be deleted. ";
					TC_message($error_message, false);

					if(strpos($this->DB()->lastErrorMessage(),'CONSTRAINT') > 0)
					TC_message('This item is used by another item in the system and cannot be deleted. ');
					return;
				}


				if($action_verb)
				{
					$message = $message_title.'  '.$action_verb;
					TC_message($message, true);
				}

			}
			else
			{
				if($action_verb)
				{
					$message = $message_title.'  not '.$action_verb;
					TC_message($message, false);
				}
			}
		}

		//////////////////////////////////////////////////////
		//
		// USER PERMISSIONS
		//
		// These methods are at the core of how other classes
		// determine the permission for this model.
		//
		// These methods can be overloaded but by default they each
		// reference the TC_currentUser() method to identify who
		// is trying to view the model.
		//
		//////////////////////////////////////////////////////


		/**
		 * Returns if the user can create new items of this model.
		 * @param TMm_User|bool $user (Optional) Default false which means the current user will be tested against.
		 * @return bool
		 *
		 * @uses TC_currentUser()
		 * @uses TMm_User::hasTungstenAccess()
		 */
		public static function userCanCreate($user = false)
		{
			if(!$user)
			{
				$user = TC_currentUser();
			}

			if(!$user)
			{
				return false;
			}
			return $user->hasTungstenAccess();
		}

		/**
		 * Returns if the user can view/see this model.
		 *
		 * @param TMm_User|bool $user (Optional) Default false which means the current user will be tested against.
		 * @return bool
		 * @uses TC_currentUser()
		 * @uses TMm_User::hasTungstenAccess()
		 */
		public function userCanView($user = false)
		{
			if(!$user && !$user = TC_currentUser())
			{
				return false;
			}
			
			return $user->hasTungstenAccess();
		}

		/**
		 * Returns if the user can edit/modify this model.
		 *
		 * @param TMm_User|bool $user (Optional) Default false which means the current user will be tested against.
		 * @return bool
		 * @uses TC_currentUser()
		 * @uses TMm_User::hasTungstenAccess()
		 */
		public function userCanEdit($user = false)
		{
			if(!$user && !$user = TC_currentUser())
			{
				return false;
			}
			
			return $user->hasTungstenAccess();
		}


		/**
		 * Returns if the user can deletethis model.
		 *
		 * @param TMm_User|bool $user (Optional) Default false which means the current user will be tested against.
		 * @return bool
		 * @uses TC_currentUser()
		 * @uses TMm_User::hasTungstenAccess()
		 */
		public function userCanDelete($user = false)
		{
			if(!$user && !$user = TC_currentUser())
			{
				return false;
			}
			
			return $user->hasTungstenAccess();
		}

		/**
		 * Returns the name of the URL target for editing this item in the admin. By default, the value returned is "edit".
		 * @return string
		 */
		public function adminEditURLTargetName()
		{
			return 'edit';
		}


		/**
		 * Returns the default url for editing this item. This assumes we have the
		 * @return string
		 */
		public function adminEditURL()
		{
			return $this->moduleForThisClass()->pathForDoAction(). $this->adminEditURLTargetName().'/'.$this->id();
		}

		//////////////////////////////////////////////////////
		//
		// PAIRING TABLES
		//
		// Pairing tables are used for one-to-many matches
		// for DB values associated with this model.
		//
		// They can be used to track a pair of IDs for this model
		// and another ID that relates to it.
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Updates a pairing table of two different models that are paired together. The table requires a very
		 * specific format in order to work properly.
		 * - One column must have the same name as the primary ID for this class.
		 * - One column must have the same name as the $paired_column_name
		 * - Pairing table must have a `date_added` field.
		 *
		 * When it is run, it compares the current paired values with the new values provided.
		 * - If the value exists, it leaves it alone
		 * - If the value doesn't exist, it adds it
		 * - If the value exists, but isn't in the new values, it's deleted
		 *
		 * @param string $table_name The name of the table where the pairing occurs
		 * @param string|TCm_Model $paired_column_name The name of the column that the second pairs go in
		 * This
		 * @param int[] $new_values The new values as an array of IDs
		 */
		public function updatePairingTable($new_values, $table_name, $paired_column_name)
		{
			// Empty value must continue forward with an empty array of values
			// It indicates to delete them all since there are no values anymore
			if($new_values === '' || is_null($new_values) )
			{
				$new_values = [];
			}
			
			if(!is_array($new_values))
			{
				TC_triggerError('Pairing Table Failure: Provide values must be an array. '.$new_values);
			}
			
			$error_found = false;
			
			$query ='SELECT * FROM `'.$table_name.'` WHERE '.$this::$table_id_column.' = :id ';
			$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
			
			// Track all the current ones as potentially to delete
			// We clear them out of the array as we go
			$existing_items_to_delete = [];
			while($row = $result->fetch())
			{
				$existing_items_to_delete[$row[$paired_column_name]] = $row;
			}
			
			// PREPARE THE INSERT STATEMENT FOR EFFICIENCY
			$query = "INSERT INTO `".$table_name."` SET ".$this::$table_id_column." = :id , ".$paired_column_name." = :paired_id, date_added = now()";
			$statement = $this->DB_Prep($query);
			
			// Loop through new values, checking to see if it already exists or if we should add it
			foreach($new_values as $new_value)
			{
				if($new_value != '' && $new_value > 0)
				{
					// Item already exists, remove it from the list of items to delete
					if(isset($existing_items_to_delete[$new_value]))
					{
						unset($existing_items_to_delete[$new_value]);
					}
					else // does not exist, must add it
					{
						$statement = $this->DB_Exec($statement, ['id' => $this->id(), 'paired_id' => $new_value]);
						if($statement->errorCode() != '00000')
						{
							$error_found = true;
						}
					}
				}
			}
			
			// LOOP THROUGH REMAINING ONES, WHICH SHOULD DEFINITELY BE DELETED
			if(count($existing_items_to_delete) > 0)
			{
				// PREPARE THE INSERT STATEMENT FOR EFFICIENCY
				$query = "DELETE FROM `" . $table_name .
					"` WHERE " . $this::$table_id_column . " = :id AND " .
					$paired_column_name . " = :paired_id LIMIT 1";
				$statement = $this->DB_Prep($query);
				
				
				foreach($existing_items_to_delete as $paired_id => $row)
				{
					$statement = $this->DB_Exec($statement, array('id' => $this->id(), 'paired_id' => $paired_id));
					if($statement->errorCode() != '00000')
					{
						$error_found = true;
					}
				}
			}
			if($error_found)
			{
				TC_message('Error Updating Values', false);
				
			}
			
		}

		/**
		 * returns the current values for a pairing table assuming the id column name matches this primary key
		 * @param string $table_name
		 * @param string $paired_column_name
		 * @return int[]
		 */
		public function currentValuesFromPairingTable($table_name, $paired_column_name)
		{
			$values = array();
			$query = 'SELECT * FROM `'.$table_name.'` WHERE '.$this::$table_id_column.' = :id ';
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			while($row = $result->fetch())
			{
				$values[] = $row[$paired_column_name];
			}
			return $values;
		}



		//////////////////////////////////////////////////////
		//
		// UTILTY METHODS
		//
		//////////////////////////////////////////////////////

		/**
		 * This function takes a string replaces any "config codes" in the format of [:code:] in that string with the
		 * equivalent config value set in TC_config. This means that a a value of [:start_date:] will be replaced with the
		 * value in $TC_config['start_date']
		 * @param string $input The string that might have config codes
		 * @return string
		 */
		public function convertConfigCodes($input)
		{
			$start_position = strpos($input, '[:');
			while($start_position !== false)
			{
				$stop_position = strpos($input, ':]');
				$variable_name = substr($input, $start_position+2, $stop_position-$start_position-2);
				$input = str_ireplace('[:'.$variable_name.':]', TC_getConfig($variable_name), $input);
				$start_position = strpos($input, '[:');
			}
			return $input;
		}


		/**
		 * Clears any inline styles of the html provided with the exception of text-align center or right.
		 * @param string $html The html to be cleaned
		 * @return string
		 * @deprecated see TCv_FormItem_HTMLEditor::cleanupHTML
		 */
		public function clearInlineStyles($html)
		{
			return $html;
		}

		/**
		 * Clears any inline styles of the html provided. It will check a value called "encoding_converted" which allows
		 * for a value to be set in a DB to avoid it continuously happening. This function will also set that value to 1,
		 * so any table with that value will fail the DB update.
		 */
		public function correctUTF8EncodingForFields()
		{
			if(!$this->encoding_converted)
			{
				$values = array();
				$db_strings = array();
				foreach(func_get_args() as $field_name)
				{
					$encoding = @mb_detect_encoding($this->$field_name, false, true); // strictly find an encoding
					if($encoding != 'UTF-8')
					{
						$values[$field_name] = mb_convert_encoding($this->$field_name, "UTF-8", $encoding);
						$db_strings[$field_name] = $field_name.' = :'.$field_name;
					}

				}

				if(sizeof($values) > 0)
				{
					$values['id'] = $this->id();
					$query = "UPDATE `".(static::tableName())."` SET encoding_converted = 1, ".implode(', ', $db_strings)." WHERE ".(static::$table_id_column)." = :id";
					$this->DB_Prep_Exec($query, $values);

					foreach($values as $field_name => $new_value)
					{
						$this->$field_name = $new_value;
					}
				}
			}

		}


		/**
		 * Returns a pre-formatted set of results for the provided filter values for a TCv_SearchableModelList.
		 *
		 * @param array $filter_values The provided filter values
		 * @param array|PDOStatement $result The results for the filter which can be an array of strings or TCm_Models or
		 * it can be a `PDOStatement` with results.
		 * @return array A specifically formatted array that is used
		 *
		 * @see TCv_SearchableModelList This class uses this method for various return values.
		 * @deprecated This method has been moved to the TCv_SearchableModelList class. This method is a pass-through that will
		 * call that same method for a dummy instantiated searchable list view.
		 */
		public function returnValueForFilterResult($filter_values, $result)
		{
			$list = new TCv_SearchableModelList();
			$this->addConsoleWarning('The method returnValueForFilterResult() is deprecated in TCm_Model. Your filters should simply return DB results or an array of models to be processed. ');
			return $list->returnValueForFilterResult($filter_values, $result);

		}


		/**
		 *
		 * @param string $action
		 * @param bool $message
		 * @return bool
		 * @deprecated No long in use and has no action.
		 */
		public function trackInHistory($action, $message = false)
		{
			return false;
		}
		
		//////////////////////////////////////////////////////
		//
		// All MODELS
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Returns all the models for this type
		 * @return TCm_Model[]
		 */
		public static function allModels() : array
		{
			$models = [];
			$query = "SELECT * FROM `".static::tableName()."`";
			if( is_string(static::$primary_table_sort))
			{
				$query .= " ORDER BY ".static::$primary_table_sort;
			}
			
			$model = new TCm_Model(false);
			$result = $model->DB_Prep_Exec($query);
			while($row = $result->fetch())
			{
				$models[] = static::init($row);
			}
			
			return $models;
			
		}


		/**
		 * Builds and generates the query for filtering and then returns the related models associated with that model
		 * @param class-string<TCm_Model> $model_class_name The name of the model to be instantiated
		 * @param string $query The query that should be run. This will be appended with limits and offsets
		 * @param array $db_values The db values for the query
		 * @param array $filter_values The values coming in from the filter
		 * @return TCm_Model[]
		 */
		public function getModelsForFilteredQuery(string $model_class_name, string $query, array $db_values, array $filter_values) : array
		{
			// Append limit to the query
			$num_page_page = 0;
			if(isset($filter_values['num_per_page']))
			{
				$num_page_page = $filter_values['num_per_page'];
			}

			// Pagination is enabled
			if($num_page_page > 0)
			{
				$query .= ' LIMIT ' . $num_page_page . ' OFFSET ' . (($filter_values['page_num'] - 1) * $num_page_page);
			}
			
			// Queries should be run based on the model class name, to respect distributed systems
			$result = $model_class_name::DB_RunQuery($query, $db_values);
			$models = [];
			while($row = $result->fetch())
			{
				$models[] = $model_class_name::init($row);
			}

			return $models;
		}


		//////////////////////////////////////////////////////
		//
		// SCHEMA
		//
		// We define the fields for a model here. This works in
		// conjunction with the schema.php file which is still used
		// until this can be phased in entirely.
		//
		// The schema should define all the fields for the model
		// but also the validation.
		//
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Returns an array of arrays which define the schema for this model. The index of each item is the field
		 * name and the properties are specific and defined below:
		 * `title` : (Optional) The commonly used name for this item. If not included, then the title is derived from
		 * the ID of the field with underscores replaced with spaces using sentence case.
		 *
		 * `type` : The DB column type for this item. This value can also be a TCm_Model name in which case it will
		 * set the type to the `int(10) unsigned` to match all the primary keys in the system however it will also
		 * validate that the model exists
		 *
		 * `comment` : Any comments on this field
		 *
		 * `nullable` : Indicates if this field can be null
		 *
		 * `foreign_key` : A foreign key string or an array of values. For the array of values it requires a
		 *  `model_name` and a `delete` and/or `update` value which corresponds to the On Delete and On Update values
		 * such as CASCADE, RESTRICT, or SET NULL. Using a model name will automatically generate a key to the
		 *  table and primary key for that model
		 *
		 * `delete` : A indicator values that ensures the column is deleted and no longer in the DB. This is used to
		 * ensure schemas remain current and remove useless fields.
		 *
		 * ********************************
		 *
		 * `validations`: An array of key/value items which are named validations that we can call.
		 *     `pattern` : A regex pattern that must be matched
		 *     `methods` : An array of validation methods . Each value is an array itself with specific values that
		 *           indicate how that method should be handled.
		 *
		 *          `class_field_name` : (OPTIONAL) the name of the field associated with a class that will call this
		 *              method. This is usually `xyz_id` and it's type must be a TCm_Model name
		 *              If omitted, then it will call the static `method_name` on the calling class.
		 *          `method_name` : the name of the method to be called
		 *          `parameter_field_names` : array of field names that are passed into the method as parameters
		 *          `error_message` : The message displayed if the validation fails.
		 *
		 * The methods must return a boolean value
		 *
		 * ********************************
		 *
		 * `content_flags` : An array of methods that should be called when checking the model for content flags.
		 * These methods must either return a `TMm_ContentFlag` or a boolean false. These methods must be callable on
		 * the model itself. Each method is passed in the "field name" which can then be used to acquire the value or
		 * perform other actions.
		 *
		 * ********************************
		 *
		 * `table_keys` : An array of table keys where the index is the name of the key and then value is an
		 * associative array with two values for the `index` and `type`. The `index` is a string and the value for
		 * the key to be added. The `type` value is a string which indicates "unique", "normal", etc.
		 *
		 * Eg:
		 * 'table_keys' => [
		 *		'user_id_group_id' => ['index' => "('user_id','group_id')", 'type' => 'unique']
		 *	]
		 *
		 * ********************************
		 *
		 * TRAITS
		 *
		 * If you need a trait to modify all the models they get attached to, then you can create a method in the
		 * trait which is added to the schema as part of TCm_Model. Since traits get added to the model, we can't use
		 * the same method name since they will overwrite each other. So the method must be called
		 * `schema_TMt_TraitName`. Where `TMt_TraitName` is the name of the specific trait. The schema function will
		 * find all the traits applied to the model, see if the method exists and if so, add those schema values to
		 * the model for installation.
		 *
		 * @return array[]
		 */
		public static function schema()
		{
			// All models have a date_added field
			$schema = [
				'date_added' => [
					'title' => 'Date added',
					'type'=> 'datetime',
					'nullable' => true,
					'readonly' => true
				],
				
				// Timestamp for date_modified
				'date_modified' => [
					'title' => 'Date modified',
					'type' => 'timestamp NULL ON UPDATE CURRENT_TIMESTAMP',
					'nullable' => true,
					'readonly' => true,
				]
			];
			
			
			// Detect all the traits associated with the model
			foreach(static::allTraits() as $trait_name)
			{
				// Check to see if that class has a method for the schema for the trait
				$trait_method_name = 'schema_'.$trait_name; // generate the method name for the schema
				$class_name = get_called_class(); // get the class being used
				if(method_exists($class_name ,$trait_method_name ))
				{
					// Get the values
					$trait_schema_values = $class_name::$trait_method_name();
					
					if(is_array($trait_schema_values))
					{
						// Combine the values with our existing schema
						$schema = array_merge($schema, $trait_schema_values);
					}
				}
			}
			
			return $schema;

		}
		
		/**
		 * Returns if the field is localized in the schema
		 * @param string $field_name
		 * @return bool
		 */
		public static function schemaFieldIsLocalized(string $field_name)
		{
			$schema = static::schema();
			
			return isset($schema[$field_name]['localization']) && $schema[$field_name]['localization'];
		}
		
		
		/**
		 * Returns the module folder that this item installs in. If you are declaring a model that extends a
		 * different model and that model lives in another module, you need to set this in order to explicitly allow
		 * Tungsten to install across modules.
		 *
		 * This is set on the extending model, returning the folder name of the module that it is meant to install in.
		 * @return string
		 */
		public static function installModuleFolder() : string
		{
			return static::baseClass()::moduleForClassName()->folder();
		}
		
		
		/**
		 * An associative array of queries that should be run if we're upgrading from an older version. The indices
		 * should be version number that it should apply to if we're passing that value. So if the index is `2.0.1`
		 * Then if we're installing from `2.0.0` or earlier, this will run AFTER other updates.
		 *
		 * The value for each index should be an array itself since multiple queries might be required.
		 * @return array
		 */
		public static function installUpdateQueries()
		{
			return [];
		}
		
		/**
		 * Returns the schema for this model, but only the fields that are localized.
		 * @return array
		 */
		public static function localizedSchemaFields()
		{
			$schema = array();
			foreach(static::schema() as $field_name => $values)
			{
				if(isset($values['localization']) && $values['localization'])
				{
					$schema[$field_name] = $values;
				}
			}
			
			return $schema;
		}
		
		
		/**
		 * This method validates the provided values against the schema() for this model. It returns the first error
		 * it finds with the field that triggered the error.
		 * @param array $values
		 * @param string $mode (Default 'create') Indicates the mode that is being used. The default is 'create'
		 * which indicates that we're making a new item of this type. The alternative is 'update'. This leaves options
		 * open for expanding the functionality with other modes for specific classes.
		 * @param null|TCm_Model $model The model used when editing
		 * @return true|string[] Returns a value of true if successful or an array with the field id and error message
		 */
		public static function validateSubmittedValuesAgainstSchema(
			array $values,
			string $mode = 'create',
			$model = null)
		{
			if($mode == 'update' && !($model instanceof TCm_Model))
			{
				TC_message('There was an error in processing your request. Please refresh, try again or log out and back in.', false);
				TC_triggerError('Model ('.get_called_class().') required in `update` mode for validateSubmittedValuesAgainstSchema()');
			}
			
			// Include the primary key when editing
			if($model)
			{
				$values[$model::$table_id_column] = $model->id();
			}
			
			$schema = static::schema();
			
			// Fill in $values array with consistent values from editor or other sources
			foreach($schema as $field_id => $schema_values)
			{
				// no value but we're in an updater, so the model has a value
				if($mode == 'update' && !isset($values[$field_id]))
				{
					$values[$field_id] = $model->valueForProperty($field_id);
				}
				
				// Deal with no title
				if(!isset($schema_values['title']))
				{
					$schema[$field_id]['title'] = ucfirst(str_replace('_',' ', $field_id));
				}
			}
			
			// Track the errors
			$errors = [];
			
			// Loop through the schema, validating and testing each of the fields against the values provided which
			// must meet all the requirements.
			foreach($schema as $field_id => $schema_values)
			{
				// Track the provided field value, which might not even be there
				$field_value = null;
				if(isset($values[$field_id]))
				{
					$field_value = $values[$field_id];
				}
				
				
				// save the validations in a convenient variable for readability
				$validations = [];
				if(isset($schema_values['validations']))
				{
					$validations = $schema_values['validations'];
				}
				
				// STEP 1 : REQUIRED validation
				// Handled first. Simplest test, all required fields must be set all the time
				if(isset($validations['required'])
					&& $validations['required'] == true)
				{
					// Check if the value is null, or it's an empty array or it's an empty string
					if($field_value == null
						|| (is_array($field_value) && count($field_value) == 0 )
						|| (trim($field_value) == '') )
					{
						$errors[] = [
							'field_id' => $field_id,
							'message' => $schema_values['title'].' '
								.TC_localize('is_required', 'is required')
						];
						continue; // not provided and required, go to the next item in the schema
					}
				}
				
				
				// STEP 2 : MODEL TYPES
				// Testing models is complex since they aren't always set and implicit assumptions create schema
				// inconsistencies. If an ID is numeric, it will validate them no matter what.
				
				// If not, then it checks if the required or nullable values permit a blank-type value
				if(isset($schema_values['type']) && substr($schema_values['type'],0,3) == 'TMm')
				{
					
					// Flag to test for validation
					$perform_validation = false;
					
					// Check if we have a value that could be an ID for a class, then we always validate it
					// Note that a value of 0 will trigger this as well
					if(is_int($field_value) || (is_string($field_value) && trim($field_value) != ''))
					{
						$perform_validation = true;
					}
					
					// Check if we have  set required validation
					// Regardless of the format, we've explicitly set a validation of required
					if(isset($validations['required']))
					{
						$perform_validation = $validations['required'];
					}
					
					// Lastly, check if it's nullable which only matters if there's no explicit required value
					// and it's not a numeric value
					elseif(isset($schema_values['nullable']))
					{
						$perform_validation = !$schema_values['nullable'];
					}
					
					// Perform the actual validation
					if($perform_validation)
					{
						$response = static::validateSchemaModelExistsWithID($schema_values['type'], $field_value);
						if(is_string($response))
						{
							$errors[] = [
								'field_id' => $field_id,
								'message' => $response];
							continue; // issue with the model, go to the next item in the schema
						}
					}
				}
				
				// STEP 3 :  REGEX PATTERN TESTING
				if(isset($validations['pattern']))
				{
					$pattern = $validations['pattern'];
					// Perform regex pattern match
					if(!preg_match($pattern,$field_value))
					{
						$errors[] = [
							'field_id' => $field_id,
							'message' => $schema_values['title'].' '
								.TC_localize('is_formatted_incorrectly', 'is formatted incorrectly')
						];
						continue;
					}
				}
				
				// STEP 4 : METHOD CALLS
				if(isset($validations['methods']))
				{
					// Loop through each of the method validations
					foreach($validations['methods'] as $method_settings)
					{
						$method_name = $method_settings['method_name'];
						$call_mode = false;
						if(isset($method_settings['mode']))
						{
							$call_mode = $method_settings['mode'];
						}
						
						// Detect if the method is only used in certain modes, in which case we only call it if it
						// matches the mode that we're in
						if($call_mode && $call_mode != $mode)
						{
							
							continue;
						}
						
						$caller = null;
						$validation_response = null;
						
						// Find method parameters, based on the field names
						$method_parameters = [];
						foreach($method_settings['parameter_field_names'] as $parameter_field_name)
						{
							// referencing a field that doesn't exist
							if(!isset($values[$parameter_field_name]))
							{
								$errors[] = [
									'field_id' => $field_id,
									'message' => $method_name.'(), '.$parameter_field_name.
										' '.TC_localize('is_not_set','is not set')];
								continue 2; // Break out of both loops and go to the next method
							}
							else
							{
								$method_parameters[] = $values[$parameter_field_name];
							}
							
						}
						
						// Scenario where we're referencing a field name and it's class
						// Lots ot check to ensure that happens
						// Some of these trigger errors that force us to skip the rest of the validation
						if(isset($method_settings['class_field_name']))
						{
							// The schema values for the field they indicating will be calling the method
							// The `type` from that value will be the class name we're going to try
							$class_field_schema_values = $schema[$method_settings['class_field_name']];
							
							// Check that the class field name even exists
							if(!isset($values[$method_settings['class_field_name']]))
							{
								$errors[] = [
									'field_id' => $field_id,
									'message' => $field_id.' '.TC_localize('is_required', 'is required')
								];
								continue;
							}
							
							// The ID to pass in, in the value for that field provided
							// Call the validation for if the model exists, which returns the model if so
							$model = static::validateSchemaModelExistsWithID(
								$class_field_schema_values['type'],
								$values[$method_settings['class_field_name']]);
							if(is_string($model))
							{
								$errors[] = [
									'field_id' => $field_id,
									'message' => $model];
								continue;
							}
							else
							{
								$caller = $model;
							}
							
						}
						else // static call on the current model
						{
							$caller = get_called_class();
						}
						
						if(method_exists($caller,$method_name))
						{
							// Call the method on that model, passing in the parameters
							$validation_response = call_user_func_array([$caller, $method_name],
							                                            $method_parameters);
							
							
							// Check if a value of false was returned, otherwise it passed
							if($validation_response === false)
							{
								$errors[] = [
									'field_id' => $field_id,
									'message' => $method_settings['error_message']
								];
							}
						}
						else // method doesn't exist
						{
							$errors[] = [
								'field_id' => $field_id,
								'message' => '`'.$method_name.'()` '.TC_localize('does_not_exist','does not exist')
							];
						}
						
						
					}
					
				}
			}
			
			// Check if we didn't have any issues, return true
			if(count($errors) == 0)
			{
				return true; // no errors
			}
			
			return $errors;
			
		}
		
		
		/**
		 * Validates if a provided class_name and ID exist, used within validating schema models
		 * @param string $class_name
		 * @param int $id
		 * @return string|TCm_Model
		 */
		protected static function validateSchemaModelExistsWithID($class_name, $id)
		{
			// Class doesn't exist
			if(!class_exists($class_name))
			{
				return 'Class `'.$class_name.'` '.TC_localize('does_not_exist','does not exist');
			}
			
			if(is_null($id))
			{
				return 'Class  `'.$class_name.'` has ID of null, not permitted';
			}
			
			$model = $class_name::init($id);
			if((!$model instanceof TCm_Model))
			{
				return $class_name::$model_title.' with ID <em>"'.$id.'"</em> '.TC_localize('does_not_exist','does not exist');
			}
			
			return $model;
			
			
		}
		
		//////////////////////////////////////////////////////
		//
		// TSt_ModelWithAPI
		//
		// This method is included here so we can easily extend
		// it for other classes. By default we return three values
		// in a model API  call
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Method that returns the api values for the model.
		 * @return array
		 */
		public function apiValues() : array
		{
			return [
				static::$table_id_column    => intval($this->id()),
				'title'                     => $this->title(),
				'last_modified'             => $this->dateModified()
			];
			
		}
		
		//////////////////////////////////////////////////////
		//
		// VALUE METHODS
		//
		// Methods associated determining if a function has graphable methods
		// Used in data graphing and presentation
		//
		//////////////////////////////////////////////////////
		public static function graphableMethods()
		{
			// All models have a date_added field
			$methods = [];
			
			// Detect all the traits associated with the model
			foreach(static::allTraits() as $trait_name)
			{
				// Check to see if that class has a method for the schema for the trait
				$trait_method_name = 'graphableMethods_'.$trait_name; // generate the method name for the schema
				$class_name = get_called_class(); // get the class being used
				if(method_exists($class_name ,$trait_method_name ))
				{
					// Get the values
					$trait_schema_values = $class_name::$trait_method_name();
					
					if(is_array($trait_schema_values))
					{
						// Combine the values with our existing schema
						$methods = array_merge($methods, $trait_schema_values);
					}
				}
			}
			
			return $methods;

		}
		
		public static function valueMethods()
		{
			// All models have a date_added field
			$methods = [];
			
			
			// Detect all the traits associated with the model
			foreach(static::allTraits() as $trait_name)
			{
				// Check to see if that class has a method for the schema for the trait
				$trait_method_name = 'valueMethods_'.$trait_name; // generate the method name for the schema
				$class_name = get_called_class(); // get the class being used
				if(method_exists($class_name ,$trait_method_name ))
				{
					// Get the values
					$trait_schema_values = $class_name::$trait_method_name();
					
					if(is_array($trait_schema_values))
					{
						// Combine the values with our existing schema
						$methods = array_merge($methods, $trait_schema_values);
					}
				}
			}
			
			return $methods;
			
		}

		//////////////////////////////////////////////////////
		//
		// PRIMARY VIEW/CREATE/EDIT/DELETE
		//
		// Methods associated with indicating if this model
		// as a primary URL Target for viewing, editing
		// or deleting in Tungsten
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Returns the primary URL target for this model that is designated as the one used to edit this model
		 * @return false|TSm_ModuleURLTarget
		 */
		public function primaryEditURLTarget()
		{
			// Find the controller for this model
			$controller = $this->moduleForThisClass()->controller();
			
			// Find the URL target in that controller which has been defined as the primary one for editing
			$url_target = $controller->primaryEditURLTargetForModelClass($this->baseClassName());
			if($url_target)
			{
				$url_target->setModel($this);
			}
			
			return $url_target;
		}
		
		/**
		 * Returns the primary URL target for this model that is designated as the one used to delete this model
		 * @return false|TSm_ModuleURLTarget
		 */
		public function primaryDeleteURLTarget()
		{
			// Find the controller for this model
			$controller = $this->moduleForThisClass()->controller();
			
			// Find the URL target in that controller which has been defined as the primary one for editing
			$url_target = $controller->primaryDeleteURLTargetForModelClass($this->baseClassName());
			if($url_target)
			{
				$url_target->setModel($this);
			}
			
			return $url_target;
		}
		
		
		/**
		 * Returns the primary URL target for this model that is designated as the one used to view this model
		 * @return false|TSm_ModuleURLTarget
		 */
		public function primaryViewURLTarget()
		{
			// Find the controller for this model
			$controller = $this->moduleForThisClass()->controller();
			
			// Find the URL target in that controller which has been defined as the primary one for editing
			$url_target = $controller->primaryViewURLTargetForModelClass($this->baseClassName());
			if($url_target)
			{
				$url_target->setModel($this);
			}
			
			return $url_target;
		}
		
		/**
		 * Returns the primary URL target for this model that is designated as the one used to create this model.
		 * Note that this is a static method since the URL target cannot accept a model since none will exist yet.
		 * @return false|TSm_ModuleURLTarget
		 */
		public static function primaryCreateURLTarget()
		{
			// Find the controller for this model
			$controller = static::moduleForClassName()->controller();
			
			// Find the URL target in that controller which has been defined as the primary one for editing
			$url_target = $controller->primaryViewURLTargetForModelClass(static::baseClass());
			
			return $url_target;
		}
		

	}
	
	
	
	
?>