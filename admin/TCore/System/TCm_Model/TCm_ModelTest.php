<?php
	class TCm_ModelTest extends TC_ModelTestCase
	{
		/**
		 * @var TCm_Model
		*/
		protected $model;

		protected function setUp () : void
		{
			$this->model = new TCm_Model('test_model');
		}

		public function testTitle()
		{
			$this->assertEquals('Model #test_model',$this->model->title());
		
		}

		public function testModelTitlePlural()
		{
			$this->assertEquals('Models',$this->model->modelTitlePlural());
			
			
		}
		
		/**
		 */
		public function testSetPropertiesFromTable()
		{
			$this->assertTrue(true);
		}
		
		
		
	}

?>