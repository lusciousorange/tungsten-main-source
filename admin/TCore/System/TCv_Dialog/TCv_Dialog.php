<?php

/**
 * Class TCv_Dialog
 *
 * A popup-dialog that is included for popup menus that appear on the screen.
 *
 */
class TCv_Dialog extends TCv_View
{
	protected $items = array(); // [array] = The list of messages to be displayed on the console
	protected $active_page = ''; // [string] = The string identifier for the active page

	/**
	 * TCv_Dialog constructor.
	 * @param string $id
	 */
	public function __construct($id = 'TCv_Dialog')
	{	
		parent::__construct($id);
		
		$this->addClassCSSFile('TCv_Dialog', true);
		$this->addClassJSFile('TCv_Dialog');
		$this->addClassJSInit('TCv_Dialog');
		
		// Add select_carat which is loaded dynamically in forms in dialogs
		$this->addFontAwesomeClassAsSymbol('select_caret', 'fas fa-caret-down');
		
		$this->setAttribute('aria-modal','true');
		$this->setAttribute('role','dialog');
		
	}
	
	/**
	 * Returns the HTML for this view
	 * @return string
	 */
	public function html()
	{
		$content = new TCv_View();
		$content->addClass('TCv_Dialog_content');
			$status_icon = new TCv_View();
			$status_icon->addClass('status_icon');
			$content->attachView($status_icon);

			$message = new TCv_View();
			$message->addClass('message');
			$content->attachView($message);

			$buttons = new TCv_View();
			$buttons->addClass('buttons');
				$right_button = new TCv_Link();
				$right_button->addClass('right_button');
				$right_button->setURL('#');
				$right_button->addText('<span>Ok</span>');
			$right_button->setIconClassName('fa fa-check');
			$buttons->attachView($right_button);

				$left_button = new TCv_Link();
				$left_button->addClass('left_button');
				$left_button->setURL('#');
				$left_button->setIconClassName('fa fa-times');
				$left_button->addText('<span>Cancel</span>');
				$buttons->attachView($left_button);

			$content->attachView($buttons);

		$this->attachView($content);
			

		return parent::html();
	}
	
}
