class TCv_Dialog {
	element = null;
	left_button = null;
	left_button_callback = null;
	right_button = null;
	right_button_callback = null;

	content = null;
	message_container = null;
	status_icon = null;
	options = {
		'data_id'               : '',
		'confirm_action' 		: '',
		'confirm_text' 			: 'OK',
		'confirm_use_on_click' 	: false,
		'cancel_text' 			: 'Cancel',
		'message' 				: 'Message',
		'type' 					: 'fa-exclamation-triangle',
		'color' 				: '#FB7C00',
		'fullscreen' 			: false
	};

	constructor(element, options) {
		this.element = element;
		this.content = this.element.querySelector('.TCv_Dialog_content');
		this.left_button = this.element.querySelector('.left_button');
		this.right_button = this.element.querySelector('.right_button');
		this.message_container = this.element.querySelector('.message');
		this.status_icon = this.element.querySelector('.status_icon');

		// Save config options
		Object.assign(this.options, options);

		this.left_button.addEventListener('click',(e) => { this.leftClicked(e); });
		this.right_button.addEventListener('click',(e) => { this.rightClicked(e); });

		// Deal with detecting clicks on buttons flagged as .TCv_Dialog_button
		document.body.addEventListener('click', (e) => {this.handleDialogButton(e)});

	}

	/**
	 * Sets the type for this dialog
	 * @param type
	 */
	setType(type) {
		this.options.type = type;
	}

	/**
	 * Sets the type for this dialog
	 * @param color
	 */
	setColor(color) {
		this.options.color = color;
	}

	/**
	 * Sets the full screen mode for this dialog
	 * @param fullscreen
	 */
	setFullScreen(fullscreen) {
		this.options.fullscreen = fullscreen;
	}

	/**
	 * Sets the full screen mode for this dialog
	 * @param fullscreen
	 */
	setAsPopupMode() {
		this.options.fullscreen = fullscreen;
	}

	/**
	 * Sets the message for the dialog box
	 * @param {string} message
	 */
	setMessage(message) {
		this.options.message = message;
	}

	/**
	 * Sets the text for the cancel button
	 * @param {string} text
	 */
	setCancelText(text) {
		this.options.cancel_text = text;
	}



	/**
	 * Sets the action to be used with the OK button
	 * @param action
	 */
	setConfirmAction(action) {
		this.options.confirm_action = action;
	}

	/**
	 *
	 * Sets if the confirmation should use "onclick'
	 * @param {bool} use_onclick
	 */
	setConfirmUseOnClick(use_onclick) {
		this.options.confirm_use_on_click = use_onclick;
	}

	/**
	 *
	 * Sets the text for the Confirm button
	 * @param {string} text
	 */
	setConfirmText(text) {
		this.options.confirm_text = text;

	}


	//////////////////////////////////////////////////////
	//
	// CLICK HANDLERS
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the left button callback
	 * @param {function} fn
	 */
	setLeftButtonCallback(fn) {
		this.left_button_callback = fn;
	}

	/**
	 *
	 * Event handler for the left button click
	 * @param {Event} event
	 */
	leftClicked(event) {
		event.preventDefault();

		if(typeof this.left_button_callback === 'function')
		{
			this.left_button_callback(event);
		}
		this.hideDialog();
	}


	/**
	 * Sets the right button callback
	 * @param {function} fn
	 */
	setRightButtonCallback(fn) {
		this.right_button_callback = fn;
	}


	/**
	 *
	 * Event handler for the right button click
	 * @param {Event} event
	 */
	rightClicked(event) {
		event.preventDefault();

		// Detect no link and just skip

		// calling a JS action
		if(this.options.confirm_use_on_click)
		{
			eval(this.options.confirm_action);
		}
		else // normal action, redirect window
		{
			// Don't bother if we're just using hashtag, no direct necessary
			if(this.options.confirm_action != '#')
			{
				window.location = this.options.confirm_action;
			}

		}

		if(typeof this.right_button_callback === 'function')
		{
			this.right_button_callback(event);
		}

		// no matter what, hide
		this.hideDialog();

	}

	/**
	 * Hides the dialog
	 */
	hideDialog() {
		this.element.classList.remove('fullscreen');
		this.element.classList.remove('popup_mode');
		this.element.hide();

	}


	/**
	 * Shows the button, triggered when clicked
	 */
	show() {

		// Update the content
		this.content.setAttribute('id',  'dialog_' + this.options.data_id);
		this.content.setAttribute('style',  'border-color:'+this.options.color);

		// Set the message
		this.message_container.innerHTML = this.options.message;

		this.status_icon.innerHTML = '';

		// Generate the icon
		let status_inside = document.createElement('i');
		status_inside.className = 'fas ' + this.options.type;
		status_inside.setAttribute('style', 'color:'+this.options.color);
		this.status_icon.append(status_inside);

		// Set the button text
		if(this.options.confirm_text === '' || this.options.confirm_text === null)
		{
			this.right_button.classList.add('hidden');
		}
		else
		{
			this.right_button.classList.remove('hidden');
			this.right_button.querySelector('span').innerHTML = this.options.confirm_text;

		}

		if(this.options.cancel_text === '' || this.options.cancel_text === null)
		{
			this.left_button.classList.add('hidden');
		}
		else
		{
			this.left_button.classList.remove('hidden');
			this.left_button.querySelector('span').innerHTML = this.options.cancel_text;

		}

		// Handle fullscreen
		if(this.options.fullscreen)
		{
			this.element.classList.add('fullscreen');
			this.element.classList.add('popup_mode');
		}
		else
		{
			this.element.classList.remove('fullscreen');
			this.element.classList.remove('popup_mode');
		}


		this.element.show('flex');
		loopFocus(this.element);



	}



	//////////////////////////////////////////////////////
	//
	// EXTERNAL DIALOG BUTTONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Handles when an external dialog button is pressed. This is a delegation method listenign on all body clicks.
	 * @param event
	 */
	handleDialogButton(event) {
		let dialog_button = event.target.closest('.TCv_Dialog_button');
		if(dialog_button)
		{
			event.preventDefault();
			this.showTCv_DialogForButton(dialog_button);

		}



	}

	/**
	 * Loads the dialog for a provided button
	 * @param button
	 */
	showTCv_DialogForButton(button) {
		this.options.message = button.getAttribute('data-dialog-content');
		this.options.data_id = button.getAttribute('id');
		this.options.color = button.getAttribute('data-dialog-color');
		this.options.type = button.getAttribute('data-dialog-icon');

		this.options.cancel_text = 'Cancel';
		if(button.hasAttribute('data-dialog-cancel-text'))
		{
			this.options.cancel_text = button.getAttribute('data-dialog-cancel-text');
		}

		this.options.confirm_text = 'Ok';
		if(button.hasAttribute('data-dialog-ok-text'))
		{
			this.options.confirm_text = button.getAttribute('data-dialog-ok-text');
		}

		// Confirm Action
		// We still need to set the confirm action which is accessed by the event handlers for the buttons
		this.options.confirm_action = button.getAttribute('href');

		// Handle Fullscreen
		this.options.fullscreen = button.getAttribute('data-dialog-fullscreen');

		this.show();
	}


	/**
	 * Configures the dialog with a given link values
	 * @param {string} link The link being used
	 * @param {string} type The Font Awesome type of dialog
	 * @param {string} color The color of the  dialog
	 * @param {boolean} fullscreen
	 */
	configureUsingLink(link, type, color, fullscreen) {
		if(fullscreen === undefined)
		{
			fullscreen = false;
		}

		let link_el = document.querySelector(link);
		if(link_el)
		{
			this.options.data_id = link_el.getAttribute('id');
			this.setConfirmAction(link_el.getAttribute('href'));
			this.setMessage(link_el.getAttribute('data-dialog-content'));
			this.setType(type);
			this.setColor(color);
			this.setFullScreen(fullscreen);

		}

	}

}