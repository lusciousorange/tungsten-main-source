/**
 * This file extends the basic functionality of comment prototypes in order to facilitate common functionality. The goal
 * is to avoid using any external library selectors as well as larger frameworks which add overhead for what Vanilla JS can do easily.
 */


Element.prototype.show = function(display_type = 'block') {

	// Handle display modes for other elements
	if(this.tagName === 'TR')
	{
		display_type = 'table-row';
	}

	this.style.display = display_type;
}


Element.prototype.hide = function() {
	this.style.display = 'none';
}

/**
 * Toggles the visibility on the element
 * @param display_mode
 */
Element.prototype.toggle = function(display_mode = 'block') {
	let display = window.getComputedStyle(this).display;
	if (display === 'none')
	{
		this.show(display_mode);
	}
	else
	{
		this.hide();
	}
}


/* SLIDE UP */
/**
 * Adds the slideUp capability to any element with an option to set the duration in milliseconds
 * @param {Number} duration The duration of the animation in seconds
 */
Element.prototype.slideUp = function(duration= 500) {

	this.style.transitionProperty = 'height, margin, padding';
	this.style.transitionDuration = duration + 'ms';
	this.style.boxSizing = 'border-box';
	this.style.height = this.offsetHeight + 'px';
	this.offsetHeight;
	this.style.overflow = 'hidden';
	this.style.height = 0;
	this.style.paddingTop = 0;
	this.style.paddingBottom = 0;
	this.style.marginTop = 0;
	this.style.marginBottom = 0;
	window.setTimeout( () => {
		this.style.display = 'none';
		this.style.removeProperty('height');
		this.style.removeProperty('padding-top');
		this.style.removeProperty('padding-bottom');
		this.style.removeProperty('margin-top');
		this.style.removeProperty('margin-bottom');
		this.style.removeProperty('overflow');
		this.style.removeProperty('transition-duration');
		this.style.removeProperty('transition-property');
		//alert("!");
	}, duration);
}

/* SLIDE DOWN */
/**
 * Adds the slide down functionality to every element
 * @param {Number} duration
 * @param {String} display_mode
 */
Element.prototype.slideDown = function(duration= 500, display_mode = 'block') {

	//this.style.removeProperty('display');
	let display = window.getComputedStyle(this).display;
	if (display !== 'none')
	{
		return; // do nothing, already showing
	}
	else
	{
		display = display_mode;
	}

	// Handle display modes for other elements
	if(this.tagName === 'TR')
	{
		display = 'table-row';
	}

	this.style.display = display;
	let height = this.offsetHeight;
	this.style.overflow = 'hidden';
	this.style.height = 0;
	this.style.paddingTop = 0;
	this.style.paddingBottom = 0;
	this.style.marginTop = 0;
	this.style.marginBottom = 0;
	this.offsetHeight;
	this.style.boxSizing = 'border-box';
	this.style.transitionProperty = "height, margin, padding";
	this.style.transitionDuration = duration + 'ms';
	this.style.height = height + 'px';
	this.style.removeProperty('padding-top');
	this.style.removeProperty('padding-bottom');
	this.style.removeProperty('margin-top');
	this.style.removeProperty('margin-bottom');
	window.setTimeout( () => {
		this.style.removeProperty('height');
		this.style.removeProperty('overflow');
		this.style.removeProperty('transition-duration');
		this.style.removeProperty('transition-property');
	}, duration);
}

/**
 * Slide toggle functionality based on detecting the current display mode
 * @param duration
 * @param display_mode
 */
Element.prototype.slideToggle = function(duration= 500, display_mode = 'block') {
	let display = window.getComputedStyle(this).display;
	if (display === 'none')
	{
		this.slideDown(duration,display_mode);
	}
	else
	{
		this.slideUp(duration);
	}
}

/**
 * A stateless keyboard utility to -
 * - Trap focus,
 * - Focus the correct Element
 *
 * This function is called in the event handler of the item that wants to set the focus
 *
 * @param {HTMLElement} popupContainer The container element where the popup exists.
 * @param {HTMLElement|boolean} focusElement The element that should get the focus
 * @return {Function} Function. The cleanup function. To undo everything done for handling A11Y
 *
 * @see https://medium.com/@im_rahul/focus-trapping-looping-b3ee658e5177
 *
 * Usage: For any element that triggers a popup, you add a click handler to that element with this function as the callback.
 */
function loopFocus(popupContainer, focusElement= false ) {

	// The selector to find all the elements that are focusable
	const FOCUSABLE_ELEMENT_SELECTORS = 'a[href], area[href], input:not([disabled]):not([type="hidden"]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, [tabindex="0"], [contenteditable]';

	if (!popupContainer) {
		throw new Error('Could not initialize focus-trapping - Element Missing');
	}

	const focusableElements = popupContainer.querySelectorAll(FOCUSABLE_ELEMENT_SELECTORS);
	let keyboardHandler;

	//There can be containers without any focusable element
	if (focusableElements.length > 0) {
		const firstFocusableEl = focusableElements[0],
			lastFocusableEl = focusableElements[focusableElements.length - 1],
			elementToFocus = focusElement ? focusElement : firstFocusableEl;
		elementToFocus.focus();

		keyboardHandler = function keyboardHandler(e) {
			if (e.keyCode === 9) { // DETECT TAB KEY
				//Rotate Focus
				if (e.shiftKey && document.activeElement === firstFocusableEl) {
					e.preventDefault();
					lastFocusableEl.focus();
				} else if (!e.shiftKey && document.activeElement === lastFocusableEl) {
					e.preventDefault();
					firstFocusableEl.focus();
				}
			}
		};
		popupContainer.addEventListener('keydown', keyboardHandler);
	}

	//The cleanup function. Put future cleanup tasks inside this.
	return function cleanUp() {


		if (keyboardHandler) {
			popupContainer.removeEventListener('keydown', keyboardHandler);
		}
	};
}

/**
 * Processes a list of CSS or JS links that are checked to see if they exist in the <head> or not. If not they are added.
 * This also detects runtime values for css and js and ensure they get called each time
 * @param {Object} links
 */
function processAsyncJSLinks(links) {

	if(typeof(links) === 'string')
	{
		// We need to find all the script tags and ensure they are valid
		links = links.split('</script>');
		let new_links = {};
		links.forEach( (script, index) =>
		{
			script = script.trim();

			// We have a non-empty value
			if(script != '')
			{
				// non-numerical indicies
				new_links['link_'+index] = script+'</script>';
			}

		});

		links = new_links;

	}

	if(typeof(links) === 'object')
	{
		// https://usefulangle.com/post/343/javascript-load-multiple-script-by-order
		let js_wait_for_load_script = '';
		let promises = [];
		// Loop through them all
		for(let tag_id in links)
		{
			let script_value = links[tag_id].trim();

			// Find the wait for load JS, and pull it out separately
			// Runs after everything loads
			if(tag_id === 'js_wait_for_load')
			{
				js_wait_for_load_script += script_value.replace(/(<([^>]+)>)/gi, "").trim();
			}
			else // script file to load
			{
				// Determine if it's loading a file or script commands

				// It's a file
				if(script_value.indexOf(" src=") > 0)
				{
					promises.push(loadScript(tag_id, script_value));
				}
				else // it's JS commands, have them wait
				{
					js_wait_for_load_script += "\n\t" + script_value.replace(/(<([^>]+)>)/gi, "").trim();
				}

			}

		}

		// Load them all as promises
		// THEN load the wait_for_load js since that runs after all of them
		Promise.all(promises)
			.then(function() {
				let script = document.createElement('script');
				script.innerHTML = js_wait_for_load_script;
				script.async = false; // need to load in order to avoid issues
				document.head.append(script);

			}).catch(function(script) {
			console.log(script + ' failed to load');
		});

	}



}

/**
 * Loads a script using a promise, used to catch multiple loads
 * @param {string} tag_id The ID for the tag, which is used to avoid duplicate loads
 * @param {string} script_string The URL for this script
 * @returns {Promise<unknown>}
 */
function loadScript(tag_id, script_string) {
	return new Promise(function(resolve, reject)
	{
		// Get the tag
		let container = document.createElement('div');
		container.innerHTML = script_string;
		let tag = container.firstElementChild;
		let tag_id = tag.getAttribute('id');

		// Determine if this item is already in the head
		// We have a non-blank ID
		if(tag_id !== null)
		{
			// We have found an element in the head with that ID
			let head_element = document.head.querySelector('#'+ tag_id);
			if(head_element)
			{
				resolve('tag_id');
				return;
			}

		}

		// At this point, we have a script tag to load

		let src = tag.getAttribute('src');

		// JS scripts must exist in a fresh script tag to load
		// Create a new script tag, based on these values
		let script = document.createElement('script');

		if(tag_id !== null) {
			script.setAttribute('id', tag.getAttribute('id'));
		}
		if(tag.getAttribute('src') != null) {
			script.setAttribute('src', src);
		}

		script.async = false;
		script.innerHTML = tag.innerHTML;// copy the inside, might have commands
		script.addEventListener('load', function () {
			resolve(src);
		});
		// no error listener, just keep pushing through
		document.head.appendChild(script);


	});
}



/**
 * Processes a list of CSS or JS links that are checked to see if they exist in the <head> or not. If not they are added.
 * This also detects runtime values for css and js and ensure they get called each time
 * @param {Object|string} links
 */
function processAsyncCSSLinks(links) {

	// Pre-process if we're given a string
	if(typeof(links) === 'string')
	{
		let container = document.createElement('div');
		container.innerHTML = links;

		// Convert strings into link values
		links = {};
		for (let index in container.children)
		{
			let element = container.children[index];
			if(element instanceof HTMLLinkElement)
			{
				links[element.getAttribute('id')] = element;
			}

		}

	}

	if(typeof(links) === 'object')
	{
		// Loop through them all
		for(let tag_id in links)
		{
			let head_element = document.head.querySelector('#'+ tag_id);

			// If the item isn't found OR it's the runtime value, add it to the head
			if(!head_element || tag_id === 'runtime')
			{
				if(links[tag_id] instanceof HTMLLinkElement)
				{
					document.head.append(links[tag_id]);
				}
				else
				{
					document.head.insertAdjacentHTML('beforeEnd', links[tag_id]);
				}

			}
		}

	}




}

/**
 * Converts HTML into a collection of HTML elements. This returns a collection because we don't know what the HTML lools like.
 * @param {string} html
 * @returns {HTMLCollection}
 */
function convertHTMLToDOMElements(html) {
	let div = document.createElement('div');
	div.innerHTML = html;

	return div.children;

}
