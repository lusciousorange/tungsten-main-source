<?php
/**
 * The top level class for all other classes.
 *
 * This class contains the most low-level functions for classes written in Tungsten.
 *
 */
class TCu_Item
{
	use TSt_ConsoleAccess, TCt_FactoryInit;
	
	protected $id; // [string] = The unique string identifier for this item. 
	
	protected bool $exists = true; // [bool] = Indicates if the item actually exists
	protected array $localized_field_values = [];
	
	public static $table_id_column = ''; // [string] = The id column for this class. if left as an empty string, attempts to instantiate a class using a value array will fail
	public static $table_name = ''; // [string] = The table for this class.
	
	public static bool $disable_tungsten_caching = false; // turn this value on in a subclass to ensure it never gets cached. This may lead to more DB queries.
	

	/**
	 * The primary ID for this item
	 *
	 * @param int|string $id The main id for this item.
	 */
	public function __construct($id)
	{
		// Determine if we have a singleton, in which case the ID should be the singleton ID
		if(static::isSingleton())
		{
			$id = static::initSingletonIdentifier();
		}
		
		$this->setID($id);

	}
	
	
	/**
	 * Hidden test function for debugging purposes that detects when items are deconstructed.
	 */
//		public function __destruct()
//		{
//			if(get_called_class() == 'TSm_ConsoleItem')
//			{
//				return;
//			}
//			print '<br />- DESTRUCT '.$this->contentCode();
//		}
	
	
	/**
	 * Returns the ID for this model
	 *
	 * @return int|string The id for this model
	 */
	public function id()
	{
		return $this->id;
	}
	
	/**
	 * Sets the ID for this model. This will override the existing ID if set.
	 * @param bool|int|string $id The unique ID for the item.
	 */
	public function setID($id)
	{
		if($id === false || is_null($id))
		{
			$this->id = null;
		}
		else
		{
			// clean the IDs
			$this->id = htmlspecialchars($id);
			$this->exists = true;

		}
			
		
	}

	/**
	 * The content code for this class which consists of the class name combined with the item's ID.
	 * @return string
	 */
	public function contentCode()
	{
		return static::baseClassName().'--'.$this->id();
	}
	
	/**
	 * Accepts a html string and finds all the relevant instances of /{{pageview: codes and replaces them with the
	 * path to the actual item in question.
	 */
	protected function replacePageViewCodesWithURL($html)
	{
		// Detect pageview codes
		if(strpos($html,'/{{pageview') !== false)
		{
			$pattern = '/{{pageview:(.*?)}}/';
			preg_match_all($pattern, $html, $matches);
			
			// only bother there is at least one
			if($matches && sizeof($matches[1]) > 0)
			{
				// loop through each one and parse/replace
				foreach($matches[1] as $match)
				{
					$replace = '#';
					if($model = TC_initClassWithContentCode($match))
					{
						$replace = $model->pageViewURLPath();
					}
					$html = str_ireplace("/{{pageview:" . $match  . "}}", $replace, $html);
					
				}
			}
		}
		
		return $html;
	}
	
	
	/**
	 * Extracts the model from a given page view code. Those codes needs to start with /{{pageview since there is a
	 * content code for a model inside of it.
	 */
	protected function modelForPageViewCode(string $page_view_code) : ?TCm_Model
	{
		$page_view_code = trim($page_view_code);
		// Detect if it starts with a pageview code
		if(strpos($page_view_code,'/{{pageview') === 0)
		{
			$content_code = str_ireplace(['/{{pageview:','}}'],'',$page_view_code);
			
			if($model = TC_initClassWithContentCode($content_code))
			{
				return $model;
			}
			
		}
		
		return null;
	}

	//////////////////////////////////////////////////////
	//
	// URL/DOMAIN FUNCTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Cleans a string for use in a URL.
	 *
	 * This method will accept a string and clean out any special characters, replacing them with a dash or empty
	 * string as necessary.
	 *
	 * @param string $url string to be cleaned for the URL
	 * @return string
	 */
	public function cleanForURL($url)
	{
		// Remove any tags that might exist
		$url = strip_tags($url);
		
		// French quotes, something incredible weird and special here
		$url = str_replace(['«','»'],"", $url);
		
		// thank you to smashingmagazine
		//Convert accented characters, and remove parentheses and apostrophes
		$from = explode (',', "–,’,',À,Á,Â,Ã,Ä,Å,Æ,Ç,È,É,Ê,Ë,Ì,Í,Î,Ï,Ð,Ñ,Ò,Ó,Ô,Õ,Ö,Ø,Ù,Ú,Û,Ü,Ý,Þ,ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,(,),[,],'");
		$to = explode (',',   '-,,,A,A,A,A,A,A,AE,C,E,E,E,E,I,I,I,I,D,N,O,O,O,O,O,O,U,U,U,U,Y,P,c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,,,,,,');
		
		// Replace all the non-ASCII characters with an empty string
		$url = preg_replace('/[^\00-\255]+/u', '', $url);
		
		//Do the replacements, and convert all other non-alphanumeric characters to dashes
		$url = preg_replace ('~[^\w\d]+~', '-', str_replace ($from, $to, trim ($url)));
		
		//Remove a dashes(-) at the beginning or end, then make it lowercase
		return strtolower(preg_replace ('/^-/', '', preg_replace ('/-$/', '', $url)));
	}

	/**
	 * Returns the URL sections for the current script as an array.
	 * @return array
	 */
	public function urlSections()
	{
		$sections = explode("/",$_SERVER['PHP_SELF']);
		return $sections;	
	}
	
	/**
	 * Returns the name of a single URL section
	 * @param int $num The index of within the sections, starting with 0 being the domain name
	 * @return mixed
	 */
	public function urlSection($num)
	{
		$sections = $this->urlSections();
		return @$sections[$num];
	}
	
	/**
	 * Returns the protocal being used by the server.
	 *
	 * @return string
	 */
	public function serverProtocol()
	{
		return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	}
	
	/**
	 * Returns the full URL for this page. It does not include parameter variables
	 * @return string
	 */
	public function pageURL()
	{
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . 	"://" .
			$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		return $url;

	}
	
	/**
	 * Returns the domain name with the protocol
	 * @return string
	 */
	public function fullDomainName()
	{
		return $this->serverProtocol().$_SERVER['HTTP_HOST'];
	}
	
	/**
	 * Returns if this model is able to be used for page title. This overridden to control which models show up in
	 * page titles for the website.
	 * @return bool
	 */
	public function isUsableAsPageTitle()
	{
		return true;
	}
	
	/**
	 * A function to reliably generate titles for a url which keeps the URL intact but makes the visible URL less
	 * cumbersome in some cases.
	 * @param string $url The url that is being processed
	 * @return string the user-friendly version of that url
	 */
	protected function userFriendlyWebpageTitle(string $url)
	{
		$parse = parse_url($url);
		if(isset($parse['host']))
		{
			$host = $parse['host'];
			$host = str_replace('www.','', $host);
			if(strpos($host,'google.') !== false)
			{
				return TC_localize('view_website','View website');
			}
			return $host;
		}
		
		return $url;
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// STRING METHODS
	//
	//////////////////////////////////////////////////////


	/**
	 * Formats a string for the North American currency format.
	 *
	 * @param float $value The value to be formatted
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string
	 */
	public function formatCurrency($value, $use_sig_digits = false)
	{
		$num_digits = 2;
		if($use_sig_digits)
		{
			$num_digits = 3;
			
		}
		
		return number_format(round($value,$num_digits), $num_digits, '.','');
	}
	
	/**
	 * Formats a string to a given date format.
	 *
	 * @param string $date The date string to be formatted
	 * @param string|bool $format (Optional) Default value is false. The format to be used. The value can be any valid format for the php `date()` method
	 * or two specific values of "full" which corresponds to "F j, Y" and "short" which corresponds to "M j, Y".
	 *
	 * @return string
	 *
	 * @deprecated This method is unnecessarily narrow and should not be necessary.
	 *
	 * @see http://php.net/manual/en/function.date.php
	 * @see http://php.net/manual/en/function.strtotime.php
	 *
	 */
	public function formatDate($date, $format = false)
	{
		if($format == false)
		{
			return $date;
		}
		elseif($format == 'full')
		{
			$format = 'F j, Y';
			return date($format, strtotime($date));	
		}
		elseif($format == 'short')
		{
			$format = 'M j, Y';
			return date($format, strtotime($date));	
		}

		return date($format, strtotime($date));
	}
	
	/**
	 * Formats a string with a default time.
	 *
	 * @param string $time The time string that is to be formatted.
	 * @param string|bool $format The format to be used
	 *
	 * @return string
	 *
	 * @deprecated This method is unnecessarily narrow and should not be necessary.
	 *
	 * @see TCu_Item::formatDateLocale()
	 * @see http://php.net/manual/en/function.date.php
	 * @see http://php.net/manual/en/function.strtotime.php
	 *
	 */
	public function formatTime($time, $format)
	{
		if($format == false)
		{
			return $time;
		}
		elseif($format == 'hour_minute_12')
		{
			$format = 'g:ia';
			return date($format, strtotime($time));	
		}
		elseif($format == 'hour_minute_24')
		{
			$format = 'H:i';
			return date($format, strtotime($time));	
		}

		return date($format, strtotime($time));
	}

	/**
	 * Converts all the instances of a `new line` into paragraph tags.
	 *
	 * @param string $string The string to be converted
	 * @param bool $xml (Optional) Defaults value is `true`. Indicates if the XML format should be used
	 *
	 * @return string
	 */
	function nl2p($string, $xml = true)
	{

		$string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);
		
		$string = '<p>'.preg_replace(
		 	   array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
		 	   array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
		
		 	   trim($string)).'</p>';

		return $string;
	}

	/**
	 * Formats a date string to a specific format that respects the locale settings for the site. This method is
	 * designed to let you provide DateTime values and formats and it converts them to local values internally.
	 *
	 * @param string|DateTime $date_string The date string to be formatted
	 * @param string $format The format to be used which follows the format for `strftime()`.
	 *
	 * @return string
	 *
	 * @see http://php.net/manual/en/function.strftime.php
	 */
	public function formatDateLocale($date_string, $format)
	{
		// Handle option of providing a DateTime object
		if($date_string instanceof DateTime)
		{
			$date_string = $date_string->format('Y-m-d H:i:s');
		}
		
		// No format characters found
		if(strpos($format,'%') === false)
		{
			$format = $this->convertDateTimeFormatToLocaleFormat($format);
		}
		return @utf8_encode(strftime($format, strtotime($date_string)));
	}
	
	/**
	 * Internal function to convert DateTime format strings to locale format strings
	 * @param string $format
	 * @return string
	 */
	public function convertDateTimeFormatToLocaleFormat($format)
	{
		$replacements = array( 'd' => '%d', 'D' => '%a', 'j' => '%e', 'l' => '%A', 'N' => '%u', 'S' => '', 'w' => '%w',
			'z' => '', 'F' => '%B', 'm' => '%m', 'M' => '%b', 'n' => '%m', 'Y' => '%Y', 'y' => '%y', 'a' => '%P',
			'A' => '%p', 'g' => '%l', 'G' => '%k', 'h' => '%I', 'H' => '%H', 'i' => '%M', 's' => '%S'
			
		);
		$search  = array_keys($replacements);
		$replace = array_values($replacements);
		
		return str_replace($search, $replace, $format);
	}
	
	/**
	 * Tints a color with a related shade lighter or darker by percentage.
	 *
	 * @param string $color A 6 character hex color.
	 * @param int $percent A number from -100 to 100 which indicates how the color is shaded. A value of -100 will turn
	 * it to pure white. A value of 100 will produce pure black. Ranges between will lighten/darken the color
	 * appropriately.
	 * @return string The 6-character hex color
	 */
	public function shadeHexColor($color, $percent)
	{
		$RGB = str_split(str_replace('#','', $color), 2);
		
		$full_color = '';
		foreach($RGB as $color_part)
		{
			$color_part = hexdec($color_part);
			if($percent > 0)
			{
				$color_part = round($color_part * (100 - $percent) / 100);
			}
			else
			{
				$color_part = round($color_part + (255-$color_part) * $percent*-1/100);
			}
			$color_part = round($color_part < 255 ? $color_part : 255);
			$full_color .= str_pad(dechex($color_part), 2, '0', STR_PAD_LEFT);
		}
		
		return $full_color;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	// These methods are included at the core functionality
	// to support the optional implementation of localization
	// in future classes.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns a localized property for the model.
	 *
	 * This method is used in conjunction with a localization module that will support multiple languages.
	 * By default this method will return the proper value itself.
	 *
	 * @param string $field The name of the property to be returned for the class.
	 * @param string|bool $language (Optional) Default value is false. The 2-digit language code to be used.
	 *
	 * @return mixed
	 */
	public function localizeProperty(string $field, $language = false)
	{
		// ENGLISH VALUE
		$text = false;
		if(TC_getConfig('use_localization') )
		{
			// Determine the language
			$module = TMm_LocalizationModule::init();
			if($language === false)
			{
				$language = $module->loadedLanguage();
			}
			
			// DEFAULT LANGUAGE SHORTCUT
			// Avoid excessive calls, also works for admin
			if($language == $module->defaultLanguage() || !$language)
			{
				return @$this->$field;
			}
			
			// CACHING
			// Check if we have this value cached
			if(isset($this->localized_field_values[$language][$field]))
			{
				return $this->localized_field_values[$language][$field];
			}
			
			// OTHER LANGUAGE
			$lang_field_name = $field.'_'.$language;
			// language value exists
			if($this->$lang_field_name != '')
			{
				$text = $this->$lang_field_name;
				$this->localized_field_values[$language][$field] = $text;
				return $text;
			}
			else // wrap in warning text
			{
				$text = TMm_LocalizationModule::handleMissingString($field, $this->$field);
				$this->localized_field_values[$language][$field] = $text;
				return $text;
			}
			
			
			
		}
		
		// NO LOCALIZATION, use English
		return $this->$field;
	}

	//////////////////////////////////////////////////////
	//
	// EXISTANCE
	//
	// Basic methods that deal with if the item exists
	// or what happens when it is destroyed
	//
	//////////////////////////////////////////////////////


	/**
	 * Turns off the property `exists` so that any instance will return false.
	 *
	 * @return bool
	 */
	public function destroy()
	{
		$this->exists = false;
		return false;
	}
	
	/**
	 * Returns if this model still exists.
	 * @return bool
	 */
	public function exists()
	{
		return $this->exists;
	}
	
	/**
	 * Returns the folder for the class being called starting from the site root.
	 *
	 * @param string|bool $class_name (Optional) Default value is false. If false, it will use the class provided.
	 * Otherwise it will use the class name provided.
	 * @param bool $use_html_path_separator Indicates if we should use the path separator instead which is always /
	 * @return string
	 */
	public function classFolderFromRoot($class_name = false, bool $use_html_path_separator = false)
	{
		// No classname, use the called class
		if($class_name === false)
		{
			$class_name = get_called_class();
		}
		
		// Same directory keys, save memory
		if(DIRECTORY_SEPARATOR == '/')
		{
			$mem_key = $class_name . '_folder_root';
		}
		else // windows or something else, differentiate
		{
			$mem_key = $class_name . '_folder_root_' . ($use_html_path_separator ? 'html' : 'directory');
		}
		
		$mem_value = TC_Memcached::get($mem_key);
		
		// Found in memcached, get out
		if(is_string($mem_value))
		{
			return $mem_value;
		}
		
		// A classname is provided. Something other than "this"
		if($class_name)
		{
			$reflection = new ReflectionClass($class_name);
			$path = $reflection->getFileName();
		}
		
		$admin_position = strpos($path,DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR);
		
		// parse until we find the "admin" folder.
		$path = substr($path, $admin_position);
		$path_pieces = explode(DIRECTORY_SEPARATOR, $path);
		array_pop($path_pieces);
		
		if($use_html_path_separator)
		{
			$path = implode('/', $path_pieces);
		}
		else
		{
			$path = implode(DIRECTORY_SEPARATOR, $path_pieces);
		}
		
		TC_Memcached::set($mem_key, $path);
		return $path;
		
	}
	
	/**
	 * Returns the Tungsten module for this class.
	 *
	 * @param string|bool $class_name (Optional) Default value false. The class name to be found. False means it will
	 * find the module for the called class.
	 *
	 * @return bool|TSm_Module
	 */
	public function moduleForThisClass($class_name = false)
	{
		return self::moduleForClassName($class_name);

	}

	/**
	 * Returns the Tungsten module for this class.
	 *
	 * @param class|bool $class_name  Default value false. The class name to be found. False means it will
	 * find the module for the called class.
	 *
	 * @return bool|TSm_Module
	 */
	public static function moduleForClassName($class_name = false)
	{
		if($class_name === false)
		{
			$class_name = get_called_class();
		}

		return TC_moduleForClass($class_name);

	}


	/**
	 * A method that can be overloaded by parent classes to which can be useful for saving on loading time.
	 *
	 * @return array An array of TCu_Items
	 */
	public function modelsToLoadWhenActive()
	{
		return array();
	}


	/**
	 * Returns if the view is being loaded in the admin
	 *
	 * @return bool
	 * @deprecated use TC_isTungstenView() instead
	 */
	public function isLoadingInTungstenView()
	{
		return TC_isTungstenView();
	}
	
	/**
	 * Returns the name of the class in the chain of parent classes that originally extended the TCm_Model class.
	 *
	 * @return string The name of the base class
	 */
	public function baseClassName() : string
	{
		$base_class = get_class($this);
		
		// Is their own base class
		if(in_array($base_class, static::$base_class_names))
		{
			return $base_class;
		}
		
		$found = false;
		while(!$found)
		{
			$parent_class = get_parent_class($base_class);
			if(in_array($parent_class, static::$base_class_names))
			{
				$found = true;
			}
			else // keep looking
			{
				// Abstract classes are the end of the chain
				$reflection = new ReflectionClass($parent_class);
				if( $reflection->isAbstract())
				{
					$found = true;
				}
				else
				{
					$base_class = $parent_class;
				}
				
			}
		}
		return $base_class;
	}
	
	/**
	 * Returns the name of the class in the chain of parent classes that originally extended the TCm_Model class.
	 *
	 * @return class-string<TCu_Item> The name of the base class
	 */
	public static function baseClass() : string
	{
		$base_class = get_called_class();
		
		// Is their own base class
		if(in_array($base_class, static::$base_class_names))
		{
			return $base_class;
		}
		
		$found = false;
		while(!$found)
		{
			$parent_class = get_parent_class($base_class);
			if(in_array($parent_class, static::$base_class_names))
			{
				$found = true;
			}
			else // keep looking
			{
				// Abstract classes are the end of the chain
				$reflection = new ReflectionClass($parent_class);
				if( $reflection->isAbstract())
				{
					$found = true;
				}
				else
				{
					$base_class = $parent_class;
				}
				
			}
		}
		return $base_class;
	}
	//////////////////////////////////////////////////////
	//
	// MEMORY USAGE
	//
	// Utility methods for memory usage
	//
	//////////////////////////////////////////////////////


	/**
	 * Convert an integer into the appropriate amount of bytes with a proper unit suffix
	 * @param int $bytes = The number of bytes
	 * @return string The resulting value such as "64kb"
	 */
	public function bytesToUnits($bytes)
	{
		if($bytes != 0)
		{
			$unit=array('B','KB','MB','GB','TB','PB');
			$i = floor(log($bytes,1024));
			return @round($bytes/pow(1024, $i),2).' '.$unit[$i];
		}
		
		return '0B';
		
	}
	
	/**
	 * Returns the current memory usage in a human-readable format.
	 *
	 * @return string
	 */
	public static function currentMemoryUsage() : string
	{
		$bytes = memory_get_usage(TC_getConfig('console_show_real_memory'));
		
		$unit = ['B','KB','MB','GB','TB','PB'];
		$i = floor(log($bytes,1024));
		return round($bytes/pow(1024, $i),2).' '.$unit[$i];
	}

	/**
	 * Duplicate of `bytesToUnits()`. Passthrough to that method.
	 *
	 * @see TCu_Item::bytesToUnits()
	 *
	 * @deprecated Please use `bytesToUnits()`
	 * @param int $bytes The number of bytes
	 * @param bool $return_bytes
	 * @return string
	 */
	public function formatBytes($bytes, $return_bytes = false)
	{
		return $this->bytesToUnits($bytes);
	}

	/**
	 * Prints the backtrace
	 *
	 * @deprecated Please use TC_backtrace();
	 *
	 * @see TC_backtrace()
	 */
	public function printBacktrace()
	{
		print TC_backtrace();

	}	
	
	/**
	 * Convenience method that return the reflection class for the called class.
	 *
	 * @return ReflectionClass
	 */
	protected function reflection()
	{
		return new ReflectionClass($this);
    }
	
	
	/**
	 * Returns the list of all the traits that are applied to this class, even parents
	 * @return string[]
	 */
	protected static function allTraits()
	{
		$traits = [];
		$class = new ReflectionClass(get_called_class());
		
		// Get all the parent classes in an array
		$all_classes = [$class]; // start with the main reflection
		
		while ($parent = $class->getParentClass())
		{
			$all_classes[] = $parent;
			$class = $parent;
		}
		
		// Loop through class reflections, combine traits into an array
		foreach($all_classes as $class_reflection)
		{
			foreach($class_reflection->getTraitNames() as $trait_name)
			{
				$traits[$trait_name] = $trait_name;
			}
		}
		
		return $traits;
	}
	
	/**
	 * Returns if this class has the trait indicated
	 * @param string $trait_name
	 * @return bool
	 */
	protected static function hasTrait($trait_name)
	{
		foreach(static::allTraits() as $trait)
		{
			if($trait == $trait_name)
			{
				return true;
			}
		}
		
		return false;
	}

}
?>