<?php
use PHPUnit\Framework\TestCase;

class TCu_ItemTest extends TestCase
{
	/**
	 * @var TCm_Model
	 */
	protected $model;
	
	protected function setUp () : void
	{
		$this->model = new TCu_Item('test');
	}
	
	public function testContentCode()
	{
		$this->model->setID('123');
		$this->assertEquals('TCu_Item--123', $this->model->contentCode());
	}
	
	public function testCleanForURL()
	{
		$url = 'RU!@#$%^&*()_{}|:"<>?[]\;,./-=\'';
		$this->assertEquals('ru-_', $this->model->cleanForURL($url));
	}
	
	public function testFormatCurrency()
	{
		$this->assertEquals('12.00', $this->model->formatCurrency(12));
		$this->assertEquals('12.80', $this->model->formatCurrency(12.8));
		$this->assertEquals('12.86', $this->model->formatCurrency(12.8643));
		
	}
	
	public function testNl2p()
	{
		$string = "   Line 1\n\n<p>Line 2\n</p><br>Line 3<br />";
		$expected = "<p>Line 1</p>\n<p>Line 2<br />Line 3</p>";
		$this->assertEquals($expected, $this->model->nl2p($string));
	}
	
	public function testClassFolderFromRoot()
	{
		// Testing no parameter
		$this->assertEquals('/admin/TCore/System/TCu_Item',$this->model->classFolderFromRoot());
		
		// Testing when a class is passed, using different class to avoid false-positive
		$this->assertEquals('/admin/TCore/System/TCm_Model',$this->model->classFolderFromRoot('TCm_Model'));
	}
	
	public function testModuleForThisClass()
	{
		$module = $this->model->moduleForThisClass();
		$this->assertIsObject($module);
		$this->assertEquals('TCore', $module->folder());
		
	}
	
	public function testBytesToUnits()
	{
		$this->assertEquals('1023 B', $this->model->bytesToUnits(1023));
		$this->assertEquals('1 KB', $this->model->bytesToUnits(1025));
		$this->assertEquals('1024 KB', $this->model->bytesToUnits(1024*1024-1));
		$this->assertEquals('1 MB', $this->model->bytesToUnits(1024*1024+1));
	}
}

?>