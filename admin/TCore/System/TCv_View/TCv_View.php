<?php
/**
 * The main class that all views should extend.
 *
 * This class handles a wide range of functionality for views in Tungsten admin as well as on the public website
 * that is built on Tungsten. By default all TCv_Views represent a `<div>` tag to be output. A view can have CSS or JS
 * added to it as well as other views that stack within it.
 *
 * All TCv_Views are built by adding content (text or other TCv_Views) to the model which is then eventually rendered
 * by the website that is showing it.
 *
 */
class TCv_View extends TCu_Item
{
	// CSS
	protected $tag_attribute_id = false; // [string] = The ID used for the display of this item.
	protected array $css_files = array(); // [array] = The array of all CSS files that should be loaded for this content item
	protected array $css_runtime_calls = array(); // [array] = The list of runtime css calls
	protected array $js_files = array(); // [array] = The array of all JS files that should be loaded for this content item
	protected array $js_lines = array(); // [array] = The array of all JS inits that should be loaded for this content item
	protected array $js_lines_wait_for_load = array(); // [array] Array of JS lines that are executed after loading
	protected array $classes = array(); // [array] = The list of classes to be assigned to this content item
	protected array $attached_views = array(); // [array] = The list of attached views to this view
	protected array $views_to_attach = array(); // [array] = The list of attached views to this view
	protected bool $show_id = true; // [bool] = Indicates if the id for this view should be shown
	protected array $parent_classes = array(); // [array] = The list of parent classes
	protected $parent_view = false; // [TCv_View] = The view that is the parent of this view
	protected bool $has_been_attached = false; // [bool] = Indicates if this view has been attached to another view
	protected bool $render_called = false;
	protected array $font_awesome_symbols = array();
	protected array $svg_icon_symbols = array();
	protected bool $collapse_attached_views_to_html = true; // [bool] = Indicates if view should be collapsed to HTML when attached to a parent view
	protected $embedded_css = false;
	
	protected bool $html_rendered = false; // [bool] = Indicates if the HTML for this view is already rendered. False or the HTML itself stored in this variable
	
	// TAG values
	protected string $tag = 'div'; // [string][div] = The tag used for the content block
	protected array $attributes = array(); // [array] = The list of attributes that are associated with the tag
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCv_View');
	
	
	/**
	 * @var array The value associated with the init of a js class. It can be
	 */
	protected $js_init_values = [];
	
	
	protected $meta_tags = array();
	
	public static $view_is_a_pages_container = false; 
	public static $view_is_pages_addable = true; 
	public static $icon_code = 'th'; 

	public static $single_tags =[
		'img', 'br', 'input', 'meta','xhtml:link'
	];
	/**
	 * Constructor
	 *
	 * @param mixed (Optional) Default is false;$id The id for this view. This can be a string or a model that is meant to be
	 * loaded into this view.
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		foreach($this->parentClasses() as $parent)
		{
			if($parent != 'TCv_View' && $parent != 'TCv_View' && $parent != 'TCu_Item')
			{
				$this->addClass($parent);	
			}
		}
		
		if(get_class($this) != 'TCv_View')
		{
			$this->addClass(get_class($this));
		}
		
		
	}
	
	/**
	 * A recursive method that returns all an array of all the class names that are parents to this class.
	 *
	 * @param null $class (Optional) Default value is null. The class that is wanted. It is not recommended to pass a class
	 * into this method but it is necessary to allow for recursion.
	 * @param array $list The existing list of classes.
	 * @return array An array of strings
	 *
	 * @uses get_parent_class()
	 */
	protected function parentClasses($class = null, $list = array())
	{
		$class = $class ? $class : $this;
		if($parent = get_parent_class($class))
		{
			$list[] = $parent;
			$list = self::parentClasses($parent, $list);
		}
		
		return $list;
	}

	//////////////////////////////////////////////////////
	//
	// ATTRIBUTES
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets the ID for this model. This overloads the existing ID if set.
	 *
	 * @param bool|int|string|TCm_Model $id The unique ID for the item. This can also be a TCm_Model that is being passed into the view.
	 *
	 * @see TCu_Item::setID()
	 */
	public function setID($id)
	{
		if($id instanceof TCm_Model)
		{
			$id = $id->id();
		}
		parent::setID($id);



	}

	/**
	 * Sets an attribute on the tag commonly seen in html as `<tag $name="$value">.
	 *
	 * @param string $name The name of the attribute in HTML to be added. If the $name equals "class" then it will
	 * add the $value as a classname instead to ensure consistency.
	 * @param string $value The value for the attribute.
	 *
	 */
	public function setAttribute($name, $value)
	{
		if($name == 'class')
		{
			$this->addClass($value);
		}
		if($name == 'id')
		{
			$this->setID($value);	
		}
		else
		{
			$this->attributes[$name] = $value;
		}	
	}
	
	/**
	 * Gets the value for a given attribute. Returns null if it's not set or found
	 * @param string $name
	 * @return string|null
	 */
	public function getAttribute(string $name) : ?string
	{
		if(isset($this->attributes[$name]))
		{
			return $this->attributes[$name];
		}
		
		return null;
	}
	
	/**
	 * Unsets an existing attribute with a given $name.
	 * @param string $name The name of the attribute to be unset
	 */
	public function unsetAttribute($name)
	{
		unset($this->attributes[$name]);
			
	}
	
	/**
	 * Changes the tag for this view.
	 *
	 * The default tag for all views is a <div> and this method allows you to set the tag to any accepted HTML value.
	 *
	 * @param string $tag The name of the tag. This method will not validate this string so a bad value will result in
	 * bad HTML .
	 */
	public function setTag($tag)
	{
		$this->tag = str_ireplace('< >', '', $tag);
	}
	
	/**
	 * The tag for this view.
	 * @return string
	 */
	public function tag()
	{
		return $this->tag;
	}
	
	/**
	 * Adds a unique attribute commonly used by parsing systems that follow the pattern `data-$name="$value"`.
	 *
	 * This method is used to easily add data values to the tag. The convention is to have dash-separated-words for
	 * data attributes.
	 * @param string $name The name of the attribute *without* the "data-" string at the start.
	 * @param string $value THe value for the data value
	 */
	public function addDataValue($name, $value)
	{
		$this->setAttribute('data-'.$name, $value);
	}
	
	/**
	 * Adds an array of data values to this view
	 * @param array $data_values The array of data values
	 */
	public function addDataValueArray($data_values)
	{
		foreach($data_values as $name => $value)
		{
			$this->setAttribute('data-'.$name, $value);
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// HTML COLLAPSING
	//
	// HTML collapsing is the settings which indicate if
	// new views that are attached to this view will be
	// immediately processed and turned into an HTML string.
	//
	// By default collapsing is turned on for performance reasons,
	// however if there is a need to keep all the objects intact
	// after they are attached, then collapsing can be turned off.
	//
	//////////////////////////////////////////////////////


	
	/**
	 * Sets if this view will collapse views attached to it to HTML. If set to true, any view attached to this object
	 * won't exist once it is attached. All the CSS and JS values will be inherited by this class and the HTML will be
	 * saved for later use.
	 * @param bool $collapse Indicate if the view should collapse
	 */
	protected function setHTMLCollapsing($collapse)
	{
		$this->collapse_attached_views_to_html = $collapse;
	}

	//////////////////////////////////////////////////////
	//
	// CSS ATTRBIUTES
	//
	// These methods deal with adding CSS "class" attributes
	// to the view. All the CSS classes are stored in an array
	// separated out for easier processing.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the array of CSS classes for this view.
	 *
	 * @return array
	 */
	public function classes()
	{
		return $this->classes;
	}

	/**
	 * Adds a CSS class to this view.
	 *
	 * @param ?string $class The class name. If class name contains spaces, they will be reprocessed as separate CSS
	 * classes.
	 */
	public function addClass(?string $class) : void
	{
		// Null do nothing
		if(is_null($class))
		{
			return;
		}
		$classes = explode(' ', $class);
		foreach($classes as $class_name)
		{
			if(trim($class_name) != '')
			{
				foreach($this->processClassName($class_name) as $processed_class)
				{
					$this->classes[$processed_class] = htmlspecialchars($processed_class);
				}
			}
		}
	}

	/**
	 * Returns if this view has a given CSS class.
	 * @param string $class The CSS class name to test for.
	 * @return bool
	 */
	public function hasClass($class)
	{
		return isset($this->classes[$class]);
	}

	/**
	 * A method to test a given CSS class name for additions or alterations.
	 *
	 * This function can be extended as needed but it's core functionality allows it to add supplemental classes for
	 * font library codes such as Font Awesome.
	 *
	 * This method does not add any classes to this view, just processes them.
	 *
	 * @param string $class
	 * @return array The array of classe that are generated from this class.
	 */
	public function processClassName($class)
	{
		$classes = [];

		// Pre-process some common Fontawesome values
		// Catch the old FA 5 prefixes
		// FA 6 only uses solid or brand, simplified solution
		if($class == 'fab' || $class == 'fa-brands')
		{
			$classes['fa-brands'] = 'fa-brands';
			return $classes;

		}
		elseif($class == 'fas' || $class == 'far' || $class == 'fal' || $class == 'fa-solid' || $class == 'fa')
		{
			$classes['fa-solid'] = 'fa-solid';
			return $classes;
		}

		// Save the value
		$classes[$class] =  htmlspecialchars($class);

		// We're dealing with Font Awesome prefix
		if(str_starts_with($class,'fa-'))
		{
			// Not brand, then solid
			// Works so long as the fab or fa-brand prefix are applied first
			if(!$this->hasClass('fa-brands'))
			{
				$classes['fa-solid'] = 'fa-solid';
			}

			if(
				substr($class, 0, 8) != 'fa-flip-'
				&& substr($class, 0, 10) != 'fa-rotate-'
				&& $class != 'fa-fw'
				&& $class != 'fa-spin'
				&& substr($class, 0, 5) != 'fa-w-'
			)
			{


				// Catch-All for a commonly used value
				if($class == 'fa-close')
				{
					$classes[$class] = 'fa-times';
				}

				// -o outlines
				elseif(strpos($class, '-o-') > 0)
				{
					$classes[$class] = str_replace('-o', '', $class);
				}
				elseif(substr($class, -2) == '-o')
				{
					$classes[$class] = substr($class, 0, strlen($class) - 2);
				}

			}

		}

		return $classes;
	}

	/**
	 * Add an array of CSS classes with each entry in the array being a class name.
	 * @param array $classes The array of CSS class names
	 */
	public function addClassArray($classes)
	{
		if(!is_array($classes))
		{
			TC_triggerError('Provided class list  is of type "'.gettype($classes).'", should be an array of strings');

		}
		foreach($classes as $class_name)
		{
			if(trim($class_name) != '')
			{
				$this->classes[$class_name] = htmlspecialchars($class_name);
			}
		}
	}

	/**
	 * Removes a CSS class from this view.
	 * @param string $class
	 */
	public function removeClass($class)
	{
		$classes = explode(' ', $class);
		foreach($classes as $class_name)
		{
			if(trim($class_name) != '')
			{
				unset($this->classes[$class_name]);
			}
		}
	}

	/**
	 * Removes all CSS classes from this view.
	 */
	public function clearClasses()
	{
		$this->classes = array();
	}

	/**
	 * Returns a string with all the CSS classes concatenated together with a space between them.
	 * @return string
	 */
	public function classesString()
	{
		return implode(' ', $this->classes);
	}

	/**
	 * Returns the attribute tag for all the CSS classes.
	 *
	 * This is the complete string with class="<your_classes>" that can be inserted to HTML.
	 *
	 * @return string
	 */
	public function attributeTagForClass()
	{
		if(sizeof($this->classes) > 0)
		{
			return 'class="'.$this->classesString().'"';
		}
		return '';
	}

	//////////////////////////////////////////////////////
	//
	// CSS FILES / LINES
	//
	// CSS can be added a a reference to a single file
	// or as a runtime string to be inserted
	//
	// Both methods allow for CSS to be attached to any view
	// and it will propagate up to the main website view.
	//
	//////////////////////////////////////////////////////


	/**
	 * Adds a new CSS runtime string to be output.
	 *
	 * These are lines of CSS that can only be determined at runtime. Each line must have a unique ID otherwise it will
	 * an existing CSS runtime value.
	 * @param string $id The unique ID for the CSS runtime
	 * @param string $css The CSS to be run. This method does not attempt to validate this CSS>
	 */
	public function addCSSRuntime($id, $css)
	{
		$this->css_runtime_calls[$id] = $css;
	}
	
	/**
	 * Adds a new CSS runtime string to be output.
	 *
	 * This is the same as addCSSRuntime.
	 * @param string $id The unique ID for the CSS runtime
	 * @param string $css The CSS to be run. This method does not attempt to validate this CSS>
	 */
	public function addCSSLine($id, $css)
	{
		$this->css_runtime_calls[$id] = $css;
	}
	
	/**
	 * Returns the array of runtimes that have been added to this view.
	 *
	 * @return array
	 * @see TCv_View::addCSSRuntime()
	 * @see TCv_View::addCSSRuntimeArray()
	 */
	public function cssRuntimes()
	{
		// try a simpler approach
		return $this->css_runtime_calls;
	}
	
	/**
	 * Adds an array of CSS runtimes to this class.
	 *
	 * Function to take an array of CSS Runtime calls and add them to this item's css runtime list list. This is most
	 * often used to cascade the complete list of items to the object this is being attached to.
	 * @param array $call_array The array of strings that are other CSS runtimes
	 * @uses TCv_View::addCSSRuntime()
	 */
	public function addCSSRuntimeArray($call_array)
	{
		foreach($call_array as $id => $css)
		{
			$this->addCSSRuntime($id, $css);
		}
	}
	

	/**
	 * Clears all CSS files and runtime calls from this view.
	 */
	public function clearCSS()
	{
		$this->css_files = array();
		$this->css_runtime_calls = array();
		
	}
	/**
	 * Adds a CSS file to be loaded for this view.
	 * @param string $id A unique string to identify this file. If an ID already exists for this view, then it will be
	 * overridden.
	 * @param string $url The URL for the CSS file
	 * @param bool|string $media The media restriction for this css file
	 * @param bool $defer_load Indicates if the loading of this CSS should be deferred
	 *
	 */
	public function addCSSFile($id, $url, $media = false, bool $defer_load = false)
	{
		if(is_array($url))
		{
			if(isset($url['media']))
			{
				$media = $url['media'];
			}
			if(isset($url['defer']))
			{
				$defer_load = $url['defer'];
			}
			
			// Save the URL to avoid arrays passing through
			$url = $url['url'];
		}
		
		$values = [
			'url' => $url,
			'media' => is_string($media) ? $media : 'all',
			'defer' => $defer_load
		];
		$this->css_files[$id] = $values;
	}
	
	/**
	 * Removes a CSS file with a particular ID
	 * @param string $id
	 */
	public function removeCSSFile(string $id)
	{
		if(isset($this->css_files[$id]))
		{
			unset($this->css_files[$id]);
		}
		
	}
	
	
	/**
	 * Returns all the CSS files that are currently added to this view
	 *
	 * @return array
	 * @see TCv_View::addCSSFile()
	 * @see TCv_View::addCSSFileArray()
	 */
	public function cssFiles()
	{
		// try a simpler approach
		return $this->css_files;
	}
	
	/**
	 * Adds an array of CSS files to this view.
	 *
	 * @param array $file_array The array of css files which are added via the addCSSFile();
	 * @see TCv_View::addCSSFile()
	 */
	public function addCSSFileArray($file_array)
	{
		foreach($file_array as $id => $url)
		{
			$this->addCSSFile($id, $url);
		}
	}
	
	/**
	 * Adds a css file that is associated with a class in Tungsten.
	 *
	 * This method will look up the class_name provided and look for a CSS file with the same class name in the same
	 * folder. This follows a common pattern of having CSS files sit next to the views they are related to.
	 *
	 * A note regarding inheritance: If the value of false is provided and you call this method from a parent, it will
	 * use the called class name, which means it will look for the the child's class name, not the parent's class name.
	 *
	 * @param string|bool $class_name (Optional) Defaults to false, which uses the called class, otherwise it will
	 * find the class name provided and include that CSS file.
	 * @param bool $defer Defaults to false. Indicates if the loading of this CSS can be deferred until after loading
	 * @uses get_called_class()
	 * @uses ReflectionClass
	 * @uses TCu_Item::classFolderFromRoot()
	 * @uses TCv_View::addCSSFile()
	 */
	public function addClassCSSFile($class_name = false, bool $defer = false)
	{
		if($class_name == false)
		{
			$class_name = get_called_class();
		}

		$class_name = $this->removeClassNameCacheBusting($class_name);
		
		// Apply git-version cache busting
		$version = '?v'.TCv_Website::gitVersion();

		try
		{
			$reflection = new ReflectionClass($class_name);
			$file_path = $this->classFolderFromRoot($class_name, true).'/'.$reflection->getName().'.css'.$version;
			$this->addCSSFile($class_name,  $file_path,false, $defer);
		}
		catch(Exception $e)
		{
			$this->addConsoleDebug('Class "'.$class_name.'" not found.');
		}

	}
	
	/**
	 * Removes the cache-busting from old system that required each view to have a version
	 * @param string $class_name
	 * @return bool|string
	 */
	public function removeClassNameCacheBusting($class_name)
	{
		// Strip away any old cache-busting
		$hash_tag_pos = strpos($class_name,'?');
		if($hash_tag_pos > 0)
		{
			$class_name = substr($class_name, 0, $hash_tag_pos);
		}
		
		return $class_name;
	}
	
	/**
	 * Returns the array of CSS tags to be processed
	 * @return string[]
	 */
	public function headCSSTags()
	{
		$files = [];
		
		// ----- CSS FILES -----
		foreach($this->cssFiles() as $id => $url)
		{
			$href = $url;
			$media = 'all';
			$defer = false;
			if(is_array($url))
			{
				$href = $url['url'];
				$media = $url['media'];
				$defer = $url['defer'];
			}
			
			if($defer)
			{
				// Attempting to load css files using the deferred method with preload
				// Creates layout shifts and jitter
				// https://web.dev/articles/defer-non-critical-css
				$files['css_link_' . $id] =
					'<link rel="preload" as="style" onload="this.onload=null;this.rel=\'stylesheet\'"; '.
					' id="css_link_'.$id
					.'" href="'.htmlspecialchars($href).'" media="'.$media.'" />';
				
			}
			else
			{
				$files['css_link_' . $id] =
					'<link rel="stylesheet" id="css_link_' . $id
					. '" href="' . htmlspecialchars($href) . '" media="' . $media . '" />';
			}
			
			
		}
		
		// ----- CSS RUNTIMES -----
		if(sizeof($this->cssRuntimes()) > 0)
		{
			$runtime = '<style>';
			
			foreach($this->cssRuntimes() as $id => $css)
			{
				$runtime .= "\n".$css;
			}
			
			$runtime .= "\n\t\t".'</style>';
			$files['runtime'] = $runtime;
		}
		
		return $files;
	}
	
	/**
	 * Returns all the CSS that should go in the head of a website.
	 *
	 * This method will collect all the CSS classes and runtimes and generate a single string of HTML that can be
	 * added to a website's <head> tag.
	 * @return string The html to be inserted into the head tag.
	 */
	public function headCSS()
	{
		$html = '';
		foreach($this->headCSSTags() as $tag)
		{
			$html .= "\n\t\t".$tag;
		}
		
		return $html;
		
	}
	
	/**
	 * Converts a linked CSS into embedded
	 * @param $url
	 */
	protected function convertLinkedCSSToEmbedded($url)
	{
		if(is_array($url))
		{
			$url = $url['url'];
		}
		$path = explode('/', $url);
		//$filename = array_pop($path);
		$path = implode('/', $path).'/';
		
		// Deal with any /? after the url paths
		$pos = strpos($url, '?');
		if($pos !== false)
		{
			$url = substr($url, 0, $pos);
		}
		
		$contents = file_get_contents( $_SERVER['DOCUMENT_ROOT'].$url);
		
		$css_string = preg_replace('/url\((\'|\")?(\w)/', 'url($1'.$path.'$2', $contents);
		$css_string = str_ireplace('url("/admin', 'url("'.$this->fullDomainName().'/admin', $css_string);
		
		// Add line breaks to avoid quoted-printable issues
//		$line_break_characters = array(';','}',',');
//		foreach($line_break_characters as $char)
//		{
//			$css_string = str_ireplace($char,$char.PHP_EOL, $css_string);
//
//		}
		
		$this->addCSS($css_string);
	}
	
	
	/**
	 * Returns the CSS for this view if it was all embedded. This will grab th
	 * @return string
	 */
	public function embeddedCSS()
	{
		if(!$this->embedded_css)
		{
			$this->embedded_css = '';
			
			foreach($this->cssFiles() as $id => $css_data)
			{
				if(is_array($css_data))
				{
					$url = $css_data['url'];
				}
				else
				{
					$url = $css_data;
				}
				
				$this->convertLinkedCSSToEmbedded($url);
				
			}
			
			foreach($this->cssRuntimes() as $id => $css_data)
			{
				$this->addCSS($css_data);
			}
			
			foreach($this->css as $css_string)
			{
				$this->embedded_css .= $css_string;
			}
		}
		
		return $this->embedded_css;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// Font Awesome Functions
	//
	// Methods related to the Font Awesome Library that
	// provides a way to add icons to the website
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds a font-awesome symbol to this website that will be generated for efficiency
	 * @see https://fontawesome.com/how-to-use/performance-and-security
	 * @param string $symbol_id The unique symbol ID, that must be unique to the page.
	 * @param string $code The Font Awesome code that is associated with this symbol such as `fab fa-facebook`
	 */
	public function addFontAwesomeClassAsSymbol($symbol_id, $code)
	{
		$this->font_awesome_symbols[$symbol_id] = $code;
	}

	/**
	 * Returns all the font-awesome symbols for this view
	 * @return array
	 */
	public function fontAwesomeSymbols()
	{
		return $this->font_awesome_symbols;
	}

	/**
	 * Returns a TCv_View which is the <i> tag that represents the symbol
	 * @param string $symbol_id
	 * @return TCv_View
	 */
	public function fontAwesomeTagForSymbol($symbol_id)
	{
		$fa_tag = new TCv_View();
		$fa_tag->setTag('i');
		$fa_tag->addClass($this->font_awesome_symbols[$symbol_id]);
		$fa_tag->setAttribute('data-fa-symbol', $symbol_id);
		return $fa_tag;
	}

	/**
	 * Returns all the regular SVG icons
	 * @return array
	 */
	public function svgIconSymbols()
	{
		return $this->svg_icon_symbols;
	}

	/**
	 * Adds a font-awesome symbol to this website that will be generated for efficiency
	 * @param string $symbol_id The unique symbol ID, that must be unique to the page.
	 * @param string $code The Font Awesome code that is associated with this symbol such as `fab fa-facebook`
	 */
	public function addTextAsSVGSymbol($symbol_id, $code)
	{
		$this->addSVGIconSymbol($symbol_id, $code);
		$this->addText($this->svgSymbolLink($symbol_id));
	}

	public function addSVGIconSymbol($symbol_id, $code)
	{
		$this->svg_icon_symbols[$symbol_id] = $code;

	}

	/**
	 * Returns the symbol link for an icon. This can be used if it is already imported.
	 * @param string $symbol_id
	 * @return string
	 */
	public function svgSymbolLink($symbol_id)
	{
		return '<svg><use xlink:href="#'.$symbol_id.'"/></svg>';
	}



	//////////////////////////////////////////////////////
	//
	// JavaScript (JS)
	//
	// Javascript methods allow for any file or line of code
	// to be added to a view.
	//
	// All approaches allow for JS to be attached to any view
	// and it will propagate up to the main website view.
	//
	//////////////////////////////////////////////////////


	/**
	 * Clears all JS stored for this view including any files or lines.
	 */
	public function clearJS()
	{
		$this->js_files = array();
		$this->js_lines_wait_for_load = array();
		$this->js_lines = array();
		
	}
	
	/**
	 * Adds a JavaScript file to be loaded for the view.
	 * @param string $id The unique value that represents this file.
	 * @param string $url The path to the file
	 */
	public function addJSFile($id, $url)
	{
		$this->js_files[$id] = $url;
	}
	
	/**
	 * Adds a JavaScript file to be loaded for the view. This file contains initialization code which is run every
	 * time, rather than attempting to avoid excess loads. This is particularly true for views loaded in dialogs and
	 * popup boxes.
	 * @param string $id The unique value that represents this file.
	 * @param string $url The path to the file
	 */
	public function addJSInitFile($id, $url)
	{
		$this->js_files['init_'.$id] = $url;
	}
	
	
	/**
	 * Returns the array of JavaScript files paths that have been added to this view.
	 * @return array
	 *
	 * @see TCv_View::addJSFile()
	 * @see TCv_View::addJSFileArray()
	 */
	public function jsFiles()
	{
		return $this->js_files;
	}
	
	/**
	 * Adds an array of JS files to this view.
	 *
	 * @param $file_array
	 *
	 * @uses TCv_View::addJSFile()
	 */
	public function addJSFileArray($file_array)
	{
		foreach($file_array as $id => $url)
		{
			$this->addJSFile($id, $url);
		}
	}
	
	/**
	 * Adds a JQuery file that belongs to a specific class in the sytem.
	 *
	 * This method will look up the class_name provided and look for a JQuery file with the same class name in the same
	 * folder. This follows a common pattern of having JQuery files sit next to the views they are related to. The name
	 * of the JQuery file must match `jquery.<$class_name>.js`
	 *
	 * A note regarding inheritance: If the value of false is provided and you call this method from a parent, it will
	 * use the called class name, which means it will look for the the child's class name, not the parent's class name.
	 *
	 * @param string|bool $class_name (Optional) Defaults to false, which uses the called class, otherwise it will
	 * find the class name provided and include that JQuery file.
	 * @param string|bool $namespace (Optional) Default false. The namespace for the class. If used, then it will attempt
	 * to load `jquery.<$namespace>.<$class_name>.js
	 *
	 * @uses get_called_class()
	 * @uses ReflectionClass
	 * @uses TCu_Item::classFolderFromRoot()
	 * @uses TCv_View::addJSFile()
	 * @see TCv_View::addClassJQueryInit()
	 */
	public function addClassJQueryFile($class_name = false, $namespace = false)
	{
		$version = '';
		if($class_name == false)
		{
			$class_name = get_called_class();
		}
		
		$class_name = $this->removeClassNameCacheBusting($class_name);
		
		// Apply git-version cache busting
		$version = '?v'.TCv_Website::gitVersion();
		
		try
		{
			$reflection = new ReflectionClass($class_name);
			$file_prefix = $this->classFolderFromRoot($class_name, true).'/jquery.'.($namespace ? $namespace.'.' : '').$reflection->getName();
			// Check for a minified version
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$file_prefix.'.min.js'))
			{
				$file_path = $file_prefix.'.min.js';
			}
			else
			{
				$file_path = $file_prefix.'.js';
			}
			
			$this->addJSFile($class_name,  $file_path.$version);
		}
		catch(Exception $e)
		{
			$this->addConsoleDebug('Class "'.$class_name.'" not found.');
		}
	}
		
	/**
	 * Adds a JS line that will initialize a class JQuery file.
	 *
	 * This method is used in conjunction with addClassJQueryFile(). It will add a JS
	 *
	 * @param string|bool $class_name (Optional) Default false which will load the called class.
	 * @param array|bool $parameters (Optional) The parameters to be provided. Must be an array, will be converted to JSON.
	 * @param bool $wait_for_load (Optional) Default true. Indicates if the line should wait for the site to load.
	 * @param string|bool $variable_name (Optional) Default to false. The variable name is the name that is used as the
	 * value for window.$variable_name which is where the reference to the class exists globally. IF set to false, then
	 * the variable name will equal the attributeID() for the view.
	 * @param string|bool $js_selector (Optional) Default to false. The string that should be used as the selector for the init.
	 *
	 * @uses TCv_View::addJSLine()
	 * @uses get_called_class()
	 * @uses ReflectionClass
	 * @see TCv_View::addClassJQueryFile()
	 */
	public function addClassJQueryInit($class_name = false, $parameters = false, $wait_for_load = true, $variable_name = false, $js_selector = false)
	{
		if($class_name == false)
		{
			$class_name = get_called_class();
		}
	
		$id = $this->attributeID();
		if($id == '')
		{
		 	$id = 'list_'.rand(100, 10000);
		 	$this->setIDAttribute($id);
		}
		
		if($variable_name === false)
		{
			$variable_name = $this->attributeID();
		}
		
		// Remove anything other than letters, numbers and underscore. Create bad variable names
		$variable_name = preg_replace("/[^a-zA-Z0-9_]/", "", $variable_name);
		
		if($js_selector === false)
		{
			$js_selector = "." . $this->attributeID();
		}
		// Use the ID for this object
		$line = "window.W.".$variable_name." = $('".$js_selector."').".$class_name."(".json_encode($parameters).");";
		$this->addClass($this->attributeID()); // ensure class is there
		$this->addJSLine($class_name.'_'.$this->id(), $line, $wait_for_load);
	}
	
	/**
	 * Adds a JS class file for this view that matches the class name for the view.
	 *
	 * This method will look up the class_name provided and look for a JS file with the same class name in the same
	 * folder.
	 *
	 * A note regarding inheritance: If the value of false is provided and you call this method from a parent, it will
	 * use the called class name, which means it will look for the the child's class name, not the parent's class name.
	 *
	 * @param string|bool $class_name (Optional) Defaults to false, which uses the called class, otherwise it will
	 * find the class name provided and include that JS file.
	 *
	 * There is an option for loading JS class files for editor forms in pages. These forms often require separate JS
	 * and are loaded using the classname then "_PageEditorForm" after the filename. Tungsten will detect this and
	 * load the correct file instead.
	 *
	 * @uses get_called_class()
	 * @uses ReflectionClass
	 * @uses TCu_Item::classFolderFromRoot()
	 * @uses TCv_View::addJSFile()
	 */
	public function addClassJSFile($class_name = false)
	{
		if($class_name == false)
		{
			$class_name = get_called_class();
		}
		
		$class_name = $this->removeClassNameCacheBusting($class_name);
		
		// Detect the page editor versions
		$is_editor_form = false;
		if(substr($class_name,-15) == '_PageEditorForm')
		{
			$is_editor_form = true;
			$class_name = substr($class_name,0,-15);
		}
		
		
		try
		{
			$reflection = new ReflectionClass($class_name);
			$file_prefix = $this->classFolderFromRoot($class_name);
			$path_prefix = $this->classFolderFromRoot($class_name, true);
			$js_class_name = $class_name.($is_editor_form ? '_PageEditorForm' : '');
			// Check for a minified version
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$file_prefix.'/'.$class_name.'.min.js'))
			{
				$file_path = $path_prefix.'/'.$js_class_name.'.min.js';
			}
			else
			{
				$file_path = $path_prefix.'/'.$js_class_name.'.js';
			}
			
			// Apply git-version cache busting
			$version = '?v'.TCv_Website::gitVersion();
			
			$this->addJSFile($js_class_name,  $file_path.$version);
		}
		catch(Exception $e)
		{
			$this->addConsoleDebug('Class "'.$class_name.'" not found.');
		}
	}
	
	/**
	 * Adds a JS line that will initialize a class JS file.
	 *
	 * @param string|bool $class_name (Optional) Default false which will load the called class.
	 * @param array|bool $parameters (Optional) The parameters to be provided. Must be an array, will be converted to JSON.
	 * @param bool $wait_for_load (Optional) Default true. Indicates if the line should wait for the site to load.
	 * @param string|bool $variable_name (Optional) Default to false. The variable name is the name that is used as the
	 * value for window.$variable_name which is where the reference to the class exists globally. IF set to false, then
	 * the variable name will equal the attributeID() for the view.
	 *
	 * @uses TCv_View::addJSLine()
	 * @uses get_called_class()
	 * @uses ReflectionClass
	 * @see TCv_View::addClassJQueryFile()
	 */
	public function addClassJSInit($class_name = false,
	                               $parameters = false,
	                               $wait_for_load = true,
	                               $variable_name = null
	                               )
	{
		if($class_name == false)
		{
			$class_name = get_called_class();
		}
		
		// OK. This can get messy. The view being loaded may or may not have an ID and we might be loading a Page
		// Editor Form, so there's a few things to check.
		
		// Deal with variable for JS in PageEditorForms
		if(substr($class_name,-15) == '_PageEditorForm')
		{
			// If we're in a Page Editor Form and there's an ID attribute, we use that for both the ID and the var name
			if($this->attributeID() != '')
			{
				$variable_name = $this->attributeID().'_pef'; // page editor form
				$id = $variable_name;
				
			}
			else // PEF but no ID set, use the class name
			{
				$variable_name = 'TMv_PageRendererContentForm_pef';
				$id = $variable_name;
			}
			
			
		}
		else // Normal view
		{
			$id = $this->attributeID();
			if($id == '')
			{
				$id = 'el_'.rand(100, 10000);
				$this->setIDAttribute($id);
			}
			
			// If they didn't provide a variable name, use the attribute name
			if($variable_name === null)
			{
				$variable_name = $this->attributeID();
			}
			
			// Remove anything other than letters, numbers and underscore. Create bad variable names
			$variable_name = preg_replace("/[^a-zA-Z0-9_]/", "", $variable_name);
			
			
			// Check until we have a JS class parent
			$class_file_found = false;
			do
			{
				// This uses reflection classes to find the first JS file in the path of inheritance. The JS classes
				// should extend the relevant parent classes if necessary for continuity.
				
				
				if(file_exists($_SERVER['DOCUMENT_ROOT'] . $this->classFolderFromRoot($class_name) .
				               '/' . $class_name . '.js') )
				{
					$class_file_found = true;
				}
				else
				{
					$reflection = new ReflectionClass($class_name);
					$parent = $reflection->getParentClass();
					if($parent)
					{
						$class_name = $parent->getName();
					}
					else
					{
						$class_name = false;
					}
					
				}
				
			} while(!$class_file_found && $class_name != false);
		}
		
		// Add any passed in parameters ot the init values
		if(is_array($parameters))
		{
			$this->updateJSClassInitValues($parameters);
		}
		
		// only bother if we found a class name
		if($class_name)
		{
			$line = "window.W." . $variable_name .
				" = new " . $class_name . "(document.getElementById('" . $id . "')," . json_encode($this->getClassJSInitValues()) . ");";
			$this->addClass($this->attributeID()); // ensure class is there
			$this->addJSLine($class_name . '_' . $id, $line, $wait_for_load);
		}
	}
	
	/**
	 * Updates the init values for the class for the form
	 * @param array $values
	 * @return void
	 */
	public function updateJSClassInitValues(array $values)
	{
		$this->js_init_values = array_merge($this->js_init_values, $values);
	}
	
	/**
	 * Adds a single value to the init list for the JS class
	 * @param string $name
	 * @param $value
	 * @return void
	 */
	public function addJSClassInitValue(string $name, $value)
	{
		$this->js_init_values[$name] = $value;
	}
	
	/**
	 * Returns the JS values for the class init
	 * @return array
	 */
	public function getClassJSInitValues()
	{
		return $this->js_init_values;
	}
	
	
	
	/**
	 * Adds a single line of JS to be run on the page when it's rendered.
	 *
	 * @param string $id The unique ID that indentifies this line.
	 * @param string $line The JS line to be run.
	 * @param bool $wait_for_load (Optional) Default false. Indicates if the line should be set to wait for the entire
	 * page to render before running. C
	 */
	public function addJSLine($id, $line, $wait_for_load = false)
	{
		if($wait_for_load)
		{
			$this->js_lines_wait_for_load[$id] = $line;
		}
		else
		{
			$this->js_lines[$id] = $line;
		}
	}
	
	/**
	 * Returns the array of javascript lines that have been added to this view.
	 * @return array
	 */
	public function jsLines()
	{
		// try a simpler approach
		return $this->js_lines;
	}
	
	/**
	 * Returns the lines that have been set to wait for loading.
	 * @return array
	 */
	public function jsLinesWaitingForLoad()
	{
		// try a simpler approach
		return $this->js_lines_wait_for_load;
	}
	
	/**
	 * Adds an array of JS lines to this view.
	 *
	 * @param array $line_array The array of lines to be added
	 * @param bool $wait_for_load (Optional) Default false. Indicates if the lines should be set to wait for site loading.
	 */
	public function addJSLineArray($line_array, $wait_for_load = false)
	{
		foreach($line_array as $id => $line)
		{
			if($wait_for_load)
			{
				$this->addJSLine($id, $line, TCv_WaitForLoading);
			}
			else
			{
				$this->addJSLine($id, $line);
			}
		}
	}
	
	/**
	 * Return the JS tags array for the page
	 * @param bool $use_wait_for_load (Optional) Indicates if the lines should be wrapped in a wait for load block.
	 * @return string[]
	 */
	public function headJSTags($use_wait_for_load = true)
	{
		$files = [];
		
		foreach($this->jsFiles() as $id => $url)
		{
			if(substr($url,0,7) == '<script')
			{
				$files['js_link_'.$id] = $url;
			}
			else
			{
				$files['js_link_'.$id] =
					'<script id="js_link_'.$id.'" src="'.htmlspecialchars($url).'" defer></script>';
			}
		}
		
		// ----- JS INITS -----
		if(sizeof($this->jsLines()) > 0)
		{
			$html = '<script>';
			foreach($this->jsLines() as $id => $line)
			{
				$html .= "\n\t".$line;
			}
			$html .= "\n\t\t".'</script>';
			$files['js_lines'] = $html;
		}
		
		// ----- JS WAIT FOR LOAD -----
		if(sizeof($this->jsLinesWaitingForLoad()) > 0)
		{
			$html ='<script>';
			if($use_wait_for_load)
			{
				$html .= "\n".'window.addEventListener("DOMContentLoaded",() => {';
			}
			foreach($this->jsLinesWaitingForLoad() as $id => $line)
			{
				$html .= "\n\t".$line;
			}
			if($use_wait_for_load)
			{
				$html .= "\n".'});';
			}
			$html .= "\n".'</script>';
			$files['js_wait_for_load'] = $html;
		}
		
		
		return $files;
	}
	
	/**
	 * Returns a string with all the JavaScript calls that are meant to be shown traditionally in the head of a page.
	 *
	 * @param bool $use_wait_for_load (Optional) Indicates if the lines should be wrapped in a wait for load block.
	 * @return string The html to be shown
	 * @uses TCv_View::jsFiles()
	 * @uses TCv_View::jsLinesWaitingForLoad()
	 */
	public function headJS($use_wait_for_load = true)
	{
		$html = '';
		foreach($this->headJSTags($use_wait_for_load) as $tag)
		{
			$html .= "\n\t\t".$tag;
		}
		
		return $html;
		
	}

	//////////////////////////////////////////////////////
	//
	// VIEW ATTACHING
	//
	// All views are designed to have other content added
	// to them, commonly by nesting views inside each other.
	//
	//
	//////////////////////////////////////////////////////

	/**
	 * Attaches another view to this view.
	 *
	 * This method will take the provided view, and deal with all the html, css, and js properties and incorporate those
	 * values into itself.
	 *
	 * If collapsing is turned off, then the attached view won't be processed but saved intact for later use.
	 *
	 * @param TCv_View|bool $view The view to be attached. If a value of false is provided, then no action is taken.
	 * @param bool $prepend (Optional) Default false. If set to true, then the provided view will be attached as the new
	 * first view inside this view. Each time a view is called with $prepend turned on, it becomes the first view.
	 * @return string|bool|TCv_View Depending on how the view was processed, this method returns the best representation
	 * of what was attached. If collpasing was turned off, then the view is returned. Otherwise HTML is returned.
	 *
	 * @uses TCv_View::setAsAttached()
	 * @uses TCv_View::handleAttachedToView()
	 * @uses TCv_View::setParentView()
	 * @uses TCv_View::attachCSSFromView()
	 * @uses TCv_View::attachJSFromView()
	 * @uses TCv_View::attachMetaTagsFromView()
	 */
	public function attachView($view, $prepend = false)
	{
		// If we receive a false value, exit gracefully
		if($view === false || is_null($view))
		{
			return false;	
		}

		// Check to see that we have a view object
		TC_assertObjectType($view, 'TCv_View');

		// Process the attached view
		$view->setAsAttached();
		$view->handleAttachedToView();
		$view->setParentView($this);

		// If we're collapsing views
		if($this->collapse_attached_views_to_html)
		{
			// Grab the HTML and save it to the array of attached views
			$html = $view->html();
			if($prepend)
			{
				array_unshift($this->attached_views, $html);
			}
			else
			{	
				$this->attached_views[] = $html;
			}
				
			// Handle CSS / JS / META
			$this->attachCSSFromView($view);
			$this->attachJSFromView($view);
			$this->attachMetaTagsFromView($view);
		
			unset($view);
			
			return $html;
		}
		else
		{
			if($prepend)
			{
				array_unshift($this->attached_views, $view);
			}
			else
			{	
				$this->attached_views[] = $view;
			}
			
			return $view;
		
			
			
		}
	}	
	
	/**
	 * Attaches all the CSS from another view to this view.
	 *
	 * @param TCv_View $view The view to add CSS from
	 */
	protected function attachCSSFromView($view)
	{
		if($view instanceof TCv_View)
		{
			foreach($view->cssFiles() as $id => $url)
			{
				$this->addCSSFile($id, $url);
			}
			
			foreach($view->cssRuntimes() as $id => $url)
			{
				$this->addCSSRuntime($id, $url);
			}

			foreach($view->fontAwesomeSymbols() as $id => $code)
			{
				$this->addFontAwesomeClassAsSymbol($id, $code);
			}

			foreach($view->svgIconSymbols() as $id => $code)
			{
				$this->addSVGIconSymbol($id, $code);
			}
		}
		
	}
	
	/**
	 * Attaches all the JS from another view to this view.
	 *
	 * @param TCv_View $view The view to add JS from.
	 */
	protected function attachJSFromView($view)
	{
		if($view instanceof TCv_View)
		{
			foreach($view->jsFiles() as $id => $url)
			{
				$this->addJSFile($id, $url);
			}
			
			foreach($view->jsLines() as $id => $line)
			{
				$this->addJSLine($id, $line, false);
			}
		
			foreach($view->jsLinesWaitingForLoad() as $id => $line)
			{
				$this->addJSLine($id, $line, true);
			}
			
		}
		
	}
	
	/**
	 * Attaches all the meta tags from another view to this view.
	 * @param TCv_View $view The view to add the meta tags from
	 */
	protected function attachMetaTagsFromView($view)
	{
		if($view instanceof TCv_View)
		{
			foreach($view->metaTags() as $id => $value)
			{
				$this->addMetaTag($id, $value);
			}
			
		}
		
	}
	
	
	/**
	 * Sets an internal flag for this view indicated that it has been attached to another view.
	 */
	public function setAsAttached()
	{
		$this->has_been_attached = true;
	}
	
	/**
	 * An overload method that can be called to deal with anything relevant when this view is attached to another view.
	 *
	 * This method will always be called prior to any other action. It can be overridden as necessary to perform tasks
	 * such as verifications.
	 */
	public function handleAttachedToView()
	{
		
	}	
	
	/**
	 * Sets the parent view for this view.
	 * @param TCv_View $parent The parent view
	 */
	public function setParentView($parent)
	{
		$this->parent_view = $parent;
	}
	
	/**
	 * Returns the parent view for this view
	 * @return TCv_View|bool
	 */
	public function parentView()
	{
		return $this->parent_view;
	}
		
	/**
	 * Returns the number of attached view for this view
	 * @return int
	 */
	public function numAttachedViews()
	{
		return sizeof($this->attached_views);
	}
	
	/**
	 * Clears all attached views.
	 *
	 * This method does not clear any attached CSS, JS or meta tags
	 */
	public function clear()
	{
		return $this->detachAllViews();
	}
	
	/**
	 * Returns if this view has any attached views.
	 * @return bool
	 *
	 * @uses TCv_View::numAttachedViews()
	 */
	public function isEmpty()
	{
		return $this->numAttachedViews() == 0;
	}
	
	/**
	 * Returns all the attached views
	 * @return TCv_View[]
	 */
	public function attachedViews()
	{
		return $this->attached_views;
	}
	
	/**
	 * Detaches all the views from this view
	 * @see TCv_View::clear();
	 */
	public function detachAllViews()
	{
		$this->attached_views = array();
	}
	
	/**
	 * Detaches a specific view with an ID.
	 *
	 * This method only works if the view has collapsing turned off. If collapsing is turned on, then the views objects
	 * are gone and only the html remains. If collapsing is turned off, then we still have view objects and we can call
	 * the id() method.
	 * @param mixed $id The id to be
	 *
	 * @uses TCv_View::id()
	 */
	public function detachViewWithID($id)
	{
		foreach($this->attached_views as $index => $view)
		{
			if($view instanceof TCv_View)
			{
				if($view->id() == $id)
				{
					unset($this->attached_views[$index]);
				}
			}
		}	
	}
	
	/**
	 * Detaches a view with a specific index.
	 *
	 * All attached views are indexed and looping through the views will provide indices which can be referenced
	 * for this method.
	 *
	 * @see TCv_View::attachedViews()
	 * @param mixed $index The index of the view
	 */
	public function detachViewWithIndex($index)
	{
		unset($this->attached_views[$index]);
	}
	
	/**
	 * Adds a view that will be attached at a later date, but is not added to the current list of attached views.
	 *
	 * This method is useful for attaching views that should be saved in the original state only to be attached at the
	 * time of rendering.
	 * @param $view
	 *
	 * @see TCv_View::attachAllUnattachedViews()
	 */
	public function addUnattachedView($view)
	{
		TC_assertObjectType($view, 'TCv_View');
		
		$this->views_to_attach[] = $view;
	}
	
	/**
	 * Attaches all the views that are currently marked as unattached, which hten clears that list of views.
	 */
	public function attachAllUnattachedViews()
	{
		foreach($this->views_to_attach as $index => $view)
		{
			$this->attachView($view);
			unset($this->views_to_attach[$index]);
		}
	}
	
		
	
	/**
	 * Adds a string or html to this view.
	 *
	 * @param string $html The text or HTML to be added to this view.
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 * @uses TCu_Text
	 */
	public function addText($html, $disable_template_parsing = false)
	{
		if(is_object($html))
		{
			TC_triggerError('Attempting to pass an object into text for a view. see TCv_View::attachView().');
		}
		
		// Nothing to do with null or empty strings values
		if(is_null($html) || $html == '')
		{
			return;
		}
		
		$text_item = new TCu_Text($html, false); // no trim
		if($disable_template_parsing)
		{
			$text_item->disableTemplateParsing();
		}
		
		
		// Get the text
		$text = $text_item->text();
		
		// Find page view links
		// this was FAR too aggressive and literally ruined forms
		// Paulo – 2024-08-13
		//$text = $this->replacePageViewCodesWithURL($text);
		
		
		$this->attached_views[] = $text;

	}
	
	/**
	 * Wraps the existing in this view within another view with the $tag provided.
	 *
	 * That new view then becomes the only attached view to this view.
	 * @param TCv_View|string $tag The tag for the new view. This can be another view or a string which is the <tag>.
	 *
	 */
	public function wrapContentInTag($tag)
	{
		// Deal with string tags
		if( ! ($tag instanceof TCv_View) )
		{
			$tag_view = new TCv_View();
			$tag_view->setTag($tag);
			$tag = $tag_view;
		}
		foreach($this->attachedViews() as $view)
		{
			if($view instanceof TCv_View)
			{
				$tag->attachView($view);
			}
			else
			{
				$tag->addText($view, true);
			}	
		}
		
		$this->detachAllViews();
		$this->attachView($tag);
	}

	//////////////////////////////////////////////////////
	//
	// META TAGS
	//
	// Meta tags can be added to any view, which will then
	// propagate up to the main website view.
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds a meta tag to the view.
	 *
	 * This method has a legacy function which the $attributes are provided as a string. In that usage case, the string
	 * must be the entire meta tag with all the relevant attributes.
	 *
	 * @param string $name The name of the meta tag
	 * @param array|string $attributes An array of attributes that should be included for the meta tag.
	 */
	public function addMetaTag($name, $attributes)
	{
		if(is_array($attributes))
		{
			$tag = new TCv_View();
			$tag->setTag('meta');
			$tag->setAttribute('name', $name);
			foreach($attributes as $attribute_name => $value)
			{
				$tag->setAttribute($attribute_name, $value);	
			}
			$this->meta_tags[$name] = $tag->html();
		}
		else // legacy
		{
			$this->meta_tags[$name] = $attributes;
		}
	}
	
	/**
	 * Returns the array of meta tags for this view
	 * @return array
	 */
	public function metaTags()
	{
		return $this->meta_tags;
	}

	//////////////////////////////////////////////////////
	//
	// ID METHODS
	//
	// Methods specifically set for the ID attribute
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the special tag attribute for id="<unique_id>".
	 *
	 * Due to the unique nature of the id attribute, this method ensures that only one ID is every set.
	 *
	 * @param string $id The attribute for the ID
	 */
	public function setIDAttribute($id)
	{
		$this->tag_attribute_id = $id;
	}
	
	/**
	 * Sets the property to indicate if the ID attribute should be shown.
	 *
	 * By default the ID attribute is shown for any view if it is not set to false or an empty string.
	 * @param bool $show
	 */
	public function setShowID($show)
	{
		$this->show_id = $show;
	}
	
	/**
	 * Returns the string with the complete attribute such as `id="<unique_id>".
	 *
	 * @return string
	 */
	public function attributeTagForID()
	{
		if($this->show_id && $this->attributeID() !== false && !is_null($this->attributeID()))
		{
			return 'id="'.htmlspecialchars($this->attributeID()).'"';
		}
		return '';
	}
	
	/**
	 * Returns the value for the ID
	 * @return bool|int|string
	 */
	public function attributeID()
	{
		if($this->tag_attribute_id)
		{
			return $this->tag_attribute_id;
		}
		else
		{
			return $this->id();
		}
	}

	//////////////////////////////////////////////////////
	//
	// HELP VIEWS
	//
	// Help views are used in the Tungsten interface to
	// provide context to the user. Any view can have an
	// associated "help view".
	//
	// The Tungsten admin will provide a button to view the
	// help view and that same functionality can be incorporated
	// into other parts of the website if necessary.
	//
	//////////////////////////////////////////////////////

	/**
	 * The help view that should be shown alongside this view. This method can return false for "no view" or it can return
	 * a TCv_View itself.
	 *
	 * This method is designed to be overloaded in parent classes that extend TCv_View.
	 *
	 * @return TCv_View|bool
	 */
	public function helpView()
	{
		return false;
	}
	
	
	
	/**
	 * A view can have default values for its help HTML. This should be a string that contains valid HTML that helps
	 * explain this view. This value is used when generating help for this component in Tungsten.
	 * @return string|null The html string or null. If null, it indicates no default help, which is what every view
	 * returns as a default.
	 */
	public static function defaultHelpHTML() : ?string
	{
		return null;
	}


	//////////////////////////////////////////////////////
	//
	// PAGES CONTENT SPECIFIC
	//
	// These are methods that must be visible to all views
	// which have an impact on page display and loading.
	//
	//
	//////////////////////////////////////////////////////


	/**
	 * Manually sets the browser's page title.
	 * @param string $page_title THe page title to be shown
	 * @param bool $lock (Optional) Default false. Indicates if the title should be locked from any future title changes.
	 */
	public function setBrowserPageTitle($page_title, $lock = false)
	{
		TCv_Website::website()->setTitle($page_title);
		if($lock)
		{
			TCv_Website::website()->lockTitle();
		}
		
	}
	
	/**
	 * Returns if the view is being loaded in the admin, Pages module page.
	 *
	 * @return bool
	 */
	public function isLoadingInAPagesModulePage()
	{
		return $this->urlSection(4) == 'pages_render.php';
	}

	//////////////////////////////////////////////////////
	//
	// RENDERING METHODS
	//
	// These are methods specific to the rendering of the view
	//
	//
	//////////////////////////////////////////////////////
	
	/**
	 * An array of values used to instantiate the return-as-json values for this view. You might want to pass additional
	 * values back along with the js/css/html that is auto generated in TSc_ModuleController. If you want your view to
	 * return additional values that are likely used by JS or PHP, then override this method and add values to the array.
	 * @return array
	 */
	public function returnAsJSONDefaultValues() : array
	{
		return [];
	}
	
	
	/**
	 * Returns the rendered html with the CSS and JS appended to the end.
	 *
	 * This method is not the normal way in which content is presented on a site. If a view is being generated
	 * asyncronously via javascript, then it is possible that all the CSS and JS is needed as well.
	 *
	 * @return string
	 * @uses TCv_View::html()
	 * @uses TCv_View::headCSS()
	 * @uses TCv_View::headJS()
	 */
	public function htmlWithCSSandJS()
	{
		// get the HTML
		$html = $this->html();
		$html .= $this->headCSS();
		$html .= $this->headJS(false);
		
		return $html;
	}
		
	/**
	 * The html for all the views that are attached.
	 *
	 * @return string
	 */
	public function htmlForAttachedViews()
	{
		$html = '';
		foreach($this->attached_views as $id => $view_array)
		{
			// Possible that the attached view is actually an array of items
			// Convert non-arrays into arrays
			if(!is_array($view_array))
			{
				$view_array = array($view_array);
			}
			
			foreach($view_array as $view )
			{
				if($view instanceof TCv_View)
				{
					$html .= $view->html();
					
					// Handle CSS / JS
					$this->attachCSSFromView($view);
					$this->attachJSFromView($view);
				}
				else
				{
					$html .= $view;
				}
			}
			
		}
		return $html;
	}

	
	
	
	/**
	 * By default a blank method that can be called to attach views and process items
	 */
	public function render()
	{
		// Add view functionality here

	}

	/**
	 * The main method for acquiring the html for this view.
	 *
	 * * This method returns the complete tag for this view. Any non-collapsed views are rendered.
	 * * Any unattached views are also attached.
	 *
	 * This method is commonly overloaded to add additional content or views. In those instances, that overload method
	 * must call `return parent::html()` otherwise no content is produced.
	 * @return string
	 */
	public function html()
	{
		// calls the render function which can be used to generate view and attach them in a more
		// particular order within extended classes

		if(!$this->render_called)
		{
			$this->render();
			$this->render_called = true;
		}

		// avoid double rendering
		$html = '<'.htmlspecialchars($this->tag);
		
		if($this->attributeTagForClass() != '')
		{
			$html .= ' '.$this->attributeTagForClass();
		}
		if($this->attributeTagForID() != '')
		{
			$html .= ' '.$this->attributeTagForID();
		}
		
		if($this->tag == 'img' && !isset($this->attributes['alt']))
		{
			$this->setAttribute('alt', '');
		}
		
		foreach($this->attributes as $name => $value)
		{
			if(is_array($value))
			{
				$value = '['.implode(',', $value).']';
			}
			
			// Always add the attribute name
			$html  .= ' '.htmlspecialchars($name);
			
			// Non-null values get added
			if(!is_null($value))
			{
				$html .= '="'.($name == 'onclick' ? $value : htmlspecialchars($value)).'"';
			}
			
		}
		
		$single_tag = in_array($this->tag, static::$single_tags);
		
		if(!$single_tag)
		{
			$html .= '>';
		}
		// deal with any outstanding unattached views
		$this->attachAllUnattachedViews();
		$html .= $this->htmlForAttachedViews();
		if(!$single_tag)
		{
			$html .= '</'.htmlspecialchars($this->tag).'>';
		}
		else
		{
			$html .= ' />';
		}
		
		return $html;
	}
	
	
	
}

define('TCv_WaitForLoading', true);
define('TCv_ViewPrepend', true);

?>