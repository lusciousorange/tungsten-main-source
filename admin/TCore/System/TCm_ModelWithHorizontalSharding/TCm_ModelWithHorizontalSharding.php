<?php
/**
 * This is used for models that have horizontal sharding, with creates multiple tables based on a the key of a main
 * model which is used to differentiate the shards.
 *
 * This will change how tables are managed as a table will be created for each item, based on the shard ID for a
 * model. This will result in a different table name structure. sh_X_table_name
 *
 * Note: ALL your functions and function calls must use static::tableName() instead of hard-coded names OR even
 * $table_name
 *
 * The shard class and ID should be something that never moves or changes. Something top-level and core to the system
 * since the entire database will by split based on that value. Once split, there is no easy way to un-shard or
 * combine them. each table will have its own IDs.
 *
 * The model used as the primary must be an instance of TCm_ModelPrimaryHorizontalShard
 * @see TCm_ModelPrimaryHorizontalShard
 */
class TCm_ModelWithHorizontalSharding extends TCm_Model
{
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCm_ModelWithHorizontalSharding');
	
	/**
	 * Returns the current model that is used for sharding for this model.
	 * @return TCm_ModelPrimaryHorizontalShard
	 */
	public static function currentPrimaryShardModel() : TCm_ModelPrimaryHorizontalShard
	{
		$current_model = null;
		$model_name = static::shardPrimaryModelClassName();
		
		if(class_exists($model_name))
		{
			// Try and find the active model
			$current_model = TC_activeModelWithClassName($model_name);
			
			// If not, grab it from the session
			if(!$current_model && isset($_SESSION['current_shard_model_id']))
			{
				$current_model = $model_name::init($_SESSION['current_shard_model_id']);
			}
		}
		
		// No model found, can't risk the code run, hard exit
		if(!$current_model)
		{
			// Trigger the logout, rather than an error
			TC_website()->logout();
			header("Location: /");exit();
			//TC_triggerError('Shard table failure: Shard ID not found for table '.parent::tableName().". Expecting an instance of ".$model_name);
			
		}
		return $current_model;
	}
	
	/**
	 * Table name functions are replaced with horizontal sharding. It requires the class to implement the abstract
	 * methods that exist.
	 * @return string
	 */
	public static function tableName(): string
	{
		$current_model = static::currentPrimaryShardModel();
		$model_name = static::shardPrimaryModelClassName();
		
		$main_table_name = parent::tableName();
		
		if(is_string($main_table_name) && $main_table_name != '')
		{
			return static::shardPrefix() . $current_model->id() . '_' . parent::tableName();
		}
		
		
		TC_triggerError('Shard table failure: Shard ID not found for table '.parent::tableName().". Expecting an instance of ".$model_name);
		
		return '';
	}
	
	/**
	 * This function must return the name of the class that is used for sharding. The system is based on dividing the
	 * tables based on the ID of the model found, usually through active model checks. When used, the tables will
	 * exist for each instance found for that model.
	 * @return class-string<TCm_ModelPrimaryHorizontalShard>
	 */
	public static function shardPrimaryModelClassName() : string
	{
		return '';
	}
	
	/**
	 * Returns the shard primary model for this item. This is often done via a commonly named method
	 * @return TCm_ModelPrimaryHorizontalShard|null
	 */
	public function shardPrimaryModel() : ?TCm_ModelPrimaryHorizontalShard
	{
		return null;
	}
	
	/**
	 * The prefix used for the shards for this table.
	 * @return class-string<TCm_Model>
	 */
	public static function shardPrefix() : string
	{
		return 'sh_';
	}
	
	/**
	 * Returns the array of models that are used for install sharding. When doing installs, the system needs to
	 * iterate over all the models that can possibly exist to run it for each model.
	 * @return array
	 */
	public static function shardInstallModels() : array
	{
		return [];
	}
	
}