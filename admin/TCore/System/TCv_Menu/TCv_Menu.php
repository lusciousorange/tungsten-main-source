<?php
/**
 * Class TCv_Menu
 */
class TCv_Menu extends TCv_View
{
	protected ?array $url_sections = null;
	protected $current_folder_class = 'current_menu'; // [string] = A utility for marking menu items as the "current page"
	protected $active_folder_class = 'active_menu'; // [string] = A utility for marking menu items as the "current page"
	protected $current_folder_index = 1; // [int] = The index for which folder index in the url is the relevant folder
	protected $auto_detected_active_menus = true; // [bool] = Indicates if active menus should be auto-detected
	
	/**
	 * TCv_Menu constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		$this->setHTMLCollapsing(false);
		// parse URL folders for use in existing menu
		$this->url_sections = explode("/",$_SERVER['PHP_SELF']); // split the requested url
		
		$this->setTag('ul');
	}
	
	/**
	 * Returns the number of items in this list
	 * @return int
	 */
	public function numItems()
	{
		return sizeof($this->attached_views);
	}

	//////////////////////////////////////////////////////
	//
	// CURRENT FOLDER
	//
	//////////////////////////////////////////////////////


	/**
	 * Overrides the existing value for the classnames assigned to existing folders
	 * @param string $class_name
	 */
	public function setCurrentFolderClass($class_name)
	{
		$this->current_folder_class = htmlspecialchars($class_name);
	}
	
	/**
	 * Returns the folder class
	 * @return string
	 */
	public function currentFolderClass()
	{
		return $this->current_folder_class;
	}
	
	/**
	 * Sets the class name assigned to any folder that is active in the list of menu items leading to current menu
	 * @param string $class_name
	 */
	public function setActiveFolderClass($class_name)
	{
		$this->active_folder_class = htmlspecialchars($class_name);
	}



	//////////////////////////////////////////////////////
	//
	// MENU ITEMS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns a <li> item that is added to this menu
	 * @param $view
	 * @return TCv_View
	 */
	public function listItemForView($view)
	{
		// Try and give the li an ID when possible
		if($view->attributeID() != '')
		{
			$list_item = new TCv_View($view->attributeID().'_container');
		}
		else
		{
			$list_item = new TCv_View();
		}
		$list_item->setTag('li');
		
		// Pull out any sublists
		$child_lists = array();
		foreach($view->attachedViews() as $index => $child_view)
		{
			if($child_view instanceof TCv_View)
			{
				if($child_view->tag() == 'ul')
				{
					$child_lists[] = $child_view;
					$view->detachViewWithIndex($index);
				}
			}
			
		}
		// move any non-Class CSS items to the parent <li>
		foreach($view->classes() as $css_class)
		{
			if(!(substr($css_class,0,1) == 'T' && substr($css_class,3,1) == '_') && substr($css_class,0,10) != 'url-target')
			{
				$list_item->addClass($css_class);
				$view->removeClass($css_class);
			}
		}
		
		$list_item->attachView($view);
		foreach($child_lists as $child_list_item)
		{
			$list_item->attachView($child_list_item);
		}
	
		return $list_item;
	}
	
	/**
	 * Override of default class to ensure that items can have the tag set to li and that any properties can be properly defined for the menu items
	 * @param bool|TCv_View $view
	 * @param bool $prepend
	 *
	 * @return void
	 */
	public function attachView($view, $prepend = false)
	{
		if($view instanceof TCv_View)
		{
			// deal with current menu
			if($view instanceof TCv_Link && $view->url() != '')
			{
				if(substr($_SERVER['REQUEST_URI'],0, strlen($view->url()))  == $view->url())
				{
					$view->addClass($this->currentFolderClass());
				}
			}
			if($view->tag() != 'li')
			{
				$view = $this->listItemForView($view);
			}


			
		}
		else
		{
			TC_triggerError('Attempting to attach a view to a TCv_Menu item that is not a TCv_View instance. Attached views can be any TCv_View item including a TCv_View instance');
		}
	
		parent::attachView($view,$prepend);
	}
	
	
	/**
	 * This function turns off the auto-detect of menu items and requires that menus have their "active_menu" and "current_menu" classes manually set.
	 */
	public function manuallySetActiveMenus()
	{
		$this->auto_detected_active_menus = false;
	}


	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the current folder that the browser is in.
	 * @return mixed|string
	 */
	protected function currentFolder()
	{	
		$current_folder = implode('/',$this->url_sections);
		$current_folder = str_ireplace('index.php', '', $current_folder);
		return $current_folder;
	}

	/**
	 * Returns the html for the view
	 * @return string
	 */
	public function html()
	{	
		$this->addClass('menu_count_'.$this->numItems());
		return parent::html();
	}
	
	
	
		
		
		
}
?>