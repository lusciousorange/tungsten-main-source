<?php
use PHPUnit\Framework\TestCase;

/**
 * Extends the test case to deal with anything related to views in Tungsten
 * Class TCt_ViewTestCase
 */
class TC_ViewTestCase extends TestCase
{
	use TCt_DatabaseAccess;
	
	protected static $local_user;
	
	/** @var TCv_Form */
	protected $view;
	
	/** @var string $view_class_name */
	protected static $view_class_name;
	
	
	public static function setUpBeforeClass() : void
	{
		$test_class_name = get_called_class();
		static::setViewClassName(substr($test_class_name, 0, strlen($test_class_name) - 4));
		
		//	Cannot clear data. Tests run simultaneously and leads to race conditions
		//clearDataInTables();
	}
	
	/**
	 * @param string $class_name
	 */
	public static function setViewClassName($class_name)
	{
		static::$view_class_name = $class_name;
	}
	
	/**
	 * Returns a string of the class name for this view
	 * @return string|TCv_View
	 */
	public function viewClassName()
	{
		return static::$view_class_name;
	}
	
	/**
	 * Returns the local test user if it exists in the system. These values are set in the
	 * `TC_config_env ['test_user_values]` and provide a consistent approach to referencing the user who is running
	 * the tests. This helps ensure that functionality related to emailing, texting and otherwise communicating with
	 * a human is limited to the human running the test.
	 * @return TMm_User|bool
	 */
	public function localTestUser()
	{
		// Previously Set
		if(static::$local_user instanceof TMm_User)
		{
			return static::$local_user;
		}
		
		$user_values = TC_getConfig('test_user_values');
		if(is_array($user_values))
		{
			static::$local_user = TMm_User::createWithValues($user_values);
			return static::$local_user;
		}
		
		TC_triggerError('Local User not configured, you must set $TC_Config["test_user_values"] in TC_config_env.php');
	}
	
	
	/**
	 * A function to deletes an model from the database and ignores permissions. This is used when we need to clear
	 * away memory in a consistent fashion.
	 * @param TCm_Model $model
	 */
	protected function deleteModelIgnorePermissions($model)
	{
		if($model instanceof TCm_Model)
		{
			$query = "DELETE FROM `" . $model::tableName() . "` WHERE " . $model::$table_id_column . " = :id LIMIT 1";
			$this->DB_Prep_Exec($query, array('id' => $model->id()));
		}
		unset($model);
	}
	
	
	/**
	 * Extend the assertion to handle
	 * @param $value
	 * @param  $constraint
	 * @param string $message
	 * @return void
	 */
	public static function assertThat($value, $constraint, string $message = ''): void
	{
		$value_text = $value;
		if(is_bool($value))
		{
			$value_text = $value ? '<em>true</em>' : '<em>false</em>';
		}
		if(is_null($value))
		{
			$value_text = '<em>null</em>';
		}
		if($value instanceof TCm_Model)
		{
			$value_text = $value->contentCode();
		}
		
		
		
		$console_item = new TSm_ConsoleItem('Assert: '.$value_text .' '.$constraint->toString(),
		                                    TSm_ConsoleUnitTest);

//		self::$count += count($constraint);
//
//		$success = $constraint->evaluate($value, $message, true);
//
		try {
			parent::assertThat($value, $constraint, $message);
		}
		catch(Exception $e)
		{
			// Catch, track and re-throw
			$console_item->setIsError();
			$console_item->setDetails('<i class="fas fa-times"></i> : '.$message);
			$console_item->commit();
			throw $e;
		}
		
		$console_item->commit();
		
	}
	
	
}