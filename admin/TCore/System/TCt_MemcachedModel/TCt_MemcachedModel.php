<?php
trait TCt_MemcachedModel {
	/**
	 * Indicates if this model should cache queries from individual properties from tables. Extend this function for
	 * classes where models change infrequently and are shared by many users.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return false;
	}
	
	/**
	 * The key used to store the main query values for acquiring this model.
	 * @return string
	 */
	public function cacheKeyName(): string
	{
		$class_name = (get_called_class())::classNameForInit();
		return $class_name . '_' . $this->id();
	}
	
	/**
	 * The key used to store the main query values for acquiring this model.
	 * @return string
	 */
	public static function cacheAllKeyName(): string
	{
		$class_name = (get_called_class())::classNameForInit();
		return $class_name . '_all';
	}
	
	/**
	 * Clears the cache that relates to this model that might be found in Memcached.
	 * @return void
	 */
	public function clearCache(): void
	{
		TC_Memcached::delete($this->cacheKeyName());
		TC_Memcached::delete(static::cacheAllKeyName());
		foreach(static::cacheAdditionalClearCacheSuffixes() as $suffix)
		{
			TC_Memcached::delete($this->cacheKeyName().$suffix);
		}
	}
	
	/**
	 * Returns a list of suffixes for this model that should be called whenever we try to clear the cache for a particular
	 * model. If a model has a related cache that should be invalidated when the primary is invalidated,
	 * then you return those suffixes as an array. The prefix needs to be the cacheKeyName() and then whatever comes after it.
	 *
	 * Example: The TMm_User class will store the array of rows for user group matches. If a user is modified, that
	 * value should be cleared as well. The key is `$this->cacheKeyName().':user_group_rows'` so this function needs
	 * to return that second part `:user_group_rows` so that it gets cleared whenever this user is cleared.
	 * @return array
	 * @see TCm_Model::cacheKeyName()
	 */
	protected static function cacheAdditionalClearCacheSuffixes() : array
	{
		return [];
	}
	
}