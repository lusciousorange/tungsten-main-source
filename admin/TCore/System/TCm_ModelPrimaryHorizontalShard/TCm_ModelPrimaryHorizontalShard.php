<?php
/**
 * This model is used for horizontal sharding as the primary model that is used to separate the sharded table. The ID
 * for this model will be used to generate and install the shards if they exist. For example if you plan on splitting
 * up data based on the user, then the user class must extend this model.
 *
 * @see TCm_ModelWithHorizontalSharding
 */
class TCm_ModelPrimaryHorizontalShard extends TCm_Model
{
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCm_ModelPrimaryHorizontalShard');
	
	public function shardClasses(): array
	{
		$classes = [];
		$module_list = TSm_ModuleList::init();
		
		$modules = $module_list->modules();
		
		
		foreach($modules as $module)
		{
			$potential_classes = $module->classNames();
			foreach($potential_classes as $class_name)
			{
				if(str_starts_with($class_name, 'TMm') && is_subclass_of($class_name, 'TCm_ModelWithHorizontalSharding'))
				{
					$reflection = new ReflectionClass($class_name);
					if(!$reflection->isAbstract())
					{
						$classes[$class_name] = $class_name;
					}
				}
			}
		}
		
		return $classes;
	}
	
	/**
	 * Finds the models that are horizontally sharded based on this primary model. It will install the tables for
	 * that model.
	 * @return void
	 */
	public function installShardTables(): void
	{
		$this->setAsCurrentShardModel();
		
		$shard_classes = $this->shardClasses();
		
		$this->addConsoleDebugObject('Install '.count($shard_classes).' shard tables ', $this);
		$installer = new TSu_InstallProcessor('shard_installer');
		foreach($shard_classes as $class_name)
		{
			$installer->processModelClassName($class_name);
		}
		
		// Don't bother putting all of that in the console. We already said we were installing X shard tables
		TC_setConfig('console_saving',0);
		$installer->runTableInstalls();
		TC_setConfig('console_saving',1);
		
	}
	
	/**
	 * Finds all the tables for this shard and drops the tables related to them. This ALWAYS runs on the server that
	 * is being loaded. If you're using a distributed system, it will not reference the server where that data
	 * exists, instead attempt to load it on whatever server is running it.
	 *
	 * WARNING: This is likely to delete a LOT of data. Be very careful calling this
	 * @return void
	 */
	public function dropShardTables(): void
	{
		// Always use the current server for these actions
		$connection = TCm_Database::connection();
		$this->setAsCurrentShardModel();
		$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 0',[],[],$connection);
		$this->addConsoleDebug('dropShardTables');
		foreach($this->shardClasses() as $class_name)
		{
			$table_name = $class_name::tableName();
			if($table_name != '')
			{
				$this->DB_Prep_Exec('DROP TABLE IF EXISTS `' . $table_name . '`',[],[],$connection);
			}
		}
		
		$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 1',[],[],$connection);
		
		
	}
	
	
	/**
	 * Extend the creation method to detect if we have successfully created the model, so that we can run the table
	 * installers that are necessary for this shard primary model.
	 * @param array $values
	 * @param $bind_param_values
	 * @param $return_new_model
	 * @return void
	 */
	public static function createWithValues(array $values, $bind_param_values = array(), $return_new_model = true)
	{
		$new_primary_model = parent::createWithValues($values, $bind_param_values, true);
		if($new_primary_model instanceof TCm_ModelPrimaryHorizontalShard)
		{
			$new_primary_model->installShardTables();
		}
		
		return $new_primary_model;
	}
	
	/**
	 * After being initialized, save this as the primary model. In the vast majority of cases, we want this so it can
	 * process the items correctly for table names.
	 * @return void
	 */
	public function runAfterInit()
	{
		$this->setAsCurrentShardModel();
	}
	
	/**
	 * Sets this to be the current shard model
	 * @return void
	 */
	public function setAsCurrentShardModel(): void
	{
		TC_saveActiveModel($this);
		$_SESSION['current_shard_model_id'] = $this->id();
	}
	
	/**
	 * Returns the current shard model for this primary shard
	 * @return static|null
	 */
	public static function currentShardModel(): ?static
	{
		$current_model = TC_activeModelWithClassName(static::baseClass());
		if(!$current_model && isset($_SESSION['current_shard_model_id']))
		{
			$current_model = static::init($_SESSION['current_shard_model_id']);
		}
		
		return $current_model;
		
	}
}