<?php
/**
 * Class TCv_Messenger
 *
 * A messenger class that is used to display feedback to user.
 *
 */
class TCv_Messenger  extends TCv_View
{
	protected $title = false; // [string] = The main title for this messenger
	protected $items = array(); // [array] = The list of messages to be displayed on the console
	protected $show_messenger = false; // [bool] = Flag to indicate if the messenger has been used and should be shown when available.
	protected $failure_title = false;
	protected $success_title = false;
	protected $final_title = false;
	protected $use_hide_button = true;

	protected $is_success = true; // [bool] = Indicate if messenger is indicating a success
	
	/**
	 * TCv_Messenger constructor.
	 * @param bool|string $id
	 */
	public function __construct($id)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TCv_Messenger', true);
		$this->addClassJSFile('TCv_Messenger');
		$this->addClassJSInit('TCv_Messenger');
	}
	
	/**
	 * Updates the messenger with the provided values
	 * @param array $values
	 */
	public function updateValues($values)
	{
		foreach($values as $id => $value)
		{
			$this->$id = $value;
		}
		$this->updateSessionValues();
	}
	
	
	/**
	 * Turns off the "Hide" button which is styled to appear in the top right corner
	 */
	public function disableHideButton()
	{
		$this->use_hide_button = false;
		$this->updateSessionValues();
		
	}

	/**
	 * Sets the messenger to be failed. This is automatically done during messengers being added, but it can also be done manually.
	 */
	public function fail()
	{
		$this->is_success = false;
		$this->updateSessionValues();
		
	}
	
	/**
	 * Return if the messenger is failed.
	 *
	 * @return bool
	 */
	public function isFailed()
	{
		return $this->is_success === false;
	}
	
	/**
	 * Returns if this messenger is successful or not
	 *
	 * @return bool
	 */
	public function isSuccess()
	{
		return $this->is_success;
	}
	
	/**
	 * Adds a message to the messenger.
	 *
	 * The first message added for any given process will result in that being the main
	 * title. If additional messages are added, then they get added to the list of message notes. IF a message is added
	 * that indicates a failure, then the first one is set as the title and the previous title is added ot the message
	 * list. This ensures that we never show a message which indicates a success, but with the styling of a failure.
	 *
	 * @param string $text The message that will be shown.
	 * @param bool $successful (Optional) Default true.
	 */
	public function addMessage($text, $successful = true)
	{
		// title never set
		if($this->title === false)
		{
			$this->title = $text;
		}
		else // title is set
		{
			// failing for the first time
			if(!$successful && !$this->isFailed())
			{
				// add the text to the title, pushing the current one down
				$this->setTitle($text);
			}
			
			// add to item list
			// 1 - failing but previously failed
			// 2 - not failing but title set
			else 
			{
				$this->addItem($text);
			}
		}

		// turn on the messegner
		$this->show_messenger = true;
		
		// fail if set
		if(!$successful)
		{
			$this->fail();
		}
		$this->updateSessionValues();
	}
	
	/**
	 * Adds an item to the list of message after the title
	 *
	 * @param string $item The item to be added
	 */
	public function addItem($item)
	{
		$this->items[] = $item;
		$this->updateSessionValues();
		
	}
	
	/**
	 * Indicates if the messenger is meant to be shown
	 * @return bool
	 */
	public function showMessenger()
	{
		return $this->show_messenger;
	}
	
	/**
	 * Returns the array of message items objects
	 * @return array
	 */
	public function items()
	{
		return $this->items;
	}


	//////////////////////////////////////////////////////
	//
	// TITLES
	//
	// Methods related to the titles for messengers which
	// may change depending on if it was successful or not
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the title for this message, optionally pushing the current title into the item list
	 *
	 * @param string $title
	 * @param bool $push_to_item_list (Optional) Default true.
	 */
	public function setTitle($title, $push_to_item_list = true)
	{
		// title not set
		if($this->title === false)
		{
			$this->title = $title;
		}
		elseif($push_to_item_list) // title is set
		{
			$this->addItem($this->title);
			$this->title = $title;
		}
		$this->updateSessionValues();
		
	}
	
	
	/**
	 * Sets the title to be shown in the messenger if failed. This will override any other titles that might be set,
	 * pushing all messages (and title) in to the items section.
	 *
	 * @param string $title
	 */
	public function setFailureTitle($title)
	{
		$this->failure_title = $title;
		$this->show_messenger = true;
		$this->updateSessionValues();
		
	}
	
	/**
	 * Sets the title to be shown in the messenger if succeeded
	 * @param string $title
	 */
	public function setSuccessTitle($title)
	{
		$this->success_title = $title;
		$this->show_messenger = true;
		$this->updateSessionValues();
		
	}
	
	/**
	 * Sets the final title, which overrides all other titles that might happen
	 * @param string $title
	 */
	public function setFinalTitle($title)
	{
		$this->final_title = $title;
		$this->updateSessionValues();
		
	}
	
	/**
	 * This function will process all the settings to ensure the proper title is shown. If there are failure or success
	 * titles set, then those will push other messages and it will also deal with checked if it's failed.
	 */
	protected function determineFinalTitle()
	{
		if($this->final_title)
		{
			$this->setTitle($this->final_title);
		}
		elseif($this->failure_title && $this->isFailed())
		{
			$this->setTitle($this->failure_title);
		}
		
		elseif($this->success_title && $this->isSuccess())
		{
			$this->setTitle($this->success_title);
		}
		$this->updateSessionValues();
		
	}
	

	/**
	 * Returns the HTML for this view
	 * @return bool|string
	 */
	public function html()
	{
		if($this->showMessenger() || TC_numProcessErrors() > 0)
		{
			// Add in any process errors
			foreach(TC_processErrors() as $error_values)
			{
				$this->addMessage($error_values['message'],false);
			}
			TC_clearProcessErrors();
			
			$this->determineFinalTitle();
			
			// Blank title, get out
			if(trim($this->title) == '')
			{
				return false;
			}
			if($this->isSuccess())
			{
				$this->addClass('TCv_Messenger_success');
			}
			else
			{
				$this->addClass('TCv_Messenger_failure');
			}
		}
		else
		{
			$this->addClass('hidden');
		}
		
		$heading = new TCv_View();
		$heading->addClass('TCv_Messenger_title');

		if($this->use_hide_button)
		{
			//$hide_button = new TCv_ToggleTargetLink();
			$hide_button = new TCv_Link();
			$hide_button->setIconClassName('fa-times');
			$hide_button->addClass("hide_button");
			$hide_button->setURL('#');
			//$hide_button->addClassToggle('.TCv_Messenger', 'hidden');
			$hide_button->addHiddenTitle(TC_localize('close_button','Close'));
			$heading->attachView($hide_button);
		}

		$header_content = new TCv_View();
		$header_content->setTag('span');
		$header_content->addClass('title_text');
		$header_content->addText($this->title);
		$heading->attachView($header_content);
		$this->attachView($heading);
		
		if(sizeof($this->items()) > 0)
		{
			$ul = new TCv_View();
			$ul->setTag('ul');
			
			foreach($this->items() as $item)
			{
				$ul->addText('<li>'.$item.'</li>');
				
			}
			$this->attachView($ul);
			
		}
		else // No values, so these should be aria hidden
		{
			$this->setAttribute('aria-hidden','true');
		}
		
		
		$this->resetMessenger(); // regardless of showing, wipe it
		
		return parent::html();
		
		
		
		
		
	}
	
	/**
	 * Clears all the relevant information from the messenger
	 */
	public function resetMessenger()
	{
		unset($_SESSION['TCv_Messenger']);
		$this->title = false;
		$this->items = array();
		$this->show_messenger = false;
		$this->failure_title = false;
		$this->success_title = false;
		$this->use_hide_button = true;
		$this->is_success = true;
	}
	
	/**
	 * Returns an array of the default values for a messenger
	 * @return array
	 */
	public static function defaultValues()
	{
		$values = array();
		$values['title'] = false;
		$values['items'] = array();
		$values['show_messenger'] = false;
		$values['failure_title'] = false;
		$values['success_title'] = false;
		$values['use_hide_button'] = true;
		$values['is_success'] = true;
		
		return $values;
	}
	
	
	protected static function convertOldSessionValue()
	{
		// Check for old version where messenger is an object
		if(isset($_SESSION['TCv_Messenger']) && $_SESSION['TCv_Messenger'] instanceof  TCv_Messenger)
		{
			// Change the messenger to track values, rather than the full messenger
			$messenger = $_SESSION['TCv_Messenger'];
			$messenger->updateSessionValues();
		}
	}
	
	public function updateSessionValues()
	{
		$indices = array(
			'title',
			'items',
			'show_messenger',
			'failure_title',
			'success_title',
			'final_title',
			'use_hide_button',
			'is_success'
		);
		
		unset($_SESSION['TCv_Messenger']);
		
		// Update the session values
		foreach($indices as $index)
		{
			$_SESSION['TCv_Messenger'][$index] = $this->$index;
		}
		
		
	}
	
	
	/**
	 * The method to instantiate the messenger. The values are stored in a session and then values are updated
	 * whenever the messenger is changed. This class is a singleton so only
	 * @return TCv_Messenger
	 */
	public static function instance()
	{
		self::convertOldSessionValue();
		
		// Create the console, if already set then likely a processing script created it and is using it.
		if(!isset($_SESSION['TCv_Messenger']))
		{
			$_SESSION['TCv_Messenger'] = TCv_Messenger::defaultValues();
		}
		
		$messenger = new TCv_Messenger('TCv_Messenger');
		$messenger->updateValues($_SESSION['TCv_Messenger']);
		return $messenger;
		
	}
	
	
	
}
?>