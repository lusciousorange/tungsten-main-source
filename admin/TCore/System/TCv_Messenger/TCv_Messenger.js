class TCv_Messenger {
	constructor(element, options) {
		this.element = element;

		if(this.element)
		{
			this.element.querySelector('.hide_button').addEventListener('click', (e) => this.hideMessenger(e));
		}

	}

	/**
	 * Triggers the message to appear, which can be called from outside of the class as well.
	 *
	 * Called using `window.W.TCv_Messenger.showMessenger("Your Message", false);`
	 *
	 * @param {string} message
	 * @param {boolean} is_success
	 */
	showMessenger(message, is_success)
	{
		this.element.classList.remove("hidden");

		if(is_success)
		{
			this.element.classList.add('TCv_Messenger_success');
			this.element.classList.remove('TCv_Messenger_failure');
		}
		else
		{
			this.element.classList.remove('TCv_Messenger_success');
			this.element.classList.add('TCv_Messenger_failure');
		}
		let list = this.element.querySelector('ul');
		if(list)
		{
			list.remove();
		}

		this.element.querySelector('.title_text').innerHTML = message;

	}

	/**
	 * Hides the messenger
	 * @param e
	 */
	hideMessenger(e = null)
	{
		if(e !== null)
		{
			e.preventDefault();
		}
		this.element.classList.add('hidden');
	}
}