<?php

/**
 * Class TCm_ModelList
 *
 * An array of models tied to a specific TCm_Model class.
 *
 */
class TCm_ModelList extends TCm_Model
{
	/**
	 * @var array|bool $models The models saved for this list
	 * @var array $subsets The array of subsets that have been saved
	 * @var string $model_class_name The name of the class that is being listed
	 */
	protected $models = false;
	protected $subsets = array();
	
	/** @var string|TCm_Model $model_class_name The name of the model class  */
	protected $model_class_name;
	protected $all_items_query = false;
	protected $filters = array();
	
	// The base classes that should be used for avoiding extended classes
	protected static $base_class_names = array('TCm_ModelList');
	
	/**
	 * TCm_ModelList constructor.
	 *
	 * @param class-string<TCm_Model> $model_class_name THe name of the model class that is being listed
	 * @param bool $init_model_list (Optional) Default true. Indicates if the model list should be instantiated based
	 * on the model class.
	 */
	public function __construct($model_class_name, $init_model_list = true)
	{
		parent::__construct('model_list_'.$model_class_name, false);
		$this->model_class_name = $model_class_name;
		
		if($model_class_name != '')
		{
			
			$this->all_items_query = "SELECT * FROM `".($model_class_name::tableName())."`";
			if($model_class_name::$primary_table_sort !== false)
			{
				$this->all_items_query .= ' ORDER BY '.$model_class_name::$primary_table_sort;
			}
			
			// Init List
			if($init_model_list)
			{
				$this->models();
			}
			
		}
	}
	
	
	/**
	 * Returns the number of models. This performs a separate fast query
	 * @param array $filter_values The optional filter values that specify what to count. needed for filtered lists
	 * where the number of results should match what is being filtered.
	 * @return int
	 */
	public function modelCount(array $filter_values = []) : int
	{
		return static::modelCountForClassWithFilters($this->model_class_name, $filter_values);
	}
	
	/**
	 * This is a generalized function for counting models with specific filtering. This
	 * @param class-string<TCm_Model>|TCm_Model $class_name The name of the class that is being counted
	 * @param array $filter_values The filter values being passed. This is normally two values of db_query and db_params. However
	 * a third value of `primary_table_alias` can be passed if the table has been aliased
	 * @return int
	 */
	public static function modelCountForClassWithFilters( $class_name, array $filter_values = []) : int
	{
		if($class_name instanceof TCm_Model)
		{
			$class_name = $class_name->baseClassName();
		}
		
		$num_found = 0;
		
		$primary_table_alias = '';
		if(isset($filter_values['primary_table_alias']))
		{
			$primary_table_alias = $filter_values['primary_table_alias'].'.';
		}
		
		$queries = [];
		
		// CASE 1 : Get them all, simpler, no parsing necessary
		if(!isset($filter_values['db_query']))
		{
			// GET THEM ALL
			$query= "SELECT count(".$primary_table_alias.($class_name::$table_id_column).") as num_models
				FROM `".$class_name::tableName()."`";
			$result = $class_name::DB_RunQuery($query);
			$row = $result->fetch();
			$num_found += $row['num_models'];
		}
		
		// CASE 2 : db_query, so parsing and unions possibly required
		else
		{
			$params = $filter_values['db_params'];
			
			// Assume there are multiple queries, since a single one is just an array with one query
			// Easier to always loop, even it's just one
			$queries = $filter_values['db_query'];
			if(is_string($filter_values['db_query']))
			{
				// Generate an array
				$queries = [$filter_values['db_query']];
			}
			$count_rows = false;
			// Flags to check for mixed usage, which fails
			$group_by_found = false;
			$count_by_found = false;
			// LOOP THROUGH THE QUERIES
			foreach($queries as $index => $query)
			{
				$after_from = substr($query, strpos($query, 'FROM '));
				
				
				// DETECT IF THERE IS A GROUP, JUST GET ALL THE ROWS
				if(str_contains(strtoupper($query), 'GROUP BY'))
				{
					$count_rows = true;
					$group_by_found = true;
					// Just get the primary column value
					$queries[$index] = "SELECT " . $primary_table_alias . ($class_name::$table_id_column) . " " . $after_from;
				}
				else
				{
					$count_by_found = true;
					// Replace everything before the `FROM
					$queries[$index] = "SELECT count(" . $primary_table_alias . ($class_name::$table_id_column) . ") as num_models " . $after_from;
				}
				
			}
			
			// CHECK FOR ERROR In MIXED USE
			if($group_by_found && $count_by_found)
			{
				TC_triggerError('Mixed use of GROUP BY and not GROUP BY in union for model list filtering is not permitted');
				
			}
			
			// Called using $class_name and not static to ensure that it respects any functionality related to sharding
			// or distributed databases.
			// Combined queries
			$result = $class_name::DB_RunQuery(implode(' UNION ', $queries), $params);
			
			if($result->errorCode() != '00000')
			{
				TC_addToConsole('Error counting models. If you are using a TCv_SearchableModelList with a filter model (subset based
			on a provided model), consider extending `modelCount()` and changing the model name being passed in. ', TSm_ConsoleWarning, true);
			}
			
			
			if($count_rows)
			{
				$num_found+= $result->rowCount();
			}
			else
			{
				while($row = $result->fetch())
				{
					$num_found += $row['num_models'];
				}
			}
			
			
		}
		
		// Return the count in the end
		return $num_found;
	}
	
	
	/**
	 * Returns the model title for singular which is the plural of the models
	 * @return string
	 */
	public static function modelTitleSingular()
	{
		$called_class = get_called_class();
		
		// Get the classname without the list
		$model_class_name = substr($called_class,0,-4);
		if(class_exists($model_class_name))
		{
			$title = $model_class_name::modelTitlePlural();
			return TC_localize($title, $title);
		}
		
		return TC_localize($called_class,$called_class);
	}
	
	/**
	 * Sets the query string that represents all the items of this model type
	 * @param string $query
	 */
	public function setAllItemsQuery(string $query) : void
	{
		$this->all_items_query = $query;
		
		// reset the value
		$this->models = false;
		$this->models();
	}
	
	/**
	 * Returns the model class name. If the class is extended, then it returns the extended class name
	 * @return string
	 */
	public function modelClassName() : string
	{
		return TCm_Model::overrideClassName($this->model_class_name);
	}
	
	/**
	 * @param string|bool $subset_name (Ootional) Default false. A subset to be saved and possibly pulled out separately.
	 * @return TCm_Model[]
	 */
	public function models($subset_name = false)
	{
		if($subset_name)
		{
			return $this->subsets[$subset_name];
		}
		else
		{
			return $this->allItems();
		}
	}
	
	/**
	 * Returns all the models that are visible.
	 *
	 * This method will use the isVisible() method if it exists for the model. If it's not found, then all the models
	 * are returned.
	 *
	 * @return TCm_Model[]
	 */
	public function modelsVisible() : array
	{
		$models = [];
		if(method_exists($this->modelClassName(), 'isVisible'))
		{
			foreach($this->models() as $model)
			{
				if($model->isVisible())
				{
					$models[$model->id()] = $model;
				}
			}
		}
		else // Method doesn't exist, just return all the models and get out
		{
			return $this->models();
		}
		
		return $models;
	}
	
	/**
	 * A method that will acquire a list of models with the provided IDS. This method can be used to avoid unnecessary
	 * Database calls for large lists of models. Any number of IDs can be provided which will be formulated into a
	 * single MySQL query.
	 *
	 * @param array $ids An array of IDs that are wanted from within the model list
	 * @return array
	 */
	public function modelsWithIDs(array $ids) : array
	{
		if(sizeof($ids) == 0)
		{
			return array();
		}
		$models = array();
		$model_class_name = $this->model_class_name;
		$primary_id_column_name = $model_class_name::$table_id_column;
		
		
		$query = "SELECT * FROM `".($model_class_name::tableName()).'` WHERE ';
		$count = 1;
		$query_values = array();
		$query_string_pieces = array();
		foreach($ids as $id)
		{
			$query_values['id_'.$id] = $id;
			$query_string_pieces['id_'.$id] = $primary_id_column_name.'=:id_'.$id;
		}
		
		$query .= implode(' OR ', $query_string_pieces);
		
		if($model_class_name::$primary_table_sort !== false)
		{
			$query .= ' ORDER BY '.$model_class_name::$primary_table_sort;
		}
		
		$result = $this->DB_Prep_Exec($query, $query_values);
		while($row = $result->fetch())
		{
			$models[] = ($this->model_class_name)::init($row);
		}
		return $models;
	}
	
	/**
	 * Returns a single model with the given ID
	 * @param int $id The id that is wanted
	 * @return bool|TCm_Model
	 */
	public function modelWithID($id)
	{
		if($this->model_class_name != '')
		{
			return ($this->model_class_name)::init($id);
		}
		return '';
	}
	
	/**
	 * Model lists are singletons
	 * @return bool
	 */
	public static function isSingleton() : bool
	{
		return true;
	}
	
	
	
	/**
	 * Returns all the items in the list
	 * @return array
	 */
	public function allItems() : array
	{
		
		if($this->models === false)
		{
			$cache_name = null;
			if(static::cacheAllItemsFromTable())
			{
				$cache_name = ($this->model_class_name)::cacheAllKeyName();
			}
			$this->models = $this->modelsForQuery($this->all_items_query,
												  array(),
												  false,
												  $cache_name);
			
		}
		
		return $this->models;
	}
	
	/**
	 * Performs a query for a given subset of models using a query and a filter. This matches the standard PDO approach for queries.
	 *
	 * @param string $query THE query to be executed
	 * @param array $filters THe values for the query to be executed
	 * @param string|bool $subset_name (Optional) Default false. The subset where these models will belong to.
	 * @return array
	 *
	 * @uses TCt_DatabaseAccess::DB_Prep_Exec()
	 */
	protected function modelsForQuery(string $query,
									  array $filters,
											 $subset_name = false,
									  ?string $cache_key_name = null) : array
	{
		$model_list = array();
		if($query != '')
		{
			$model_class_name = $this->model_class_name;
			
			// Runs it on the static method, so that it respects DBs
			if(!class_exists($model_class_name))
			{
				$this->addConsoleError($model_class_name.' DOES NOT EXIST');
			}
			
			
			$rows = null;
			// Deal with memcached
			if($cache_key_name)
			{
				$rows = TC_Memcached::get($cache_key_name);
			}
			
			if(!is_array($rows))
			{
				$result = $model_class_name::DB_RunQuery($query, $filters);
				$rows = $result->fetchAll();
				
				// Save the cache if set
				if($cache_key_name)
				{
					TC_Memcached::set($cache_key_name, $rows);
				}
			}
			
			foreach($rows as $row)
			{
				$model = $model_class_name::init($row);
				if(!$model)
				{
					TC_triggerError('Model with class '.$model_class_name.' could not be initialized.');
				}
				$model_list[$model->id()] = $model;
			}
			
			if($subset_name)
			{
				$this->subsets[$subset_name] = $model_list;
			}
		}
		return $model_list;
	}
	
	/**
	 * Adds a single model to the existing list. This is commonly done during initialization fo the list or when new
	 * models are being craeted and the developer wants to maintain an accurate list.
	 *
	 * @param TCm_Model $model The model to be added
	 * @param string|bool $subset_name (Optional) Default false will add it to hte primary list
	 *
	 */
	public function addModel($model, $subset_name = false) : void
	{
		if($subset_name)
		{
			$this->subsets[$subset_name][$model->id()] = $model;
		}
		else
		{
			$this->models[$model->id()] = $model;
		}
	}
	
	/**
	 * Returns a list of values for a given method of the model class. Useful when needing to return an array of values
	 * for a given method amongst the full set or a subset.
	 *
	 * @param string $method The method name to be called
	 * @param string|bool $subset_name (Optional) Default false will search all objects. The subset name that the method should look in.
	 * @return array
	 */
	public function valuesForMethod($method, $subset_name = false) : array
	{
		$values = array();
		
		if($subset_name)
		{
			$model_list = $this->subsets[$subset_name];
		}
		else
		{
			$model_list = $this->allItems();
		}
		foreach($model_list as $model)
		{
			$values[$model->id()] = $model->$method();
		}
		return $values;
	}
	
	
	/**
	 * Processes a list of values to rearranges the list according to those values. By default this method will "just work"
	 * if the database table has a value called "display_order" and that all items are ordered together.
	 * @param array $values
	 */
	public function processRearrangedList(array $values) : void
	{
		$num_updated = 0;
		
		$class_name = $this->model_class_name;
		if($class_name == '')
		{
			$this->addConsoleError('Model class name not defined for '.$values['view_class']);
			return;
		}
		
		if(!class_exists($class_name))
		{
			$this->addConsoleError('Model class "'.$class_name.'" not found for  for '.$values['view_class']);
			return;
			
		}
		
		
		$primary_id = $class_name::$table_id_column;
		$query = "UPDATE `".$class_name::tableName()."` SET display_order = :display_order WHERE ".$primary_id." = :id";
		$statement = $this->DB_Prep($query);
		for($display_order = 1; $display_order <= $values['num_items']; $display_order++)
		{
			$plugin_id = $values['item_'.$display_order];
			$db_values = array('display_order' => $display_order, 'id' => $plugin_id);
			$this->DB_Exec($statement, $db_values);
			$num_updated++;
		}
		$this->addConsoleMessage('Rearranged '.$num_updated.' items');
		
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PROCESS FILTER LISTS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Processes the filter values and returns a PDOStatement
	 * @param array $filter_values
	 * @return PDOStatement
	 */
	public function processFilterList($filter_values)
	{
		$model_name = $this->modelClassName();
		$query = "SELECT * FROM  `".$model_name::tableName()."`";
		
		if(!str_contains(strtoupper($query),'ORDER BY'))
		{
			$query .= "ORDER BY " . $model_name::$primary_table_sort;
		}
		
		$result = $this->DB_Prep_Exec($query);
		return $result;
		
	}
	
	/**
	 * Standard function which takes filter values and returns models according to the model list settings
	 * @param array $filter_values
	 * @return array|TCm_Model[]
	 */
	public function processFilterListAsModels($filter_values)
	{
		$items = array();
		$result = $this->processFilterList($filter_values);
		
		// Returned PDO statement
		if($result instanceof PDOStatement)
		{
			while($row = $result->fetch())
			{
				$model = ($this->modelClassName())::init($row);
				
				$items[$model->id()] = $model;
			}
			
			return $items;
		}
		
		// NEW METHOD
		// Convert into an array and get the models
		if(is_array($result) && isset($result['db_query']) && isset($result['db_params']))
		{
			$query = $result['db_query'];
			
			// Avoid double order by
			if(!str_contains(strtoupper($query),'ORDER BY'))
			{
				$model_class_name = $this->modelClassName();
				$primary_sort = $model_class_name::$primary_table_sort;
				if($primary_sort != '')
				{
					$query .= " ORDER BY " . $primary_sort;
				}
			}
			
			return $this->getModelsForFilteredQuery($this->model_class_name,
													$query,
													$result['db_params'],
													$filter_values);
		}
		
		// Already have models in an array
		
		
		// Already have models
		if(is_array($result))
		{
			return $result;
		}
		
		
		return $items;
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// TIME BREAKDOWNS
	//
	// Methods related to doing a time grouping of models
	// into ranges that can be processed for easy graphing
	//
	//////////////////////////////////////////////////////
	
	
	
	/**
	 * Returns an hourly breakdown of when items are created based on the provided properties. By default, calling
	 * this method will return the breakdown based on the date_added with any item that has that hour in the date
	 * will be grouped together
	 *
	 * @param int $start_hour (Optional) Default 0. The first hour, 0 for midnight based on the 24 hour clock
	 * @param bool $where_clause (Optional) Default false. The where clause that is part of the query
	 * @param array $query_params (Optional) Default array(). The array of values to be passed
	 * @param string $date_field (Optional) Default 'date_added' The field that the dates should be checked against
	 * @return int[] An array of counts for each hour with the indices being the date and hour in a format such as
	 * "2017-03-12 17" where 17 equates  to any item item that matched 17:00:00 to 17:59:59.
	 */
	public function hourlyBreakdown($start_hour = 0, $where_clause = false, $query_params = array(), $date_field = 'date_added')
	{
		/** @var TCm_Model|string $model_class_name */
		$model_class_name = $this->model_class_name;
		$table_name = $model_class_name::tableName();
		$primary_id_column_name = $model_class_name::$table_id_column;
		
		if($start_hour == '')
		{
			$start_hour = 0;
		}
		
		// STEP 1 : Get the values grouped by the proper date
		$query =
			"SELECT 
				HOUR($date_field) breakdown_hour, 
				DATE($date_field) breakdown_date, 
				COUNT($primary_id_column_name) as num_items
			FROM $table_name ";
		
		if($where_clause)
		{
			$query .= "	WHERE " . $where_clause;
		}
		
		$query .= "	GROUP BY HOUR($date_field), DATE($date_field)
			ORDER BY $date_field ASC";
		
		$result = $this->DB_Prep_Exec($query, $query_params);
		$rows = $result->fetchAll();
		
		// STEP 2 : Go back to the start hour to ensure a complete graph
		$first_hour = $rows[0]['breakdown_hour'];
		
		// Require the previous day
		if($first_hour < $start_hour)
		{
			$date = new DateTime($rows[0]['breakdown_date'] . ' 12:00:00');
			$date->modify("-1 Day");
			
			
			$new_values = array(
				'breakdown_hour' => $start_hour,
				'breakdown_date' => $date->format("Y-m-d"),
				'num_items' => 0
			);
			array_unshift($rows, $new_values);
		}
		elseif($first_hour > $start_hour) // same day
		{
			$new_values = array(
				'breakdown_hour' => $start_hour,
				'breakdown_date' => $rows[0]['breakdown_date'],
				'num_items' => 0
			);
			array_unshift($rows, $new_values);
			
		}
		
		// STEP 3 : Jump ahead to the next start hour to ensure a complete graph
		$last_hour = $rows[sizeof($rows) - 1]['breakdown_hour'];
		
		$validation_start_hour = $start_hour;
		
		// Avoid a -1 scenario
		if($validation_start_hour == 0)
		{
			$validation_start_hour = 24;
		}
		
		
		if($last_hour > $validation_start_hour-1) // Need to go ahead by one day
		{
			$date = new DateTime($rows[sizeof($rows) - 1]['breakdown_date'].' 12:00:00');
			$date->modify("+1 Day");
			
			
			$new_values = array(
				'breakdown_hour' => $validation_start_hour-1, // one hour before again
				'breakdown_date' => $date->format("Y-m-d"),
				'num_items' => 0
			);
			array_push($rows, $new_values);
		}
		elseif($last_hour < $validation_start_hour) // same day
		{
			$new_values = array(
				'breakdown_hour' => $validation_start_hour-1,
				'breakdown_date' => $rows[0]['breakdown_date'],
				'num_items' => 0
			);
			array_push($rows, $new_values);
			
		}
		
		// STEP 4 : Fill in the blanks to generate the complete hourly list
		
		$hourly_values = array();
		
		$current_hour = $rows[0]['breakdown_hour'];
		$previous_day = false;
		foreach($rows as $row)
		{
			// we've switched days
			if($row['breakdown_hour'] < $current_hour)
			{
				// Fill in the missing hours for the previous day
				
				for($i = $current_hour; $i< 24; $i++)
				{
					$hourly_values[$previous_day.' '.$i] = 0;
				}
				$current_hour =0;
			}
			while($current_hour < $row['breakdown_hour'])
			{
				$hourly_values[$row['breakdown_date'].' '.$current_hour] = 0;
				$current_hour++;
				if($current_hour == 24)
				{
					$current_hour = 0;
				}
				
			}
			$hourly_values[$row['breakdown_date'].' '.$row['breakdown_hour']] = $row['num_items'];
			$current_hour++;
			if($current_hour == 24)
			{
				$current_hour = 0;
			}
			
			$previous_day = $row['breakdown_date'];
		}
		
		return $hourly_values;
		
		
	}
	
	/**
	 * Returns a 10-minute breakdown of when items are created based on the provided properties. By default, calling
	 * this method will return the breakdown based on the date_added with any item that has that hour in the date
	 * will be grouped together
	 *
	 * @param int $start_hour (Optional) Default 0. The first hour, 0 for midnight based on the 24 hour clock
	 * @param bool $where_clause (Optional) Default false. The where clause that is part of the query
	 * @param array $query_params (Optional) Default array(). The array of values to be passed
	 * @param string $date_field (Optional) Default 'date_added' The field that the dates should be checked against
	 * @return int[] An array of counts for each hour with the indices being the date and hour in a format such as
	 * "2017-03-12 17" where 17 equates  to any item item that matched 17:00:00 to 17:59:59.
	 */
	public function tenMinuteBreakdown($start_hour = 0, $where_clause = false, $query_params = array(), $date_field =
	'date_added')
	{
		/** @var TCm_Model|string $model_class_name */
		$model_class_name = $this->model_class_name;
		$table_name = $model_class_name::tableName();
		$primary_id_column_name = $model_class_name::$table_id_column;
		
		if($start_hour == '')
		{
			$start_hour = 0;
		}
		
		// STEP 1 : Get the values grouped by the proper date
		// Returns dates as HH:m0 with the tens always being zero
		$query =
			"SELECT 
				CONCAT(SUBSTR($date_field , 12 , 4),'0') breakdown_ten_minute,  
				DATE($date_field) breakdown_date, 
				COUNT($primary_id_column_name) as num_items
			FROM $table_name ";
		
		if($where_clause)
		{
			$query .= "	WHERE " . $where_clause;
		}
		
		$query .= "	GROUP BY SUBSTR(date_added , 12 , 4), DATE($date_field)
			ORDER BY $date_field ASC";
		
		$result = $this->DB_Prep_Exec($query, $query_params);
		$rows = $result->fetchAll();
		
		if(count($rows) == 0)
		{
			return array();
		}
		
		// STEP 2 : Go back to the start hour to ensure a complete graph
		list($first_hour, $first_ten) = explode(':',$rows[0]['breakdown_ten_minute']);
		
		$start_hour = str_pad($start_hour, 2,'0', STR_PAD_LEFT);
		
		// Require the previous day
		if($first_hour < $start_hour)
		{
			$date = new DateTime($rows[0]['breakdown_date'] . ' 12:00:00');
			$date->modify("-1 Day");
			
			
			$new_values = array(
				'breakdown_ten_minute' => $start_hour.':00',
				'breakdown_date' => $date->format("Y-m-d"),
				'num_items' => 0
			);
			array_unshift($rows, $new_values);
		}
		elseif($first_hour > $start_hour) // same day
		{
			$new_values = array(
				'breakdown_ten_minute' => $start_hour.':00',
				'breakdown_date' => $rows[0]['breakdown_date'],
				'num_items' => 0
			);
			array_unshift($rows, $new_values);
			
		}
		
		// STEP 3 : Jump ahead to the next start hour to ensure a complete graph
		$parts = explode(':', $rows[sizeof($rows) - 1]['breakdown_ten_minute']);
		$last_hour = $parts[0];
		
		$validation_start_hour = $start_hour;
		
		// Avoid a -1 scenario
		if($validation_start_hour == 0)
		{
			$validation_start_hour = 24;
		}
		
		$last_date = new DateTime($rows[sizeof($rows) - 1]['breakdown_date'].' 12:00:00');
		
		if($last_hour > $validation_start_hour-1) // Need to go ahead by one day
		{
			$last_date->modify("+1 Day");
			
			
			$new_values = array(
				'breakdown_ten_minute' => str_pad($validation_start_hour-1,2,'0',STR_PAD_LEFT).':50', // one hour before again
				'breakdown_date' => $last_date->format("Y-m-d"),
				'num_items' => 0
			);
			array_push($rows, $new_values);
		}
		elseif($last_hour < $validation_start_hour) // same day
		{
			$new_values = array(
				'breakdown_ten_minute' => str_pad($validation_start_hour-1,2,'0',STR_PAD_LEFT).':50',
				'breakdown_date' => $last_date->format("Y-m-d"),
				'num_items' => 0
			);
			array_push($rows, $new_values);
			
		}
		
		
		// STEP 4 : Fill in the blanks to generate the complete hourly list
		// Track everything in hour.ten to allow for easier comparison
		$hour_ten_values = array();
		
		$current_time = new DateTime($rows[0]['breakdown_date'].' '.$rows[0]['breakdown_ten_minute'].':00');
		$format = 'Y-m-d H:i';
		
		$previous_day = false;
		
		foreach($rows as $row_index => $row)
		{
			$row_date_ten = $row['breakdown_date'].' '.$row['breakdown_ten_minute'];
			
			// Check for more than one-day disparity
			// Possibly jump ahead to avoid filling in multiple empty areas
			if($row['breakdown_date'] != $current_time->format('Y-m-d'))
			{
				$row_date = new DateTime($row_date_ten . ':00'); // create date object of this date
				$interval = $current_time->diff($row_date); // find the interval
				if($interval->days > 1) // more than 24 hours, skip ahead
				{
					// Jump to the start of that day, avoid long gaps
					$current_time = new DateTime($row['breakdown_date'] . ' 00:00:00');
				}
			}
			// we've switched days, count up to 12:5
			if($row_date_ten < $current_time->format($format))
			{
				// Loop while we're in the previous day
				while($current_time->format('Y-m-d') != $row['breakdown_date'])
				{
					$hour_ten_values[$previous_day.' '.$current_time->format('H:i')] = 0;
					$current_time->modify('+10 minutes');
				}
			}
			
			// Same Day Fill Gaps
			// Loop through any missing items now that we're certainly on the same day
			while($current_time->format($format) < $row_date_ten)
			{
				$hour_ten_values[$current_time->format($format)] = 0;
				$current_time->modify('+10 minutes');
				
			}
			
			// Add the current one
			$hour_ten_values[$row_date_ten] = $row['num_items'];
			$current_time->modify('+10 minutes'); // move the equivalent count up as well
			$previous_day = $row['breakdown_date'];
		}
		
		
		return $hour_ten_values;
		
		
	}
	
	/**
	 * @param int[] $breakdown An array of integers with each index being a date and hour, almost always acquired
	 * from hourlyBreakdown().
	 * @param int $start_hour
	 * @return array[][] A 2D array where the first index is the date and the second index is the time with the
	 * values being the count for that time
	 * @see TCm_ModelList::hourlyBreakdown()
	 */
	public function hourlyBreakdownIntoDays($breakdown, $start_hour = 0)
	{
		$this->addConsoleDebug('--- hourlyBreakdownIntoDays()');
		
		$first_item = array_values($breakdown)[0];
		$items_per_day = array();
		$day = substr($first_item,0,10); // start with the first day
		foreach($breakdown as $index => $count)
		{
			if(substr($index, 11) + 0 == $start_hour)
			{
				$day = substr($index, 0, 10);
			}
			$date = new DateTime($index . ':00:00');
			$items_per_day[$day][$date->format('ga')] = $count;
		}
		
		
		return $items_per_day;
	}
	
	
	/**
	 * @param int[] $breakdown An array of integers with each index being a date and hour, almost always acquired
	 * from hourlyBreakdown().
	 * @param int $start_hour
	 * @return array[][] A 2D array where the first index is the date and the second index is the time with the
	 * values being the count for that time
	 * @see TCm_ModelList::hourlyBreakdown()
	 */
	public function tenMinuteBreakdownIntoDays($breakdown, $start_hour = 0)
	{
		$this->addConsoleDebug('--- tenMinuteBreakdownIntoDays()');
		$first_item = array_values($breakdown)[0];
		$items_per_day = array();
		$day = substr($first_item,0,10); // start with the first day
		foreach($breakdown as $index => $count)
		{
			if(substr($index, 11,2) + 0 == $start_hour)
			{
				$day = substr($index, 0, 10);
			}
			$date = new DateTime($index . ':00');
			$items_per_day[$day][$date->format('g:ia')] = $count;
		}
		
		
		return $items_per_day;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return false;
	}
	
	
	
}
?>