<?php
class TC_FormWithModelTestCase extends TC_ViewTestCase
{
	/**
	 * Generates the form with a given model and saves to to the form value in this test
	 * @param TCm_Model|string $model
	 * @return TCv_FormWithModel
	 */
	public function resetFormWithModel($model)
	{
		unset($_SESSION['TCm_FormTracker']); // unset any form trackers
		$form_name = static::$view_class_name;
		return new $form_name($model);
	}
	
	/**
	 * Generates the form with a given model and saves to to the form value in this test. This also calls the html()
	 * method to render the form.
	 * @param TCm_Model|string $model
	 * @return TCv_FormWithModel
	 */
	public function resetAndRenderFormWithModel($model)
	{
		$form = $this->resetFormWithModel($model);
		$form->html();
		return $form;
	}
	
	/**
	 * Processes the form in this tester with the provided set of values. This will mimic the functionality of the
	 * form, set the post values and run the processor.
	 * @param TCv_FormWithModel $form
	 * @param array $values
	 * @return TCc_FormProcessorWithModel
	 */
	public function processFormWithValues($form, $values)
	{
		//print "\n---processFormWithValues()";
		
		// Generate the Form, Renders, Full action to mimic real rendering
		// Only bother if we haven't done it yet
		// Tracker Form Name only gets added via the html() method, which indicates if it ran already
		if(!$form->formItemWithID('tracker_form_name'))
		{
			$form->html();
		}
		
		
		
		// Override any values for the form that we want to test with
		foreach($values as $form_id => $form_value)
		{
			//	print "\n".$form_id.' = '.$form_value;
			$form->formItemWithID($form_id)->setSavedValue($form_value);
		}
		
		// Reprocess the Form Tracker to update the values
		$form->saveFormTrackerValues();
		
		// Save values to post
		//	print "\n---saving to post";
		$_POST = array();
		foreach($form->formItems() as $form_item)
		{
			if($form_item instanceof TCv_FormItem)
			{
				$value = $form_item->currentValue();
				//	print "\n".$form_item->id();
				//	print " = ".$value;
				$_POST[$form_item->id()] = $value;
				
			}
		}
		
		$form_processor = TCc_FormProcessorWithModel::init();
		$form_processor->run(false);
		
		if(!$form_processor->isValid())
		{
			$messenger = TCv_Messenger::instance();
			foreach($messenger->items() as $message)
			{
				print "\n".strip_tags($message);
			}
		}
		return $form_processor;
	}
	
	/**
	 * Tests if this form is showing the submit button. Fails if it is
	 * @param TCv_FormWithModel $form
	 */
	protected function assertFormNotShowingSubmit($form)
	{
		// Ensure we've hid the submit button
		$this->assertFalse(
			$form->hasSubmitButton(),
			"Submit Button Not Set");
	}
	
	/**
	 * Tests if this form is showing the submit button. Fails if it is
	 * @param TCv_FormWithModel $form
	 */
	protected function assertFormShowingSubmit($form)
	{
		// Ensure we've hid the submit button
		$this->assertTrue(
			$form->hasSubmitButton(),
			"Submit Button should be visible");
	}
	
	/**
	 * Tests that the form does NOT have a model set
	 * @param TCv_FormWithModel $form
	 */
	protected function assertFormModelNotSet($form)
	{
		$this->assertFalse(
			$form->modelName(),
			'Model Name should not be set');
		
	}
	
	/**
	 * Tests that the form does NOT have a model set
	 * @param TCv_FormWithModel $form
	 */
	protected function assertFormModelSet($form)
	{
		$this->assertTrue(
			$form->modelName(),
			'Model Name should be set');
		
	}
	
}