<?php
/**
 * Class TCt_DatabaseAccess
 *
 * A trait that handles various method related to connecting to databases via Tungsten. Some methods are wrappers on
 * existing PDO functionality, while others add new features like console entries for DB queries.
 *
 * This trait has both static and instance methods for accessing databases.
 */
trait TCt_DatabaseAccess
{
	public static ?TCm_Database $database_connection = null;
	
	////////////////////////////////////////////////
	//
	// STATIC METHODS
	// These are newer methods that add more flexibility
	// by allowing for multiple connections without
	// needing an instance.
	//
	////////////////////////////////////////////////
	
	/**
	 * The static connection for this model
	 * @return TCm_Database|null
	 */
	public static function DB_Connection() : ?TCm_Database
	{
		if(is_null(static::$database_connection))
		{
			static::$database_connection = TCm_Database::connection();
		}
		
		return static::$database_connection;
	}
	
	/**
	 * Prepares query for being run on the database
	 * @param string $query The query to prepare
	 * @param TCm_Database|null $database_connection The connection to use. If not set, then it uses DB_Connection()
	 * @return PDOStatement
	 */
	public static function DB_Prepare(string $query, ?TCm_Database $database_connection = null) : PDOStatement
	{
		if(is_null($database_connection))
		{
			$database_connection = static::DB_Connection();
		}
		return $database_connection->prepare($query);
	}
	
	/**
	 * @param PDOStatement $statement The PDO statement to be executed
	 * @param array|null $values The array of values to be passed, null indicates no values
	 * @param array $bind_param_values The array of binding parameter values
	 * @param TCm_Database|null $database_connection The connection to use. If not set, then it uses DB_Connection()
	 * @return PDOStatement
	 */
	public static function DB_Execute(PDOStatement $statement,
	                                  ?array $values = null,
	                                  array $bind_param_values = [],
	                                  ?TCm_Database $database_connection = null) : PDOStatement
	{
		if(is_null($database_connection))
		{
			$database_connection = static::DB_Connection();
		}
		
		
		$exception = false;
		$start_time = microtime(true); // start timer
		try
		{
			
			if(is_array($values))
			{
				$bind_values = array(); // make an array to save bind parameters so they don't get overwritten
				foreach($values as $index => $value)
				{
					if(is_null($value))
					{
						// Must pass in the null directly
						$statement->bindValue($index, null, PDO::PARAM_NULL);
					}
					elseif(isset($bind_param_values[$index]))
					{
						$bind_values[$index] = $value;
						$param_data_type = $bind_param_values[$index];
						$statement->bindParam(':'.$index, $bind_values[$index], $param_data_type);
					}
					else
					{
						if(is_array($value))
						{
							$value = implode(',', $value);
						}
						$statement->bindValue($index, $value);
					}
					
					
				}
			}
			
			$statement->execute();
			
			
		}
		catch (Exception $e)
		{
			$exception = $e;
		}
		
		
		if($statement->errorCode() != '00000')
		{
			$error_info = $statement->errorInfo();
			$database_connection->setHasError($error_info[2]);
			TC_trackProcessError($error_info[0].' '.$error_info[2],'db_error');
		}
		
		$end_time = microtime(true);
		
		$db_action_name = strtoupper(substr($statement->queryString, 0, 6));
		
		if($db_action_name == 'INSERT')
		{
			$database_connection->setInsertedID($database_connection->lastInsertID());
		}
		
		
		// DEBUG LOGGING
		// Checks that we're logging them AND that we have access to the console
		if(method_exists(get_called_class(), 'addConsoleItem'))
		{
			$query = $statement->queryString;
			$type = static::consoleTypeForQuery($query);
			
			$console_item = new TSm_ConsoleItem(htmlspecialchars(static::compositeQueryWithValues($query, $values)), $type);
			$console_item->setClassName(get_called_class());
			if($database_connection->transactionID())
			{
				$console_item->setDBTransactionID($database_connection->transactionID());
			}
			
			// track time spent
			$console_item->setTimeSpent($end_time - $start_time);
			$details = '';
			if($exception)
			{
				$console_item->setIsError();
				$details = $exception->getMessage();
				$result = false;
			}
			else
			{
				$rows = $statement->rowCount();
				$details ="Succeeded • ".$rows." row".($rows==1 ? '' : 's')." affected • ".number_format(($end_time - $start_time)*1000, 1).'ms';
			}
			$details .= "<br />Database: ".$database_connection->databaseName();
			
			$console_item->setDetails($details);
			$console_item->commit();
			
		}
		
		// If we are in the middle of a transaction, then we should re-throw the exception so that it can be caught and dealt with
		if($database_connection->inTransaction() && $exception)
		{
			$database_connection->rollback($exception->getMessage());
			//throw $exception;
		}
		
		// ONLY RETURNS ON NO EXCEPTION
		return $statement;
	}
	
	/**
	 * Runs a query for this item this combines the two actions of prepare and execute into a single function that is
	 * easy to call anywhere int he systeem
	 * @param string $query The query to prepare
	 * @param array|null $values The array of values to be passed, null indicates no values
	 * @param array $bind_param_values The array of binding parameter values
	 * @param TCm_Database|null $database_connection The connection to use. If not set, then it uses DB_Connection()
	 * @return PDOStatement
	 */
	public static function DB_RunQuery(string $query,
	                                   ?array $values = null,
	                                   array $bind_param_values = [],
	                                   ?TCm_Database $database_connection = null) : PDOStatement
	{
		$statement = static::DB_Prepare($query,$database_connection);
		
		return static::DB_Execute($statement, $values, $bind_param_values,$database_connection);
	}
	
	
	////////////////////////////////////////////////
	//
	// INSTANCE METHODS
	// These are the original methods and commonly used
	// however they now use the static calls internally
	//
	////////////////////////////////////////////////
	
	/**
	 * Returns the connection to the database
	 * @return null|TCm_Database
	 * @deprecated Use the static
	 */
	public function DB() : ?TCm_Database
	{
		return static::DB_Connection();
	}
	
	/**
	 * A wrapper method that will prepare a statement using the existing database.
	 *
	 * @param $query
	 * @return PDOStatement
	 *
	 * @uses TCm_Database
	 */
	public function DB_Prep($query) : PDOStatement
	{
		return static::DB_Prepare($query);
	}
	
	
	/**
	 * A wrapper method that executes a PDO statement, handling DB errors and Console entries.
	 * @param PDOStatement $statement
	 * @param array|bool $values The values to be passed into the PDO
	 * @param array $bind_param_values Any binding parameters for the query
	 * @return PDOStatement
	 *
	 * @uses PDOStatement
	 * @uses TSm_ConsoleItem
	 */
	public function DB_Exec(PDOStatement $statement, $values = false, $bind_param_values = array())
	{
		return static::DB_Execute($statement, $values, $bind_param_values);
	}
	
	/**
	 * A wrapper and convenience method that will execute a query on the existing database.
	 * @param string $query The query to be executed
	 * @param array|null $values
	 * @param array $bind_param_values
	 * @param ?TCm_Database $connection
	 * @return PDOStatement
	 *
	 * @uses TCt_DatabaseAccess::DB_Prep()
	 * @uses TCt_DatabaseAccess::DB_Exec()
	 */
	public function DB_Prep_Exec($query, $values = null, $bind_param_values = [], ?TCm_Database $connection = null)
	{
		
		return static::DB_RunQuery($query, $values, $bind_param_values, $connection);
		
	}
	
	////////////////////////////////////////////////
	//
	// UTILITIES
	//
	////////////////////////////////////////////////
	
	/**
	 * Parses a query with values to return a human-readable string with values inserted where `:placeholders` are found.
	 * @param string $query The query
	 * @param array $values The array of values
	 * @return string
	 */
	protected static function compositeQueryWithValues($query, $values)
	{
		if(is_array($values))
		{
			foreach($values as $name => $value)
			{
				$name = str_ireplace(':', '', $name); // strip the colon from in front
				
				if(is_null($value))
				{
					//$value = 'null';
				}
				elseif(is_array($value))
				{
					$value = implode(',', $value);
				}
				elseif(is_bool($value))
				{
					$value = $value ? '1' : '0';
				}
				// Find any non-string, non-int and don't try and show the value
				elseif(!is_string($value) && !is_int($value) && !is_float($value))
				{
					$value = 'BLOB';
				}
				
				// Separate check for null values
				if(is_null($value))
				{
					$slashed_value = 'null';
				}
				else
				{
					$slashed_value = "'".addslashes($value)."'";
				}
				
				// Replace items that have :value which includes specific things that come after them
				// avoids finding :value_with_more
				$search_params = array(
					':'.$name.' '   ,
					':'.$name.','  ,
					':'.$name."\n"  ,
					':'.$name.")"
				);
				$replace_params = array(
					$slashed_value." "  ,
					$slashed_value.","  ,
					$slashed_value." "  ,
					$slashed_value.")"
				);
				$query = str_ireplace($search_params, $replace_params, $query);
				
				if($name == substr($query, (-1 * strlen($name) )))
				{
					$query = str_ireplace(':'.$name.'', $slashed_value, $query);
				}
			}
			
		}
		return $query;
		
	}
	
	/**
	 * Returns a TSm_ConsoleQuery type for the given query
	 *
	 * @param string $query
	 * @return int
	 *
	 * @see TSm_ConsoleItem
	 */
	public static function consoleTypeForQuery($query)
	{
		// get the constants
		if(!class_exists('TSm_ConsoleItem') )
		{
			return 0;
		}
		
		$type = TSm_ConsoleMessage;
		$query = str_ireplace('(', '', $query);
		$type_string = strtoupper(substr(trim($query),0, 6));
		if($type_string == 'INSERT')
		{
			$type = TSm_ConsoleQuery_Insert;
		}
		elseif($type_string == 'UPDATE')
		{
			$type = TSm_ConsoleQuery_Update;
		}
		elseif($type_string == 'SELECT')
		{
			$type = TSm_ConsoleQuery_Select;
		}
		elseif($type_string == 'DELETE')
		{
			$type = TSm_ConsoleQuery_Delete;
		}
		return $type;
		
	}
	
	/**
	 * Indicates if queries should be logged in the console
	 * @param bool $log
	 * @deprecated  This no longer does anything.
	 */
	public function setLogQueriesInConsole($log = true)
	{
	
	}
	
	
	
	/**
	 * Returns the primary key for the given table
	 * @param string $table
	 * @return string
	 */
	public function primaryKeyForTable($table)
	{
		if(isset($_SESSION['t_keys'][$table]))
		{
			return $_SESSION['t_keys'][$table];
		}
		$query = "SHOW INDEX FROM `$table` WHERE Key_name = 'PRIMARY'";
		$result = $this->DB_Prep_Exec($query);
		if($row = $result->fetch())
		{
			$_SESSION['t_keys'][$table] = $row['Column_name'];
		}
		
		return $_SESSION['t_keys'][$table];
		
	}
	
	/**
	 * A function that helps compose a MySQL query using commonly provided values. This function isn't "all-encompassing",
	 * nor is it meant ot replace the ability to write queries. It is a shortcut for the most frequent needs. All
	 * properties are provided as arrays and if the arrays aren't empty, then it concatenates them appropriately.
	 *
	 * @param string $selects The single string with all the table joins.
	 * @param string $tables The single string with all the table joins.
	 * @param array $where_clauses Array of items that are included as "where". They are joined with AND
	 * @param array $having_clauses Array of parameters for "having". Joined with AND
	 * @param array $order_clauses Array of `Order By` items
	 * @return string
	 */
	protected function composeMySQLSelectQueryWithValues($selects, $tables, $where_clauses, $having_clauses, $order_clauses)
	{
		$query = "SELECT ".implode(', ', $selects)." FROM ".$tables;
		
		
		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
		}
		
		if(sizeof($having_clauses) > 0)
		{
			$query .= " HAVING ".implode(' AND ', $having_clauses);
		}
		
		if(sizeof($order_clauses) > 0)
		{
			$query .= " ORDER BY ".implode(', ', $order_clauses);
		}
		
		
		return $query;
	}
	
	
}
?>