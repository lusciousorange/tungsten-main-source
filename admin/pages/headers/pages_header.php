<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_config.php");


	//////////////////////////////////////////////////////
	//
	// SETUP THEME
	//
	// Grab the theme from the pages settings
	// Instantiate it and if it's not installed, use Tungsten
	//
	//////////////////////////////////////////////////////
	
	$pages_module = TMm_PagesModule::init();
	$theme_class_name = $pages_module->themeName();
	
	if(is_null($theme_class_name))
	{
		print 'Website disabled or no site theme as been set';
		exit();
	}
	
	if(isset($skip_tungsten_authentication) && $skip_tungsten_authentication)
	{
		$theme_class_name::$skip_authentication = true;
	}
	
	/** @var TMv_PagesTheme $website */
	$website = $theme_class_name::website();
	$website->renderPageView(); // call the rendering afterwards. some calls to TC_website() might happen
	
	
	//////////////////////////////////////////////////////
	//
	// INSTALL CHECK
	//
	// Check if Tungsten is installed. If not we need to redirect
	// Pages should never be attempted to be loaded until the install is complete
	//
	//////////////////////////////////////////////////////


	if(!$website->installComplete())
	{
		header("Location: /admin/install/");
		exit();
	}

	//////////////////////////////////////////////////////
	//
	// PROCESS THE MENU ITEM
	//
	//////////////////////////////////////////////////////
	if($website instanceof TMv_PagesTheme)
	{
		$website->performMenuItemActions();
	}
	
		
?>