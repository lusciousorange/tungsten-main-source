<?php
class TMm_Pages301RedirectList extends TCm_ModelList
{
	/**
	 * TMm_Pages301RedirectList constructor.
	 * @param bool $init_model_list (Optional) Default true.
	 */
	public function __construct($init_model_list = false)
	{
		parent::__construct('TMm_Pages301Redirect',$init_model_list);
		
	}
	
	/**
	 * @param bool $subset_name
	 * @return TMm_Pages301Redirect[]
	 */
	public function models ($subset_name = false)
	{
		return parent::models($subset_name);
	}
	
	/**
	 * Returns the Redirect URL model for the current URL. This functionality ignores domains
	 * @return bool|TMm_Pages301Redirect
	 */
	public function findRedirectForCurrentURL()
	{
		$cleaned_request_URL = TMm_Pages301Redirect::cleanSourceURL($_SERVER['REQUEST_URI']);
		
		$query = "SELECT * FROM `page_301_redirects` WHERE source_url_cleaned = :source LIMIT 1";
		$result = $this->DB_Prep_Exec($query, array('source' => $cleaned_request_URL));
		if($row = $result->fetch())
		{
			return TMm_Pages301Redirect::init($row);
		}
		
		return false;
	}
}