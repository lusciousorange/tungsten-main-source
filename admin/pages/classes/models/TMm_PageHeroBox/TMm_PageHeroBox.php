<?php
/**
 * Class TMm_PageHeroBox
 *
 * A hero box for a given menu item
 */
class TMm_PageHeroBox extends TCm_Model
{
	protected $title, $description, $is_visible, $display_order, $button_text, $target, $redirect_to_menu,
		$redirect_url, $url_append, $menu_id, $image_filename, $background_position;


	public static $table_name = 'pages_hero_boxes'; // [string] = The id column for this class
	public static $table_id_column = 'hero_box_id'; // [string] = The id column for this class
	public static $model_title = 'Hero box';
	public static $model_title_plural = 'Hero boxes';
	
	
	/**
	 * TMm_PageHeroBox constructor.
	 * @param array|int $hero_box_id
	 */
	public function __construct($hero_box_id)
	{
		parent::__construct($hero_box_id);

	}

	/**
	 * Returns the upload folder for the view
	 * @return string
	 */
	public static function uploadFolder()
	{
		return $_SERVER['DOCUMENT_ROOT'].'/assets/pages/';
	}
	

	/**
	 * Returns the title
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Returns the description for this item
	 * @return string
	 */
	public function description()
	{
		return $this->description;
	}

	/**
	 * Returns the target for this item
	 * @return string
	 */
	public function target()
	{
		return $this->target;
	}

	/**
	 * Returns the button text for this item
	 * @return string
	 */
	public function buttonText()
	{
		return $this->button_text;
	}

	/**
	 * Returns the menu_id for the content view
	 * @return int
	 */
	public function menuID()
	{
		return $this->menu_id;
	}

	/**
	 * Returns the redirect menu_id for the content view
	 * @return int
	 */
	public function redirectToMenuID()
	{
		return $this->redirect_to_menu;
	}

	/**
	 * Returns the redirect url for the content view
	 * @return int
	 */
	public function redirectToURL()
	{
		return $this->redirect_url;
	}


	
	/**
	 * Returns the menu item
	 * @return bool|TMm_PagesMenuItem
	 */
	public function menuItem()
	{
		return TMm_PagesMenuItem::init( $this->menu_id);
	}
	
	public function backgroundPosition()
	{
		return $this->background_position;
	}
	
	/**
	 * Returns if the item is visible
	 * @return bool
	 */
	public function isVisible()
	{
		return $this->is_visible;
	}

	/**
	 * Toggles the visibility for this item
	 */
	public function toggleVisibility()
	{
		$this->updateDatabaseValue('is_visible', !$this->isVisible());
	}

	/**
	 * Updates the display order for this content id
	 * @param int $display_order
	 * @param int $container_id
	 */
	public function updateDisplayOrder($display_order)
	{
		$query = "UPDATE pages_hero_boxes SET display_order = :display_order WHERE hero_box_id = :hero_box_id ";
		$result = $this->DB_Prep_Exec($query, array('hero_box_id'=>$this->id(), 'display_order'=>$display_order));
		$this->display_order = $display_order;
	}

	/**
	 * Returns the background photo file for this viewed menu.
	 *
	 * This method will return the closest file it can find in the hierarchy of hte menu item.
	 * @return false|TCm_File
	 */
	public function photoFile() : false|TCm_File
	{
		$file = false;
		if($this->image_filename != '')
		{
			$file = new TCm_File(false, $this->image_filename, static::uploadFolder(), TCm_File::PATH_IS_FROM_SERVER_ROOT);
		}


		// Handle the preferred width from the settings
		if($file)
		{
			$file->setCropOutputWidth(TC_getModuleConfig('pages', 'image_crop_width'));
		}

		return $file;

	}
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title displayed for hero box',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'is_visible' => [
					'title'         => 'Visible',
					'comment'       => 'Indicates if the box is visible',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'show_button' => [
					'title'         => 'Show button',
					'comment'       => 'Indicates if the button should be shown',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'display_order' => [
					'title'         => 'Display Order',
					'comment'       => 'The order in which they are displayed',
					'type'          => 'smallint(5) unsigned',
					'nullable'      => false,
				],
				'image_filename' => [
					'title'         => 'Image filename',
					'comment'       => 'The filename of the image',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'description' => [
					'title'         => 'Description',
					'comment'       => 'The description of the image',
					'type'          => 'mediumtext',
					'nullable'      => false,
				],
				'button_text' => [
					'title'         => 'Button text',
					'comment'       => 'The button text',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'target' => [
					'title'         => 'Target',
					'comment'       => 'The target indicating if it opens in a new window',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'redirect_to_menu' => [
					'title'         => 'Redirect to menu',
					'comment'       => 'The value for redirecting, which can include a menu ID',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
				],
				'redirect_url' => [
					'title'         => 'Redirect URL',
					'comment'       => 'The value for redirecting this page',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'url_append' => [
					'title'         => 'URL Append',
					'comment'       => 'Text that is appended to the URL',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'background_position' => [
					'title'         => 'URL Append',
					'comment'       => 'Text that is appended to the URL',
					'type'          => 'varchar(64) DEFAULT "top center"',
					'nullable'      => false,
				],
				
				'menu_id' => [
					'title'         => 'Menu ID',
					'comment'       => 'The ID for the page menu that this hero box belongs to',
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => false,
					
					// If the page is deleted, we delete this hero box too
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesMenuItem',
						'delete'        => 'CASCADE'
					],
				],
			];
	}

}

?>