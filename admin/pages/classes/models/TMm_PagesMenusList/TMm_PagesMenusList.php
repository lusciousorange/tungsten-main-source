<?php

/**
 * Legacy class for the list of page menus. This was renamed to `TMm_PagesMenuItemList which follows the standard
 * Tungsten format. It's possible that old classes extended the older TMm_PagesMenusList, so we maintain it for
 * backward compatibility.
 */
class TMm_PagesMenusList extends TMm_PagesMenuItemList
{
	
	/**
	 * TMm_PagesMenusList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		
	}
	
	
}

