<?php
/**
 * Class TMm_PageContentList
 *
 * A model list of all the page content items
 */
class TMm_PageContentList extends TCm_ModelList
{
	protected $page_content_items_found = array();
	protected $hidden_views = array();
	
	/**
	 * TMm_PageContentList constructor.
	 * @param bool $init_model_list (Optional) Default true.
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_PagesContent',$init_model_list);
		
		$this->processAvailableContentOptions();
		$this->processHiddenViews();
	}
	
	
	/**
	 * Internal function to process the content options for the website
	 */
	protected function processAvailableContentOptions()
	{
		// the root of the admin
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$d = dir($admin_root);
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			// Process the pages content folder for any content views that should be loaded
			$class_folder_path = $admin_root.$module_folder.'/classes/views/';
			$this->processModuleFolder($class_folder_path, $module_folder);
		
		}
				
		
	}

	/**
	 * Processes a particular module folder for available content items
	 * @param string $class_folder_path
	 * @param string $module_folder
	 */
	protected function processModuleFolder($class_folder_path, $module_folder)
	{
		// check if it is a directory and skip . and ..
		if(is_dir($class_folder_path) && $module_folder != '.' && $module_folder != '..')
		{
			$class_overrides = TC_getConfig('class_overrides');
			if(is_null($class_overrides))
			{
				$class_overrides = [];
			}
			// if here then we are looking at /admin/<module>/
			$plugin_folder = dir($class_folder_path);
			while($plugin_file = $plugin_folder->read())
			{
				if($plugin_file != '.' && $plugin_file != '..') // ignore the two return links
				{
					// found a file or folder, only save it if it is a class
					$view_class_name = str_ireplace('.php','',$plugin_file);
					if(class_exists($view_class_name))
					{
						$traits = class_uses($view_class_name);

						// Only bother with classes that use TMt_PageContentView AND aren't content layouts
						if(isset($traits['TMt_PagesContentView']) && !is_subclass_of($view_class_name,'TMv_ContentLayout'))
						{
							// Check if the class is an override
							// in those cases show the original
							// class is an override of an existing class
							// don't show it since the overriden one will appar and

							if(!in_array($view_class_name, $class_overrides))
							{
								if(!isset($this->page_content_items_found[$view_class_name]))
								{
									$this->page_content_items_found[$view_class_name] = $view_class_name;
								}
								else // two found with the same name.
								{
									// DUE TO LOADING, THIS NEVER ACTUALLY GETS SHOWN :(
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Returns the page content options
	 * @return array
	 */
	public function pageContentOptions()
	{
		return $this->page_content_items_found;
	}
	
	/**
	 * Clears any hidden views from the page content views hidden table
	 */
	public function clearHiddenViews()
	{
		$query = "DELETE FROM pages_content_views_hidden ";
		$result = $this->DB_Prep_Exec($query);
		
	}
	
	/**
	 * Adds a hidden view to the list
	 * @param string $class_name
	 */
	public function addHiddenView($class_name)
	{
		TMm_PagesContentViewHidden::createWithValues(['class_name' => $class_name]);
	}
	
	/**
	 * Adds a hidden view to the list
	 * @param string $class_name
	 */
	public function removeHiddenView($class_name)
	{
		$query = "DELETE FROM pages_content_views_hidden WHERE class_name = :class_name ";
		$this->DB_Prep_Exec($query, array('class_name' => $class_name));
		
	}
	
	/**
	 * Finds the hidden views and saves them to the model
	 */
	protected function processHiddenViews()
	{
		$query = "SELECT * FROM `pages_content_views_hidden` ";
		$result = $this->DB_Prep_Exec($query);
		while($row = $result->fetch())
		{
			$this->hidden_views[$row['class_name']] = $row['class_name'];
		}
	}
	

	/**
	 * Returns the list of hidden view class names
	 * @return string[]
	 */
	public function hiddenViews()
	{
		return $this->hidden_views;
	}
	
	/**
	 * Returns if the provided class name is a hidden view
	 * @param string $class_name
	 * @return bool
	 */
	public function viewIsHidden($class_name)
	{
		return isset($this->hidden_views[$class_name]);
	}
	
	
	/**
	 * Returns all the view classes that exist in the page menus including those that might not have an actual view
	 * yet. The return value is an array with two properties 'view_class' and 'menu_ids'. The menu_ids are
	 * comma-separated list of IDs.
	 * @return array
	 */
	public function viewClassesInPageContent()
	{
		$query = "SELECT view_class, GROUP_CONCAT(DISTINCT menu_id) as menu_ids
		FROM pages_content GROUP BY view_class ORDER BY menu_id";
		$result = $this->DB_Prep_Exec($query);
		$classes = array();
		while($row = $result->fetch())
		{
			$classes[$row['view_class']] = $row['menu_ids'];
		}
		
		return $classes;
	}
	
}

?>