<?php
/**
 * Class TMm_PagesContent
 *
 * A page content item which represents a piece of content, content-block or layout in the page view.
 */
class TMm_PagesContent extends TCm_Model
{
	use TMt_PageRenderItem;
	use TSt_ModelWithHistory {
		TMt_PageRenderItem::historyChangeAdjustments insteadof TSt_ModelWithHistory;
		TMt_PageRenderItem::historyHeadingAdjustments insteadof TSt_ModelWithHistory;
		
	}
	
	protected int $content_id;
	protected string $module;
	
	protected $content_title; // [string] = the title assigned
	protected $menu_id; // [int] = The menu_id that this content item is related to
	protected $icon = false;
	protected $indent_level = false;
	protected $visibility_condition = '';
	protected bool $equal_height_columns;
	
	protected $container_content = false;
	
	public static $table_name = 'pages_content'; // [string] = The id column for this class
	public static $table_id_column = 'content_id'; // [string] = The id column for this class
	public static $model_title = 'Page content';
	public static ?string $mirror_table_suffix = '_unpub';
	
	
	/**
	 * TMm_PagesContent constructor.
	 * @param array|int $content_id
	 */
	public function __construct($content_id)
	{
		parent::__construct($content_id);
		$this->setPropertiesFromTable('pages_content');
	}

	/**
	 * Returns the upload folder for the view
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		return $_SERVER['DOCUMENT_ROOT'].'/assets/pages/';
	}
	
	
	/**
	 * Extend the renderer functionality to return the page menu item instead
	 * @return null|TMm_PagesMenuItem
	 */
	public function renderer() :?TMm_PagesMenuItem
	{
		return $this->menuItem();
	}
	
	
	public static function variablesModelClass() : string
	{
		return 'TMm_PagesContentVariable';
	}
	
	/**
	 * Returns the title
	 * @return string
	 */
	public function title() : string
	{
		return $this->content_title;
	}
	
	
	
	/**
	 * Returns the view class for this content view
	 * @return string
	 */
	public function viewClass() : string
	{
		return $this->view_class;
	}
	
	/**
	 * Returns the menu_id for the content view
	 * @return int
	 */
	public function menuID() : int
	{
		return $this->menu_id;
	}
	
	/**
	 * Returns the menu item
	 * @return null|TMm_PagesMenuItem
	 */
	public function menuItem() :?TMm_PagesMenuItem
	{
		return TMm_PagesMenuItem::init( $this->menu_id);
	}

	public function visibilityCondition(): string
	{
		return $this->visibility_condition;
	}

	

	/**
	 * Returns if the content item is visible
	 * @return bool
	 */
	public function isVisible() : bool
	{

		$condition = $this->visibilityCondition();
		if($condition != '')
		{
			$text = new TCu_Text($condition);

			return $text->evaluateTemplateConditionAsBoolean();
		}

		return $this->is_visible;
	}
	
	
	/**
	 * A method to change the menu ID for this item, which recursively updates the menu IDs for the items contained
	 * inside of it.
	 * @param int $menu_id
	 */
	public function changeMenuID($menu_id) : void
	{
		// Find the position layout order and update it to the last of the new layout
		if($this->isLayoutSection())
		{
			$new_menu = TMm_PagesMenuItem::init($menu_id);
			$new_position = count($new_menu->mainLevelContentItems()) +1;
			
			$overrides['position_layout'] = $new_position;
			$this->updateDatabaseValue('position_layout', $new_position);
			
		}
		
		// Update the menu ID for this item
		$this->updateDatabaseValue('menu_id', $menu_id);
		
		// Recursively update the children menu IDs
		foreach($this->containerContent() as $content)
		{
			$content->changeMenuID($menu_id);
		}
	}
	
	
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	// Content flagging is commonly done per-model, but
	// pages are always more complicated due to how they
	// are built and managed
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A hook method to implement custom content flags that are unique to this model. These will be included with the
	 * normal flags for the item
	 * @return array
	 */
	public function customContentFlags() : array
	{
		// Shortcut to avoid processing if not setup
		if(!TC_getConfig('use_content_flagging'))
		{
			return [];
		}
		
		/** @var string|TMt_PagesContentView $view_class */
		$view_class = $this->viewClass();
		if(class_exists($view_class))
		{
			$flags = $view_class::flagsForPageContent($this);
			if(!is_array($flags))
			{
				$this->addConsoleError('Response from flagsForPageContent() is not an array');
				return [];
			}
		}
		else
		{
			$flags  = [];
			$flags[] = new TMm_ContentFlag('missing-content-view-'.$view_class,
			                               'Missing Content View<br /><em style="font-size:0.7em;">'.$view_class.'</em>');
		}
		return $flags;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES – TSt_ModelWithHistory
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of related models that are associated with this model for history purposes. This is usually
	 * used to show the complete history of changes to a model, by pulling in history from other related models.
	 * For example, changing the user groups for a TMm_User shows up in the user's history. News categories are
	 * another example.
	 * @return array[]
	 */
	public static function historyRelatedModels() : array
	{
		return [
			[
				'model_name' => 'TMm_PagesContentVariable',
				'reference_column_name' => 'content_id',
				'heading_display_column_id'     => 'variable_name',
			],
		];
	}
	
	
	
	




	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'content_title' => [
					'title'         => 'Content title',
					'comment'       => 'The title for the content',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'menu_id' => [
					'title'         => 'Menu ID',
					'comment'       => 'The ID for the page menu that this content belongs to',
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => false,
					
					// If the page is deleted, we delete this hero box too
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesMenuItem',
						'delete'        => 'CASCADE'
					],
				],
			
				'module' => [
					'title'         => 'Module',
					'comment'       => 'The module for the content',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
			
				
				// Columns to be purposely deleted
				'description' =>    [
					'delete'        => true
				],
				// Found in older systems, breaks things
				'user_id' => [
					'delete'         => true
				],
			
			];
	
	}
	
	public static function installUpdateQueries()
	{
		return [
			'6.0.0' => [
				// Correct font-awesome icons
				"UPDATE pages_content_variables SET value = 'fab fa-twitter' WHERE value = 'fa-twitter' ",
				"UPDATE pages_content_variables SET value = 'fab fa-instagram' WHERE value = 'fa-instagram' ",
				"UPDATE pages_content_variables SET value = 'fab fa-facebook' WHERE value = 'fa-facebook' ",
				"UPDATE pages_content_variables SET value = 'fab fa-facebook-square' WHERE value = 'fa-facebook-square' ",
				"UPDATE pages_content_variables SET value = 'fab fa-facebook-square' WHERE value = 'fa-facebook-square' ",
				
				// Update items that reference page template values to use the new setup
				// Join table with itself, find the items that match the name and value, then update the values
				"UPDATE pages_content_variables a INNER JOIN pages_content_variables b USING(content_id)
				SET b.value = 'page_template_value', a.variable_name = 'page_template_value', a.value = REPLACE(REPLACE(a.value,'{{pages_template_setting.',''),'}}','')
				WHERE a.match_id != b.match_id AND a.variable_name = 'redirect_url' AND a.value LIKE '{{pages%' AND b.variable_name = 'target' AND b.value = 'another_website';"
			],
		
		];
	}
	
	
}

?>