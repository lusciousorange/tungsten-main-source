<?php

/**
 * A class to represent a navigation menu in the system. Each navigation menu is a representation of a series of
 * pages on the site and how they are presented.
 */
class TMm_PagesNavigationType extends TCm_Model
{
	protected int $navigation_id;
	
	protected string $title = '';
	protected string $short_code = '';
	protected bool $is_flat = false;
	
	protected bool $is_limited_to_viewed_menu;
	
	public static $table_name = 'pages_navigation_types';
	public static $table_id_column = 'navigation_id';
	public static $model_title = 'Navigation type';
	
	protected ?array $pages = null;
	
	public function title() : string
	{
		return $this->title;
	}
	
	public function shortCode() : string
	{
		return $this->short_code;
	}
	
	/**
	 * Returns if this navigation type renders as flat, so no nesting.
	 * @return bool
	 */
	public function isFlat() : bool
	{
		return $this->is_flat;
	}
	
	/**
	 * Returns the pages for this navigation type.
	 * @return TMm_PagesMenuItem[]
	 */
	public function pages() : array
	{
		if(is_null($this->pages))
		{
			$this->pages = [];
			
			// Get just the menus we need, based on the flat display order
			if($this->is_flat)
			{
				$query = "SELECT p.* FROM pages_navigation_matches m INNER JOIN pages_menus p USING(menu_id) WHERE navigation_id = :navigation_id ORDER BY m.display_order ASC";
				$result = $this->DB_Prep_Exec($query,['navigation_id' => $this->id()]);
				while($row = $result->fetch())
				{
					$page = TMm_PagesMenuItem::init($row);
					$this->pages[$page->id()] = $page;
				}
				
				
			}
			else // nested, need the full menu list to understand
			{
				$page_menus = TMm_PagesMenuItemList::init();
				foreach($page_menus->items() as $page)
				{
					if($page->matchesNavigationTypes([$this]))
					{
						$this->pages[$page->id()] = $page;
					}
				}
			}
		}
		
		return $this->pages;
	}
	
	/**
	 * Extend the functionality but stop anyone from deleting the main menus.
	 * @param $user
	 * @return bool
	 */
	public function userCanDelete($user = false) : bool
	{
		if($this->id() == 1)
		{
			return false;
		}
		
		return parent::userCanDelete($user);
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema()
			+ [
				
				'title' => [
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'short_code' => [
					'comment'       => 'The code used to represent this navigation item. Used in IDs for views.',
					'type'          => 'varchar(16)',
					'nullable'      => false,
				],
				'is_flat' => [
					'comment'       => 'Indicates if this navigation renders flat and ignores any hierarchy',
					'type'          => 'bool',
					'nullable'      => false,
				],
				'is_limited_to_viewed_menu' => [
					'comment'       => 'Indicates this navigation is limited to the viewed menu children',
					'type'          => 'bool',
					'nullable'      => false,
				],
			
			];
		
	}
	
	
}