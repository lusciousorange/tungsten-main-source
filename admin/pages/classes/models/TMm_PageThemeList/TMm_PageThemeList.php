<?php
/**
 * Class TMm_PageThemeList
 *
 * The list of all the page content available for the website
 */
class TMm_PageThemeList extends TCm_Model
{
	protected $themes = array();
	
	/**
	 * TMm_PageThemeList constructor.
	 */
	public function __construct()
	{
		parent::__construct('TMm_PageThemeList');
		
		$this->processAvailableThemes();
	}
	
	/**
	 * Processes all available themes
	 */
	protected function processAvailableThemes()
	{
		// the root of the admin
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$d = dir($admin_root);
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			// Process the pages content folder for any content views that should be loaded
			$class_folder_path = $admin_root.$module_folder.'/classes/themes/';
			$this->processModuleFolder($class_folder_path, $module_folder);
		
		}
				
		
	}

	/**
	 * processes a content folder from a single module
	 *
	 * @param string $class_folder_path
	 * @param string $module_folder
	 */
	protected function processModuleFolder($class_folder_path, $module_folder)
	{
		// check if it is a directory and skip . and ..
		if(is_dir($class_folder_path) && $module_folder != '.' && $module_folder != '..')
		{
			// if here then we are looking at /admin/<module>/
			$plugin_folder = dir($class_folder_path);
			while($plugin_file = $plugin_folder->read())
			{
				if($plugin_file != '.' && $plugin_file != '..') // ignore the two return links
				{
					// found a file or folder, only save it if it is a class
					$view_class_name = str_ireplace('.php','',$plugin_file);
					if(class_exists($view_class_name))
					{
						if(is_subclass_of($view_class_name, 'TMv_PagesTheme'))
						{
							$path_from_site_root = str_ireplace($_SERVER['DOCUMENT_ROOT'], '', $class_folder_path).'/';
							$this->themes[$view_class_name] = $path_from_site_root;
						
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Returns the list of possible page themes
	 * @return TMv_PagesTheme[]
	 */
	public function availableThemes()
	{
		return $this->themes;
	}
	
	
	
		
}

?>