<?php

/**
 * Class TMm_PagesMenuItem
 *
 * Represents a single menu in the system which is commonly referred to as a page.
 */
class TMm_PagesMenuItem extends TCm_Model
{
	use TMt_PageRenderer;
	use TMt_WorkflowItem;
	use TMt_Page301Redirectable;
	use TMt_Searchable;
	use TSt_ModelWithHistory;

	protected int $menu_id;
	
	protected ?array $children = null;
	protected $title = false; // [string] = The title for this menu
	protected $title_original = false;
	protected string $meta_title = '';
	protected $parent_id = -1; // [int] = The parent menu id for this menu item
	protected $content_entry = false; // [string] = Indicates how the content is managed for this menu item
	protected $redirect = ''; // [string] = The location where the redirect occurs if necessary
	protected $is_current = false; // [bool] = Indicates if this menu item is one of the current menu items
	protected $ancestors = false;
	protected $folder = false;
	protected $title_dynamic_class = false;
	protected $photo_filename = '';
	protected $page_photo_file = null; // the file for hte page, no active models considered
	protected ?string $photo_alt_text = '';
	
	// The photo file. Null indicates it's never been calculated. False means we don't have one
	protected null|false|TCm_File $photo_file = null ; // tracks the TCm_File for this page


	protected $title_override, $content_visible, $requires_authentication, $redirect_to_menu,
		$redirect_class_name, $redirect_class_target, $redirect_class_method, $photo_type, $photo_position;
	protected $model_class_name_title_display = null;
	
	protected $is_visible; // Setting type on this breaks things
	protected bool $is_visible_on_sitemap = false;
	// Deprecated value that is now calculated rather than saved
	protected bool $is_active = false;
	
	protected ?array $hero_boxes = null;
	protected $model_list_class_name = '';
	protected $visibility_condition = '';
	protected $is_visible_conditional = null;
	protected $is_content_visible_computed = null;
	protected $model_class_name = '';
	protected $model_class_name_primary_view_page = '';
	
	protected int $display_order = 0;
	
	protected $content_visibility_condition = '';
	protected $is_model_list = 0;

	protected $user_specific_access = array();
	
	protected $meta_description;
	protected ?string $description_dead = null;// exists sometimes in Dbs
	protected $parent;
	
	protected $import_code;
	protected $title_suffix = '';
	
	
	
	protected ?array $navigation_types = null;
	
	public static $table_name = 'pages_menus';
	public static $table_id_column = 'menu_id';
	public static $model_title = 'Page';
	public static $primary_table_sort = "display_order ASC";
	
	/**
	 * These are the different modes that a model can be assigned to on the public website. By default there is only
	 * `view` but the next most common one would be `edit`. Others can be defined per-site.
	 * @var string[]
	 */
	public static $public_page_model_modes = array(
		'view' => 'View – This is the primary page to view these models.'
	);
	
	/**
	 * TMm_PagesMenuItem constructor.
	 * @param array|int $menu_id
	 */
	public function __construct($menu_id)
	{
		parent::__construct($menu_id, false); // don't load from the table, need zero ID
		
		if(is_array($menu_id))
		{
			$this->convertArrayToProperties($menu_id);
		}
		elseif($this->id > 0)
		{
			$this->setPropertiesFromTable('pages_menus');
			$this->title_original = $this->title;
		}
		else
		{
			$this->id = 0;
		}
		
	}

	/**
	 * Returns the upload folder
	 * @return string
	 */
	public static function uploadFolder()
	{
		return $_SERVER['DOCUMENT_ROOT'].'/assets/pages/';
	}
	
	/**
	 * Delete this page menu item with the given verb if provided
	 * @param bool|string $action_verb
	 */
	public function delete($action_verb = false)
	{
		if(@$_SESSION['pages_last_selected'] == $this->id())
		{
			$_SESSION['pages_last_selected'] = $this->parentID();
		}
		
		foreach($this->children() as $child_menu)
		{
			$child_menu->delete(null);
		}
		
		
		parent::delete($action_verb = false);	
	}



	/**
	 * Returns the name of the URL target for editing this item in the admin. By default, the value returned is "edit".
	 * @return string
	 */
	public function adminEditURLTargetName()
	{
		return 'page-builder';
	}

	/**
	 * Returns the name of the class that uses the TMt_PageRenderItem
	 * @return string
	 */
	public static function pageRenderItemClassName()
	{
		return 'TMm_PagesContent';
	}
	
	/**
	 * Returns the display order for this menu item
	 * @return int
	 */
	public function displayOrder() : int
	{
		return $this->display_order;
	}

	//////////////////////////////////////////////////////
	//
	// CHILDREN
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the list of children page menu items
	 * @return TMm_PagesMenuItem[]
	 */
	public function children() : array
	{
		if(is_null($this->children))
		{
			$this->children = [];

			$menu_list = TMm_PagesMenuItemList::init();

			$children = $menu_list->childrenForMenuItem($this);
			$this->children = $children;



		}
		return $this->children;
	}

	/**
	 * Returns the number of children
	 * @return int
	 */
	public function numChildren() : int
	{
		return sizeof($this->children());
	}
	
	/**
	 * Returns the number of children that are visible
	 * @param TMm_PagesNavigationType[] $navigation_types The navigation types that should be checked for matching
	 * against. If an empty array is provided, then it will ignore navigation types.
	 * @return int
	 */
	public function numChildrenVisible(array $navigation_types = []) : int
	{
		return sizeof($this->childrenVisible($navigation_types));
	}
	
	/**
	 * Returns the children that are visible
	 * @param TMm_PagesNavigationType[] $navigation_types The navigation types that should be checked for matching
	 * against. If an empty array is provided, then it will ignore navigation types.
	 * @return TMm_PagesMenuItem[]
	 */
	public function childrenVisible(array $navigation_types = [])
	{
		$children = $this->children();
		
		foreach($children as $id =>$child)
		{
			if(!$child->isVisible($navigation_types))
			{
				unset($children[$id]);
			}
		}
		
		
		return $children;
	}
	
	/**
	 * Returns the array of all descendants for this menu item
	 * @return TMm_PagesMenuItem[]
	 */
	public function descendants()
	{
		$descendants = array();
		foreach($this->children() as $child)
		{
			$descendants[] = $child;
			if($child->numChildren() > 0)
			{
				$descendants = array_merge($descendants, $child->descendants());
			}
		}
		return $descendants;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// ANCESTRY
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the parent ID for this menu item
	 * @return int
	 */
	public function parentID()
	{
		return $this->parent_id;
	}

	/**
	 * Returns the parent menu item or false if one doesn't exist
	 * @return bool|TMm_PagesMenuItem
	 */
	public function parent()
	{
		if(is_null($this->parent))
		{
			
			$this->parent = (get_class($this))::init($this->parent_id);
			if(!$this->parent)
			{
				$this->addConsoleWarning('Menu (ID #' . $this->id() . ') with Missing Parent (ID #' . $this->parent_id . ')');
			}
		}
		return $this->parent;
	}
	
	
	/**
	 * Return the parent for a given level. The root is 0, so level1 is the first folder and so-on.
	 * @param int $level
	 * @return bool|TMm_PagesMenuItem
	 */
	public function parentForLevel($level)
	{
		$ancestors = $this->ancestors();
		return $ancestors[$level];
	}
	
	/**
	 * Returns the level that this menu is located at
	 * @return int
	 */
	public function level()
	{
		// pulling in the "root" item
		if($this->id() == 0)
		{
			return 0;
		}
		
		if($this->parent_id == 0)
		{
			return 1;
		}
		else
		{
			return $this->parent()->level() + 1;
		}
	}
	
	/**
	 * Returns all the ancestors for this menu item. The first in the array is the earliest ancestor
	 * @return TMm_PagesMenuItem[]
	 */
	public function ancestors()
	{
		if($this->ancestors === false)
		{
			$ancestors = array();
			$ancestors[] = $this;
			if($this->parent_id > 0 && $this->parent())
			{
				$ancestors = array_merge($this->parent()->ancestors(), $ancestors);
			}
			
			$this->ancestors = array();
			$number = 1;
			foreach($ancestors as $ancestor)
			{
				$this->ancestors[$number] = $ancestor;
				$number++;
			}
		}
		return $this->ancestors;
	}
	
	/**
	 * Returns the ancestor for a given level
	 * @param int $level
	 * @return TMm_PagesMenuItem|bool
	 */
	public function ancestorForLevel($level)
	{
		$ancestors = $this->ancestors();
		if(isset($ancestors[$level]))
		{
			return $ancestors[$level];
		}
		return false;

	}
	
	/**
	 * Returns if the provided menu item is an ancestor
	 * @param TMm_PagesMenuItem $menu_item
	 * @return bool
	 */
	public function menuItemIsAncestor($menu_item)
	{
		foreach($this->ancestors() as $ancestor)
		{
			if($ancestor->id() == $menu_item->id())
			{
				return true;
			}
		}
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// REARRANGING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Updates the display order for this item
	 * @param int $display_order
	 * @return void
	 */
	public function updateDisplayOrder(int $display_order) : void
	{
		if($display_order > 0)
		{
			$this->updateDatabaseValue('display_order', $display_order);
		}
	}
	
	/**
	 * Moves this menu item up one level
	 * @return array
	 */
	public function moveUp() : array
	{
		// FIND THE PREVIOUS MENU ITEM
		$query = "SELECT * FROM  `".static::tableName()."`
			WHERE parent_id = :parent_id AND display_order < :display_order
			ORDER BY display_order DESC
			LIMIT 1";
		$result = $this->DB_Prep_Exec($query, ['parent_id' => $this->parentID(), 'display_order' => $this->displayOrder()]);
		if($row = $result->fetch())
		{
			$response = ['updated' => true];
			$response['display_orders'] = [];
			// Instantiate the other menu
			$other_menu = TMm_PagesMenuItem::init($row);
			
			// Move that menu to this spot
			$other_menu->updateDisplayOrder($this->displayOrder());
			$response['display_orders'][$other_menu->id()] = $this->displayOrder();
			
			// Make this display order down by one
			$new_display_order = $this->displayOrder() - 1;
			$this->updateDisplayOrder($new_display_order);
			$response['display_orders'][$this->id()] = $new_display_order;
			return $response;
			
			
		
		}
		else
		{
			return ['updated' => false,
				'message' => 'Single item cannot move up'];
		}
		
	}
	
	public function moveDown() : array
	{
		
		// FIND THE NEXT MENU ITEM
		$query = "SELECT * FROM  `".static::tableName()."`
			WHERE parent_id = :parent_id AND display_order > :display_order
			ORDER BY display_order ASC
			LIMIT 1";
		$result = $this->DB_Prep_Exec($query, ['parent_id' => $this->parentID(), 'display_order' => $this->displayOrder()]);
		if($row = $result->fetch())
		{
			$response = ['updated' => true];
			$response['display_orders'] = [];
			// Instantiate the other menu
			$other_menu = TMm_PagesMenuItem::init($row);
			
			// Move that menu to this spot
			$other_menu->updateDisplayOrder($this->displayOrder());
			$response['display_orders'][$other_menu->id()] = $this->displayOrder();
			
			// Make this display order down by one
			$new_display_order = $this->displayOrder() + 1;
			$this->updateDisplayOrder($new_display_order);
			$response['display_orders'][$this->id()] = $new_display_order;
			return $response;
		}
		else
		{
			return ['updated' => false,
				'message' => 'Last item cannot move down'];
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// PHOTOS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the background photo file for this viewed menu.
	 *
	 * This method will return the closest file it can find in the hierarchy of hte menu item.
	 * @return bool|TCm_File
	 */
	public function photoType()
	{
		return $this->photo_type;
	}
	
	/**
	 * Returns the position pin for the photo of where it should be held when displayed. This method also recursively
	 * returns a value if it's inhereting from a parent
	 * @return string
	 */
	public function photoPosition()
	{
		// Recursion: If we're a child looking for a photo but dont' have one, we use the parent's settings
		if($this->photoType() == 'photo' && $this->photo_filename == '' && $this->parentID() > 0)
		{
			return $this->parent()->photoPosition();
		}

		return $this->photo_position;
	}

	/**
	 * Returns the background photo file for this viewed menu.
	 *
	 * This method will return the closest file it can find in the hierarchy of hte menu item.
	 * @return false|TCm_File
	 */
	public function photoFile() : false|TCm_File
	{
		if(is_null($this->photo_file))
		{
			// Set to false since it can't be null
			$this->photo_file = false;

			// check for a photo from a model
			if($active_model = $this->activeModelLoadedForPage())
			{
				if(method_exists($active_model,'pageDisplayPhoto'))
				{
					if($photo = $active_model->pageDisplayPhoto())
					{
						if($photo->exists())
						{
							$this->addConsoleDebug('FOUND photo');
							$this->photo_file = $active_model->pageDisplayPhoto();
						}
					}
				}
			}


			// Not found
			if(!$this->photo_file) // no value returned
			{
				if($photo = $this->pagePhotoFile())
				{
					if($photo->exists())
					{
						$this->photo_file = $photo;
					}
				}
			}

			// Handle the preferred width from the settings
			if($this->photo_file)
			{
				$this->photo_file->setCropOutputWidth(TC_getModuleConfig('pages', 'image_crop_width'));
			}
		}

		return $this->photo_file;

	}
	
	/**
	 * The photo file used for this page which does not take into account active models. This is the photo that would
	 * be loaded for the page regardless of the content on the page.
	 */
	public function pagePhotoFile()
	{
		if($this->page_photo_file === null)
		{
			// If we have a hero box and there's at least one
			if(TC_getConfig('use_pages_hero_boxes') && $this->photoType() == 'hero_box')
			{
				$hero_boxes = $this->heroBoxes();
				foreach($hero_boxes as $box)
				{
					// Grab the first photo
					$file = $box->photoFile();
					if($file->isImage()) // avoid weird stuff that might show up in a hero box
					{
						$this->page_photo_file = $file;
						return $this->page_photo_file;
					}
					
					
				}
			}
			
			// Regardless of what's been set, if it's not using a single photo, return
			if($this->photoType() != 'photo')
			{
				$this->page_photo_file = false;
				return null;
			}
		}
		
		// check for a photo from a model
		if($this->photo_filename != '')
		{
			$this->page_photo_file = new TCm_File(false, $this->photo_filename, static::uploadFolder(), TCm_File::PATH_IS_FROM_SERVER_ROOT);
		}
		
		if(!$this->page_photo_file && $this->parentID() > 0)
		{
			$this->page_photo_file = $this->parent()->photoFile();
		}
		
		// Handle the preferred width from the settings
		if($this->page_photo_file)
		{
			$this->page_photo_file->setCropOutputWidth(TC_getModuleConfig('pages', 'image_crop_width'));
		}
		
		return $this->page_photo_file;
	}
	
	/**
	 * Returns the photo file that should be used on the background, which takes into account the active models and
	 * it's settings related to background photos.
	 * @return TCm_File
	 */
	public function backgroundPhotoFile()
	{
		// Check for an active model that doesn't want to use the photo for backgrounds
		// In that scenario we need to return the page photo
		if($active_model = $this->activeModelLoadedForPage())
		{
			// Only bother if we setting for using photos on backgrounds
			if(method_exists($active_model,'usePhotoForPageBackgrounds'))
			{
				if(!$active_model->usePhotoForPageBackgrounds())
				{
					return $this->pagePhotoFile();
				}
			}
			
		}
		
		// in any other case, the photo file works
		return $this->photoFile();
		
	}

	/**
	 * Returns if this particular menu has a unique photo file
	 *
	 *
	 * @return bool
	 */
	public function hasUniquePhotoFile()
	{
		if(($active_model = $this->activeModelLoadedForPage()) && $active_model->photoFile())
		{
			return true;
		}

		return $this->photo_filename != '';
	}

	/**
	 * Sets the photo for this menu item to be from a model. This value is normally pulled automatically from the
	 * page menu item itself, however this value will override that.
	 * @param TCm_File $photo
	 */
	public function setPhotoFromModel($photo)
	{
		$this->photo_file = $photo;
	}

	//////////////////////////////////////////////////////
	//
	// HERO BOXES
	//
	// Functions related to hero boxes for this menu item
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns if this menu uses hero boxes
	 * @return bool
	 */
	public function usesHeroBoxes() : bool
	{
		return TC_getConfig('use_pages_hero_boxes') && $this->photoType() == 'hero_box';
	}

	/**
	 * Returns the array of hero boxes for this menu item
	 * @return TMm_PageHeroBox[]
	 */
	public function heroBoxes() : array
	{
		if(is_null($this->hero_boxes))
		{
			$this->hero_boxes = [];
			
			if(TC_getConfig('use_pages_hero_boxes'))
			{
				$query = "SELECT * FROM `pages_hero_boxes` WHERE menu_id = :menu_id ORDER BY display_order ASC";
				$result = $this->DB_Prep_Exec($query, array('menu_id' => $this->id()));
				while($row = $result->fetch())
				{
					$hero_box = TMm_PageHeroBox::init($row);
					$this->hero_boxes[] = $hero_box;
				}
			}
		}

		return $this->hero_boxes;
	}

	//////////////////////////////////////////////////////
	//
	// FOLDERS
	//
	// Methods related to folder management
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the folder for this page
	 * @return string
	 */
	public function folder()
	{
		return $this->localizeProperty('folder');
	}
	
	/**
	 * Validation for folder names to confirm it's not using one of our restricted values.
	 * @param string $folder_name
	 * @return bool
	 */
	public static function validateFolderName(string $folder_name) : bool
	{
		$restricted_folders = ['admin','t-api','w'];
		if(in_array(strtolower($folder_name), $restricted_folders))
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns the folder for a given level
	 * @param $level
	 * @return bool|string
	 */
	public function folderForLevel($level)
	{
		if($level <= 0)
		{
			return false;	
		}
		
		if($this->level() == $level)
		{
			return $this->folder();
		}
		
		if($level < $this->level())
		{
			return $this->ancestorForLevel($level)->folder();
		}
		
		return false;
	}
	
	
	/**
	 * Internal function to handle special cases when using the folder value
	 * @return string
	 */
	protected function cleanFolder()
	{
		$folder = $this->folder();
		if($folder == '/')
		{
			$folder = '';
		}
		elseif($folder == '')
		{
			$folder = 'Undefined';
		}
		return $folder;
	}
	
	/**
	 * Processes a url and returns the value without double slashes
	 * @param $string
	 * @return string
	 */
	public function removeDoubleSlashes($string)
	{
		return str_ireplace('//', '/', $string);
	}
	
	
	/**
	 * Returns the path to the menu item but it won't include the menu item folder in the path.
	 * @return string
	 */
	public function pathToMenu() : string
	{
		if($this->parentID() > 0)
		{
			$parent = (get_class($this))::init($this->parent_id);
			
			return $this->removeDoubleSlashes($parent->pathToMenu().'/'.$parent->cleanFolder().'/');
		}
		else
		{
			return '/';
		}
	}
	
	/**
	 * Returns the path to the folder
	 * @return string
	 */
	public function pathToFolder() : string
	{
		if($this->folder() != '')
		{
			return $this->removeDoubleSlashes($this->pathToMenu().$this->cleanFolder().'/');
		}
		else
		{
			return $this->removeDoubleSlashes($this->pathToMenu());
		}
	}
	
	/**
	 * Returns the path to URL that is actually displayed for this menu. This takes into account other factors like
	 * the ends of the URLs but also redirects.
	 * @param ?TCm_Model $override_model An override model if we want to generate a URL for something that isn't the
	 * active model or any other scenario that it might be relevant
	 * @return string
	 */
	public function viewURL(?TCm_Model $override_model = null) : string
	{
		// If there's a redirect, just go straight there
		if($redirect = $this->redirect($override_model))
		{
			return $redirect;
		}
		
		return $this->pageViewURLPath().$this->viewURL_ModelIDWithSuffix($override_model);
	}
	
	/**
	 * Method that returns the portion of the URL that is the model ID with the suffix. This can be extended or
	 * replaced to control the response to IDs.
	 * @param ?TCm_Model $override_model An override model if we want to generate a URL for something that isn't the
	 * active model or any other scenario that it might be relevant
	 * @return string
	 */
	public function viewURL_ModelIDWithSuffix(?TCm_Model $override_model = null) : string
	{
		$id_with_suffix = '';
		if($this->model_class_name != '' && class_exists($this->model_class_name))
		{
			$model = null;
			if($override_model)
			{
				$model = $override_model;
			}
			else
			{
				$model = TC_activeModelWithClassName($this->model_class_name);
			}
			if($model)
			{
				$id_with_suffix .= $model->id() . '/';
				
				// Backwards compatibility issue to check for the method
				// Older sites allowed for models but not the trait, so it can create unnecessary errors
				if(method_exists($model, 'pageViewURLSuffix'))
				{
					$suffix = $this->cleanForURL($model->pageViewURLSuffix());
					if($suffix != '')
					{
						$id_with_suffix .= $suffix.'/';
					}
					
				}
			}
		}
		
		return $id_with_suffix;
	}


	//////////////////////////////////////////////////////
	//
	// TITLE
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the title for this menu item
	 * @return string
	 */
	public function title()
	{
		return strip_tags(''.$this->localizeProperty('title'));
	}
	
	/**
	 * Adds a suffix to the title. This must be manually pulled when necessary
	 * @param string $suffix
	 * @return void
	 */
	public function addTitleSuffix($suffix)
	{
		$this->title_suffix = $this->title_suffix . $suffix;
	}
	
	/**
	 * Returns the title for this menu item plus any suffixes that have been added. Normally used in page titles and
	 * displays
	 * @return string
	 */
	public function titleWithSuffix()
	{
		return $this->title().$this->title_suffix;
	}
	
	/**
	 * Returns the title suffix for this page
	 * @return string
	 */
	public function titleSuffix()
	{
		return $this->title_suffix;
	}
	
	/**
	 * Returns the title for this menu item that is displayed on public sites. This menu takes into account the model
	 * being provided.
	 *
	 * 1. If the menu item has the `model_class_name_title_display` set to `page-title` then that is used.
	 * 2. If the model is found and `pageDisplayTitle()` is set, it will use that method
	 * 3. Otherwise it defaults to the page title
	 *
	 * @return string
	 * @see TMt_PageModelViewable::pageDisplayTitle()
	 */
	public function titleOrModelTitle()
	{
		$title = $this->title();
		if($this->modelClassName())
		{
			// class exists but we've set the main title to show
			if($this->model_class_name_title_display == 'page_title')
			{
				return $title;
			}
			
			$model = TC_activeModelWithClassName($this->modelClassName());
			if($model)
			{
				if(method_exists($model, 'pageDisplayTitle'))
				{
					$title = $model->pageDisplayTitle();
				}
				else
				{
					$title = $model->title();
				}

			}
		}
		return $title;
	}

	/**
	 * Returns the description for this menu item
	 * @return string
	 */
	public function description()
	{
		// Detect a model and use their description
		if($this->requiresActiveModel())
		{
			/** @var TMt_PageModelViewable|TCm_Model $model */
			$model = $this->activeModelLoadedForPage();
			if(($model instanceof TCm_Model) && TC_classUsesTrait($model,'TMt_PageModelViewable'))
			{
				$description = $model->metaDescription();
				
				// Only bother if they aren't blank. Otherwise we want to give the page values a chance to get used.
				// If they are blank then the cascade will go to the site description.
				if($description != '')
				{
					return $description;
				}
			}
		}
		
		return strip_tags(''.$this->localizeProperty('meta_description'));
	}
	
	/**
	 * Returns the description for this item
	 * @return string
	 */
	public function metaDescription()
	{
		return $this->meta_description;
		
	}
	
	
	
	/**
	 * Returns the title for this menu item
	 * @return string
	 */
	public function metaTitle()
	{
		// Detect a model and use their title
		if($this->requiresActiveModel())
		{
			/** @var TMt_PageModelViewable|TCm_Model $model */
			$model = $this->activeModelLoadedForPage();
			if(($model instanceof TCm_Model) && TC_classUsesTrait($model,'TMt_PageModelViewable'))
			{
				$meta_title = $model->metaTitle();
				
				// Only bother if they aren't blank. Otherwise we want to give the page values a chance to get used.
				// If they are blank then the cascade will go to the site meta title.
				if($meta_title != '')
				{
					return $meta_title;
				}
			}
		}
		
		return strip_tags(''.$this->localizeProperty('meta_title'));
	}
	
	
	/**
	 * Returns the page title for this page. If the value was set in the form, it will use this value. Otherwise, it will
	 * return the regular title
	 * @return mixed|string
	 */
	public function pageTitle()
	{
		if($this->title_override != '')
		{
			return $this->localizeProperty('title_override');
		}
		return $this->title();
	}
	/**
	 * Returns the title for the menu item without any filtering or removal of unsafe characters
	 * @return string|bool
	 */
	public function titleUnfiltered()
	{
		return $this->title_original;
	}

	/**
	 * Sets the title for the menu item
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Returns the title for this page with all the ancestors included as well
	 * @param string $separator The string used to separate the pages
	 * @return string
	 */
	public function titleWithFullAncestors($separator = ' - ')
	{
		$titles = array();
		foreach($this->ancestors() as $ancestor)
		{
			$titles[] = $ancestor->title();
		}
		return implode($separator, $titles);
		
	}
	
	/**
	 * Sets the page title for the page
	 * @param string $title The new title for the page
	 *
	 */
	public function setPageTitle($title)
	{
		$this->title_override = $title;
	}

	/**
	 * Returns the class name for the dynamic title.
	 * @return bool|string
	 * @deprecated
	 */
	public function titleDynamicClass()
	{
		return $this->title_dynamic_class; 
	}

	/**
	 * Returns the class name for the model class name
	 * @return bool|class-string<TCm_Model>
	 */
	public function modelClassName()
	{
		return $this->model_class_name;
	}
	
	/**
	 * Returns if this menu is a template page for a model.
	 * @return bool
	 */
	public function isModelTemplatePage() : bool
	{
		return $this->model_class_name != '';
	}
	
	/**
	 * Returns false for usable because it is handled by the theme processor. This allows other models to be
	 * processed and used instead of this page title, which is actually the fallback.
	 * @return bool
	 */
	public function isUsableAsPageTitle()
	{
		return false;
	}
	
	/**
	 * Returns the active model that is loaded for this page menu. This only returns a TCm_Model if we're loading a
	 * this page and it has a model class name and there's an active model for that class name.
	 * @return bool|TCm_Model|TMt_PageModelViewable
	 */
	public function activeModelLoadedForPage()
	{
		if($this->modelClassName())
		{
			$active_model = TC_activeModelWithClassName($this->modelClassName());
			if($active_model)
			{
				return $active_model;
			}
		}
		
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// VISIBILITY
	//
	//////////////////////////////////////////////////////

	/**
	 * @return int
	 * @deprecated
	 */
	public function isVisibleValue()
	{
		return $this->is_visible;
	}
	
	/**
	 * Calculates the visibility of the page based on all the known factors that don't deal with the specifics of
	 * what menu they appear in. This includes content visibility, conditional visibility, and authentication.
	 * @param TMm_PagesNavigationType[] $navigation_types The navigation types that should be checked for matching
	 * against. If an empty array is provided, then it will ignore navigation types.
	 * @return bool
	 */
	public function isVisible(array $navigation_types = []) : bool
	{
		// Content is deactivated, so page is inactive, then it should never appear
		if(!$this->contentVisible())
		{
			return false;
		}
		
		// Authentication, but no user is set
		if($this->requiresAuthentication() && !TC_currentUser())
		{
			return false;
		}
		
		// Check for navigation types
		if(count($navigation_types) > 0)
		{
			if(!$this->matchesNavigationTypes($navigation_types))
			{
				return false;
			}
		}
		
		// Doesn't meet conditional visibility requirements
		if(!$this->isValidConditionalVisibility())
		{
			return false;
		}
		
		
		// Could not find a reason to hide this page
		return true;
		
	}
	
	/**
	 * Returns if the provided navigation types are set to match for this menu item.
	 * @param TMm_PagesNavigationType[] $types An array of types to check against
	 * @return bool
	 */
	public function matchesNavigationTypes(array $types) : bool
	{
		// Prime navigation types based on IDs
		$this->navigationTypes();
		
		// Loop through them all, seeing if one matches the known types
		foreach($types as $type)
		{
			if(isset($this->navigation_types[$type->id()]))
			{
				return true;
			}
		}
		
		return false;
		
		
	}
	
	/**
	 * Returns all the navigation types that are associated with this menu item
	 * @return array
	 */
	public function navigationTypes(): array
	{
		if(is_null($this->navigation_types))
		{
			$this->navigation_types = [];
			$list = TMm_PagesMenuItemList::init();
			foreach($list->pagesNavigationTypeMatches() as $match)
			{
				if($match['menu_id'] == $this->id())
				{
					$type = TMm_PagesNavigationType::init($match['navigation_id']);
					$this->navigation_types[$type->id()] = $type;
				}
			}
		}
		
		return $this->navigation_types;
	}
	
	/**
	 * Adds a navigation type to this menu item
	 * @param TMm_PagesNavigationType $navigation_type
	 * @return void
	 */
	public function addNavigationType(TMm_PagesNavigationType $navigation_type): void
	{
		if(!$this->matchesNavigationTypes([$navigation_type]))
		{
			// Create the match
			TMm_PagesNavigationTypeMenuItemMatch::createWithValues(
				[
					'menu_id' => $this->id(),
					'navigation_id' => $navigation_type->id()
				]);
			$this->navigation_types[$navigation_type->id()] = $navigation_type;
		}
	}
	
	
	
	/**
	 * Returns if the visibility conditions are met for this page, regardless of what the visible setting is
	 * configured for. This method simply indicates if any of the conditions set for model name, login, etc have been
	 * met.
	 * @return bool|null
	 */
	public function isValidConditionalVisibility()
	{
		if(is_null($this->is_visible_conditional))
		{
			$this->is_visible_conditional = true;

			// NO user logged in
			if($this->requiresAuthentication() && !TC_currentUser())
			{
				$this->is_visible_conditional = false;

			}
			// We require a model class but an active one can't be found
			if($this->isMissingActiveModel())
			{
				$this->is_visible_conditional = false;
			}

			// Still Visible, Check conditions
			if($this->is_visible_conditional)
			{
				// CONDITIONAL
				if($this->visibilityCondition() != '')
				{
					$text = new TCu_Text($this->visibilityCondition());
					$this->is_visible_conditional = $text->evaluateTemplateConditionAsBoolean();
				}

			}

		}

		return $this->is_visible_conditional;

	}
	
	/**
	 * Returns if this menu item requires an active model to render
	 * @return bool
	 */
	public function requiresActiveModel()
	{
		return $this->model_class_name != '';
	}
	
	/**
	 * Returns if this page requires a active model but no model can be found with the class name. This normally
	 * indicates that the page is attempting to view something that doesn't exist.
	 * @return bool
	 */
	public function isMissingActiveModel()
	{
		return $this->requiresActiveModel() && !TC_activeModelWithClassName($this->model_class_name);
	}
	
	/**
	 * The name used for visibility requirements
	 * @return string
	 */
	public function visibilityName() : string
	{
		$options = TC_getModuleConfig('pages','navigation_types');
		$options = explode(',', $options);
		
		if($this->is_visible == 0)
		{
			return 'Hidden';
		}
		else
		{
			$index = (int)$this->is_visible - 1;
			if(isset($options[$index]))
			{
				return  trim($options[$index]);
			}
			else
			{
				return "Unknown";
			}
		}
	}

	
	/**
	 * @return string
	 */
	public function visibilityCondition()
	{
		return $this->visibility_condition;
	}

	/**
	 * Toggles the visibility of this menu item
	 */
	public function toggleVisibility()
	{
		if($this->is_visible == 1 || $this->is_visible == 0)
		{
			$query = "UPDATE pages_menus SET is_visible = !is_visible WHERE menu_id = :menu_id";
			$result = $this->DB_Prep_Exec($query, array('menu_id'=>$this->id()));
			$this->is_visible = !$this->is_visible;
		}
	}

	/**
	 * Set this menu item as a current menu item
	 * @param bool $is
	 */
	public function setIsCurrent($is = true)
	{
		$this->is_current = $is; 
	}
	
	/**
	 * Returns if the menu item is current
	 * @return bool
	 */
	public function isCurrent()
	{
		return $this->is_current;
	}
	
	
	/**
	 * Returns if this menu is active. This is a broader enabled/disabled setting
	 * @return bool
	 */
	public function isActive()
	{
		$is_active = $this->content_visible > 0;
		if($is_active && $this->parent()->level() >=1)
		{
			return $this->parent()->isActive();
		}
		
			
		return $is_active;
	}
	
	
	/**
	 * Returns if the content for this menu item is visible. This method includes validation for if conditional
	 * visibility values are met.
	 * @return bool
	 */
	public function contentVisible()
	{
		// HIDDEN OR MODULE LIST, Menu Not Visible
		if($this->content_visible <= 0)
		{
			return false;
		}
		else
		{
			return $this->isValidConditionalVisibility();
		}

	}

	
	//////////////////////////////////////////////////////
	//
	// DUPLICATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Duplicates the page and all the content on it.
	 * @param array|bool $override_values An array of values that are overridden in the duplication process
	 */
	public function duplicate($override_values = array())
	{
		// Ensure a different folder name
		$override_values['folder'] = $this->folder().'-copy-'.rand(10, 1000);
		$override_values['title'] = $this->title().' [Copy]';
		
		// Put it at the bottom of the parent
		$override_values['display_order'] = $this->parent()->numChildren() + 1;
		
		// Duplicate the new page and keep going if it exists
		if($new_page = parent::duplicate($override_values))
		{
			$new_menu_id_overrides = array('menu_id' => $new_page->id());
			// Loop through the sections and duplicate them as well
			foreach($this->layoutContentItems() as $content_item)
			{
				// pass in the newly created menu_id
				$content_item->duplicate($new_menu_id_overrides);
			}
			
			// We do not import page hero boxes
			if($this->usesHeroBoxes())
			{
				foreach($this->heroBoxes() as $hero_box)
				{
					$hero_box->duplicate($new_menu_id_overrides);
				}
			}
			
			// Save it as the active model
			// This will alter the redirect to go to this page when successful
			TC_saveActiveModel($new_page);
		}
		
		
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// MODEL LISTS
	//
	// Functions related to replacing the menu item with
	// a model list with links to this page and an ID
	//
	// This functionality allows a menu item to be replaced
	// entirely by a series of links with the same URL that
	// are pulled from a Tungsten Model List.
	//
	//////////////////////////////////////////////////////

	public function isModelList()
	{
		return $this->is_model_list;
	}

	public function modelListClassName()
	{
		$parts = explode('::', trim($this->model_list_class_name));
		if(sizeof($parts) > 1)
		{
			return $parts[0];
		}
		return $this->model_list_class_name;
	}


	public function modelListClassMethod()
	{
		$parts = explode('::', trim($this->model_list_class_name));
		if(sizeof($parts) > 1)
		{
			return str_ireplace('()','', $parts[1]);
		}
		return 'modelsVisible';
	}



	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns if this menu item requires authentication to be viewed
	 * @return bool
	 */
	public function requiresAuthentication() : bool
	{
		return $this->requires_authentication;
	}
	
	/**
	 * Returns if this menu item is visible including the check to match if authentication is needed.
	 * @return bool
	 * @deprecated
	 */
	public function isVisibleWithAuthentication() : bool
	{
		return $this->isVisible();
		
		
	}
	

	//////////////////////////////////////////////////////
	//
	// CONTENT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the content entry type
	 * @return bool
	 */
	public function contentEntry()
	{
		return $this->content_entry;
	}
	
	/**
	 * Returns if the page uses managed content
	 * @return bool
	 */
	public function isContentManaged()
	{
		return $this->content_entry == 'manage';
	}
	
	/**
	 * Returns if this uses a class method
	 * @return bool
	 */
	public function isClassMethod()
	{
		return $this->content_entry == 'class_method';
	}
	
	/**
	 * Returns if this menu redirects to another item or URL
	 * @return bool
	 */
	public function isRedirectToMenu()
	{
		return $this->content_entry == 'redirect_to_menu';
	}
	
	public function isRedirectToExternalURL()
	{
		return $this->content_entry == 'redirect' ;
	}
	
	
	/**
	 * Returns the redirect path if it exists, otherwise null
	 * @param ?TCm_Model $override_model
	 * @return null|string
	 */
	public function redirect(?TCm_Model $override_model = null) : ?string
	{
		if($this->isRedirectToMenu())
		{
			$menu = TMm_PagesMenuItem::init( $this->redirect_to_menu);
			if($menu instanceof TMm_PagesMenuItem)
			{
				return $menu->viewURL($override_model);
			}
		}
		elseif($this->isRedirectToExternalURL())
		{
			return $this->redirect;
		}
		return null;
		
	}
	
	
	/**
	 * Returns the redirect class name
	 * @return string
	 */
	public function redirectClassName()
	{
		return $this->redirect_class_name;
	}
	
	/**
	 * Returns the redirect class name with a friendlier human-readable name
	 * @return string
	 */
	public function redirectClassNameFriendly()
	{
		$class_name = $this->redirect_class_name;
		
		if(property_exists($class_name, 'model_title'))
		{
			return $class_name::$model_title;
		}
		
		$class_name = str_ireplace('TMv_', '', $class_name);
		return $class_name;
	}
	
	/**
	 * Returns the class method for the redirect
	 * @return string
	 */
	public function redirectClassMethod()
	{
		return $this->redirect_class_method;
	}
	
	/**
	 * Returns the redirect class target page
	 * @return string
	 */
	public function redirectClassTargetPage()
	{
		if($this->redirect_class_target != 'referrer')
		{
			$menu = TMm_PagesMenuItem::init( $this->redirect_class_target);
			if($menu)
			{
				return $menu->pathToFolder();
			}
		}
		
		// failsafe return to referrer
		return $_SERVER['HTTP_REFERER'];
		
	}
	
	public function modelClassViewMode()
	{
		return $this->model_class_name_primary_view_page;
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PageRenderer
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns an array of properties that used to create a layout.
	 * @param string $layout_class_name
	 * @return array
	 */
	public function additionalLayoutCreationProperties($layout_class_name)
	{
		$properties = array();
		$properties['css_classes' ] = $layout_class_name::pageContent_defaultClasses();

		return $properties;
	}

	/**
	 * Returns the view to be used when trying to view this item as a page builder
	 * @return string
	 */
	public function rendererPageBuilderURL()
	{
		return '/admin/pages/do/page-builder/'.$this->id();

	}


	/**
	 * Returns the name of the form view class that is used to edit/create TMt_PageRenderItem
	 * @return string
	 */
	public function pageRenderItem_ContentFormClassName()
	{
		return 'TMv_PageMenuContentForm';
	}
	
	
	/**
	 * Returns the ID column for the renderer. By default this returns 'content_id' since that is used by Pages as
	 * well as the page_rendered_items
	 * ID Column
	 * @return string
	 */
	public function rendererIDColumn()
	{
		return 'menu_id';
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Permission method which allows pages to be viewed. This must return true unless you want the page to be
	 * uneditable in the admin.
	 * @param bool $user
	 * @return bool|null
	 */
	public function userCanView($user = false)
	{
		if(TC_isTungstenView())
		{
			return true; // Must be true otherwise editor is broken
		}
		
		// Check to see if content visible
		return $this->isActive();
		
	}
	
	/**
	 * Permission for editing a page.
	 * @param TMm_User|bool $user
	 * @return bool|null
	 */
	public function userCanEdit($user = false)
	{
		if($user == false)
		{
			$user = TC_currentUser();
		}
		
		$pages_module = TMm_PagesModule::init();
		if($pages_module->userHasFullAccess($user))
		{
			return true;
		}
		
		// Check for page-specific access for this user
		return $this->userHasPageSpecificAccess($user); // Must be true otherwise editor is broken
	}
	
	public function userCanDelete($user = false)
	{
		
		return $this->userCanEdit($user);
	}
	
	/**
	 * Returns if the provided access has page-specific access to this menu item. This is only necessary if the user
	 * isn't already part of a group with full access to the pages module.
	 *
	 * @param TMm_User $user
	 * @return bool
	 */
	public function userHasPageSpecificAccess($user)
	{
		if(isset($this->user_specific_access[$user->id()]))
		{
			return $this->user_specific_access[$user->id()];
		}
		
		
		// Recursively look for a parent with access
		if($this->parent()->level() > 0 && $this->parent()->userHasPageSpecificAccess($user))
		{
			$this->user_specific_access[$user->id()] = true;
			return true;
		}
		
		$query = "SELECT g.* FROM pages_user_group_access a INNER JOIN user_groups g USING(group_id) WHERE menu_id = :menu_id";
		$result = $this->DB_Prep_Exec($query, array('menu_id' => $this->id()));
		while($row = $result->fetch())
		{
			$group = TMm_UserGroup::init($row);
			if($user->inGroup($group))
			{
				$this->user_specific_access[$user->id()] = true;
				return true;
			}
		}
		
		// No access found, save the value regardless
		$this->user_specific_access[$user->id()] = false;
		return false;
		
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PageModelViewable
	//
	// Although this class doesn't extend that view, it
	// does have the main method for viewing since other parts
	// of Tungsten can trigger it.
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the URL path that should be used to view this item on the public website. This is defined by each
	 * model to return the correct URL as needed, likely with the ID and possible a human readable.
	 *
	 * If a page on the site uses this model and is set as the primary view page, then this will work automatically
	 * @return bool|string A value of false indicates no path to view
	 * @see TCm_Model::cleanForURL()
	 */
	public function pageViewURLPath()
	{
		return $this->pathToFolder();
	}
	
	/**
	 * Returns the page view code for this item. These codes are used in locations where we want to reference an item
	 * that we need to insert the actual link afterwards.
	 * @return string
	 */
	public function pageViewCode()
	{
		return '/{{pageview:'.$this->contentCode().'}}';
	}

	/**
	 * Whether this item should be appearing on the sitemap
	 * @return bool
	 */
	public function isVisibleOnSitemap()
	{
		return $this->is_visible_on_sitemap == 1;
	}
	/**
	 * Whether this item should be appearing on the sitemap
	 * @return bool
	 */
	public function appearsOnSitemap()
	{
		// If the page is deactivated, it never appears in the sitemap
		if(!$this->isActive())
		{
			return false;
		}
		// Pages that are not visible on sitemap should be skipped here
		// Generated from TMt_PageModelViewable
		if(!$this->isVisibleOnSitemap())
		{
			return false;
		}
		// Requires permissions, regardless of the model
		if(!$this->userCanView() )
		{
			return false;
		}
		
		
		// Pages that require authentication to view them
		if( $this->requiresAuthentication())
		{
			return false;
		}
		
		
		// Pages that are redirect to an external URL
		if( $this->isRedirectToExternalURL())
		{
			return false;
		}
		
		// Soft 404s should not appear
		elseif($this->folder() == '404')
		{
			return false;
		}
		
		// Pages that require an active model don't load on their own
		// Generated from TMt_PageModelViewable
		elseif($this->requiresActiveModel())
		{
			return false;
		}
			
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// IMPORT CODES
	//
	//////////////////////////////////////////////////////
	
	public function importCode()
	{
		return $this->import_code;
	}
	
	public static function pageMenuItemWithImportCode($import_code)
	{
		$import_code = trim($import_code);
		if($import_code !== '')
		{
			$model = TCm_Model::init(false);
			$query = "SELECT * from `pages_menus` WHERE import_code = :import_code LIMIT 1";
			$result = $model->DB_Prep_Exec($query, ['import_code' => $import_code]);
			if($row = $result->fetch())
			{
				return TMm_PagesMenuItem::init($row);
				
			}
		}
		
		return null;
	}
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	// Content flagging is commonly done per-model, but
	// pages are always more complicated due to how they
	// are built and managed
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A flag method that detects if this renderer has no content
	 */
	public function flagNoContent()
	{
		// Don't flag pages that are actually redirects
		$redirect = $this->redirect();
		if($redirect && $redirect != '')
		{
			return false;
		}
		
		
		if($this->numLayoutContentItems() == 0)
		{
			return new TMm_ContentFlag('no-page-content', 'No page content');
			
		}
		
		
		return false;
	}
	
	/**
	 * A hook method to implement custom content flags that are unique to this model. These will be included with the
	 * normal flags for the item
	 * @return array
	 */
	public function customContentFlags()
	{
		$flags = [];
		// Loop through the items and add their flags
		foreach($this->contentItems() as $content_item)
		{
			$flags = array_merge($flags, $content_item->contentFlags());
		}
		
		return $flags;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_Searchable
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this class is publicly searchable. Defaults to false to avoid any accidentaly data exposure.
	 * Every publicly available class needs to have this method set.
	 * @return bool
	 */
	public static function isSearchableOnPublicSite() : bool
	{
		return true; // default to true for pages
	}
	
	/**
	 * The `Select` portion of the query for searching this model.
	 *
	 * @return string
	 */
	public static function searchQuerySelectPortion() : string
	{
		return "SELECT pm.*, pcv.value, pcv.content_id FROM pages_menus pm
    	LEFT JOIN pages_content pc ON(pm.menu_id = pc.menu_id)
    	LEFT JOIN pages_content_variables pcv ON(pc.content_id = pcv.content_id) ";
	}
	
	/**
	 * The `Where` portion of the query for searching this model. This method should not include the "Where" call
	 * itself, just what comes after it. This method must return an associative array with two indices, both of
	 * which have arrays within them. The 'where' array return snippets of text to be added in the where clause. The
	 * 'parameters' array is an associative array of parameters to be passed to PDO.
	 * @return array[]
	 */
	public static function searchQueryWherePortion() : array
	{
		$values = [];
		
		$values['where'] = [
			'pm.model_class_name = ""', // ignore if they require a model class
			'pm.content_entry = "manage"'
		];
		
		$values['parameters'] = [];
		
		return  $values;
	}
	
	/**
	 * Used for anything that would come after the `WHERE` portion of the query such as:
	 * ORDER BY
	 * GROUP BY
	 *
	 * @return string
	 */
	public static function searchQueryAfterWherePortion() : ?string
	{
		return 'GROUP BY pm.menu_id';
	}
	
	/**
	 * Returns the fields that should be searched in this class
	 *
	 * @return string[]
	 */
	public static function searchFields() : array
	{
		// Default values that belong to this page,
		return [
			'title',
			'meta_description',
			'pcv.value'
		];
	}
	
	
	/**
	 * @return string|null
	 */
	public function searchSubtitle() : ?string
	{
		// Don't need a subtitle for top-level pages, just duplicates what is already there
		if($this->level() == 1)
		{
			return null;
		}
		
		$ancestors = $this->ancestors();
		$titles = array();
		foreach ($ancestors as $ancestor)
		{
			$titles[] = $ancestor->title();
		}
		
		return implode(' &rsaquo; ', $titles);
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The schema for page module viewable, which is stored here because it's about pages as well and we need to
	 * reference from both places. Referencing static items from a trait that doesn't use the trait is deprecated.
	 * @return array[]
	 */
	public static function schema_TMt_PageModelViewable() : array
	{
		return [
			'meta_title' => [
				'comment'       => 'The meta title override for this page',
				'type'          => 'varchar(255)',
				'nullable'      => false,
				'localization'  => true,
			
			],
			
			'meta_description' => [
				'title'         => 'Description',
				'comment'       => 'The description used in the meta tag',
				'type'          => 'varchar(255)',
				'nullable'      => false,
				'localization'  => true,
			
			],
			'is_visible_on_sitemap' => [
				'comment'       => 'Whether this page is visible on sitemap',
				'type'          => 'bool DEFAULT 1',
				'nullable'      => false,
			],
		];
	}
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema()
			+ static::schema_TMt_PageModelViewable() // Schema for SEO stuff
			+ [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title for the page',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'localization'  => true,
					
					/// Methods to run flag checks on
					'content_flags' =>  [
						'flagNoContent' // added to the title, but doesn't really matter where since it's for the model
					],
				],
				
				
				'folder' => [
					'title'         => 'Folder',
					'comment'       => 'The folder for this module',
					'type'          => 'varchar(128)',
					'nullable'      => false,
					'localization'  => true,
					
					// Validations
					'validations'   => [
						'required'      => true,
						'methods'   => [
							[
								'method_name' => 'validateFolderName',
								'parameter_field_names' => ['folder'],
								'error_message' => 'Folder name is not permitted'
							],
						]
					]
				],
				
				'redirect' => [
					'title'         => 'Redirect',
					'comment'       => 'The value for redirecting this page',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				
				'redirect_to_menu' => [
					'title'         => 'Redirect to menu',
					'comment'       => 'The value for redirecting, which can include a menu ID',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
				],
				'parent_id' => [
					'title'         => 'Parent menu',
					'comment'       => 'The ID for the parent',
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => false,
				],
				'display_order' => [
					'title'         => 'Display order',
					'comment'       => 'The order of this item, in relation to items with the same parent',
					'type'          => 'smallint(5) unsigned',
					'nullable'      => false,
					'index'         => 'display_order'
				],
				
				'content_entry' => [
					'title'         => 'Content entry type',
					'comment'       => 'Indicates how content is entered and managed ',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'photo_filename' => [
					'title'         => 'Photo filename',
					'comment'       => 'The name of the photo which can include cropping values',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'photo_type' => [
					'title'         => 'Photo type',
					'comment'       => 'The type of photo we are using',
					'type'          => 'varchar(20) DEFAULT "photo"',
					'nullable'      => false,
				],
				'photo_alt_text' => [
					'title'         => 'Photo alt text',
					'comment'       => 'The alt text for the photo',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'localization'  => true,
				],
				'photo_position' => [
					'title'         => 'Background photo position',
					'comment'       => 'The position text for the background photo',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'redirect_class_name' => [
					'title'         => 'Redirect class name',
					'comment'       => 'The name of the class for the redirect',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'redirect_class_method' => [
					'title'         => 'Redirect class method',
					'comment'       => 'The name of the method in the class for the redirect',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'redirect_class_target' => [
					'title'         => 'Redirect class target',
					'comment'       => 'The name of the target for the redirect',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'is_visible' => [
					'title'         => 'Visible',
					'comment'       => 'Indicates if page is visible. Possibly stores string values',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				
				'is_model_list' => [
					'title'         => 'Is model list',
					'comment'       => 'Indicates if this page shows a model list',
					'type'          => 'bool',
					'nullable'      => false,
				],
				'model_list_class_name' => [
					'title'         => 'Model list class name',
					'comment'       => 'The name of the class that is used when a page uses class names with lists',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				
				'content_visible' => [
					'title'         => 'Content visible',
					'comment'       => 'Indicates if the content is visible',
					'type'          => 'bool DEFAULT 1',
					'nullable'      => false,
				],
				'is_active' => [
					'title'         => 'Active',
					'comment'       => 'Indicates if the page is active',
					'type'          => 'bool DEFAULT 1',
					'nullable'      => false,
				],
				'requires_authentication' => [
					'title'         => 'Requires authentication',
					'comment'       => 'Indicates if a user must be logged in to see this page',
					'type'          => 'bool',
					'nullable'      => false,
				],
				'title_override' => [
					'title'         => 'Title override',
					'comment'       => 'The override title for this page',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'model_class_name' => [
					'title'         => 'Model class name',
					'comment'       => 'The model that is associated with this page.',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'model_class_name_title_display' => [
					'title'         => 'Model class name title display',
					'comment'       => 'The setting on how to set the title for the model',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'model_class_name_primary_view_page' => [
					'title'         => 'Model class name primary view page',
					'comment'       => 'Indicates how this model acts in relation to the primary view settings such as edit or view',
					'type'          => 'varchar(20)',
					'nullable'      => false,
				],
				'visibility_condition' => [
					'title'         => 'Visibility condition',
					'comment'       => 'The condition on visibility which is formatted as a series of method calls ',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'content_visibility_condition' => [
					'title'         => 'Content Visibility condition',
					'comment'       => 'The condition on Content visibility which is formatted as a series of method calls ',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'import_code' => [
					'title'         => 'Import code',
					'comment'       => 'The code used when importing this page. Tracking purposes only.  ',
					'type'          => 'varchar(32)',
					'nullable'      => true,
				],
				
				// Old values that ends up in DBs
				'user_id' => [
					'delete' => true,
				],
				'is_visible_mobile' => [
					'delete' => true,
				],
				'keywords' => [
					'delete' => true,
				],
			
			
			
			];
	}
	
	
	
	public static function installUpdateQueries()
	{
		// At one point 7.0.5, we changed the "contentCode()" functionality to return the base class name, for
		// consistency. In order to break stuff, we want the values stored in databases to also use that naming
		// convention, so seek and update those values, so they still work
		$content_code_update_snippet = " SET value =
		CONCAT(
			SUBSTR(value, 1, POSITION('/{{' IN value)-1),
			'/{{pageview:TMm_PagesMenuItem',
			SUBSTR(value, POSITION('--' IN value))
		)
		WHERE value LIKE '%/{{pageview:TMm_%PagesMenuItem%}}%'
		";
		return [
			'6.0.0' => [
				// When upgrading to a newer version of pages, we require these variables types to be set
				"INSERT INTO module_variables SET module_name = 'pages', variable_name = 'navigation_types', value='Main Menus, Utilities', date_added = now()",
				"INSERT INTO module_variables SET module_name = 'pages', variable_name = 'website_theme_editor', value='', date_added = now()",
				"INSERT INTO module_variables SET module_name = 'pages', variable_name = 'use_GDPR', value='0', date_added = now()",
				"INSERT INTO module_variables SET module_name = 'pages', variable_name = 'use_301_redirects', value='0', date_added = now()",
			
			],
			
			// Metadata and renaming description to meta_description
			'6.4.0' => [
				// At this moment, we just crated a new empty `meta_description`, delete it
				// Rename it, JUST IN CASE something goes out of order. Deleting that column after naming would suck
				"ALTER TABLE pages_menus CHANGE meta_description description_dead text NOT NULL COMMENT 'DEPRECATED, part of update process'",
				
				// Rename the old one to the new value
				"ALTER TABLE pages_menus CHANGE description meta_description text NOT NULL COMMENT 'The description'",
			
			],
			'7.0.0' => [
				['TMm_PagesNavigationTypeList','convertToNavigationTypes']
			],
			
			// Changing content code to use base models, which means updating the codes to reference main page class
			'7.0.5' => [
				"UPDATE `pages_content_variables` ".$content_code_update_snippet,
				"UPDATE `pages_renderer_content_variables` ".$content_code_update_snippet,
				"UPDATE `pages_content_variables_unpub` ".$content_code_update_snippet,
				"UPDATE `pages_renderer_content_variables_unpub` ".$content_code_update_snippet,
			
			],
		];
	}
	
	
	/**
	 * Returns if this class uses the advanced metadata for meta values such as titles. If the feature switch
	 * for `use_seo_advanced` is enabled this is checked to see the URL target should be presented.
	 * @return bool
	 */
	public static function modelUsesAdvancedMetaData() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES – TSt_ModelWithHistory
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of related models that are associated with this model for history purposes. This is usually
	 * used to show the complete history of changes to a model, by pulling in history from other related models.
	 * For example, changing the user groups for a TMm_User shows up in the user's history. News categories are
	 * another example.
	 * @return array[]
	 */
	public static function historyRelatedModels() : array
	{
		return [
			[
				'model_name' => 'TMm_PagesContent',
				'reference_column_name' => 'menu_id',
				'heading_display_column_id'     => 'view_class',
				'heading_display_title'         => 'Content',
			],
			[
				'model_name' => 'TMm_Pages301Redirect',
				'reference_column_name' => 'target_page_menu_id',
				'heading_display_column_id'     => ' ',
				'heading_display_title'         => '301 Redirect',
				'additional_filter_where' => 'ISNULL(t.class_name)',
			],
		
		];
	}
	
}
?>