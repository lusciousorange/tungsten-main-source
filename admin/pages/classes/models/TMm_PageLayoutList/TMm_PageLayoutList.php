<?php
/**
 * Class TMm_PageLayoutList
 *
 * The list of all the page layouts available for the website
 *
 */
class TMm_PageLayoutList extends TCm_ModelList
{
	protected $page_layouts_found = array();
	protected $page_sections_found = array();
	
	/**
	 * TMm_PageLayoutList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TSm_Plugin',$init_model_list);
		
		$this->processAvailableLayouts();
				
	}

	/**
	 * Processes the available layouts
	 */
	protected function processAvailableLayouts()
	{
		// the root of the admin
		$admin_root = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$d = dir($admin_root);
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module_folder => $module)
		{
			// Process the pages content folder for any content views that should be loaded
			if($module_folder == 'pages')
			{
				$class_folder_path = $admin_root.$module_folder.'/layouts/';
			}
			else
			{
				$class_folder_path = $admin_root.$module_folder.'/classes/views/';
			}
			$this->processModuleFolder($class_folder_path, $module_folder);
		
		}
				
		
	}

	/**
	 * processes a single module folder for all the relevant page layouts
	 * @param string $class_folder_path
	 * @param string $module_folder
	 */
	protected function processModuleFolder($class_folder_path, $module_folder)
	{
		// check if it is a directory and skip . and ..
		if(is_dir($class_folder_path) && $module_folder != '.' && $module_folder != '..')
		{
			// if here then we are looking at /admin/<module>/
			$plugin_folder = dir($class_folder_path);
			while($plugin_file = $plugin_folder->read())
			{
				if($plugin_file != '.' && $plugin_file != '..') // ignore the two return links
				{
					// found a file or folder, only save it if it is a class
					$view_class_name = str_ireplace('.php','',$plugin_file);
					if(class_exists($view_class_name))
					{
						//$interfaces = class_implements($view_class_name);
						
						
						if(is_subclass_of($view_class_name, 'TMv_ContentLayout'))
						{
							if(!isset($this->page_layouts_found[$view_class_name]))
							{
								$path_from_site_root = str_ireplace($_SERVER['DOCUMENT_ROOT'], '', $class_folder_path).'/';
								$this->page_layouts_found[$view_class_name] = $path_from_site_root;
							}
						}
						
						if($view_class_name == 'TMv_ContentLayout_Section' ||
							is_subclass_of($view_class_name, 'TMv_ContentLayout_Section'))
						{
							if(!isset($this->page_sections_found[$view_class_name]))
							{
								$path_from_site_root = str_ireplace($_SERVER['DOCUMENT_ROOT'], '', $class_folder_path).'/';
								$this->page_sections_found[$view_class_name] = $path_from_site_root;
							}
							else // two found with the same name.
							{
								// DUE TO LOADING, THIS NEVER ACTUALLY GETS SHOWN :(
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Returns the list of page layouts
	 * @return TMv_ContentLayout[]
	 */
	public function pageLayouts()
	{
		return $this->page_layouts_found;
	}
	
	/**
	 * Returns the list of page sections
	 * @return TMv_ContentLayout[]
	 */
	public function pageSections()
	{
		return $this->page_sections_found;
	}
	
	
	
}

?>