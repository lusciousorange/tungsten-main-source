<?php

/**
 * Class TMm_PagesContentViewHidden
 *
 * A representation of one view entry that shows they are hidden. This is currently a shell class used to properly
 * define schemas.
 */
class TMm_PagesContentViewHidden extends TCm_Model
{
	public static $table_name = 'pages_content_views_hidden';
	public static $table_id_column = 'hidden_id';
	public static $model_title = 'Page content view hidden';
	
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'class_name' => [
					'title'         => 'Class Name',
					'comment'       => 'The name of the class view that is hidden',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
			];
		
	}

	
}