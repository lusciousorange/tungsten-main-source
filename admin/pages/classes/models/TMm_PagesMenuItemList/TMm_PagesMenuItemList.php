<?php

/**
 * List of all the page menus
 */
class TMm_PagesMenuItemList extends TCm_ModelList
{
	protected array $items = [];
	protected array $items_by_level = [];
	protected array $items_with_parent_id = [];
	protected bool $menus_processed = false;
	
	protected ?TMm_PagesMenuItem $homepage_menu_item = null;
	
	protected ?array $pages_navigation_type_matches = null;
	
	/**
	 * TMm_PagesMenuItemList constructor.
	 */
	public function __construct()
	{
		parent::__construct('TMm_PagesMenuItem', false);
		
		//$this->addConsoleDebug('CONSTRUCT');
		
		$this->handleLegacyMultiRowConversion();
		
		// FLAT INITIALIZATION
		// This is what we use for the full cache
		$cache_name = ($this->model_class_name)::cacheAllKeyName();
		$query = "SELECT * FROM `pages_menus` ORDER BY display_order ASC";
		$all_pages = $this->modelsForQuery($query,
											  array(),
											  false,
											  $cache_name);
		foreach($all_pages as $menu_item)
		{
			$this->items_with_parent_id[$menu_item->parentID()][$menu_item->id()] = $menu_item;
			
			if($menu_item->folder() == '/')
			{
				$this->homepage_menu_item = $menu_item;
			}
		}
	
	}
	
	/**
	 * Legacy function to deal with multi-row conversions from older formatting
	 * @return void
	 */
	protected function handleLegacyMultiRowConversion() : void
	{
		if( ! TC_getModuleConfig('pages','multirow_is_converted'))
		{
			// This will get run IF someone updates the pages settings, but it's just one time and that's fine
			// It has no effect on each subsequent call
			$query = 'UPDATE pages_content SET view_class= "TMv_ContentLayout_Section" WHERE view_class = "TMv_ContentLayout_MultiRow" ';
			$this->DB_Prep_Exec($query);
			
			$query = 'UPDATE pages_renderer_content SET view_class= "TMv_ContentLayout_Section" WHERE view_class = "TMv_ContentLayout_MultiRow" ';
			$this->DB_Prep_Exec($query);
			
			// Save the variable so we never do it again
			TC_setModuleConfig('pages','multirow_is_converted', 1);
		}
	}
	
	/**
	 * A method that is called one time to process all the menu items and ensure they are saved properly.
	 *
	 */
	protected function processMenuItems() : void
	{
		if(!$this->menus_processed)
		{
			$this->traverseMenuItem();
			$this->menus_processed = true;
		}
		
	}
	
	
	/**
	 * A recursive method that processes all the menu items processing them in true sequence which respects the
	 * parents and the full order. This also takes the liberty to check to see if the orders are incorrect for
	 * whatever reason and adjust them as well.
	 *
	 * @param ?TMm_PagesMenuItem $menu_item The item that is being processed
	 * @param int $level
	 */
	private function traverseMenuItem(?TMm_PagesMenuItem $menu_item = null, int $level = 1) : void
	{
		// First time through, instantiate menu items
		if(is_null($menu_item))
		{
			$order = 1;
			foreach($this->items_with_parent_id[0] as $parent_menu_item)
			{
				$this->items[$parent_menu_item->id()] = $parent_menu_item;
				$this->items_by_level[1][$parent_menu_item->id()] = $parent_menu_item;
				
				// Correct Orders
				if($parent_menu_item->displayOrder() != $order)
				{
					$parent_menu_item->updateDisplayOrder($order);
				}
				$order++;
				
				$this->traverseMenuItem($parent_menu_item, $level+1);
			}
		}
		else
		{
			$order = 1;
			foreach($menu_item->children() as $child_item)
			{
				$this->items[$child_item->id()] = $child_item;
				$this->items_by_level[$level][$child_item->id()] = $child_item;
				
				// Correct Orders
				if($child_item->displayOrder() != $order)
				{
					$child_item->updateDisplayOrder($order);
				}
				$order++;
				$this->traverseMenuItem($child_item, $level+1);
			}
		}
	}
	
	
	/**
	 * Returns all the items in the system which is sorted by the true display order
	 * @return TMm_PagesMenuItem[]
	 */
	public function items() : array
	{
		$this->processMenuItems();
		return $this->items;
	}
	
	/**
	 * Returns all the menus in a 2D array which is first sorted by the level (1,2,3 etc) then the items within that level
	 * @return array
	 */
	public function itemsByLevel() : array
	{
		$this->processMenuItems();
		return $this->items_by_level;
	}
	
	
	/**
	 * Returns all the elements for a given level
	 * @param int $level
	 * @return array|mixed
	 */
	public function itemsForLevel(int $level) : array
	{
		$this->processMenuItems();
		
		if(is_array($this->items_by_level[$level]))
		{
			return $this->items_by_level[$level];
		}
		
		return []; // failsafe
	}
	
	/**
	 * Returns all the children for a given parent
	 * @param TMm_PagesMenuItem $parent_item
	 * @return array
	 */
	public function childrenForMenuItem(TMm_PagesMenuItem $parent_item) : array
	{
		if(isset($this->items_with_parent_id[$parent_item->id()]))
		{
			return $this->items_with_parent_id[$parent_item->id()];
		}
		return [];
		
	}
	
	/**
	 * This filters the list, but it's based on the pages already having been instantiated, so we do all of our work
	 * on the models, rather than on the DB.
	 * @param $filter_values
	 * @return TMm_PagesMenuItem[]
	 */
	public function processFilterList($filter_values) : array
	{
		$all_pages = $this->items();
		//$this->addConsoleDebug($all_pages);
	
//		// DEAL WITH PARTIAL ACCESS
		
		// Instantiate the list to an array, which is our check for this being a thing
		$partial_access_page_ids = [];
		$pages_module = TMm_PagesModule::init();
		if(!$pages_module->userHasFullAccess())
		{
			// Loop through all the pages first
			foreach($all_pages as $page)
			{
				// If the user can edit it, then save the ID, plus the IDs of all the ancestors
				if($page->userCanEdit())
				{
					$partial_access_page_ids[$page->id()] = $page->id();
					foreach($page->ancestors() as $ancestor)
					{
						$partial_access_page_ids[$ancestor->id()] = $ancestor->id();
					}
				}
			}
			
			// Remove any that don't fall under the partial access list
			// Loop through all the pages, clearing them as we go
			foreach($all_pages as $id => $page)
			{
				// Not in the list, we don't show it
				if(!in_array($id, $partial_access_page_ids))
				{
					unset($all_pages[$id]);
				}
			}
			
		}
	
		// Loop through all the pages, clearing them as we go
		foreach($all_pages as $id => $page)
		{
			// Is ACTIVE
			if(isset($filter_values['status']) && $filter_values['status'] != '')
			{
				if($filter_values['status'] == 'unpublished')
				{
					if(!$page->hasUnpublishedContent())
					{
						unset($all_pages[$id]);
						continue;
					}
				}
				else
				{
					$page_is_active = $page->isActive();
					if($filter_values['status'] == 1 && !$page_is_active
						|| $filter_values['status'] == 0 && $page_is_active)
					{
						unset($all_pages[$id]);
						continue;
					}
				}
				
				
			}
				
				// Navigation items first
			if(isset($filter_values['navigation_type']) && $filter_values['navigation_type'] != '')
			{
				if($filter_values['navigation_type'] == 'none')
				{
					// If the page has ANY, remove it
					if(count($page->navigationTypes()) > 0)
					{
						unset($all_pages[$id]);
						continue;
					}
				}
				else
				{
					$navigation_type = TMm_PagesNavigationType::init($filter_values['navigation_type']);
					if(!$page->matchesNavigationTypes([$navigation_type]))
					{
						unset($all_pages[$id]);
						continue;
					}
				}
			}
			
			if(isset($filter_values['search']) && $filter_values['search'] != '')
			{
				$found = false;
				//$this->addConsoleDebug($filter_values['search']);
				
				$search_string = strtolower($filter_values['search']);
				
				// Search title
				if(str_contains(strtolower($page->title()),$search_string ))
				{
					$found = true;
				}
				elseif(str_contains(strtolower($page->pageViewURLPath()),$search_string ))
				{
					$found = true;
				}
				
				
				if(!$found)
				{
					unset($all_pages[$id]);
					continue;
					
				}
				
			}
		}
		return $all_pages;
		
	}
	
	/**
	 * Returns an array of models that are linkable in the pages system. This includes all the pages on the website
	 * but also all the other Page Model Viewable items on the site.
	 * @param bool $include_class_separator_titles (Optional, default false) Indicates if the array should include
	 * strings which are the class names for the next items. These must be detected by the programmer and handled
	 * separately from the provided models. Most likely by calling `::$model_title`. There will be strings interspersed
	 * with models.
	 * @return TMm_PagesMenuItem[]|TMt_PageModelViewable[]
	 */
	public function linkablePagesAndItems(bool $include_class_separator_titles = false) : array
	{
		$items = array();
		
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			// Don't include menus that require a model. They literally can't be linked to effectively
			if(!$menu_item->modelClassName() )
			{
				$items[] = $menu_item;
			}
			
		}
		
		/** @var class-string<TMt_PageModelViewable> $model_names */
		$model_names = TC_modelsWithTrait('TMt_PageModelViewable');
		foreach($model_names as $model_name)
		{
			if($model_name::isPageLinkable())
			{
				if($include_class_separator_titles)
				{
					$items[] = $model_name;
				}
				$model_list_class = $model_name . 'List';
				if(class_exists($model_list_class))
				{
					$method_name = $model_name::linkableItemsListMethod();
					//$values[]
					
					// Get the list, pass in false to avoid loading them all by accident, not always the case
					$model_list = $model_list_class::init(false);
					foreach($model_list->$method_name() as $model)
					{
						$items[] = $model;
					}
				}
			}
			
		}
		
		return $items;
	}
	
	/**
	 * Returns an array formatted specifically for linking in TinyMCE. This requires an array with titles and values.
	 * @return array
	 * @uses TMm_PagesMenuItemList::linkablePagesAndItems
	 */
	public function menuArrayForTinyMCE() : array
	{
		$values = array();
		foreach($this->linkablePagesAndItems(true) as $linkable_item)
		{
			// Detect a class name
			if(is_string($linkable_item))
			{
				$values[] = [
					'title' => '----- '.strtoupper($linkable_item::$model_title).' -----',
					'value' => '#'
				];
			}
			else
			{
				if($linkable_item instanceof TMm_PagesMenuItem)
				{
					$title = $linkable_item->titleWithFullAncestors();
				}
				else // A TMt_PageModelViewable item, other than a menu item
				{
					$title = $linkable_item->title();
				}
				
				$values[] = [
					'title' => $title,
					'value' => $linkable_item->pageViewCode()
				];
			}
			
			
			
			
		}
		return $values;
		
	}
	
	/**
	 * A method that is called when the menus are rearrange and the dislpay orders must be updated
	 * @param array $post_values The values being passed via post
	 */
	public function rearrangeMenus(array $post_values) : void
	{
		$query = "UPDATE pages_menus SET parent_id = :parent_id, display_order = :display_order WHERE menu_id = :menu_id";
		
		try
		{
			$parent_display_orders = array();
			$this->DB()->beginTransaction();
			$statement = $this->DB_Prep($query);
			$item_num = 1;
			$menu_id = $post_values['item_'.$item_num];
			while($menu_id > 0)
			{
				$parent_id = $post_values['item_'.$item_num.'_parent'];
				$parent_display_orders[$parent_id]++;
				$values = array(
					'parent_id' => $parent_id,
					'display_order' => $parent_display_orders[$parent_id],
					'menu_id' => $menu_id,
				
				);
				$this->DB_Exec($statement, $values);
				$item_num++;
				$menu_id = $post_values['item_'.$item_num];
				
				$last_parent_id = $parent_id;
			}
			
			
			$this->DB()->commit();
		}
		catch (Exception $e)
		{
			$this->DB()->rollBack($e->getMessage());
			$this->addConsoleMessage('Display Order Update Failed', TSm_ConsoleIsError);
		}
		
	}
	
	/**
	 * Returns the homepage menu item, if it exists
	 * @return ?TMm_PagesMenuItem
	 */
	public function homepageMenuItem() : ?TMm_PagesMenuItem
	{
		return $this->homepage_menu_item;
	}
	
	/**
	 * Returns the pages that use the provided view
	 * @param string $view_name
	 * @return TMm_PagesMenuItem[]
	 */
	public function pagesWithView(string $view_name) : array
	{
		$pages = [];
		$query = "SELECT * FROM `pages_content` WHERE view_class = :view_class";
		$result = $this->DB_Prep_Exec($query, ['view_class' => $view_name]);
		while($row = $result->fetch())
		{
			$pages[] = TMm_PagesMenuItem::init($row);
		}
		
		return $pages;
	}
	
	/**
	 * Returns the page that is set up as a 404. It must have the folder with the name 404.
	 * @return ?TMm_PagesMenuItem
	 */
	public function pageFor404() : ?TMm_PagesMenuItem
	{
		$query = "SELECT * FROM `pages_menus` WHERE folder = '404' LIMIT 1";
		$result = $this->DB_Prep_Exec($query);
		if($row = $result->fetch())
		{
			return TMm_PagesMenuItem::init($row);
		}
		
		return null;
		
	}
	
	/**
	 * Creates a new page which is added to the bottom of the main menus
	 * @return array
	 */
	public function createPage() : array
	{
		$query = "SELECT (COUNT(menu_id) + 1) as display_order FROM pages_menus WHERE parent_id = 0";
		$result = $this->DB_Prep_Exec($query);
		$row = $result->fetch();
		
		
		$values = array(
			'parent_id' => 0,
			'display_order' => $row['display_order'],
			'content_entry' => 'manage',
			'title' => 'New Page',
			'folder' => 'page-'.$row['display_order']
		);
		$menu_item = TMm_PagesMenuItem::createWithValues($values);
		if($menu_item instanceof TMm_PagesMenuItem)
		{
			$menu_manager = new TMv_PagesMenuManager();
			$li = $menu_manager->listItemForMenu($menu_item);
			return [
				'page_id' => $menu_item->id(),
				'menu_manager_html' => $li->html(),
			];
		}
		
		return ['error' => 'Page could not be created'];
	}
	
	/**
	 * Method to generate the purchase list based on the values from this filter
	 */
	public function generateExcel() : void
	{
		// this may take a while
		set_time_limit(0);
		ob_start("ob_gzhandler");
		
		
		$excel = TMm_PagesExcel::init();
		$excel->setMenuItems($this->items());
		$excel->generate();
		ob_end_flush();
	}
	
	/**
	 * Returns ALL the page navigation type matches to avoid excess queries for often one value
	 * @return array
	 */
	public function pagesNavigationTypeMatches() : array
	{
		if(is_null($this->pages_navigation_type_matches))
		{
			$this->pages_navigation_type_matches = [];

			$rows = TC_Memcached::get('pages_navigation_matches');
			if(!is_array($rows))
			{
				$query = "SELECT * FROM `pages_navigation_matches`";
				$result = $this->DB_Prep_Exec($query);
				$rows = $result->fetchAll();
				TC_Memcached::set('pages_navigation_matches', $rows);
			}

			$this->pages_navigation_type_matches = $rows;

		}
		
		return $this->pages_navigation_type_matches;
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
}

