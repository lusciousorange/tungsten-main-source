<?php
/**
 * Class TMm_PagesTemplateSetting
 *
 * A setting for a page template
 */
class TMm_PagesTemplateSetting extends TCm_Model
{
	protected int $setting_id;
	protected $title, $code; // [string] = the title assigned
	protected $value = ''; // [string] = the description
	protected $use_html_editor = 0;

	public static $table_name = 'pages_template_settings'; // [string] = The id column for this class
	public static $table_id_column = 'setting_id'; // [string] = The id column for this class
	public static $model_title = 'Template setting';
	
	
	/**
	 * TMm_PagesTemplateSetting constructor.
	 * @param array|int $setting_id
	 */
	public function __construct($setting_id)
	{
		parent::__construct($setting_id, false);
		
		
		if(is_array($setting_id) || ctype_digit($setting_id) )
		{
			$this->setPropertiesFromTable();
		}
		else
		{
			$query = "SELECT * FROM `".static::tableName()."` WHERE code = :code LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('code' => $setting_id));
			if($row = $result->fetch())
			{
			//	$this->id = $row['settin'];
				$this->convertArrayToProperties($row);
			//	$this->installed = true;
			}
			else
			{
				$this->addConsoleWarning('Page Template Setting "'.$setting_id.'" does not exist');
			}
			
	
		}

	}

	/**
	 * Returns the title
	 *
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Returns the code
	 * @return string
	 */
	public function code()
	{
		return $this->code;
	}
	
	/**
	 * Returns the value
	 * @return string
	 */
	public function value()
	{
		if($this->useHTMLEditor())
		{
			return $this->value;
		}
		return nl2br($this->value);
	}

	/**
	 * Returns if this setting uses the HTML Editor
	 * @return bool
	 */
	public function useHTMLEditor()
	{
		return $this->use_html_editor;
	}


	/**
	 * Converts the setting to a string
	 * @return string
	 */
	public function __toString()
	{
		return $this->value();
	}
	

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title for the setting',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'code' => [
					'title'         => 'Code',
					'comment'       => 'The code for the setting',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'value' => [
					'title'         => 'Value',
					'comment'       => 'The value for the setting which could be long-form values',
					'type'          => 'text',
					'nullable'      => false,
				],
				'use_html_editor' => [
					'title'         => 'Use HTML Editor',
					'comment'       => 'Indicates if the HTML editor should be used for the value',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'user_id' => [
					'delete' => true,
				]
			];
	}

}

?>