<?php

/**
 * Class TMm_PagesUserGroupAccess
 *
 * A representation of one match between a user group and a page. This is currently a shell class used to properly
 * define schemas.
 */
class TMm_PagesUserGroupAccess extends TCm_Model
{
	public static $table_name = 'pages_user_group_access';
	public static $table_id_column = 'access_id';
	public static $model_title = 'Page user group access';
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'menu_id' => [
					'title'         => 'Menu ID',
					'comment'       => 'The menu ID for the page that this access belongs to',
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => false,
					
					// If the page is deleted, we delete this hero box too
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesMenuItem',
						'delete'        => 'CASCADE'
					],
				],
				'group_id' => [
					'title'         => 'User Group ID',
					'comment'       => 'The user group  ID for the group that this access belongs to',
					'type'          => 'TMm_UserGroup',
					'nullable'      => false,
					
					// If the page is deleted, we delete this hero box too
					'foreign_key'   => [
						'model_name'    => 'TMm_UserGroup',
						'delete'        => 'CASCADE'
					],
				],
			];
	}

	
}