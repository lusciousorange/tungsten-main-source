<?php

/**
 * Class TMm_PagesContentVariable
 *
 * A representation of one content variable for a page. This is currently a shell class used to properly
 * define schemas.
 */
class TMm_PagesContentVariable extends TCm_Model
{
	use TSt_ModelWithHistory;
	
	protected int $match_id;
	protected int $content_id;
	protected string $variable_name;
	protected string $value;
	
	public static $table_name = 'pages_content_variables';
	public static $table_id_column = 'match_id';
	public static $model_title = 'Page content variable';
	public static ?string $mirror_table_suffix = '_unpub';
	
	
	//////////////////////////////////////////////////////
	//
	// TSt_ModelWithHistory
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Adjust histories to not return value variable name columns, but instead use them to explain the value.
	 * @param string $column_name
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyChangeAdjustments(string $column_name,
	                                                TSm_ModelHistoryState $current_state,
	                                                ?TSm_ModelHistoryState $previous_state) : ?array
	{
		
		
		// Never show changes on the content_id or the variable name
		// Can't happen
		if($column_name == 'content_id' || $column_name == 'variable_name')
		{
			return null;
		}
		// For variables, use the variable_name as the title
		elseif($column_name == 'value')
		{
			
			$variable_name = $current_state->valueForProperty('variable_name');
			
			if($variable_name == 'responsive_collapse')
			{
				return null;
			}
			
			
			$variable_name = ucfirst(str_replace('_',' ', $variable_name));
			return ['title' => $variable_name];
		}
		
		// Otherwise return "no changes"
		return [];
	}
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'content_id' => [
					'title'         => 'Content ID',
					'comment'       => 'The content ID this variable that this access belongs to',
					'type'          => 'TMm_PagesContent',
					'nullable'      => false,
					
					// If the page is deleted, we delete this hero box too
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesContent',
						'delete'        => 'CASCADE'
					],
				],
				'variable_name' => [
					'title'         => 'Variable Name',
					'comment'       => 'The name of the variable',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'value' => [
					'title'         => 'Value',
					'comment'       => 'The value for this variable',
					'type'          => 'text',
					'nullable'      => true,
					'localization'  => true,
				],
				'user_id' => [
					'delete'		=> true,
				]
			
			];
		
	}

	
}