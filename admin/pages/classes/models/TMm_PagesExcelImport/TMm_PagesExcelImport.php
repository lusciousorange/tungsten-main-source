<?php
/**
 * A class to handle the import of a pages excel file that is being provided
 */
class TMm_PagesExcelImport extends TCm_Model
{
	protected $file;
	protected $highest_row = 0;
	protected $highest_column = 'A';
	
	protected $sheet;
	
	protected $column_names = array();
	protected $column_indices = array();
	
	protected $ignored_column_names = array('menu_id','1','2','3','4','source_url');
	
	// Counters for the import
	/** @var TMm_PagesMenuItem $menus_updated  */
	protected $menus_updated = array();
	
	/** @var TMm_PagesMenuItem $menus_unchanged  */
	protected $menus_unchanged = array();
	
	/** @var TMm_PagesMenuItem $menus_created  */
	protected $menus_created = array();
	
	/**
	 * @var TMm_PagesMenuItem[] $current_menu_for_level The array of menus for a given level which is used to track
	 * what parent
	 * everything should have when creating/updating menus
	 */
	protected $current_menu_for_level = array(0 => null, 1 => null, 2 => null, 3 => null, 4 => null);
	
	/**
	 * All the menu items for each row if they exist
	 * @var TMm_PagesMenuItem[] $menu_items_by_row
	 */
	protected $menu_items_by_row = array();
	
	
	/**
	 * TMm_PagesExcelImport constructor.
	 * @param TCm_File $excel_file
	 */
	public function __construct(TCm_File $excel_file)
	{
		parent::__construct('pages_import');
		
		$this->file = $excel_file;
		
		// Preload all the menus
		TMm_PagesMenuItemList::init();
		
		// Preload all the 301 redirects
		TMm_Pages301RedirectList::init(true);
		
		
		// Setup the menu for level 0, which is referenced when looking at parents
		$menu_0 = TMm_PagesMenuItem::init(0);
		$this->current_menu_for_level[0] = $menu_0;
		
		
	}
	
	/**
	 * Main function to process the file. This must be called manually.
	 */
	public function processFile()
	{
		$this->addConsoleDebug('----- processFile() -----');
		
		$excel = TCm_Excel::initFromFile($this->file);
		
		$this->sheet = $excel->getSheet(0);
		
		// Validate the sheet first
		$is_valid = $this->validateSheet();
		if(!$is_valid)
		{
			TC_message('There is a formatting error with the provided import file',false);
			return;
		}
		
		// If we're here, all good
		// Loop through each row of the worksheet in turn
		// Start at row three
		for ($row = 3; $row <= $this->highest_row; $row++)
		{
			$menu_data = $this->menuDataForRow($row);
			
			// Uses $menu_items_for_row, which is filled during the validation process
			$this->processMenuRow($row, $menu_data);
			
			
			// IDs are in one of the 4 rows and the others should be blank
		//	$menu_id = $menu_data[0] + $menu_data[1] + $menu_data[0] + $menu_data[0];
		//	if(is_int($menu_data[0]))

		}
		
		$this->generateMessageSummary();
		
	}
	
	/**
	 * The validation on the file before any processing is done. This work is front-loaded to identify issues before
	 * any changes are made.
	 * @return bool
	 */
	protected function validateSheet()
	{
		$this->highest_row = $this->sheet->getHighestRow();
		$this->highest_column = $this->sheet->getHighestColumn();
		
		$this->column_names = $this->sheet->rangeToArray('A2:'.$this->highest_column.'2');
		$this->column_names = $this->column_names[0];
		foreach($this->column_names as $index => $name)
		{
			$this->column_indices[$name] = $index;
		}
		
		if(!isset($this->column_indices['folder']))
		{
			TC_message("Missing Column: folder");
			return false;
		}
		
//		if(!isset($this->column_indices['title']))
//		{
//			TC_message("Missing Column: title");
//			return false;
//		}
		
		if($this->column_names[0] != 'menu_id')
		{
			TC_message('First column must be menu_id');
			return false;
		}
		
		// Check for 4 columns of levels
		if(    $this->column_names[2] != '1'
			|| $this->column_names[3] != '2'
			|| $this->column_names[4] != '3'
			|| $this->column_names[5] != '4' )
		
		{
			TC_message('Columns C through F must be numbered 1, 2, 3, and 4 to indicate the four levels');
			return false;
		}
		
		// Loop through each row of the worksheet in turn
		// Start at row three
		$current_level = 0;
		for ($row = 3; $row <= $this->highest_row; $row++)
		{
			$menu_data = $this->menuDataForRow($row);
			
			// save title and folder for easy processing
			//$title = $menu_data['title'];
			$folder = $menu_data['folder'];
			
			// Validate folder name
			if($folder != '/' && !preg_match( '/^[a-zA-Z0-9_-]*$/',$folder  ) )
			{
				TC_message('Row '.$row.' folder name can only container letters, numbers, dashes and underscores');
				return false;
			}
			
			
			// First row
			if($row == 3 && trim($menu_data['1']) == '')
			{
				TC_message('First menu item must exist in level 1');
				return false;
			}
			
			// Check for multiple IDs or level entries
			$num_levels = 0;
			$title = '';
			for($level = 1; $level <= 4; $level++)
			{
				if(trim($menu_data[$level.""]) != '')
				{
					$num_levels++;
					$title = $menu_data[$level.""];
				}
			}
			
			if($num_levels > 1)
			{
				TC_message("Row ".$row." : Only one level can have a page title in it");
				return false;
			}
			
			$level = $this->levelForMenuData($menu_data);
			
			// Check that at least one level is set for a non-blank row
			if(!$level && ( $title != '' || $folder != '') )
			{
				TC_message("Row ".$row." : One level must be set");
				return false;
				
			}
			
			// Check if we've jumped more than one level
			if($level > $current_level && $level - $current_level > 1)
			{
				TC_message("Row ".$row." : Levels can only increase by a maximum one level per row");
				return false;
			}
			
			// No level is set, skip the rest of this loop iteration
			if(!$level)
			{
				continue;
			}
			
			// Save the current level
			$current_level  = $level;
			
			// Load menus and check if they exist
			$menu_id = (int)$menu_data['menu_id'];
			$import_code = $menu_data['import_code'];
			if($menu_id > 0 || $import_code != '')
			{
				// Reset the menu_item variable for each loop
				$menu_item = null;
			
				// We have an actual menu_id, try and find one
				if($menu_id > 0)
				{
					$menu_item = TMm_PagesMenuItem::init($menu_id);
				}
				
				// No menu found, try and find one with import codes
				if(!$menu_item instanceof TMm_PagesMenuItem)
				{
					$menu_item = TMm_PagesMenuItem::pageMenuItemWithImportCode($import_code);
				}
				
				// If we found a menu_item, either way save it
				if($menu_item instanceof TMm_PagesMenuItem)
				{
					$this->menu_items_by_row[$row] = $menu_item;
				}
				elseif($menu_id > 0) // only throw errors for missing menu ids
				{
					TC_message("Row ".$row." : Menu item not found");
					return false;
				}
			}
			else
			{
				$this->menu_items_by_row[$row] = false;
			}
			
		}
		
		
		return true;
	}
	
	/**
	 * Generates the menu data for a given row with indices that match the column names
	 * @param int $row_num
	 * @return array
	 */
	protected function menuDataForRow(int $row_num)
	{

		// Read a row of data into an array
		$raw_menu_data = $this->sheet->rangeToArray('A' . $row_num . ':' . $this->highest_column . $row_num,
		                                  "",
		                                  TRUE,
		                                  FALSE);
		$raw_menu_data = $raw_menu_data[0];
		
		$menu_data = array();
		foreach($raw_menu_data as $column => $value)
		{
			$menu_data[$this->column_names[$column]] = $value;
		}
	
		return $menu_data;
	}
	
	/**
	 * Determines the level for the menu data, which is set by a non-empty value in a column.
	 * @param $menu_data
	 * @return int  value between 1 and 4 indicating the level of the menu
	 */
	protected function levelForMenuData($menu_data)
	{
		for($column = 1; $column <= 4; $column++)
		{
			if($menu_data[$column] != '')
			{
				return $column;
			}
		}
		
		return 0;
	}
	
	/**
	 * Processes the menu data for a given row. Empty rows without a title or folder are skipped
	 * @param int $row
	 * @param array $menu_data
	 */
	protected function processMenuRow(int $row, array $menu_data)
	{
		$folder = $menu_data['folder'];
		$level = $this->levelForMenuData($menu_data);
		
		// Empty Row, Skip
		if($folder == '')
		{
			$this->addConsoleWarning('Skipping Empty Row :'.$row);
			return;
		}
		
		// Check for a menu item, which means an update
		if(isset($this->menu_items_by_row[$row]) &&  $menu_item = $this->menu_items_by_row[$row])
		{
			$is_updated = $this->updateMenuItemWithMenuData($menu_item, $menu_data);
			if($is_updated)
			{
				$this->menus_updated[] = $menu_item;
			}
			else
			{
				$this->menus_unchanged[] = $menu_item;
				
			}
			
		}
		else // create a new one
		{
			$menu_item = $this->createMenuItemWithMenuData($menu_data);
			
			// Create the navigation type match
			$first_type = TMm_PagesNavigationType::init(1);
			if($first_type)
			{
				$menu_item->addNavigationType($first_type);
			}
			
			$this->menus_created[] = $menu_item;
			
		}
		// Update the current menu for level
		$this->current_menu_for_level[$level] = $menu_item;
		
	}
	
	/**
	 * @param array $menu_data
	 * @param bool $is_creator Indicates if the data values are for creating a new item, in which case we need to add
	 * a few more properties
	 * @return array THe array of update or insert values for the given row and menu data
	 */
	protected function databaseValuesForMenuData(array $menu_data, bool $is_creator = false)
	{
		$values = array();
		
		
		foreach($this->column_names as $index => $column_name)
		{
			// Only bother with non-ignored column names
			// Ignore empty column names
			if(trim($column_name) != '' &&  !in_array($column_name, $this->ignored_column_names))
			{
				$values[$column_name] = $menu_data[$column_name];
			}
			
		}
		
		// FIND THE TITLE
		for($column = 1; $column <= 4; $column++)
		{
			if($menu_data[$column] != '')
			{
				$values['title'] = $menu_data[$column];
			}
		}
		
		// Deal with empty folder column
//		if($values['folder'] == '')
//		{
//			$folder_name = $values['title'];
//			$folder_name = strtolower(str_ireplace(' ' ,'_',$folder_name)); // replace spaces with underscore
//			$folder_name = preg_replace("/(\W)+/", "", $folder_name); // only find A-Za-z0-9_
//			$folder_name = strtolower(str_ireplace('_' ,'-',$folder_name)); // replace underscores with dashes
//			$folder_text = new TCu_Text($folder_name);
//			$folder_name = $folder_text->removeAccents();
//			$values['folder'] = $folder_name;
//		}
		
		// Set the parent_id
		$level = $this->levelForMenuData($menu_data);
		$parent = $this->current_menu_for_level[$level -1 ];
		$values['parent_id'] = $parent->id();
		
		if($is_creator)
		{
			$values['display_order'] = $parent->numChildren() + 1;
			if(!isset($values['content_entry']))
			{
				$values['content_entry'] = 'manage';
			}
			if(!isset($values['is_visible']))
			{
				$values['is_visible'] = 1;
			}
			
			$values['redirect_class_target'] = 'referrer'; // consistency with hidden value
		}
		return $values;
	}
	
	/**
	 * Updates a menu item with the provided menu data
	 * @param array $menu_data
	 * @return TMm_PagesMenuItem|null The created item
	 */
	protected function createMenuItemWithMenuData(array $menu_data)
	{
		$db_values = $this->databaseValuesForMenuData($menu_data, true);
		
		$menu_item = TMm_PagesMenuItem::createWithValues($db_values);
		
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			$this->updateMenuItemRedirectsWithMenuData($menu_item, $menu_data);
		}
		return $menu_item;
	}
	
	/**
	 * Updates a menu item with the provided menu data
	 * @param TMm_PagesMenuItem $menu_item
	 * @param array $menu_data
	 * @return bool Indicates if something was updated
	 */
	protected function updateMenuItemWithMenuData(TMm_PagesMenuItem $menu_item, array $menu_data)
	{
		$is_updated = false;
		
		$db_values = $this->databaseValuesForMenuData($menu_data);
		
		// Detect changes in the actual values
		if($menu_item->valuesDifferFromCurrent($db_values))
		{
			$menu_item->updateWithValues($db_values);
			$is_updated = true;
		}
		
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			// Update the redirects and ensure our flag is set in the case
			// Ensure method call first to avoid short-circuiting
			$is_updated = $this->updateMenuItemRedirectsWithMenuData($menu_item, $menu_data) || $is_updated;
		}
		return $is_updated;
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @param array $menu_data
	 * @return bool Indicates if something was updated
	 */
	protected function updateMenuItemRedirectsWithMenuData(TMm_PagesMenuItem $menu_item, array $menu_data)
	{
		$is_updated = false;
		
		$existing_redirects = $menu_item->get301Redirects();
		// Search for existing redirects based on what's seen
		
		if($menu_data['source_url'] == '')
		{
			// There's nothing there and we have some, so delete them all
			if(count($existing_redirects) > 0)
			{
				$menu_item->delete301Redirects();
				$is_updated = true;
			}
		}
		else // we have something in the source_url
		{
			$match_found = false;
			foreach($existing_redirects as $redirect)
			{
				if($redirect->source() == $menu_data['source_url'])
				{
					$match_found = true;
					break; // get out of the for loop
					
				}
			}
			
			if(!$match_found)
			{
				// Create a redirect since we found a different one
				$is_updated = true;
				
				// Explode the source URL to detect multiples
				$urls = explode("\n",$menu_data['source_url']);
				foreach($urls as $source_url)
				{
					if(trim($source_url) != '')
					{
						// Pass through so extending works with languages
						$data = ['source_url' => $source_url];
						$values = $this->generate301RedirectValues($menu_item, $data);
						TMm_Pages301Redirect::createWithValues($values);
					}
				}
				
				
				
			}
			
		}
		// If we're here, we
		
		return $is_updated;
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @param array $menu_data
	 * @return array
	 */
	protected function generate301RedirectValues(TMm_PagesMenuItem $menu_item, array $menu_data)
	{
		return array(
			'source_url' => $menu_data['source_url'],
			'target_page_menu_id' => $menu_item->id()
		);
		
	}
	
	protected function generateMessageSummary()
	{
		TC_message('Processing Complete');
		$text = '<strong>'.count($this->menus_created). " Created</strong>";
		foreach($this->menus_created as $menu_item)
		{
			$text .= '<br />'.$menu_item->id().' | <a target="_blank" href="/admin/pages/do/page-builder/'.$menu_item->id().'">'
				.$menu_item->title(). '</a> |
			<a href="'.$menu_item->pathToFolder().'" target="_blank">'.$menu_item->pathToFolder().'</a>';
		}
		TC_message($text);
		
		$text = '<strong>'.count($this->menus_updated). " Updated</strong>";
		foreach($this->menus_updated as $menu_item)
		{
			$text .= '<br />'.$menu_item->id().' | <a target="_blank" href="/admin/pages/do/page-builder/'
				.$menu_item->id().'">'
				.$menu_item->title(). '</a> |
			<a href="'.$menu_item->pathToFolder().'" target="_blank">'.$menu_item->pathToFolder().'</a>';
		}
		TC_message($text);
		
		$text = '<strong>'.count($this->menus_unchanged). " Unchanged</strong>";
		TC_message($text);
	}
}
