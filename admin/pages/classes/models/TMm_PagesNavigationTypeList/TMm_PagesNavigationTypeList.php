<?php
class TMm_PagesNavigationTypeList extends TCm_ModelList
{
	/**
	 * TMm_PagesNavigationTypeList constructor.
	 * @param bool $init_model_list (Optional) Default true.
	 */
	public function __construct($init_model_list = false)
	{
		parent::__construct('TMm_PagesNavigationType',$init_model_list);
		
	}
	
	/**
	 * @param bool $subset_name
	 * @return TMm_PagesNavigationTypeList[]
	 */
	public function models ($subset_name = false) : array
	{
		return parent::models($subset_name);
	}
	
	
	/**
	 * A processing function that deals with the upgrade to the navigation menus in the system, instead of just some
	 * items to track.
	 * @return void
	 */
	public static function convertToNavigationTypes() : void
	{
		$caller = new TCm_Model(0);
		
		$caller->addConsoleDebug('---- convertToNavigationTypes ----');
		
		$navigation_types = static::init()->models();
		
		// Get out if we've done this before
		if(count($navigation_types) > 0)
		{
			return;
		}
		
		// Convert navigation value to models
		
		$types = TC_getModuleConfig('pages','navigation_types');
		$types = explode(',', $types);
		
		$page_menus = TMm_PagesMenuItemList::init()->items();
		
		$num = 1;
		foreach($types as $navigation_type)
		{
			$values = [
				'title' => $navigation_type,
				'short_code' => strtolower(str_replace(' ', '_', trim($navigation_type))),
			];
			
			$navigation_type = TMm_PagesNavigationType::createWithValues($values);
			
			foreach($page_menus as $menu_item)
			{
				// This page was part of this old navigation item
				$is_visible_value = $menu_item->isVisibleValue();
				if($is_visible_value == $num)
				{
					TMm_PagesNavigationTypeMenuItemMatch::createWithValues(['menu_id' => $menu_item->id(), 'navigation_id' => $navigation_type->id()]);
				}
			}
			
			// Increase the count for finding menu items
			$num++;
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
	
}