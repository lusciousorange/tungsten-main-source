<?php

/**
 * A paring match between one page menu item and one navigation item.
 */
class TMm_PagesNavigationTypeMenuItemMatch extends TCm_Model
{
	protected int $menu_id;
	protected int $navigation_id;
	
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id';
	public static $table_name = 'pages_navigation_matches'; // shorter name to avoid issues
	public static $model_title = 'Page navigation match';
	
	/**
	 * TMm_PageMenuItemNavigationMatch constructor.
	 */
	public function __construct($match_id)
	{
		parent::__construct($match_id);
	}
	
	/**
	 * Returns the page for this match
	 * @return TMm_PagesMenuItem
	 */
	public function page() : TMm_PagesMenuItem
	{
		return TMm_PagesMenuItem::init($this->menu_id);
	}
	
	/**
	 * Returns the navigation for this match
	 * @return TMm_PagesNavigationType
	 */
	public function navigation() : TMm_PagesNavigationType
	{
		return TMm_PagesNavigationType::init($this->navigation_id);
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'menu_id' => [
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => false,
					
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesMenuItem',
						'delete'        => 'CASCADE'
					],
				],
				
				'navigation_id' => [
					'type'          => 'TMm_PagesNavigationType',
					'nullable'      => false,
					
					'foreign_key'   => [
						'model_name'    => 'TMm_PagesNavigationType',
						'delete'        => 'CASCADE'
					],
				],
				
				'display_order' => [
					'comment'       => 'The display order for menu items if they are displayed as flat items',
					'type'          => 'tinyint',
					'nullable'      => true,
					
				],
				
				// Unique constraint on user-group matches
				'table_keys' => [
					'menu_navigation_id' => ['index' => "(`menu_id`,`navigation_id`)", 'type' => 'unique'],
				]
				
			];
	}
	
	
	
}