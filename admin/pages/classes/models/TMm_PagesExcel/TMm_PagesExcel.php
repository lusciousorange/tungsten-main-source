<?php
class TMm_PagesExcel extends TCm_Excel
{
	/** @var TMm_PagesMenuItem[] $pages */
	protected $menu_items;
	protected $filter_title = 'Pages';
	protected $default_column_widths = array(
		'title' => 50,
		'folder' => 25,
		'description' => 70,
		'source_url' => 60
	);
	
	protected $columns = array(
		'menu_id',
		'import_code',
		'1',
		'2',
		'3',
		'4',
		'folder',
		'meta_description',
	);
	protected $letters_for_number = array('','A','B','C','D','E','F','G','H','I','J','K','L');
	protected $last_column = 'A';
	
	protected $heading_style = array(
		'font' => array(
			'bold' => true,
			'size' => 12,
			'color' => array('rgb' => 'FFFFFF'),
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => '333333')
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				'color' => array('rgb' => 'FFFFFF')
			)
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		)
	);
	
	protected $title_style = array(
		'font' => array(
			'bold' => true,
			'size' => 12,
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'DDDDDD')
		),
	
	);
	
	
	public function __construct()
	{
		// Never show deprecated when generating Excel
		error_reporting(error_reporting() & ~E_DEPRECATED);

		parent::__construct('pages_list');
		
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			$this->columns[] = 'source_url';
		}
		
		$this->sheet->setTitle('Pages');
		$this->setFilename('Pages');
		
		$this->last_column = $this->letters_for_number[count($this->columns)];
		
		
	}
	
	/**
	 * @param TMm_PagesMenuItem[] $menu_items
	 */
	public function setMenuItems(array $menu_items)
	{
		$this->menu_items = $menu_items;
	
		// Create Header Row
		$this->createHeaderRow();
		
		// Process Values
		$this->processValues();
		
		
	}
	
	/**
	 * Creates the header row
	 * @throws PHPExcel_Exception
	 */
	public function createHeaderRow()
	{
		// Auto height for everything
		$this->sheet->getDefaultRowDimension()->setRowHeight(-1);
		
		// Create the heading rows
		$letter = 'A';
		
		// Set a taller row height
		$this->sheet->getRowDimension('1')->setRowHeight('20');
		
		// ----- HEADING ROW ROW -----
		
		// Level + ID columns
		$this->setCellValue($letter++.$this->current_row, 'Codes');
		
		// Title columns
		$this->setCellValue('C'.$this->current_row, 'Page titles');
		
		
		// Property columns
		$this->setCellValue('G'.$this->current_row, 'Page properties');
		
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			$this->setCellValue('I'.$this->current_row, '301 Redirect and Import Source');
		}
		
		$this->setCellStyle('A1:'.$this->last_column.'1', $this->heading_style);
		
		// Redirect and Import URLs
		$this->nextRow();
		
		// ----- TITLE ROW -----
		
		// Create the title rows
		$letter = 'A';
		foreach($this->columns as  $title)
		{
			$this->setCellValue($letter++.$this->current_row, $title);
		}
		
		$this->setCellStyle('A2:'.$this->last_column.'2', $this->title_style);
		$this->nextRow();
	
		
	}
	
	/**
	 * Loops through the menu items and creates a row for each one
	 */
	protected function processValues()
	{
		
		foreach($this->menu_items as $menu_item)
		{
			$this->processMenuItem($menu_item);
			$this->nextRow();
		}
		
		// POST PROCESSING
		
		// Merge Top Row
		$this->sheet->mergeCells('A1:B1');
		$this->sheet->mergeCells('C1:F1');
		$this->sheet->mergeCells('G1:H1');
		
		
		// Title
//		$folder_column = 'G';
//		$this->sheet->getColumnDimension($folder_column)->setAutoSize(false);
//		$this->sheet->getColumnDimension($folder_column)->setWidth($this->default_column_widths['title']);
//
		// Folder
		$folder_column = 'G';
		$this->sheet->getColumnDimension($folder_column)->setAutoSize(false);
		$this->sheet->getColumnDimension($folder_column)->setWidth($this->default_column_widths['folder']);
		
		
		// Description
		$description_column = 'H';
		$this->sheet->getColumnDimension($description_column)->setAutoSize(false);
		$this->sheet->getColumnDimension($description_column)->setWidth($this->default_column_widths['description']);
		$this->sheet->getStyle($description_column.'3:'.$description_column.$this->current_row)
		            ->getAlignment()
		            ->setWrapText(true);
		
		// Source
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			$folder_column = 'I';
			$this->sheet->getColumnDimension($folder_column)->setAutoSize(false);
			$this->sheet->getColumnDimension($folder_column)->setWidth($this->default_column_widths['source_url']);
		}
		
		// Freeze the top two rows
		$this->sheet->freezePane('A3');
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 */
	protected function processMenuItem(TMm_PagesMenuItem $menu_item)
	{
		$letter = 'A';
		
		$this->setCellValue($letter++.$this->current_row, $menu_item->id());
		$this->setCellValue($letter++.$this->current_row, $menu_item->importCode());
		
		
		// Ensure IDs appear in the correct level
		if($menu_item->level() == 1)
		{
			$this->setCellValue($letter++.$this->current_row, $menu_item->title());
			$letter++;
			$letter++;
			$letter++;
		}
		elseif($menu_item->level() == 2)
		{
			$letter++;
			$this->setCellValue($letter++.$this->current_row, $menu_item->title());
			$letter++;
			$letter++;
		}
		elseif($menu_item->level() == 3)
		{
			$letter++;
			$letter++;
			$this->setCellValue($letter++.$this->current_row, $menu_item->title());
			$letter++;
		}
		elseif($menu_item->level() == 4)
		{
			$letter++;
			$letter++;
			$letter++;
			$this->setCellValue($letter++.$this->current_row, $menu_item->title());
		}
		
		
		$this->setCellValue($letter++.$this->current_row, $menu_item->folder());
		$this->setCellValue($letter++.$this->current_row, $menu_item->description());
		
		// 301 Redirect
		
		if(TC_getModuleConfig('pages','use_301_redirects'))
		{
			$this->setCellValue($letter++ . $this->current_row, $this->redirectURLForMenuItem($menu_item));
		}
		
		
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return string The url to load
	 */
	protected function redirectURLForMenuItem(TMm_PagesMenuItem $menu_item)
	{
		$url = '';
		$redirects = $menu_item->get301Redirects();
		foreach($redirects as $redirect)
		{
			return $redirect->source();
		}
		
		return $url;
	}
	
}