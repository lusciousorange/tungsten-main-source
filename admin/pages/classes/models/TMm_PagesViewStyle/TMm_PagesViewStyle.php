<?php
/**
 * Class TMm_PagesViewStyle
 *
 * A setting for a page template. This class was originally designed to work with the values in the database, however
 * the primary key was changed to the classname as part of a shift that moved these values into the theme file. The
 * styles are now defined in the static componentViewStyles() method in the specific theme.
 */
class TMm_PagesViewStyle extends TCm_Model
{
	protected int $style_id;
	
	protected string $title;
	protected string $css_class;
	protected string $description;
	protected string $required_view_classes;

	protected ?array $required_classes = null;
	
	protected bool $apply_to_container = false;
	
	
	public static $table_name = 'pages_css_styles';
	public static $table_id_column = 'css_class'; // changed from an ID as part of the upgrade to the theme-based setup
	public static $model_title = 'CSS style';
	public static $primary_table_sort = "title ASC";
	
	/**
	 * TMm_PagesTemplateSetting constructor.
	 * @param array|int $css_class
	 */
	public function __construct($css_class)
	{
		
		if(is_array($css_class))
		{
			parent::__construct($css_class);
		}
		
		// CSS class passed in, instantiate using that value
		elseif(is_string($css_class))
		{
			$mem_key = 'TMm_PagesViewStyle_'.$css_class;
			$row = TC_Memcached::getArrayValueOrRun($mem_key, function() use($css_class){
				$query = "SELECT * FROM `pages_css_styles` WHERE css_class = :css_class LIMIT 1";
				$result = $this->DB_Prep_Exec($query, ['css_class' => $css_class]);
				if($row = $result->fetch())
				{
					return $row;
				}
				
				return [];
				
			});
			
			if(count($row) > 0) // has values
			{
				parent::__construct($row);
			}
			else
			{
				$this->destroy();
				return null;
			}
		}
		
		
		
	}

	/**
	 * Returns the title
	 *
	 * @return string
	 */
	public function title() : string
	{
		return $this->title;
	}

	/**
	 * Returns the description
	 *
	 * @return string
	 */
	public function description() : string
	{
		return $this->description;
	}

	/**
	 * Returns the class name
	 * @return string
	 */
	public function className() : string
	{
		return $this->css_class;
	}
	

	public function requiredViewClasses() : string
	{
		return $this->required_view_classes;
	}
	
	
	/**
	 * Returns if this style is applied to the content container instead of the element itself. This only works for
	 * content items, not all items.
	 * @return bool
	 */
	public function applyToContentContainer() : bool
	{
		return $this->apply_to_container;
	}
	
	
	/**
	 * checks if the provided view class is permitted for this style
	 * @param string $view_class_name
	 * @return bool
	 */
	public function viewClassIsPermitted(string $view_class_name) : bool
	{
		$required_class_string = trim($this->requiredViewClasses());
		// OPTION 1 : Nothing required
		if($required_class_string == '')
		{
			return true;
		}
		
		// Lazy load
		if($this->required_classes == null)
		{
			$this->required_classes = explode(' ',$required_class_string);
		}
		
		
		// Loop through the array of classes
		foreach($this->required_classes as $required_class_name)
		{
			// Fast and easy, always true
			if($view_class_name == $required_class_name)
			{
				return true;
			}
			// ----- SPECIAL CASES -----
			
			// SECTION
			if($required_class_name == 'TMv_ContentLayout_Section')
			{
				// If we're dealing with a subclass of a layout section
				if(is_subclass_of($view_class_name, 'TMv_ContentLayout_Section'))
				{
					return true;
				}
				continue; // Go to the next required class
			}
			
			// ROW
			if($required_class_name == 'TMv_ContentLayout')
			{
				if(is_subclass_of($view_class_name, 'TMv_ContentLayout') // IS A LAYOUT BUT
					&& ! is_subclass_of($view_class_name, 'TMv_ContentLayout_Section')) // NOT A SECTION
				{
					return true;
				}
				continue; // Go to the next required class
			}
			
			// COLUMNS
			if($required_class_name == 'TMv_ContentLayoutBlock')
			{
				if(is_subclass_of($view_class_name, 'TMv_ContentLayoutBlock'))
				{
					return true;
				}
				continue; // Go to the next required class
			}
			
			// CONTENT VIEWS
			if($required_class_name == 'TMt_PagesContentView')
			{
				// Ignore sections, rows and columns
				if( $view_class_name !== 'TMv_ContentLayoutBlock' // Not a content block
					&& !is_subclass_of($view_class_name, 'TMv_ContentLayout_Section')
					&& ! is_subclass_of($view_class_name, 'TMv_ContentLayout')
					&& ! is_subclass_of($view_class_name, 'TMv_ContentLayoutBlock')
				)
				{
					return true;
				}
				continue; // Go to the next required class
			}
			
			// CHECKING FOR SUBCLASSES NOT YET CAUGHT
			if(is_subclass_of($view_class_name, $required_class_name))
			{
				return true;
			}
				
			// TRAITS , return if it matches
			if(substr($required_class_name, 0, 3) == 'TMt')
			{
				$traits = class_uses($view_class_name);
				
				// Only bother with classes that use TMt_PageContentView AND aren't content layouts
				if(isset($traits[$required_class_name]))
				{
					return true;
				}
			}
		}
		

		return false;
	}
	

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title for the style',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'css_class' => [
					'title'         => 'CSS Class',
					'comment'       => 'The CSS class that this style is associated with',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'description' => [
					'title'         => 'Description',
					'comment'       => 'A brief description of the CSS class',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'required_view_classes' => [
					'title'         => 'Required View Classes',
					'comment'       => 'Comma-separate list of views that are allowed to have this style',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'apply_to_container' => [
					'comment'       => "Indicates if this style is applied to the content container",
					'type'          => 'bool',
					'nullable'      => false,
				],
				'user_id' => [
					'delete'       => true,
				],
			
			];
	
	
	}
	
}

?>