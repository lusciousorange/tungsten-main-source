<?php

/**
 * Class TMm_Pages301Redirect
 *
 * Represents a single page redirect that exists in teh system. It tracks an "old" URL and then if that URL exists,
 * it redirects to the new target. That target can be a page or another model which uses TMt_Page301Redirectable.
 *
 * @uses TMt_Page301Redirectable
 */
class TMm_Pages301Redirect extends TCm_Model
{
	use TSt_ModelWithHistory;
	
	protected int $redirect_id;
	
	protected $source_url, $source_url_cleaned, $target_page_menu_id, $url_suffix, $class_name, $item_id;
	protected string $language;
	
	public static $table_name = 'page_301_redirects';
	public static $table_id_column = 'redirect_id'; // [string] = The id column for this class
	public static $model_title = '301 redirect';
	
	/**
	 * @param $id
	 * @param bool $load_from_table
	 */
	public function __construct($id, $load_from_table = true)
	{
		parent::__construct($id, $load_from_table);
		
		// Find missing ones, also works to clear out bad ones that were processed wrong and need to be re-processed
		if($this->source_url_cleaned == '')
		{
			$this->updateDatabaseValue('source_url_cleaned', self::cleanSourceURL($this->source()));
		}
	}
	
	/**
	 * @return string
	 */
	public function source()
	{
		return $this->source_url;
	}
	
	/**
	 * Returns the model that this 301 redirect targets. This is either a page or model that is redirectable
	 * @return TMm_PagesMenuItem|TMt_Page301Redirectable
	 */
	public function targetModel()
	{
		if($this->target_page_menu_id != null)
		{
			return TMm_PagesMenuItem::init($this->target_page_menu_id);
		}
		else // model
		{
			return ($this->class_name)::init($this->item_id);
		}
		
	}
	
	/**
	 * @return string
	 */
	public function urlSuffix()
	{
		return $this->url_suffix;
	}
	
	/**
	 * @return string
	 */
	public function fullTargetUrl()
	{
		return $this->targetModel()->pageViewURLPath().$this->urlSuffix();
	}
	
	/**
	 * Returns the cleaned source URL
	 * @return string
	 */
	public function cleanedSourceURL()
	{
		return $this->source_url_cleaned;
	}
	
	/**
	 * Cleans a source URL to be a consistent format for the redirect process
	 * @param string $url
	 * @return string
	 */
	public static function cleanSourceURL($url)
	{
		// Remove protocol and domain if it exists
		$cleaned_string = trim($url);
		
		// Detect a basic word which means it's a folder
		if(preg_match('/^(\w)*$/', $cleaned_string))
		{
			$cleaned_string = '/'.$cleaned_string;
		}
		else // cleaning required
		{
			// Remove everything up to and including the domain name and any number of subdomains (www)
			$cleaned_string = preg_replace('#^(http(s)?://)?(\w*)+(\.\w*)*#','', $cleaned_string);
			
			$parts = explode('?', $cleaned_string, 2);
			
			$cleaned_string = $parts[0];
			$cleaned_string = str_replace('//','/', $cleaned_string); // Fix double-slashes
			$cleaned_string = rtrim($cleaned_string,"/");
			
			$params = false;
			if(isset($parts[1]))
			{
				$cleaned_string .= '?'.$parts[1];
			}
			
		}
		
		return $cleaned_string;
	}
	
	/**
	 * Extended creation functionality to deal with adding cleaned urls
	 * @param array $values
	 * @param $bind_param_values
	 * @param $return_new_model
	 * @return int|TCm_Model|TMm_Pages301Redirect|TMt_WorkflowItem|null
	 */
	public static function createWithValues(array $values, $bind_param_values = array(), $return_new_model = true)
	{
		// Deal with source cleaned
		if(!isset($values['source_url_cleaned']))
		{
			$values['source_url_cleaned'] = self::cleanSourceURL($values['source_url']);
		}
		return parent::createWithValues($values, $bind_param_values, $return_new_model);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Adjust histories to not return value variable name columns, but instead use them to explain the value.
	 * @param string $column_name
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyChangeAdjustments(string $column_name,
	                                                TSm_ModelHistoryState $current_state,
	                                                ?TSm_ModelHistoryState $previous_state) : ?array
	{
		
		
		// Never show the class_name, won't change, never matters for display
		// Also we use the cleaned version, so ignore the source
		if($column_name == 'class_name' || $column_name == 'source_url')
		{
			return null;
		}
		
		
		// Otherwise return "no changes"
		return [];
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'source_url' => [
					'title'         => 'Source URL',
					'comment'       => 'The original URL',
					'type'          => 'mediumtext',
					'nullable'      => false,
				],
				'source_url_cleaned' => [
					'title'         => 'Source URL cleaned',
					'comment'       => 'The original URL cleaned',
					'type'          => 'mediumtext',
					'nullable'      => false,
				],
				'target_page_menu_id' => [
					'title'         => 'target page menu id',
					'comment'       => 'The ID for the page menu if it exists',
					'type'          => 'TMm_PagesMenuItem',
					'nullable'      => true,
				],
				'url_suffix' => [
					'title'         => 'URL Suffix',
					'comment'       => 'the suffix that should be appended to the URL',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'language' => [
					'title'         => 'Language',
					'comment'       => 'The language for the redirect. Used in localization.',
					'type'          => 'varchar(8) DEFAULT "en"',
					'nullable'      => false,
				],
				
				'class_name' => [
					'title'         => 'Class Name',
					'comment'       => 'The name of the model class used for this redirect',
					'type'          => 'varchar(128)',
					'nullable'      => true,
				],
				'item_id' => [
					'title'         => 'Item ID',
					'comment'       => 'The id that corresponds to the class name for the item',
					'type'          => 'int(10) unsigned',
					'nullable'      => true,
				],
			
			
			
			];
		
	}
	
	
}