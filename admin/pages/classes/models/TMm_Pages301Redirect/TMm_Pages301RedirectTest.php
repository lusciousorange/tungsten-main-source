<?php
class TMm_Pages301RedirectTest extends TC_ModelTestCase
{
	
	/**
	 * The data provider that returns an array of values and results to be tested
	 */
	public function provideCleanSourceUrlData() : array
	{
		$data = array();
		
		// Leading slash, and a folder with
		$data['leading slash, no protocol, no domain, no params'] = [
				"/test",
				"/test"
			];
		
		$data['protocol, domain, no params'] = [
				"/test",
				"http://domain.com/test"
			];
		
		$data['protocol(s), domain, no params'] = [
				"/test",
				"https://domain.com/test"
			];
		
		$data['protocol, www, domain, no params'] = [
				"/test",
				"https://www.domain.com/test"
			];
		
		$data['no protocol, domain, no params'] = [
				"/test",
				"domain.com/test"
			];
		
		$data['full domain, nothing trailing'] = [
				"",
				"https://www.domain.com"
			];
		
		$data['trailing slash removed, no params'] = [
				"/test",
				"http://domain.com/test/"
			];
		
		$data['trailing slash removed, with params'] = [
				"/test?",
				"http://domain.com/test/?"
			];
		
		$data['filename with params'] = [
				"/index.html?",
				"http://domain.com/index.html?"
			];
		
		$data['no domain, with params and trailing slash'] = [
				"/path?b=4",
				"/path/?b=4"
			];
		$data['double-backslash'] = [
				"/path?b=4",
				"//path/?b=4"
			];
		
		$data['basic text no slashes'] = [
				"/abc",
				"abc"
			];
		
		
		return $data;
	}
	
	/**
	 * @dataProvider provideCleanSourceUrlData
	 * @param string $expected_result THe expected result of this test
	 * @param string $input The provided input for the test
	 */
	public function testCleanSourceURL(string $expected_result, string $input )
	{
		$cleaned = TMm_Pages301Redirect::cleanSourceURL($input);
		
		$this->assertSame($expected_result, $cleaned);
		
	}
	
}