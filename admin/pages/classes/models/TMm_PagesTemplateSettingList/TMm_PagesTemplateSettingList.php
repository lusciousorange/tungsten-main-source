<?php
/**
 * Class TMm_PagesTemplateSettingList
 *
 * A model list of all the page template settings
 */
class TMm_PagesTemplateSettingList extends TCm_ModelList
{

	/**
	 * TMm_PageContentList constructor.
	 * @param bool $init_model_list (Optional) Default true.
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_PagesTemplateSetting',$init_model_list);
		
	}

	/**
	 * @param bool $subset_name
	 * @return TMm_PagesTemplateSetting[]
	 */
	public function models ($subset_name = false)
	{
		return parent::models($subset_name);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
	
}

?>