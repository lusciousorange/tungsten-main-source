<?php
/**
 * Class TMm_PageRendererContent
 *
 * A page content item used in the general case for page renderers.
 */
class TMm_PageRendererContent extends TCm_Model
{
	use TMt_PageRenderItem;
	use TSt_ModelWithHistory {
		TMt_PageRenderItem::historyChangeAdjustments insteadof TSt_ModelWithHistory;
		TMt_PageRenderItem::historyHeadingAdjustments insteadof TSt_ModelWithHistory;
		
	}
	
	protected string $content_title = '';
	protected string  $description;
	protected $menu_id;
	protected $icon = false;
	protected $indent_level = false;
	protected string $visibility_condition = '';

	protected $container_content = false;
	
	public static $table_name = 'pages_renderer_content'; // [string] = The id column for this class
	public static $table_id_column = 'content_id'; // [string] = The id column for this class
	public static $model_title = 'Page renderer content';
	public static ?string $mirror_table_suffix = '_unpub';
	
	
	/**
	 * TMm_PagesContent constructor.
	 * @param array|int $content_id
	 */
	public function __construct($content_id)
	{
		parent::__construct($content_id);
		$this->setPropertiesFromTable('pages_renderer_content');
	}

	/**
	 * Returns the upload folder for the view
	 * @return string
	 */
	public static function uploadFolder() : string
	{
		return $_SERVER['DOCUMENT_ROOT'].'/assets/page_renderer/';
	}
	
	/**
	 * @return class-string<TCm_Model>
	 */
	public static function variablesModelClass() : string
	{
		return 'TMm_PageRendererContentVariable';
	}
	/**
	 * Returns the title
	 * @return string
	 */
	public function title() : string
	{
		return $this->content_title;
	}
	
	/**
	 * Returns the description for this content item
	 * @return string
	 */
	public function description() : string
	{
		return $this->description;
	}
	
	/**
	 * Returns the view class for this content view
	 * @return string
	 */
	public function viewClass() : string
	{
		return $this->view_class;
	}
	
	/**
	 * Returns the menu_id for the content view
	 * @return int
	 */
	public function menuID() : int
	{
		return $this->menu_id;
	}
	
	/**
	 * Returns the menu item
	 * @return null|TMm_PagesMenuItem
	 */
	public function menuItem() : ?TMm_PagesMenuItem
	{
		return TMm_PagesMenuItem::init( $this->menu_id);
	}

	public function visibilityCondition() : string
	{
		return $this->visibility_condition;
	}


	/**
	 * Returns if the content item is visible
	 * @return bool
	 */
	public function isVisible() : bool
	{

		$condition = $this->visibilityCondition();
		if($condition != '')
		{
			$text = new TCu_Text($condition);

			return $text->evaluateTemplateConditionAsBoolean();
		}

		return $this->is_visible;
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES – TSt_ModelWithHistory
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the array of related models that are associated with this model for history purposes. This is usually
	 * used to show the complete history of changes to a model, by pulling in history from other related models.
	 * For example, changing the user groups for a TMm_User shows up in the user's history. News categories are
	 * another example.
	 * @return array[]
	 */
	public static function historyRelatedModels() : array
	{
		return [
			[
				'model_name' => 'TMm_PageRendererContentVariable',
				'reference_column_name' => 'content_id',
				'heading_display_column_id'     => 'variable_name',
			],
		];
	}

	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema() : array
	{
		return parent::schema() + [
				
				'title' => [
					'title'         => 'Title',
					'comment'       => 'The title for the content',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'localization'  => true,
				],
				'description' => [
					'title'         => 'Description',
					'comment'       => 'The description for the content',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'item_class_name' => [
					'title'         => 'Item Class Name',
					'comment'       => 'The class name, which can come from a variety of sources who can render content',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'item_id' => [
					'title'         => 'Item ID',
					'comment'       => 'The ID for the item that this content belongs to',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					
				],
				
				
				'table_keys' => [
					// Index for when we reference ID and class_name, which happens a lot
					'item_id_class_name' => ['index' => "`item_id`,`item_class_name`", 'type' => 'normal'],
				],
			
			
			];
		
	}
	
	public static function installUpdateQueries()
	{
		return [
			'7.0.3' => [
				// item_id changed to use varchar, so we can store the name of the dashboard, instead of the id
				// Need to fix all the old IDs of 0 to point to `main`
				"UPDATE pages_renderer_content SET item_id = 'main' WHERE item_id = 0 AND item_class_name = 'TMm_Dashboard'",
			],
			
		];
	}


}

?>