<?php

/**
 * Class TMm_PagesModule
 *
 * An extension of the normal module to add functionality specific to this module.
 */
class TMm_PagesModule extends TSm_Module
{
	protected $page_specific_user_groups;
	
	/**
	 * TSm_Module constructor.
	 * @param array|int $module_id
	 */
	public function __construct($module_id = 'pages')
	{
		parent::__construct($module_id);
		
		// ensure this module is always auto-updated
		// since it's core to the operation of the system
		$this->setAsAutoUpdate();
	}
	
	/**
	 * Indicates if this website uses 301 Redirects
	 * @return bool
	 */
	public function use301Redirects()
	{
		return TC_getModuleConfig('pages','use_301_redirects');
	}
	
	/**
	 * Indicates if this website uses 301 Redirects
	 * @return bool
	 */
	public function useBulkImport()
	{
		return TC_getModuleConfig('pages','use_bulk_import');
	}
	
	/**
	 * Extend user functionality to include users with page-specific access
	 * @param TMm_User $user
	 * @return bool
	 */
	public function checkUserPermission(TMm_User $user) :bool
	{
		$access = parent::checkUserPermission($user);
		
		// No permission yet, try page specific
		if(!$access)
		{
			$page_specific_groups = $this->userGroupsWithPageSpecificAccess();
			foreach($page_specific_groups as $user_group)
			{
				if($user->inGroup($user_group))
				{
					return true;
				}
			}
			
		}
		return $access;

	}
	
	/**
	 * Gets the theme name for a given user. If someone is logged in, we can manually set the theme that loads for
	 * them. This can be used during development to ensure certain people see different interfaces at different times.
	 *
	 * Iin the config for the site, add a valued called `user_themes` with an array of values where each index for
	 * that array is a specific User ID. Then make the value for that index the view name. This could mean that you
	 * have an array with several indices that all have the same value, which is normal.
	 *
	 * Setting these will override whatever value is set for both the editor and the live modes. They will ALWAYS see
	 * that theme.
	 * @return string|null
	 */
	public function getUserThemeName() : ?string
	{
		// Determine if the user has a specific theme set
		if($user = TC_currentUser())
		{
			if(TC_configIsSet('user_themes'))
			{
				$user_themes = TC_getConfig('user_themes');
				if(isset($user_themes[$user->id()]))
				{
					return trim($user_themes[$user->id()]);
				}
			}
			
		}
		
		return null;
	}
	
	/**
	 * Returns the name of the theme to be used. This function can also hook into config value called `user_themes`
	 * which has an array indexed by user ids which forces a specific theme.
	 * @return class-string<TMv_PagesTheme>|null The name of the theme being used. Null indicates no theme is to be
	 * loaded
	 */
	public function themeName() : ?string
	{
		// Try to get the user theme
		$user_theme_name = $this->getUserThemeName();
		if(is_string($user_theme_name))
		{
			return $user_theme_name;
		}
		
		$theme_name = trim($this->variable('website_theme'));
		
		if($theme_name == '')
		{
			return null;
		}
		
		
		return $theme_name;
		
	}
	
	/**
	 * Returns the editor theme name. This requires comparing two values for what is the theme.
	 * If a user theme is set in the config array of `user_themes` then those will be used instead
	 * @return null|class-string<TMv_PagesTheme> The name of the theme being used
	 */
	public function editorThemeName() : ?string
	{
		// Try to get the user theme
		$user_theme_name = $this->getUserThemeName();
		if(is_string($user_theme_name))
		{
			return $user_theme_name;
		}
		
		$theme_name = $this->themeName();
		$editor_theme_name = $this->variable('website_theme_editor');
		if($editor_theme_name != '' && $editor_theme_name)
		{
			$theme_name = $editor_theme_name;
			
		}
		
		
		return $theme_name;
	}
	
	
	/**
	 * Checks if the user provided has full access to the Pages module
	 * @param ?TMm_User $user The user to be tested. Defaults to the current user if not eet
	 * @return bool
	 */
	public function userHasFullAccess(?TMm_User $user = null) : bool
	{
		if(!$user)
		{
			$user = TC_currentUser();
		}
		
		return parent::checkUserPermission($user);
	}
	
	/**
	 * returns the array of user groups that have access to at least on page
	 * @return TMm_UserGroup[]
	 */
	public function userGroupsWithPageSpecificAccess()
	{
		if($this->page_specific_user_groups == null)
		{
			$this->page_specific_user_groups = array();
			
			$query = "SELECT g.* FROM pages_user_group_access a INNER JOIN user_groups g USING(group_id) GROUP BY a.group_id ";
			$result = $this->DB_Prep_Exec($query);
			while($row = $result->fetch())
			{
				$this->page_specific_user_groups[] = TMm_UserGroup::init($row);
			}
			
		}
		
		
		
		return $this->page_specific_user_groups;
	}
	
	
}





