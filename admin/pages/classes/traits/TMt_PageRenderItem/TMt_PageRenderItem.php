<?php
/**
 * Trait TMt_PageRenderItem
 *
 * A trait which adds the ability to have this item be rendered in a TMv_PageContentRendering view. This can be added to other
 * models that will operate with the same functionality.
 *
 * Using this trait requires some manual setup of databases to have the following properties for these items.
 * "view_class text NOT NULL",
 * "container_id int(10) unsigned NOT NULL",
 * "position_layout int(10) unsigned NOT NULL",
 * "position_block int(10) unsigned NOT NULL",
 * "position_content int(10) unsigned NOT NULL",
 * "id_attribute text NOT NULL",
 * "css_classes text NOT NULL",
 */
trait TMt_PageRenderItem
{
	protected $id_attribute = '', $container_id;
	protected $is_visible, $view_class;
	protected int $position_layout = 0;
	protected int $position_row = 0;
	protected int $position_content = 0;
	protected int $position_block = 0;
	
	protected $item_class_name, $item_id;
	protected $css_classes; // The CSS class names assigned to this content item
	protected $variables = null; //  The values of the different variables saved for this content item. There can be up to 9 of them
	protected $variable_rows = [];
	protected $delete_occurred = false;
	
	protected bool $is_section_fixed = false;
	
	
	protected int $display_order = 0;
	/**
	 * Returns the ID attribute
	 * @return string
	 */
	public function idAttribute() : string
	{
		if($this->hasOverrideIDAttribute())
		{
			return $this->id_attribute;
		}
		else
		{
			return 'pages_content_'.$this->id();
		}
	}
	
	
	/**
	 * Returns if this item has an override ID attribute
	 * @return bool
	 */
	public function hasOverrideIDAttribute() : bool
	{
		return $this->id_attribute != '';
	}


	/**
	 * Returns the view class for this dashboard view
	 * @return string
	 */
	public function viewClass() : string
	{
		return $this->view_class;
	}
	
	/**
	 * Returns the renderer for this item
	 * @return null|TCm_Model|TMt_PageRenderer
	 */
	public function renderer() : ?TCm_Model
	{
		return ($this->item_class_name)::init($this->item_id);
	}
	
	/**
	 * Returns the class name for the renderer.
	 * @return string
	 */
	public function rendererClassName() : string
	{
		// Must instantiate in order to deal with parents and extensions
		return get_class($this->renderer());
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// VISIBILITY
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns if this dashboard is visible, which is always true
	 * @return bool
	 */
	public function isVisible() : bool
	{
		return true;
	}


	/**
	 * Toggles the visibility for this content item
	 */
	public function toggleVisibility() : void
	{
		$query = "UPDATE `".self::tableName()."`  SET is_visible = !is_visible WHERE ".self::$table_id_column." = :id";
		$result = $this->DB_Prep_Exec($query, array('id'=>$this->id()));
		$this->is_visible = !$this->is_visible;
	}

	//////////////////////////////////////////////////////
	//
	// CONTAINERS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * The array of content that exists within this item.
	 * @param bool $force_reload Indicates if the container content should be reloade
	 * @return TMm_PagesContent[]
	 */
	public function containerContent(bool $force_reload = false) : array
	{
		if($this->container_content === false || $force_reload)
		{
			$this->container_content = array();
			
			$pre_loaded_content = $this->renderer()->contentModelsForContainer($this);
			if(is_array($pre_loaded_content))
			{
				foreach($pre_loaded_content as $content)
				{
					$this->container_content[$content->id()] = $content;
				}
			}
			else // Go get the containers
			{
				
				/** @var TMt_PagesContentView $class_name */
				$class_name = $this->viewClass();
				
				if(class_exists($class_name) && $class_name::pageContent_IsAContainer())
				{
					// The first sorting term, position_xyz = 0, evaluates to True,
					// otherwise it evaluates to False. True sorts AFTER False so, the first sorting criterion
					// makes sure that rows with the display_order of 0 are sorted at the end of the list.
					
					
					//$db_name = get_called_class()::pr
					$query = "SELECT * FROM `" . self::tableName() . "`
					WHERE container_id = :container_id ORDER BY
					position_layout=0, position_layout,
					position_row=0, position_row,
					position_block=0, position_block,
					position_content=0, position_content ASC";
					$result = $this->DB_Prep_Exec($query, array('container_id' => $this->id()));
					while($row = $result->fetch())
					{
						$content = (get_called_class())::init($row);
						$this->container_content[$content->id()] = $content;
					}
				}
			}
			
			// After acquiring everything, clean it up if necessary
			$this->correctPotentialLayoutForDBRows();
			
		}

		return $this->container_content;

	}
	
	/**
	 * Updates a layout to include rows in the database, alongside layouts, blocks and content. This method will find
	 * layouts that don't have rows generated for them yet, and split them up so they do. This uses the flag `is_section_fixed`
	 * which indicates if it's been done and doesn't need it again.
	 */
	protected function correctPotentialLayoutForDBRows() : void
	{
		// Not a layout section, get out
		if(!$this->isLayoutSection())
		{
			return;
		}
		
		// It has been fixed already, get out
		if($this->is_section_fixed)
		{
			return;
		}
		
		
		// These are the page content variables that must be converted to the row
		$converted_variables = array(
			'column_format',
			'responsive_collapse',
			'responsive_flip_order',
			'equal_height_columns',
			'extend_full_width',
		);
		
		
		$row_1 = $this->variable('row_1');
		
		// Confirm we have old rows
		if($row_1 != '')
		{
			
			// Get the thing rendering it
			$renderer = $this->renderer();
			for($row_num = 1; $row_num <= 5; $row_num++)
			{
				// Get the class name, only bother if it's set
				$row_class_name = $this->variable('row_'.$row_num);
				if($row_class_name != '')
				{
					
					// CREATE THE ROW
					$content_values = array(
						'is_visible' => 1,
						'container_id' => $this->id(), // child of current item
						'view_class' => $row_class_name,
						$renderer->rendererIDColumn() => $renderer->id(), // The thing it belongs to
						'position_row' => $row_num,
						'css_classes' => $this->variable('css_classes_'.$row_num) // move styles
					);
					
					// Deal with non-pages and the class name
					$renderer_class = $this->rendererClassName();
					if($renderer_class != 'TMm_PagesMenuItem'
						&& !is_subclass_of($renderer_class, 'TMm_PagesMenuItem'))
					{
						$content_values['item_class_name'] = $renderer_class;
					}
					
					
					$render_item_class_name = $renderer->renderItemClassName();
					
					/** @var TMt_PageRenderItem $row_content_item */
					if($row_content_item = $render_item_class_name::createWithValues($content_values))
					{
						// MOVE RELEVANT BLOCKS TO ROW
						// Grab the first X items from the container content, and change their parents
						$row_view = new $row_class_name();
						$num_columns = count($row_view->contentBlocks());
						for($column_num = 1; $column_num <= $num_columns; $column_num++)
						{
							$column_content = array_shift($this->container_content);
							if($column_content)
							{
								$column_content->updateWithValues([
																	  'container_id' => $row_content_item->id(),
																	  'position_block' => $column_num
																  ]);
							}
						}
						
						// Deal with variables from the layout
						// All of these properties get passed down to the row instead
						$row_variable_values = array();
						foreach($converted_variables as $varname)
						{
							$row_variable_values[$varname] =  $this->variable($varname);
						}
						$row_content_item->updateVariablesWithArray($row_variable_values);
						
						// Attach the row to this layout
						$this->container_content[$row_content_item->id()] = $row_content_item;
						
					}
					
				}
				
				// Delete the variables associated with ROWS
				// These have been transferred to their parents
				$this->deleteVariable('row_'.$row_num);
				$this->deleteVariable('css_classes_'.$row_num);
				
				
			}
			
			// Now that we're done with the section, we remove all the converted varaibles too
			foreach($converted_variables as $variable_name)
			{
				$this->deleteVariable($variable_name);
			}
			
		}
		
		$view_class_name = $this->viewClass();
		// Detect "rows" that should be sections
		if($view_class_name != 'TMv_ContentLayout_Section' &&
			!is_subclass_of($view_class_name, 'TMv_ContentLayout_Section'))
		{
			$row_view_class = $this->viewClass();
			// Create the section
			$section_values = array(
				'view_class' => 'TMv_ContentLayout_Section',
				'menu_id' => $this->menuID(),
				'css_classes' => $this->cssClasses(),
				'position_layout' => $this->position_layout,
				'visibility_condition' => $this->visibility_condition,
				'is_visible' => 1,
				'container_id' => 0, // top-level parent
			);
			
			// Create the section
			$section = TMm_PagesContent::createWithValues($section_values);
			
			// Move this item to be a child of that section
			$row_values = array(
				'view_class' => $row_view_class,
				'container_id' => $section->id(),
				'position_layout' => 0,
				'position_row' => 1,
				'visibility_condition' => '',
				'css_classes' => '' // clear them out, moved to parent
			);
			$this->updateWithValues($row_values);
			
			$this->cleanOldDoubleRowItems();
			
		}
		
		
		$this->updateDatabaseValue('is_section_fixed',1);
		
	}
	
	/**
	 * Cleanup function for upgrades to page content which update page content that use the now deprecated double-row
	 * views.
	 */
	public function cleanOldDoubleRowItems() : void
	{
		if($this->viewClass() == 'TMv_ContentLayout_TitleWith_3Column'
			|| $this->viewClass() == 'TMv_ContentLayout_TitleWith_2Column')
		{
			
			// Duplicate the row, but change some values
			$row_values = $this->duplicationValues();
			$row_values['view_class'] = 'TMv_ContentLayout_'.($this->numChildrenContent()-1).'Column';
			$row_values['position_row'] = $this->layoutRowPosition()+1;
			
			/** @var TMt_PageRenderItem|string $model_class */
			$model_class = $this->renderer()->renderItemClassName();
			$row = $model_class::createWithValues($row_values);
			
			// Move the three columns for this item to a new row
			foreach($this->children() as $child_content)
			{
				if($child_content->blockPosition() > 1) // avoid the first one
				{
					$new_values = [
						'container_id' =>  $row->id(), // attach to the new row
						'position_block' => $child_content->blockPosition() - 1 // adjust position to be correct
					];
					
					$child_content->updateWithValues($new_values);
				}
			}
			
			// Update the view class for this item
			$this->updateDatabaseValue('view_class','TMv_ContentLayout_1Column');
		}
	
	}
	
	
	/**
	 * Returns the number of children content items
	 * @return int
	 */
	public function numChildrenContent() : int
	{
		return sizeof($this->containerContent());
	}
	
	/**
	 * Shorthand method that is easier to understand
	 * @return TMm_PagesContent[]
	 */
	public function children() : array
	{
		return $this->containerContent();
	}

	/**
	 * Returns the container for this item
	 * @return null|TCu_Item|TMt_PageRenderItem
	 */
	public function container() :?TCm_Model
	{
		if($this->container_id > 0)
		{
			return (get_called_class())::init($this->container_id);
		}
		return null;
	}
	
	/**
	 * Returns the ID for the container.
	 * @return int
	 */
	public function containerID() : int
	{
		return $this->container_id;
	}

	/**
	 * Returns if the content is root level content for the page
	 * @return bool
	 */
	public function isRootLevelContent() : bool
	{
		return $this->container_id == 0;
	}


	/**
	 * Returns the layout position
	 * @return int
	 */
	public function layoutPosition() : int
	{
		return $this->position_layout;
	}
	
	/**
	 * Returns if this item is a layout section
	 * @return bool
	 */
	public function isLayoutSection() : bool
	{
		return $this->position_layout > 0;
	}
	
	/**
	 * Returns the layout row
	 * @return int
	 */
	public function layoutRowPosition() : int
	{
		return $this->position_row;
	}
	
	/**
	 * Returns if this item is a layout row
	 * @return bool
	 */
	public function isLayoutRow() : bool
	{
		return $this->position_row > 0;
	}
	
	/**
	 * Returns the block position
	 * @return int
	 */
	public function blockPosition() : int
	{
		return $this->position_block;
	}
	
	/**
	 * Returns if this item is a layout block column
	 * @return bool
	 */
	public function isLayoutBlock() : bool
	{
		return $this->position_block > 0;
	}
	
	/**
	 * Returns the content position
	 * @return int
	 */
	public function contentPosition() : int
	{
		return $this->position_content;
	}
	
	/**
	 * Returns if this item is a layout content
	 * @return bool
	 */
	public function isLayoutContent() : bool
	{
		return $this->position_content > 0;
	}
	
	
	
	/**
	 * Deletes this content item
	 * @param bool $verb
	 */
	public function delete($verb = false) : void
	{
		// Avoid recursive calls to delete in extended classes
		if($this->delete_occurred === false)
		{
			foreach($this->containerContent() as $page_content)
			{
				$page_content->delete($verb);
			}
			
			// Deleting a column, adjust the row to have one less
			if($this->viewClass() == 'TMv_ContentLayoutBlock')
			{
				if($this->container())
				{
					$this->container()->removeColumnFromRow($this);
				}
				
			}
			$this->delete_occurred = true;
		}
		parent::delete($verb);
	}
	
	/**
	 * @param TMt_PageRenderItem $column
	 */
	public function removeColumnFromRow($column) : void
	{
		// Update columns with block numbers
		$query = "UPDATE `".self::tableName()."` SET position_block = position_block - 1
				WHERE container_id = :container_id AND position_block > :this_position";
		$this->DB_Prep_Exec($query, array('container_id' => $this->id(), 'this_position' => $column->position_block ));
		
		// Update the row column count
		$this->updateRowColumnCount(-1);
		unset($this->container_content[$column->id()]);
		
	}

	/**
	 * Updates the display order for this content id
	 * @param int $display_order
	 * @param int $container_id
	 */
	public function updateDisplayOrder(int $display_order, int $container_id) : void
	{
		$query = "UPDATE `".self::tableName()."` SET display_order = :display_order, container_id = :container_id WHERE ".self::$table_id_column." = :id";
		$result = $this->DB_Prep_Exec($query, array('id'=>$this->id(), 'display_order'=>$display_order, 'container_id'=>$container_id));
		$this->display_order = $display_order;
	}

	/**
	 * Updates the position_layout
	 * @param string $position
	 */
	public function updateLayoutPosition($position) : void
	{
		if($this->position_layout != $position)
		{
			$query = "UPDATE `" . self::tableName() . "` SET position_layout = :position_layout WHERE " . self::$table_id_column . " = :id";
			$this->DB_Prep_Exec($query, array('id' => $this->id(), 'position_layout' => $position));
			$this->position_layout = $position;
		}
	}
	
	/**
	 * Updates the position_row
	 * @param string $position_row
	 */
	public function updateRowPosition($position) : void
	{
		if($this->position_row != $position)
		{
			$query = "UPDATE `" . self::tableName() . "` SET position_row = :position WHERE " . self::$table_id_column . " = :id";
			$this->DB_Prep_Exec($query, array('id' => $this->id(), 'position' => $position));
			$this->position_row = $position;
		}
	}
	
	/**
	 * Updates the position_block
	 * @param string $position
	 */
	public function updateColumnPosition($position) : void
	{
		if($this->position_block != $position)
		{
			$query = "UPDATE `" . self::tableName() . "` SET position_block = :position WHERE " . self::$table_id_column . " = :id";
			$this->DB_Prep_Exec($query, array('id' => $this->id(), 'position' => $position));
			$this->position_block = $position;
		}
	}
	
	/**
	 * Updates the content position and container id
	 * @param string $position_content
	 * @param int $container_id
	 */
	public function updateContentPosition($position_content, $container_id) : void
	{
		if($this->position_content != $position_content || $this->container_id != $container_id)
		{
			$query = "UPDATE `" . self::tableName() . "` SET position_content = :position_content, container_id = :container_id WHERE " . self::$table_id_column . " = :id";
			$this->DB_Prep_Exec($query, array('id' => $this->id(), 'position_content' => $position_content, 'container_id' => $container_id));
			$this->position_content = $position_content;
			$this->container_id = $container_id;
		}
	}
	
	/**
	 * Creates a new row in this section with one column. Only has an effect if it's being added to an actual section.
	 * @return null|TMt_PageRenderItem The row that was created
	 */
	public function addRow() : ?TCm_Model
	{
		if($this->isLayoutSection())
		{
			
			$content_values = array(
				'view_class' => 'TMv_ContentLayout_1Column',
				$this->renderer()->rendererIDColumn() => $this->renderer()->id(),
				'is_visible' => 1,
				'container_id' => $this->id(),
				'position_row' => $this->numChildrenContent()+1
			);
			
			// Deal with non-pages and the class name
			if($this->item_class_name != null)
			{
				$content_values['item_class_name'] = $this->item_class_name;
			}
			
			
			$render_item_class_name = $this->renderer()->renderItemClassName();
			
			/** @var TMt_PageRenderItem $row_content_item */
			if($row_content_item = $render_item_class_name::createWithValues($content_values))
			{
				// Create the column
				// Build on existing values
				$content_values['container_id'] = $row_content_item->id();
				$content_values['view_class'] = 'TMv_ContentLayoutBlock';
				$content_values['position_block'] = 1;
				$content_values['position_row'] = 0;
				$column_item = $render_item_class_name::createWithValues($content_values);
				
				return $row_content_item;
			}
			else
			{
				$this->addConsoleError('Row could not be created.');
			}
			
		}
		else
		{
			$this->addConsoleError('Attempting to add a row to a non-section. No Effect.');
		}
		return null;
	}
	
	/**
	 * Creates a new column in this row. Only has an effect if it's being added to an actual row.
	 * @return null|TMt_PageRenderItem The column that was created
	 */
	public function addColumn() : ?TCm_Model
	{
		if($this->isLayoutRow())
		{
			$new_num_columns = $this->numChildrenContent()+1;
			$content_values = array(
				'view_class' => 'TMv_ContentLayoutBlock',
				$this->renderer()->rendererIDColumn() => $this->renderer()->id(),
				'is_visible' => 1,
				'container_id' => $this->id(),
				'position_block' => $new_num_columns
			);
			
			// Deal with non-pages and the class name
			if($this->item_class_name != null)
			{
				$content_values['item_class_name'] = $this->item_class_name;
			}
			
			$render_item_class_name = $this->renderer()->renderItemClassName();
			
			/** @var TMt_PageRenderItem $column_content_item  */
			if($column_content_item = $render_item_class_name::createWithValues($content_values))
			{
				// Update the view class
				$this->updateDatabaseValue('view_class', 'TMv_ContentLayout_'.$new_num_columns.'Column');
				return $column_content_item ;
			}
			else
			{
				$this->addConsoleError('Column could not be created.');
			}
			
		}
		else
		{
			$this->addConsoleError('Attempting to add a column to a non-row. No Effect.');
		}
		return null;
	}
	
	/**
	 * Updates this view to have one less column, which means it chooses the evenly spaced layout
	 * @param int $difference
	 */
	public function updateRowColumnCount(int $difference) : void
	{
		if($this->isLayoutRow())
		{
			$new_num_columns = $this->numChildrenContent() + $difference ;
			$this->updateDatabaseValue('view_class', 'TMv_ContentLayout_'.$new_num_columns.'Column');
			
		}
		
	}
	
		
		/**
	 * Updates the list of positions for the layouts in this menu item
	 * @param array $order_values
	 */
	public function updateRowPositionsWithArray(array $order_values) : void
	{
		try
		{
			$this->DB()->beginTransaction();
			
			foreach($order_values as $content_id => $order)
			{
				// Same class name, get the row
				$row = (get_class($this))::init($content_id);
				
				// Update the row
				$row->updateRowPosition($order);
			}
			$this->DB()->commit();
		}
		catch (Exception $e)
		{
			$this->DB()->rollBack($e->getMessage());
			$this->addConsoleMessage('Layout Display Order Update Failed', TSm_ConsoleIsError);
		}
		
	}
	
	/**
	 * Adds a new content view to this layout block.
	 * @param string $view_name The name of the view to be created
	 * @param array $variables The list of variables for this content item. Each entry in the array of key-value
	 * pairs with the index being the `variable_name` and the value corresponding to the value being added.
	 *
	 * Eg: $variables[
	 *   'html_content' => '<p>Something</p>',
	 *   'format' => 'left',
	 *   ]
	 *
	 * @return null|TMt_PageRenderItem|TMm_PagesContent
	 */
	public function createNewContentView(string $view_name,
	                                     array $variables = []) : ?TCm_Model
	{
		
		if($this->isLayoutBlock())
		{
			if(!TC_classUsesTrait($view_name,'TMt_PagesContentView'))
			{
				TC_triggerError('Class `'.$view_name.'` cannot be added to page content unless it uses the `TMt_PagesContentView` trait');
			}
			
			$values = [
				$this->renderer()->rendererIDColumn() => $this->renderer()->id(),
				'view_class' => $view_name,
				'container_id' => $this->id(),
				'is_visible' => '1',
				'position_content' => '1',
			];
			
			// Create the content item
			$render_item_class_name = $this->renderer()->renderItemClassName();
			$content = $render_item_class_name::createWithValues($values);
			
			if($content)
			{
				
				$content_variable_class_name = $content->variablesModelClass();
				// Loop through the variables provided and add them
				foreach($variables as $variable_name => $value)
				{
					$create_values = [
						'value' => $value,
						'variable_name' => $variable_name,
						'content_id' => $content->id()
					];
					
					$content_variable_class_name::createWithValues($create_values);
					
				}
				
				return $content;
			}
		}
		else
		{
			$this->addConsoleError('Attempting to add content to a non-layout block. No Effect.');
		}
		return null;
		
	}

	//////////////////////////////////////////////////////
	//
	// CSS CLASSES
	//
	//////////////////////////////////////////////////////



	/**
	 * Returns the user-defined CSS classes
	 * @return string
	 */
	public function cssClasses() : string
	{
		return $this->css_classes;
	}
	
	/**
	 * Returns the array of CSS classes represented as TMm_PagesViewStyle models
	 * @return TMm_PagesViewStyle[]
	 */
	public function cssPageViewStyles() : array
	{
		if($this->css_classes == '')
		{
			return [];
		}
		
		$classes = explode(' ', trim($this->css_classes));
		
		$view_style_classes = [];
		foreach($classes as $css_class_name)
		{
			if($item = TMm_PagesViewStyle::init($css_class_name))
			{
				$view_style_classes[] = $item;
			}
		}
		
		
		
		
		return $view_style_classes;
	}
	
	
	/**
	 * Returns the user-defined CSS classes
	 * @param string $css_class
	 * @return bool
	 */
	public function hasCSSClass($css_class) : bool
	{
		if($this->css_classes == '')
		{
			return false;
		}
		return in_array($css_class, explode(' ', trim($this->css_classes)));

	}

	/**
	 * Toggles the visibility for this content item
	 * @param string $css_class
	 */
	public function toggleCSSClass($css_class) : void
	{
		if($this->hasCSSClass($css_class))
		{
			$classes = explode(' ', $this->css_classes);
			foreach($classes as $index => $class_name)
			{
				if($class_name == $css_class)
				{
					unset($classes[$index]);
				}
			}
			$css_classes = implode(' ', $classes);

		}
		else // not there yet
		{
			$css_classes = $this->css_classes.' '.$css_class;
		}
		$this->updateDatabaseValue('css_classes', $css_classes);

	}



	//////////////////////////////////////////////////////
	//
	// VARIABLES
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the name of the variables table name
	 *
	 * // ----- VARIABLES, PAIRS A VARIABLE WITH A CONTENT ITEM -----
	 * $tables['pages_content_variables']['primary_key'] = 'match_id';
	 * $tables['pages_content_variables']['columns'] = array(
	 * "dashboard_id int(10) unsigned NOT NULL",
	 * "variable_name text NOT NULL",
	 * "value text NOT NULL",
	 * "date_added datetime NOT NULL",
	 * "user_id int(10) unsigned NOT NULL");
	 *
	 * // Foreign keys
	 * $tables['pages_content_variables']['indices']['content_id'] = 'content_id';
	 * $tables['pages_content_variables']['foreign_key']['content_id'] = 'pages_content';
	 * @return string
	 */
	public static function variablesTableName() : string
	{
		return (static::variablesModelClass())::tableName();
	}
	
	/**
	 * @return class-string<TCm_Model>
	 */
	public static function variablesModelClass() : string
	{
		return 'TMm_PageRendererContentVariable';
	}

	/**
	 * Returns the list of variables for content item.
	 * @return array
	 * #@see TMt_PageRenderer::contentVariablesForID()
	 */
	public function variables() : array
	{
		if(is_null($this->variables))
		{
			$this->variables = [];
			$content_variables = $this->renderer()->contentVariablesForID($this->id());
			foreach($content_variables as $row)
			{
				$this->variables[$row['variable_name']] = $row['value'];
				$this->variable_rows[$row['variable_name']] = $row;
			}
		}
		return $this->variables;
	}
	
	/**
	 * Returns the array of variable models. These are rarely used but useful for OOP operations. The items are
	 * indexed by the `variable_name` which matches other indexing in the system,
	 * @return TCm_Model[]|TMm_PagesContentVariable[]|TMm_PageRendererContentVariable[]
	 */
	public function variableModels() : array
	{
		$models = [];
		$this->variables(); // trigger the call
		foreach($this->variable_rows as $row)
		{
			$class_name = static::variablesModelClass();
			$models[$row['variable_name']] = $class_name::init($row);
		}
		return $models;
	}
	
	/**
	 * Returns the variable row, which includes additional values
	 * @param string $variable_name
	 * @return ?array
	 */
	public function variableRowForName(string $variable_name) : ?array
	{
		// pre-load variables
		$this->variables();
		
		if(isset($this->variable_rows[$variable_name]))
		{
			return $this->variable_rows[$variable_name];
		}
		return null;
	}

	/**
	 * Updates all the variables with a new set.
	 * @param array $variables
	 * @param array[] $additional_values Any additional values that should be added into the variable that is being
	 * saved. The indices for these values MUST math the indices in the $variables parameter. the columns for these
	 * values must also be defined as each entry is a $column_name => $value
	 */
	public function updateVariablesWithArray(array $variables, array $additional_values = []) : void
	{
		$field_id_column = self::$table_id_column;
		
		// Get the current variables
		$current_variable_models = $this->variableModels();
		
		// Loop through each provided variable
		foreach($variables as $variable_name => $value)
		{
			$db_values = array(
				'variable_name' => $variable_name,
				'value' => $value,
				$field_id_column => $this->id()
			);
			
			// Test for additional values for this particular variable
			if(isset($additional_values[$variable_name]))
			{
				foreach($additional_values[$variable_name] as $additional_column_name => $additional_value)
				{
					$db_values[$additional_column_name] = $additional_value;
				}
			}
			
			
			// Model exists, update
			if(isset($current_variable_models[$variable_name]))
			{
				$current_model = $current_variable_models[$variable_name];
				$current_model->updateWithValues($db_values);
				
			}
			else // doesn't exist, create
			{
				$class_name = $this->variablesModelClass();
				$class_name::createWithValues($db_values);
			}
			
			
		}

		// Set to false so that it can be reprocessed if necessary
		$this->variables = null;
		$this->variable_rows = [];
		
	}

	/**
	 * Updates a single variable
	 * @param string $variable_name
	 * @param string $value
	 */
	public function updateVariable(string $variable_name, $value) : void
	{
		$variable_array = array();
		$variable_array[$variable_name] = $value;
		$this->updateVariablesWithArray($variable_array);
	}


	/**
	 * Returns a variable for a particular name
	 * @param string $name
	 * @return string
	 */
	public function variable(string $name) : string
	{
		$this->variables();
		if(isset($this->variables[$name]))
		{
			return $this->variables[$name];
		}
		return '';
	}

	/**
	 * Deletes a variable for a particular name
	 * @param string $name
	 */
	public function deleteVariable(string $name) : void
	{
		$table_name = static::variablesTableName();
		$field_name = self::$table_id_column;
		$query = "DELETE FROM $table_name WHERE $field_name = :id AND variable_name = :variable_name LIMIT 1";
		$this->DB_Prep_Exec($query, array('id' => $this->id(), 'variable_name' => $name));
		unset($this->variables[$name]);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// BACKGROUND IMAGES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the background image file used for this view
	 * @return TCm_File Always returns this class. It handles internally if it exists
	 */
	public function backgroundImageFile() : TCm_File
	{
		$renderer_class_name = $this->rendererClassName();
		$upload_folder = $renderer_class_name::uploadFolder();
		$background_image = new TCm_File(false,
		                                 $this->variable('background_image_file'),
		                                 $upload_folder,
		                                 TCm_File::PATH_IS_FROM_SERVER_ROOT);
		return $background_image;
		
	}
	
	
	/**
	 * Empties the variable named "background_image_file" and deletes the file from the server
	 * @see TMv_ContentLayoutBlock
	 * @see TMv_ContentLayout
	 */
	public function removeBackgroundPhoto() : void
	{
		$background_image = $this->backgroundImageFile();
		if($background_image->exists())
		{
			$background_image->delete();
		}
		$this->updateVariable('background_image_file','');
	}
	
	//////////////////////////////////////////////////////
	//
	// DUPLICATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Duplicates this content view with a provided parent. This is a recursive function that propagates down to any
	 * of the children in the view.
	 * @param array|bool $override_values An array of values that are overridden in the duplication process
	 * @param  $parent
	 * @return ?TMt_PageRenderItem Returns the newly created item
	 */
	public function duplicate($override_values = array(), $parent = null)
	{
		// Undo any overrides that might pass through recursion
		unset($override_values['container_id']);
		unset($override_values['position_layout']);
		unset($override_values['position_row']);
		unset($override_values['position_content']);
		
		// This only occurs for the first parent, since everything else is nested inside.
		// The new item needs to be at the end needs to be moved to the end of the container
		if(is_null($parent))
		{
			if($this->isLayoutSection())
			{
				$new_position = count($this->renderer()->mainLevelContentItems()) +1;
				
				$override_values['position_layout'] = $new_position;
			}
			elseif($this->isLayoutRow())
			{
				$new_position = $this->container()->numChildrenContent() + 1;
				$override_values['position_row'] = $new_position;
			}
			elseif($this->isLayoutContent())
			{
				$new_position = $this->container()->numChildrenContent() + 1;
				$override_values['position_content'] = $new_position;
			}
		}
		else // parent ID provided, need to ensure
		{
			$override_values['container_id'] = $parent->id();
		}
		
		// Duplicate this item
		/** @var TMt_PageRenderItem $new_content */
		$new_content = parent::duplicate($override_values);
		
		// Duplicate the variables
		$new_content->updateVariablesWithArray($this->variables());
		
		foreach($this->containerContent() as $content)
		{
			// Duplicate the children content with the newly created item as the parent
			$content->duplicate($override_values, $new_content);
		}
		
		return $new_content;
	}
	
	//////////////////////////////////////////////////////
	//
	// HISTORIES
	//
	//////////////////////////////////////////////////////
	
	
	public static function historyHeadingAdjustments(TSm_ModelHistoryState $current_state,
	                                                 ?TSm_ModelHistoryState $previous_state) : ?array
	{
		//TC_addToConsole('----- historyHeadingAdjustments'.$current_state->className());
		
		$view_class = $current_state->valueForProperty('view_class');
		
		// Third levels are using variables but the view class name is passed in as part of the parent heading
		if($current_state->className() == 'TMm_PagesContentVariable'
			|| $current_state->className() == 'TMm_PageRendererContentVariable')
		{
			$view_class = $current_state->valueForProperty('parent_heading');
			
		}
		
		
		$ignored_view_classes = [
			'TMv_ContentLayoutBlock', // never show column changes
			
		];
		
		
		// Don't show any of the ignored classes
		if(in_array($view_class, $ignored_view_classes))
		{
			return null;
		}
		
		// IGNORE ANYTHING ELSE LAYOUT RELATED
		if( $view_class == 'TMv_ContentLayout_Section'
			|| is_subclass_of($view_class, 'TMv_ContentLayout_Section')
			|| is_subclass_of($view_class, 'TMv_ContentLayout')
		)
		{
		//	TC_addToConsole('* SKIP *');
			return null;
		}
		
	//	TC_addToConsole('View Class: "'.$view_class.'" '.$current_state->valueForProperty('content_id'));
		
		return [];
	}
	
	/**
	 * Adjust histories to not return value variable name columns, but instead use them to explain the value.
	 * @param string $column_name
	 * @param TSm_ModelHistoryState $current_state
	 * @param ?TSm_ModelHistoryState $previous_state
	 * @return array|null
	 */
	public static function historyChangeAdjustments(string $column_name,
	                                                TSm_ModelHistoryState $current_state,
	                                                ?TSm_ModelHistoryState $previous_state) : ?array
	{
		
		$adjustments = [];
		
		// Ignore when row views are changed
		if($column_name == 'view_class')
		{
			// Editing, never how view class changes. Almost always changes to layout
			if($previous_state)
			{
				return null;
			}
			// Never anything related to columns
			if($current_state->valueForProperty('view_class') == 'TMv_ContentLayoutBlock')
			{
				return null;
			}
			
		}
		
		$ignored_columns = [
			'position_layout',
			'position_row',
			'position_block',
			'position_content',
			'equal_height_columns',
			'container_id',
			'is_visible',
		
		];
		
		if(in_array($column_name, $ignored_columns))
		{
			return null;
		}
		
		
		// Otherwise return "no changes"
		return $adjustments;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMAS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The schema specifically for this trait. Adds functionality related to tracking publishing.
	 * @return array[]
	 */
	public static function schema_TMt_PageRenderItem() : array
	{
		return [
			'container_id' => [
				'title'         => 'Container ID',
				'comment'       => 'The parent ID for the container for this content',
				'type'          => 'TMm_PageRendererContent',
				'nullable'      => false,
				'validations'   => [
					'required'      => false, // Not required since it's possible for the container to be zero
				]
			],
			'view_class' => [
				'title'         => 'View Class',
				'comment'       => 'The view class name for the content',
				'type'          => 'varchar(128)',
				'nullable'      => false,
			],
			
			'equal_height_columns' => [
				'title'         => 'Equal heights column',
				'comment'       => 'Indicates if layouts will use an equal height columns',
				'type'          => 'tinyint(1) unsigned',
				'nullable'      => false,
			],
			'position_layout' => [
				'title'         => 'Position layout number',
				'comment'       => 'The number for the position in layouts',
				'type'          => 'smallint(10) unsigned',
				'nullable'      => false,
			],
			'position_row' => [
				'title'         => 'Position row number',
				'comment'       => 'The number for the position in row',
				'type'          => 'smallint(10) unsigned',
				'nullable'      => false,
			],
			'position_block' => [
				'title'         => 'Position block number',
				'comment'       => 'The number for the position in block',
				'type'          => 'smallint(10) unsigned',
				'nullable'      => false,
			],
			'position_content' => [
				'title'         => 'Position content number',
				'comment'       => 'The number for the position in content',
				'type'          => 'smallint(10) unsigned',
				'nullable'      => false,
			],
			'css_classes' => [
				'title'         => 'CSS Classes',
				'comment'       => 'The css classes for this content',
				'type'          => 'varchar(255)',
				'nullable'      => false,
			],
			'visibility_condition' => [
				'title'         => 'Visibility Condition',
				'comment'       => 'The visibility conditions for this content',
				'type'          => 'varchar(255)',
				'nullable'      => false,
			],
			'is_visible' => [
				'title'         => 'Visible',
				'comment'       => 'Indicates if this is visible',
				'type'          => 'tinyint(1) unsigned',
				'nullable'      => false,
			],
			'id_attribute' => [
				'title'         => 'id_attribute',
				'comment'       => 'The ID attribute for the content',
				'type'          => 'varchar(64)',
				'nullable'      => false,
			],
			
			'is_section_fixed' => [
				'title'         => 'Section fixed',
				'comment'       => 'Flag if the a section has been fixed, never needed again ',
				'type'          => 'bool DEFAULT false',
				'nullable'      => false,
			],
		
		];
		
	}
	
	
}

