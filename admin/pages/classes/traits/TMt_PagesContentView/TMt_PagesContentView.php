<?php
/**
 * Trait TMt_PagesContentView
 *
 * A trait which adds the necessary functionality for PagesContentViews to work properly as an augment to existing views.
 * Any view can be converted into a page view which lets it be used in the page builder. In order to so, have that view
 * implement this class and define the values appropriate. Refer to an existing Page Content View as an example.
 *
 * NOTE: The page content item is set AFTER the constructor, which means that you cannot access those values in the
 * constructor.
 *
 */
trait TMt_PagesContentView
{
	protected $renderer;
	protected $control_panel_view = null;
	protected bool $disable_further_rendering = false;
	protected ?string $background_image_file = '';
	
	// Values that exist for other page content views
	protected $content_item;
	
	
	/**
	 * @param TMt_PageRenderer $renderer
	 */
	public function setRenderer($renderer) : void
	{
		$this->renderer = $renderer;
	}
	
	/**
	 * Sets this view to disable any further rendering
	 * @return void
	 */
	protected function disableFurtherRendering() : void
	{
	//	$this->addConsoleDebug('Rendering disabled from Page Content View ');
		$this->disable_further_rendering = true;
	}
	
	/**
	 * Returns if this view has been set to disable any further rendering. This is sent as the view renders, forcing
	 * the renderer to ignore any other item.
	 * @return bool
	 */
	public function disablesFurtherRendering() : bool
	{
		return $this->disable_further_rendering;
	}
	
	/**
	 * Returns the upload folder for the page content item
	 * @return string Default $_SERVER['DOCUMENT_ROOT'].'/assets/';
	 */
	public static function uploadFolder() : string
	{
		return $_SERVER['DOCUMENT_ROOT'].'/assets/';
	}

	/**
	 * Returns if the the view is loading within the page editor
	 * @return bool
	 */
	public function isLoadingInPageBuilder() : bool
	{
		// Backtrace through the calls to see if we're calling from PageBuilder
		$calls = debug_backtrace();
		foreach($calls as $call_info)
		{
			$class_match =
				isset($call_info['class']) &&
				($call_info['class'] == 'TMv_PageBuilder' || is_subclass_of($call_info['class'],'TMv_PageRendererContentForm') );

			if($class_match || is_subclass_of($call_info['file'],'TMv_PageRendererContentForm') )
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * A hook method that can be called to perform changes to the class prior to rendering but after initialization.
	 * This can be used to customize the view based on values provided in the editor.
	 *
	 */
	public function configureForPageView()
	{
	}

	/**
	 * A hook method that can be called to override the view parameter that is passed into the view. This method is
	 * called each time and if it returns a non-null value, it will use that value as the parameter for the view when
	 * it is rendered in a page content view.
	 *
	 * This method is a static method since it is called before instantiation of the view.
	 * @param bool|TMm_PagesContent $content_model The content model that is being loaded
	 * @return null|string|TCm_Model
	 */
	public static function overrideViewParameter($content_model = false)
	{
		return null;
	}

	/**
	 * Returns if the view is loading within the page editor
	 * @return bool
	 */
	public function isLoadingInPageContentView() : bool
	{
		// Backtrace through the calls to see if we're calling from PageBuilder
		$calls = debug_backtrace();
		foreach($calls as $call_info)
		{
			//print
			$class_match = isset($call_info['class']) &&
				($call_info['class'] == 'TMv_PageBuilder'
					|| $call_info['class'] == 'TMv_PageContentRendering'
					|| is_subclass_of($call_info['class'],'TMv_PageRendererContentForm')
				);

			if($class_match || is_subclass_of($call_info['file'],'TMv_PageRendererContentForm') )
			{
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Saves the variables for the content item
	 * @param TMt_PageRenderItem $content_item
	 */
	protected function saveVariablesForContentItem($content_item) : void
	{
		// save values
		foreach($content_item->variables() as $variable_name => $value)
		{
			$this->$variable_name = $value;
			
			// Intercept and check for localization
			if(TC_getConfig('use_localization'))
			{
				$localization = TMm_LocalizationModule::init();
				
				// All the values are saved in the column `value`, but localization adds columns for each language
				// such as `value_fr` or `value_es` so we need to acquire those, which are based on the non-default
				// languages that are already configured
				
				// We acquire the entire row of values which are generated when the values are acquired
				$row = $content_item->variableRowForName($variable_name);
				
				// Loop through the non-default languages to find the other ones
				foreach($localization->nonDefaultLanguages() as $language => $lang_values)
				{
					// We save them into the variable name with the underscore which is what the system wants
					// So if the variable name is `html_content` then we save the french version into `html_content_fr`
					$varname = $variable_name.'_'.$language;
					if(isset($row['value_'.$language]))
					{
						$this->$varname = $row['value_'.$language];
					}
				
				}
			}

		}
	}
	
	
	/**
	 * Sets the page content item for this view
	 * @param TMt_PageRenderItem $content_item
	 */
	public function setPagesContentItem($content_item) : void
	{

		if(isset($this->content_item))
		{
			return;
		}

		$traits = class_uses($content_item);
		if(isset($traits['TMt_PageRenderItem']))
		{
			$this->content_item = $content_item;
			
			$this->saveVariablesForContentItem($content_item);
			
			if(TC_isTungstenView())
			{
				$this->setShowID(true);
			}
			
			if($this->id === false)
			{
				$this->id = $this->content_item->idAttribute();
			}


			// If no ID attribute is set, then use the content item one
			if(is_null($this->attributeID())
				|| trim($this->attributeID()) == ''
				|| ($this instanceof TMv_ContentLayout_Section))
			{
				$this->setIDAttribute($this->content_item->idAttribute());
			}

			// Call the configuration hook
			$this->configureForPageView();
		}
		else
		{
			TC_triggerError('Provided Content Item must use the Trait TMt_PageRenderItem');
		}
	}
	
	/**
	 * Returns the content item
	 * @return TMm_PagesContent
	 */
	public function contentItem()
	{
		if(isset($this->content_item))
		{
			return $this->content_item;
		}
		return null;
	}
	
	/**
	 * Returns the content icon override if it exists
	 * @return bool|TCv_View
	 */
	public function contentIconOverride()
	{
		return false;
	}
	
	public function noContentView() : ?TCv_View
	{
		return new TMv_NoContentPreview($this->contentItem());
	}
	
	/**
	 * This function is used to set the form items that are shown for this particular content item. The TCv_Form item
	 * that is being shown is passed as a parameter and those values are loaded as properties in your model.
	 *
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		return array();
	}
	
	/**
	 * The optional form processing which is run before the values are saved. This is called when page content is
	 * created or updated and by default performs no action.
	 *
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return string[] Returns an array of values that should be stored for the page content
	 */
	public static function pageContent_FormProcessing($form_processor) : array
	{
		return array();
	}
	
	
	
	/**
	 * Returns the title of the page content view. This is what people see when considering adding that item to a page.
	 * @return string
	 */
	public static function pageContent_ViewTitle() : string
	{
		return 'Page content view';
	}
	
	/**
	 * Returns the description of the content view
	 * @return string
	 */
	public static function pageContent_ViewDescription() : string
	{
		return '';
	}
	
	/**
	 * Returns the name if the icon code from Font Awesome for this view
	 * @return string
	 */
	public static function pageContent_IconCode() : string
	{
		$model = new TCm_Model(false);
		$module = $model->moduleForThisClass(get_called_class());
		if($module)
		{
			return $module->iconCode();
		}
		return 'fa-circle';
	}
	
	/**
	 * Returns if the view can be added as a content item. You can manually adjust certain items for your site if necessary.
	 * @return bool
	 */
	public static function pageContent_IsAddable() : bool
	{
		return true;
	}
	
	/**
	 * Returns if a preview should be shown in the page builder. When possible, this should return true however if the
	 * view is meant to show account information, sensitive information, or perhaps requires session variables from the
	 * public website, then you can turn off the preview.
	 * @return bool
	 */
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return true;
	}

	/**
	 * Returns if this view is a container, rather than a final view
	 * @return bool
	 */
	public static function pageContent_IsAContainer() : bool
	{
		return false; 
	}
	
	/**
	 * Return the name of the model that this view requires on instantiation. If this method returns a non-null value,
	 * then the rendering will attempt to instantiate a model with the ID provided through the URL in the ['id] parameter
	 * then pass that model into this view. If a view rquires an instance of a model then the URL should look something
	 * like /path/to/view/some-characters-optional/<id>/. The ID must be an integer.
	 *
	 * IF an array is returned, it will attempt to instantiate in the order provided. If one of the values is the
	 * string 'class_name', then it will attempt to pass in the model class name, which is how form creators are
	 * often instantiated.
	 * @return null|array|string
	 */
	public static function pageContent_View_InputModelName() : null|array|string
	{
		return null;
	}

	/**
	 * Returns an additional view that should be attached to the builder preview. This method only works if the ShowPreviewInBuilder
	 * is turned off.
	 * @return TCv_View|false
	 * @deprecated No longer used. Instead call the static method pageContent_BuilderPreviewContent()
	 */
	public function pageContent_BuilderPreviewAdditionalContent() : ?TCv_View
	{
		return null;
	}
	
	/**
	 * Returns an additional view that should be attached to the builder preview. This method only works if the ShowPreviewInBuilder
	 * is turned off.
	 * @param TMm_PagesContent|TMm_PageRendererContent $content_model
	 * @return ?TCv_View
	 */
	public static function pageContent_BuilderPreviewContent($content_model) : ?TCv_View
	{
		return null;
	}
	
	/**
	 * Returns the list of default classes that will be applied to this content. This provides a means to allow for customization of the
	 * view while still setting natural defaults.
	 * @return string
	 */
	public static function pageContent_defaultClasses()
	{
		return '';
	}
	
	/**
	 * Generates an empty input model that is used for this content view. This is used internally to ensure that
	 * classes with required classes load properly.
	 * @return TCm_Model|bool
	 */
	public static function emptyInputParameter()
	{
		$param = false;
		if($model_name = static::pageContent_View_InputModelName())
		{
			// Handle arrays
			if(is_array($model_name))
			{
				$model_name = $model_name[0];
			}
			
			// Find the first one we can
			$query = "SELECT * FROM `".($model_name::tableName())."` ORDER BY date_added LIMIT 1";
			$db = TCm_Database::connection();
			$statement = $db->prepare($query);
			$statement->execute();
			
			if($statement->rowCount() > 0)
			{
				$row = $statement->fetch();
				// Generate a model with ID zero
				$param = $model_name::init($row);
			}
			else
			{
				// Generate a model with ID zero
				$param = $model_name::init(array($model_name::$table_id_column => '0' ));
				
			}
			
			
		}
		
		return $param;
	}
	
	/**
	 * Returns an empty content view that is used when processing content views
	 * @return TMt_PagesContentView|TCv_View
	 */
	public static function emptyContentView() : ?TCv_View
	{
		/** @var TMt_PagesContentView $view_class */
		$view_class = get_called_class();
		return ($view_class)::init($view_class::emptyInputParameter());
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTROL PANEL VIEWS
	// A page content view can have an associated control
	// panel view which is handled by page builders as needed
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the control panel view for this content view
	 * @param TCv_View $control_panel_view
	 */
	public function setControlPanelView($control_panel_view)
	{
		$this->control_panel_view = $control_panel_view;
	}
	
	/**
	 * Returns if this view has a control panel
	 * @return bool
	 */
	public function hasControlPanelView() : bool
	{
		return ($this->control_panel_view instanceof TCv_View);
	}
	
	/**
	 * Returns the control panel view or null if it's not set
	 * @return null|TCv_View
	 */
	public function controlPanelView() : ?TCv_View
	{
		return $this->control_panel_view;
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the flags for a given page content item. This method should use that content item to check and return
	 * any flags specific to this view. This is commonly done by checking the variables against the expected values
	 * @param TMm_PagesContent|TMm_PageRendererContent $page_content
	 * @return TMm_ContentFlag[]
	 */
	public static function flagsForPageContent($page_content) : array
	{
		return [];
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	// Settings related to localizing page content views
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return array
	 */
	public static function localizedPageContentSettings() : array
	{
		return [];
	}
	
	//////////////////////////////////////////////////////
	//
	// BACKGROUND IMAGES
	//
	//////////////////////////////////////////////////////
	
	
	
	/**
	 * Returns the image view for the background photo
	 * @return null|TCv_Image
	 */
	public function backgroundImageView() : ?TCv_Image
	{
		// early exit if it's not used
		if(is_null($this->background_image_file) || $this->background_image_file === '')
		{
			return null;
		}
		$renderer_class_name = $this->contentItem()->rendererClassName();
		$upload_folder = $renderer_class_name::uploadFolder();
		
		$background_image = $this->contentItem()->backgroundImageFile();
		
		$is_normal_pages_content =  $this->contentItem() instanceof TMm_PagesContent;
		
		// Check if the file is missing
		if(!$background_image->exists() && !$is_normal_pages_content)
		{
			// Use the class to strip out CROP values
			$background_filename = $background_image->filename();
		
			// Detect if we have items with page renderers and the image is in the old location of `page_renderer`.
			// We need to move it to the appropriate folder
			$possible_old_location_file =
				$_SERVER['DOCUMENT_ROOT'].'/assets/page_renderer/'.$background_filename;
			
			// We found the file in the old location
			if(file_exists($possible_old_location_file))
			{
				$this->addConsoleWarning('Moving file from page_renderer');
				$this->addConsoleWarning('File : '. $possible_old_location_file);
				
				// Move the file
				rename($possible_old_location_file, $background_image->filenameFromServerRoot());
				
				// Re-generate the TCm_File
				$background_image = new TCm_File(false, $this->background_image_file, $upload_folder, TCm_File::PATH_IS_FROM_SERVER_ROOT);
				
			}
			else // File isn't in the old spot, genuinely missing file
			{
				return null;
			}
		}
		
		if($image = new TCv_Image(false, $background_image))
		{
			$filter_options = TC_getConfig('page_builder','background_image_filtering');
			if(count($filter_options) > 0)
			{
				$view_filters = explode(',', $this->filters);
				foreach($view_filters as $filter_name)
				{
					$filter_name = trim($filter_name);
					if($filter_name != '' && isset($filter_options[$filter_name]))
					{
						//$filter_values = $filter_options[$filter_name];
						
						$image->applyFilter($filter_options[$filter_name]['function'],
											$filter_options[$filter_name]['args']);
						
					}
					else
					{
						$this->addConsoleWarning('Code not found in filters: '.$filter_name);
					}
				}
			}
			
			$image->createCacheImage();
			return $image;
		}
		
		return null;
			
		
	
	}
	
	/**
	 * Applies the background image to the view, using CSS.
	 * @return void
	 */
	protected function applyBackgroundImage() : void
	{
		if($photo_view = $this->backgroundImageView())
		{
			//$photo_view = new TCv_Image(false, $background_image);
			
			$selector = '#'.$this->attributeID();
			
			// Columns add it to the container, for historical reasons keep it this way.
			if($this instanceof TMv_ContentLayoutBlock)
			{
				$selector .= ' .layout_block_container';
			}
			
			// Full width is 1800
			// Large enough to cover multiple sizes
			$width = 1800;
			
			// If there is a percentage background, we want the width
			// to be generated based on the values
			if($this->background_size != 'cover' && $this->background_size != 'contain')
			{
				$width = $this->calculateRenderWidthFromColumns();
				$width = $width * (int)$this->background_size / 100;
			}
			
			$photo_view->scaleInsideBox($width, 10000);
			$photo_view->createCacheImage();
			
			$css = $selector.' { background-image: url("'.$photo_view->cacheFolderPath().$photo_view->cacheFilename().'"); ';
			$css .= ' background-position: '.$this->background_position.'; ';
			$css .= ' background-size: '.$this->background_size.'; ';
			
			if($this->background_size == 'contain')
			{
				$css .= ' background-repeat:no-repeat;';
			}
			$css .= '}';
			
			// Content width version
			$content_width = TC_getConfig('content_width');
			$css .= $photo_view->generateScaledBackgroundImageMediaCSS($content_width, $selector);
			
			
			$single_column_width = TC_getConfig('content_single_column_width');
			$css .= $photo_view->generateCroppedBackgroundImageMediaCSS($single_column_width,
																		750,
																		$selector,
																		100);
			
			$css .= $photo_view->generateCroppedBackgroundImageMediaCSS(500,
																		1000,
																		$selector,
																		100);
			
			// Handle responsive for
			$this->addCSSLine($this->attributeID().'_background_image', $css);
			$this->addClass('has_bg_photo');
			
			$this->addDeferBackgroundImageCode();
		}
	}
	
	/**
	 * Adds the code that does a one-time deferral of any background images until the page is loaded. This is applied once
	 * as it at one time no matter how many
	 * @return void
	 */
	protected function addDeferBackgroundImageCode() : void
	{
		$this->addClass('defer_load');
		// @see https://www.corewebvitals.io/pagespeed/defer-background-images
		// Give all the items with the defer_load background, no background with important
		$this->addCSSLine('defer_background_image', ".defer_load { background-image: none !important;}");
		
		// Add JS to remove those classes
		$this->addJSLine('defer_background_image',
						 "document.querySelectorAll('.defer_load').forEach(el => {el.classList.remove('defer_load');});",
						 true);
	}
	
	
	/**
	 * Function to calculate the render width for this content item, based on the layout of the item in provided
	 * columns. This can be called by any page content view.
	 * @return int
	 */
	protected function calculateRenderWidthFromColumns() : int
	{
		$content_item = $this->contentItem();
		if($column = $content_item->container())
		{
			$column_num = $column->blockPosition();
			if($row = $column->container())
			{
				$view_class = $row->viewClass();
				
				if(method_exists($view_class, 'pixelWidthForColumn'))
				{
					
					return $view_class::pixelWidthForColumn($column_num);
				}
				
				
			}
		}
		
		if(TC_configIsSet('content_width'))
		{
			return TC_getConfig('content_width');
		}
		
		return 1000;
		
	}
	
	
	
	
}
?>