<?php

/**
 * Trait TMt_PageModelViewable
 *
 * This trait is applied to models that are used as a model class in a page in the Pages module. This is done when a
 * page loads content dynamically base on a model via the ID in the URL. This trait must be applied to them in order
 * to ensure the system detects and builds them properly.
 */
trait TMt_PageModelViewable
{
	public static $view_menu_item;
	public static $view_url_path;
	
	public static $edit_menu_item;
	public static $edit_url_path;
	
	protected ?string $meta_description;
	protected ?string $meta_title;
	
	
	/**
	 * An array of menu items for view modes which are cached if found
	 * @var array
	 */
	public static array $view_mode_menu_items = [];
	
	protected $is_visible_on_sitemap = true;
	
	//////////////////////////////////////////////////////
	//
	// SITEMAP.XML
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this class should appear on the sitemap. Defaults to the isPageLinkable value. If we can't link
	 * to it, we likely don't want it on the sitemap. Can be extended and overridden if necessary.
	 * @return bool
	 */
	public static function showOnSitemap() : bool
	{
		return static::isPageLinkable();
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// VIEW
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the menu item associated with this model for a particular view mode. Queries the DB for the mode and
	 * the model names for this model.
	 *
	 * @param string $mode
	 * @return null|TMm_PagesMenuItem
	 */
	public static function menuItemWithPrimaryViewMode(string $mode) : ?TMm_PagesMenuItem
	{
		// Key doesn't exist, go find it in the DB
		if(!array_key_exists($mode, static::$view_mode_menu_items))
		{
			static::$view_mode_menu_items[$mode] = null;
			$query = "SELECT * FROM `pages_menus`
					WHERE model_class_name_primary_view_page = :mode
					  AND (model_class_name = :class_name OR model_class_name = :class_name_base)
					  LIMIT 1";
			
			// Run the query, making sure to use the page menu DB_connection, instead of this model's
			$result = static::DB_RunQuery(  $query,
			                                [
				                                'mode' => $mode,
				                                'class_name' => get_called_class(),
				                                'class_name_base' => static::baseClass()
			                                ],
			                                [],
			                                TMm_PagesMenuItem::DB_Connection());
			if($row = $result->fetch())
			{
				static::$view_mode_menu_items[$mode] = TMm_PagesMenuItem::init($row);
			}
		}
		
		return static::$view_mode_menu_items[$mode];
	}
	
	/**
	 * Returns the URL for this item for a given view mode. This relies on the modes being set in `TMm_PagesMenuItem`
	 * in the static variable `$public_page_model_modes`.
	 * @param string $view_mode
	 * @return string|null
	 */
	public function pageViewUrlForViewMode(string $view_mode) : null|string
	{
		$view_menu_item = static::menuItemWithPrimaryViewMode($view_mode);
		if($view_menu_item instanceof TMm_PagesMenuItem)
		{
			return $view_menu_item->viewURL($this);
		}
		
		return null;
	}
	
	
	/**
	 * Returns the menu item for viewing this model
	 * @return bool|TMm_PagesMenuItem
	 */
	public static function pageViewMenuItem()
	{
		return static::menuItemWithPrimaryViewMode('view');
	}
	
	/**
	 * Returns the base value for viewing this model. This detects the view property in the pages list and returns
	 * the path to that menu
	 * @return string|bool
	 */
	public static function pageViewBaseURL()
	{
		// Check if we've ever set it
		if(static::$view_url_path === null)
		{
			$menu_item = static::pageViewMenuItem();
			if($menu_item)
			{
				static::$view_url_path = $menu_item->pathToFolder();
			}
			else
			{
				static::$view_url_path = false;
			}
		}
		
		return static::$view_url_path;
	}
	
	/**
	 * Clears the page view base URL
	 * @return void
	 */
	public static function clearPageViewBaseURL() : void
	{
		static::$view_url_path = null;
	}
	
	/**
	 * Returns the URL path that should be used to view this item on the public website. This is defined by each
	 * model to return the correct URL as needed, likely with the ID and possible a human readable.
	 *
	 * If a page on the site uses this model and is set as the primary view page, then this will work automatically
	 * @return null|string A value of null indicates no path to view
	 * @see TCm_Model::cleanForURL()
	 */
	public function pageViewURLPath()
	{
		$possible_override = $this->overridePageViewURLPath();
		if(is_string($possible_override))
		{
			return $possible_override;
		}
		
		return $this->pageViewUrlForViewMode('view');
	}
	
	/**
	 * A function to safely extend pageViewURLPath() so that models that use this trait can override specific values. If this
	 * function returns null, then nothing happens and the default functionality occurs. However if it returns a string, then that's
	 * the value that is used.
	 * @return string|null
	 */
	public function overridePageViewURLPath() : ?string
	{
		return null;
	}
	
	
	/**
	 * The end of the URL for this model. This is sometimes overridden to return a model-specific value other than
	 * the title and sometimes it returns nothing if that text isn't wanted.
	 * @return string
	 */
	public function pageViewURLSuffix()
	{
		return $this->title();
	}
	
	
	/**
	 * Returns the page view code for this item. These codes are used in locations where we want to reference an item
	 * that we need to insert the actual link afterwards.
	 * @return string
	 */
	public function pageViewCode()
	{
		return '/{{pageview:'.$this->contentCode().'}}';
	}
	
	/**
	 * Returns if the page for this item is visible.
	 * @return bool
	 */
	public function pageViewMenuItemIsVisible()
	{
		$menu_item = static::pageViewMenuItem();
		
		// No menu item
		if(!$menu_item)
		{
			return false;
		}
		elseif(!$menu_item->isActive())
		{
			return false;
		}
		return true;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// EDIT
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the menu item for editing this model
	 * @return false|TMm_PagesMenuItem
	 */
	public function pageEditMenuItem()
	{
		return static::menuItemWithPrimaryViewMode('edit');
	}
	
	/**
	 * Returns the base value for viewing this model. This detects the view property in the pages list and returns
	 * the path to that menu
	 * @return string|bool
	 */
	public function pageEditBaseURL()
	{
		// Check if we've ever set it
		if(static::$edit_url_path === null)
		{
			$menu_item = static::pageEditMenuItem();
			if($menu_item)
			{
				static::$edit_url_path = $menu_item->pathToFolder();
			}
			else
			{
				static::$edit_url_path = false;
			}
		}
		
		return static::$edit_url_path;
	}
	
	/**
	 * Returns the URL path that should be used to edit this item on the public website. This is defined by each
	 * model to return the correct URL as needed, likely with the ID and possible a human readable
	 * @return bool|string A value of false indicates no path to view
	 * @see TCm_Model::cleanForURL()
	 */
	public function pageEditURLPath()
	{
		return $this->pageViewUrlForViewMode('edit');
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PAGE DISPLAY SETTINGS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the metadata
	 * @return string|null
	 */
	public static function pageDisplay() : ?string
	{
		return null;
	}
	
	/**
	 * Returns the title to be shown for this page when it is being viewed. By default this returns the regular title
	 * method value for any TCm_Model
	 * @return string
	 * @see TCm_Model::title()
	 */
	public function pageDisplayTitle() : string
	{
		if(method_exists($this, 'title'))
		{
			return $this->title();
		}
		
		return '';
	}

	/**
	 * Returns the page display photo. This looks in the model for a method that returns a TCm_File commonly called `photoFile`
	 * or `photo()`.
	 * @return false|TCm_File
	 */
	public function pageDisplayPhoto() : false|TCm_File
	{
		$photo_file_methods = array('photoFile','photo');

		foreach($photo_file_methods as $method_name)
		{
			if(method_exists($this, $method_name))
			{
				/** @var TCm_File $photo */
				$photo = $this->$method_name();

				if($photo instanceof TCm_File)
				{
					if($photo->exists())
					{
						return $photo;
					}
				}


			}

		}
		return false;
	}

	/**
	 * Indicates if the page photo should be used for backgrounds. Setting this to false will stop it from loading in
	 * the backgrounds but it still gets used in other spots for the page, such as sharing
	 * @return bool
	 */
	public function usePhotoForPageBackgrounds()
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// PAGE LINKING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A method to indicate if items are linkable in the pages module. This should be disabled for models that aren't
	 * publicly linkable in the pages module.
	 * @return bool
	 */
	public static function isPageLinkable()
	{
		return true;
	}
	
	/**
	 * Returns the name of the method that should be called when pulling up the list of linkable models. For large
	 * data lists, this can impede the functionality of the "link" button in tinyMCE.
	 * @return string
	 */
	public static function linkableItemsListMethod() : string
	{
		return 'models';
	}
	
	//////////////////////////////////////////////////////
	//
	// SITEMAPS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Whether this item should be appearing on the sitemap
	 * @return bool
	 */
	public function isVisibleOnSitemap()
	{
		return ($this->is_visible_on_sitemap == 1);
	}
	
	/**
	 * Returns if this item should appear on the sitemap. This tests a bunch of properties
	 * @return bool
	 */
	public function appearsOnSitemap()
	{
		// Pages that are not visible on sitemap should be skipped here
		// Generated from TMt_PageModelViewable
		if(!$this->isVisibleOnSitemap())
		{
			return false;
		}
		// Requires permissions, regardless of the model
		if(!$this->userCanView() )
		{
			return false;
		}
		
		
		if(method_exists($this,'pageViewMenuItemIsVisible') && !$this->pageViewMenuItemIsVisible())
		{
			return false;
		}
		
		
		
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// METADATA VALUES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this class uses the advanced  for meta values such as titles. If the feature switch
	 * for `use_seo_advanced` is enabled this is checked to see the URL target should be presented.
	 * @return bool
	 * @deprecated see modelUsesAdvancedMetaData
	 */
	public static function modelUsesAdvancedSEO()
	{
		return true;
	}
	
	
	/**
	 * Returns if this class uses the advanced meta data. If it's enabled, then it will show these fields as an
	 * option for that model in the editor.
	 * @return bool
	 * @deprecated see modelUsesAdvancedMetaData
	 */
	public static function modelUsesAdvancedMetaData() : bool
	{
		return static::modelUsesAdvancedSEO();
	}
	
	
	/**
	 * Returns the description for this item
	 * @return string
	 */
	public function metaDescription()
	{
		if(property_exists($this,'meta_description'))
		{
			return strip_tags($this->localizeProperty('meta_description'));
		}
		
	}
	
	/**
	 * Returns the title for this menu item
	 * @return string
	 */
	public function metaTitle()
	{
		if(property_exists($this,'meta_title'))
		{
			return strip_tags($this->localizeProperty('meta_title'));
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMAS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The schema specifically for this trait. Adds functionality related to seo
	 * @return array[]
	 */
	public static function schema_TMt_PageModelViewable()
	{
		return TMm_PagesMenuItem::schema_TMt_PageModelViewable();
		
	}
}