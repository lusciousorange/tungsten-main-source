<?php
/**
 * Trait TMt_PageRenderer
 *
 * A trait which adds the ability for a TCm_Model to be an item that renders pages content.
 *
 */
trait TMt_PageRenderer
{
	protected ?array $content_items = null; // [array] = The array of content items attached to this menu
	protected array $content_items_by_container = [];
	
	protected bool $has_unpublished_page_content = false;
	
	/**
	 * @var null|array The array containing all the content variables for the items in this page. When loading a
	 * page, these calls are handled by the page to avoid excessive DB hits.
	 */
	protected ?array $content_variables = null;
	
	/**
	 * Returns if the item uses content management. Override this method as necessary
	 * @return bool
	 */
	public function isContentManaged() : bool
	{
		return true;
	}

	/**
	 * Returns the name of the class that uses the TMt_PageRenderItem
	 * @return class-string<TMt_PageRenderItem|TCm_Model>
	 */
	public static function pageRenderItemClassName() : string
	{
		return 'TMm_PageRendererContent';
	}
	
	/**
	 * A function to return the render item class name. This is a passthrough function for the static method.
	 * @return class-string<TMt_PageRenderItem>
	 */
	public function renderItemClassName() : string
	{
		return static::pageRenderItemClassName();
	}
	
	/**
	 * Returns the name of the form view class that is used to edit/create TMt_PageRenderItem
	 * @return class-string<TCv_FormWithModel>
	 */
	public function pageRenderItem_ContentFormClassName() : string
	{
		
		return 'TMv_PageRendererContentForm';
	}

	
	/**
	 * Returns the ID column for the renderer. By default this returns 'item_id' since that is what the general
	 * page_renderer uses.
	 * ID Column
	 * @return string
	 */
	public function rendererIDColumn() : string
	{
		return 'item_id';
	}

	/**
	 * Returns the ID for the renderer.
	 * @return int|string
	 */
	public function rendererID() : int|string
	{
		return $this->id();
	}

	/**
	 * Returns the view to be used when trying to view this item as a page builder
	 * @return string
	 */
	public function rendererPageBuilderURL() : string
	{
		if(isset($_SERVER['HTTP_REFERER']))
		{
			return $_SERVER['HTTP_REFERER'];
		}
		return '/';

	}


	/**
	 * Returns the list of content items for this page
	 * @return TMt_PageRenderItem[]|TMm_PagesContent[]
	 */
	public function contentItems() : array
	{
		if(is_null($this->content_items))
		{
			$table = $this->renderItemTableName();
			$db_values = array();
			
			// BUILD THE QUERY
			$query = "SELECT * FROM `$table` ";
			
			
			// Deal with using the generic page_renderer_content table.
			// Substr to avoid hitting _unpub
			if(substr($table,0,22) == 'pages_renderer_content')
			{
				$query .= " WHERE item_id = :renderer_id AND item_class_name = :class_name ";
				$db_values['class_name'] = get_called_class();
			}
			else
			{
				$query .= " WHERE ". $this->rendererIDColumn()." = :renderer_id";
			}
			
			
			$db_values['renderer_id'] = $this->rendererID();
			$query .= " ORDER BY
					position_layout=0, position_layout,
					position_row=0, position_row,
					position_block=0, position_block,
					position_content=0, position_content ASC";
			$result = $this->DB_Prep_Exec($query, $db_values);
			
			// Loop through items
			$this->content_items = [];
			while($row = $result->fetch())
			{
				$name = $this->renderItemClassName();
				
				/** @var TMt_PageRenderItem $content_model */
				$content_model = $name::init($row);
				$this->content_items[$content_model->id()] = $content_model;
				
				// instantiate based on the parent id, that way it will exist as we iterate
				if(!$content_model->isLayoutContent()) // any non-content can be a parent
				{
					$this->content_items_by_container[$content_model->id()] = array();
				}
				
				$this->content_items_by_container[$content_model->containerID()][] = $content_model;
			}

		}

		return $this->content_items;
	}
	
	/**
	 * Returns the content models that are content models directly underneath a given container
	 * @param TMt_PageRenderItem $container
	 * @return null|TMt_PageRenderItem[]
	 */
	public function contentModelsForContainer($container) : ?array
	{
		if(isset($this->content_items_by_container[$container->id()]))
		{
			return $this->content_items_by_container[$container->id()];
		}
		
		return null;
	}
	
	/**
	 * Returns the number of content items
	 * @return int
	 */
	public function numContentItems() : int
	{
		return sizeof($this->contentItems());
	}

	/**
	 * Returns the list of content items that are not nested within a container
	 * @return array
	 */
	public function mainLevelContentItems() : array
	{
		$main_level_items = [];
		foreach($this->contentItems() as $content_item)
		{
			if($content_item->container() == false)
			{
				$main_level_items[] = $content_item;
			}
		}
		return $main_level_items;
	}


	/**
	 * Returns the list of content items that are marked as visible
	 * @return TMt_PageRenderItem[]
	 */
	public function visibleContentItems() : array
	{
		$visible_items = [];
		foreach($this->contentItems() as $content_item)
		{
			if($content_item->isVisible())
			{
				$visible_items[] = $content_item;
			}
		}
		return $visible_items;
	}


	/**
	 * Accepts an array of value arranged as $content_id => $display_order and updates the display order of each of the content items.
	 * @param array $display_orders
	 * @param array $parent_values
	 */
	public function updateContentDisplayOrder(array $display_orders, array $parent_values) : void
	{
		try
		{
			$this->DB()->beginTransaction();
			foreach($this->contentItems() as $content_item)
			{
				$content_item->updateDisplayOrder(
					$display_orders[$content_item->id()],
					$parent_values[$content_item->id()]
				);

			}
			$this->DB()->commit();
		}
		catch (Exception $e)
		{
			$this->DB()->rollBack($e->getMessage());
			$this->addConsoleMessage('Display Order Update Failed', TSm_ConsoleIsError);
		}

	}

	/**
	 * Updates the list of positions for the layouts in this menu item
	 * @param array $order_values
	 */
	public function updateLayoutPositionsWithArray(array $order_values) : void
	{
		try
		{
			$this->DB()->beginTransaction();
			foreach($order_values as $content_id => $order)
			{
				$name = $this->renderItemClassName();
				$content_item = $name::init($content_id);
				$content_item->updateLayoutPosition($order);
			}
			$this->DB()->commit();
		}
		catch (Exception $e)
		{
			$this->DB()->rollBack($e->getMessage());
			$this->addConsoleMessage('Layout Display Order Update Failed', TSm_ConsoleIsError);
		}

	}

	/**
	 * Returns an array of properties that used to create a layout.
	 * @param class-string<TMt_PagesContentView> $layout_class_name
	 * @return array
	 */
	public function additionalLayoutCreationProperties(string $layout_class_name) : array
	{
		return array();
	}

	/**
	 * 	Adds a layout to this page with the classname provided. This method will actually generate a multi-row
	 * @param class-string<TMt_PagesContentView> $layout_class_name
	 * @return null|TMt_PageRenderItem
	 */
	public function addContentLayoutWithClassName(string $layout_class_name, int $num_columns = 1) : ?TCm_Model
	{
		$render_item_class_name = $this->renderItemClassName();
		$renderer_field_name = $this->rendererIDColumn();

		$content_values = array(
			'is_visible' => 1,
			'view_class' => $layout_class_name,
			$renderer_field_name => $this->rendererID(),
			'position_layout' => $this->maxLayoutNumber()+1,
			'container_id' => 0,
		);
		$content_values = array_merge($content_values, $this->additionalLayoutCreationProperties($layout_class_name));

		// Deal with non-pages and the class name
		if($render_item_class_name != 'TMm_PagesContent')
		{
			$content_values['item_class_name'] = get_called_class();
		}
		

		/** @var TMt_PageRenderItem $section */
		if($section = $render_item_class_name::createWithValues($content_values))
		{
			// Add Responsive Collapsing variable
			$section->updateVariable('responsive_collapse','1');
			
			$content_values['view_class'] = 'TMv_ContentLayout_'.$num_columns.'Column';
			$content_values['container_id'] = $section->id();
			$content_values['position_layout'] = 0; // remove the layout setting
			$content_values['position_row'] = 1; // add the row value
			
			
			if($row = $render_item_class_name::createWithValues($content_values))
			{
				for($col = 1; $col <= $num_columns; $col++)
				{
					$content_values['position_row'] = 0; // remove the row value
					$content_values['view_class'] = 'TMv_ContentLayoutBlock';
					$content_values['position_block'] = $col; // add the row value
					$content_values['container_id'] = $row->id();
					
					$column = $render_item_class_name::createWithValues($content_values);
				}
				// Reset the content items, ensures they process properly
				$this->content_items = null;
				return $section;
			}
			else
			{
				$this->addConsoleError('Could not create row');
			}
			
			
		}
		else
		{
			$this->addConsoleError('Could not create section');
		}

		return null;
	}

	/**
	 * Returns the primary list of content items that are layout sections
	 * @return TMt_PageRenderItem[]|TMm_PagesContent[]
	 */
	public function layoutContentItems() : array
	{
		$layout_content_items = array();
		foreach($this->contentItems() as $content_item)
		{
			if($content_item->container() == false)
			{
				$layout_content_items[] = $content_item;
			}
		}

		return $layout_content_items;
	}

	/**
	 * 	Returns the primary list of content items that are layout sections
	 * @return int
	 */
	public function numLayoutContentItems() : int
	{
		return sizeof($this->layoutContentItems());
	}
	
	/**
	 * Returns the maximum layout position, to ensure things always stack properly
	 * @return int
	 */
	public function maxLayoutNumber() : int
	{
		$max = 0;
		foreach($this->layoutContentItems() as $layout)
		{
			if($layout->layoutPosition() > $max)
			{
				$max = $layout->layoutPosition();
			}
		}
		return $max;
	}
	
	/**
	 * Returns if this item uses the page theme styling, which will then be installed when loading the views and builder
	 * @return bool
	 */
	public function usePageThemeStyling() : bool
	{
		return true;
	}
	
	/**
	 * Generates and returns the content item with a given ID
	 * @param int $id
	 * @return TMt_PageRenderItem|TCm_Model
	 */
	public function contentItemWithID(int $id) : TCm_Model
	{
		$content_class_name = $this->renderItemClassName();
		
		/** @var TMt_PageRenderItem $content_item */
		$content_item = ($content_class_name)::init($id);
		
		return $content_item;
	}
	
	/**
	 * Adds a column to an element with a given row ID
	 * @param int $row_id
	 * @returns TMt_PageRenderItem The newly created column
	 */
	public function addColumnToRowWithID(int $row_id) : TCm_Model
	{
		$row = $this->contentItemWithID($row_id);
		
		if(!$row->isLayoutRow())
		{
			TC_triggerError('Provide item with ID <em>'.$row_id.'</em> is not a layout row');
		}
		
		return $row->addColumn();
	}
	
	/**
	 * Adds a row to an element with a given section ID
	 * @param int $section_id
	 * @returns TMt_PageRenderItem The newly created row
	 */
	public function addRowToSectionWithID(int $section_id)
	{
		$section = $this->contentItemWithID($section_id);
		
		if(!$section->isLayoutSection())
		{
			TC_triggerError('Provide item with ID <em>'.$section_id.'</em> is not a layout section');
		}
		
		return $section->addRow();
	}
	
	
	
	/**
	 * Adds a content to this page. This finds the first section, the first row, and the first column in that
	 * section and adds that content to the page.
	 * @param string $view_name The name of the view to be created
	 * @param array $variables The list of variables for this content item. Each entry in the array of key-value
	 * pairs with the index being the `variable_name` and the value corresponding to the value being added.
	 *
	 * Eg: $variables[
	 *   'html_content' => '<p>Something</p>',
	 *   'format' => 'left',
	 *   ]
	 *
	 * @return null|TMt_PageRenderItem|TMm_PagesContent
	 */
	public function createNewContentViewInFirstAvailableColumn(string $view_name,
	                                                           array $variables	= [])
	{
		// Find the column
		$column = null;
		foreach($this->contentItems() as $item)
		{
			if(is_null($column) && $item->isLayoutBlock())
			{
				$column = $item;
			}
		}
		
		if($column)
		{
			return $column->createNewContentView($view_name, $variables);
		}
		else
		{
			$this->addConsoleWarning('No column found when creating new content');
		}
	
		return null;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// CONTENT VARIABLES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the name of the table for variables
	 * @return string
	 */
	public function variablesTableName() : string
	{
		// Get the class name for the render item
		$render_item_class_name = static::pageRenderItemClassName();
		$variables_class_name = $render_item_class_name::variablesModelClass();
		
		
		return $variables_class_name::tableName();
	}
	
	/**
	 * Returns the name of the table for the render item
	 * @return string
	 */
	public function renderItemTableName() : string
	{
		$render_item_class_name = static::pageRenderItemClassName();
		$table_name = $render_item_class_name::tableName();
		return $table_name;
	}
	
	
	/**
	 * Returns all the content variables for a given content ID
	 * @param int $content_id
	 * @return array|mixed
	 */
	public function contentVariablesForID($content_id)
	{
		$variables = $this->allContentVariablesRows();
		
		if(isset($variables[$content_id]))
		{
			return $variables[$content_id];
		}
		
		return [];
	}
	
	/**
	 * Acquires ALL the content variables for this renderer at one time.
	 * @return array
	 */
	public function allContentVariablesRows() : array
	{
		if( is_null($this->content_variables))
		{
			$this->content_variables = [];
			// Get the name content item table (pages_content)
			$render_item_class_name = $this->renderItemClassName();
			$render_item_table_name = $this->renderItemTableName();
			$variable_table_name = $this->variablesTableName();
			
			$query = "SELECT v.* FROM `$variable_table_name` v INNER JOIN `$render_item_table_name` USING(content_id)
				WHERE ".$this->rendererIDColumn()." = :id";
			$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
			while($row = $result->fetch())
			{
				$row_content_id = $row['content_id'];
				// Init table if it doesn't exist
				if(!isset($this->content_variables[$row_content_id]))
				{
					$this->content_variables[$row_content_id] = [];
				}
				// Add the row to the collection of values for the content ID
				$this->content_variables[$row_content_id][] = $row;
				
			}
		}
		
		return $this->content_variables;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// CONTENT PREVIEWING
	//
	//////////////////////////////////////////////////////
	
	
	
	/**
	 * Returns the content of this page stripped of tags and trimmed to X characters. This will only look at
	 * `TMv_HTMLEditor` items and
	 * @param int $num_characters THe number of characters to trim to
	 * @return string
	 */
	
	public function contentPreviewPlainText($num_characters)
	{
		$lang_suffix = '';
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			$localization = TMm_LocalizationModule::init();
			$language = $localization->loadedLanguage();
			if($language != $localization->defaultLanguage())
			{
				$lang_suffix = '_'.$language;
			}
			
		}
		$description = '';
		foreach($this->mainLevelContentItems() as $section)
		{
			foreach($section->children() as $row)
			{
				foreach($row->children() as $column)
				{
					foreach($column->children() as $content_item)
					{
						if($content_item->viewClass() == 'TMv_HTMLEditor' && $content_item->isVisible())
						{
							$variable_row = $content_item->variableRowForName('html_content');
							$description .= $variable_row['value'.$lang_suffix];
						}
					}
				}
			}
			
		}
		
		// Replace some formatting with useful inline equivalents
		$description = str_ireplace(array('</h2>','</h3>','</h4'),' : ', $description);
		$description = str_ireplace(array('</p>','<br />','<br>','<br/>'),' ', $description);
		$description = str_ireplace(array('</li>'),', ', $description);
		$description = strip_tags($description);
		
		// Deal with trimming off text
		if(strlen($description) > $num_characters)
		{
			return substr($description, 0, $num_characters).'…';
		}
		
		return $description;
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	// Content flagging is commonly done per-model, but
	// pages are always more complicated due to how they
	// are built and managed
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A flag method that detects if this renderer has no content
	 */
	public function flagNoContent()
	{
		if($this->numLayoutContentItems() == 0)
		{
			return new TMm_ContentFlag('no-page-content','No page content');
			
		}
		
		
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// PUBLISHING
	//
	// Methods related to publishing
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this particular model uses, publishing, which accounts for the larger flag, but can also be
	 * extended to force a particular model to never use publishing.
	 * @return bool
	 */
	public static function modelUsesPublishing() : bool
	{
		if(!TC_getConfig('use_pages_version_publishing'))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Generates an unpublished version of this model's content in the page builder.
	 */
	public function generateUnpublishedVersion() : void
	{
		//$this->addConsoleDebug('generateUnpublishedVersion');
		
		// Feature disabled or not used by this model
		if(!static::modelUsesPublishing())
		{
			return;
		}
		
		$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 0');
		
		/** @var class-string<TCm_Model> $render_item_class_name */
		$render_item_class_name = static::pageRenderItemClassName();
		$mirror_table_suffix = $render_item_class_name::$mirror_table_suffix;
		$table = $render_item_class_name::tableName();
		
		// Catch any possible double-dips on the suffix
		// We need a clean table without the unpub on it
		if(strpos($table,$mirror_table_suffix) > 0)
		{
			$table = str_ireplace($mirror_table_suffix,'',$table);
			
		}
		
		// Determine if existing unpublished version exists
		// This a manual DB process since it's copying values over and adjusting for the fact that IDs will be
		// mismatched over time as content is updated and changed. We can't trust any ID to be transferable
		$query = "SELECT * FROM `".$table . $mirror_table_suffix."` WHERE ".$this->rendererIDColumn()." = :id LIMIT 1";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		if($result->rowCount() > 0)
		{
			// GET OUT
			$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 1');
			return;
		}
		
		
		
		// We need to track the IDs and update them afterward for the sake of
		$new_container_ids = [];
		
		// Get the content items and reproduce them, tracking the original_id for later
		// Get them in order of section, row, block, content, this ensures that we always have parent IDs
		$query = "SELECT * FROM `".$table."` WHERE ".$this->rendererIDColumn()." = :id
		ORDER BY position_layout=0, position_layout,
					position_row=0, position_row,
					position_block=0, position_block,
					position_content=0, position_content ASC";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		$table_schema = $render_item_class_name::schema();
		$insert_values = [];
		while($source_values = $result->fetch())
		{
			$snippets = [];
			
			// Track the original ID separately, so that we can reference it back later
			$snippets[] = " original_id = :original_id ";
			$insert_values['original_id'] = $source_values['content_id'];
			unset($source_values['content_id']);
			
		//	$this->addConsoleDebug($db_values);
			
			// PERFORM THE UPDATE
			$query = "INSERT INTO  `".$table . $mirror_table_suffix."` SET ";
			foreach($source_values as $name => $value)
			{
				// Only bother if it's in the schema
				if(isset($table_schema[$name]))
				{
					$snippets[] = " $name = :$name ";
					$insert_values[$name] = $value;
				}
			}
			
			// Deal with container_id
			//$snippets[] = 'container_id = :container_id';
			if($source_values['container_id'] == 0)
			{
				$insert_values['container_id'] = 0;
				
			}
			else // look it up based on the source value
			{
				$insert_values['container_id'] = $new_container_ids[$source_values['container_id']];
			}
		
			$query .= implode(', ', $snippets);
			$this->DB_Prep_Exec($query, $insert_values);
			
			$new_content_id = $this->DB()->insertedID();
			
			// Save the new IDs referenced by the OLD ID for iterative use
			$new_container_ids[$insert_values['original_id']] = $new_content_id;
			
		}
		
//		// Update the container_ids to match
//		foreach($new_ids as $original_id => $new_id)
//		{
//			$query = "UPDATE `".$table.$mirror_table_suffix."` SET container_id = :new_id WHERE container_id = :original_id";
//			$this->DB_Prep_Exec($query, ['original_id' => $original_id, 'new_id' => $new_id]);
//		}
		
		// GENERATE THE VARIABLES
		$variable_class_name = $render_item_class_name::variablesModelClass();
		$variable_table_name = $variable_class_name::tableName();
		$variable_primary_key = $variable_class_name::$table_id_column;
		
		// Get ALL the variables from the live tables, create new ones in the mirror
		$query = "SELECT v.* FROM `$variable_table_name` v INNER JOIN $table USING(content_id)
			WHERE ".$this->rendererIDColumn()." = :id";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		while($db_values = $result->fetch())
		{
			// Save the original ID
			$db_values['original_id'] = $db_values[$variable_primary_key];
			
			// Clear the primary key
			unset($db_values[$variable_primary_key]);
			
				
			// Update the content_id to reference the mirror IDs, not the live ones
			$db_values['content_id'] = $new_container_ids[$db_values['content_id']];
			
			
			// Run the query for each content item
			$query = "INSERT INTO `".$variable_table_name . $mirror_table_suffix."` SET ";
			$snippets = array();
			foreach($db_values as $name => $value)
			{
				$snippets[] = " $name = :$name ";
			}
			$query .= implode(', ', $snippets);
			$this->DB_Prep_Exec($query, $db_values);
		}
		
		
		$this->DB_Prep_Exec('SET FOREIGN_KEY_CHECKS = 1');
		
	}
	
	public function publish() : void
	{
		$this->addConsoleWarning('--- PUBLISH --- ');
		
		$db = static::DB_Connection();
		
		// Get core values
		$render_item_class_name = static::pageRenderItemClassName();
		$mirror_table_suffix = $render_item_class_name::$mirror_table_suffix;
		$table = $render_item_class_name::tableName();
		
		
		// WE start with all the IDs of all the LIVE content items
		// These are cleared out as items are deleted
		$content_to_delete = [];
		$query = "SELECT content_id FROM `" . $table . "` WHERE " . $this->rendererIDColumn() . " = :id ";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		while($row = $result->fetch())
		{
			$content_to_delete[$row['content_id']] = $row['content_id'];
		}
		
		
		//$this->addConsoleDebug('BEFORE');
		//$db->beginTransaction();
		
		//$this->addConsoleDebug('AFTER');
		// TRACK lookup values for containers
		// They are all different, so there's no correlation
		// These are indexed by the mirror IDs so we can look up what the live value should be
		$live_container_ids = [];
		
		// Store this query, since we might do it a few times
		// It's sorted to find the sections, then rows, then columns
		$unpublished_content_query = "SELECT * FROM `" . $table . $mirror_table_suffix . "` WHERE " .
			$this->rendererIDColumn() .	" = :id
			ORDER BY position_layout=0, position_layout,
					position_row=0, position_row,
					position_block=0, position_block,
					position_content=0, position_content ASC";
		$unpublished_result = $this->DB_Prep_Exec($unpublished_content_query, ['id' => $this->id()]);
		
		// Loop through all the unpublished content
		// Create it if it doesn't exist, update it if it does
		while($row = $unpublished_result->fetch())
		{
			// Instantiate the mirror object
			// use new to avoid any caching
			// pass the row to avoid any DB calls
			$mirror_object = new $render_item_class_name($row);
			$mirror_content_id = $row['content_id'];
			
			//$this->addConsoleDebug($mirror_object);
			
			// Track the original ID
			$original_id = $row['original_id'];
			unset($row['original_id']);
			
			
			// CONSOLE TRACKING
			$console_values = [
				'content_id' => $row['content_id'],
				'view_class' => $row['view_class'],
				'original_id' => $original_id,
				'container_id' => $row['container_id'],

			];
			$this->addConsoleDebug($console_values);
			
			// Get the model for the live version
			
			$update_values = $row;
			unset($update_values['content_id']); // can't pass that in, we need to generate the new one
			
			// Find the live container value
			if($update_values['container_id'] > 0)
			{
				$update_values['container_id'] = $live_container_ids[$update_values['container_id']];
			}
			
			// No original ID, create it
			if(is_null($original_id))
			{
				$this->addConsoleWarning('Create a '.$update_values['view_class']);
				
				// Create the item
				$live_model = $render_item_class_name::createWithValues($update_values);
				
			}
			else // Original exists
			{
				// Instantiate the model based on the original ID
				$live_model = $render_item_class_name::init($original_id);
				$live_model->updateWithValues($update_values);
				
				// If we have an original ID, remove it from the things we are going to delete
				unset($content_to_delete[$original_id]);
				
			}
			
			// Save the live container IDs
			$live_container_ids[$mirror_content_id] = $live_model->id();
			
			// Update the variables for the live model
			// Go straight to the DB, there might be other values
			//$live_model->updateVariablesWithArray($mirror_object->variables());
			
			
		}
		
		// GENERATE THE VARIABLES
		$variable_class_name = $render_item_class_name::variablesModelClass();
		$variable_table_name = $variable_class_name::tableName();
		$variable_primary_key = $variable_class_name::$table_id_column;
		
		// Get ALL the variables from the live tables, create new ones in the mirror
		$query = "SELECT v.* FROM `" . $variable_table_name . $mirror_table_suffix . "` v
			INNER JOIN $table$mirror_table_suffix USING(content_id)
			WHERE ".$this->rendererIDColumn()." = :id";
		$result = $this->DB_Prep_Exec($query, ['id' => $this->id()]);
		while($row = $result->fetch())
		{
			
			// Instantiate the mirror object
			// use new to avoid any caching
			// pass the row to avoid any DB calls
			$mirror_object = new $variable_class_name($row);
			$mirror_primary_value = $row[$variable_primary_key];
			
			//$this->addConsoleDebug($mirror_object);
			
			// Track the original ID
			$original_id = $row['original_id'];
			unset($row['original_id']);
			
			$update_values = $row;
			unset($update_values[$variable_primary_key]);
			
			// Find the live content value
			$update_values['content_id'] = $live_container_ids[$update_values['content_id']];
			
			// No original ID, create it
			if(is_null($original_id))
			{
				$this->addConsoleWarning('Create variable: '.$update_values['variable_name']);
				
				// Create the item
				$live_model = $variable_class_name::createWithValues($update_values);
				
			}
			else // Original exists
			{
				$live_model = $variable_class_name::init($original_id);
				$live_model->updateWithValues($update_values);
				
				
			}
		
		}
		
		
		$this->addConsoleDebug('--- DELETE ---');
		if(count($content_to_delete) > 0)
		{
			$query = "DELETE FROM $table WHERE content_id IN(".implode(',', $content_to_delete).")";
			$this->DB_Prep_Exec($query);
		}
		else
		{
			$this->addConsoleMessage('Nothing to delete');
		}
		
		// Clear out the unpublished stuff, we're done
		$this->addConsoleDebug('--- CLEAR UNPUBLISHED VALUES ---');
		$this->deleteUnpublishedChanges();
		$this->clearUnpublishedFlag();
		
		
		
		// Handle the commit or rollback
		// Junk call to force a rollback
		//$this->DB_Prep_Exec('Select * from sdfsfsf');
		
//		try {
//			// do your queries here, using PDO and $db object
//			$db->commit();
//		}
//		catch (PDOException $e)
//		{
//			$db->rollback("There was a problem with publishing. The changes were not applied. If this continues, contact your developer");
//			$this->addConsoleError($e);
//		}
//
		
	}
	
	public function discardUnpublishedChanges() : void
	{
		$this->addConsoleWarning('--- DISCARD UNPUBLISHED CHANGES --- ');
		
		$this->deleteUnpublishedChanges();
		$this->clearUnpublishedFlag();
		
	}
	
	/**
	 * Clears any of the unpublished content for this item. This deletes any of those values.
	 * @return void
	 */
	public function deleteUnpublishedChanges() : void
	{
		/** @var class-string<TCm_Model> $render_item_class_name */
		$render_item_class_name = static::pageRenderItemClassName();
		$mirror_table_suffix = $render_item_class_name::$mirror_table_suffix;
		
		
		if($mirror_table_suffix != null)
		{
			// Delete the mirror table content, let the foreign keys handle the rest
			$query = "DELETE FROM ".$render_item_class_name::tableName().$mirror_table_suffix." WHERE "
				.$this->rendererIDColumn()." = :id";
			$this->DB_Prep_Exec($query, ['id' => $this->id()]);
		}
	}
	
	/**
	 * Updates this item to indicate that is has unpublished content.
	 * @return void
	 */
	public function markAsUnpublished() : void
	{
		if($this->modelUsesPublishing() && !$this->hasUnpublishedContent())
		{
			$this->updateDatabaseValue('has_unpublished_page_content', 1);
		}
	}
	
	/**
	 * Removes the flag related to unpublished content
	 * @return void
	 */
	public function clearUnpublishedFlag(): void
	{
		if($this->modelUsesPublishing())
		{
			$this->updateDatabaseValue('has_unpublished_page_content', 0);
		}
		
	}
	
	public function hasUnpublishedContent() : bool
	{
		return $this->has_unpublished_page_content;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMAS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The schema specifically for this trait. Adds functionality related to tracking publishing.
	 * @return array[]
	 */
	public static function schema_TMt_pageRenderer() : array
	{
		return [
			
			'has_unpublished_page_content' => [
				'comment'       => 'Indicates if this item has unpublished page content',
				'type'          => 'bool DEFAULT 0',
				'nullable'      => false,
			],
		];
		
	}
}

