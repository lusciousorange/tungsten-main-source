<?php

/**
 * Trait TMt_Page301Redirectable
 *
 * Applied to items that can have 301 redirects in place for them.
 */
trait TMt_Page301Redirectable
{
	/** @var TMm_Pages301Redirect[] $page_redirects */
	protected $page_redirects = null;
	
	/**
	 * Returns all the 301 redirects that point to this item
	 * @return TMm_Pages301Redirect[]
	 */
	public function get301Redirects()
	{
		if($this->page_redirects == null)
		{
			$this->page_redirects = [];
			
			$base_class = $this->baseClassName();
			
			if($base_class == 'TMm_PagesMenuItem')
			{
				$query = "SELECT * FROM `page_301_redirects` WHERE target_page_menu_id = :menu_id ORDER BY date_added ASC";
				$params = ['menu_id' => $this->id()];
			}
			else
			{
				$query = "SELECT * FROM `page_301_redirects` WHERE class_name = :class_name AND item_id = :id ORDER BY date_added ASC";
				$params = ['class_name' => $base_class, 'id' => $this->id()];
			}
			$result = $this->DB_Prep_Exec($query, $params,[],TMm_Pages301Redirect::DB_Connection());
			while($row = $result->fetch())
			{
				$this->page_redirects[] = TMm_Pages301Redirect::init($row);
			}
		}
		
		return $this->page_redirects;
		
	}
	
	/**
	 * Deletes all the 301 redirects for this item
	 */
	public function delete301Redirects()
	{
		$base_class = $this->baseClassName();
		
		if($base_class == 'TMm_PagesMenuItem')
		{
			$query = "DELETE FROM page_301_redirects WHERE target_page_menu_id = :menu_id";
			$params = ['menu_id' => $this->id()];
		}
		else
		{
			$query = "DELETE FROM page_301_redirects WHERE class_name = :class_name AND item_id = :id";
			$params = ['class_name' => $base_class, 'id' => $this->id()];
		}
		
		$result = $this->DB_Prep_Exec($query, $params,[],TMm_Pages301Redirect::DB_Connection());
		
	}
}