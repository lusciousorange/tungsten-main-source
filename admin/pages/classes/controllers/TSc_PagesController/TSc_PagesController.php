<?php

/**
 * Class TSc_PagesController
 */
class TSc_PagesController extends TSc_ModuleController
{
	/**
	 * TSc_PagesController constructor.
	 * @param int|string|TSm_Module|bool $module
	 */
	public function __construct($module = false)
	{
		// Allow loading without providing a module
		if($module === false)
		{
			$module = TSm_Module::init('pages');
		}
		parent::__construct($module);
		
		// manual set the model to the user. That's what the dashboard wants
		$this->model = TC_website()->user();

		// Handle Single-Page Setup
		if($module->installed())
		{
			$single_page_value = TC_getModuleConfig('pages', 'single_page');
			if($single_page_value > 0 && $this->currentURLTarget())
			{
				if ($this->currentURLTarget()->name() == 'manage' && $single_page_value > 0)
				{
					header("Location: /admin/pages/do/page-builder/" . $single_page_value);
					exit();
				}
			}
		}
	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		/** @var TSm_ModuleURLTarget $target */
		
		$settings_url_target = $this->URLTargetWithName('settings');
		$settings_url_target->setValidationClassAndMethod('TMm_PagesModule','userHasFullAccess');
		
		
		$target = TSm_ModuleURLTarget::init( '');
		$target->setNextURLTarget('manage');
		$this->addModuleURLTarget($target);

		/** @var TSm_ModuleURLTarget $manage */
		$manage = TSm_ModuleURLTarget::init( 'manage');
		$manage->setTitle('Pages'); // set to the same name to skip
		if(TC_getConfig('use_tungsten_9'))
		{
			$manage->setViewName('TMv_PagesMenuItemManagerList');
		}
		else // version 8
		{
			$manage->setViewName('TMv_PagesMenuManager');
			
		}
		$this->addModuleURLTarget($manage);
	
		
		if(TC_getConfig('use_tungsten_9'))
		{
			$target = TSm_ModuleURLTarget::init('create-page');
			$target->setViewName('TMv_PageMenuItem_CreateForm');
			$target->setTitle('Add page');
			$target->setAsRightButton('fa-plus');
			$target->setValidationClassAndMethod('TMm_PagesModule', 'userHasFullAccess');
			$target->setParent($manage);
			$this->addModuleURLTarget($target);
		}
		else // old one that adds to the bottom
		{
			$target = TSm_ModuleURLTarget::init( 'create-page');
			$target->setModelName('TMm_PagesMenuItem');
			$target->setTitle('Add page');
			$target->setAsRightButton('fa-plus');
			$target->setParent($manage);
			$target->setModelName('TMm_PagesMenuItemList');
			$target->setModelActionMethod('createPage');
			$target->setValidationClassAndMethod('TMm_PagesModule','userHasFullAccess');
			$this->addModuleURLTarget($target);
		}
		
		$target = TSm_ModuleURLTarget::init( 'rearrange-menus');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setTitle('Rearrange');
		$target->setAsRightButton('fa-sort');
		$target->setParent($manage);
		$target->setNextURLTarget(null);
		$target->setValidationClassAndMethod('TMm_PagesModule','userHasFullAccess');
		$this->addModuleURLTarget($target);
		
		// OLDER DELETE THAT IS USED AND REFERENCED
		$target = TSm_ModuleURLTarget::init( 'delete-menu-item');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('delete');
		$target->setTitleUsingModelMethod('title');
		$target->setNextURLTarget('manage');
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanDelete');
		$this->addModuleURLTarget($target);
		
		// STANDARDIZED DELETE
		$target = TSm_ModuleURLTarget::init( 'delete');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('delete');
		$target->setTitleUsingModelMethod('title');
		$target->setNextURLTarget('manage');
		$target->setAsPrimaryModelDeleteTarget();
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanDelete');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init( 'edit');
		$target->setViewName('TMv_EditMenuItemForm');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setTitleUsingModelMethod('title');
		$target->setTitle('Page settings');
		$target->setModelInstanceRequired();
		$target->setParent($manage);
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanEdit');
		$target->setAsModelSubmenu(1);
		$this->addModuleURLTarget($target);
		
		// MOVE UP AND DOWN
		$target = TSm_ModuleURLTarget::init( 'menu-move-up');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('moveUp');
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanEdit');
		$target->setNextURLTarget(null);
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'menu-move-down');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('moveDown');
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanEdit');
		$target->setNextURLTarget(null);
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);
		
		
		// Redirect for legacy support purposes
		$target = TSm_ModuleURLTarget::init('edit-menu-properties');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setNextURLTarget('edit');
		$this->addModuleURLTarget($target);
		
		
		
		$target = TSm_ModuleURLTarget::init( 'page-preview');
		$target->setViewName('TMv_PageContentRendering');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setTitleUsingModelMethod('title');
		$target->setTitle('Page preview');
		$target->setModelInstanceRequired();
		$target->setParent($manage);
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'duplicate-page');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('duplicate');
		$target->setTitle('Duplicate');
		$target->setNextURLTarget('edit');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'edit-content');
		$target->setViewName('TMv_EditContentForm');
		$target->setModelName('TMm_PagesContent');
		$target->setTitleUsingModelMethod('title');
		$target->setModelInstanceRequired();
		//$target->setParent($target);
		$target->loadModelWithMethod('menuItem');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init( 'delete-content');
		$target->setModelName('TMm_PagesContent');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('delete');
		$target->setTitleUsingModelMethod('title');
		$target->setNextURLTarget('manage');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'duplicate-content');
		$target->setModelName('TMm_PagesContent');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('duplicate');
		$target->setTitle('Duplicate');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'remove-photo');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('removePhoto');
		$target->setTitleUsingModelMethod('title');
		$target->setNextURLTarget(NULL);
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init( 'remove-background-photo');
		$target->setModelName('TMm_PagesContent');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('removeBackgroundPhoto');
		$target->setTitleUsingModelMethod('title');
		$target->setNextURLTarget(NULL);
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init( 'page-components');
		$target->setViewName('TMv_PageComponentManager');
		$target->setTitle('Components');
		//$target->setModelName('TMm_PagesContent');
		//$target->setTitleUsingModelMethod('title');
		//$target->setModelInstanceRequired();
		//$target->setParent($target);
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'hide-page-content-view');
		$target->setTitle('Hide page content view');
		$target->setModelName('TMm_PageContentList');
		$target->setModelActionMethod('addHiddenView');
		$target->setPassValuesIntoActionMethodType('GET','class_name');
		$target->setNextURLTarget(null);
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'show-page-content-view');
		$target->setTitle('Hide page content view');
		$target->setModelName('TMm_PageContentList');
		$target->setModelActionMethod('removeHiddenView');
		$target->setPassValuesIntoActionMethodType('GET','class_name');
		$target->setNextURLTarget(null);
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'menu-list-tinyMCE');
		$target->setModelName('TMm_PagesMenuItemList');
		$target->setModelActionMethod('menuArrayForTinyMCE');
		$target->setNextURLTarget(null);
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);
		
		//////////////////////////////////////////////////////
		//
		// PAGE RIGHT PANELS
		//
		//////////////////////////////////////////////////////
//		$target = TSm_ModuleURLTarget::init( 'page-item-navigation');
//		$target->setViewName('TMv_PageMenuItem_NavigationForm');
//		$target->setTitle('Navigation');
//		$target->setLoadInRightPanel('fa-compass','model');
//		$target->setModelName('TMm_PagesMenuItem');
//		$target->setModelInstanceRequired();
//		//$target->setTitleUsingModelMethod('title');
//		$target->setParentURLTargetWithName('edit');
//		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'page-item-display');
		$target->setViewName('TMv_PageMenuItem_DisplayForm');
		$target->setTitle('Display');
		$target->setLoadInRightPanel('fa-eye','model');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		//$target->setTitleUsingModelMethod('title');
		$target->setParentURLTargetWithName('edit');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init( 'page-item-photo');
		$target->setViewName('TMv_PageMenuItem_PhotoForm');
		$target->setTitle('Photo');
		$target->setLoadInRightPanel('fa-image','model');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		//$target->setTitleUsingModelMethod('title');
		$target->setParentURLTargetWithName('edit');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'page-item-users');
		$target->setViewName('TMv_PageMenuItem_UsersForm');
		$target->setTitle('User access');
		$target->setLoadInRightPanel('fa-users','model');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		//$target->setTitleUsingModelMethod('title');
		$target->setParentURLTargetWithName('edit');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'page-item-advanced');
		$target->setViewName('TMv_PageMenuItem_AdvancedForm');
		$target->setTitle('Advanced settings');
		$target->setLoadInRightPanel('fa-code','model');
		$target->setModelName('TMm_PagesMenuItem');
		$target->setModelInstanceRequired();
		//$target->setTitleUsingModelMethod('title');
		$target->setParentURLTargetWithName('edit');
		$this->addModuleURLTarget($target);
		
		
		//////////////////////////////////////////////////////
		//
		// HERO BOXES
		//
		// URL Targets related to hero boxes for a page
		//
		//////////////////////////////////////////////////////

		$hero_box_list = TSm_ModuleURLTarget::init( 'page-hero-boxes');
		$hero_box_list->setViewName('TMv_PageHeroBoxList');
		$hero_box_list->setModelName('TMm_PagesMenuItem');
		$hero_box_list->setTitleUsingModelMethod('title');
		$hero_box_list->setTitle('Hero boxes');
		$hero_box_list->setModelInstanceRequired();
		$hero_box_list->setParentURLTargetWithName('edit');
		$hero_box_list->setValidationClassAndMethod('TMm_PagesMenuItem', 'usesHeroBoxes()');
		$hero_box_list->setAsModelSubmenu();
		$this->addModuleURLTarget($hero_box_list);

		$target = TSm_ModuleURLTarget::init( 'create-page-hero-box');
		$target->setViewName('TMv_PageHeroBoxForm');
		$target->setModelName('TMm_PagesMenuItem');
		//$target->setTitleUsingModelMethod('title');
		$target->setTitle('Add hero box');
		$target->setModelInstanceRequired();
		$target->setParent($hero_box_list);
		$target->setValidationClassAndMethod('TMm_PagesMenuItem', 'usesHeroBoxes()');
		$target->setAsRightButton('fa-plus');
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init( 'edit-page-hero-box');
		$target->setViewName('TMv_PageHeroBoxForm');
		$target->setModelName('TMm_PageHeroBox');
		$target->setTitleUsingModelMethod('title');
		$target->setTitle('Edit hero box');
		$target->setModelInstanceRequired();
		$target->setParent($hero_box_list, 'menuItem()');
		$target->setValidationClassAndMethod('TMm_PagesMenuItem', 'usesHeroBoxes()');
		$this->addModuleURLTarget($target);

		$target = TSm_ModuleURLTarget::init( 'delete-page-hero-box');
		$target->setModelName('TMm_PageHeroBox');
		$target->setTitle('Delete hero box');
		$target->setModelInstanceRequired();
		$target->setParent($hero_box_list, 'menuItem()');
		$target->setValidationClassAndMethod('TMm_PagesMenuItem', 'usesHeroBoxes()');
		$target->setModelActionMethod('delete');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		//////////////////////////////////////////////////////
		//
		// PAGE BUILDER TARGETS
		//
		// Anything with the page builder has to deal with
		// potentially multiple types since it handles all the
		// renderers.
		//
		//////////////////////////////////////////////////////
		
		// The page builder that is loaded by any of the editors
		$target = TSm_ModuleURLTarget::init( 'page-builder');
		$target->setViewName('TMv_PageBuilder_ThemeWrapper');
		$target->setModelName('TMm_PagesMenuItem');
	//	$target->setTitleUsingModelMethod('title');
		$target->setTitle('Page content');
		$target->setIconCode('fa-pencil-ruler');
		$target->setModelInstanceRequired();
		if(TC_getConfig('use_tungsten_9'))
		{
			$target->setParentURLTargetWithName('edit');
		}
		else
		{
			$target->setTitleUsingModelMethod('title');
		}
		$target->setValidationClassAndMethod('TMm_PagesMenuItem','userCanEdit');
		$target->setAsPrimaryModelEditTarget(); // Page builder is the primary way to edit a menu
		$target->setAsModelSubmenu(2);
		$this->addModuleURLTarget($target);
		
		
		// The page builder that is loaded by any of the editors
		$target = TSm_ModuleURLTarget::init( 'add-section');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('addSection');
		$target->setTitle('Add page content');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/layout_class_name
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'add-row');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('addRow');
		$target->setTitle('Add row');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/section_id
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'add-column');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('addColumn');
		$target->setTitle('Add column');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/row_id
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'delete-content');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('deleteContent');
		$target->setTitle('Delete content');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/content_id
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'toggle-content-css-class');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('toggleCSSClassForContent');
		$target->setTitle('Toggle CSS content class');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/content_id/css_class
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init( 'toggle-content-visibility');
		$target->setModelName('TMv_PageBuilder');
		$target->setModelActionMethod('toggleContentVisibility');
		$target->setTitle('Toggle content visibility');
		$target->setAsStaticFunctionCall();
		$target->setPassValuesIntoActionMethodType('url'); // /renderer_class/renderer_id/content_id
		$target->setNextURLTarget('referrer'); // just in case it loads normally, we return
		$this->addModuleURLTarget($target);
		
		
		
		//////////////////////////////////////////////////////
		//
		// IMPORT / EXPORT
		//
		//////////////////////////////////////////////////////
		$target = TSm_ModuleURLTarget::init( 'bulk-import');
		$target->setViewName('TMv_PagesBulkImportForm');
		$target->setTitle('Bulk import');
		$target->setValidationClassAndMethod('TMm_PagesModule','useBulkImport');
		
		if(TC_getConfig('use_tungsten_9'))
		{
			$target->setParentURLTargetWithName('manage');
			$target->setAsRightButton('fa-file-import');
		}
		else // Tungsten 8
		{
			$target->setParentURLTargetWithName('settings');
			
		}
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('download-pages-excel');
		$target->setParentURLTargetWithName('bulk-import');
		$target->setTitle('Download excel');
		$target->setModelName('TMm_PagesMenuItemList');
		$target->setModelActionMethod('generateExcel');
		$target->setNextURLTarget(NULL);
		$target->setValidationClassAndMethod('TMm_PagesModule','useBulkImport');
		$this->addModuleURLTarget($target);
		
		//////////////////////////////////////////////////////
		//
		// CSS STYLES
		//
		// Styles for pages
		//
		//////////////////////////////////////////////////////
		
		// Tungsten 9 deprecates this entirely
		// Instead you define the classes in the theme itself
		// @see TMv_PagesTheme::componentViewStyles()
		
		if(!TC_getConfig('use_tungsten_9'))
		{
			$this->generateDefaultURLTargetsForModelClass(
				'TMm_PagesViewStyle',
				'css-style',
				false,
				false,
				TC_currentUserIsDeveloper());
		}

		$this->generateDefaultURLTargetsForModelClass(
			'TMm_PagesTemplateSetting',
			'template-setting'
			);
		
		//////////////////////////////////////////////////////
		//
		// 301 REDIRECTS
		//
		// Note that the page-specific 301 redirection functionality
		// is handled by the main TSc_ModuleController which detects that
		// a page uses the trait and applies it as necessary.
		//
		//////////////////////////////////////////////////////
		$this->generateDefaultURLTargetsForModelClass(
			'TMm_Pages301Redirect',
			'301-redirect');
		
		
		//////////////////////////////////////////////////////
		//
		// NAVIGATION TYPES
		//
		//////////////////////////////////////////////////////
		
		$this->generateDefaultURLTargetsForModelClass(
			'TMm_PagesNavigationType',
			'navigation',
			false,
			false,
			TC_currentUserIsDeveloper());
		
		
		// Tungsten 9 does NOT use the main 'edit'
		// Instead, have it redirect to page builder
		if(TC_getConfig('use_tungsten_9'))
		{
//			$this->removeURLTargetWithName('edit');
			$edit = $this->URLTargetWithName('edit');
			$edit->setViewName('TMv_PageMenuItem_AttributesForm');
			$edit->setTitle('Attributes');
			$edit->setIconCode('fa-sliders-h');
		}
		
		//////////////////////////////////////////////////////
		//
		// METADATA
		//
		//////////////////////////////////////////////////////
		
		/**
		 * Generates the sitemap XML that is used on the site.
		 */
		$target = TSm_ModuleURLTarget::init( 'sitemap_xml');
		$target->setViewName('TMv_PagesSiteMapXML');
		$target->setTitle('Sitemap XML');
		$target->setAsSkipsAuthentication();
		$target->setReturnViewHTMLOnly();
		$this->addModuleURLTarget($target);
		
		// DEAL WITH THE OTHER ITEMS ASSOCIATED WITH THE MODEL
		
		// METADATA PAGE MODEL VIEWABLE
		$this->detectAdvancedMetadataForModel('TMm_PagesMenuItem', '');
		
		// 301 REDIRECTS
		$this->detect301RedirectsForModel('TMm_PagesMenuItem', '');

		// HISTORY
		$this->detectHistoryForModel('TMm_PagesMenuItem', '');
		
		
		//////////////////////////////////////////////////////
		//
		// PUBLISHING
		//
		//////////////////////////////////////////////////////
		// Tungsten 9 does NOT use the main 'edit'
		// Instead, have it redirect to page builder
		if(TC_getConfig('use_tungsten_9') && TC_getConfig('use_pages_version_publishing'))
		{
			$target = TSm_ModuleURLTarget::init('install-test');
			$target->setTitle('Publish');
			$target->setModelName('TMm_PagesMenuItem');
			$target->setModelActionMethod('install-test');
			$target->setNextURLTarget('referrer');
			$this->addModuleURLTarget($target);
			
			
			$target = TSm_ModuleURLTarget::init('publish');
			$target->setTitle('Publish');
			$target->setModelName('TMm_PagesMenuItem');
			$target->setModelInstanceRequired();
			$target->setModelActionMethod('publish');
			$target->setNextURLTarget('referrer');
			$this->addModuleURLTarget($target);
		
			$target = TSm_ModuleURLTarget::init('discard-unpublished-changes');
			$target->setTitle('Discard unpublished');
			$target->setModelName('TMm_PagesMenuItem');
			$target->setModelInstanceRequired();
			$target->setModelActionMethod('discardUnpublishedChanges');
			$target->setNextURLTarget('referrer');
			$this->addModuleURLTarget($target);
		}
		
		//////////////////////////////////////////////////////
		//
		// SUBMENU GROUPS
		//
		//////////////////////////////////////////////////////
		if(!TC_getConfig('use_tungsten_9'))
		{
			$this->disableNavigationSubsections();
			
			//$this->defineSubmenuGroupingWithURLTargets('edit', 'page-builder', 'page-hero-boxes');
			$this->defineSubmenuGroupingWithURLTargets(
				'settings',
				'navigation-list',
				'template-setting-list',
				'page-components',
				'css-style-list', // stays in T8, gone in T9
				'301-redirect-list',
				'bulk-import'
			);
		}
	}
	


}
?>