/**
 * The class JS file for the page editor form for a TCv_Photo. This handles the events related to the form interaction
 *
 */
class TMv_Photo_PageEditorForm extends TCv_Form
{
	constructor(element, options) {
		super(element, options);

		this.addFieldEventListener('crop_ratio','change',this.ratioChanged,true);

		this.addFieldEventListener('target','change',this.targetChanged,true);

		this.addFieldEventListener('is_placeholder','change',this.placeholderChanged,true);

	}

	/**
	 * Event handler for when the target changes
	 */
	targetChanged() {
		let field_value = this.fieldValue('target');

		if(field_value === 'none')
		{
			this.hideFieldRows([
				'redirect_to_menu',
				'redirect_url',
				'linked_file',
				'track_outbound',
				'new_window',
			]);

		}
		else if(field_value === 'menu_item')
		{
			this.hideFieldRows([
				'redirect_url',
				'linked_file',
				'track_outbound',
			]);

			this.showFieldRows([
				'redirect_to_menu',
				'new_window',
			]);

		}
		else if(field_value === 'another_website')
		{
			this.hideFieldRows([
				'redirect_to_menu',
				'linked_file',
			]);

			this.showFieldRows([
				'redirect_url',
				'track_outbound',
				'new_window',
			]);

		}
		else if(field_value === 'file')
		{
			this.hideFieldRows([
				'redirect_to_menu',
				'redirect_url',
			]);

			this.showFieldRows([
				'new_window',
				'linked_file',
				'track_outbound',
			]);

		}
	}


	/**
	 * Event handler for when the ratio changes
	 */
	ratioChanged() {
		let new_value = this.fieldValue('crop_ratio');
		let aspect_ratio_field = this.element.querySelector('#image_file_aspect_ratio');
		if(aspect_ratio_field)
		{
			aspect_ratio_field.value = new_value;
			aspect_ratio_field.dispatchEvent(new Event('change'));

		}
	}

	/**
	 * Event handler for when the placeholder changes
	 */
	placeholderChanged() {
		if(parseInt(this.fieldValue('is_placeholder')) > 0) // Is placeholder, hide the row
		{
			this.hideFieldRows('image_file');
			this.element.querySelector('#image_file').removeAttribute('required');
		}
		else // Not a placeholder
		{
			this.showFieldRows('image_file');
			this.element.querySelector('#image_file').setAttribute('required','required');
		}
		this.targetChanged();

	}

}