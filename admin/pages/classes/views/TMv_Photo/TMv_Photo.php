<?php
/**
 * Class TMv_Photo
 */
class TMv_Photo extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $upload_path = '/assets/pages/';
	protected $file = '';
	protected $crop_ratio = '';
	protected $crop_max_dimension = 1200;
	protected $new_window = 0;
	protected $primary_image_file = false;
	protected $target = '';
	protected $redirect_to_menu = '';
	protected $alt_text = '';
	protected $image_file = ''; // The name of the image file
	protected $is_placeholder = '0';
	protected $image_width;
	protected $caption = '';
	
	protected $redirect_url;
	protected $linked_file;
	
	
	/**
	 * TMv_Photo constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_Photo');
		
		$this->setTag('figure');
		
		// Determine the max crop width
		$this->crop_max_dimension = TC_getConfig('content_width');
		
		
		
	}

	/**
	 * The upload path
	 * @param string $upload_path
	 */
	public function setUploadFolder($upload_path)
	{
		$this->upload_path = $upload_path;
	}


	/**
	 * Manually set the filename
	 * @param string $filename
	 */
	public function setFilename($filename)
	{
		$this->image_file = $filename;
	}
	
	
	/**
	 * Returns the cropper data values for the photo
	 * @return array
	 */
	public function cropperDataValues()
	{
		$values = array();

		if($this->imageFile())
		{
			if($crop_values = $this->imageFile()->cropValues())
			{
				$values['x'] = $crop_values['src_x'];
				$values['y'] = $crop_values['src_y'];
				$values['width'] = $crop_values['src_width'];
				$values['height'] = $crop_values['src_height'];
				$values['scaleX'] = 1;
				$values['scaleY'] = 1;
				$values['rotate'] = 0;
			}
			
		}
		
		return $values;
	}
	
	/**
	 * Returns the name of the file
	 * @return string
	 */
	public function filename()
	{
		return $this->localizeProperty('image_file');
	}


	/**
	 * Returns the image file if it exists
	 * @return bool|TCm_File
	 */
	public function imageFile()
	{
		if($this->primary_image_file === false)
		{
			if($content = $this->contentItem())
			{
				$renderer = $content->renderer();
				$this->primary_image_file = new TCm_File(
					$this->id().'file',
					$this->filename(),
					$renderer::uploadFolder(),
					TCm_File::PATH_IS_FROM_SERVER_ROOT);
				
			}
			
		}
		return $this->primary_image_file;
	}
	
	/**
	 * The content icon, which is an override to show the photo
	 * @return bool|TCv_Image
	 */
	public function contentIconOverride()
	{
		if($this->imageFile())
		{
			$photo = new TCv_Image($this->attributeID().'_photo', $this->imageFile());
			$photo->scaleInsideBox(40, 30);
			return $photo;
		}
		return false;
	}
	
	/**
	 * The url that this photo links to when clicked
	 * @return mixed|string
	 */
	public function buttonURL()
	{
		$url = '#';
		if($this->target == 'menu_item')
		{
			// We must detect if there's a pageviewcode, in which case we just include that
			if(substr($this->redirect_to_menu,0,12) == '/{{pageview:')
			{
				$url = $this->redirect_to_menu; // parsed by the page renderer
			}
			else // legacy menu item, so find the ID and render it
			{
				/** @var TMm_PagesMenuItem $menu */
				$menu = TMm_PagesMenuItem::init( $this->redirect_to_menu);
				if($menu)
				{
					$url = $menu->pathToFolder();
				}
			}
		}
		elseif($this->target == 'another_website')
		{
			$url = $this->localizeProperty('redirect_url');
		}
		elseif($this->target == 'file')
		{
			$url = $this->upload_path.$this->localizeProperty('linked_file');
		}
		elseif($this->target == 'original')
		{
			$url = $this->upload_path.$this->filename();
			
			// REMOVE THE CROPPING VALUES
			$crop_position = strpos($url,'{CROP');
			if($crop_position > 0)
			{
				$url = substr($url,0,$crop_position);
			}
		}
		return $url;
		
	}


	/**
	 * Returns the view of the image for this photo
	 * @return bool|TCv_Image|TCv_View
	 */
	protected function photoView()
	{
		if($this->is_placeholder)
		{
			$view = new TCv_View();
			$view->addClass('placeholder');
			
			$icon = new TCv_View();
			$icon->setTag('i');
			$icon->addClass('fas fa-image');
			$icon->addClass('placeholder_icon');
			$view->attachView($icon);
			
			$text = new TCv_View();
			$text->addClass('explanation');
			$text->addText($this->alt_text);
			$view->attachView($text);
			
			return $view;
			
			
		}
		
		if($file = $this->imageFile())
		{
			
			// Generate the image to be shown
			$image = new TCv_Image($this->attributeID() . '_photo', $file);
			
			// calculate the width of this photo as it's rendered in the view
			$width = $this->calculateRenderWidthFromColumns();
			$image->setRenderedColumnWidth($width);
			
			$image->setAltText($this->localizeProperty('alt_text'));
			$image->scaleInsideBox($this->crop_max_dimension, 3000);
			return $image;
		}

		return false;

	}
	
	
	/**
	 * Generates the HTML for this view
	 * @return string
	 */
	public function render()
	{
		$this->legacyUpgrade();

		if($photo_view = $this->photoView())
		{
			if($this->image_width > 0)
			{
				$this->addClass('width_'.$this->image_width);
			}
			
			// Wrap in a link if necessary
			if($this->target != 'none')
			{
				$link = new TCv_Link();
				$link->setURL($this->buttonURL());
				$link->attachView($photo_view);
				//$link->setTitle($this->localizeProperty('link')); // Removed. Breaks with pageview linking
				
				if($this->new_window)
				{
					$link->openInNewWindow();
				}
				
			
				
				$this->attachView($link);
				
			}
			else
			{
				$this->attachView($photo_view);
			}
			if($this->caption != '' && TC_getConfig('use_photo_captions'))
			{
				$figcaption = new TCv_View();
				$figcaption->setTag('figcaption');
				$figcaption->addText($this->caption);
				$this->attachView($figcaption);
			}
			
		}

		
	}


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$upload_folder = ($this->renderer)::uploadFolder();
		$params = [
			'upload_path' => $upload_folder,
			'init_data' => $this->cropperDataValues()
		];
		
		$this->addClassJSFile('TMv_Photo_PageEditorForm');
		$this->addClassJSInit('TMv_Photo_PageEditorForm', $params);
		
		$form_items = array();

		$link = new TCv_FormItem_Select('crop_ratio','Trim and Crop');
		$link->setAsHalfWidth();
		$link->setHelpText('Indicate if the photo should be restricted to a certain ratio of size. The numbers next to the titles indicate the ratio of width to height.');
		$link->addOption('', 'Free Form ');
		$link->addOption('1', 'Square (1:1)');
		$link->addOption('2', 'Wide (2:1)');
		$link->addOption('3', 'Super-2ide (3:1)');
		$link->addOption('1.5', '6x4 photo horizontal (3:2)');
		$link->addOption('0.5', 'Tall (1:2) ');
		$link->addOption('0.333', 'Super-tall (1:3)');
		$link->addOption('0.666', '4x6 Photo vertical (2:3)');

		$form_items['crop_ratio'] = $link;
		
		$field = new TCv_FormItem_Select('image_width','Width');
		$field->setAsHalfWidth();
		$field->setHelpText('Indicate if the photo should be reduced in width by a fixed percentage. ');
		$field->addOption('100', '100% - Full width');
		$field->addOption('75', '75%');
		$field->addOption('50', '50%');
		$field->addOption('25', '25%');
		$form_items['image_width'] = $field;
		
		
		$file = new TCv_FormItem_FileDrop('image_file','Image File');
		$file->setHelpText('The image file to be used.');
		$file->setUseCropper(false, true);
		$file->setIsRequired(true);
		$file->setUploadFolder($upload_folder);
		$form_items['image_file'] = $file;
		
		$field = new TCv_FormItem_TextField('alt_text','Alt Text');
		$field->setHelpText('Describe this photo for people who are unable to see it.
			Learn about <a target="_blank" href="https://en.wikipedia.org/wiki/Alt_attribute">alt text</a>.');
		$form_items['alt_text'] = $field;
		
		if(TC_getConfig('use_photo_captions'))
		{
			$field = new TCv_FormItem_TextField('caption', 'Caption');
			$field->setHelpText('Provide photo credit or details to appear under the photo (optional).');
			$form_items['caption'] = $field;
		}
		$heading = new TCv_FormItem_Heading('actions_heading','Button actions');
		$form_items['actions_heading'] = $heading;
		
		
		$target = new TCv_FormItem_Select('target','Button');
		$target->setHelpText('If this photo is a button, select an action that will occur when the photo is clicked.');
		$target->addOption('none', 'No action');
		$target->addOption('menu_item', 'PAGE - Link to a specific page on this website');
		$target->addOption('file', 'FILE - Link to a file on this website ');
		$target->addOption('another_website', 'SITE – Link to a website outside of this one ');
		$target->addOption('original', 'ORIGINAL – Opens the original photo that was uploaded');
		$target->setAsHalfWidth();
		$form_items['target'] = $target;
		
		$placeholder = new TCv_FormItem_Select('is_placeholder','Use Placeholder');
		$placeholder->setHelpText("Choose if you want to show a grey placeholder with alt text instead of showing the photo.");
		$placeholder->addOption(0,'No');
		$placeholder->addOption(1,'Yes – Hide the photo, show a placeholder with the alt text');
		$placeholder->setAsHalfWidth();
		$form_items['placeholder'] = $placeholder;
		
		
		$redirect = new TMv_FormItem_LinkablePageSelect('redirect_to_menu','Page');
		$redirect->setHelpText('Select the menu that this will redirect to');
		$redirect->addClass('target_option');
		$redirect->addOption('','Select a page');
		$form_items['redirect_to_menu'] = $redirect;
		
		
		$title = new TCv_FormItem_TextField('redirect_url','Website Address (URL)');
		$title->setHelpText('The website address or URL, which should start with http://.');
		$title->setDefaultValue('http://');
		$form_items['redirect_url'] = $title;
		
		$link = new TCv_FormItem_FileDrop('linked_file','Linked File');
		$link->setHelpText('The file that will be linked to.');
		//$link->setUseCropper();
		$link->setUploadFolder($upload_folder);
		$form_items['linked_file'] = $link;
		
		$new_window = new TCv_FormItem_Select('new_window','Open In New Window');
		$new_window->addOption('0', 'NO - Links opens in the same browser window');
		$new_window->addOption('1', 'YES - Links opens in a new browser window');
		//$new_window->setAsHalfWidth();
		$form_items['new_window'] = $new_window;
		
		
		
		
//		$heading = new TCv_FormItem_Heading('image_settings_heading','Image Settings');
//		$form_items['image_settings_heading'] = $heading;

		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string  { return 'Photo'; }
	public static function pageContent_IconCode() : string  { return 'fa-image'; }
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A general content layout which is never actually used. All content layouts should extend this class.'; 
	}

	//////////////////////////////////////////////////////
	//
	// LEGACY SUPPORT
	//
	// Methods related to older functions that update to hte
	// newer approach of handling photos
	//
	//////////////////////////////////////////////////////

	/**
	 * Tests if this photo is old and upgrades
	 */
	public function legacyUpgrade()
	{
		// cropper value not set
		$old_file = $this->contentItem()->variable('file');
		if($old_file != '') 
		{
			if( ! isset($this->cropper) )
			{
				$values = array();
				$values['cropper'] = '';
				$values['crop_ratio'] = '';
				$values['image_file'] = $this->file.'{CROP,0,0';

				$old_link = $this->contentItem()->variable('link');
				if($old_link != '')
				{
					$values['target'] = 'another_website';
					$values['redirect_url'] = $old_link;
				}
				else
				{
					$values['target'] = 'none';
				}
				
				list($width, $height) = getimagesize($_SERVER['DOCUMENT_ROOT'].$this->upload_path.$old_file);

				$values['image_file'] .= ','.$width.','.$height.'}';

				// Process Each one
				foreach($values as $name => $value)
				{
					$this->contentItem()->updateVariablesWithArray($values);
				}	
				
				// Resave variable names				
				foreach($this->contentItem()->variables() as $variable_name => $value)
				{
					$this->$variable_name = $value;
				}
			
			
				//$new_path = $_SERVER['DOCUMENT_ROOT'].$this->upload_path.$old_file
				//copy($_SERVER['DOCUMENT_ROOT'].$this->upload_path.$old_file, $_SER'bar/test.php');
			}	
		}

		elseif(isset($this->crop_x))
		{
			// interim solution exists with crop values stored in separate values
			// move them into the filename to match with new system of TCv_Image
			$variables = $this->contentItem()->variables();
			$variables['image_file'] .= '{CROP,'.$this->crop_x.','.$this->crop_y.','.$this->crop_width.','.$this->crop_height.'}';
			unset($variables['crop_x']);
			unset($variables['crop_y']);
			unset($variables['crop_width']);
			unset($variables['crop_height']);

			$this->contentItem()->updateVariablesWithArray($variables);

		}
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTENT FLAGGING
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the flags for a given page content item. This method should use that content item to check and return
	 * any flags specific to this view. This is commonly done by checking the variables against the expected values
	 * @param TMm_PagesContent $page_content
	 * @return TMm_ContentFlag[]
	 */
	public static function flagsForPageContent(TMm_PagesContent $page_content) : array
	{
		$flags = [];
		if($page_content->variable('alt_text') == '')
		{
			$flags[] = new TMm_ContentFlag('no_alt_text','Photo missing alt text');
		}
		return $flags;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		return [
			'alt_text' => true,
			'caption' => true,
		
		];
	}

}

?>