<?php
class TMv_PageHeroBox extends TCv_View
{
	/** @var TMm_PageHeroBox $hero_box */
	protected $hero_box;

	/**
	 * TMv_PageHeroBoxes constructor.
	 * @param TMm_PageHeroBox $hero_box
	 */
	public function __construct($hero_box)
	{
		$this->hero_box = $hero_box;
		parent::__construct('hero_box_'.$hero_box->id());
		$this->addClassCSSFile('TMv_PageHeroBox');

	}


	/**
	 *
	 */
	public function html()
	{
		$photo_view = new TCv_Image(false, $this->hero_box->photoFile());

		// Pass in the width and height of the expected image to force a cropping
		//$photo_view->generateCroppedImageFile(TC_getModuleConfig('pages','image_crop_width'), TC_getModuleConfig('pages','image_crop_height'));

		$this->setAttribute('style',
							" background-image:url('".$photo_view->cacheFileURL()."');
									background-position:".$this->hero_box->backgroundPosition().";
									");

		$content_container = new TCv_View();
		$content_container->addClass('content_width_container');

		$title = new TCv_View();
		$title->addClass('hero_box_title');
		$title->addText($this->hero_box->title());
		$content_container->attachView($title);

		$description = new TCv_View();
		$description->addClass('hero_box_description');
		$description->addText($this->hero_box->description());
		$content_container->attachView($description);

		if($this->hero_box->buttonText() != '')
		{
			$button = TMv_QuickButton::init('button_' . $this->hero_box->id());
			$button->addText($this->hero_box->buttonText());
			$button->setTargetValue($this->hero_box->target());
			$button->setRedirectToMenu($this->hero_box->redirectToMenuID());
			$button->setRedirectToURL($this->hero_box->redirectToURL());
			$content_container->attachView($button);
		}
		$this->attachView($content_container);

		return parent::html();
	}
	

}

?>