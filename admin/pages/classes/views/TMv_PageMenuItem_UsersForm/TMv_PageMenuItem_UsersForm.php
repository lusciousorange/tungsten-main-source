<?php
/**
 * Class TMv_PageMenuItem_UsersForm
 *
 * The form to edit a menu item's photo.
 */
class TMv_PageMenuItem_UsersForm extends TCv_FormWithModel
{
	
	/**
	 * TMv_PageMenuItem_PhotoForm constructor.
	 * @param bool|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->crop_width = TC_getModuleConfig('pages', 'image_crop_width');
		$this->crop_height = TC_getModuleConfig('pages', 'image_crop_height');
		
		$this->setButtonText('Update user access');
	}
	
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
		
		// Get the pages module
		$pages = TMm_PagesModule::init();
		
		// Exit if they don't have full access
		if(!$pages->userHasFullAccess())
		{
			$field = new TCv_FormItem_HTML('no_access_for users', '');
			$field->addText("You don't have permission to edit these values");
			$this->attachView($field);
			
			return;
		}
		
		
		$field = new TCv_FormItem_HTML('existing access', 'Existing user groups');
		$field->setHelpText("These user groups have existing full access to the Pages module");
		
		$list = new TCv_View();
		$list->setTag('ul');
		
		$permitted_user_groups = $pages->permittedUserGroups();
		foreach($permitted_user_groups as $user_group)
		{
			$list->addText('<li>'.$user_group->title().'</li>');
		}
		$field->attachView($list);
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('user_group_access', 'Permitted user group');
		$field->setHelpText('The comma-separated list of user IDs that are considered super-users which permit them to move an item from any stage, to any stage.');
		$field->setUseMultiple(
			TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
			'pages_user_group_access',
			'group_id',
			$this->model()
		);
		
		$user_group_list = TMm_UserGroupList::init();
		
		
		foreach($user_group_list->groups() as $user_group)
		{
			if(!isset($permitted_user_groups[$user_group->id()]))
			{
				$field->addOption($user_group->id(), $user_group->title());
			}
			
		}
		$field->useFiltering();
		
		$this->attachView($field);
		
	}
	
	
}