<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

	/** @var TMt_PageRenderer $renderer_item */
	$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
	
	/** @var TMt_PageRenderItem|TCm_Model $content_item */
	$content_item = ($_POST['content_model_class_name'])::init($_POST['content_id']);
	
	$row_num = htmlentities($_POST['row_num']);
	$col_num = htmlentities($_POST['col_num']);
	
	/** @var string|TCv_View|TMt_PagesContentView $view_class */
	$view_class = $content_item->viewClass();
	
	// Reset Tungsten
	$tungsten->clearCSS();
	$tungsten->clearJS();
	$tungsten->clear();

	// Create Form
	$form = new TMv_HTMLTableCellForm($content_item, $row_num, $col_num);
	$tungsten->attachView($form);
	
	$icon_css_class = implode(' ', $tungsten->processClassName($view_class::pageContent_IconCode()));
	
	// Save Values
	$values = array(
		'content_id' => $content_item->id(),
		'html' => $tungsten->bodyContent(),
		'css' => $tungsten->headCSSTags(),
		'js' => $tungsten->headJSTags(false),
		'view_class' => $view_class,
		'view_icon' => $icon_css_class,
		'view_title' => "Table Cell : Row $row_num, Column $col_num",
	
	);
	echo json_encode($values);

