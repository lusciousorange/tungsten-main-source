<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

	/** @var TCm_Model|TMt_PageRenderItem $content_item */
	$content_item = ($_POST['content_model_class_name'])::init($_POST['content_id']);
	
	$view_class = $content_item->viewClass();

	$view = new TCv_View('content_style_button_list');

	//$view->addText($content_item->id());

	// Use the theme values, which also gets the old ones from the DB as well
	$pages_module = TMm_PagesModule::init();
	$theme_name = $pages_module->editorThemeName();
	
	foreach($theme_name::allComponentCSSViewStyles() as $view_style)
	{
		if($view_style->viewClassIsPermitted($view_class))
		{
			$link = new TCv_Link();
			$link->addClass('style_toggle_button');
			$link->addText('<div class="style_title">'.$view_style->title().'</div>');
			$description = new TCv_View();
			$description->addClass('style_desc');
			$description->setTag('span');
			$description->addText($view_style->description());
			$link->attachView($description);
			$link->addDataValue('class-name', $view_style->className());
			$link->addDataValue('content-id', $content_item->id());
			$link->addDataValue('style-id', $view_style->id());
			$link->setURL('#');
			if($content_item->hasCSSClass($view_style->className()))
			{
				$link->addClass('selected');
			}
			$view->attachView($link);
			//$all_classes[$view_style->className()] = $view_style->title();
//			$view->addText('<br />'.$view_style->className());
		}

	}
	
	// Save Values
	$values = [
		'content_id' => $content_item->id(),
		'html' => $view->html(),

		];
	echo json_encode($values);

?>