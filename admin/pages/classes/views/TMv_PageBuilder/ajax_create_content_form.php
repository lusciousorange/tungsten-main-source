<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

	/** @var TMt_PageRenderer $renderer_item */
	$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
	
	
	/** @var TMt_PagesContentView $view_class */
	$view_class = $_POST['view_class'];
	
	
	/** @var TMt_PageRenderer $renderer_object */
	$renderer_object = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);


	// This script runs separately and it should really set an active model for consistency
	TC_saveActiveModel($renderer_object);
	
	// Reset Tungsten
	$tungsten->clearCSS();
	$tungsten->clearJS();
	$tungsten->clear();
	
	// Create Form
	$form_name = $renderer_object->pageRenderItem_ContentFormClassName();
	/** @var TMv_PageRendererContentForm $form */
	$form = new $form_name($_POST['content_model_class_name']);
	$form->setRendererItem($renderer_object);
	$form->setContentViewClass($view_class);
	$form->setContainerID($_POST['container_id']);
	$tungsten->attachView($form);
	
	$icon_css_class = implode(' ', $tungsten->processClassName($view_class::pageContent_IconCode()));
	
	// Save Values
	$values = array(
		
		'html' => $tungsten->bodyContent(),
		'css' => $tungsten->headCSSTags(),
		'js' => $tungsten->headJSTags(false),
		'view_class' => $view_class,
		'view_icon' => $icon_css_class,
		'view_title' => $view_class::pageContent_ViewTitle(),
		
	
		);
	echo json_encode($values);


?>