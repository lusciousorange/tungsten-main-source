<?php

/**
 * Class TMv_PageBuilder
 * The view used to build pages in the Pages module. It works by adding TMv_ContentLayoutSection items to a page, and
 * using the settings within that layout to manage content.
 */
class TMv_PageBuilder extends TMv_PageContentRendering
{
	protected $content_container;
	protected $columns_show_settings = true;
	protected $layouts_show_settings = true;
	protected $layouts_show_visibility = true;
	protected $content_show_visibility = true;
	protected $content_show_workflow = true;
	protected $content_class_names = false;
	protected $show_styles = true;
	protected $show_all_previews = false;
	
	protected string $show_preview_button_text = 'Show preview';
	protected string $hide_preview_button_text = 'Hide preview';
	
	/**
	 * TMv_PageBuilder constructor.
	 * @param TMt_PageRenderer $renderer_item The menu item that we're generating a page builder for
	 */
	public function __construct($renderer_item)
	{
		parent::__construct($renderer_item);
		
		// Generate the unpublished version if it doesn't exist
		$renderer_item->generateUnpublishedVersion();
		
		// Turn on publishing mode
		static::enablePublishingMode($renderer_item);
		
		
		
		// Ensure new layouts work as expected
		$this->loadLayoutCSSFiles();
		
		$this->addClassCSSFile('TMv_PageBuilder');
		$this->addClassJSFile('TMv_PageBuilder');
		$init_values = array();
		$init_values['renderer_item_id'] = $this->renderer_item->rendererID();
		$init_values['renderer_item_class_name'] = get_class($renderer_item);
		$init_values['renderer_view_name'] = get_called_class();
		$init_values['content_model_class_name'] = $this->renderer_item->renderItemClassName();
		$this->addClassJSInit('TMv_PageBuilder', $init_values);
		
		// Load up symbols in case of blank interface
		$this->addFontAwesomeClassAsSymbol('fa-cog', 'fa-cog');
		$this->addFontAwesomeClassAsSymbol('fa-plus', 'fa-plus');
		$this->addFontAwesomeClassAsSymbol('fa-paint-brush', 'fa-paint-brush');
		$this->addFontAwesomeClassAsSymbol('fa-eye', 'fa-eye');
		$this->addFontAwesomeClassAsSymbol('fa-eye-slash', 'fa-eye-slash');
		$this->addFontAwesomeClassAsSymbol('fa-long-arrow-up', 'fa-long-arrow-up');
		$this->addFontAwesomeClassAsSymbol('fa-long-arrow-down', 'fa-long-arrow-down');
		$this->addFontAwesomeClassAsSymbol('fa-trash-alt', 'fa-trash-alt');
		$this->addFontAwesomeClassAsSymbol('select_caret', 'fas fa-caret-down'); // used in popup forms
		$this->addFontAwesomeClassAsSymbol('fa-comments', 'fa-comments'); // workflow
		
		$this->addFontAwesomeClassAsSymbol('TCv_Form9_select_arrow', 'fas fa-chevron-down');
		
		
		// Load SortableJS
		// @see https://sortablejs.github.io/Sortable/
		$this->addJSFile('SortableJS','https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js');
		
		
		
		$this->enableAllViews(); // turn on the option to show all views
		
		$this->content_container = new TCv_View('page_builder_content_container');
		if($this->renderer_item instanceof TMm_PagesMenuItem)
		{
			$this->content_container->addClass('section_' . $this->renderer_item->parentForLevel(1)->folder());
		}
		
	}
	
	/**
	 * Handles processing the theme name
	 * @return void
	 */
	protected function processThemeName() : void
	{
		$pages_module = TMm_PagesModule::init();
		$theme_name = $pages_module->editorThemeName();
		
		if($theme_name == '')
		{
			print 'No theme set';
			exit();
		}
	}
	
	/**
	 * Sets the names of the classes that will count as content to be added
	 * @param string[] $names
	 */
	public function setContentClassNames($names)
	{
		$this->content_class_names = $names;
	}
	
	/**
	 * Returns the view if there is no content available for the page being viewed.
	 *
	 * @return TCv_View
	 */
	public function noContentView()
	{
		$view = new TCv_View('no_content_view');
		$view->addClass('TMv_ContentLayout');
		//	$view->addClass('TMv_PagesTheme');
		$container = new TCv_View();
		$container->addClass('content_width_container');
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		
		$singular_title = ($this->renderer_item)::$model_title;
		
		$explanation->addText($singular_title . ' currently has no content. You can add content by using the
<strong>Add New Layout Row</strong> button. Once you add a layout section, you can add content items as you see fit.');
		$container->attachView($explanation);
		$view->attachView($container);
		
		return $view;
	}
	
	/**
	 * Returns the view if there is no content available for the page being viewed.
	 * @return TCv_View
	 */
	public function notManagedView()
	{
		$view = new TCv_View('no_content_view');
		$view->addClass('TMv_ContentLayout');
		$view->addClass('TMv_PagesTheme');
		
		$container = new TCv_View();
		$container->addClass('content_width_container');
		
		$explanation = new TCv_View();
		$explanation->setTag('p');
		$explanation->addText('This page is not setup to have content managed. This means that the menu item
		performs another action other than showing content. ');
		if(TC_getConfig('use_tungsten_9'))
		{
			$explanation->addText('If this is incorrect, you can change the settings in the advanced settings
			found in the right panel for this page. ');
			
		}
		else
		{
			$explanation->addText('If this is incorrect, you can change the settings in
		the <a href="/admin/pages/do/edit/"'. $this->renderer_item->id() . '\">Page properties</a>.');
		
		}
		$container->attachView($explanation);
		$view->attachView($container);
		
		return $view;
	}
	
	
	/**
	 * Turns on all previews for this builder, which will ignore the settings related to hiding them. Commonly used
	 * for Tungsten-side views that need to show them all.
	 */
	public function enableAllPreviews()
	{
		$this->show_all_previews = true;
	}
	
	/**
	 * The explanation of the visibility condition
	 * @param TMt_PageRenderItem $content_model
	 * @return mixed|string
	 */
	public function visibilityConditionExplanation($content_model)
	{
		if(method_exists($content_model, 'visibilityCondition'))
		{
			$condition = $content_model->visibilityCondition();
			if($condition != '')
			{
				$condition = str_replace(array('{{','}}','TMm_'), '', $condition);
				$condition = str_replace('current-user', 'Current user', $condition);
				
				// Get rid of spaces right before or after periods
				$condition = str_replace(array(' .','. ','.'), ' ', $condition);
				
				return $condition;
			}
			
		}
		return '';
	}
	
	/**
	 * Extend functionality to deal with page builder scenarios
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_model
	 * @return bool|TCu_Item|TMv_NoContentPreview|TMt_PagesContentView
	 */
	public function viewForContentModel($content_model)
	{
		// Check for cached version
		if(isset($this->content_views[$content_model->id()]))
		{
			return $this->content_views[$content_model->id()];
		}
		
		/** @var string|TMt_PagesContentView $view_class_name */
		$view_class_name = $content_model->viewClass();
		
		$view = null;
		
		// Handle view with a classname that doesn't exist
		if(!class_exists($view_class_name))
		{
			$this->addConsoleDebugObject('MISSING VIEW : '.$content_model->viewClass(), $content_model);
			
			$no_view_container = new TMv_NoContentPreview($content_model);
			$view = $no_view_container;
		}
		else // View class exists
		{
			
			// Handle not showing previews in builder
			if(
				// That all requires the show all in previews to be set
				// Check first before complicated validation
				!$this->show_all_previews
				&&
				
				// Show the preview if they manually set it to NOT show in the builder
				(!$view_class_name::pageContent_ShowPreviewInBuilder()
					// Input model is a string or an array, we don't show those in page builder
					|| is_array($view_class_name::pageContent_View_InputModelName())
					|| is_string($view_class_name::pageContent_View_InputModelName())
				)
			
			)
			{
				$no_view_container = new TMv_NoContentPreview($content_model);
				$view = $no_view_container;
			}
			
			// Generate the normal view for the model
			else
			{
				/** @var TMt_PagesContentView $view */
				$view = parent::viewForContentModel($content_model);
				
			}
		}
		// Content model doesn't exist, abort
		if(!$content_model instanceof TCm_Model || !$view instanceof TCv_View)
		{
			return false;
		}
		
		// Empty placeholders
		if($view instanceof TMv_ContentLayoutBlock)
		{
			$view->attachEmptyPlaceholder();
		}
		
		// Add content class ID
		$view->addClass('content_' . $content_model->id());
		
		// Handle visibility
		$vis_condition = $this->visibilityConditionExplanation($content_model);
		if($vis_condition != '')
		{
			$view->addClass('invisible'); // add invisible to all of them, so the style gets applied
			$view->addClass('has_vis_condition');
			$view->addDataValue('vis-condition', $vis_condition);
		}
		elseif(!$content_model->isVisible())
		{
			$view->addClass('invisible');
		}
//		if(!$content_model->isVisible())
//		{
//			$view->addClass('invisible');
//			$view->addDataValue('vis-condition', );
//
//		}
		
		// Handle Control Panel
		// Every view can have a control panel set, so we deal with it here
		$control_panel = $this->controlPanelForContentModel($content_model);
		if($control_panel)
		{
			$view->setControlPanelView($control_panel);
			
			// Control Panel for Rows, which are any non-sections
			if($view instanceof TMv_ContentLayout && $view->tag() != 'section')
			{
				$view->attachViewToRoot($control_panel, true);
				
			}
			elseif($view instanceof TMv_ContentLayout_Section)
			{
				// Must be after content to ensure hover works
				$view->attachViewToRoot($control_panel);
				
			}
			
		}
		
		
		
		return $view;
	}
	
	
	/**
	 * Turns off the button for column settings
	 */
	public function disableColumnSettings()
	{
		$this->columns_show_settings = false;
	}
	
	/**
	 * Turns off the button for layout settings
	 */
	public function disableLayoutSettings()
	{
		$this->layouts_show_settings = false;
	}
	
	/**
	 * Turns off the button for layout settings
	 */
	public function disableLayoutVisibility()
	{
		$this->layouts_show_visibility = false;
	}
	
	/**
	 * Turns off the button for layout settings
	 */
	public function disableContentVisibility()
	{
		$this->content_show_visibility = false;
	}
	
	/**
	 * Turns off the button for layout settings
	 */
	public function disableContentWorkflow()
	{
		$this->content_show_workflow = false;
	}
	
	/**
	 * Turns off the buttons for styles
	 */
	public function disableStyles()
	{
		$this->show_styles = false;
	}
	
	
	/**
	 * Determines if this content item has at least one style
	 * @param TMt_PageRenderItem $content_item
	 * @return bool
	 */
	public function contentItemHasStyle($content_item)
	{
		$view_class_name = $content_item->viewClass();
		
		$pages_module = TMm_PagesModule::init();
		$theme_name = $pages_module->editorThemeName();
		
		foreach($theme_name::allComponentCSSViewStyles() as $view_style)
		{
			// If this one is permitted, we get out
			if($view_style->viewClassIsPermitted($view_class_name))
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * @param TMt_PageRenderItem|bool $content_item
	 * @param string $type
	 * @param string $title
	 * @param string $icon
	 * @return TCv_Link
	 */
	protected function controlButton($content_item, $type, $title, $icon)
	{
		$traits = class_uses($content_item);
		$uses_render_item = isset($traits['TMt_PageRenderItem']);
		
		if($uses_render_item)
		{
			$button = new TCv_Link('control_'.$type.'_'.$content_item->id());
		}
		else
		{
			$button = new TCv_Link();
			
		}
		$button->addClass('content_button');
		$button->addClass('content_'.$type.'_button');
		$button->setIconClassName($icon, $icon);
		$button->setURL('#');
		
		if($uses_render_item)
		{
			$button->setAttribute('data-content-id', $content_item->id());
		}
		$button->setAttribute('data-button-type', $type);
		$button->setTitle($title);
		$button->addText('<span>'.$title.'</span>');
		
		return $button;
	}
	
	/**
	 * Generates the correct control panel for the provided content item
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View|TCv_Link
	 */
	protected function controlPanelForContentModel($content_item)
	{
		$panel = false;
		if($content_item->isLayoutSection())
		{
			$panel = $this->controlPanelForSection($content_item);
		}
		elseif($content_item->isLayoutRow())
		{
			$panel = $this->controlPanelForRow($content_item);
		}
		elseif($content_item->isLayoutBlock())
		{
			$panel = $this->controlPanelForLayoutBlock($content_item);
		}
		elseif($content_item->isLayoutContent())
		{
			$panel = $this->controlPanelForContent($content_item);
		}
		
		// Deal with applied CSS styles/classes
		$page_view_styles = $content_item->cssPageViewStyles();
		$styles_box = new TCv_View();
		$styles_box->addClass('applied_styles');
		foreach($page_view_styles as $page_view_style)
		{
			$style = new TCv_View();
			$style->addText($page_view_style->title());
			$styles_box->attachView($style);
			
		}
		$panel->attachView($styles_box);
		
		
		if($content_item->hasOverrideIDAttribute())
		{
			$id_attribute_box = new TCv_View();
			$id_attribute_box->addClass('override_id');
			$id_attribute_box->addText('#'.$content_item->idAttribute());
			$panel->attachView($id_attribute_box);
		}
		
		
		// Check for content flags
		if(TC_getConfig('use_content_flagging') && $content_item->hasContentFlags())
		{
			$button = new TMv_ContentFlagButton($content_item);
			$button->addClass('content_button'); // add class that is needed for panels
			$button->setAttribute('data-button-type', 'flags');
			
			// Add the subtitle
			$button->addText('<span>Flags</span>');
			
			$panel->attachView($button,1);
		}
		
		return $panel;
		
	}
	
	/**
	 * Generates a view for a control panel visibility button
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View|TCv_Link
	 */
	protected function controlPanelVisibilityButton($content_item)
	{
		if ($content_item->isVisible())
		{
			$icon = 'fa-eye';
		}
		else
		{
			$icon = 'fa-eye-slash';
		}
		
		$title = 'Toggle visibility';
		
		$condition_text = $this->visibilityConditionExplanation($content_item);
		if($condition_text != '')
		{
			$title = $condition_text;
		}
		
		
		
		$button = $this->controlButton($content_item, 'visible', $title ,$icon);
		return $button;
		
	}

//	/**
//	 * Generates the views related to the delete button for a control panel
//	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
//	 * @param string $message
//	 * @return TCv_View
//	 */
//	protected function controlPanel_DeleteBox($content_item, $message)
//	{
//		$confirm_delete_box = new TCv_View('confirm_delete_' . $content_item->id());
//		$confirm_delete_box->addClass('confirm_delete_box');
//
//		$explanation = new TCv_View();
//		$explanation->addClass('confirm_delete_explanation');
//		$explanation->addText($message);
//
//		$confirm_delete_box->attachView($explanation);
//
//		$cancel_button = new TCv_Link();
//		$cancel_button->addClass('cancel_delete_content_button');
//		$cancel_button->setURL('#');
//		$cancel_button->addText('Cancel');
//		$cancel_button->setAttribute('data-content-id', $content_item->id());
//		$confirm_delete_box->attachView($cancel_button);
//
//		$confirm_button = new TCv_Link();
//		$confirm_button->addClass('confirm_delete_content_button');
//		$confirm_button->setURL('#');
//		$confirm_button->addText('Confirm');
//		$confirm_button->setAttribute('data-content-id', $content_item->id());
//		$confirm_delete_box->attachView($confirm_button);
//
//		return $confirm_delete_box;
//	}
	
	/**
	 * Generates a control panel for a section.
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View
	 */
	protected function controlPanelForSection($content_item)
	{
		$control_panel = new TCv_View('control_panel_'.$content_item->id());
		$control_panel->addClass('control_panel');
		$control_panel->addClass('section_control_panel');
		
		// SETTINGS BUTTON
		if($this->layouts_show_settings)
		{
			$button = $this->controlButton($content_item, 'edit', 'Section properties', 'fa-cog');
			$control_panel->attachView($button);
		}
		
		// ADD ROW BUTTONS
		$button = $this->controlButton($content_item, 'add_row', 'Add row', 'fa-plus');
		$button->setURL('/admin/pages/do/add-row/'
		                .get_class($this->renderer_item).'/'
		                .$this->renderer_item->rendererID().'/'
		                .$content_item->id()
		);
		$control_panel->attachView($button);
		
		
		
		
		// VISIBILITY BUTTON
		if($this->layouts_show_visibility)
		{
			$button = $this->controlPanelVisibilityButton($content_item);
			$control_panel->attachView($button);
		}
		
		
		// STYLES BUTTON
		if($this->show_styles && $this->contentItemHasStyle($content_item))
		{
			$control_panel->attachView($this->controlButton_style($content_item));
			
		}
		
		// REARRANGE BUTTONS
		$button = $this->controlButton($content_item, 'section_move_up', 'Move section up', 'fa-long-arrow-up');
		$control_panel->attachView($button);
		
		$button = $this->controlButton($content_item, 'section_move_down', 'Move section down', 'fa-long-arrow-down');
		$control_panel->attachView($button);
		
		// DELETE BUTTON
		$control_panel->attachView($this->controlButton_delete($content_item));

//		// DELETE CONFIRM BOX
//		$message = 'Delete this section (and all content inside of it)?';
//		$control_panel->attachView($this->controlPanel_DeleteBox($content_item, $message));
		
		return $control_panel;
	}
	
	/**
	 * Generates a control panel for a single row.
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View
	 */
	protected function controlPanelForRow($content_item)
	{
		$control_panel = new TCv_View('control_panel_'.$content_item->id());
		$control_panel->addClass('control_panel');
		$control_panel->addClass('section_row_control_panel');
		
		// SETTINGS BUTTON
		if($this->layouts_show_settings)
		{
			$button = $this->controlButton($content_item, 'edit', 'Row properties', 'fa-cog');
			$control_panel->attachView($button);
		}
		
		// ADD COLUMN BUTTONS
		$button = $this->controlButton($content_item, 'add_column', 'Add column', 'fa-plus');
		$control_panel->attachView($button);
		
		
		
		// VISIBILITY BUTTON
		if($this->layouts_show_visibility)
		{
			$button = $this->controlPanelVisibilityButton($content_item);
			$control_panel->attachView($button);
		}
		
		
		// STYLES BUTTON
		if($this->show_styles && $this->contentItemHasStyle($content_item))
		{
			$control_panel->attachView($this->controlButton_style($content_item));
			
		}
		
		// REARRANGE BUTTONS
		$button = $this->controlButton($content_item, 'row_move_up', 'Move row up', 'fa-long-arrow-up');
		$control_panel->attachView($button);
		
		$button = $this->controlButton($content_item, 'row_move_down', 'Move row down', 'fa-long-arrow-down');
		$control_panel->attachView($button);
		
		// DELETE BUTTON
		$control_panel->attachView($this->controlButton_delete($content_item));
		
		
		
		// DELETE CONFIRM BOX
//		$message = 'Delete this row (and all content inside of it)?';
//		$control_panel->attachView($this->controlPanel_DeleteBox($content_item, $message));
		
		return $control_panel;
	}
	
	/**
	 * Generates a control panel for a layout block.
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View
	 */
	protected function controlPanelForLayoutBlock($content_item)
	{
		$control_panel = new TCv_View('control_panel_'.$content_item->id());
		$control_panel->addClass('control_panel');
		$control_panel->addClass('layout_block_control_panel');
		
		if($this->columns_show_settings)
		{
			$button = $this->controlButton($content_item, 'edit', 'Column properties', 'fa-cog');
			$control_panel->attachView($button);
			
		}
		
		$button = $this->controlButton($content_item, 'add_content', 'Add content to column', 'fa-plus');
		if($button)
		{
			$button->addClass('add_content_button');
			$button->setAttribute('data-layout-block-id', $content_item->id());
			$control_panel->attachView($button);
			
		}
		
		// WORKFLOW COMMENTS
		if( $this->content_show_workflow && TC_getConfig('use_workflow'))
		{
			//$control_panel->attachView($this->controlButton_workflow($content_item));
			// Disabled workflow button comments
		}
		
		
		// REARRANGE BUTTON
		$button = $this->controlButton($content_item, 'column_rearrange', 'Move this column', 'fa-arrows');
		$control_panel->attachView($button);
		
		
		// DELETE CONFIRM BOX
//		$message = 'Delete column (and all content inside of it)?';
//		$control_panel->attachView($this->controlPanel_DeleteBox($content_item, $message));
		
		
		// STYLES BUTTON
		if($this->show_styles && $this->contentItemHasStyle($content_item))
		{
			$control_panel->attachView($this->controlButton_style($content_item));
			
		}
		
		// DELETE BUTTON
		$control_panel->attachView($this->controlButton_delete($content_item));
		
		
		return $control_panel;
	}
	
	/**
	 * Generates a control panel for content.
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_View
	 */
	protected function controlPanelForContent($content_item)
	{
		$control_panel = new TCv_View('control_panel_'.$content_item->id());
		$control_panel->addClass('control_panel');
		$control_panel->addClass('content_control_panel');
		
		// EDIT BUTTON
		// Only editable if view class exists
		if(class_exists($content_item->viewClass()))
		{
			$button = $this->controlButton($content_item, 'edit', 'Edit content', 'fa-pencil');
			$control_panel->attachView($button);
		}
		
		// VISIBILITY BUTTON
		if($this->layouts_show_visibility)
		{
			$button = $this->controlPanelVisibilityButton($content_item);
			$control_panel->attachView($button);
		}
		
		// WORKFLOW COMMENTS
		if( $this->content_show_workflow && TC_getConfig('use_workflow'))
		{
			//$control_panel->attachView($this->controlButton_workflow($content_item));
			
		}
		
		// REARRANGE BUTTON
		$button = $this->controlButton($content_item, 'rearrange', 'Move this content', 'fa-arrows');
		$control_panel->attachView($button);
		
		
		// STYLES BUTTON
		if($this->show_styles && $this->contentItemHasStyle($content_item))
		{
			$control_panel->attachView($this->controlButton_style($content_item));
			
		}
		
		// DELETE BUTTON
		$control_panel->attachView($this->controlButton_delete($content_item));
		
		
		// DELETE CONFIRM BOX
		// MUST BE LAST FOR STYLING
//		$message = 'Delete this content?';
//		$control_panel->attachView($this->controlPanel_DeleteBox($content_item, $message));
		
		
		
		
		return $control_panel;
	}
	
	/**
	 * The control button for the style icon
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_Link
	 */
	protected function controlButton_style($content_item)
	{
		$button = $this->controlButton($content_item, 'styles', 'Styles', 'fa-paint-brush');
		
		$classes = $content_item->cssPageViewStyles();
		$num_styles = count($classes);
		if($num_styles == 0)
		{
			$num_styles = '';
		}
		$indicator = new TCv_View();
		$indicator->addClass('num_styles');
		$indicator->addText($num_styles);
		$button->attachView($indicator);
		
		
		return $button;
	}
	
	/**
	 * The control button for the workflow icon
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_Link
	 */
	protected function controlButton_workflow($content_item)
	{
		return $this->controlButton($content_item,
		                            'workflow_comments',
		                            'Comment in workflow',
		                            'fa-comments');
	}
	
	/**
	 * The control button for the delete button
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return TCv_Link
	 */
	protected function controlButton_delete($content_item)
	{
		return $this->controlButton($content_item,
		                            'delete',
		                            'Delete '.$this->contentItemTypeName($content_item),
		                            'fa-trash-alt');
	}
	
	/**
	 * The content item type name which describes what it is.
	 * @param TMt_PageRenderItem|TMm_PagesContent $content_item
	 * @return string
	 */
	protected function contentItemTypeName($content_item)
	{
		if($content_item->isLayoutSection()) { return 'section'; }
		if($content_item->isLayoutRow()) { return 'row'; }
		if($content_item->isLayoutBlock()) { return 'column'; }
		
		return 'content';
	}
	
	/**
	 * Separate function to attach content to a layout block. Done this way to allow for extending with an editor
	 * @param TMt_PagesContentView $content_view
	 * @deprecated Replaced with individual views for each item
	 */
	protected function controlPanelForContentView($content_view)
	{
		TC_triggerError('Calling deprecated controlPanelForContentView');
	}
	
	/**
	 * @param string $id
	 * @param string $title
	 * @param TCv_View $view
	 * @return TSv_PopupContainer
	 */
	public function buildPopup($id, $title, $view)
	{
		/** @var TSv_PopupContainer $popup_container */
		$popup_container = TSv_PopupContainer::init($id);
		
		$popup_container->setAttribute('aria-modal','true');
		$popup_container->setAttribute('role','dialog');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addClass('popup_heading');
		$cancel_view = new TCv_Link();
		$cancel_view->addClass('cancel_popup');
		$cancel_view->setIconClassName('fa-times');
		$cancel_view->setURL('#');
		$heading->attachView($cancel_view);
		$heading->addText($title);
		$popup_container->attachView($heading);
		
		
		$popup_content = new TCv_View();
		$popup_content->addClass('popup_content');
		$popup_content->attachView($view);
		
		$popup_container->attachView($popup_content);
		
		return $popup_container;
	}
	
	/**
	 * @return TMv_ContentLayout[]
	 */
	public function sectionOptions()
	{
		$layout_list  = TMm_PageLayoutList::init();
		return array_keys($layout_list->pageSections());
	}
	
	/**
	 * Adds the layout popup
	 * @return TSv_PopupContainer
	 */
	public function addSectionPopup() : TCv_View
	{
		$id = 'add_section_popup';
		$title = 'Select a section style';
		
		$target = new TCv_View();
		$target->addClass('button_grid_options');
		
		foreach($this->sectionOptions() as $class_name)
		{
			/** @var TMt_PagesContentView $class_name */
			
			$add_content_link = new TCv_Link();
			$add_content_link->addClass('button_grid_button');
			$add_content_link->addClass('select_layout_button');
			$add_content_link->setAttribute('data-class-name', $class_name);
			$add_content_link->setTitle($class_name::pageContent_ViewTitle());
			$add_content_link->setURL('/admin/pages/do/add-section/'
			                          .get_class($this->renderer_item).'/'
			                          .$this->renderer_item->rendererID().'/'
			                          .$class_name
			);
			
			/** @var TMv_ContentLayout $layout_view */
			$layout_view = new $class_name();
			$preview = $layout_view->preview();
			
			// Add the description inside the preview
			$description = new TCv_View();
			$description->setTag('span');
			$description->addClass('preview_description');
			$description->addText('<h3>'.$class_name::pageContent_ViewTitle().'</h3>');
			$description->addText($class_name::pageContent_ViewDescription());
			$preview->attachView($description);
			
			$add_content_link->attachView($preview);
			
			
			$target->attachView($add_content_link);
			
		}
		
		return $this->buildPopup($id, $title, $target);
		
	}
	
	/**
	 * Adds the content popup
	 * @return TSv_PopupContainer
	 */
	public function addContentPopup()
	{
		/** @var TMm_PageContentList $content_list */
		$content_list  = TMm_PageContentList::init(false);
		if($this->content_class_names === false)
		{
			$this->content_class_names = $content_list->pageContentOptions();
			
		}
		
		
		
		$id = 'add_content_popup';
		$title = 'Add content';
		
		$hidden_views = array();
		$target = new TCv_View();
		
		$target->addClass('button_grid_options');
		foreach($this->content_class_names as $class_name)
		{
			/** @var TMt_PagesContentView $class_name */
			if($class_name::pageContent_IsAddable())
			{
				$add_content_link = new TCv_Link();
				$add_content_link->addClass('button_grid_button');
				$add_content_link->addClass('select_content_option_button');
				$add_content_link->setAttribute('data-class-name', $class_name);
				$add_content_link->setTitle($class_name::pageContent_ViewTitle());
				$add_content_link->setURL('#');
				
				$preview = new TCv_View();
				$preview->setTag('span');
				$preview->addClass('preview');
				$icon = new TCv_View();
				$icon->addClass('content_icon');
				$icon->setTag('span');
				$icon->addClass($class_name::pageContent_IconCode());
				$preview->attachView($icon);
				
				$content_title = new TCv_View();
				$content_title->setTag('span');
				$content_title->addClass('content_title');
				$content_title->addText($class_name::pageContent_ViewTitle());
				$preview->attachView($content_title);
				
				$module_details = new TCv_View();
				$module_details->addClass('module_details');
				$module = $this->moduleForThisClass($class_name);
				$icon_view = new TCv_View();
				$icon_view->setTag('i');
				$icon_view->addClass($module->iconCode());
				$module_details->attachView($icon_view);
				$module_details->addText(' ');
				$module_details->addText($module->title());
				$preview->attachView($module_details);
				
				// Add the desription inside the preview
				$description = new TCv_View();
				$description->setTag('span');
				$description->addClass('preview_description');
				$description->addText('<h3>'.$class_name::pageContent_ViewTitle().'</h3>');
				$description->addText($class_name::pageContent_ViewDescription());
				$preview->attachView($description);
				
				
				
				$add_content_link->attachView($preview);
				//	$layout_view = new $class_name();
				//	$add_content_link->attachView($layout_view->preview());	
				
				
				if($content_list->viewIsHidden($class_name))
				{
					$hidden_views[] = $add_content_link;
				}
				else
				{
					
					$target->attachView($add_content_link);
				}
				
			}
		}
		
		if(sizeof($hidden_views) > 0)
		{
			$heading = new TCv_Link('view_hidden_content_options_link');
			$heading->setURL('#');
			$heading->setAttribute('onclick', "document.getElementById('hidden_content_options').slideToggle(); return false;");
			$heading->addText('View hidden content options');
			$target->attachView($heading);
			
			$box = new TCv_View('hidden_content_options');
			$explanation = new TCv_View();
			$explanation->setTag('p');
			$explanation->addText("These are content options that are used in special cases on this website and their general use isn't recommended. ");
			$box->attachView($explanation);
			foreach($hidden_views as $hidden_view)
			{
				$box->attachView($hidden_view);
			}
			$target->attachView($box);
		}
		return $this->buildPopup($id,  $title, $target);
		
	}
	
	/**
	 * Returns the popup for editing content
	 * @return TSv_PopupContainer
	 */
	public function editContentPopup()
	{
		$id = 'edit_content_popup';
		$title = '<span class="view_class_title"></span>';
		
		$target = new TCv_View();
		$target->addClass('target_container');
		
		return $this->buildPopup($id,  $title, $target);
		
		
	}
	
	/**
	 * Returns the popup for styles
	 * @return TSv_PopupContainer
	 */
	public function stylesPopup()
	{
		$id = 'styles_popup';
		$title = '<i class="view_class_icon fas fa-paint-brush"></i> <span class="view_class_title">Content Styles</span>';
		
		$target = new TCv_View();
		$target->addClass('target_container');
		
		return $this->buildPopup($id,  $title, $target);
		
		
	}
	
	/**
	 * Returns the button for adding a layout button
	 * @return TCv_View
	 */
	public function addSectionButton()
	{
		$outer_container = new TCv_View('add_layout_section');
		$outer_container->addClass('TMv_ContentLayout');
		
		$container = new TCv_View();
		$container->addClass('content_width_container');
		
		$add_section_button = new TCv_Link();
		$add_section_button->setURL('#');
		$add_section_button->addClass('add_section_button');
		$add_section_button->addText('Add new section');
		$container->attachView($add_section_button);
		
		$outer_container->attachView($container);
		return $outer_container;
	}
	
	/**
	 * Changes the text shown when toggling the preview
	 * @param string $show_preview
	 * @param string $hide_preview
	 * @return void
	 */
	public function setPreviewButtonText(string $show_preview, string $hide_preview) : void
	{
		$this->show_preview_button_text = $show_preview;
		$this->hide_preview_button_text = $hide_preview;
	}
	/**
	 * The preview toggle button
	 * @return TCv_ToggleTargetLink
	 */
	public function previewToggleButton() : TCv_ToggleTargetLink
	{
		$button = new TCv_ToggleTargetLink('preview_toggle_button');
		//$button->setIconClassName('fa-eye');
		$button->addText('<span class="preview_off">'.$this->show_preview_button_text
		                 .'<i class="fas fa-toggle-off"></i></span>');
		$button->addText('<span class="preview_on">'.$this->hide_preview_button_text
		                 .'<i class="fas fa-toggle-on"></i></span>');
		$button->addClassToggle('.TMv_PageBuilder_ThemeWrapper','preview_mode');
		$button->addClassToggle('.TMv_PageBuilder_ThemeWrapper','editor_mode');
		//	$button->addClassToggle('#preview_toggle_button','editor_mode');
		
		$button->addVisibilityTarget('.TMv_PageContentHeader');
		
		return $button;
		
	}
	
	/**
	 * The preview toggle button
	 * @return TCv_ToggleTargetLink
	 */
	public function publishButton() : TCv_Link
	{
		$module = $this->renderer_item->moduleForThisClass();
		
		$button = new TCv_Link('publish_button');
		$button->addText('Publish changes');
		$button->setIconClassName('fa-check');
		$button->setURL('/admin/'.$module->folder().'/do/publish/'.$this->renderer_item->id());
		$button->useDialog('Are you sure you want to publish these changes? The existing content on this page will be replaced with what you see in this editor. ',
		                   'fa-check-circle',
		                   '#096746'); //$tungsten-green-plus-1
		$button->setDialogOKText('Yes, publish');
		return $button;
		
	}
	
	/**
	 * The button to discard unpublishing changes
	 * @return TCv_ToggleTargetLink
	 */
	public function discardChangesButton() : TCv_Link
	{
		$module = $this->renderer_item->moduleForThisClass();
		
		$button = new TCv_Link('discard_changes_button');
		$button->addText('Discard changes');
		$button->setIconClassName('fa-times');
		$button->setURL('/admin/'.$module->folder().'/do/discard-unpublished-changes/'.$this->renderer_item->id());
		$button->useDialog('Are you sure you want to discard all the changes on this page? This cannot be undone.',
		                   'fa-exclamation-triangle',
		                   '#671119'); //$tungsten-red-plus-1
		$button->setDialogOKText('Yes, discard');
		return $button;
		
	}
	
	/**
	 * Returns the bottom control bar for this page builder
	 * @return TCv_View
	 */
	public function bottomControlBar()
	{
		$control_bar = new TCv_View('bottom_control_bar');
		
		// Pages not using content management
		if($this->renderer_item instanceof TMm_PagesMenuItem
			&& !$this->renderer_item->isContentManaged())
		{
			return $control_bar;
		}
		
		// FLEX DIRECTION IS row-reverse
		// So when we hide the add section button, it's still right aligned
		
		$left_group = new TCv_View();
		$left_group->addClass('manage_group');
		$left_group->attachView($this->previewToggleButton());
		
		if(TC_getConfig('use_pages_version_publishing'))
		{
			
			$left_group->attachView($this->discardChangesButton());
			$left_group->attachView($this->publishButton());
		}
		
		
		$control_bar->attachView($left_group);
		
		$add_section_button = new TCv_Link();
		$add_section_button->setURL('#');
		$add_section_button->addClass('add_section_button');
		$add_section_button->addText('Add new section');
		$add_section_button->setIconClassName('fa-plus');
		$control_bar->attachView($add_section_button);
		
		
		return $control_bar;
	}
	
	
	/**
	 * Returns the HTML for the view
	 * @return string
	 */
	public function render()
	{
		if($this->renderer_item->isContentManaged())
		{
			$this->addClass('render_class_'.get_class($this->renderer_item));
			
			
			if($this->renderer_item->numContentItems() == 0)
			{
				$this->content_container->attachView($this->noContentView());
			}
			
			
			$this->generateViews();
			
			$this->attachGeneratedViews($this->content_container);
			$this->attachView($this->content_container);
			
			// Only shows for tungsten 8
			if(!TC_getConfig('use_tungsten_9'))
			{
				$this->attachView($this->addSectionButton());
			}
			
		}
		else
		{
			$this->attachView($this->notManagedView());
		}
		
		parent::render();
	}
	
	//////////////////////////////////////////////////////
	//
	// HELP
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the help view for use within Tungsten
	 * @return TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
		
		$p = new TCv_View();
		$p->setTag('h2');
		$p->addText('This is where you manage the content for this page.');
		$help_view->attachView($p);
		
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Recommended: Use a desktop computer that allows for easy drag and drop. Editing will work on a phone, but for complex editing, a large screen is beneficial.');
		$help_view->attachView($p);
		
		
		
		$p = new TCv_View();
		$p->setTag('h3');
		$p->addText('Layout sections');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Content on pages are organized into Layout Sections, which can then have columns in them. Your content then goes in one of those columns. ');
		$help_view->attachView($p);
		
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Add a Layout Section</strong>: Click on the ');
		$link = new TSv_HelpLink('Add New Layout Row', '.add_section_button');
		$p->attachView($link);
		$p->addText(' button and select from the list of options available.');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Layout Settings</strong>: Change the settings for a layout section using the ');
		
		
		$p->addText('gear  button. When expanded, it will provide options to ');
		
		$link = new TSv_HelpLink('hide', '.section_control_panel .content_visibility_button');
		$p->attachView($link);
		
		$p->addText(', ');
		
		$link = new TSv_HelpLink('edit', '.section_control_panel .content_edit_button');
		$p->attachView($link);
		
		$p->addText(', ');
		
		$link = new TSv_HelpLink('delete', '.section_control_panel .content_delete_button');
		$p->attachView($link);
		
		$p->addText(', or ');
		
		$link = new TSv_HelpLink('rearrange', '.section_control_panel .content_arrange_layout_button');
		$p->attachView($link);
		
		$p->addText(' a layout section. Moving or deleting a content layout will do the same to any content inside of it.  ');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('h3');
		$p->addText('Managing Content');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Add Content to a Layout</strong>: Click on the ');
		
		$link = new TSv_HelpLink('', '.add_content_button');
		$link->addClass('fa-plus');
		$p->attachView($link);
		$p->addText(' button to add content to a layout column. Select from the list of available content options, configure any settings if necessary and click on the Create Content button to finish.');
		$help_view->attachView($p);
		
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Rearrange Content</strong>: Drag and Drop using the ');
		$link = new TSv_HelpLink('', '.content_arrange_content_button');
		$link->addClass('fa-arrows');
		$p->attachView($link);
		$p->addText(' button to move content to any other column or in a different order in a single column..');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Edit Content</strong>: Click on the ');
		$link = new TSv_HelpLink('', '.content_edit_button');
		$link->addClass('fa-pencil');
		$p->attachView($link);
		$p->addText(' button.');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('<strong>Hide/Delete Content</strong>: Hide content using the ');
		$link = new TSv_HelpLink('', '.content_visibility_button');
		$link->addClass('fa-eye');
		$p->attachView($link);
		$p->addText(' button, or permanently delete content with the ');
		$link = new TSv_HelpLink('', '.content_delete_button');
		$link->addClass('fa-trash-alt');
		$p->attachView($link);
		$p->addText(' button.');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('Depending on your website, the list of available content will contain options that are specific to your needs. Speak to your developer if you are uncertain about what type of content view should be used.');
		$help_view->attachView($p);
		
		
		$p = new TCv_View();
		$p->setTag('h3');
		$p->addText('CSS Classes');
		$help_view->attachView($p);
		
		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('All content items have the option to have CSS classes assigned to them. This is dependant on your design and are most likely defined by the developer of the website. Once a style is in the system, the form provides shortcut links below the form to avoid typos.');
		$help_view->attachView($p);
		
		return $help_view;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PUBLISHING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Editing function that processes adding a new section to the page builder
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int $renderer_id The id of the specific renderer such as page with ID 17
	 * @param class-string<TCv_View> $layout_class_name The name of the layout class that is being added
	 * @return array
	 */
	public static function addSection(string $renderer_class_name,
	                                  int|string $renderer_id,
	                                  string $layout_class_name = 'TMv_ContentLayout_Section') : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			
			$render_item_view = $renderer_item->addContentLayoutWithClassName($layout_class_name);
			
			$page_builder = static::init($renderer_item);
			$page_builder->processLayoutContentModel($render_item_view);
			$view = $page_builder->layoutViewWithID($render_item_view->id());
			
			return [
				'content_id' => $render_item_view->id(),
				'html' => $view->html()
			];
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Editing function that processes adding a new section to the page builder
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int $renderer_id The id of the specific renderer such as page with ID 17
	 * @param int $section_id The ID of the section being added to
	 * @return array
	 */
	public static function addRow(string $renderer_class_name,
	                              int|string $renderer_id,
	                              int $section_id ) : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			$new_row = $renderer_item->addRowToSectionWithID($section_id);
			
			if($new_row)
			{
				$page_builder = static::init($renderer_item);
				$new_row_view = $page_builder->viewForContentModel($new_row);
				
				return [
					'content_id' => $new_row->container()->id(),
					'html' => $new_row_view->html()
				];
				
			}
			
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Editing function that processes adding a new column to the page builder
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int $renderer_id The id of the specific renderer such as page with ID 17
	 * @param int $row_id The ID of the row being added to
	 * @return array
	 */
	public static function addColumn(string $renderer_class_name,
	                                 int|string $renderer_id,
	                                 int $row_id ) : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			$new_column = $renderer_item->addColumnToRowWithID($row_id);
			
			if($new_column)
			{
				// Get a new one, with no caching
				$content_class_name = $renderer_item->renderItemClassName();
				$row = new $content_class_name($row_id);
				
				/** @var TMv_PageBuilder $page_builder */
				$page_builder = static::init($renderer_item);
				$row_view = $page_builder->viewForContentModel($row);
				
				
				return [
					'row_id' => $row->id(),
					'html' => $row_view->html()
				];
				
			}
			
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Deleting of content on a page
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int|string $renderer_id The id of the specific renderer such as page with ID 17
	 * @param int $content_id The ID of the content being deleted
	 * @return array
	 */
	public static function deleteContent(string $renderer_class_name,
	                                     int|string $renderer_id,
	                                     int $content_id ) : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			$content_model = $renderer_item->contentItemWithID($content_id);
			
			
			// Delete it
			$content_model->delete();
			
			// remove any stored values, just in case
			// Allowing it to fully re-render
			$content_class_name = $renderer_item->renderItemClassName();
			$content_class_name::clearAllModelsFromMemory();
			
			// No change, by default
			$values = [];
			
			
			if($content_model->isLayoutBlock())
			{
				// Get the row
				$parent_model_id = $content_model->container()->id();
				$row = new $content_class_name($parent_model_id);
				
				
				/** @var TMv_PageBuilder $page_builder */
				$page_builder = static::init($renderer_item);
				$row_view = $page_builder->viewForContentModel($row);
				
				
				return [
					'replacement_content_id' => $row->id(),
					'html' => $row_view->html()
				];
				
			}
			else // must return something to ensure json response
			{
				return ['success' => 'No issue'];
			}
			
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Toggles the CSS class for a content item on a page
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int $renderer_id The id of the specific renderer such as page with ID 17
	 * @param int $content_id The ID of the content that is being edited
	 * @param string $css_class The name of the CSS class being toggled
	 * @return array
	 */
	public static function toggleCSSClassForContent(string $renderer_class_name,
	                                                int|string $renderer_id,
	                                                int $content_id,
	                                                string $css_class) : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			$content_model = $renderer_item->contentItemWithID($content_id);
			
			$content_model->toggleCSSClass($css_class);
			
			
			// GENERATE RESPONSE
			// All the css styles to be applied
			$data = [];
			$data['num_styles'] = count($content_model->cssPageViewStyles());
			$data['styles'] = [];
			foreach($content_model->cssPageViewStyles() as $page_view_style)
			{
				$data['styles'][] = [
					'id' => $page_view_style->id(),
					'title' => $page_view_style->title(),
					'description' => $page_view_style->description(),
					'class' => $page_view_style->className(),
				];
			}
			
			return $data;
			
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Toggles the visibility for a content item
	 * @param class-string<TMt_PageRenderer> $renderer_class_name The name of the renderer for this page builder. This is the main
	 * model that is responsible for the content such as a page, news post, etc
	 * @param int $renderer_id The id of the specific renderer such as page with ID 17
	 * @param int $content_id The ID of the content that is being edited
	 * @return array
	 */
	public static function toggleContentVisibility(string $renderer_class_name,
	                                               int|string $renderer_id,
	                                               int $content_id) : array
	{
		// Enable publishing mode
		static::enablePublishingMode($renderer_class_name);
		
		/** @var TMt_PageRenderer|TCm_Model $renderer_item */
		$renderer_item = $renderer_class_name::init($renderer_id);
		$renderer_item->markAsUnpublished();
		
		if($renderer_item && $renderer_item->userCanEdit())
		{
			$content_model = $renderer_item->contentItemWithID($content_id);
			
			$content_model->toggleVisibility();
			
			return ['visibility' => $content_model->isVisible() ? true : false];
			
			
		}
		
		return [
			'error' => 'Item not found or permission denied'
		];
	}
	
	/**
	 * Enable the publishing mode, which changes the tables for the classes that are used, avoiding updates to
	 * the database. This must be called for any interaction which is modifying or editing page content.
	 * @param TMt_PageRenderer|TCm_Model|string $page_renderer The renderer that is being worked on. This can be the
	 * model for the item itself or the class name as a string
	 */
	public static function enablePublishingMode($page_renderer) : void
	{
		if(TC_getConfig('use_pages_version_publishing'))
		{
			//	TC_addToConsole('enablePublishingMode');
			
			// Get the class name, which allows the passing of an object for convenience
			$page_renderer_class_name = $page_renderer;
			if($page_renderer instanceof TCm_Model)
			{
				$page_renderer_class_name = get_class($page_renderer);
			}
			
			// only bother if this class uses publishing
			if($page_renderer_class_name::modelUsesPublishing())
			{
				// Update the Renderer table name
				$page_render_item_class_name = $page_renderer_class_name::pageRenderItemClassName();
				$page_render_item_class_name::switchToMirrorTable();
				
				// Update the variables table name
				/** @var class-string<TMt_PageRenderItem> $page_render_item_class_name */
				$variable_class_name = $page_render_item_class_name::variablesModelClass();
				$variable_class_name::switchToMirrorTable();
			}
		}
		
	}
}
