<?php
require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

// Enable publishing mode
TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

/** @var TMt_PageRenderer $renderer_item */
$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
$renderer_item->markAsUnpublished();

foreach($_POST as $order => $value)
{
	if(!is_int($order))
	{
		unset($_POST[$order]);
	}
}


$response = [];

$renderer_item->updateLayoutPositionsWithArray($_POST);

echo json_encode($response);

