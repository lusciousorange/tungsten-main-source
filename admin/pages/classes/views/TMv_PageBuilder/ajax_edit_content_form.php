<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

	/** @var TMt_PageRenderItem|TCm_Model $content_item */
	$content_item = ($_POST['content_model_class_name'])::init($_POST['content_id']);
	
	/** @var string|TCv_View|TMt_PagesContentView $view_class */
	$view_class = $content_item->viewClass();
	
	/** @var TMt_PageRenderer $renderer_item */
	$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
	
	// This script runs separately and it should really set an active model for consistency
	TC_saveActiveModel($renderer_item);
	TC_saveActiveModel($content_item);
	
	
	// Reset Tungsten
	$tungsten->clearCSS();
	$tungsten->clearJS();
	$tungsten->clear();

	// Create Form
	$form_name = $renderer_item->pageRenderItem_ContentFormClassName();
	/** @var TMv_PageRendererContentForm $form */
	$form = new $form_name($content_item);
	$form->setRendererItem($renderer_item);
	$tungsten->attachView($form);
	
	$icon_css_class = implode(' ', $tungsten->processClassName($view_class::pageContent_IconCode()));
	
	// Save Values
	$values = array(
		'content_id' => $content_item->id(),
		'html' => $tungsten->bodyContent(),
		'css' => $tungsten->headCSSTags(),
		'js' => $tungsten->headJSTags(false),
		'view_class' => $view_class,
		'view_icon' => $icon_css_class,
		'view_title' => $view_class::pageContent_ViewTitle(),
	
	);
	echo json_encode($values);

?>