<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);
	
	/** @var TMt_PageRenderer $renderer_item */
	$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
	$renderer_item->markAsUnpublished();

	/** @var TMt_PageRenderItem|TCm_Model $column */
	$column = ($_POST['content_model_class_name'])::init($_POST['column_id']);
	
	// STEP 1 : Remove that column from the current row
	// Column still exists, just no longer associated with that row
	$original_row = $column->container();
	$original_row->removeColumnFromRow($column);

	// STEP 2 : Move column into the new row
	$column->updateWithValues(array('container_id' => $_POST['new_row_id']));

	// STEP 3 : Update the row positions with provided values
	$num_columns = 0;
	foreach($_POST as $content_id => $order)
	{
		if(is_int($content_id))
		{
			/** @var TMt_PageRenderItem $content_item */
			$content_item = ($_POST['content_model_class_name'])::init($content_id);
			$content_item->updateColumnPosition($order);
			$num_columns++;
		}
		
	}
	
	// STEP 4 : Correct the number of rows on the new row to have the correct amount
	$new_row = ($_POST['content_model_class_name'])::init($_POST['new_row_id']);
	$new_row->updateWithValues(array('view_class' => 'TMv_ContentLayout_'.$num_columns.'Column'));

	$response = [];
	
	echo json_encode($response);
	