class TMv_PageBuilder {
	delegate_click_handlers = {};
	selected_content_container_id = null;
	options = {
		renderer_item_id : 0,
		renderer_item_class_name : '',
		selected_container_id : 0,
		renderer_view_name : '',
		content_model_class_name : '',

	}

	constructor(element, params) {

		this.element = element;
		Object.assign(this.options, params);


		// listen for the add section button, outside of the view
		document.querySelector('.add_section_button').addEventListener('click', (e) => this.show_addSectionPopup(e));

		this.element.addEventListener('click', (e) => this.delegateClickHandler(e));

		// Add delegate click handlers for all the actions that happen via clicks
		this.addDelegateClickHandler(".content_add_column_button", (e) => this.addColumn(e));
		this.addDelegateClickHandler(".content_add_row_button", (e) => this.addRow(e));
		this.addDelegateClickHandler(".content_row_move_up_button", (e) => this.moveRowUp(e));
		this.addDelegateClickHandler(".content_row_move_down_button", (e) => this.moveRowDown(e));
		this.addDelegateClickHandler(".content_section_move_up_button", (e) => this.moveSectionUp(e));
		this.addDelegateClickHandler(".content_section_move_down_button", (e) => this.moveSectionDown(e));
		this.addDelegateClickHandler(".content_visible_button", (e) => this.toggleContentVisibility(e));
		this.addDelegateClickHandler(".content_delete_button", (e) => this.show_DeleteContentConfirmation(e));
		this.addDelegateClickHandler(".TCv_HTMLTableCell", (e) => this.show_EditTableCellPopup(e));
		this.addDelegateClickHandler(".content_rearrange_button", (e) => e.preventDefault()); // do nothing

		// STYLES
		this.addDelegateClickHandler(".content_styles_button", (e) => this.show_stylesPopup(e));

		let styles_popup = document.querySelector("#styles_popup");
		if(styles_popup)
		{
			document.querySelector("#styles_popup").addEventListener('click', (e) => this.toggleStyle(e));
		}



		// Edit/settings buttons
		this.addDelegateClickHandler(".content_edit_button", (e) => this.show_editContentPopup(e));

		// Add content buttons
		this.addDelegateClickHandler(".add_content_button", (e) => this.show_addContentPopup(e));

		// Control button hovers
		this.element.addEventListener( "mouseover", (e) => this.controlButtonMouseover(e));
		this.element.addEventListener( "mouseout", (e) => this.controlButtonMouseout(e));


		// Popup container – add section button
		document.querySelectorAll(".select_layout_button").forEach(el => {
				el.addEventListener('click', this.addSection.bind(this));
			});

		// Popup container – add content button
		document.querySelectorAll(".select_content_option_button").forEach(el => {
			el.addEventListener('click', this.show_addContentForm.bind(this));
		});


		// Popup container cancel listeners
		document.querySelectorAll(".TSv_PopupContainer .cancel_popup").forEach(el => {
			el.addEventListener('click',this.cancelPopup.bind(this));
		});


		this.cleanup();

	}

	/**
	 * The single callback that handles all delegate click handlers on the page builder
	 * @param event
	 */
	delegateClickHandler(event) {

		for (const selector in this.delegate_click_handlers)
		{
			let closest = event.target.closest(selector);
			if(closest)
			{
				event.preventDefault(); // Prevent the default on all of these
				let callback = this.delegate_click_handlers[selector];
				callback(event);

			}

		}

	}


	/**
	 * Adds a delegate click handler which will run on the main page builder and handle elements that don't exist yet.
	 * @param {string} selector
	 * @param {function} callback
	 */
	addDelegateClickHandler(selector, callback) {
		this.delegate_click_handlers[selector] = callback;
	}

	/**
	 * Returns the element with a given content ID. This uses the data-content-id attribute instead of the actual IDs, since the
	 * IDs might be changed for styling purposes
	 * @param content_id
	 * @returns {HTMLElement}
	 */
	contentItem(content_id) {
		return this.element.querySelector('[data-content-id="'+ content_id+'"]');
	}

	//////////////////////////////////////////////////////
	//
	// SECTIONS
	//
	//////////////////////////////////////////////////////


	/**
	 * Shows the "add section" popup when requested. If there's only one section type, it will just add a default section.
	 * @param event
	 */
	show_addSectionPopup(event) {
		event.preventDefault();

		let popup = document.querySelector('#add_section_popup');

		// // Skip step if there is only one layout
		let num_section_styles = popup.querySelectorAll('.button_grid_button').length;

		if(num_section_styles === 1)
		{
			this.addSection('TMv_ContentLayout_Section'); // Only one means it's just automatic
		}
		else // more than one
		{
			popup.show();
			loopFocus(popup);
		}

	}

	/**
	 * Adds a section to this page
	 * @param event The event object OR a string name for the class to add
	 */
	async addSection(event) {

		// We were passed a class name instead, just load that
		let class_name;
		if(typeof event == "string")
		{
			class_name = event;
		}
		else
		{
			event.preventDefault();
			class_name = event.currentTarget.getAttribute('data-class-name');
			this.cancelPopup(event);

		}

		let request_url = '/admin/pages/do/add-section/'
			+ this.options.renderer_item_class_name+'/'
			+ this.options.renderer_item_id+'/'
			+ class_name
			+ '?return_type=json'
		;

		// Send the async request
		const response = await this.sendRequest('GET', request_url,{});

		// Add the html to the bottom of the container
		this.element.querySelector('#page_builder_content_container').insertAdjacentHTML('beforeEnd', response.html);

		// Hide the no-content view
		let no_view = this.element.querySelector('#no_content_view');
		if(no_view)
		{
			no_view.hide();
		}

		// Scroll the page builder, which is now the parent that has overflow, so we need that
		this.element.parentElement.scrollTop = this.element.parentElement.scrollHeight;

		this.cleanup();

	}

	/**
	 * Moves a row up
	 * @param event
	 */
	async moveSectionUp(event) {

		let section = event.target.closest('section');
		if(section.previousElementSibling)
		{
			// Animate the change
			await this.animateItemSwap(section, 'previous');

			await this.updateRearrangedSections(section);
		}
		else
		{
			console.error('ERROR: Attempting to move TOP section')
		}

	}

	/**
	 * Moves a row down
	 * @param event
	 */
	async moveSectionDown(event) {

		let section = event.target.closest('section');
		if(section.nextElementSibling)
		{
			// Animate the change
			await this.animateItemSwap(section, 'next');

			await this.updateRearrangedSections(section);

		}
		else
		{
			console.error('ERROR: Attempting to move BOTTOM section')
		}

	}

	/**
	 * Updates when sections are rearranged
	 * @param section
	 * @returns {Promise<void>}
	 */
	async updateRearrangedSections(section) {

		let send_values = {
			"renderer_item_id" : this.options.renderer_item_id,
			"renderer_item_class_name" : this.options.renderer_item_class_name,

		};

		let all_sections = this.element.querySelectorAll('section.TMv_ContentLayout_Section.pages_layout');

		// Loop through the section children, finding the content layouts and adding them to the request
		let display_order = 0;

		for(const section_item of all_sections) {
			display_order++;
			send_values[section_item.getAttribute('data-content-id')] = display_order;
		}
		send_values['num_items'] = display_order;

		await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_rearrange_layouts.php", send_values);

		this.cleanup();
	}


	//////////////////////////////////////////////////////
	//
	// ROWS
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds a row to the particular section
	 * @param event The event object
	 */
	async addRow(event) {

		let section = event.target.closest('section');
		section.classList.add('tungsten_loading');

		let section_id = section.getAttribute('data-content-id');


		let request_url = '/admin/pages/do/add-row/'
			+ this.options.renderer_item_class_name+'/'
			+ this.options.renderer_item_id+'/'
			+ section_id
			+'?return_type=json'
		;

		// Send the async request
		const response = await this.sendRequest('GET', request_url,{});

		section.querySelector('.section_control_panel').insertAdjacentHTML('beforebegin',response.html);

		this.cleanup();
		section.classList.remove('tungsten_loading');
	}

	/**
	 * Moves a row up
	 * @param event
	 */
	async moveRowUp(event) {
		let row = event.target.closest('.row');
		if(row.previousElementSibling)
		{
			// Animate the change
			await this.animateItemSwap(row, 'previous');
			await this.updateRearrangedRows(row);
		}
		else
		{
			console.error('ERROR: Attempting to move TOP row')
		}

	}

	/**
	 * Moves a row down
	 * @param event
	 */
	async moveRowDown(event) {
		let row = event.target.closest('.row');
		if(row.nextElementSibling)
		{
			// row.classList.add('tungsten_loading');
			// row.nextElementSibling.classList.add('tungsten_loading');

			// Animate the change
			await this.animateItemSwap(row, 'next');

			await this.updateRearrangedRows(row);

		}
		else
		{
			console.error('ERROR: Attempting to move BOTTOM row')
		}

	}

	/**
	 * Updates when rows are rearranged
	 * @param row
	 * @returns {Promise<void>}
	 */
	async updateRearrangedRows(row) {

		// Prep the request
		let section = row.closest('section');

		let send_values = {
			"renderer_item_id" : this.options.renderer_item_id,
			"renderer_item_class_name" : this.options.renderer_item_class_name,
			"container_id" : section.getAttribute('data-content-id'),

		};

		// Loop through the section children, finding the content layouts and adding them to the request
		let display_order = 0;

		for(const child_row of section.children) {

			if(child_row.classList.contains('TMv_ContentLayout'))
			{
				display_order++;
				send_values[child_row.getAttribute('data-content-id')] = display_order;
			}

		}
		send_values['num_items'] = display_order;

		await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_rearrange_rows.php", send_values);

		this.cleanup();
	}



	//////////////////////////////////////////////////////
	//
	// COLUMNS
	//
	//////////////////////////////////////////////////////

	/**
	 * The event handler for adding a column
	 * @param event
	 */
	async addColumn(event) {
		let row = event.target.closest('.row');
		row.classList.add('tungsten_loading');

		let row_id = row.getAttribute('data-content-id');

		let request_url = '/admin/pages/do/add-column/'
			+ this.options.renderer_item_class_name+'/'
			+ this.options.renderer_item_id+'/'
			+ row_id
			+'?return_type=json'
		;


		// Send the async request
		const response = await this.sendRequest('GET', request_url,{});

		let dom_elements = convertHTMLToDOMElements(response.html);
		if(dom_elements.length === 1)
		{
			// REPLACE THE ENTIRE ROW
			let new_row = dom_elements[0];
			row.replaceWith(new_row);
		}
		this.cleanup();

	}



	//////////////////////////////////////////////////////
	//
	// CONTENT
	//
	//////////////////////////////////////////////////////

	/**
	 * Shows the "add content" popup which shows all the possible content items that can be added
	 * @param {Event} event
	 */
	async show_addContentPopup(event) {
		event.preventDefault();

		let button = event.target.closest('.add_content_button');
		let content_id = button.getAttribute('data-content-id');

		// Save the ID for the next call
		this.selected_content_container_id = content_id;

		this.contentItem(content_id).classList.add('tungsten_loading');

		let popup = document.querySelector('#add_content_popup');
		popup.show();
		loopFocus(popup);
	}

	/**
	 * Adds a section to this page
	 * @param event The event object OR a string name for the class to add
	 */
	async show_addContentForm(event) {
		event.preventDefault();

		let class_name = event.currentTarget.getAttribute('data-class-name');

		let send_values = {
			"view_class" : class_name,
			"renderer_item_id" : this.options.renderer_item_id,
			"renderer_item_class_name" : this.options.renderer_item_class_name,
			"container_id" : this.selected_content_container_id,
			'content_model_class_name' : this.options.content_model_class_name

		};

		const response = await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_create_content_form.php", send_values, false);

		this.showEditContentPopupWithResponse(response);
		this.cleanup();

	}

	/**
	 * The edit content popup is used in various scenarios including when content is created, edited or when table values are
	 * updated as well.
	 * @param {Object} response The response coming from sendRequest()
	 */
	showEditContentPopupWithResponse(response) {

		// Find the popup
		let popup = document.querySelector('#edit_content_popup');

		let target_container = popup.querySelector('.target_container');
		target_container.className = '';
		target_container.classList.add('target_container');
		target_container.classList.add(response.view_class+'_content_form');
		target_container.innerHTML = response.html;

		this.processResponseForCSSandJS(response);

		// Set the title
		popup.querySelector('.view_class_title').textContent = response.view_title;
		popup.show();
		loopFocus(popup);

	}

	//////////////////////////////////////////////////////
	//
	// DELETION and VISIBILITY
	//
	//////////////////////////////////////////////////////

	/**
	 * Shows the "delete content" confirmation button.
	 * @param event
	 */
	show_DeleteContentConfirmation(event) {
		event.preventDefault();

		let button = event.target.closest('.content_delete_button');
		let content_id = button.getAttribute('data-content-id');

		/** @var {TCv_Dialog} dialog */
		let dialog = window.W.TCv_Dialog;
		dialog.setMessage('Are you sure you want to delete this item and any content inside of it?');
		dialog.setCancelText('Cancel');
		dialog.setConfirmText('Confirm');
		dialog.setColor('rgb(221, 0, 0)');
		dialog.setType('fa-trash-alt');
		dialog.setConfirmAction('#'); // stop the button from doing anything

		dialog.setRightButtonCallback((e) => {this.deleteContentItem(e, content_id)});

		dialog.show();

	}

	/**
	 *
	 * @param {Event} event
	 * @param {int} content_id
	 * @returns {Promise<void>}
	 */
	async deleteContentItem(event, content_id) {
		event.preventDefault();

		this.contentItem(content_id).classList.add('tungsten_loading');

		let request_url = '/admin/pages/do/delete-content/'
			+ this.options.renderer_item_class_name+'/'
			+ this.options.renderer_item_id+'/'
			+ content_id
			+'?return_type=json'
		;

		// Send the async request
		const response = await this.sendRequest('GET', request_url,{});

		// We're being given a specific ID to replace with new HTML
		if(response.replacement_content_id != null)
		{
			this.contentItem(response.replacement_content_id).outerHTML = response.html;

		}
		else // just delete the thing
		{
			// Deal with content actually deleting the container element
			let element_to_delete = this.contentItem(content_id);
			if(element_to_delete)
			{
				// Page content items, we actually want the parent container
				if(element_to_delete.classList.contains('page_content_item') && !element_to_delete.classList.contains('row'))
				{
					// Find the content container, which on dashboards could be several levels up
					element_to_delete = element_to_delete.closest('.content_container');
				}
				element_to_delete.remove();
			}

		}

		this.cleanup();

	}

	/**
	 * Toggles the visibility on a given content item
	 * @param event
	 * @returns {Promise<void>}
	 */
	async toggleContentVisibility(event) {
		event.preventDefault();

		let button = event.target.closest('.content_visible_button');
		let content_id = button.getAttribute('data-content-id');



		let request_url = '/admin/pages/do/toggle-content-visibility/'
			+ this.options.renderer_item_class_name+'/'
			+ this.options.renderer_item_id+'/'
			+ content_id
			+'?return_type=json'
		;

		// Send the async request
		const response = await this.sendRequest('GET', request_url,{});

		let content_element = this.contentItem(content_id);

		if(response.visibility === false)
		{
			content_element.classList.add('invisible');
			content_element.classList.remove('visible');
			button.querySelector('use').setAttribute('xlink:href', '#fa-eye-slash');

		}
		else
		{
			content_element.classList.remove('invisible');
			content_element.classList.add('visible');
			button.querySelector('use').setAttribute('xlink:href', '#fa-eye');

		}
		this.cleanup();

	}

	//////////////////////////////////////////////////////
	//
	// STYLES
	//
	//////////////////////////////////////////////////////
	async show_stylesPopup(event) {
		event.preventDefault();

		let button = event.target.closest('.content_styles_button');
		let content_id = button.getAttribute('data-content-id');

		this.contentItem(content_id).classList.add('tungsten_loading');

		let send_values = {
			"content_id" : content_id,
			'content_model_class_name' : this.options.content_model_class_name,
			"renderer_item_class_name" : this.options.renderer_item_class_name,

		};

		const response = await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_styles_editor.php", send_values, false);

		// Show the form for this content item

		let popup = document.querySelector('#styles_popup');

		let target_container = popup.querySelector('.target_container');
		target_container.innerHTML = response.html;


		popup.show();
		loopFocus(popup);

		this.cleanup();



	}

	async toggleStyle(event) {
		event.preventDefault();

		let button = event.target.closest('.style_toggle_button');
		if(button)
		{


			let content_id = button.getAttribute('data-content-id');
			let class_name = button.getAttribute('data-class-name');

			let element = this.contentItem(content_id);

			let request_url = '/admin/pages/do/toggle-content-css-class/'
				+ this.options.renderer_item_class_name+'/'
				+ this.options.renderer_item_id+'/'
				+ content_id+'/'
				+ class_name
				+'?return_type=json'
			;

			// Send the async request
			const response = await this.sendRequest('GET', request_url,{});

			button.classList.toggle('selected');

			element.classList.toggle(class_name);

			// Update style count
			if(response.num_styles === 0)
			{
				response.num_styles = '';
			}
			this.element.querySelector('#control_panel_' + content_id + ' .num_styles').textContent = response.num_styles;

			// Update listed styles
			let applied_styles = this.element.querySelector('#control_panel_' + content_id + ' .applied_styles');
			applied_styles.innerHTML = '';

			for (const style_data of response.styles)
			{
				let div = document.createElement('div');
				div.textContent = style_data.title;
				applied_styles.append(div);
			}

		}
	}


	//////////////////////////////////////////////////////
	//
	// CONTENT REARRANGING
	//
	//////////////////////////////////////////////////////
	/**
	 * Configures all the sortable content rearranging. This method
	 */
	configureContentRearranging() {

		// Loop through all the content blocks (columns) on the page
		this.element.querySelectorAll('.TMv_ContentLayoutBlock.content_column').forEach(el => {
			this.configureColumnForContentRearranging(el);
		});
	}

	/**
	 * Adds sortable for a single column. This checks to see if the value is already set, since each sortable is saved with
	 * the corresponding DOM element.
	 * @param column
	 */
	configureColumnForContentRearranging(column) {
		// Ensure we have a column, but also ensure we haven't done this before
		if(column.classList.contains('content_column') && column.content_sortable === undefined)
		{

			// Save the sortable to the actual column
			column.content_sortable = new Sortable(column.querySelector('.layout_block_container'), {
				handle 			: '.content_rearrange_button',
				group			: 'content_group',
				ghostClass		: 'content_ghost',
				//animation		: 150, // no animation. Makes it feel gittery
				//fallbackOnBody	: true, // appends the element to the body, nope
				//swapThreshold	: 0.65,
				onChange		: this.contentRearrangeChanged.bind(this),
				onEnd 			: this.contentRearrangeDone.bind(this),
			});
		}
	}

	/**
	 * Event handler each time content is moved
	 * @param {Event} event
	 */
	contentRearrangeChanged(event) {
		this.updateEmptyContainers();
	}

	/**
	 * Event handler for when the rearranging is done and the values should be saved
	 * @param {Event} event
	 */
	async contentRearrangeDone(event) {
		let content_item = event.item;
		let layout_block = content_item.closest('.TMv_ContentLayoutBlock');

		let send_values = {
			renderer_item_id : this.options.renderer_item_id,
			layout_block_id : layout_block.getAttribute('data-content-id'),
			renderer_item_class_name : this.options.renderer_item_class_name,
			content_model_class_name : this.options.content_model_class_name,
		};

		let display_order = 0;
		let content_children = layout_block.querySelectorAll('.page_content_item');
		send_values.element_orders = [];
		content_children.forEach(el => {
			display_order++;
			send_values.element_orders[display_order] = el.getAttribute('data-content-id');

		});

		const response = await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_rearrange_content.php", send_values);

		// clear any rearranged styling. Lingers after moving the mouse
		let rearrange_style = this.element.querySelector('.button_hover_rearrange');
		if(rearrange_style)
		{
			rearrange_style.classList.remove('button_hover_rearrange');
		}

		this.cleanup();
	}

	//////////////////////////////////////////////////////
	//
	// COLUMN REARRANGING
	//
	//////////////////////////////////////////////////////
	/**
	 * Configures all the sortable column rearranging.
	 */
	configureColumnRearranging() {
		// Loop through all the content blocks (columns) on the page
		this.element.querySelectorAll('.TMv_ContentLayout.row').forEach(el => {
			this.configureRowForColumnRearranging(el);
		});
	}

	/**
	 * Adds sortable for a single row. This checks to see if the value is already set, since each sortable is saved with
	 * the corresponding DOM element.
	 * @param row
	 */
	configureRowForColumnRearranging(row) {

		// Not at max, if we don't have one set, do it it now
		if(row.classList.contains('row') && row.column_sortable === undefined)
		{
			// Save the sortable to the actual column
			row.column_sortable = new Sortable(row.querySelector('.cwc'), {
				handle 			: '.content_column_rearrange_button',
				group			: 'column_group',
				ghostClass		: 'column_ghost',
				//animation		: 150, // no animation. Makes it feel gittery
				//fallbackOnBody	: true, // appends the element to the body, nope
				//swapThreshold	: 0.65,
				onMove			: this.columnMoved.bind(this),
				onEnd 			: this.columnRearrangeDone.bind(this),
			});
		}
	}

	/**
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	columnMoved(event) {
		let from_row = event.from.closest('.row');
		let to_row = event.to.closest('.row');
		// cancel the event if we're moving to a DIFFERENT row and that row is maxed out
		if(from_row !== to_row && to_row.classList.contains('max_columns'))
		{
			return false;
		}
	}


	/**
	 * Event handler for when the rearranging is done and the values should be saved
	 * @param {Event} event
	 */
	async columnRearrangeDone(event) {
		let column = event.item;
		let new_row = column.closest('.row');
		let original_row = event.from.closest('.row');

		let send_values = {
			renderer_item_id 			: this.options.renderer_item_id,
			renderer_item_class_name 	: this.options.renderer_item_class_name,
			content_model_class_name 	: this.options.content_model_class_name,
			column_id 					: column.getAttribute('data-content-id'),
			original_row_id 			: original_row.getAttribute('data-content-id'),
			new_row_id 					: new_row.getAttribute('data-content-id'),

		};

		// ORIGINAL ROW
		// Re-assign the column names on the original
		let display_order = 0;
		original_row.querySelectorAll('.TMv_ContentLayoutBlock').forEach(el => {
			display_order++;
			el.classList.remove('column_1','column_2','column_3','column_4','column_5');
			el.classList.add('column_' + display_order);

		});

		// Update the layout name, removing the old, adding the new
		original_row.className = original_row.className.replace(/(^|\s)TMv_ContentLayout_\S+/g, '');
		original_row.classList.add('TMv_ContentLayout_' + display_order + 'Column');

		// NEW ROW
		// Re-assign the column names on the original
		display_order = 0;
		new_row.querySelectorAll('.TMv_ContentLayoutBlock').forEach(el => {
			display_order++;
			el.classList.remove('column_1','column_2','column_3','column_4','column_5');
			el.classList.add('column_' + display_order);

			// Additionally update the send_values
			send_values[el.getAttribute('data-content-id')] = display_order;


		});

		// Update the layout name, removing the old, adding the new
		new_row.className = new_row.className.replace(/(^|\s)TMv_ContentLayout_\S+/g, '');
		new_row.classList.add('TMv_ContentLayout_' + display_order + 'Column');


		await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_rearrange_column.php", send_values);

		// clear any rearranged styling. Lingers after moving the mouse
		let rearrange_style = this.element.querySelector('.button_hover_rearrange');
		if(rearrange_style)
		{
			rearrange_style.classList.remove('button_hover_rearrange');
		}

		 this.cleanup();
	}

	//////////////////////////////////////////////////////
	//
	// CONTROL PANEL HOVERS
	//
	//////////////////////////////////////////////////////

	/**
	 * Delegation method that handles when the mouse moves over a control button.
	 * @param {Event} event
	 */
	controlButtonMouseover(event) {
		event.preventDefault();
		let button = event.target.closest('.content_button');
		if(button)
		{
			let control_panel = button.closest('.control_panel');

			control_panel.classList.add('button_hover');
			control_panel.classList.add('button_hover_'+button.getAttribute('data-button-type'));

		}
	}

	/**
	 * Delegation method that handles when the mouse moves out a control button.
	 * @param {Event} event
	 */
	controlButtonMouseout(event) {
		event.preventDefault();
		let button = event.target.closest('.content_button');
		if(button)
		{
			let control_panel = button.closest('.control_panel');

			control_panel.classList.remove('button_hover');
			control_panel.classList.remove('button_hover_'+button.getAttribute('data-button-type'));

		}
	}

	//////////////////////////////////////////////////////
	//
	// SETTINGS
	//
	//////////////////////////////////////////////////////

	/**
	 * Shows the "edit content" popup which is true for any item on the page. Different forms show different values. however
	 * they all are rendered via the same form.
	 *
	 * This applies to all the content include sections, rows, columns, and content itself
	 * @param {Event} event
	 */
	async show_editContentPopup(event) {
		event.preventDefault();

		let button = event.target.closest('.content_edit_button');
		let content_id = button.getAttribute('data-content-id');

		// Must use data-content-id since the actual ID might be changed
		this.contentItem(content_id).classList.add('tungsten_loading');

		// addLoadingToView(parent_view);
		let send_values = {
			"content_id" : content_id,
			'content_model_class_name' : this.options.content_model_class_name,
			"renderer_item_class_name" : this.options.renderer_item_class_name,
			"renderer_item_id" : this.options.renderer_item_id,

		};

		const response = await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_edit_content_form.php", send_values, false);

		this.showEditContentPopupWithResponse(response);
		this.cleanup();
	}

	//////////////////////////////////////////////////////
	//
	// POPUPS
	//
	//////////////////////////////////////////////////////

	/**
	 * Cancels any popups on the page
	 * @param event
	 */
	cancelPopup(event) {
		event.preventDefault();
		let popup_window = event.target.closest('.TSv_PopupContainer');
		popup_window.hide();

		// Clear edit_content_popup
		if(popup_window.getAttribute('id') === 'edit_content_popup')
		{
			popup_window.querySelector('.target_container').innerHTML = '';
		}

		this.cleanup();
	}



	//////////////////////////////////////////////////////
	//
	// CLEANUP
	//
	//////////////////////////////////////////////////////


	/**
	 * Cleanup function that calls other update functions to ensure the interface is still accurate
	 */
	cleanup() {
		this.configureContentRearranging();
		this.configureColumnRearranging();
		this.updateEmptyContainers();
		this.updateControlButtonStatus();
		this.removeTungstenLoading();
	}


	/**
	 * Removes any tungsten_loading indicators from the interface
	 */
	removeTungstenLoading() {
		document.querySelectorAll('.tungsten_loading').forEach(el => { el.classList.remove('tungsten_loading');});

	}

	/**
	 * Updates the first and last statuses on each of the sections and the rows in that section
	 */
	updateControlButtonStatus() {
		let sections = this.element.querySelectorAll('section.TMv_ContentLayout_Section');
		// Loop through each section, and eventually each row in each section
		let section_count = 1;
		sections.forEach(section => {
			section.classList.remove('first_section');
			section.classList.remove('last_section');

			if(section_count === 1)
			{
				section.classList.add('first_section');
			}

			if(section_count === sections.length)
			{
				section.classList.add('last_section');
			}

			// Loop through each row in the section
			let rows = section.querySelectorAll('.row.TMv_ContentLayout');
			let row_count = 1;
			let num_rows = rows.length;


			rows.forEach(row => {
				row.classList.remove('first_row');
				row.classList.remove('last_row');
				if(row_count === 1)
				{
					row.classList.add('first_row');
				}

				// Separately check if we have the last row
				if(row_count === num_rows)
				{
					row.classList.add('last_row');

				}

				// Deal with max columns
				let columns = row.querySelectorAll('.cwc .TMv_ContentLayoutBlock.content_column');
				row.classList.remove('max_columns');
				if(columns.length >= section.getAttribute('data-max-cols'))
				{
					row.classList.add('max_columns');
				}

				row_count++;
			});
			section_count++;

		});

	}

	/**
	 * Updates empty containers, which allows for the placeholders
	 */
	updateEmptyContainers() {

		this.element.querySelectorAll('.TMv_ContentLayoutBlock.page_content_item').forEach(el => {
			let child_elements = el.querySelectorAll('.layout_block_container > .content_container');
			if(child_elements.length === 0)
			{
				el.classList.add('is_empty');
			}
			else
			{
				el.classList.remove('is_empty');
			}
		});

	}

	//////////////////////////////////////////////////////
	//
	// TABLE BUILDER
	//
	// IT needs some so much of the PageBuilder infrastructure
	// that it's just so much easier to have it here
	//
	//////////////////////////////////////////////////////

	/**
	 * Shows the "edit cell" popup which is used for a TMv_HTMLTableBuilder. This shows a form that manages the content
	 * for a single cell in those tables.
	 * @param event
	 */
	async show_EditTableCellPopup(event)
	{
		// The target is always the cell
		let cell = event.target.closest('.TCv_HTMLTableCell');

		// Find the cell, find the table
		let table = cell.closest('.TMv_HTMLTableBuilder');
		if(table)
		{
			let content_id = table.getAttribute('data-content-id');

			table.classList.add('tungsten_loading');

			event.preventDefault();

			let send_values = {
				"content_id" : content_id,
				'content_model_class_name' : this.options.content_model_class_name,
				"renderer_item_class_name" : this.options.renderer_item_class_name,
				"renderer_item_id" : this.options.renderer_item_id,

				// Need to pass along the row and column
				'row_num' : cell.getAttribute('data-r'),
				'col_num' : cell.getAttribute('data-c'),


			};

			const response = await this.sendRequest('POST', "/admin/pages/classes/views/TMv_PageBuilder/ajax_edit_cell_form.php", send_values, false);

			this.showEditContentPopupWithResponse(response);
			this.cleanup();


		}


	}



	//////////////////////////////////////////////////////
	//
	// REQUEST AND ANIMATION
	//
	//////////////////////////////////////////////////////

	/**
	 * A function that animates the rearranging of two rows/sections with the "one at a time" approach. The $item is the thing
	 * that is being moved and then it indicates up or down, which then swaps with the thing next to it
	 * @param {HTMLElement} item The item being moved
	 * @param {string} next_previous Indicates if the item is being moved to the next or previous spot
	 * @param {function} update_callback The callback function that should be run AFTER the movement is complete
	 */
	async animateItemSwap(item, next_previous) {

		// Set the values for later
		let other_item;
		let callback;

		// Move it to the next one
		if(next_previous === 'next')
		{
			other_item = item.nextElementSibling;
			callback = () => {item.parentElement.insertBefore(other_item, item)};
		}
		if(next_previous === 'previous')
		{
			other_item = item.previousElementSibling;
			callback = () => {item.parentElement.insertBefore(item, other_item)};

		}

		// If we have values, do it
		if(other_item && callback) {

			// Add the swapping classname, enables transition
			item.classList.add('swap_transition');
			other_item.classList.add('swap_transition');

			// Calculate target dimensions
			let final_y_item = other_item.getBoundingClientRect().top - item.getBoundingClientRect().top;
			let final_y_other = item.getBoundingClientRect().top - other_item.getBoundingClientRect().top;

			// Apply transformations
			item.style.transform = 'translate(0px, '+ final_y_item +'px)';
			other_item.style.transform = 'translate(0px, '+ final_y_other +'px)';

			// Pause 500 milliseconds to let the animation complete
			await new Promise(r => setTimeout(r, 500));

			// Trigger the timeout to clear it all away
			callback(); // run the action
			item.classList.remove('swap_transition');
			item.style.transform = null;
			other_item.classList.remove('swap_transition');
			other_item.style.transform = null;
		}

	}


	/**
	 * Sends a request to the server and returns a promise. Used in async methods to send requests
	 * @param {string} method
	 * @param {string} url
	 * @param {object} data
	 * @param {boolean} trigger_publishing_flag Indicates if the publishing flag should not be switched. This should be set to true
	 * on calls that perform a task but do NOT count as a publishing change. For example, loading a popup that doens't change the DB.
	 * @return {Promise<unknown>}
	 */
	sendRequest(method, url, data, trigger_publishing_flag = true) {

		method = method.toUpperCase();

		let config = {
			method : method,
			headers : {}
		};

		if(method === 'POST')
		{

			config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			config.body = new URLSearchParams(data);
		}

		return fetch(url, config)
			.then(response =>
			{
				if(!response.ok)
				{
					throw new Error(`HTTP error: ${response.status}`);
				}

				return response.json();

			}).then(response => {

				// Pass it along the chain
				return response;


			})
			.catch(exception =>
			{
				console.error(exception);
			})

			.finally(() =>
			{
				if(trigger_publishing_flag)
				{
					// mark the wrapper as having changes
					this.element.closest('.TMv_PageBuilder_ThemeWrapper').classList.add('has_unpublished');
				}
			});


	}

	/**
	 * Handles parsing the response for JS and CSS
	 * @param response
	 */
	processResponseForCSSandJS(response) {

		// UPDATE CSS
		if(typeof(response.css) === 'object' && response.css !== null)
		{
			processAsyncCSSLinks(response.css);
		}

		// UPDATE JS
		if(typeof(response.js) === 'object' && response.js !== null)
		{
			processAsyncJSLinks(response.js);
		}

	}



}