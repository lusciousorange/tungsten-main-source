<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);

	/** @var TMt_PageRenderer $renderer_item */
	$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
	$renderer_item->markAsUnpublished();

	// Explode the comma-separated string into an array
	$elements = explode(',', $_POST['element_orders']);
	
	foreach($elements as $order => $content_id)
	{
		if($order > 0 && $content_id > 0)
		{
			/** @var TMt_PageRenderItem $content_item */
			$content_item = ($_POST['content_model_class_name'])::init($content_id);
			$content_item->updateContentPosition($order, $_POST['layout_block_id']);
		}
		
	}
	
	$response = [];
	
	echo json_encode($response);
	

