<?php
require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

// Enable publishing mode
TMv_PageBuilder::enablePublishingMode($_POST['renderer_item_class_name']);


/** @var TMt_PageRenderer $renderer_item */
$renderer_item = ($_POST['renderer_item_class_name'])::init($_POST['renderer_item_id']);
$renderer_item->markAsUnpublished();

$content_class_name = $renderer_item->renderItemClassName();

/** @var TMt_PageRenderItem $content_item */
$content_item = ($content_class_name)::init($_POST['container_id']);

foreach($_POST as $order => $value)
{
	if(!is_int($order))
	{
		unset($_POST[$order]);
	}
}
$content_item->updateRowPositionsWithArray($_POST);

$response = [];

echo json_encode($response);

