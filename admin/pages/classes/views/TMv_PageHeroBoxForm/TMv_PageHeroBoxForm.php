<?php
class TMv_PageHeroBoxForm extends TCv_FormWithModel
{
	protected $menu_item;
	/**
	 * TMv_PageHeroBoxForm constructor.
	 *
	 * If a page menu item is passed, then we're creating a new item.
	 *
	 * @param TMm_PagesMenuItem|TMm_PageHeroBox $model
	 */
	public function __construct($model)
	{

		if($model instanceof TMm_PageHeroBox)
		{
			parent::__construct($model);
			$this->menu_item = $model->menuItem();
		}
		elseif($model instanceof TMm_PagesMenuItem)
		{
			parent::__construct('TMm_PageHeroBox');
			$this->menu_item = $model;
		}

		$this->addClassJSFile('TMv_PageHeroBoxForm');
		$this->addClassJSInit('TMv_PageHeroBoxForm');




	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$field->setHelpText("The main title of the hero box. Preferably only a few words");
		$this->attachView($field);
		
		$description = new TCv_FormItem_TextField('description', 'Secondary line');
		$description->setHelpText("A brief secondary line that supports the main title");
		$this->attachView($description);

		$field = new TCv_FormItem_Select('is_visible', 'Visibility');
		$field->setHelpText("Indicate if this hero box is currently visible on the website.");
		$field->addOption('1', 'Visible');
		$field->addOption('0', 'Hidden');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('background_position', 'Background position');
		$field->addHelpText('Identify where the background image is "pinned". ');
		$field->setDefaultValue('center center');
		$field->addOption('left top', 'Top left');
		$field->addOption('center top', 'Top center');
		$field->addOption('right top', 'Top right');
		$field->addOption('left center', 'Middle left');
		$field->addOption('center center', 'Centered');
		$field->addOption('right center', 'Middle right');
		$field->addOption('left bottom', 'Bottom left');
		$field->addOption('center bottom', 'Bottom center');
		$field->addOption('right bottom', 'Bottom right');
		$this->attachView($field);
		
		$field = new TCv_FormItem_FileDrop('image_filename','Background image');
		$field->setIsRequired();
		$field->setHelpText('The background image for this hero box. ');
		
		$width = TC_getModuleConfig('pages','image_crop_width');
		$height = TC_getModuleConfig('pages','hero_box_image_crop_height');
		if($height == 0)
		{
			$height = TC_getModuleConfig('pages','image_crop_height');
		}
		$field->setUseCropper([$width, $height]);
		$this->attachView($field);



		$field = new TCv_FormItem_Heading('button_heading', 'Button');
		$this->attachView($field);

		$field = new TCv_FormItem_Select('show_button', 'Show button');
		$field->setHelpText("Indicate if a button is to be shown for this box. ");
		$field->addOption('1', 'Yes - Show a button to click on');
		$field->addOption('0', 'No - There will be nothing to click on for this hero box');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('button_text', 'Button text');
		$field->setHelpText("The text shown on the button");
		$this->attachView($field);

		$this->attachView($this->linkingGroup());

		$field = new TCv_FormItem_Hidden('menu_id', '');
		$field->setValue($this->menu_item->id());
		$this->attachView($field);


	}
	

	public function linkingGroup()
	{
		$block = new TCv_FormItem_Group('linking_group');

		$field = new TCv_FormItem_Heading('linking_heading', 'Linking');
		$block->attachView($field);


		$target = new TCv_FormItem_Select('target','Target');
		$target->setHelpText('Indicate what you want the button to target when pressed.');
		$target->addOption('menu_item', 'PAGE - Link to a specific page on this website');
		$target->addOption('another_website', 'SITE – Link to a website outside of this one ');
		$block->attachView($target);


		$redirect = new TCv_FormItem_Select('redirect_to_menu','Page');
		$redirect->setHelpText('Select the menu that this will redirect to');
		$redirect->useFiltering();
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}

			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}

		$block->attachView($redirect);


		$field = new TCv_FormItem_TextField('redirect_url','Website address');
		$field->setHelpText('The website address or URL, which should start with http://.');
		$field->setDefaultValue('http://');
		$block->attachView($field);



		return $block;

	}
	
}
?>