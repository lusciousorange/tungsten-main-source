class TMv_PageHeroBoxForm extends TCv_Form {
	constructor(element, settings) {
		super(element, settings);

		// Add event listener from TCv_Form JS class
		this.addFieldEventListener('show_button', 'change', this.showButtonChanged, true);
		this.addFieldEventListener('target', 'change', this.targetChanged, true);
	}

	/**
	 * handles the target changing
	 * @param event
	 */
	targetChanged(event) {
		let value = this.fieldValue('target');
		if(value === 'menu_item')
		{
			this.showFieldRows('redirect_to_menu');
			this.hideFieldRows('redirect_url');
		}
		else if(value === 'another_website')
		{

			this.hideFieldRows('redirect_to_menu');
			this.showFieldRows('redirect_url');
		}

	}

	/**
	 * Handles the button changing
	 * @param event
	 */
	showButtonChanged(event) {
		if(this.fieldValue('show_button') === '1')
		{
			this.showFieldRows(['button_text','linking_group']);
		}
		else
		{
			this.hideFieldRows(['button_text','linking_group']);

		}
	}

}