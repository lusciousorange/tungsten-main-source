<?php
/**
 * Class TMv_HTMLEditor
 *
 * A WYSIWYG editor that is embedded on pages
 */
class TMv_HTMLEditor extends TCv_View 
{
	use TMt_PagesContentView;
	
	protected $html_content = '<p></p>';
	
	
	/**
	 * TMv_HTMLEditor constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
	}

	/**
	 * @return string
	 */
	public function html()
	{
		$text = $this->localizeProperty('html_content');
		$text = str_ireplace('<p>&nbsp;</p>','',$text);
		$text = str_ireplace('<p></p>','',$text);
		
		// Attach noreferrer to external links
		// Based on the fact that embedded links are formatted consistently
		$text = str_ireplace('<a href="http','<a rel="noreferrer" href="http',$text);
		
		
		$this->addText($text);
		return parent::html();
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$editor = new TCv_FormItem_HTMLEditor('html_content', 'HTML Editor');
		$editor->setIsRequired();
		$editor->setShowTitleColumn(false);
		$form_items[] = $editor;
		
		
		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string  { return 'Text editor'; }
	public static function pageContent_IconCode() : string  { return 'fa-code'; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A WYSIWYG content editor for adding simple formatted text to your page.'; 
	}
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		return [
			'html_content' => true,
			
		];
	}
}

?>