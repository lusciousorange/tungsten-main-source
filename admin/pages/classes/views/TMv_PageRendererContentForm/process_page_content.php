<?php 
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");

	// Enable publishing mode
	TMv_PageBuilder::enablePublishingMode($_POST['renderer_class_name']);
	
	//////////////////////////////////////////////////////
	//
	// PROCESSOR STARTUP
	//
	//////////////////////////////////////////////////////
	$form_processor = TCc_FormProcessorWithModel::init();
	$form_class = $form_processor->formClassName();

	$id = $form_processor->formValue($form_processor->formValue('renderer_id_field_name'));

	/** @var TMt_PageRenderer $renderer */
	$renderer = ($form_processor->formValue('renderer_class_name'))::init($id);

	$is_placeholder = false;
	// Special Case Placeholder
	if($_POST['view_class'] == 'TMv_PageContentPlaceholder')
	{
		$_POST['view_class'] = $_POST['placeholder_class_name'];
		$is_placeholder = true;
	}

	// Detect ROW column format changes
	if(isset($_POST['column_format']))
	{
		$_POST['view_class'] = $_POST['column_format'];
	}

	// Detect ROW column format changes
	if(isset($_POST['section_style']))
	{
		$_POST['view_class'] = $_POST['section_style'];
	}


	//////////////////////////////////////////////////////
	//
	// MESSENGER
	//
	//////////////////////////////////////////////////////

	$messenger = TC_website()->messenger();
	$messenger->setSuccessTitle('The content was successfully updated');
	$messenger->setFailureTitle('The content could not be updated');


	//////////////////////////////////////////////////////
	//
	// VALIDATE
	//
	//////////////////////////////////////////////////////

	$form_processor->processFormItems();
	$form_class::customFormProcessor_Validation($form_processor); // validation should always run

	//////////////////////////////////////////////////////
	//
	// UPDATE DATABASE
	//
	// DB updates happen with models which correlate to tables
	// DB updates don't happen automatically for regular forms
	//
	//////////////////////////////////////////////////////
	if($form_processor->isValid())
	{
		$form_class::customFormProcessor_performPrimaryDatabaseAction($form_processor);
	}
	
	if($form_processor->isValid())
	{
		$form_class::customFormProcessor_afterUpdateDatabase($form_processor);
	}

	// Placeholders just need the main value, then get out
	if($is_placeholder)
	{
		$url = $renderer->rendererPageBuilderURL();
		if(TC_getConfig('use_tungsten_9'))
		{
			$url .= "?return_type=fullpage";
		}
		
		$form_processor->setSuccessURL($url);
		$form_processor->finish();
	}


	//////////////////////////////////////////////////////
	//
	// PROPERTY PRIMING
	//
	//////////////////////////////////////////////////////

	$variable_values = array();
	
	if($form_processor->isValid())
	{
		
		
		/** @var TMt_PageRenderItem|string $content_item */
		$content_item = $form_processor->model();
		
		/** @var TMt_PagesContentView|string $view_class */
		$view_class = ($content_item->viewClass());
		
		/** @var TMt_PagesContentView $item_view_sample */
		$item_view_sample = ($view_class)::emptyContentView();
		$item_view_sample->setPagesContentItem($content_item);
		$item_view_sample->setRenderer($renderer);
		$form_class_name = $form_processor->formClassName(); // the name of the form class being processed (usually TMv_PageRendererContentForm)
		
		/** @var TCv_FormItem[] $form_items */
		$form_items = $form_class_name::formItemsForContentView($item_view_sample); // the form items for that page content item
		
		// Deal with groups
		// Loop through items, find the groups and add those items to the main list for processing
		foreach($form_items as $index =>  $form_item)
		{
			if($form_item instanceof TCv_FormItem_Group)
			{
				foreach($form_item->formItems() as $group_form_item)
				{
					$form_items[$group_form_item->id()] = $group_form_item;
				}
				// Remove the group from the list
				unset($form_items[$index]);
			}
		}
		
		// process form items
		foreach($form_items as $form_item)
		{
			if($form_item->saveToDatabase())
			{
				// Detect if the value is set, if not then we don't want an error to happen
				if($form_processor->fieldIsSet($form_item->id()))
				{
					$variable_values[$form_item->id()] = $form_processor->databaseValue($form_item->id());
				}
				else
				{
					$variable_values[$form_item->id()] = $form_item->defaultValue();
				}
				
			}
			
			
		}
		
		// Call the hook method for form processing on a page content item
		$new_values = $view_class::pageContent_FormProcessing($form_processor);
		
		// The returned values need to added to variable values
		if(is_array($new_values))
		{
			foreach($new_values as $index => $value)
			{
				$variable_values[$index] = $value;
			}
		}
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// UPDATE VARIABLES
	//
	//////////////////////////////////////////////////////
	if($form_processor->isValid())
	{
		$additional_values = array();
		
		// Deal with localization, which adds in extra properties to those values
		if(TC_getConfig('use_localization'))
		{
			$localization = TMm_LocalizationModule::init();
			
			// Get the localized fields
			$localization_fields = $item_view_sample::localizedPageContentSettings();
			
			if(count($localization_fields) > 0)
			{
				// Go through each form item again, checking if it's localized
				foreach($form_items as $form_item)
				{
					// field requires localization
					if(isset($localization_fields[$form_item->id()]))
					{
						
						// Create an array to save the additional values, could be multiple if more than 2 languages
						$field_values = array();
						foreach($localization->nonDefaultLanguages() as $language => $language_settings)
						{
							$submitted_language_value = $form_processor->databaseValue($form_item->id().'_'.$language);
							$field_values['value_'.$language] = $submitted_language_value;
						
							// Remove the field from the list of variable_values
							unset($variable_values[$form_item->id().'_'.$language]);
						
						
						}
						
						// Add the values to the additional values
						$additional_values[$form_item->id()] = $field_values;
					}
				}
			}
		}
		$content_item->updateVariablesWithArray($variable_values,$additional_values);
		// array('html_content' => array('value_fr'=>'fr text')));
	}

	
	//////////////////////////////////////////////////////
	//
	// FINISH AND REDIRECT
	//
	//////////////////////////////////////////////////////


	$url = $renderer->rendererPageBuilderURL();
	if(TC_getConfig('use_tungsten_9'))
	{
		if(strpos($url,'return_type=fullpage') === false)
		{
			$url .= "?return_type=fullpage";
		}
	}

	$form_processor->setSuccessURL($url);

	$form_processor->finish();
	
?>