<?php
/**
 * The form to edit the contnet on page
 * Class TMv_PageRendererContentForm
 */
class TMv_PageRendererContentForm extends TCv_FormWithModel
{
	/** @var TMt_PageRenderItem $content_item */
	protected $content_item;

	/** @var TMt_PageRenderer $renderer */
	protected $renderer;
	
	/** @var TMt_PagesContentView|TCv_View $content_view */
	protected $content_view;

	protected $view_class_name = '';

	/**
	 * TMv_PageRendererContentForm  constructor.
	 * @param string|TCm_Model|TMt_PageRenderItem $content_item
	 */
	public function __construct($content_item)
	{	
		parent::__construct($content_item);

		// Check if provided item is a model
		// Then we're editing
		$traits = class_uses($content_item);
		if($content_item instanceof TCm_Model &&  isset($traits['TMt_PageRenderItem']))
		{
			$this->content_item = $content_item;
			$this->setContentViewClass($this->content_item->viewClass());
			$this->content_view->setPagesContentItem($this->content_item);
			$this->content_view->setRenderer($content_item->renderer());
			
			// The ID of the item to always be the attribute of the view plus `_pef` for Page Editor Form
			$this->setIDAttribute($this->content_view->attributeID().'_pef');
			$this->setButtonText('Update Content');
		}
		else // Just a class name, which is still the ID of the form
		{
			$this->setIDAttribute('TMv_PageRendererContentForm_pef');
			$this->setButtonText('Create Content');
		}
		
		
		$this->setAction($this->classFolderFromRoot('TMv_PageRendererContentForm').'/process_page_content.php');
		$this->addClassCSSFile('TMv_PageRendererContentForm');
		
		// Ensure that it loads in the same iframe and ignores the base
		$this->setAttribute('target','_self');
		
	}
			
	/**
	 * The list of form items for a given content view. This should always be called rather than calling
	 * pageContent_EditorFormItems() directly on a page view.
	 * @param TMt_PagesContentView|TCv_View $content_view
	 * @return array
	 */
	public static function formItemsForContentView($content_view)
	{
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			return static::localizedFormItemsForContentView($content_view);
		}
		
		
		// Not localized, do return the normal values, ensuring it's an array
		$form_editor_items = $content_view->pageContent_EditorFormItems();
		
		if(is_array($form_editor_items))
		{
			return $form_editor_items;
		}
		else
		{
			return array();
		}
		
	}
	
	/**
	 * Returns the form items that are localized for the given content form
	 * @param TMt_PagesContentView|TCv_View $content_view
	 * @return TCv_View[]
	 */
	public static function localizedFormItemsForContentView($content_view)
	{
		$form_editor_items = $content_view->pageContent_EditorFormItems();
		
		// Generate a new array that puts them in the correct order
		$new_form_editor_items = [];
		
		// Get the localized fields
		$localization_fields = $content_view::localizedPageContentSettings();
		$localization = TMm_LocalizationModule::init();
		
		foreach($form_editor_items as $index => $form_item)
		{
			// Add the normal one back
			$new_form_editor_items[$index] = $form_item;
			
			// field requires localization
			if(isset($localization_fields[$form_item->id()]))
			{
				// Detect hidden title columns, show then stack them
				if(!$form_item->showTitleColumn())
				{
					$form_item->setShowTitleColumn(true);
					$form_item->addClass('stacked');
				}
				
				// Build a section for each language
				foreach($localization->nonDefaultLanguages() as $language => $language_settings)
				{
					
					$form_item_language = clone $form_item;
					$form_item_language->setID($form_item_language->id().'_'.$language);
					$form_item_language->setTitle($form_item_language->title().' ('.$language_settings['title'].')');
					$form_item_language->addClass('localization');
					$form_item_language->addClass('localization_'.$language);
					$form_item_language->addClass($form_item->id().'_row');
					
					// If we aren't enforcing required, then disable it no matter what
					if(!TC_getModuleConfig('localization','enforce_required'))
					{
						$form_item_language->disableRequired();
					}
					
					if($default_value_localized = $form_item->defaultValueLocalized($language))
					{
						$form_item_language->setDefaultValue($default_value_localized);
					}
					
					$varname = $form_item_language->id();
					if(property_exists($content_view, $varname))
					{
						// values aren't saved in normal space, so we need to pull them differently
						$form_item_language->setSavedValue($content_view->$varname);
						
					}
					
					$new_form_editor_items[$index.'_'.$language] = $form_item_language;
					
				}
			}
		}
		
		return $new_form_editor_items;
	
	}

	/**
	 * Adds the form settings that are specific to this TMt_PagesContentView .
	 */
	public function addContentSpecificSettings()
	{
	//	print '<br />DEBUG: TMv_PageContenForm:addContentSpecificSettings()';
		$heading = new TCv_FormItem_Heading('content_values', ' Content properties');
		$this->attachView($heading);
		
		
		// Removed because it breaks loading of JS classes. Also corrected in other locations
		//$this->content_view->setIDAttribute('TMv_PageRendererContentForm_view');
		
		// Generate the views
		$form_items = self::formItemsForContentView($this->content_view);
		

		// Handle CSS / JS that might hvae been added
		$this->addClass('TMv_PageRendererContentForm_view');
		$this->addClass(get_class($this->content_view).'_form');
		//$this->addJSLine('turn_off_previous_js', 'window.'.$this->content_view->id() + ".off('**');");
		$this->attachCSSFromView($this->content_view);
		$this->attachJSFromView($this->content_view);
		$this->attachMetaTagsFromView($this->content_view);

		foreach($form_items as $form_item)
		{
			if($form_item instanceof TCv_FormItem)
			{
				$form_item->setSaveToDatabase(false); // processed separately in the process script
				
				if($this->isEditor())
				{
					$form_item->setEditorValue($this->content_item->variable($form_item->id()));
				}
				
			}
			
			if($form_item instanceof TCv_FormItem_Group)
			{
				foreach($form_item->formItems() as $group_form_item)
				{
					if($group_form_item instanceof TCv_FormItem)
					{
						$group_form_item->setSaveToDatabase(false); // processed separately in the process script
						
						if($this->isEditor())
						{
							$group_form_item->setEditorValue($this->content_item->variable($group_form_item->id()));
						}
						
					}
				}
			}
			
			$this->attachView($form_item);
			
		}
			
		if(sizeof($form_items) == 0)
		{
			$this->attachView($this->noContentOptionsView());
		}
	}
	
	protected function processContentFormItem($form_item)
	{
		if($form_item instanceof TCv_FormItem)
		{
			$form_item->setSaveToDatabase(false); // processed separately in the process script
			
		}
		if($this->isEditor())
		{
			$form_item->setEditorValue($this->content_item->variable($form_item->id()));
		}
	}
	
	/**
	 * Returns a view for when there are no content options.
	 * @return TCv_View
	 */
	public function noContentOptionsView()
	{
		$no_form_item_note = new TCv_View();
		$no_form_item_note->setTag('p');
		$no_form_item_note->setAttribute('style', 'font-size:14px; ');
		if($this->isEditor())
		{
			$no_form_item_note->addText('This item has no options for content settings. Press the "Update Content" button to add this content to the page, or press the "X" in the top right corner to cancel.');
		
		}
		else
		{
			$no_form_item_note->addText('This item has no options for content settings. Press the "Create Content" button to add this content to the page, or press the "X" in the top right corner to cancel.');
		}
		return $no_form_item_note;
	}
	
	/**
	 * Sets the view class for this item
	 * @param TMt_PagesContentView|bool $view_class
	 */
	public function setContentViewClass($view_class)
	{
		// If there's a required input model, pass in the class name which is almost always going to work
	

		$this->content_view = ($view_class)::emptyContentView();
		$this->content_view->setRenderer($this->renderer);
		$view_class_field = new TCv_FormItem_Hidden('view_class', '');
		$view_class_field->setValue($view_class);

		$this->attachView($view_class_field);

		$this->view_class_name = $view_class;
		
	}

	/**
	 * Sets the render item information which includes indicating the field name and the id to be set
	 * @param TMt_PageRenderer $renderer
	 * @param string int $value
	 */
	public function setRendererItem($renderer)
	{
		$this->renderer = $renderer;
		
		$field = new TCv_FormItem_Hidden($renderer->rendererIDColumn(), '');
		$field->setValue($renderer->rendererID());
		$this->attachView($field);

		$field = new TCv_FormItem_Hidden('renderer_class_name', '');
		$field->setValue(get_class($renderer));
		$field->setSaveToDatabase(false);
		$this->attachView($field);

		$field = new TCv_FormItem_Hidden('renderer_id_field_name', '');
		$field->setValue($renderer->rendererIDColumn());
		$field->setSaveToDatabase(false);
		$this->attachView($field);
		
		if( !($renderer instanceof TMm_PagesMenuItem))
		{
			$field = new TCv_FormItem_Hidden('item_class_name', '');
			$field->setValue(get_class($renderer));
			$this->attachView($field);
			
		}

	}

	/**
	 * Sets the container id for this view
	 * @param $container_id
	 */
	public function setContainerID($container_id)
	{
		$name = $this->modelName();
		$container = $name::init($container_id);
		
		$menu_id_field = new TCv_FormItem_Hidden('container_id', '');
		$menu_id_field->setValue($container->id());
		$this->attachView($menu_id_field);
		
		$menu_id_field = new TCv_FormItem_Hidden('position_content', '');
		$menu_id_field->setValue($container->numChildrenContent()+1);
		$this->attachView($menu_id_field);
		
		$menu_id_field = new TCv_FormItem_Hidden('is_visible', '');
		$menu_id_field->setValue(1);
		$this->attachView($menu_id_field);
	
	
	}
	
	/**
	 * Configuration of the form
	 */
	public function configureFormElements()
	{
		
		$this->addContentSpecificSettings();
		
		$this->addDeveloperValues();
		
		
	}
	
	/**
	 * Attaches the developer values to the form
	 */
	public function addDeveloperValues()
	{
		// DEVELOPER
		$heading = new TCv_FormItem_Heading('developer_values', 'Advanced');
		$this->attachView($heading);
		
		$css_classes = new TCv_FormItem_Select('css_classes', 'Component styles');
		
		$css_classes->setUseMultiple(' ');
		$css_classes->useFiltering();
		$css_classes->addOption('','Add one or more component styles');
		
		$all_classes = array();
		
		$pages_module = TMm_PagesModule::init();
		$theme_name = $pages_module->editorThemeName();
		foreach($theme_name::allComponentCSSViewStyles() as $view_style)
		{
			if($view_style->viewClassIsPermitted(get_class($this->content_view)))
			{
				$all_classes[$view_style->className()] = $view_style->title();
			}
			
		}
		
		$css_classes->addOptions($all_classes);
		$this->attachView($css_classes);
		
		$id_attribute = new TCv_FormItem_TextField('id_attribute', 'ID Attribute');
		$id_attribute->setHelpText('The unique ID attribute for this view. If left blank it will be auto filed with an appropriate value.');
		$id_attribute->setAsHalfWidth();
		$this->attachView($id_attribute);
		
		if($this->view_class_name != 'TMv_ContentLayoutBlock' )
		{
			$field = new TCv_FormItem_TextField('visibility_condition', 'Visibility Condition');
			$field->setHelpText('A system-call string such as {{module_folder.method}} which returns a boolean
		value that will be used to determine if this items should appear on the website.');
			$field->setAsHalfWidth();
			$this->attachView($field);
		}
		
	}


	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);
		
		// Update publishing
		$form_processor->model()->renderer()->markAsUnpublished();
		
		
		
	}
	
	
}