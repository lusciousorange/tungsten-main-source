<?php

/**
 * Generates a menu item based on a provided navigation type.
 */
class TMv_PagesNavigationTypeMenu extends TMv_PageMenus
{
	public function __construct(TMm_PagesNavigationType $navigation_type)
	{
		parent::__construct($navigation_type->shortCode());
		
		if($navigation_type->isFlat())
		{
			$this->setAsManual();
			foreach($navigation_type->pages() as $page)
			{
				$button = $this->menuButtonForPagesMenuItem($page);
				$this->attachView($button);
			}
		}
	}
}