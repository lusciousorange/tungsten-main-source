<?php
/**
 * Class TMv_PageContentRendering
 */
class TMv_PageContentRendering extends TCv_View
{
	protected $section_views = array();
	protected $content_views = array();
	/**
	 * @var bool|TMt_PageRenderer The render item that is being rendered
	 */
	protected $renderer_item = false;
	protected $show_all_views = false;
	protected $views_generated = false;
	protected $generated_views_attached = false;
	protected $theme_name = '';

	/** @var bool Variable to check if rendering is still permitted. If disabled by a component, it will not let any
	 * other view render.
	 */
	protected $rendering_permitted = true;

	/**
	 * TMv_PageContentRendering constructor.
	 * @param bool|TMt_PageRenderer $renderer_item The render item that is being rendered
	 */
	public function __construct( $renderer_item )
	{	
		$this->renderer_item = $renderer_item;

		parent::__construct('page_content_rendering_'.$this->renderer_item->id());
		
		$this->addClassCSSFile('TMv_PageContentRendering');
		$this->addClassCSSFile('TMv_ContentLayout');

		$this->theme_name = TC_getModuleConfig('pages', 'website_theme');
	}

	/**
	 * Loads the CSS files for all the layouts
	 */
	protected function loadLayoutCSSFiles()
	{
		$layout_list  = TMm_PageLayoutList::init();
		$layouts = $layout_list->pageLayouts();
		foreach($layouts as $class_name => $path)
		{
			if($class_name::pageContent_IsAddable())
			{
				$this->addClassCSSFile($class_name);
			}
		}

	}

	/**
	 * Generates the views for the provided menus
	 * @uses TMm_PagesMenuItem::layoutContentItems()
	 * @uses TMv_PageContentRendering::processLayoutContentModel()
	 */
	public function generateViews()
	{
		if(!$this->views_generated)
		{
			$this->views_generated = true;
			foreach($this->renderer_item->layoutContentItems() as $layout_content_model)
			{
				$this->processLayoutContentModel($layout_content_model);
			}
		}	
	}
	
	/**
	 * Turns on the functionality where all views are shown.
	 * @return void
	 */
	public function enableAllViews()
	{
		$this->show_all_views = true;
	}
	
	
	
	/**
	 * Handles the layout of the provided content model into layout views stored internally.
	 * @param TMt_PageRenderItem $layout_content_model The content page model that is being processed
	 * @uses TMv_PageContentRendering::viewForContentModel()
	 * @uses TMt_PageRenderItem::containerContent()
	 */
	public function processLayoutContentModel($layout_content_model)
	{
		if($layout_content_model->isVisible() || $this->show_all_views)
		{
			$section_view = $this->viewForContentModel($layout_content_model);
			if($section_view)
			{
				$section_view->addClass('section_'.(count($this->section_views) + 1 ) );
				$this->section_views[$layout_content_model->id()] = $section_view;
			}
		}
	}
	
	/**
	 * Returns the layout view for the provided ID
	 * @param string $content_model_id The ID of the view that is watned.
	 * @return TMv_ContentLayout
	 */
	public function layoutViewWithID($content_model_id)
	{
		return $this->section_views[$content_model_id];
	}
	
	/**
	 * Generate the view for a single content item. This is a recursive function that deals with the nesting of the content inside of containers
	 * @param TMt_PageRenderItem $content_model
	 */
	protected function processContainerContent($content_model)
	{
		// Find container content, generate views and attach them
		foreach($content_model->containerContent() as $child_model)
		{
			if($this->show_all_views || $child_model->isVisible() )
			{
				$this->attachContentModelToParent($child_model);
			}
		}
		
	}
	
	/**
	 * Attaches the content model to the parent and returns the parent for chaining
	 * @param TMt_PageRenderItem $child_model
	 * @return TMt_PagesContentView|TCv_View|false
	 */
	protected function attachContentModelToParent($child_model)
	{
		// Get the view for the child
		$child_view = $this->viewForContentModel($child_model);
		
		
		// Find the parent and attach it
		$parent_model = $child_model->container();
		$parent_view = $this->viewForContentModel($parent_model);
		if($parent_view)
		{
			$parent_view->attachView($child_view);
			
			// Detect if the child disabled rendering
			// Check if the view disables rendering
			if($child_view && $child_view->disablesFurtherRendering())
			{
				$this->rendering_permitted = false;
			}
			
			return $parent_view;
		}
		else
		{
			TC_triggerError('Attempting to attach model to non-existent parent');
		}
		
		return false;
	}
	
	/**
	 * Generate the view for a single content item. This is a recursive function that deals with the nesting of the
	 * content inside of containers. This function does internal caching to avoid excess rendering.
	 * @param TMt_PageRenderItem $content_model
	 * @return bool|TCu_Item
	 */
	protected function viewForContentModel($content_model)
	{
		// Check for cached version
		if(isset($this->content_views[$content_model->id()]))
		{
			return $this->content_views[$content_model->id()];
		}
		
		/** @var string|TMt_PagesContentView $view_class_name */
		$view_class_name = $content_model->viewClass();
		
		// Check to see if rendering has been disabled
		if(!$this->rendering_permitted)
		{
			return null;
		}
		// Handle view with a classname that doesn't exist
		if(!class_exists($view_class_name))
		{
			$this->addConsoleDebugObject('MISSING VIEW : '.$content_model->viewClass(), $content_model);
			return false;
		}

		// Handle TMv_ContentLayoutBlock
		// These views already exist in the respective parent, so we should use those instead for consistency
		if($view_class_name == 'TMv_ContentLayoutBlock')
		{
			// Ensure the container exists
			if($content_model->container())
			{
				$parent_view = $this->viewForContentModel($content_model->container());
				$content_view = $parent_view->layoutBlockViewForBlockPosition($content_model->blockPosition());
				$content_view->setAttribute('data-content-id', $content_model->id());
			}
			else // Catch a scenario where a column exists with no parent
			{
				// Delete the container
				$content_model->delete();
				$this->addConsoleDebugObject('MISSING PARENT '.$content_model->viewClass()
				                             .', Deleting', $content_model);
				return false;
			}
		}
		else
		{
			// Find the input model name if it exists
			$required_model_names = $view_class_name::pageContent_View_InputModelName(); // false or a class name
			$view_param = false; // default to false, which means no parametser needed
			
			$override = $view_class_name::overrideViewParameter($content_model);
			if($override !== null)
			{
				$view_param = $override;
			}
			elseif($required_model_names)
			{
				// Always an array for consistency
				if(!is_array($required_model_names))
				{
					$required_model_names = array($required_model_names);
				}
				
				// Loop through each required model name
				// Once we find an active model, we stop
				// If we find the string "class_name" then we pass in the name of the previous value
				foreach($required_model_names as $index => $required_model_name)
				{
					// avoid repeating, cascades until it finds a param value
					if($view_param === false)
					{
						if(class_exists($required_model_name))
						{
							// Try if active model already exists
							// This requires the model to be
							if($model = TC_activeModelWithClassName($required_model_name))
							{
								$view_param = $model;
							}
							elseif(count($required_model_names) == 1)
							{
								// Legacy Support
								// If there is only one option, then it's safe to try and instantiate it from the URL
								
								$model = ($required_model_name)::init($_GET['id']);
								if($model)
								{
									$view_param = $model;
									TC_saveActiveModel($model);
								}
							}
						}
						elseif($required_model_name == 'class_name' && $index > 0)
						{
							// We indicate we pass in the last class_name
							// so we need to get the previous index and just set it to the class_name
							$view_param = $required_model_names[$index - 1];
						}
						else
						{
							$this->addConsoleWarning('Page Content Input Parameter not valid : ' . $required_model_name
							                         . '. Check that the page is configured to have a model class.');
						}
					}
				}
				
				
			}
			
			// Deal with passing in the content model into layouts
			if(is_subclass_of($view_class_name, 'TMv_ContentLayout'))
			{
				$view_param = $content_model;
				
			}
			
			/** @var TMt_PagesContentView|TCv_View $content_view */
			$init_class_name = $view_class_name::classNameForInit($view_param);
			$content_view = new $init_class_name($view_param); // don't save instance
			$content_view->setAttribute('data-content-id', $content_model->id());
		}
		
		
		// Handle as set by pages layout
		if(method_exists($content_view,'setAsPagesLayout'))
		{
			$content_view->setAsPagesLayout();
		}
		
		// ACTUAL CONTENT
		if(!$content_model->isRootLevelContent())
		{
			$content_view->addClass('page_content_item');
		}
		
		if($content_model->layoutRowPosition() > 0)
		{
			$content_view->addClass('row');
		}
		
		
		$content_view->setPagesContentItem($content_model);
		
		$content_view->setHTMLCollapsing(false);
		
		// Loop through the view styles, apply them to the element, if appropriate
		// Some are applied to the content container
		foreach($content_model->cssPageViewStyles() as $page_view_style)
		{
			if(!$content_model->isLayoutContent() || !$page_view_style->applyToContentContainer())
			{
				$content_view->addClass($page_view_style->className());
			}
			
		}
		
		
		// Save to our list of views, referenced later
		$this->content_views[$content_model->id()] = $content_view;
		
		// Pass the view through the theme hook for updating
		$theme_name = $this->theme_name;
		if(class_exists($theme_name))
		{
			$content_view = $theme_name::hook_updateContentView($content_view);
		}
		
		// Process Container Content
		// This generates a recursive call that will go through the children and generate views for them as well
		$this->processContainerContent($content_model);

		return $content_view;
	}
	
	/**
	 * Function to attach content to a layout block. Done this way to allow for extending with an editor
	 * @param TMv_ContentLayoutBlock $layout_block The content block that is being added to
	 * @param TMt_PagesContentView $content_view The content view that is being added
	 * @return TMv_ContentLayoutBlock
	 */
	protected function attachContentToLayoutBlock($layout_block, $content_view)
	{
		$layout_block->attachView($content_view); 
		return $layout_block;
	}
	
	/**
	 * Attaches the generated views to this view unless a target is provided in which case they are attached to that target
	 * @param bool|TCV_View $target attaches the generated view
	 */
	public function attachGeneratedViews($target = false)
	{
		if(!$this->generated_views_attached)
		{
			$this->generated_views_attached = true;
			foreach($this->section_views as $view)
			{
				if($target)
				{
					$target->attachView($view);	
				}
				else
				{
					$this->attachView($view);
				}
				
			}			
		}			
		
	}
	
	/**
	 * Returns the HTML for this view
	 */
	public function render()
	{	
		$this->generateViews();
		$this->attachGeneratedViews();

	}
	
	/**
	 * Extend the functionality around page rendering to parse page view settings so they link absolutely to a given
	 * item
	 * @return string|string[]
	 */
	public function html()
	{
		$html = parent::html();
		return $this->replacePageViewCodesWithURL($html);
	}
	
	
	
	
}
?>