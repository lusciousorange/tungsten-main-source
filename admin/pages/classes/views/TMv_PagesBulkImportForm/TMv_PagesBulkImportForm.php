<?php

/**
 * Class TMv_PagesBulkImportForm
 *
 *
 */
class TMv_PagesBulkImportForm extends TCv_Form
{
	public static $upload_folder = '/../files/pages-excel/';
	public function __construct()
	{
		parent::__construct('bulk_import');
	//	$this->setSuccessURL('/admin/pages/');
		
	}

	public function configureFormElements()
	{
		$field = new TCv_FormItem_HTML('download','Export');
		$field->setHelpText("Download a pre-formatted excel file that you can update and change to import.");
		
		$url_link = new TSv_ModuleURLTargetLink('download-pages-excel');
		//$url_link->setIconClassName('fa-file-excel');
		$url_link->setAttribute('style','display:inline-block;padding:5px 10px; background:#333;color:#FFF;');
		$field->attachView($url_link);
		$field->preventLayoutStacking();
		$this->attachView($field);
		
//		$field = new TCv_FormItem_TextBox('page_items','Page Item');
//		$field->setHelpText('Add a list of menu items that you want added to the pages module. If you want to indent a level, use a single dash (-) to indicate to indent. Maximum 4 levels deep.');
//		$this->attachView($field);
		
		$field = new TCv_FormItem_FileDrop('upload','Import');
		$field->setHelpText("Upload a pre-formatted excel file that must match the format seen in the export.");
		$field->setUploadFolder($_SERVER['DOCUMENT_ROOT'] . static::$upload_folder);
		$field->preventLayoutStacking();
		$this->attachView($field);
		
	}

	/**
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		$upload_folder = $_SERVER['DOCUMENT_ROOT'] . static::$upload_folder;
		
		
		$file = new TCm_File('excel_file',
		                     $form_processor->formValue('upload'),
		                     $upload_folder,
		                     true);
		if($file->exists())
		{
			$importer = TMm_PagesExcelImport::init($file);
			$importer->processFile();
		}
		else
		{
			$form_processor->fail('File upload did not succeed. Please try again. ');
		}
		

	}
	
	/**
	 * Help html
	 * @return string|null
	 */
	public static function defaultHelpHTML(): ?string
	{
		return
'<p>This form is used to handle bulk import of page data in order to save time with setup or updates on large websites. This is done through
the use of a specifically formatted excel file.</p>

<ol>
	<li>The export will generate a file to use with the current pages, and with the correct columns.</li>
    <li>Update the sheet, add pages, modify them, etc</li>
    <li>Import will apply those changes. </li>
</ol>
		
<h2>Modifying the sheet</h2>
<p>Every column has a purpose. Do <strong>not</strong> rearrange or add columns as that will break the import. </p>
		
<ul>
	<li>The first four columns indicate the "level" of the item, which indicates nesting of navigation. So items in level 2
		 will be a child of the previous level 1 menu item in the excel sheet. If a column has a number in it, then that indicates
		 the menu ID of an existing page. </li>
	<li>The title is what is shown on the page and it is required.</li>
	<li>The folder indicates the name in the path of the URL for just this page and should only container letters, numbers, underscore and dash.</li>
	<li>If 301 Redirects are enabled on the site via <a href="/admin/pages/do/settings">Pages > Settings</a>, then you can indicate the URL from a previous website
that this page maps to. </li>
</ul>

<h3>Creating new pages</h3>

<ul>
	<li>New pages are indicated by using an "x" in one of the level columns. </li>
	<li>Existing pages are updated by referencing their ID in the level column. </li>
	<li> Every page must have an entry in one and only one level column.</li>
	<li>Titles are required and can be renamed via the import</li>
	<li>Folders are optional and can be renamed. If left blank, they will be generated from the title using basic letters and numbers with spaces converted to dashes</li>
	<li>Page re-ordering <strong>does not work</strong> via import. Use the page builder.</li>
	<li>New pages are added as the last child of their parents.</li>
</ul>

';
	}
	
	
}