<?php
/**
 * Class TMv_ContentLayout
 */
abstract class TMv_ContentLayout extends TCv_View
{
	use TMt_PagesContentView;
	
	/** @var TMv_ContentLayoutBlock[] $content_blocks */
	protected array $content_blocks = [];
	protected $content_width_container = false;
	protected $preview_grouping_colour = '0,0,0';
	protected $responsive_collapse = 1;
	protected $responsive_flip_order = 0;
	protected $extend_full_width = 0;
	protected $is_manual_layout = true;
	protected $background_position = 'center center';
	protected $background_size = 'cover';
	protected $next_column = 1;
	protected $margin_uses_pixels = false;
	protected $num_pattern_columns = 0;
	protected $equal_height_columns = 0;

	protected string $column_format;
	
	// FILTERS
	protected $filters = '';
	
	protected static array $column_percentages = [[100,0]];
	protected static ?array $column_widths = null;
	
	/**
	 * TMv_ContentLayout constructor which passes in a string ID or false.
	 * @param bool|string|TMm_PagesContent $id
	 */
	public function __construct ($id = false)
	{
		if(is_object($id) && TC_classUsesTrait($id, 'TMt_PageRenderItem'))
		{
			$this->setPagesContentItem($id);
		}

		parent::__construct($id);

		$this->content_width_container = new TCv_View();
		$this->setContentWidthContainerClassName('content_width_container');
		$this->addClassCSSFile('TMv_ContentLayout');

		

	}

	/**
	 * Indicates the css class name for the content width container
	 * @param string|bool $class_name (Optional) Default "content_width_container" THe class name applied
	 */
	public function setContentWidthContainerClassName ($class_name)
	{
		$this->content_width_container->clearClasses();
		if ($class_name)
		{
			$this->content_width_container->addClass($class_name);
		}
		
		// add the content_width_container shortcode always
		// Necessary for column spacing
		$this->content_width_container->addClass('cwc');
		
	}

	/**
	 * Sets the margin for this item
	 * @param string|int $margin
	 * @deprecated Margin percentage no longer used
	 */
	public function setMarginPercentage($margin)
	{
	}

	/**
	 * Sets this layout to use equal heights
	 */
	public function setAsEqualHeights()
	{
		$this->equal_height_columns = true;
	}

	/**
	 * Adds a content block to the list that define this layout.
	 * @param TMv_ContentLayoutBlock $view The LayoutBlock or View that should be added as a content block.
	 */
	public function defineContentBlock ($view)
	{
		$view->addClass('content_column');
		$this->content_blocks[] = $view;
	}

	/**
	 * Returns the list of content blocks
	 * @return TMv_ContentLayoutBlock[]
	 */
	public function contentBlocks ()
	{
		return $this->content_blocks;
	}

	/**
	 * Returns the view for the given block position
	 *
	 * @param int $block_position The position of this block in this layout, starts at 1
	 * @return TMv_ContentLayoutBlock
	 */
	public function layoutBlockViewForBlockPosition ($block_position)
	{
		$blocks = $this->contentBlocks(); // generate the blocks, necessary for multi-row
		return $blocks[$block_position - 1];
	}

	/**
	 * Turns off responsive for this layout
	 *
	 */
	public function disableResponsive ()
	{
		$this->responsive_collapse = 0;
	}
	
	/**
	 * Abstract method that must be defined for every content layout which returns how many columns are in layout.
	 * @return int
	 */
	public static abstract function numColumns();
	
	/**
	 * Calculates the pixel width for a given column in this layout. This requires the static values for
	 * $column_percentages to be set properly. It also requires the content_width and content_column_gap config
	 * properties to be set.
	 * @param int $column_num
	 * @return int
	 */
	public static function pixelWidthForColumn(int $column_num) : int
	{
		$content_width = TC_getConfig('content_width');
		
		
		// Determine if we can even calculate widths. If so, do it for all of them
		if(is_null(static::$column_widths) )
		{
			// set it at the content width for all 5,just in case one isn't set. We need values
			static::$column_widths = [$content_width,$content_width,$content_width,$content_width,$content_width,$content_width];
			
			// If there is no set column gap, we can't calculate anything
			if(TC_configIsSet('content_column_gap'))
			{
				$column_gap_width = TC_getConfig('content_column_gap');
				// Calculate how many columns and gaps actually exist for this layout
				$num_gaps = 0;
				$num_columns = 0;
				foreach(static::$column_percentages as $column_values)
				{
					$num_columns++; // add this column
					$num_gaps += $column_values[1]; // Second value is the number of gaps
					
				}
				
				// Add the number of columns minus 1 to the number of gaps
				
				$num_gaps += $num_columns - 1 ;
				
				// remove all the gaps, to be left with a usable width.
				// Individual columns will add that value back in
				$usable_width = $content_width - ($num_gaps * $column_gap_width);
			//	$debug->addConsoleDebug("Usable width: ". $usable_width);
				
				// Loop through each column, overriding the default width with a real one
				foreach(static::$column_percentages as $index => $column_values)
				{
					$column_percentage = $column_values[0];
					$column_num_gaps = $column_values[1];
					
					// Usable width times the percentage, but add in flat amounts for gaps that should be in there too
					$pixel_width = round($usable_width * $column_percentage / 100) +
						($column_num_gaps * $column_gap_width);
				//	$debug->addConsoleDebug('Column '.($index+1) ." : ". $pixel_width);
					
					static::$column_widths[$index + 1] = $pixel_width;
				}
			}
			
		}
	
		
		// If nothing else, return a value we can use safely
		return static::$column_widths[$column_num];
		
	}
	
	//////////////////////////////////////////////////////
	//
	// MANUAL LAYOUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets this content layout to use the pages layout. This is done automatically in the page renderer and shouldn't need to be called.
	 */
	public function setAsPagesLayout ()
	{
		$this->is_manual_layout = false;
		$this->removeClass('manual_layout');
	}

	/**
	 * @deprecated No longer necessary as content layouts will work without needing this setting turned on
	 */
	public function setAsManualLayout ()
	{
		$this->is_manual_layout = true;
		$this->addClass('manual_layout');
	}

	/**
	 * Attaches a view to a content block with a particular number. In most cases, these will be column nubmers (1, 2, 3, etc). This function only has an effect on views that have been already set to use manual layout. This function has no effect otherwise since content is generated through the Page Builder.
	 * @param ?TCv_View $view The view that is being attached
	 * @param int $block_number The block number starting at 1, and counting up to the highest number of columns.
	 */
	public function attachViewToContentBlock ($view, $block_number = 0)
	{
		$this->attachViewToColumnNum($view, $block_number);
	}
	
	/**
	 * Attaches a view to a content block with a particular number. In most cases, these will be column numbers (1, 2,
	 * 3, etc). This function only has an effect on views that have been already set to use manual layout. This function has no effect otherwise since content is generated through the Page Builder.
	 * @param ?TCv_View $view The view that is being attached
	 * @param int $column_number The column number starting at 1, and counting up to the highest number of columns.
	 * @param bool $use_content_container Indicates if the content container should be used
	 */
	public function attachViewToColumnNum (?TCv_View $view,
	                                       int $column_number = 0,
	                                       bool $use_content_container = true) : void
	{
		// Do nothing on null views
		if(is_null($view))
		{
			return;
		}
		if ($column_number == 0)
		{
			$column_number = $this->next_column;
			$this->next_column++;
			
		}
		
		if ($this->is_manual_layout)
		{
			$column = $this->content_blocks[$column_number - 1];
			
			if($use_content_container)
			{
				$column->attachView($view);
			}
			else
			{
				$column->attachViewWithoutContentContainer($view);
			}
		}
	}

	/**
	 * Adds text to a content block. It will be wrapped in a basic view
	 * @param string $text The text to be added
	 * @param int $block_number The block number starting at 1, and counting up to the highest number of columns.
	 * @uses TMv_ContentLayout::attachViewToContentBlock()
	 */
	public function addTextToContentBlock ($text, $block_number = 0)
	{
		if(!is_string($text) )
		{
			TC_triggerError('Provided text must be a string, '.gettype($text).' provided.');
		}
		$view = new TCv_View();
		$view->addText($text);

		$this->attachViewToContentBlock($view, $block_number);
	}

	//////////////////////////////////////////////////////
	//
	// MARGIN CALCULATIONS
	//
	// Calculations within margins are
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets the margin pattern for a column.
	 *
	 * Each column is sized based on a percentage width of the container with a possible left margin being added for spacing.
	 * That spacing is entirely based on the margin percentage which is subtracted from each column and applied to all the columns but the first one in most cases.
	 *
	 * For example a 2 column layout would be set to 50/50 for the widths of each column. The first column would not have
	 * a left column but the second column would. The system then calculates the appropriate values which will be generated
	 * by the system on output.
	 * @param int $column_num The number of the column (1, 2, 3, etc)
	 * @param float|int $width The percentage width of the column. the various percentages should add up to 100 in most cases.
	 * @param float|int $column_span (Optional) Default 1. Indicates if this column overlaps more than one column in the expanded version. For example, if there is a 4 column layout and this column actually spans 2, then a 2 should be passed in.
	age
	 * @deprecated Margins are set for each layout in their own css file along with --layout-column-gap
	 **/
	public function setMarginPatternForColumn ($column_num, $width, $column_span = 1)
	{
		// Do nothing
	}
	
	/**
	 * Generates the CSS values for the margins for this column and adds them to the auto-generated CSS for this layout
	 * @param string|bool $css_prefix A prefix for classes that are used to identify if this is a subtype for css
	 * classes
	 * @return array
	 * @deprecated Margins no long calculated in PHP
	 */
	public function generateMarginCSS($css_prefix = false)
	{
	
	}



	

	/**
	 * Returns the CSS that forces a column to be a single column
	 * @return string
	 */
	protected function singleColumnCSS()
	{
		return "
		width:100% !important;
		margin-left:0px !important;
		margin-right:0px !important;
		padding-left:0px !important;
		padding-right:0px !important;
		float:none;
		clear:left;";
	}



	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////

	/**
	 * Override attach view method to attach to the inside content width container
	 *
	 * @param bool|TCv_View|TMt_PagesContentView $view The view that is being attached
	 * @param bool $prepend (Optional) Default false. Indicates if this view should be prepended before the other views.
	 */
	public function attachView($view, $prepend = false)
	{
		$this->content_width_container->attachView($view, $prepend);
		
//		// Handle control panel
//		if(method_exists($view, 'hasControlPanelView') && $view->hasControlPanelView())
//		{
//			$this->content_width_container->attachView($view->controlPanelView());
//		}
//
		
	}

	/**
	 * Attaches the view to the root of this view instead of to the content_width_container which is the default interaction with attachView()
	 * @param TCv_View $view The view to be attached
	 * @param bool $prepend (Optional) Default false
	 * @see TMv_ContentLayout::attachView()
	 */
	public function attachViewToRoot($view, $prepend = false)
	{
		parent::attachView($view, $prepend);
		
	
	}
	
	/**
	 * Sets the colour for this layout in the Page Builder preview
	 * @param string $color The RGB colour formatted as three values from 0 to 255, comma-separated
	 */
	public function setPreviewGroupingColor($color)
	{
		$this->preview_grouping_colour = $color;
	}



	/**
	 * Generates the preview for this layout to be shown in the PageBuilder.
	 * @return TCv_View
	 */
	public function preview()
	{
		$preview = new TCv_View('preview_'.get_called_class());
		$preview->addClass('preview');
		$preview->addClass('TMv_ContentLayout');
		$preview->addClass(get_called_class());	
		//$preview->addClassCSSFile(get_called_class());
		if($this->hasClass('two_row_layout'))
		{
			$preview->addClass('two_row_layout');
		}
		$container = new TCv_View();
		$container->addClass('cwc');
		
		$number = 1;

		foreach($this->content_blocks as $content_block)
		{
			$numbering = new TCv_View();
			$numbering->addClass('numbering');
			$numbering->setAttribute('style','background-color: rgba('.$this->preview_grouping_colour.', 0.3); color: rgb('.$this->preview_grouping_colour.');border-color: rgb('.$this->preview_grouping_colour.');');
			$numbering->addText($number);
			$content_block->attachView($numbering);
			$container->attachView($content_block);
			$number++;
		}
		
		$preview->attachView($container);


		return $preview;
	}

	

	/**
	 * Returns the HTML for this view
	 * @return string
	 */
	public function html()
	{
		//$this->addClassCSSFile(get_called_class());


		if($this->equal_height_columns)
		{
			$this->addClass('full_height_row');
		}

		// Deal with Expanding to Full Width
		if($this->extend_full_width == 1)
		{
			$this->addClass('extend_full_width');
		}
		elseif($this->extend_full_width === 'left')
		{
			$this->addClass('extend_full_width_left');
		}
		elseif($this->extend_full_width === 'right')
		{
			$this->addClass('extend_full_width_right');
		}
		
		$this->applyBackgroundImage();
		
		
		$this->addClass('responsive_collapse_'.$this->responsive_collapse);

		if($this->responsive_flip_order)
		{
			$this->addClass('responsive_flip_order');
		}



		
		if($this->is_manual_layout)
		{
			foreach($this->content_blocks as $content_block)
			{	
				$content_block->addClass('TMv_ContentLayoutBlock');
				$content_block->removeClass('pages_block');
				$this->attachView($content_block);
			}
		}
		else
		{
			$this->addClass('pages_layout');
		}

		parent::attachView($this->content_width_container);

		//$this->addText('<div style="clear:both;"></div>');
		return parent::html();
	}
	
	
	/**
	 * Indicates if this section allows background settings to be shown
	 * @return bool
	 */
	public static function allowsBackgroundSettings() : bool
	{
		return TC_getConfig('page_builder','row_background_images');
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$content_model = $this->contentItem();
		
		$field = new TCv_FormItem_Select('column_format', 'Column Format');
		$field->setSaveToDatabase(false);
		$field->setDefaultValue($content_model->viewClass());
		$layout_list = TMm_PageLayoutList::init();
		$current_num_columns = ($content_model->viewClass())::numColumns();
		
		foreach($layout_list->pageLayouts() as $class_name => $path)
		{
			if($current_num_columns == $class_name::numColumns())
			{
				$field->addOption($class_name, $class_name::pageContent_ViewTitle());
				
			}
			
		}
		$form_items['column_format'] = $field;
		
		
		$responsive_collapse = new TCv_FormItem_Select('responsive_collapse', 'Responsive Collapse');
		$responsive_collapse->setHelpText('When turned on, columns will collapse into a single column view on smaller screens.');
		$responsive_collapse->addOption('1', 'YES - Columns collapse to a single column');
		$responsive_collapse->addOption('0', 'NO - Columns will maintain their ratios');
		$responsive_collapse->setDefaultValue($this->responsive_collapse ? 1 : 0);
		
		$form_items['responsive_collapse'] = $responsive_collapse;
		
		
		// Add option
		if(sizeof($this->content_blocks) == 2)
		{
			$field = new TCv_FormItem_Select('responsive_flip_order', 'Responsive Flip Order');
			$field->setHelpText('When turned on, columns that are collapsed into a single column will "flip" their order with the right column shown first.');
			$field->addOption('1', 'YES - Show the right column before the left column');
			$field->addOption('0', 'NO - Maintain the same order');
			$field->setDefaultValue($this->responsive_flip_order ? 1 : 0);
			$form_items['responsive_flip_order'] = $field;
		}
	
		// BACKGROUND IMAGE SETTINGS
		if(($content_model->viewClass())::allowsBackgroundSettings())
		{
			$file = new TCv_FormItem_FileDrop('background_image_file', 'Background Image');
			$file->addCSSClassForRow('background_field');
			
			// Ensure the upload folder matches that of the renderer
			$renderer_class_name = get_class($content_model->renderer());
			$file->setUploadFolder($renderer_class_name::uploadFolder());
			$file->setHelpText('An optional background image that will be loaded for this layout. ');
			$file->setUseCropper();
			$form_items['background_image_file'] = $file;
			
			
			$background_position = new TCv_FormItem_Select('background_position', 'Background position');
			$background_position->addCSSClassForRow('background_field');
			$background_position->addHelpText('Identify where the background image is "pinned". ');
			$background_position->setDefaultValue('center center');
			$background_position->addOption('left top', 'Top Left');
			$background_position->addOption('center top', 'Top Center');
			$background_position->addOption('right top', 'Top Right');
			$background_position->addOption('left center', 'Middle Left');
			$background_position->addOption('center center', 'Centered');
			$background_position->addOption('right center', 'Middle Right');
			$background_position->addOption('left bottom', 'Bottom Left');
			$background_position->addOption('center bottom', 'Bottom Center');
			$background_position->addOption('right bottom', 'Bottom Right');
			$form_items['background_position'] = $background_position;
			
			$background_size = new TCv_FormItem_Select('background_size', 'Background Size');
			$background_size->addCSSClassForRow('background_field');
			$background_size->addHelpText('Identify how the background is sized. ');
			$background_size->setDefaultValue('cover');
			$background_size->addOption('cover', 'Cover');
			$background_size->addOption('contain', 'Contain');
			$background_size->addOption('10%', '10%');
			$background_size->addOption('20%', '20%');
			$background_size->addOption('30%', '30%');
			$background_size->addOption('40%', '40%');
			$background_size->addOption('50%', '50%');
			$background_size->addOption('60%', '60%');
			$background_size->addOption('70%', '70%');
			$background_size->addOption('80%', '80%');
			$background_size->addOption('90%', '90%');
			$form_items['background_size'] = $background_size;
		}
	
		$equal_height_columns = new TCv_FormItem_Select('equal_height_columns', 'Equal height columns');
		$equal_height_columns->setHelpText('Indicate if the columns for this layout should be forced to be the same height.');
		$equal_height_columns->addOption('0', 'NO – Heights behave normally');
		$equal_height_columns->addOption('1', 'YES – Heights adjust to the tallest column');
		
		$form_items['equal_height_columns'] = $equal_height_columns;
		
		$extend_full_width = new TCv_FormItem_Select('extend_full_width', 'Extend Full Width');
		$extend_full_width->setHelpText('Indicate if columns exist within a container or extend to the full width.');
		$extend_full_width->addOption('0', 'NO - Columns exist within a defined width');
		$extend_full_width->addOption('1', 'YES - Both edges extend to the outer edge of the window');
		$extend_full_width->addOption('left', 'LEFT – Left edge extends to the full width of the window');
		$extend_full_width->addOption('right', 'RIGHT – Right edge extends to the full width of the window');
		$extend_full_width->setDefaultValue($this->extend_full_width ? 1 : 0);
		
		$form_items['extend_full_width'] = $extend_full_width;
			
		

		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string { return 'Content layout'; }
	public static function pageContent_IconCode() : string { return 'fa-archive'; }
	public static function pageContent_IsAddable() : bool { return false; }
	public static function pageContent_IsAContainer() : bool { return true; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A general content layout which is never actually used. All content layouts should extend this class.'; 
	}





}

?>