<?php
/**
 * Class TMv_PagesBreadcrumbHeading
 */
class TMv_PagesBreadcrumbHeading extends TCv_View
{
	protected ?string $separator = '';
	protected $url_sections = false;
	protected $viewed_menu = false;
	protected $rendered = false;
	protected $links = array();
	protected $use_model_titles = true;
	
	protected $starting_level = 1;
	
	protected ?int $homepage_id = null;
	
	public function __construct($viewed_menu = false)
	{
		parent::__construct();
		if($viewed_menu instanceof TMm_PagesMenuItem)
		{
			$this->viewed_menu = $viewed_menu;
		}
		$this->addClass('breadcrumbs');
		
		$this->setTag('nav');
		$this->setAttribute('aria-label','Breadcrumb');
	}
	
	/**
	 * Sets the viewed menu for the heading
	 * @param TMm_PagesMenuItem $viewed_menu
	 */
	public function setViewedMenu(TMm_PagesMenuItem $viewed_menu) : void
	{
		$this->viewed_menu = $viewed_menu;
	}
	
	/**
	 * Indicate the "level" of navigation to show. The first level starts at 1.
	 * @param int $level
	 */
	public function setStartingLevel(int $level) : void
	{
		$this->starting_level = $level;
		
	}
	
	/**
	 * Sets the separator that will be put between each breadcrumb.
	 * @param string $separator The string that separates each item
	 */
	public function setSeparator(string $separator) : void
	{
		$this->separator = $separator;
	}
	
	/**
	 * Creates a link view for a menu item and returns that view.
	 * @param TMm_PagesMenuItem $menu_item The menu item in which the view link view is needed
	 * @return TCv_View Returns the view for this breadcrumb, usually a TCv_Link but possibly overridden
	 */
	public function linkViewForMenuItem(TMm_PagesMenuItem $menu_item) : TCv_View
	{
		$link = new TCv_Link();
		$link->setURL($menu_item->viewURL());
		if($this->use_model_titles)
		{
			$link->addText($menu_item->titleOrModelTitle());
			
		}
		else
		{
			$link->addText($menu_item->title());
			
		}
		
		// Adds a suffix to the breadcrumbs
		$link->addText($menu_item->titleSuffix());
		return $link;
		
	}
	
	/**
	 * Replaces last link in the list with the link provided.
	 * @param TCv_link $link The new link to replace the last one in the series
	 */
	public function replaceLastLink(TCv_Link $link) : void
	{
		$this->render(); // render first
		array_pop($this->links);
		$this->addBreadcrumbLink($link);
	}
	
	/**
	 * Sets the breadcrumbs to include "Home" at the beginning with the URL "/".
	 */
	public function includeHomeAtStart() : void
	{
		$page_list = TMm_PagesMenuItemList::init();
		$homepage  = $page_list->homepageMenuItem();
		if($homepage)
		{
			$this->homepage_id = $homepage->id();
			
			if($this->viewed_menu && $this->homepage_id != $this->viewed_menu->id())
			{
				$breadcrumb_link = new TCv_Link();
				$breadcrumb_link->setURL('/');
				// Redundant title
				//$breadcrumb_link->setTitle($homepage->titleWithSuffix());
				$breadcrumb_link->addText($homepage->titleWithSuffix());
				$this->addBreadcrumbLink($breadcrumb_link);
				
			}
		}
	}
	
	
	/**
	 * Returns an array of breadcrumb menus. By default this is calculated by starting with the menu item being viewed and working back
	 * @return TMm_PagesMenuItem[]
	 */
	public function breadcrumbMenus() : array
	{
		$menus = array();
		if($this->viewed_menu && $this->viewed_menu->id() > 0)
		{
			
			$menu_item = $this->viewed_menu;
			while($menu_item instanceof TMm_PagesMenuItem)
			{
				$menus[] = $menu_item;
				
				if($menu_item->parentID() == 0)
				{
					$menu_item = false;
				}
				else
				{
					$menu_item = $menu_item->parent();
				}
			}
			krsort($menus);
		}
		
		return $menus;
		
	}
	
	/**
	 * Processes the provided menus, adding them to the breadcrumb
	 * @param TMm_PagesMenuItem[] $menus
	 *
	 * @uses TMv_PagesBreadcrumbHeading::addBreadcrumbLink()
	 * @uses TMv_PagesBreadcrumbHeading::linkViewForMenuItem()
	 */
	public function processMenus(array $menus) : void
	{
		foreach($menus as $menu)
		{
			// Avoid anything less than the starting level
			if($menu->level() < $this->starting_level)
			{
				continue;
			}
			
			if(is_string($menu))
			{
				$view = new TCv_View();
				$view->setTag('span');
				$view->addText($menu);
				$this->addBreadcrumbLink($view);
			}
			elseif($menu->title() != '' && $menu->title() != '[::]')
			{
				$link = $this->linkViewForMenuItem($menu);
				$this->addBreadcrumbLink($link);
			}
		}
	}
	
	/**
	 * Adds an item to the breadcrumb bar.
	 * @param string|TCv_Link|TCv_View $link This can be a string or a TCv_Link
	 */
	public function addBreadcrumbLink($link) : void
	{
		if($link instanceof TCv_View)
		{
			$link->addClass('breadcrumb');
		}
		$this->links[] = $link;
	}
	
	
	
	/**
	 *
	 * @param TMm_PagesContent $content_item
	 * @deprecated Replaced by setViewedMenu() or by passing the menu via the constructor
	 * @see TMv_PagesBreadcrumbHeading::setViewedMenu()
	 *
	 */
	public function setPagesContentItem(TMm_PagesContent $content_item) : void
	{
		$this->setViewedMenu($content_item->menuItem());
	}
	
	public function render() : void
	{
		if($this->rendered === false)
		{
			$this->rendered = true;
			
			if($this->viewed_menu)
			{
				
				$menus = $this->breadcrumbMenus();
				$this->processMenus($menus);
				
			}
			
			$num_links = count($this->links);
			$count = 1;
			foreach($this->links as $link)
			{
				// Not the first one, so add a separator
				if($count > 1)
				{
					$this->addSeparator();
				}
				
				if($link instanceof TCv_View)
				{
					// Last one isn't a link
					if($count == $num_links && $link instanceof TCv_Link)
					{
						$link->revertToSpan();
					}
					$this->attachView($link);
					
					
				}
				else
				{
					$this->addText($link);
				}
				$count++;
			}
		}
	}
	
	/**
	 * The HTML for the separator
	 * @return string
	 */
	public function separatorHTML() : string
	{
		return '<span class="separator" aria-hidden="true">'.$this->separator.'</span>';
	}
	
	/**
	 * Manually adds a separator to the view
	 * @return void
	 */
	public function addSeparator() : void
	{
		$this->addText($this->separatorHTML());
	}
	
	
}
?>