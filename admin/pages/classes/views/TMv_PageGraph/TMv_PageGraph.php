<?php

/**
 * This view is a widget, usable in both dashboards and regular page builders, which renders a graph of one of
 * several pre-made datasets. To add a new dataset to the list of datasets available to this class, you must
 * write a method that returns graphable data in some model or model list and then mark it as a graphable
 * method using the {@see TMt_DashboardModelViewable} trait. Check the trait's documentation for details.
 *
 * @see TMt_DashboardModelViewable
 */
class TMv_PageGraph extends TCv_Graph
{
	// Both traits are needed because this widget should be usable on both dashboards and regular pages, and
	// the traits affect what page builders a widget is available in.
	use TMt_DashboardView;
	use TMt_PagesContentView;

	protected
		$data_method, $model_id, $caption, $show_legend, $x_axis_title, $y_axis_title,
		$sparkline, $toolbar_options, $series_colors, $num_x_axis_labels, $page_graph_height;

	protected $color_palettes = [
		// first colour from all other colour arrays
		'all_colours' => ['#256BB2', '#7526D6', '#BF44AF', '#BE4242', '#CF7200', '#B4902D', '#0B8474'],
		// some other useful colour sets
		'green,_red,_and_yellow' => ['#0B8474', '#BE4242', '#B4902D'],
		// tungsten colours, from 0 to -3
		'blue' => ['#256BB2', '#2E86DE', '#7AB5FF', '#B6D6FF'],
		'purple' => ['#7526D6', '#9252E2', '#BA90FF', '#D5BFFF'],
		'pink' => ['#BF44AF', '#F251DC', '#FF9FF3', '#FFC5F8'],
		'red' => ['#BE4242', '#EE5253', '#FF8585', '#FFBABA'],
		'orange' => ['#CF7200', '#F8971D', '#FFBB6B', '#FFD6A6'],
		'yellow' => ['#B4902D', '#FFD343', '#FFE375', '#FFF0B8'],
		'green' => ['#0B8474', '#05A88C', '#38D9B6', '#A2F4DF'],
	];

	/**
	 * Constructor
	 * @param string $id The ID for the chart. Must be unique in order to load multiple on the same page.
	 */
	public function __construct(?string $id = null)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TMv_PageGraph');
	}
	
	/**
	 * Allow this view to be used on pages AND dashboards
	 * @return bool
	 */
	public static function isAlsoPageView() : bool
	{
		return true;
	}
	
	/**
	 * Manaully sets the data method
	 * @param string $data_method
	 * @return void
	 */
	public function setDataMethod(string $data_method)
	{
		$this->data_method = $data_method;
	}
	
	
	
	public function render()
	{
		// We ALWAYS need an id for the chart target div, so make absolutely certain one gets set.
		if (empty($this->id()))
		{
			$this->setID($this->content_item->idAttribute().'_graph');
		}


		try {

			$dataset = $this->validateDataset();
			$model_class = $dataset['model'];
			$method_name = $dataset['method'];
			$method_info = $dataset['info'];
			$model_id_list = explode(',', $this->model_id);
			if (empty($model_id_list))
			{
				$model_id_list = [0];
			}

			// Sometimes a graph is on a page meant to display a specific model. In those cases, the chosen
			// dataset must be a graphable method from the page model specifically, and we need to find an
			// active page model to query data from.
			$page_model_class = $this->getPageModelName();
			$page_model = NULL;
			if (!empty($page_model_class))
			{
				
				if ($model_class !== $page_model_class)
				{
					throw new Exception('Page model and dataset model mismatch.');
				}
				$page_model = TC_activeModelWithClassName($page_model_class);
				if ($page_model === false)
				{
					throw new Exception('Could not find page model.');
				}
			}

			$this->validateChartType($method_info, $model_id_list);
			$this->setChartType($this->chart_type);

			$this->processHeightValue();

			// Get data for graphs on pages with models
			if ($page_model !== NULL)
			{
				$series = call_user_func_array([$page_model, $method_name], []);
			}
			// Get data for graphs with a single model selected
			elseif (count($model_id_list) === 1)
			{
				$series = $this->callModelMethod($model_class, $model_id_list[0], $method_name);
			}
			// Get data for charts with multiple models selected. Each model's title will be prepended to all
			// of its series, to prevent duplicate series names.
			else
			{
				$series = [];
				foreach ($model_id_list as $model_id)
				{
					$new_data = $this->callModelMethod($model_class, $model_id, $method_name);

					$model_title = (new $model_class($model_id))->title();
					foreach($new_data as $series_name => $series_data)
					{
						$series[$model_title.' '.$series_name] = $series_data;
					}
				}
			}

			// Abort if an error occured above when trying to retrieve data.
			if ($this->hasClass('error'))
			{
				return;
			}

			// Add series to chart. Include series type so datasets that have mixed types work.
			foreach($series as $name => $data)
			{
				$current_type = $this->chart_type;

				// for mixed charts; include series type.
				if (isset($this->mixedType))
				{
					$series_index = count($this->series); // $this->series is from TCv_Graph
					$current_type = $method_info['series_types'][$series_index];
				}

				$this->addSeries($data, $name, $current_type);
			}

			if(!isset($this->config['colors']))
			{
				$this->setSeriesColors($this->color_palettes[$this->series_colors]);
				
			}
			
			// setSeriesColors doesn't affect boxplots, so we set their colours manually, based on the series colours
			$this->setBoxplotColours();


			// Misc setup

			if (!empty($method_info['x_value_type']))
			{
				$this->setXValueType($method_info['x_value_type']);
			}

			$this->setSparkline( ($this->sparkline === '1') );

			$tool_opts = explode(',', $this->toolbar_options);
			$this->setToolbar(
				in_array('zoom', $tool_opts), // zoom on x axis
				false, // NOTE: as of ApexCharts v3.33.1, y axis panning is bugged when x zoom is enabled; turning off for now
				in_array('zoom', $tool_opts), // click and drag to zoom to area
				in_array('download', $tool_opts)
			);

			$x_title = (!empty($method_info['x_axis_title']) ? $method_info['x_axis_title'] : $this->x_axis_title);
			$y_title = (!empty($method_info['y_axis_title']) ? $method_info['y_axis_title'] : $this->y_axis_title);

			$extra_config = [
				'legend' => [ 'show' => ($this->show_legend === '1') ],
				'xaxis' => [
					'tickAmount' => (is_numeric($this->num_x_axis_labels) ? (int)$this->num_x_axis_labels : null)
				],
			];
			
			if(!is_null($y_title))
			{
				$extra_config['yaxis'] = [ 'title' => [ 'text' => $y_title ] ];
			}
			
			if(!is_null($x_title))
			{
				$extra_config['xaxis'] = [ 'title' => [ 'text' => $x_title ] ];
			}
			
			//$extra_config['yaxis'] = ['title' => [ 'text' => $y_title ]];

			$this->updateConfig($extra_config);


			// Attach a div for the chart to target, send the configuration to the JS, etc
			parent::render();

			// Attach a description, if one was given.
			if(!empty($this->caption))
			{
				$caption = new TCv_View($this->id().'_caption');
				$caption->setTag('figcaption');
				$caption->addText($this->caption);
				$this->attachView($caption);
			}
		}
		// Any error that occurs gets added to the console as well as to the view
		catch (Throwable $e)
		{
			$this->addConsoleError($e->getMessage().'<br>at line '.$e->getLine().' in file '.$e->getFile());
			$this->addText('ERROR: '.$e->getMessage());
			$this->addClass('error');
		}
	}
	
	/**
	 * Processes the height of the figure to match the provided values. If they don't have one or choose auto, then
	 * it defaults to the golden ratio that already exists on graphs.
	 * @return void
	 */
	protected function processHeightValue() : void
	{
		$height = $this->page_graph_height;
		
		if(is_null($height) || $height == '')
		{
			// Do nothing, auto is already in place
		}
		else
		{
			// Chart height set to 100%
			// page graphs resize to the height of the container
			$this->setHeight('100%');
			
			// Set the height of the figure
			$css_line = '#'.$this->attributeID()." { height: ".$height.";}";
			$this->addCSSLine($this->attributeID().'_height', $css_line);
		}
		
		
	}

	/**
	 * Sets the colour for any boxplot series in the chart.
	 *
	 * Boxplot colouration is not affected by {@see setSeriesColors()}, so we manually set their colours; the
	 * lower quartile gets the colour it should have based on the list given to setSeriesColors, and the upper
	 * quartile gets a slightly lighter shade of that colour.
	 */
	protected function setBoxplotColours() : void
	{
		if ($this->chart_type === TCv_Graph::BoxplotType)
		{
			$boxplot_index = 0;
		}
		elseif ($this->chart_type === TCv_Graph::BoxplotScatterMixedType)
		{
			foreach ($this->series as $i => $s)
			{
				if ($s['type'] === TCv_Graph::BoxplotType)
				{
					$boxplot_index = ($i % count($this->color_palettes[$this->series_colors]));
					break;
				}
			}
		}
		if (isset($boxplot_index))
		{
			$lower_colour = $this->color_palettes[$this->series_colors][$boxplot_index];
			$upper_colour = $this->adjustColourLightness($lower_colour, 40);
			$this->updateConfig([
				'plotOptions' => [
					'boxPlot' => [ 'colors' => ['lower' => $lower_colour, 'upper' => $upper_colour] ]
				]
			]);
		}
	}

	/**
	 * Lightens or darkens a given hex colour by a given amount (-255 to 255).
	 * @param string $hex The colour to lighten.
	 * @param int $amount An integer from -255 to 255; the amount by which to lighten or darken the colour.
	 * @return string The lightened colour as a hex code.
	 */
	protected function adjustColourLightness(string $hex, int $amount) : string
	{
		$amount = max( -255, min(255, $amount) );

		// Normalize into a six character hex string.
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		}

		// Split into R, G and B
		$colour_parts = str_split($hex, 2);
		$result = '#';

		// Lighten/darken by converting each part to decimal, adding the amount to change, and converting back to hex.
		foreach ($colour_parts as $colour) {
			$colour = hexdec($colour);
			$colour = max( 0, min(255,$colour + $amount) );
			$result .= str_pad(dechex($colour), 2, '0', STR_PAD_LEFT);
		}

		return $result;
	}

	//////////////////////////////////////////////////////
	//
	// VALIDATION FUNCTIONS
	//
	//////////////////////////////////////////////////////

	/**
	 * Gets the name of the current page's model class, if it has one.
	 * @return string
	 */
	protected function getPageModelName() : string
	{
		$page_menu_item = TC_activeModelWithClassName(TMm_PagesMenuItem::class);
		if (!is_null($page_menu_item))
		{
			$class_name = $page_menu_item->modelClassName();
			return $class_name;
		}
		return '';
	}

	/**
	 * Validates the chosen dataset saved in $this->data_method on several metrics.
	 *
	 * To pass muster, the dataset must be a string with the format '<model>-<method>'; the named model has to
	 * exist and have the trait TMt_DashboardModelViewable; and the method has to exist and be marked as a
	 * graphableMethod.
	 *
	 * @return array The model class, method name, and method info provided by graphableMethods, in that order.
	 */
	protected function validateDataset() : array
	{
		// Validate that $graphable_method is in the expected format, <model>-<method>
		if (preg_match('/([A-Za-z0-9_]+)-([A-Za-z0-9_]+)/', $this->data_method, $matches) === false)
		{
			throw new Exception('Invalid dataset input format. Expected: "<model>-<method>".');
		}

		$model = $matches[1];
		$method = $matches[2];

		// Does the given class exist, and does it have TMt_DashboardModelViewable ?
		if (class_exists($model) === false)
		{
			throw new Exception("Class '{$model}' does not exist.");
		}
		if (TC_classUsesTrait($model, TMt_DashboardModelViewable::class) !== true)
		{
			throw new Exception("Class '{$model}' does not have the 'TMt_DashboardModelViewable' trait.");
		}

		// Does the method exist in the class, and is it marked as a graphableMethod?
		if (method_exists($model, $method) === false)
		{
			throw new Exception("Method '{$model}->{$method}()' does not exist.");
		}

		$graph_methods_info = $model::graphableMethods();
		if (array_key_exists($method, $graph_methods_info) === false)
		{
			throw new Exception("Method '{$model}->{$method}()' is not marked as a graphable method.");
		}


		return ['model' => $model, 'method' => $method, 'info' => $graph_methods_info[$method]];
	}

	/**
	 * Validates the chart type saved in $this->chart_type on several metrics.
	 *
	 * To pass muster, the chart type must match one of the class constants for chart type from TCv_Graph; if
	 * the method_info defines options 'restrict_chart_type' or 'num_dimensions', this type must match those
	 * restrictions; if this is a mixed type, then we must have a defined list of series_types in method_info;
	 * and if this is a single series chart type, we cannot expect to have multiple series later.
	 *
	 * @param array $method_info Information about a dataset pulled from an index of graphableMethods().
	 * Needed to check for restrictions on chart type defined by the dataset, etc.
	 * @param array $model_id_list A list of model id's we'll be pulling data from later. Needed to check
	 * restrictions on the number of series allowed in this chart type, etc.
	 */
	protected function validateChartType(array $method_info, array $model_id_list) : void
	{
		// Is this a valid chart type?
		if (!self::isStringChartType($this->chart_type))
		{
			throw new Exception("'{$this->chart_type}' is not a valid chart type.");
		}

		// Does the chart type fall in line with any chart type restrictions on this method?
		if ( isset($method_info['restrict_chart_type'])
			&& self::isStringChartType($method_info['restrict_chart_type'])
			&& $this->chart_type !== $method_info['restrict_chart_type'] )
		{
			throw new Exception("This dataset is restricted to '{$method_info['restrict_chart_type']}' "
				."chart type; '{$this->chart_type}' chart type given.");
		}
		elseif ( isset($method_info['num_dimensions'])
			&& array_key_exists($method_info['num_dimensions'], self::NumDimensionsInDatapoint)
			&& self::numDimensionsInChartType($this->chart_type) !== $method_info['num_dimensions'] )
		{
			$actual_num_dimensions = self::numDimensionsInChartType($this->chart_type);
			throw new Exception("This dataset is restricted to {$method_info['num_dimensions']}-dimensional "
				."chart types; a {$actual_num_dimensions}-dimensional type was given.");
		}

		// If this is a mixed chart type, we should have some series types defined in method_info.
		if (self::isStringMixedChartType($this->chart_type) && empty($method_info['series_types']))
		{
			throw new Exception('Datasets with a mixed chart type must define what type to use '
				.'for each series using the \'series_types\' option in graphableMethods.');
		}

		// Do we have multiple series in a single series chart type?
		if (self::isChartTypeSingleSeries($this->chart_type)
			&& ($method_info['is_multiple_series'] || count($model_id_list) > 1) )
		{
			$type_name = self::chartTypeName($this->chart_type);
			throw new Exception("{$type_name} charts are single series only; multiple series given.");
		}
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	public static function pageContent_ViewTitle() : string
	{
		return 'Graph';
	}

	public static function pageContent_ViewDescription() : string
	{
		return 'A graph of data.';
	}

	public static function pageContent_IconCode() : string
	{
		return 'fa-chart-bar';
	}
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		// Should not be visible in page builders because on pages that have models, this class will throw an
		// error in the page builder, because it cannot find a specific model to query for data.
		return false;
	}

	public function pageContent_EditorFormItems() : array
	{
		$form_items = [];

		$field = new TCv_FormItem_HTML('how_to', 'How to use this item');
		$field->addText(
			'This item presents a graph of data. Select a data set from the list of available sets on the '
			.'website. If you don’t see what you need, speak to a developer. (Developers: please refer to '
			.'the TMt_DashboardModelViewable trait to add new data sets.)'
		);
		$form_items[] = $field;

		if(TC_currentUser()->isAdmin())
		{
			// First, determine which models we're looking in for graphable methods. If we're on a page meant
			// for displaying a specific model, then we only allow datasets for that model; otherwise, we
			// offer every datasets from every model that has them.
			$page_model_name = $this->getPageModelName();
			$graphable_classes = [];
			if (!empty($page_model_name))
			{
				if (TC_classUsesTrait($page_model_name, TMt_DashboardModelViewable::class))
				{
					$graphable_classes = [$page_model_name];
				}
			}
			else
			{
				// Deal with Dashboards that are specific models
				/** @var TMm_Dashboard $active_dashboard */
				$active_dashboard = TC_activeModelWithClassName('TMm_Dashboard');
				$active_dashboard_model_name = $active_dashboard?->modelClassName();
				
				
				if(!is_null($active_dashboard_model_name))
				{
					$graphable_classes = [$active_dashboard_model_name];
					
					// Set the page module name, so that it uses the active model instead
					$page_model_name = $active_dashboard_model_name;
				}
				else
				{
					$graphable_classes = TC_modelsWithTrait(TMt_DashboardModelViewable::class);
					
				}
				
				
			}

			// Now, construct the dropdown of datasets the user can pick from for this graph.
			$field = new TCv_FormItem_Select('data_method', 'Data source');
			$field->setHelpText('Indicate which data source to graph');
			$field->setIsRequired();
			$field->addOption('', 'Select a data source');
			
			foreach ($graphable_classes as $model)
			{
				$methods = $model::graphableMethods();
				foreach($methods as $method_name => $method_attrs)
				{
					$is_multiple_series = isset($method_attrs['is_multiple_series']) && $method_attrs['is_multiple_series'];
					
					$option_val = $model.'-'.$method_name;
					$field->addOption(
						$option_val,
						$model::modelTitleSingular().' - '.$method_attrs['description']
							. ( $is_multiple_series ? ' (Multiple Series)' : '')
					);
					@$field->addOptionAttribute($option_val, 'data-num-dimensions', $method_attrs['num_dimensions']);
					@$field->addOptionAttribute($option_val, 'data-restrict-chart-type', $method_attrs['restrict_chart_type']);
					$field->addOptionAttribute($option_val, 'data-multi-series', $is_multiple_series ? 'true' : 'false');
					$field->addOptionAttribute($option_val, 'data-x-title', $method_attrs['x_axis_title']);
					$field->addOptionAttribute($option_val, 'data-y-title', $method_attrs['y_axis_title']);
				}
			}
			$field->useFiltering();
			$form_items[] = $field;

			
			// Some graphable methods require a model id.
			// NOTE: if this is for a page that has a model, this field will be disabled by the javascript.
			$field = new TCv_FormItem_Select('model_id', 'Models to chart');
			$field->setHelpText('Some data sets require you to pick one or more models to pull data from.');
			$field->useFiltering();
			$field->setUseMultiple(',');
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_Select('chart_type', 'Chart type');
			$field->setHelpText('Indicate what type of graph to render.');
			$field->setIsRequired();
			$field->addOption('', 'Select an option');
			$field->addClass('dataset_field');
			// Add options for simple types
			foreach (self::NumDimensionsInDatapoint as $num => $types) {
				$field->startOptionGroup("d{$num}", "{$num}-Dimensional Types");
				sort($types);
				foreach ($types as $type) {
					// Make a more human friendly type name before adding it to the list
					$type_name = self::chartTypeName($type)
						. (self::isChartTypeSingleSeries($type) ? ' (Single Series)' : '');
					$field->addOption($type, $type_name);
				}
				$field->endOptionGroup();
			}
			// Add options for mixed types
			$field->startOptionGroup('mixed', 'Mixed types');
			$field->addOption(self::LineBarAreaMixedType, self::chartTypeName(self::LineBarAreaMixedType));
			$field->addOption(self::ScatterLineMixedType, self::chartTypeName(self::ScatterLineMixedType));
			$field->addOption(self::CandlestickLineMixedType, self::chartTypeName(self::CandlestickLineMixedType));
			$field->addOption(self::BoxplotScatterMixedType, self::chartTypeName(self::BoxplotScatterMixedType));
			$field->endOptionGroup();
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_TextField('x_axis_title', 'X-Axis title');
			$field->setHelpText('A title for the chart\'s x axis, if it has axes.');
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_TextField('y_axis_title', 'Y-Axis title');
			$field->setHelpText('A title for the chart\'s y axis, if it has axes.');
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_Select('show_legend', 'Show legend');
			$field->setHelpText('Inidicates if the chart will have a legend, provided the Sparkline box is NOT checked.');
			$field->addOption('1','Yes - show the legend');
			$field->addOption('0','No - hide the legend');
			
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_Textfield('caption', 'Caption');
			$field->setHelpText('Provide a caption for the chart that describes the data.');
			$field->addClass('dataset_field');
			$field->setPlaceholderText('A brief caption about the data');
			$form_items[] = $field;


			$field = new TCv_FormItem_Heading('style_heading', 'Styling settings');
			$field->addClass('dataset_field');
			$form_items[] = $field;


			$field = new TCv_FormItem_Select('series_colors', 'Series colors');
			$field->setHelpText("Indicate which set of colours to use for the graph's series.");
			$field->addClass('dataset_field');
			foreach (array_keys($this->color_palettes) as $colour_set)
			{
			 	$field->addOption($colour_set, ucwords(str_replace('_', ' ', $colour_set)));
			}
			$form_items[] = $field;

			$field = new TCv_FormItem_TextField('num_x_axis_labels', 'Number of X-Axis labels');
			$field->setHelpText('Override the default number of x-axis labels for this chart. Note: Has no '
				.'effect on charts with datetime x values.');
			$field->setIsInteger();
			$field->addClass('dataset_field');
			$form_items[] = $field;
			
			$field = new TCv_FormItem_Select('page_graph_height', 'Height');
			$field->addOption('', 'Auto - Use golden ratio of 1.6');
			$field->addClass('dataset_field');
			for($val = 200; $val <= 600; $val += 50)
			{
				$field->addOption($val.'px', $val.'px');
				
			}
			
			$form_items[] = $field;
			
			
			$field = new TCv_FormItem_CheckboxList('toolbar_options', 'Toolbar options');
			$field->setHelpText('Enable a toolbar with zooming and/or chart download options.');
			$tool_opts = explode(',', $this->toolbar_options);
			$field->addCheckbox('zoom', 'Allow zooming', in_array('zoom', $tool_opts));
			$field->addCheckbox('download', 'Allow chart download', in_array('download', $tool_opts));
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_Checkbox('animate', 'Animations');
			$field->setHelpText('Enable or disable the animations that happen when the chart first loads.');
			$field->setDefaultValue('1');
			$field->addClass('dataset_field');
			$form_items[] = $field;

			$field = new TCv_FormItem_Checkbox('sparkline', 'Sparkline');
			$field->setHelpText('If checked, the chart has no UI; only the primary data paths are drawn.');
			$field->addClass('dataset_field');
			$form_items[] = $field;

			// Add the JS class to help manipulate and filter the values
			$params = [
				'is_editor' => true,
				'previous_model_class' => explode("-", $this->data_method)[0],
				'previous_models' => explode(',', $this->model_id),
				'page_has_model' => (!empty($page_model_name))
			];
			
			$this->addClassJSFile('TMv_PageGraph_PageEditorForm');
			$this->addClassJSInit('TMv_PageGraph_PageEditorForm', $params);
			
		}

		return $form_items;
	}


	//////////////////////////////////////////////////////
	//
	// URL TARGETS
	//
	//////////////////////////////////////////////////////

	/**
	 * For a URL target. Returns a list of models that can be graphed by the given graphable method.
	 * @see TMt_DashboardModelViewable
	 * @param string $graphable_model The model class in which the method resides
	 * @param string $graphable_method The graphable method to get a list of valid models for
	 * @return array An array of model id's and their human-readable names
	 */
	public static function graphableModels(string $graphable_model, string $graphable_method) : array
	{
		// Abort if no user logged in
		if (TC_currentUser() === false)
		{
			return [];
		}

		try
		{
			// interested in this for debugging; ie seeing if a particular filtering method takes a long time
			$before = microtime(true);

			// Does the class exist and does it use the trait TMt_DashboardModelViewable?
			if (isset($graphable_model) === false || empty($graphable_model))
			{
				throw new Exception("No graphable class name given.");
			}
			if (class_exists($graphable_model) === false)
			{
				throw new Exception("Class '{$graphable_model}' does not exist.");
			}
			if (TC_classUsesTrait($graphable_model, TMt_DashboardModelViewable::class) !== true)
			{
				throw new Exception("Class '{$graphable_model}' is not graphable. Does it have the 'TMt_DashboardModelViewable' trait?");
			}

			// Does the method exist and is it marked as a graphable method?
			if (isset($graphable_method) === false || empty($graphable_method))
			{
				throw new Exception("No method name given.");
			}
			if (method_exists($graphable_model, $graphable_method) === false)
			{
				throw new Exception("Method '{$graphable_model}->{$graphable_method}()' does not exist.");
			}

			$graphable_methods_info = $graphable_model::graphableMethods();

			if (array_key_exists($graphable_method, $graphable_methods_info) === false)
			{
				throw new Exception("Method '{$graphable_model}->{$graphable_method}()' is not graphable. Does "
					."the array in '{$graphable_model}::graphableMethods()' include an indice for this method?");
			}

			// ACTUAL WORK BEGINS

			// Does this model have an associated list class? If no, just return empty; it must not need models.
			$model_list_class = $graphable_model.'List';
			if (class_exists($model_list_class) === false)
			{
			//	$this->addConsoleMessage("No model list class '{$model_list_class}' found; returning empty model list
				//.");
				return ['models' => []];
			}

			$method_info = $graphable_methods_info[$graphable_method];

			// Use a filtering method if we have one for this graphable method - if not, just use the default 'models' method.
			$model_list_method = $method_info['model_list_method'] ?? 'models';

			if (method_exists($model_list_class, $model_list_method) === false)
			{
				throw new Exception("Method {$model_list_class}->{$model_list_method}() does not exist.");
			}

			$model_array = (new $model_list_class())->$model_list_method();

			// Abort if we don't actually have an array of models
			if ( is_array($model_array) === false )
			{
				throw new Exception("Model list returned by {$model_list_class}->{$model_list_method}() was "
					.'not an array; it was a "'.gettype($model_array).'".');
			}

			// Pare down the acquired list to just an id and a title; nothing more is needed.
			$result_array = [];
			foreach ($model_array as $model)
			{
				$result_array[] = [
					'id' => $model->id(),
					'title' => $model->title()
				];
			}

			// interested in this for debugging purposes; ie seeing if a particular filtering method takes a long time
			$after = microtime(true);
			$time_ms = ($after-$before)*1000;
			//$this->addConsoleDebug("Time to get model list for {$graphable_model}->{$graphable_method}() =
			// {$time_ms} ms");

			return [
				'runtime_in_milliseconds' => $time_ms,
				'models' => $result_array
			];
		}
		catch (Throwable $e)
		{
			
			$console_item =
				new TSm_ConsoleItem($e->getMessage().'<br>at line '.$e->getLine().' in file '.$e->getFile()
				, TSm_ConsoleMessage, true);
			$console_item->commit();
			
			//$this->addConsoleError();
			return ['error' => "Error finding models; see tungsten console."];
		}
	}

}
