class TMv_PageGraph_PageEditorForm
{
	element;

	options = {

	};
	constructor(element, params) {
		this.element = element;

		// The URL from which to request lists of models
		this.fetchUrl = '/admin/dashboard/do/models-for-graphable-method';

		// Whether or not the page this graph is going on has a model or not
		this.pageHasModel = params.page_has_model;

		// CONFIGURE DATASET FIELD
		this.datasetField = this.element.querySelector('select#data_method');
		this.configureField(this.datasetField, false);
		
		// CONFIGURE MODELS FIELD
		this.modelsField = this.element.querySelector('#model_id');
		this.configureField(this.modelsField, this.pageHasModel);

		this.modelsField.savedClass = params.previous_model_class;
		this.modelsField.savedModels = params.previous_models;

		// CONFIGURE CHARTY TPES FIELD
		this.chartTypesField = this.element.querySelector('#chart_type');
		this.configureField(this.chartTypesField, true);

		// CONFIGURE X AND Y FIELDs
		this.xTitleField = this.element.querySelector('#x_axis_title');
		this.configureField(this.xTitleField, true);

		this.yTitleField = this.element.querySelector('#y_axis_title');
		this.configureField(this.yTitleField, true);


		// The submit button of the form; so we can disable and enable it as we need to.
		this.submitButton = this.datasetField.form.querySelector('input[type="submit"], button[type="submit"]');

		// The index of the selected dataset; used to reset the dataset dropdown when errors occur loading new models.
		this.oldDataset = this.datasetField.value;

		// A list of option elements; used to reset the model dropdown when errors occur loading new models.
		this.oldModels = [];

		// Get all the fields AFTER
		this.nonDatasetRows = this.datasetField.closest('.form_table').querySelectorAll(".dataset_field");

		if (this.pageHasModel)
		{
			let option = document.createElement("option");
			option.text = "Auto-detected page model";
			option.value = 0;
			option.selected = true;
			this.modelsField.add(option);
		}

		// If we have a saved dataset, all form fields stay as they are.
		// If we DON'T have a saved dataset, all form rows aside from dataset start hidden.
		// if (this.dataset.selectedOption().value === '') {
		// 	for (let i = 0; i < this.nonDatasetRows.length; i++) {
		// 		this.nonDatasetRows[i].style.display = "none";
		// 	}
		// }

		// Add event handlers to all form items that need them.
		this.datasetField.addEventListener('change', (e) => this.dataSetChanged(e));
		//this.modelsField.addEventListener('change', (e) => this.restrictNumberofModels());


		// Trigger the first change, so if we're editing an existing graph all the right options appear.
		// Wait a second to ensure models load, otherwise we have a race condition
		setTimeout(() => this.dataSetChanged(), 1000);

	}

	/**
	 * Event handler for dataset dropdown; loads a list of models into the model dropdown if the chosen
	 * dataset requires specific models to be chosen, or clears the models dropdown if the dataset doesn't
	 * require any.
	 */
	dataSetChanged(event) {
		// If empty dataset, just hide everything and we're done.
		if (this.datasetField.value === "")
		{
			this.nonDatasetRows.forEach(el => el.hide());
			return;
		}

		// If the current page has a model, we're autodetecting which model ID gets the graph. So, show/hide
		// form elements as necessary for the dataset and return without doing an AJAX call.
		if (this.pageHasModel) {
			this.nonDatasetRows.forEach(el => el.show());
			this.showHideFormElements();
			return;
		}

		//document.getElementById('model_id').tom_select.setMaxItems(1)

		// Dea with multiple value series
		if (this.datasetField.selectedDatasetValues().multiSeries === "true")
		{
			this.modelsField.tom_select.setMaxItems(null);
		}
		else
		{
			this.modelsField.tom_select.setMaxItems(1);
		}
		this.modelsField.tom_select.sync();


		// If the current page does NOT have a model...

		// Disable the submit button, dataset dropdown, and model dropdown until we finish changing stuff
		this.submitButton.disabled = true;
		this.datasetField.disabled = true;
		this.modelsField.disabled = true;


		let url_params = new URLSearchParams();

		// Dataset option values are structured "Model-methodname"
		let chosen_dataset = this.datasetField.value.split('-');
		url_params.append('model',chosen_dataset[0]);
		url_params.append('method',chosen_dataset[1]);


		// Delete all options and tell user we're trying to find new ones for them
		while (this.modelsField.options.length > 0) {
			this.oldModels.push(this.modelsField.options[0].cloneNode(true));
	    	this.modelsField.remove(0);
		}
		this.modelsField.tom_select.sync();

		// Attempt to find new list of models for the user to pick from and update form as necessary
		const opts = { method: 'GET', headers: {} };
		fetch(this.fetchUrl + "?" + url_params, opts)
			.then(response => response.json())
			.then(response => {

				// Check for errors
				if (response.error !== undefined)
				{
					this.mutateFormErrorHandler(response.error);
					return;
				}

				// Add all models to the dropdown
				for (let i = 0; i < response.models.length; i++) {
					let option = document.createElement("option");
					option.text = response.models[i].title;
					option.value = response.models[i].id;
					if (this.modelsField.savedClass === chosen_dataset[0]
						&& this.modelsField.savedModels.includes(option.value))
					{
						option.selected = true;
					}
					this.modelsField.add(option);
				}
			})
			// Error handling
			.catch(this.mutateFormErrorHandler.bind(this))
			// Re-enable the form items we disabled earlier once we're done
			.finally(() => {
				// Update the old dataset index for next time, clear the old models
				this.oldDataset = this.datasetField.value;
				this.oldModels = [];

				// Modify the form as necessary for the current dataset
				this.nonDatasetRows.forEach(el => el.show());

				this.showHideFormElements();

				// Re-enable and update the dataset and model dropdowns
				this.modelsField.disabled = false;
				this.datasetField.disabled = false;
				this.modelsField.tom_select.sync();
				this.datasetField.tom_select.sync();

				// Re-enable the submit button
				this.submitButton.disabled = false;
			});
	}

	/**
	 * To be used in case of errors when loading models. Outputs whatever the error object was to the console,
	 * alerts the user that something went wrong, and attempts to revert the dataset and model dropdowns to
	 * their state prior to the error occurring.
	 * @param error An error or other object with information about what happened. Will be printed to console.
	 */
	mutateFormErrorHandler(error) {
		// Alert user to the fact that something went wrong
		console.log("ERROR:", error);
		alert("ERROR: Could not load model list for dataset \"" + this.datasetField.selectedOption().text
			+ "\". See developer console for details.");

		// Change the dataset back to the old one and re-add the old models
		this.datasetField.value = this.oldDataset;
		while (this.modelsField.options.length > 0) {
			this.modelsField.remove(0);
		}
		while (this.oldModels.length > 0) {
			this.modelsField.add(this.oldModels.shift());
		}
	}


	/**
	 * Decides whether form fields aside from the dataset field should be hidden, shown, or locked to a value
	 * provided by the dataset, based on the currently selected dataset.
	 */
	showHideFormElements() {
		const dataAttrs = this.datasetField.selectedDatasetValues();
		const requiredType = dataAttrs.restrictChartType;
		const numDimensions = dataAttrs.numDimensions;
		const restrictTypeIsSet = (requiredType !== "" && requiredType != null);
		const numDimensionsIsSet = (numDimensions !== "" && numDimensions !== "0" && numDimensions != null);

		if(this.pageHasModel || this.modelsField.options.length === 0)
		{
			this.modelsField.handleHide();
		}
		else
		{
			this.modelsField.handleShow();
		}

		// Deal with titles
		if(dataAttrs.xTitle !== '')
		{
			this.xTitleField.setValueAndHide(dataAttrs.xTitle);
		}
		else
		{
			this.xTitleField.handleShow();
		}

		if(dataAttrs.yTitle !== '')
		{
			this.yTitleField.setValueAndHide(dataAttrs.yTitle)
		}
		else
		{
			this.yTitleField.handleShow();
		}

		// CHART TYPE DROPDOWN
		// Loop thru options, enabling/disabling them according to the rules of num-dimensions and restrict-chart-type.
		for (let i = 1; i < this.chartTypesField.length; i++) {
			// disable everything except the required type
			if (restrictTypeIsSet) {
				this.chartTypesField[i].disabled = (requiredType !== this.chartTypesField[i].value);
				// Automatically set the required chart type as the chart type
				if (this.chartTypesField[i].disabled === false)
					this.chartTypesField.selectedIndex = i;
			}
			// disable everything except types under the option group for the dimensions we're restricted to
			else if (numDimensionsIsSet) {
				let optGroup = this.chartTypesField[i].parentNode;
				this.chartTypesField[i].disabled = ("d"+numDimensions !== optGroup.id);
			}
			// under normal circumstances, disable mixed charts, and enable everything else
			else {
				this.chartTypesField[i].disabled = (this.chartTypesField[i].parentNode.id === "mixed");
			}
		}
		// If current selection is now disabled, un-select it.
		if (this.chartTypesField.selectedOption().disabled === true) {
			this.chartTypesField.selectedIndex = 0;
		}
		// Hide the field if the chart type was restricted, show otherwise
		(restrictTypeIsSet) ? this.chartTypesField.handleHide() : this.chartTypesField.handleShow();

	}

	/**
	 * Appends functionality to the select field to accommodate changes in the interface
	 * @param {HTMLElement} field THe  field that is being modified
	 * @param {Boolean} replaceTextOnHide Indicates if this field is replaced with text when hidden
	 */
	configureField(field, replaceTextOnHide) {

		// Add functions related to select boxes
		if(field.tagName === 'SELECT')
		{
			// Add functionality to the dataSet field to get the selected option values
			field.selectedDatasetValues = function ()
			{
				return this.selectedOption().dataset;
			};

			field.selectedOption = function ()
			{
				return this.options[this.selectedIndex];
			};

		}

		field.handleShow = function()
		{
			// No replacement, show the row
			if (this.replacement === undefined)
			{
						this.closest('.table_row').show();
			}
			else
			{
				this.value = '';
				this.show(); // show the field
				this.replacement.hide(); // hide the replacement


			}
		};

		field.handleHide = function() {
			if (this.replacement === undefined)
			{
				this.closest('.table_row').hide();
			}
			else
			{
				if(field.tagName === 'SELECT')
				{
					this.replacement.innerHTML = this.selectedOption().text;
				}
				else
				{
					this.replacement.innerHTML = this.value;
				}

				if(this.classList.contains('tomselected'))
				{
					let parent = this.parentElement;
					parent.querySelector('.ts-wrapper').hide();

				}


				this.hide();
				// if (this.selectedOption !== undefined)
				// {
				// 	this.closest('.table_row').slideUp();
				// }

				this.replacement.show();
			}
		};

		field.setValueAndHide = function(val) {
			this.value = val;
			//(this.selectedOption !== undefined) ? this.selectedIndex = val : this.value = val;
			this.handleHide();
		}


		if (replaceTextOnHide === true) {
			field.replacement = document.createElement("div");
			field.replacement.classList.add('replacement');

			field.closest(".form_item_container").appendChild(field.replacement);
		}

	}

}
