<?php
class TMv_YouTubeEmbedded extends TCv_View
{
	use TMt_PagesContentView;
	
	protected string $url = '';
	protected string $title = '';
	protected bool $enable_third_party_cookies = true;
	
	/**
	 * TMv_YouTubeEmbedded constructor.
	 */
	public function __construct()
	{
		
		parent::__construct();
		
	}
	
	/**
	 * @return bool
	 */
	public function youTubeCode()
	{
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->url, $match))
		{
			return $match[1];
		}
		
		return false;
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		$this->addClass('responsive_iframe');
		
		$code = $this->youTubeCode();
		$video_tag = new TCv_View();
		$video_tag->setTag('iframe');
		
		$video_tag->setAttribute('title', $this->title);
		$video_tag->setAttribute('width', 600);
		$video_tag->setAttribute('height', 340);
		$video_tag->setAttribute('frameborder', '0');
		$video_tag->setAttribute('webkitAllowFullScreen', 'webkitAllowFullScreen');
		$video_tag->setAttribute('mozallowfullscreen', 'mozallowfullscreen');
		$video_tag->setAttribute('allowFullScreen', 'allowFullScreen');
		
		if($this->enable_third_party_cookies)
		{
			$url = 'https://www.youtube.com/embed/' . htmlspecialchars($code);
		}
		else
		{
			$url = 'https://www.youtube-nocookie.com/embed/' . htmlspecialchars($code);
			
		}
		
		if(strpos($url, '?') > 0)
		{
			$url .= '&rel=0';
		}
		else
		{
			$url .= '?rel=0';
		}
		
		$video_tag->setAttribute('src', $url);
		
		$this->attachView($video_tag);
		
		return parent::html();
	}
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$field = new TCv_FormItem_TextField('url', 'YouTube URL');
		$field->setDefaultValue($this->url);
		$field->setHelpText('The code from the You Tube URL. ');
		$form_items[] = $field;
		
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setDefaultValue($this->title);
		$field->setHelpText('The title for this video');
		$form_items[] = $field;
		
		$field = new TCv_FormItem_Select('enable_third_party_cookies', 'Enable third-party cookies');
		$field->setHelpText('Check this if you want to allow third-party cookies, which should be done if you use YouTube tracking for metrics');
		$field->addOption('1', 'YES - enable third party cookies for this video');
		$field->addOption('0', 'NO - disable third party cookies for this video');
		$form_items[] = $field;
		
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string  { return 'YouTube video'; }
	public static function pageContent_IconCode() : string  { return 'fab fa-youtube'; }
	public static function pageContent_ViewDescription() : string
	{
		return 'An embedded YouTube video.';
	}
	
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return true;
	}
	
}

?>