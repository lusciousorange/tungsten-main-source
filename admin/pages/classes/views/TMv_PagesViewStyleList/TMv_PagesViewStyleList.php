<?php
class TMv_PagesViewStyleList extends TCv_ModelList
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_PagesViewStyle');
		$this->defineColumns();
	}
	
	/**
	 * Defines the columns for the viuew
	 */
	public function defineColumns()
	{
		
		$title = new TCv_ListColumn('title');
		//$title->setWidthAsPercentage(30);
		$title->setTitle('Title');
		$title->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($title);

		$title = new TCv_ListColumn('class_name');
		$title->setWidthAsPercentage(20);
		$title->setTitle('Class name');
		$title->setContentUsingModelMethod('className');
		$this->addTCListColumn($title);

		$title = new TCv_ListColumn('required_classes');
		$title->setWidthAsPercentage(20);
		$title->setTitle('Required');
		$title->setContentUsingListMethod('requiredViewColumn');
		$this->addTCListColumn($title);

		/*
				$code = new TCv_ListColumn('code');
				$code->setTitle('Code');
				$code->setContentUsingModelMethod('code');
				$this->addTCListColumn($code);

		*/
		
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
				
	}
	
	
	/**
	 * @param TMm_PagesViewStyle $model
	 * @return bool|TCv_View
	 */
	public function titleColumn($model)
	{
		$column = new TCv_View();
		
		$url_target = TC_URLTargetFromModule( 'pages' , 'css-style-edit');
		$url_target->setModel($model);
		$link = TSv_ModuleURLTargetLink::init($url_target);
		$column->attachView($link);
		
		$column->addText('<br/><em>'.$model->description().'</em>');
		return $column;
		
	}
	
	/**
	 * @param TMm_PagesViewStyle $model
	 * @return bool|TCv_View
	 */
	public function requiredViewColumn($model)
	{
		$required_class_string = trim($model->requiredViewClasses());
	
		$required_classes = explode(' ',$required_class_string);
		
		return implode('<br />', $required_classes);

		
	}
	
	
	/**
	 * @param TMm_PagesViewStyle $model
	 * @return bool|TCv_View
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'css-style-edit', 'fa-pencil');
		
	}
	
	/**
	 * @param TMm_PagesViewStyle $model
	 * @return bool|TCv_View
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'css-style-delete', 'fa-trash');
	
	}
	
	/**
	 * Returns the help view
	 * @return bool|TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('This is a list of settings that your web developer has setup. In most cases these values don\'t change, however this mechanism provides a convenient way to access and change these items if necessary. This provides long-term flexibility to ensure all the content on this website is manageable.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Create New Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('New Template Setting', '.url-target-template-setting-create');
				$p->attachView($link);
			$p->addText(' button to add a new setting. This will not add any content to your website, but instead enable your developer to use this content. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Edit Existing Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('Setting Title', '.TCv_ListColumn_title .url-target-template-setting-edit');
				$p->attachView($link);
			$p->addText(' or ');
				$link = new TSv_HelpLink('', '.url-target-template-setting-edit.list_control_button');
				$link->addClass('fa-pencil');
				$p->attachView($link);
			$p->addText(' to edit the values for a setting. Please note that if you change the "code", you may break existing functionally on your website.');
			$help_view->attachView($p);
			
						
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Delete Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('', '.url-target-template-setting-delete');
				$link->addClass('fa-trash');
				$p->attachView($link);
				
			$p->addText(' button. This is not recommended without speaking to a devloper. If you no longer want a piece of content to appear, you can edit the setting and delete the value.');
			$help_view->attachView($p);
			
		return $help_view;
	}
		
}
?>