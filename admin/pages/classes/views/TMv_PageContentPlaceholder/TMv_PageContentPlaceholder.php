<?php

/**
 * Class TMv_PageContentPlaceholder
 *
 * A placeholder view that allows a developer to setup a classname that doesn't yet exist in the system.
 */
class TMv_PageContentPlaceholder extends TCv_View
{
	use TMt_PagesContentView;


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		$editor = new TCv_FormItem_TextField('placeholder_class_name', 'Class Name');
		$editor->setHelpText('The name of the PHP class that will eventually be created. Once in the system, this view will generate that view.');
		$editor->setIsRequired();
		$form_items[] = $editor;


		return $form_items;
	}

	public static function pageContent_ViewTitle() : string  { return 'Developer placeholder'; }
	public static function pageContent_IconCode() : string  { return 'fa-question-circle'; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }

	public static function pageContent_ViewDescription() : string
	{
		return 'A placeholder that allows developers to block out a layout without having the actual classes in place.';
	}
}