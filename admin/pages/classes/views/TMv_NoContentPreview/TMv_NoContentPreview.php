<?php

/**
 * Class TMv_NoContentPreview
 *
 * An "empty" view that shows a content item when no preview is available.
 */
class TMv_NoContentPreview extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $html_content = '<p></p>';
	
	protected $content_model;
	
	// Values that exist for other page content views
	protected $content_item;
	
	/**
	 * TMv_NoContentPreview constructor.
	 * @param   $content_model
	 */
	public function __construct($content_model)
	{
		parent::__construct();
		
		$this->setPagesContentItem($content_model);
		
		$this->content_model = $content_model;
		
		
		$this->addClassCSSFile('TMv_NoContentPreview');
	}
	
	public function render()
	{
		parent::render();
		
		$this->addClass($this->content_model->cssClasses());
		
		
		// Set basic properties, some are historic or used in page builder
		$this->addClass('page_content_item');
		$this->addClass('missing_content');
		$this->setAttribute('data-content-id', $this->content_model->id());
		
		/** @var string|TMt_PagesContentView $view_class_name */
		$view_class_name = $this->content_model->viewClass();
		
		// View class doesn't exist
		if(!class_exists($view_class_name))
		{
			$icon_tag = new TCv_View();
			$icon_tag->setTag('i');
			$icon_tag->addClass('fa-question-circle');
			$this->attachView($icon_tag);
			
			$this->addText('<span>'.$view_class_name.'</span>');
			$this->addText('<p class="description">Placeholder view</p>');
			
		}
		else
		{
			$icon_tag = new TCv_View();
			$icon_tag->setTag('i');
			$icon_tag->addClass($view_class_name::pageContent_IconCode());
			$this->attachView($icon_tag);
			
			
			$this->addText('<span>' . $view_class_name::pageContent_ViewTitle() . '</span>');
			$this->addText('<p class="description">' . $view_class_name::pageContent_ViewDescription() . '</p>');
			
			$preview = $view_class_name::pageContent_BuilderPreviewContent($this->content_model);
			if($preview instanceof TCv_View)
			{
				$preview->addClass('no_content_preview');
				$this->attachView($preview);
			}
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Saves the variables for the content item
	 * @param TMt_PageRenderItem $content_item
	 */
	protected function saveVariablesForContentItem($content_item) : void
	{
		// Do nothing. Don't save the variables
	}
	
	public static function pageContent_IsAddable() : bool { return false; }
	
	
}

?>