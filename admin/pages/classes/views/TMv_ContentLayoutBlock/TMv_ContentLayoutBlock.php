<?php
/**
 * Class TMv_ContentLayoutBlock
 */
class TMv_ContentLayoutBlock extends TCv_View
{
	use TMt_PagesContentView;
	protected $container ;
	protected $background_position = "center center";
	protected $background_size = 'cover';
	
	// FILTERS
	protected $filters = '';


	/**
	 * TMv_ContentLayoutBlock constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->container = new TCv_View();
		$this->container->addClass('layout_block_container');
		$this->addClass('pages_block');
	}
	
	/**
	 * Returns the content container for the provided view
	 * @param TCv_View $view
	 * @return TCv_View
	 */
	public function containerForContentView($view)
	{
		$id = false;
		if($view instanceof TCv_View)
		{
			$id = $view->id();

			if($id != '')
			{
				$id .= '_container';
			}

		}

		$content_container = new TCv_View($id);
		$content_container->addClass('content_container');

		if($view instanceof TCv_View)
		{
			$content_container->addClass(get_class($view) . '_container');
			
			if($view->hasClass('page_content_item'))
			{
				if(method_exists($view, 'contentItem'))
				{
					$content_item = $view->contentItem();
					// Loop through the view styles, apply them to the element, if appropriate
					foreach($content_item->cssPageViewStyles() as $page_view_style)
					{
						if($page_view_style->applyToContentContainer())
						{
							$content_container->addClass($page_view_style->className());
						}
					}
				}
			}
			
		}

		return $content_container;
	}
	

	/**
	 * Override the attachView function to attach these items to the content container. This is the default in order
	 * to consistently use layouts properly.
	 * @param bool|TCv_View|TMt_PagesContentView $view
	 * @param bool $prepend
	 * @return void
	 */
	public function attachView($view, $prepend = false)
	{
		// Handle empty views, which should not trigger errors
		if(! $view instanceof TCv_View)
		{
			return;
		}
		
		$content_container = $this->containerForContentView($view);
		$content_container->attachView($view);
		
		// Handle control panel
		if(method_exists($view, 'hasControlPanelView') && $view->hasControlPanelView())
		{
			$content_container->attachView($view->controlPanelView());
		}
		$this->container->attachView($content_container, $prepend);
		
		
	}
	
	/**
	 * Override the attachView function to attach these items to the content container. This is the default in order
	 * to consistently use layouts properly.
	 * @param bool|TCv_View|TMt_PagesContentView $view
	 * @param bool $prepend
	 * @return void
	 */
	public function attachViewWithoutContentContainer($view, $prepend = false)
	{
		$this->container->attachView($view, $prepend);
	}
	
	/**
	 * Attaches the view to the root of this view instead of to the content_width_container which is the default interaction with attachView()
	 * @param TCv_View $view The view to be attached
	 * @param bool $prepend (Optional) Default false
	 * @see TMv_ContentLayout::attachView()
	 */
	public function attachViewToRoot($view, $prepend = false)
	{
		parent::attachView($view, $prepend);
		
		
	}
	
	
	/**
	 * Adds the empty placeholder view to this block.
	 * @return void
	 */
	public function attachEmptyPlaceholder()
	{
		$empty_placeholder = new TCv_View();
		$empty_placeholder->addClass('empty_placeholder');
		$this->container->attachView($empty_placeholder);
	}
	

	/**
	 * Returns the HTML
	 * @return string
	 */
	public function html()
	{
		
		// Handle control panel
		// Added here so that it's appended to the end of the container
		if(method_exists($this, 'hasControlPanelView') && $this->hasControlPanelView())
		{
			$this->container->attachView($this->controlPanelView());
		}
		
		
		parent::attachView($this->container);

		$this->applyBackgroundImage();
		
		return parent::html();
	}
	
	/**
	 * Indicates if this section allows background settings to be shown
	 * @return bool
	 */
	public static function allowsBackgroundSettings() : bool
	{
		return TC_getConfig('page_builder','column_background_images');
	}
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		if(static::allowsBackgroundSettings())
		{
			
			$file = new TCv_FormItem_FileDrop('background_image_file', 'Background Image');
			
			// Ensure the upload folder matches that of the renderer
			$content_model = $this->contentItem();
			$renderer_class_name = get_class($content_model->renderer());
			$file->setUploadFolder($renderer_class_name::uploadFolder());
			
			$file->setHelpText('An optional background image that will be loaded for this layout block. ');
			$file->setUseCropper();
			$form_items[] = $file;
			
			if($filter_options = TC_getConfig('page_builder','background_image_filtering'))
			{
				if(is_array($filter_options) && count($filter_options) > 0)
				{
					$field = new TCv_FormItem_Select('filters', 'Background Filters');
					$field->addCSSClassForRow('background_field');
					$field->setUseMultiple(',');
					$field->useFiltering();
					foreach($filter_options as $name => $settings)
					{
						$field->addOption($name, $settings['name']);
					}
					$form_items['background_filters'] = $field;
					
				}
				
			}
			
			
			$background_position = new TCv_FormItem_Select('background_position', 'Background Position');
			$background_position->addHelpText('Identify where the background image is "pinned". ');
			$background_position->setDefaultValue('center center');
			$background_position->addOption('left top', 'Top Left');
			$background_position->addOption('center top', 'Top Center');
			$background_position->addOption('right top', 'Top Right');
			$background_position->addOption('left center', 'Middle Left');
			$background_position->addOption('center center', 'Centered');
			$background_position->addOption('right center', 'Middle Right');
			$background_position->addOption('left bottom', 'Bottom Left');
			$background_position->addOption('center bottom', 'Bottom Center');
			$background_position->addOption('right bottom', 'Bottom Right');
			$form_items[] = $background_position;
			
			$background_size = new TCv_FormItem_Select('background_size', 'Background Size');
			$background_size->addHelpText('Identify how the background is sized. ');
			$background_size->setDefaultValue('cover');
			$background_size->addOption('cover', 'Cover');
			$background_size->addOption('contain', 'Contain');
			$background_size->addOption('10%', '10%');
			$background_size->addOption('20%', '20%');
			$background_size->addOption('30%', '30%');
			$background_size->addOption('40%', '40%');
			$background_size->addOption('50%', '50%');
			$background_size->addOption('60%', '60%');
			$background_size->addOption('70%', '70%');
			$background_size->addOption('80%', '80%');
			$background_size->addOption('90%', '90%');
			$form_items[] = $background_size;
		}


		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string { return 'Content column'; }
	public static function pageContent_IconCode() : string { return 'fa-archive'; }
	public static function pageContent_IsAddable() : bool { return false; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A content container which resides within a column of a layout.'; 
	}
	
	
	
}

?>