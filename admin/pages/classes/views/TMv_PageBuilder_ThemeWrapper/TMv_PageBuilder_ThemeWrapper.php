<?php

/**
 * Class TMv_PageBuilder_ThemeWrapper
 *
 * This class is designed to provide a structure to ensure consistency between the main style and the page builder.
 * It functions by loading the page builder within the wrapper and it can then be extended to apply theme specific
 * items in the page builder to improve the overall experience.
 */
class TMv_PageBuilder_ThemeWrapper extends TCv_View
{
	protected $renderer_item;
	
	
	protected ?TMv_PageBuilder $page_builder = null;
	
	/**
	 * @var class-string The name of the theme that is being loaded
	 */
	protected string $theme_name = 'TMv_Theme_Plain';
	protected ?TMv_PagesTheme $theme_view = null;
	
	// Extendable to set other class names
	protected string $page_builder_class_name = 'TMv_PageBuilder';
	
	protected bool $is_iframe = false;
	
	protected ?TCv_View $theme_view_container = null;
	
	/**
	 * @var null|TMm_PagesMenuItem
	 * The page menu item that should be used for the frame and trim of the page builder. This is used with page
	 * builders on items that aren't `TMm_PagesMenuItem` to show the full layout while still having the "editor' be
	 * for that item.
	 */
	protected $frame_menu_item = null;
	
	/**
	 * TMv_PageBuilder constructor.
	 * @param TMt_PageRenderer $renderer_item The menu item that we're generating a page builder for
	 * @param TMt_PageRenderer $theme_renderer_item The page render (likely a TMm_PagesMenuItem that is used as the
	 * theme renderer. This is applied when the theme should be different than what's loaded.
	 */
	public function __construct($renderer_item, $theme_renderer_item = null )
	{
		$this->renderer_item = $renderer_item;
		parent::__construct();
		
		$this->addClassCSSFile('TMv_PageBuilder_ThemeWrapper');
		
		// Instantiate the theme view container, which contains all the items
		$this->theme_view_container = new TCv_View();
		$this->theme_view_container->addClass('theme_view_container');
		
		
		// If we're in Tungsten 8 OR we have a return type of fullpage
		if(
			!TC_getConfig('use_tungsten_9')
			|| (isset($_GET['return_type']) && $_GET['return_type'] == 'fullpage') )
		{
			// Add the base, so that clicks go to the parent
			if(TC_getConfig('use_tungsten_9'))
			{
				$this->addMetaTag('base', '<base target="_top">');
			}
		
			$this->page_builder = ($this->page_builder_class_name)::init($this->renderer_item);
			
			$this->frame_menu_item = $theme_renderer_item;
			if(!$this->frame_menu_item)
			{
				$this->frame_menu_item = $renderer_item;
			}
			$this->findThemeView();
		}
		else // LOAD THE IFRAME
		{
			$this->loadIFrame();
		
		}
		
		$this->addClass('editor_mode');
		
		if($this->renderer_item->hasUnpublishedContent())
		{
			$this->addClass('has_unpublished');
		}
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// FRAME and THEME
	//
	//////////////////////////////////////////////////////
	
	
	protected function loadIFrame()
	{
		$this->setTag('iframe');
		
		// Get the renderer item since this needs to deal with all `TMt_PageRenderers`
		
		if($current_url_target = TC_currentURLTarget())
		{
			$module_name = $current_url_target->module()->folder();
			$url_target_name = $current_url_target->name();
			
			$this->setAttribute('src',
			                    '/admin/'.$module_name.'/do/'.$url_target_name.'/'.$this->renderer_item->id().'?return_type=fullpage');
			$this->is_iframe = true;
			
		}
		else
		{
			TC_triggerError('Error loading page builder. Refresh or try hitting the back button');
		}
	}
	
	/**
	 * Finds the theme view for the wrapper.
	 */
	protected function findThemeView()
	{
		$pages_module = TMm_PagesModule::init();
		$this->theme_name = $pages_module->editorThemeName();
		
		// Adds the styles from the theme view that we're using
		// Pulls in external fonts, etc
		$this->theme_view = ($this->theme_name)::init($this->frame_menu_item);
		$this->theme_view->hook_updatePageBuilder($this->page_builder);
		$this->theme_view->setViewedMenu($this->frame_menu_item); // backwards compatibility
		
		// Look for page specific CSS
		// These are referenced by menu ID in the theme folder in a subfolder that must be called `css_pages`
		// The file must be called `page_X.css` where X is the ID for that page
		// You can generate that file via SCSS if you prefer, doesn't matter
		
		$file_path = $this->theme_view->classFolderFromRoot(false, true).'/css_pages/page_'.$this->renderer_item->id().'.css';
		$full_path = $_SERVER['DOCUMENT_ROOT'].$file_path;;
		if(file_exists($full_path))
		{
			$this->addCSSFile('page_'.$this->renderer_item->id(),$file_path);
		}
		
		// Load CSS from all the section styles
		$layout_list  = TMm_PageLayoutList::init();
		foreach($layout_list->pageSections() as $class_name => $path)
		{
			$this->addClassCSSFile($class_name);
		}
		
		// Deal with class names added to theme view container
		$this->theme_view_container->addClass('TMv_PagesTheme');
		$this->theme_view_container->addClass($this->theme_name);
		$this->theme_view_container->addClass('page_'.$this->renderer_item->rendererID());
		
		// Tungsten 9 functionality
		if(TC_getConfig('use_tungsten_9'))
		{
			// Attach the messenger, so that updates get added to the actual view when they happen
			// Otherwise it requires a refresh and that breaks things
			$messenger = TCv_Messenger::instance();;
			if($messenger instanceof TCv_Messenger)
			{
				$this->attachView($messenger);
				
			}
			// Insert the versioning message
			if(TC_getConfig('use_pages_version_publishing'))
			{
				$message = new TCv_View('unpublished_warning');
				$message->addText('This page has unpublished changes');
				$this->attachView($message);
			}
		}
		
	}
	
	/**
	 * Returns a view of the page properties including the title, url and image if it's set.
	 * @return TCv_View The content header, augmented with settings for managing it
	 */
	protected function pagePropertyPreview()
	{
		
		if($this->theme_view)
		{
			// Avoid false loading of models using the page ID in main page builder
			// This looks at the renderer model and if it's a page, undoes any of the OTHER models
			// which use the TMt_PageModelViewable trait
			if($this->renderer_item instanceof TMm_PagesMenuItem)
			{
				foreach(TC_activeModels() as $model)
				{
					// Detect non-pages viewable models, remove them from the active
					if(TC_classUsesTrait($model, 'TMt_PageModelViewable') && !($model instanceof TMm_PagesMenuItem))
					{
						TC_removeActiveModel($model);
					}
				}
			}
			
			
			
			$header = $this->theme_view->pageContentHeader();
			
			if(($this->theme_name)::$always_show_page_content_header_in_page_builder)
			{
				$header->addClass('always_show');
			}
			
			return $header;
		}
		
		return null;
		
		
	}
	
	/**
	 * Returns the `pages_content` view. This is a hook method that can be extended for custom wrappers
	 * @return TCv_View
	 */
	protected function pageContentView()
	{
		// Wrap the page builder inside of the `pages_content` container
		$page_content_view = new TCv_View('pages_content');
		
		return $page_content_view;
	}

	
	public function render()
	{
		// Do nothing if we're rendering an iframe, that frame will src this page and load it internally
		if($this->is_iframe)
		{
			return;
		}
		
		if($this->frame_menu_item instanceof TMm_PagesMenuItem)
		{
			$this->theme_view_container->attachView($this->pagePropertyPreview());
		}
		
		$page_content_view = $this->pageContentView();
		
		$page_content_view->attachView($this->page_builder);
		$this->theme_view_container->attachView($page_content_view);
		$this->attachView($this->theme_view_container);
		
		// Add the popups to the parent of the wrapper to ensure it sits outside of the pages theme setup
		$this->attachView($this->page_builder->addSectionPopup());
		$this->attachView($this->page_builder->addContentPopup());
		$this->attachView($this->page_builder->editContentPopup());
		$this->attachView($this->page_builder->stylesPopup());
		
		if(TC_getConfig('use_tungsten_9'))
		{
			$this->attachView($this->page_builder->bottomControlBar());
		}
		
		// Attach the theme CSS at the very end
		$this->attachCSSFromView($this->theme_view);
		
	}

	


	
}

