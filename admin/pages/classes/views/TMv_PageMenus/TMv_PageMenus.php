<?php
/**
 * Class TMv_PageMenus
 *
 * A view that shows the menus for a given menu item. This view will create nested unordered lists that can be used to
 * display navigation on a website. By default, the view will only return the direct descendants, however the number of
 * levels to be included can be changed. BY default the view will return the main navigation for the website.
 */
class TMv_PageMenus extends TCv_Menu
{
	protected $num_levels = 1000;
	protected $starting_level = 1;
	protected $force_show_homepage = false;
	protected $validation = false;
	protected $show_all_in_one_level = false;
	protected $generate_submenus_for_viewed = false;
	protected $menus_generated = false;
	
	// Indicates if titles should be wrapped in a span for rendering
	protected bool $wrap_title_in_span = false;
	
	protected ?array $submenu_wrapper = null; // A wrapper to be placed around submenus
	
	protected array $navigation_types = [];

	/**
	 * @var bool|TMm_PagesMenuItem
	 */
	protected $viewed_menu = false;
	protected $processing_first_level_for_view = true;

	/**
	 * TMv_PageMenus constructor.
	 * @param string $id The unique ID for the menu
	 */
	public function __construct($id)
	{
		parent::__construct($id);

		$website = TC_website();
		if($website instanceof TMv_PagesTheme)
		{
			$this->viewed_menu = $website->viewedMenu();
		}
		
		$this->addClassCSSFile('TMv_PageMenus');


		
	}
	
	/**
	 * Sets up this menu to be manual, so it can use all the existing functionality related to page menus but the
	 * processing isn't based on the navigational structure. Used when creating navigation outside of the normal setup.
	 * @return void
	 */
	public function setAsManual() : void
	{
		$this->menus_generated = true;
	}
	
	/**
	 * Sets the navigation type for these menus. These are based on different types set in the Pages Settings. The
	 * first type is ID 1 (usually Main Menus), the second 2, etc
	 * @param int|array $navigation_type The type or types of the navigation
	 * @deprecated THis is the old way that used numbers from a string explided
	 * @see addNavigationType()
	 */
	public function setNavigationType($navigation_type)
	{
		// Always an array
		if(is_array($navigation_type))
		{
			foreach($navigation_type as $type_id)
			{
				if($type = TMm_PagesNavigationType::init($type_id))
				{
					$this->addNavigationType($type);
				}
			}
		}
		else
		{
			if($type = TMm_PagesNavigationType::init($navigation_type))
			{
				$this->addNavigationType($type);
			}
		}
		
	}
	
	/**
	 * Adds a navigation type to these menus
	 * @param TMm_PagesNavigationType $type
	 * @return void
	 */
	public function addNavigationType(TMm_PagesNavigationType $type) : void
	{
		$this->navigation_types[] = $type;
	}

	/**
	 * Overrides the default menu item that is used to process this menu view
	 * @param TMm_PagesMenuItem $viewed_menu
	 */
	public function setViewedMenu($viewed_menu)
	{
		$this->viewed_menu = $viewed_menu;
	}
	
	/**
	 * An override function since it is common to want to show the homepage in some navigation but not others. It will
	 * default to the menu item with the folder "/".
	 */
	public function forceShowHomepage()
	{
		$this->force_show_homepage = true;
	}
	
	/**
	 * Turns on the feature where the titles are wrapped in a span.
	 * @return void
	 */
	public function enableWrapTitlesInSpan(): void
	{
		$this->wrap_title_in_span = true;
	}
	
	/**
	 * Indicates the number of levels that should be shown for this menu.
	 * @param int $num_levels
	 */
	public function setNumLevelsToShow($num_levels)
	{
		$this->num_levels = $num_levels;
	}
	
	/**
	 * Indicate the "level" of navigation to show. The first level starts at 1.
	 * @param int $level
	 */
	public function setStartingLevel($level)
	{
		$this->starting_level = $level;

	}


	/**
	 * Method to calculate if the provided menu item passes all the visibility tests
	 * @param TMm_PagesMenuItem $page_menu_item
	 * @return bool
	 */
	protected function menuItemIsVisible(TMm_PagesMenuItem $page_menu_item) : bool
	{
		// Forcing the homepage
		if($this->force_show_homepage && $page_menu_item->folder() == '/')
		{
			return true;
		}
		
		// Start with the menu item's own checks
		if(!$page_menu_item->isVisible())
		{
			return false;
		}
		
		// Model lists don't get shown
		if($page_menu_item->isModelList())
		{
			return false;
		}
		
		// Avoid any that don't match the navigation type
		if(!$page_menu_item->matchesNavigationTypes($this->navigation_types))
		{
			return false;
		}
		
		// Check for any menu-level validation being set
		if(is_array($this->validation))
		{
			
			$method_name = $this->validation['method_name'];
			if($page_menu_item->$method_name() != $this->validation['expected_value'])
			{
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Handles the attaching of a page menu item to a menu view.
	 *
	 * @param TMm_PagesMenuItem $page_menu_item The menu item that is being added
	 * @param TCv_Menu $menu The menu that is being used
	 * @param int $num_children_levels The number of children levels for this item
	 */
	public function processMenuItemForMenu($page_menu_item, $menu, $num_children_levels)
	{
		// Check if we can show the button
		if($this->menuItemIsVisible($page_menu_item))
		{
			
			// add the child to the menu
			$menu->attachView($this->listItemForMenuItem($page_menu_item, $num_children_levels));

			
		}
		
		$this->processModelListItemsIntoMenu($page_menu_item, $menu);
		
	}
	
	/**
	 * Generates and returns the `<li>` item that is created for a menu item. This function can be recursive since it
	 * will also call and generate the menu that exists beneath it.
	 * @param TMm_PagesMenuItem $page_menu_item
	 * @param int $num_children_levels
	 * @return TCv_View
	 *
	 * @uses TMv_PageMenus::menuButtonForPagesMenuItem()
	 * @uses TMv_PageMenus::menuViewForMenuItem()
	 * @uses TMv_PageMenus::listItemForView()
	 *
	 */
	protected function listItemForMenuItem(TMm_PagesMenuItem $page_menu_item, int $num_children_levels)
	{
		$button = $this->menuButtonForPagesMenuItem($page_menu_item);
		
		// Convert the button to a list item
		$list_item = $this->listItemForView($button);
		$list_item->addClass('l_'.$page_menu_item->level()); // add a level marker, used for keyboard nav and styling
		
		// recursively find children
		if($num_children_levels > 0)
		{
			$submenus = $this->menuViewForMenuItem($page_menu_item, $num_children_levels - 1);
			if(!is_null($submenus) &&
				(!method_exists($submenus, 'numItems') || $submenus->numItems() > 0))
			{
				$list_item->addClass('has_children');
				
				if(is_array($this->submenu_wrapper))
				{
					$wrapper = new TCv_View();
					$wrapper->setTag($this->submenu_wrapper['tagname']);
					$wrapper->addClassArray($this->submenu_wrapper['classes']);
					$wrapper->attachView($submenus);
					$list_item->attachView($wrapper);
				}
				else
				{
					$list_item->attachView($submenus);
				}
				
			}
		}
		
		return $list_item;
	}
	
	/**
	 * Defines a submenu wrapper which is used to encapsulate submenus being added to primary menus, used for styling
	 * purposes.
	 * @param string $tagname The tag name for the wrapper
	 * @param string[] $classes The classes to be applied to the wrapper
	 * @return void
	 */
	public function setSubmenuWrapper($tagname = 'div', $classes = []) : void
	{
		$this->submenu_wrapper = ['tagname' => $tagname, 'classes' => $classes];
	}
	
	
	/**
	 * Processes a page menu item that uses a list to replace itself and adds those items as buttons to the menu
	 * provided.
	 * @param TMm_PagesMenuItem $page_menu_item
	 * @param TCv_Menu $menu
	 */
	protected function processModelListItemsIntoMenu($page_menu_item, $menu)
	{
		// Get the potential array of models
		$models = $this->modelsForPageMenuItem($page_menu_item);
		
		if(is_array($models))
		{
			$this->convertModelsIntoMenuButtons($models, $page_menu_item, $menu );
			
		}
	}
	
	/**
	 * Returns the array of models for a given page menu item. This is part of the ModelListItems functionality which
	 * sets a page menu item to return an array of models to generate navigation.
	 * @param TMm_PagesMenuItem $page_menu_item
	 * @return false|mixed|null
	 */
	protected function modelsForPageMenuItem($page_menu_item)
	{
		if($page_menu_item->isModelList() && $page_menu_item->matchesNavigationTypes($this->navigation_types))
		{
			// Generate the list of models
			/** @var TCm_ModelList $model_list */
			$model_list = ($page_menu_item->modelListClassName())::init();
			return call_user_func(array($model_list, $page_menu_item->modelListClassMethod()));
		}
		
		return null;
	}
	
	/**
	 * Converts an array of models into buttons that are part of the provided menu. The models must use the trait
	 * TMt_PageModelViewable which is normally already set as part of the configuration of the layout.
	 * @param TCm_Model[]|TMt_PageModelViewable[] $models
	 * @param TMm_PagesMenuItem
	 * @param TCv_Menu $menu
	 */
	protected function convertModelsIntoMenuButtons($models, $page_menu_item, $menu)
	{
		foreach($models as $model)
		{
			if($button = $this->buttonForModel($model ,$page_menu_item))
			{
				$menu->attachView($button);
			}
			else
			{
				$this->processUnknownValueInModelList($model, $menu, $page_menu_item);
			}
			
		}
	}
	
	/**
	 * Returns the button for a given model
	 *
	 * @param TCm_Model|TMt_PageModelViewable $model
	 * @param null|TMm_PagesMenuItem $page_menu_item The menu item that is being loaded
	 * @return TCv_Link
	 */
	public function buttonForModel($model, $page_menu_item)
	{
		if($model instanceof TCm_Model && TC_classUsesTrait($model, 'TMt_PageModelViewable'))
		{
			
			
			$button = new TCv_Link($this->id . '_' . get_class($model) . '_' . $model->id());
			$button->addText($model->title());
			
			// GET THE VIEW MODE FOR THE page menu item, that indicates how we generate a URL
			// If the menu item is set to "view" then we get the view URL
			if($page_menu_item && $page_menu_item->modelClassName() == get_class($model))
			{
				if($page_menu_item->modelClassViewMode() == 'view')
				{
					$button->setURL($model->pageViewURLPath());
				}
				elseif($page_menu_item->modelClassViewMode() == 'edit')
				{
					$button->setURL($model->pageEditURLPath());
					
				}
				else // A list of non-standard model modes. build it based on the URL
				{
					$button->setURL($page_menu_item->pageViewURLPath()
					                .$model->id().'/'.$model->cleanForURL($model->title()).'/');
				}
				
			}
			else
			{
				$button->setURL($model->pageViewURLPath());
			}
			
			$active_model = TC_activeModelWithClassName(get_class($model));
			if($this->viewed_menu
				&& $page_menu_item
				&& $this->viewed_menu->id() == $page_menu_item->id()
				&& $active_model
				&& $active_model->id() == $model->id()
			)
			{
				$button->addClass('current_menu');
			}
			return $button;
		}
		
		return null;
	}
	
	/**
	 * This is a hook method to process non-model values that get returned as part of a model list menu item
	 * @param mixed $item
	 * @param TCv_Menu $menu
	 * @param TMm_PagesMenuItem $page_menu_item
	 */
	protected function processUnknownValueInModelList($item, $menu, $page_menu_item)
	{
		// No functionality
		
	}

	/**
	 * Sets the validation for these menus.
	 * @param string $method_name The method to be called on the TMm_PagesMenuItem
	 * @param mixed $expected_value The expected value that will allow that menu item to be shown
	 */
	public function setValidation($method_name, $expected_value)
	{
		$this->validation = array('method_name' => $method_name, 'expected_value' => $expected_value);
	}
	
	/**
	 * Returns a TCv_Menu for the item provided. This is a recursive method that will parse through the entire child
	 * list if allowed
	 *
	 * @param TMm_PagesMenuItem $page_menu_item The menu item being evaluated.
	 * @param int $num_children_levels (Optional) Default 0
	 * @return ?TCv_View This returns a view, a menu view OR null if nothing should occur
	 */
	public function menuViewForMenuItem(TMm_PagesMenuItem $page_menu_item, int $num_children_levels = 0): ?TCv_View
	{
		// create a menu for the item being provided
		// If it's the first level, then the menu we want to add to is actually this view
		if($this->processing_first_level_for_view)
		{
			$menu = $this;
			$this->processing_first_level_for_view = false;
		}
		else
		{
			$menu = new TCv_Menu($this->id . '_' . $page_menu_item->id());
			
		}
		
		// loop through children of that menu
		foreach($page_menu_item->children() as $child_menu)
		{
			$this->processMenuItemForMenu($child_menu, $menu, $num_children_levels);
		}
		
		return $menu;
	}
	
	/**
	 * Returns a TCv_link for a given TMm_PagesMenuItem
	 *
	 * @param TMm_PagesMenuItem $menu_item THe menu item to be used
	 * @return TCv_Link The link to that menu item
	 */
	public function menuButtonForPagesMenuItem(TMm_PagesMenuItem $menu_item) : TCv_Link
	{
		$button = new TCv_Link($this->id . '_menu_item_' . $menu_item->id());
		
		if($this->wrap_title_in_span)
		{
			$span = new TCv_View();
			$span->setTag('span');
			$span->addClass('t');
			$span->addText($menu_item->title());
			$button->attachView($span);
		}
		else
		{
			$button->addText($menu_item->title());
		}
		
		$button->setURL($menu_item->viewURL());


		if($this->viewed_menu)
		{
			if($this->viewed_menu->menuItemIsAncestor($menu_item))
			{
				$button->addClass('highlight_menu');
			}
			if($menu_item->id() == $this->viewed_menu->id())
			{
				$button->addClass('current_menu');
			}
		}

		foreach($menu_item->navigationTypes() as $type)
		{
			$button->addClass('nav_'.$type->id());
		}
		$button->addDataValue('menu-id', $menu_item->id());
		if(!$menu_item->isVisible())
		{
			$button->addClass('hidden');
		}
		

		return $button;
	}

	/**
	 * Returns a TMm_PagesMenuItem that is the root for the site. This menu doesn't actually exist, but calling the
	 * "children" will provide the top level of navigation.
	 * @return TMm_PagesMenuItem
	 */
	public function rootMenuItem()
	{
		return TMm_PagesMenuItem::init( 0);
	}




	//////////////////////////////////////////////////////
	//
	// ALL POSSIBLE ON ONE LEVEL
	//
	//////////////////////////////////////////////////////


	/**
	 * Sets this menu to find all the possible items and show them all in one level. This will find all the menu
	 * items that match the visible criteria anywhere in the path of hte viewed menu and show them all in a single
	 * flat menu structure.
	 *
	 * This is most commonly used for subnavigation that must persist across different levels and is best used with a
	 * single navigation type.
	 */
	public function showAllPossibleInOneLevel()
	{
		$this->show_all_in_one_level = true;
	}

	/**
	 * Attaches all the possible navigation for the menu items in one level.
	 */
	protected function attachAllVisibleMenusInOneLevel()
	{
		if($this->viewed_menu)
		{
			$starting_menu = $this->viewed_menu->ancestorForLevel($this->starting_level);
			if($starting_menu)
			{
				foreach($starting_menu->descendants() as $menu_item)
				{
					if($this->menuItemIsVisible($menu_item))
					{
						// No classname, all good
						if($menu_item->modelClassName() == '')
						{
							$button = $this->menuButtonForPagesMenuItem($menu_item);
							$this->attachView($button);

						}
						// Match by class name
						elseif($this->viewed_menu->modelClassName() == $menu_item->modelClassName())
						//elseif($active_model = TC_activeModelWithClassName($menu_item->modelClassName()))
						{
							$button = $this->menuButtonForPagesMenuItem($menu_item);
							$this->attachView($button);

						}


					}
				}
			}
		}

	}


	//////////////////////////////////////////////////////
	//
	// GENERATE SUBMENUS FOR VIEWED MENU
	//
	//////////////////////////////////////////////////////

	/**
	 * Sets this menu to show the submenus for the viewed menu
	 */
	public function generateSubmenusForViewedMenu()
	{
		$this->generate_submenus_for_viewed = true;
	}


	/**
	 * Attaches all the possible navigation for the menu items in one level.
	 */
	protected function attachViewedMenuSubmenus()
	{
		if($this->viewed_menu)
		{
			foreach($this->viewed_menu->children() as $menu_item)
			{
				if($this->menuItemIsVisible($menu_item))
				{
					$button = $this->menuButtonForPagesMenuItem($menu_item);
					$this->attachView($button);
				}
			}
		}
	}

	/**
	 * Returns the number of items in this list
	 * @return int
	 */
	public function numItems()
	{
		$this->generateMenus();
		return sizeof($this->attached_views);
	}

	public function generateMenus()
	{
		if(!$this->menus_generated)
		{
			$this->menus_generated = true;

			// Handle single flat navigation type
			if(count($this->navigation_types) == 1 && $this->navigation_types[0]->isFlat())
			{
				$this->generateFlatMenus();
			}
			else
			{
				
				
				if($this->starting_level == 1)
				{
					// special case with level 0. Need the root menu item
					$menu_item_for_level = $this->rootMenuItem();
				}
				else
				{
					// fail gracefully with no viewed menu found
					if(!$this->viewed_menu)
					{
						return '';
					}
					
					$menu_item_for_level = $this->viewed_menu->ancestorForLevel($this->starting_level - 1); // -1 since level 1 would want the children for level 0
				}
				
				if($this->show_all_in_one_level)
				{
					$this->attachAllVisibleMenusInOneLevel();
				}
				elseif($this->generate_submenus_for_viewed)
				{
					$this->attachViewedMenuSubmenus();
				}
				elseif($menu_item_for_level instanceof TMm_PagesMenuItem)
				{
					$this->menuViewForMenuItem($menu_item_for_level, $this->num_levels - 1);
				}
			}
		}
	}
	
	/**
	 * Generates menus for a single flat navigation type
	 * @return void
	 */
	public function generateFlatMenus() : void
	{
		$single_navigation_type = $this->navigation_types[0];
		
		foreach($single_navigation_type->pages() as $page)
		{
			if($this->menuItemIsVisible($page))
			{
				$button = $this->menuButtonForPagesMenuItem($page);
				$this->attachView($button);
			}
			
			
		}
	}
	
	/**
	 * Returns the HTML for this view
	 * @return string
	 */
	public function html()
	{
		// Single Page means no content should be returned
		$single_page_value = TC_getModuleConfig('pages', 'single_page');
		if($single_page_value > 0)
		{
			return '';
		}

		$this->generateMenus();


		if($this->numItems() == 0)
		{
			return '';
		}


		// Not needed this class has it all
		//$this->attachView($menus);	
			
		return parent::html();
	
	}
	
	

	//////////////////////////////////////////////////////
	//
	// DEPRECATED
	//
	//
	//////////////////////////////////////////////////////


	/**
	 * @deprecated
	 */
	public function setMobileButtonText($mobile_button_text)
	{
		// DO NOTHING, DEPRECATED
	}

	/**
	 * @deprecated
	 */
	public function hideMobile()
	{
		// DO NOTHING, DEPRECATED
	}


	/**
	 * @deprecated
	 * @see TMv_PageMenus::setNumLevelsToShow()
	 */
	public function setNumLevels($num_levels)
	{
		$this->setNumLevelsToShow($num_levels);
	}
	
	
	
	
}
?>