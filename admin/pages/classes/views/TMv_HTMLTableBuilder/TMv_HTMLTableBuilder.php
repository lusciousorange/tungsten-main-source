<?php
/**
 * Class TMv_HTMLTableBuilder
 */
class TMv_HTMLTableBuilder extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $rows, $columns, $header_rows, $footer_rows, $column_widths;
	
	/**
	 * TMv_Photo constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_HTMLTableBuilder');
		
	}
	
	/**
	 * Generates a single cell
	 * @param int $row
	 * @param int $column
	 * @return TCv_HTMLTableCell
	 */
	protected function cell($row, $column)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addDataValue('r', $row);
		$cell->addDataValue('c', $column);
		
		$varname = "r$row"."_c$column";
		
		if(!isset($this->$varname))
		{
			$this->$varname = '';
		}
		$text = $this->localizeProperty($varname);
		
		if($text != '')
		{
			$cell->addText($text);
		}
		else
		{
			$cell->addText('&nbsp;');
		}
		
		return $cell;
	}
	
	/**
	 * Builds the table from the values set
	 * @return TCv_HTMLTable
	 */
	protected function buildTable()
	{
		
		// Build the table
		$table = new TCv_HTMLTable();
		
		
		
		// Build Header Row
		if($this->header_rows > 0)
		{
			// Build the rows
			for($row_num = 1; $row_num <= $this->header_rows;  $row_num++)
			{
				$row = new TCv_HTMLTableRow();
				for($col_num = 1; $col_num <= $this->columns;  $col_num++)
				{
					$cell = $this->cell($row_num, $col_num);
					$cell->setTag('th');
					$row->attachView($cell);
				}
				
				$table->attachHeadRow($row);
				
			}
		}
		
		// Build the main rows
		$start_row = $this->header_rows + 1;
		$last_row = $this->rows - $this->footer_rows;
		for($row_num = $start_row; $row_num <= $last_row;  $row_num++)
		{
			$row = new TCv_HTMLTableRow();
			
			for($col_num = 1; $col_num <= $this->columns;  $col_num++)
			{
				$cell = $this->cell($row_num, $col_num);
				$row->attachView($cell);
			}
			
			$table->attachView($row);
			
		}
		
		// Build Footer Row
		if($this->footer_rows > 0)
		{
			$start_row = $this->rows - $this->footer_rows + 1;
			// Build the rows
			for($row_num = $start_row; $row_num <= $this->rows;  $row_num++)
			{
				$row = new TCv_HTMLTableRow();
				for($col_num = 1; $col_num <= $this->columns;  $col_num++)
				{
					$cell = $this->cell($row_num, $col_num);
					$row->attachView($cell);
				}
				
				$table->attachFooterRow($row);
				
			}
		}
		
		return $table;
	}
	
	/**
	 * Function to detect the column width values and if it's set and they correctly add up to 100, then generate the
	 * css styles that will apply to that item.
	 */
	public function handleColumnWidths()
	{
		$column_widths = $this->column_widths;
		if($column_widths != '')
		{
			$values = explode(',', $column_widths);
			// Ensure the number of columns match
			if($this->columns == count($values) && array_sum($values) == 100)
			{
				$column_num = 0;
				foreach($values as $percentage)
				{
					$column_num++;
					
					$css_line = '#'.$this->id().' [data-c="'.$column_num.'"] { width:'.$percentage.'%;  }';
					$this->addCSSLine($this->id().'col'.$column_num, $css_line);
					// Add CSS
				}
				$this->addClass('has_set_widths');
				
			}
			else
			{
				$this->addConsoleWarning('Column Widths are incorrectly formatted. Please confirm they add up to 100 and the number of values matches the number of columns');
			}
		}
		
	}
	
	public function render()
	{
		if($this->isLoadingInPageBuilder())
		{
			$this->addClass('in_page_builder');
			
		}
		$this->attachView($this->buildTable());
		
		$this->handleColumnWidths();
		parent::render();
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		
		$field = new TCv_FormItem_Select('columns', 'Number of Data Column');
		$field->setOptionsWithNumberRange(1,20);
		$field->setIsRequired();
		$field->setDefaultValue(1);
		$form_items['columns'] = $field;
		
		$field = new TCv_FormItem_Select('rows', 'Total Number of Rows');
		$field->addHelpText('The total number of rows including those that are header or footer rows');
		$field->setOptionsWithNumberRange(1,200);
		$field->setIsRequired();
		$field->setDefaultValue(1);
		$form_items['rows'] = $field;
		
		$field = new TCv_FormItem_Select('header_rows', 'Number of Header Rows');
		$field->setHelpText("The number of rows that will be marked as header rows");
		$field->setOptionsWithNumberRange(0,5);
		$field->setIsInteger();
		$field->setIsRequired();
		$field->setDefaultValue(0);
		$form_items['header_rows'] = $field;
		
		$field = new TCv_FormItem_Select('footer_rows', 'Number of Footer Rows');
		$field->setHelpText("The number of rows that will be marked as footer rows");
		$field->setOptionsWithNumberRange(0,5);
		$field->setIsRequired();
		$field->setDefaultValue(0);
		$form_items['footer_rows'] = $field;
		
		$field = new TCv_FormItem_File('csv_file','Upload CSV File');
		$field->setHelpText("Upload data from a CSV File. The dimensions and content will be replaced with what is found in the file.");
		$field->setSaveToDatabase(false); // never save the file name
		$field->setValidateFileType(array('csv'));
		$form_items['csv_file'] = $field;
		
		$field = new TCv_FormItem_TextField('column_widths', 'Column Widths');
		$field->setHelpText("A comma separate list of widths which must add up to 100 and the number of items must match the number of columns. Invalid values will have no effect");
		$field->setPlaceholderText('10,25,25,40');
		$form_items['column_widths'] = $field;
		
		
		return $form_items;
	}
	
	/**
	 * Hook method for the form processing that handles the uploaded CSV file.
	 
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return string[] Returns an array of values that should be stored for the page content
	 */
	public static function pageContent_FormProcessing($form_processor)
	{
		$variable_values = array();
		
		// Do nothing
		//$form_processor->addConsoleDebug('Processing form');
		
		// Detect if the csv_file has been set, if so, process the values
		$filename = $form_processor->databaseValue('csv_file');
		if($filename != '')
		{
			$path = ($form_processor->model())::uploadFolder();
			
			// Check if the uploaded file exists
			if(file_exists($path.$filename))
			{
				// Convert the CSV File to an array of values
				$csv = array_map('str_getcsv', file($path.$filename));
				
				// Ensure a valid array was parsed
				if(is_array($csv))
				{
					// Count the number of rows and columns
					$num_rows = count($csv);
					$num_columns = count($csv[0]);
					
					// Update the rows and columns values
					$variable_values['rows'] = $num_rows;
					$variable_values['columns'] = $num_columns;
					
					// Nested loops to get the values for each cell
					$row_count = 0;
					foreach($csv as $row)
					{
						$row_count++;
						
						$column_count = 0;
						foreach($row as $column_value)
						{
							$column_count++;
							
							// Save the value to the array that is being returned
							$variable_values['r'.$row_count.'_c'.$column_count] = '<p>'.$column_value.'</p>';
						}
					}
				}
				
				// Delete the file to avoid a trace existing
				unlink($path.$filename);
			}
			else
			{
				$form_processor->failFormItemWithID('csv_file', 'CSV File Not Found');
			}
			
			
		}
		
		return $variable_values;
		
	}
	
	public static function pageContent_ViewTitle() : string  { return 'Table'; }
	public static function pageContent_IconCode() : string  { return 'fa-table'; }
	public static function pageContent_ViewDescription() : string
	{
		return 'A table (grid) of information .';
	}
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		// Generate a grid of values to ensure they get checked in validation
		$grid_values = [];
		
		for($row = 1; $row <100 ; $row++)
		{
			for($column = 1 ; $column < 20 ; $column++)
			{
				$grid_values['r'.$row.'_c'.$column] = true;
			}
		}
		
		return $grid_values;
	}
}