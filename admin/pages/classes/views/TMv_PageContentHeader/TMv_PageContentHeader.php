<?php
class TMv_PageContentHeader extends TCv_View
{
	protected $menu_item;
	protected $breadcrumb_separator = ' | ';
	protected $show_breadcrumbs = true;
	protected $show_page_description = false;
	protected $background_photo_target = 'page_content_header';
	protected $include_home_in_breadcrumbs = true;
	
	
	/**
	 * TMv_PagesTheme constructor.
	 * @param TMm_PagesMenuItem $menu_item
	 */
	public function __construct($menu_item)
	{
		parent::__construct('page_content_header');
		$this->menu_item = $menu_item;
		
		
	}
	
	/**
	 * Returns the internal functions to call for this header, which indicates the order in how they are rendered.
	 * Each of these functions should return TCv_View/null and this indicates how they are displayed on the site. The
	 * exception is attachBackgroundImage, which requires $this to be passed in.
	 * This method can be overridden to change the order, as well as and or remove other function calls for
	 * customization.
	 *
	 * These are added inside of configureViews(). If you override configureViews(), you must manually handle the
	 * adding of all these items.
	 * @return string[]
	 */
	public function functionRenderOrder() : array
	{
		return [
			'attachBackgroundImage',
			'pageHeading',
			'breadcrumbHeadingContainer',
			'pageDescription',
		];
	}
	
	
	/**
	 * Returns the view for the background image for this header
	 * @return TCv_Image|TCv_View|TMv_PageHeroBoxes|null
	 */
	public function backgroundImageView() : ?TCv_View
	{
		if(TC_getConfig('use_pages_hero_boxes') && $this->menu_item->usesHeroBoxes())
		{
			/** @var TMv_PageHeroBoxes $hero_box */
			$hero_box = TMv_PageHeroBoxes::init($this->menu_item);
			$hero_box->setTimerMilliseconds(6000);
			return $hero_box;
		}
		elseif($photo_file = $this->menu_item->photoFile())
		{
			if($photo_file->isImage())
			{
				return new TCv_Image(false, $photo_file);
			}
			elseif($photo_file->extension() == 'mp4')
			{
				$video_view = new TCv_View('background_image');
				$video_view->setTag('video');
				$video_view->setAttribute('autoplay', 'autoplay');
				$video_view->setAttribute('loop', 'loop');
				$video_view->setAttribute('muted', 'muted');
				
				$source = new TCv_View();
				$source->setTag('source');
				$source->setAttribute('src', $photo_file->filenameFromSiteRoot());
				$source->setAttribute('type', 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
				$video_view->attachView($source);
				
				return $video_view;
			}
		}
		
		return null;
	}
	
	/**
	 * Handles attaching the background image to the provided view, which can often be $this
	 * @param TCv_View $target_view
	 *
	 */
	protected function attachBackgroundImage($target_view) : void
	{
		if($this->background_photo_target != 'page_content_header')
		{
			return;
		}
		// Get the background view
		$background_view = $this->backgroundImageView();
		
		// Images get applied as a background on the view being provided
		if($background_view instanceof TCv_Image)
		{
			$background_url =' url("' . $background_view->file()->filenameFromSiteRoot() . '")';
			
			
			$style = 'background-image: '.$background_url.';';
			
			$position = $this->menu_item->photoPosition();
			if($position != '')
			{
				$style .= ' background-position: '.$this->menu_item->photoPosition().';';
			}
			
			$target_view->addClass('has_photo');
			$target_view->addClass('has position');
			$target_view->setAttribute('style', $style);
		}
		elseif($background_view instanceof TCv_View)
		{
			$target_view->attachView($background_view);
		}
		else
		{
			// No idea what we have, to pass to a hook method to deal with on a site-by-site basis
			$this->hook_attachUnknownBackgroundImageView($target_view, $background_view);
		}
		
	}
	
	/**
	 * A hook method to deal with scenarios where we're attaching a background image view that we can't identify.
	 * @param TCv_View $target_view
	 * @param mixed $background_view
	 */
	protected function hook_attachUnknownBackgroundImageView(TCv_View $target_view, $background_view)
	{
		// Do nothing. Override as needed.
	}
	
	/**
	 * Sets background images to be applied to page headings.
	 */
	public function applyBackgroundToPageHeading() : void
	{
		$this->background_photo_target = 'page_heading';
	}
	
	/**
	 * Returns the page heading view
	 * @return TCv_View
	 */
	protected function pageHeading() : TCv_View
	{
		$container = new TCv_View('header_page_title');
		
		// deal with background image
		if($this->background_photo_target == 'page_heading')
		{
			$this->attachBackgroundImage($container);
		}
		
		$content_width_container = new TCv_View();
		$content_width_container->addClass('content_width_container');
		
		foreach($this->pageHeadingViews() as $view)
		{
			$content_width_container->attachView($view);
		}
		
		$container->attachView($content_width_container);
		
		return $container;
	}
	
	/**
	 * Returns an array of views that are attached to the page heading
	 * @return TCv_View[]
	 */
	protected function pageHeadingViews() : array
	{
		$views = array();
		
		$page_title = new TCv_View();
		$page_title->setTag('h1');
		$page_title->addText($this->menu_item->titleOrModelTitle());
		$views['h1'] = $page_title;
		
		return $views;
	}
	
	
	////////////////////////////////////////////////
	//
	// BREADCRUMBS
	//
	// Methods related to breadcrumbs
	//
	////////////////////////////////////////////////
	
	/**
	 * Hides the breadcrumbs
	 */
	public function hideBreadcrumbs() : void
	{
		$this->show_breadcrumbs = false;
	}
	
	/**
	 * Shows the breadcrumbs
	 */
	public function showBreadcrumbs() : void
	{
		$this->show_breadcrumbs = true;
	}
	
	/**
	 * Sets the separator for the breadcrumbs
	 * @param string $separator
	 */
	public function setBreadcrumbSeparator(string $separator) : void
	{
		$this->breadcrumb_separator = $separator;
	}
	
	/**
	 * Indicates if the homepage should be included in the breadcrumbs
	 * @param bool $yes_no
	 */
	public function setIncludeHomeInBreadcrumbs(bool $yes_no) : void
	{
		$this->include_home_in_breadcrumbs = $yes_no;
	}
	
	
	
	/**
	 * Returns the breadcrumb view. Option to return multiple views if required.
	 * @return TMv_PagesBreadcrumbHeading|TCv_View[]
	 */
	protected function breadcrumbHeading()
	{
		/** @var TMv_PagesBreadcrumbHeading $breadcrumbs */
		$breadcrumbs = TMv_PagesBreadcrumbHeading::init($this->menu_item);
		$breadcrumbs->setSeparator($this->breadcrumb_separator);
		
		if($this->include_home_in_breadcrumbs)
		{
			$breadcrumbs->includeHomeAtStart('Home');
		}
		
		return $breadcrumbs;
	}
	
	/**
	 * Returns the breadcrumb view, null if it shouldn't be shown
	 * @return TCv_View|null
	 */
	protected function breadcrumbHeadingContainer() : ?TCv_View
	{
		if(!$this->show_breadcrumbs)
		{
			return null;
		}
		$container = new TCv_View('header_page_breadcrumbs');
		
		$content_width_container = new TCv_View();
		$content_width_container->addClass('content_width_container');
		
		$headings = $this->breadcrumbHeading();
		if(is_array($headings))
		{
			foreach($headings as $heading_view)
			{
				if($heading_view instanceof TCv_View)
				{
					$content_width_container->attachView($heading_view);
				}
			}
		}
		elseif($headings instanceof TCv_View)
		{
			$content_width_container->attachView($headings);
		}
		
		
		$container->attachView($content_width_container);
		
		return $container;
	}
	
	////////////////////////////////////////////////
	//
	// PAGE DESCRIPTION
	//
	// Methods related to the page description
	//
	////////////////////////////////////////////////
	
	/**
	 * Turns on the page description appearance on the site
	 */
	public function showPageDescription() : void
	{
		$this->show_page_description = true;
	}
	
	/**
	 * Turns on the page description appearance on the site
	 */
	public function hidePageDescription() : void
	{
		$this->show_page_description = false;
	}
	
	
	
	/**
	 * Returns the page description view
	 * @return TCv_View|null
	 */
	protected function pageDescription() : ?TCv_View
	{
		if(!$this->show_page_description)
		{
			return null;
		}
		$container = new TCv_View('header_page_description');
		
		$content_width_container = new TCv_View();
		$content_width_container->addClass('content_width_container');
		
		$page_title = new TCv_View();
		$page_title->setTag('p');
		$page_title->addText(nl2br($this->menu_item->description()));
		
		$content_width_container->attachView($page_title);
		$container->attachView($content_width_container);
		
		if(trim($this->menu_item->description()) == '')
		{
			$container->addClass('empty');
		}
		
		return $container;
	}
	
	////////////////////////////////////////////////
	//
	// RENDERING AND OUTPUT
	//
	////////////////////////////////////////////////
	
	/**
	 * Configures the main view. This method can be extended or overwritten on extended class to customize the theme.
	 */
	public function configureViews() : void
	{
		// Loop through the function in order
		foreach($this->functionRenderOrder() as $function_name)
		{
			if($function_name == 'attachBackgroundImage')
			{
				$this->attachBackgroundImage($this);
			}
			else
			{
				$this->attachView($this->$function_name());
				
			}
		}
		
		
		
	}
	
	/**
	 * Renders the views
	 */
	public function render()
	{
		// Avoid attempts to render when no menu item exists
		if(!$this->menu_item)
		{
			return;
		}
		
		$this->configureViews();
		
	}
	
	/**
	 * Returns the view shown when content is incorrectly shown
	 * @return TCv_View
	 */
	public function noContentViewContent() : TCv_View
	{
		$view = new TCv_View('no_content_view');
		
		$message = TC_localize('page_content_missing', 'The content for this page is not visible');
		if($this->menu_item->isMissingActiveModel())
		{
			/** @var string|TCm_Model $class_name */
			$class_name = $this->menu_item->modelClassName();
			$message = 	$class_name::modelTitleSingular().' '.TC_localize('x_not_found',' not found');
		}
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText($message);
		$view->attachView($heading);
		
		return $view;
		
		
	}
	
	/**
	 * Extends the html method to not show anything if the content isn't visible.
	 * @return string
	 */
	public function html()
	{
		// Check for content visible
		if($this->menu_item && !$this->menu_item->contentVisible() && !TC_isTungstenView())
		{
			$this->render();
			$this->render_called = true;
			$this->detachAllViews();
			
			$view = $this->noContentViewContent();
			$view->addClass('content_width_container');
			
			
			$this->attachView($view);
			
			
		}
		
		return parent::html();
	}
}
?>