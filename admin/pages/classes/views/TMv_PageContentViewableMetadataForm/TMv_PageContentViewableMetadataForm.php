<?php
class TMv_PageContentViewableMetadataForm extends TCv_FormWithModel
{
	
	/**
	 * TMv_EditMenuItemForm constructor.
	 * @param bool|TMm_PagesMenuItem|TMt_PageModelViewable $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		
	}
	
	/**
	 * Configures the form elements for this form
	 */
	public function configureFormElements()
	{
		$website_title_prefix = TC_getModuleConfig('pages', 'website_title');
		$title_length = strlen($website_title_prefix) + 3; // number of characters plus " – "
		$remaining_characters = 60 - $title_length;
		
		$title = new TCv_FormItem_TextField('meta_title', 'Metadata page title');
		$title->setHelpText('An override for the title shown in the browser window and in previews when shared on other sites. If left blank, the website will use the existing title for this item. <br /><br />'.$remaining_characters.' characters max. ');
		$title->setPlaceholderText(strip_tags($this->model()->title()));
		$title->setMaxLength($remaining_characters);
		$this->attachView($title);
		
		
		$description = new TCv_FormItem_TextBox('meta_description', 'Metadata page description');
		$description->setHelpText('A one or two line description which appears in search results and when shared on other sites, such as social media. <br /><br />160 characters max.');
		$description->setMaxLength(160);
		$this->attachView($description);
		

		$field = new TCv_FormItem_RadioButtons('is_visible_on_sitemap', 'Visible on sitemap.xml');
		$field->setHelpText('The website generates a sitemap.xml file which tells search engines about the pages on this site. You can choose to exclude this item from the sitemap.');
		$field->addOption(1, '<strong>Yes</strong> – This item is listed on the sitemap.xml file');
		$field->addOption(0, '<strong>No</strong> – Hide this on the sitemap.xml file');
		$this->attachView($field);
	}
	
	/**
	 * Help html
	 * @return string|null
	 */
	public static function defaultHelpHTML(): ?string
	{
		return
			'<p>This form relates to metadata, which is information about this item, which doesn\'t appear on the page itself
 but does appear in the code for the page. This is used by search engines, social media websites and other sources that want
 specific information about this item.</p>
	<h2>Default values</h2>
	<ul>
		<li><strong>Page title</strong> will use the title of the item, and you do not need to set this title again, unless it is to override the default.</li>
		<li><strong>Page description</strong> that are left blank, will attempt to pull the first text it finds from the item, if possible. It is recommended to set the description.</li>
		<li><strong>Sitemap.xml</strong> will default to "yes", so it must be disabled for any item that should not appear.</li>
	</ul>

';
	}
}