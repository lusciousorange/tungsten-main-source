<?php

/**
 * Class TMv_HTMLTableCellForm
 *
 * The form for a single cell table
 */
class TMv_HTMLTableCellForm extends TCv_FormWithModel
{
	protected $variable_name;
	/**
	 * TMv_HTMLTableCellForm constructor.
	 * @param string|TMt_PageRenderItem $model
	 * @param int $row_num
	 * @param int $col_num
	 */
	public function __construct($model, $row_num, $col_num)
	{
		parent::__construct($model);
		
		$this->addConsoleDebug($model);
		$this->setButtonText('Update Cell Content');
		
		$this->variable_name = 'r'.$row_num.'_c'.$col_num;
		$this->setOnLoadFocusItem('cell_content');
		
		// Enable publishing mode
		//static::enablePublishingMode($renderer_class_name);
		
	}
	
	/**
	 * Trigger the init config to enable publishing mode
	 * @param TCc_FormProcessor $form_processor
	 * @return void
	 */
	public static function customFormProcessor_initConfig(TCc_FormProcessor $form_processor): void
	{
		$renderer_class_name  = $form_processor->formValue('renderer_class_name');
		TMv_PageBuilder::enablePublishingMode($renderer_class_name);
		
	}
	
	/**
	 * @return bool|TCm_Model|TMt_PageRenderItem
	 */
	public function model()
	{
		return parent::model();
	}
	
	public function configureFormElements()
	{
		$form_item = new TCv_FormItem_HTMLEditor('cell_content', 'Cell content');
		$form_item->useBasicEditor();
		$form_item->setSavedValue($this->model()->variable($this->variable_name));
		$this->attachView($form_item);
		
		
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			$localization = TMm_LocalizationModule::init();
			
			
			$value_row = $this->model()->variableRowForName($this->variable_name);
			
			
			// Build a section for each language
			foreach($localization->nonDefaultLanguages() as $language => $language_settings)
			{
				$form_item_language = clone $form_item;
				$form_item_language->setHelpText("The translated content for this cell. <br /><br />If this content is numeric and the same in all languages, you can leave this blank and the default language value will be used ");
				$form_item_language->setID($form_item_language->id().'_'.$language);
				$form_item_language->setTitle($form_item_language->title().' ('.$language_settings['title'].')');
				$form_item_language->addClass('localization');
				$form_item_language->addClass('localization_'.$language);
				$form_item_language->addClass($form_item->id().'_row');
				
				if($default_value_localized = $form_item->defaultValueLocalized($language))
				{
					$form_item_language->setDefaultValue($default_value_localized);
				}
				
				if(is_array($value_row))
				{
					// values aren't saved in normal space, so we need to pull them differently
					$form_item_language->setSavedValue($value_row['value_'.$language]);
				}
				else
				{
					$form_item_language->setSavedValue('');
				}
				
				
				$this->attachView($form_item_language);
				
			}
		}
		
		$field = new TCv_FormItem_Hidden('variable_name', 'Variable name');
		$field->setValue($this->variable_name);
		$this->attachView($field);
		
		// Pass through the renderer class name, required for publishing mode
		// Must be used before the model exists
		$field = new TCv_FormItem_Hidden('renderer_class_name', 'Renderer class name');
		$field->setValue($this->model()->rendererClassName());
		$field->setAsPrivateValue();
		$this->attachView($field);
		
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		$form_processor->addConsoleDebug('processing form');
		
		/** @var TMt_PageRenderItem $model */
		$model = $form_processor->model();
		
		$variable_name = $form_processor->formValue('variable_name');
		
		$variable_array =[
			$variable_name => $form_processor->formValue('cell_content')
		];
		
		$additional_values = [];
		
		// Intercept and check for localization
		if(TC_getConfig('use_localization'))
		{
			$localization = TMm_LocalizationModule::init();
			
			// Build a section for each language
			foreach($localization->nonDefaultLanguages() as $language => $language_settings)
			{
				$additional_values[$variable_name] =
				[
					'value_fr' => $form_processor->formValue('cell_content_'.$language),
				];
			}
		}
		
		$model->updateVariablesWithArray($variable_array, $additional_values);
		
		// Update publishing
		$model->renderer()->markAsUnpublished();
		
	}
}