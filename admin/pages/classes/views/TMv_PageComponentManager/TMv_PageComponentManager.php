<?php
class TMv_PageComponentManager extends TCv_List
{
	protected TMm_PageContentList $content_list;
	/**
	 * TMv_PageComponentManager constructor.
	 */
	public function __construct()
	{
		parent::__construct('page_component_manager');
		
		//$this->setModelClass('TMm_CustomForm');
		$this->defineColumns();
		
		$this->content_list = TMm_PageContentList::init();
		
		// Get all the classes that exist, even if they aren't used in anything
		$classes = $this->content_list->viewClassesInPageContent();
		$content_options = $this->content_list->pageContentOptions();
		foreach($content_options as $class_name => $path)
		{
			if(!isset($classes[$class_name]))
			{
				// Create empty item which would be a comma-separated string
				$classes[$class_name] = '';
			}
		}
		
		$this->addClassCSSFile('TMv_PageComponentManager');
		$this->addClassJSFile('TMv_PageComponentManager');
		$this->addClassJSInit('TMv_PageComponentManager');
		
		
		
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns() : void
	{
		$column = new TCv_ListColumn('icon');
		$column->setTitle('Icon');
		$column->setWidthAsPixels(40);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('module');
		$column->setTitle('Module');
		$column->setWidthAsPixels(120);
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('usage');
		$column->setTitle('Usage');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('class_name');
		$column->setTitle('Class');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('visibility');
		$column->setTitle('Visibility');
		$column->setAlignment('right');
		$column->setRightPadding(10);
		$column->setWidthAsPixels(60);
		$this->addTCListColumn($column);

//		$column = new TCv_ListColumn('visible');
//		$column->setTitle('Visible');
//		$column->setContentUsingListMethod('visibleColumn');
//		$this->addTCListColumn($column);
//
	
	}
	
	public function toggleLinkForClass($class_name) : TCv_Link
	{
		$link_id = 'toggle_'.$class_name;
		// Only toggle for visible classes
		$toggle_link = new TCv_Link($link_id);
		$toggle_link->addClass('toggle_link');
		$toggle_link->setIconClassName('fa-toggle-on');
		//$toggle_link->addClassToggle('#'.$link_id, 'view_hidden');
		//$toggle_link->addClassToggle('#toggle_'.$class_name.' svg', 'fa-toggle-off');
		if($this->content_list->viewIsHidden($class_name))
		{
			$toggle_link->setURL('/admin/pages/do/show-page-content-view/?class_name=' . $class_name);
			$toggle_link->setTitle('Show in page builder content options');
			$toggle_link->addClass('view_hidden');
			
		}
		else
		{
			$toggle_link->setURL('/admin/pages/do/hide-page-content-view/?class_name=' . $class_name);
			$toggle_link->setTitle('Hide in page builder content options');
		}
		return $toggle_link;
	}
	

	public function render()
	{
		// Get all the content items
		$content_list  = TMm_PageContentList::init();
		
		$classes = $content_list->viewClassesInPageContent();
		$content_options = $content_list->pageContentOptions();
		
		
		// Loop through them all, adding the ones that aren't used
		foreach($content_options as $class_name => $path)
		{
			if(!isset($classes[$class_name]))
			{
				// Create empty item which would be a comma-separated string
				$classes[$class_name] = '';
			}
		}
		
		// Sort classes by keys, which are class names
		ksort($classes);
		
		// Not look at the base class
		unset($classes['TMv_ContentLayout']);
		unset($classes['TMv_ContentLayoutBlock']);
		unset($classes['TMv_NoContentPreview']); // these are never added themselves
		unset($classes['TMv_PageContentPlaceholder']); // these get converted to views with other classes
		
		// Loop through ALL classes, building the row
		foreach($classes as $class_name => $menu_ids)
		{
			// Deal with non-addable components
			
			// Track display flags
			$is_missing = false;
			$is_used = true;
			
			
			
			$values = [
				'id' => $class_name,
				'class_name' => $class_name,
			];
			
			// If the class exists and it uses the correct trait, we have values to pull
			if(class_exists($class_name) && TC_classUsesTrait($class_name, 'TMt_PagesContentView'))
			{
				/** @var TMt_PagesContentView|TCv_View $class_name */
				
				// We found a non-addable item, don't show it
				if(!$class_name::pageContent_IsAddable())
				{
					continue;
				}
				
				$values['icon'] = '<i class="fas '.$class_name::pageContent_IconCode().'"></i>';
				$values['module'] = $class_name::moduleForClassName()->title();
				$values['title'] = $class_name::pageContent_ViewTitle();
				
				// Visibility can vary
				if(is_subclass_of($class_name,'TMv_ContentLayout'))
				{
					$values['visibility'] = '';
				}
				else
				{
					$link = $this->toggleLinkForClass($class_name);
					$values['visibility'] = $link;
				}
				
				
			}
			else
			{
				$is_missing = true;
				$values['icon'] = '<i class="fas fa-exclamation-circle "></i>';
				$values['module'] = 'Not found';
				$values['title'] = $class_name;
				$values['visibility'] = '';
			}
			
			
			// --- USAGE ---
			
			if(trim($menu_ids) == '')
			{
				$menus = [];
			}
			else
			{
				$menus = explode(',',$menu_ids);
			}
			
			$num_menus = count($menus);
			$usage_view = new TCv_View();
			
			if($num_menus == 0)
			{
				$is_used = false;
				$usage_view->addText('None');
			}
			elseif($num_menus > 5)
			{
				$usage_view->addText($num_menus.' pages');
			}
			else // small enough to create links
			{
				foreach($menus as $menu_id)
				{
					if($menu_id != '')
					{
						if($menu_item = TMm_PagesMenuItem::init($menu_id))
						{
							$link = new TCv_Link();
							$link->addClass('menu_link');
							$link->setURL('/admin/pages/do/page-builder/' . $menu_id);
							
							$titles = array();
							foreach($menu_item->ancestors() as $ancestor)
							{
								$titles[] = $ancestor->title();
							}
							
							
							$link->addText(implode(' – ', $titles));
						}
						$usage_view->attachView($link);
					}
				}
			}
			
			$values['usage'] = $usage_view;
			
			
			
			// --- CREATE THE ROW ---
			
			$row = $this->rowForArrayValues($values);
			if($is_missing)
			{
				$row->addClass('missing');
			}
			if(!$is_used)
			{
				$row->addClass('not_used');
				
			}
			$this->addTCListRow($row);
		}
		parent::render();
		
	}
}