class TMv_PageComponentManager
{
	constructor(element, params) {

		// Get all the toggle links and add an event listener for toggling
		element.querySelectorAll('.toggle_link').forEach(el => {
			el.addEventListener('click', this.toggleHandler.bind(this));
		});
	}

	toggleHandler(event) {
		let button = event.target.closest('a');

		// Toggle the class
		button.classList.toggle('view_hidden');

		// Trigger the call
		fetch(button.getAttribute('href'),{
			method : 'GET',
		});

		// Prevent the execution
		event.preventDefault();
	}
}