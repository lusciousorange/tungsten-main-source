<?php

/**
 * A form to show a 301 redirect configuration
 */
class TMv_Pages301RedirectForm extends TCv_FormWithModel
{
	protected $target_model;
	/**
	 * TMv_Pages301RedirectForm constructor.
	 * @param string|TMm_Pages301Redirect|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		// editor mode
		if($model instanceof TMm_Pages301Redirect)
		{
			parent::__construct($model);
			$this->target_model = $model->targetModel();
			$this->setSuccessURL('page-301-redirect-list',$this->target_model);
		}
		elseif($model instanceof TCm_Model)
		{
			$this->target_model = $model;
			parent::__construct('');
			$this->setSuccessURL('page-301-redirect-list');
		}
		else // normal creator
		{
			parent::__construct('');
			$this->setSuccessURL('301-redirect-list');
			
		}
		
	}
	
	/**
	 * Set the required model for the form
	 * @return class-string<TCm_Model>|null
	 */
	public static function formRequiredModelName(): ?string
	{
		return 'TMm_Pages301Redirect';
	}
	
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('source_url', 'Source URL');
		$field->setHelpText('The exact URL that is being detected, without the domain.');
		$field->setPlaceholderText('/path/from/site/root/?maybe=option');
		$field->setIsRequired();
		$field->setNoSpaces();
		$this->attachView($field);
		
		// We have something existing. Either creating something that's tied to an existing target or editing one
		if($this->target_model)
		{
			// Add the target details
			$field = new TCv_FormItem_HTML('item_title','Target');
			
			$view = new TCv_View();
			$view->setTag('p');
			$view->addText($this->target_model::modelTitleSingular().': '. $this->target_model->title());
			$view->addText('<br />');
			$link = new TCv_Link();
			$link->addText($this->target_model->pageViewURLPath());
			$link->setURL($this->target_model->pageViewURLPath());
			$view->attachView($link);
			
			$field->attachView($view);
			$this->attachView($field);
			
			// Add the fields
			// Editors only see the title, creators see all the pages
			if($this->target_model instanceof TMm_PagesMenuItem)
			{
				$field = new TCv_FormItem_Hidden('target_page_menu_id', '');
				$field->setValue($this->target_model->id());
				$this->attachView($field);
			}
			elseif($this->target_model instanceof TCm_Model) // NON-PAGES model provided. These are 301s for other models
			{
				$field = new TCv_FormItem_Hidden('class_name', '');
				$field->setValue($this->target_model->baseClassName());
				$this->attachView($field);
				
				$field = new TCv_FormItem_Hidden('item_id', '');
				$field->setValue($this->target_model->id());
				$this->attachView($field);
				
			}
		}
		else // CREATE PAGE no restriction
		{
			$field = new TCv_FormItem_Select('target_page_menu_id','Target page');
			$field->setHelpText('Select the menu that this 301 will redirect to');
			$field->setIsRequired();
			$field->addClass('target_option');
			$field->useFiltering();
			$field->addOption('','Select a target page');
			
			$menu_0 = TMm_PagesMenuItem::init(0);
			foreach($menu_0->descendants() as $menu_item)
			{
				$titles = array();
				foreach($menu_item->ancestors() as $ancestor)
				{
					$titles[] = $ancestor->title();
				}
				
				$field->addOption($menu_item->id(), implode(' – ', $titles));
			}
			$this->attachView($field);
		}
//		$field = new TCv_FormItem_Select('target_page_menu_id','Target Page');
//		$field->setHelpText('Select the menu that this 301 will redirect to');
//		$field->setIsRequired();
//		$field->addClass('target_option');
//		if($this->target_page instanceof TMm_PagesMenuItem)
//		{
//			$field->addOption($this->target_page->id(), $this->target_page->title());
//		}
//		else
//		{
//			$field->useFiltering();
//			/** @var TMm_PagesMenuItem $menu_0 */
//			$menu_0 = TMm_PagesMenuItem::init(0);
//			foreach($menu_0->descendants() as $menu_item)
//			{
//				$titles = array();
//				foreach($menu_item->ancestors() as $ancestor)
//				{
//					$titles[] = $ancestor->title();
//				}
//
//				$field->addOption($menu_item->id(), implode(' – ', $titles));
//			}
//
//		}
//		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('url_suffix', 'Additional text after URL');
		$field->setHelpText('Text that should be appended to the end of the target URL. ');
		$field->setPlaceholderText("text_after_URL_to_menu_item");
		$field->setNoSpaces();
		$this->attachView($field);



	}
	
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		
		$source = $form_processor->formValue('source_url');
		$cleaned = TMm_Pages301Redirect::cleanSourceURL($source);
		$form_processor->addDatabaseValue('source_url_cleaned', $cleaned);

	}
	
}