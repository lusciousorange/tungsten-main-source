<?php
/**
 * Class TMv_PageMenuItem_NavigationForm
 *
 * The form to edit a menu item's navigation. This is also used for
 */
class TMv_PageMenuItem_AttributesForm extends TCv_FormWithModel
{
	
	/**
	 * TMv_PageMenuItem_NavigationForm constructor.
	 * @param TMm_PagesMenuItem|string $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setButtonText('Update page');
		$this->setIDAttribute('TMv_PageMenuItem_AttributesForm');
	}
	
	/**
	 * Set the required model for the form
	 * @return string|null
	 */
	public static function formRequiredModelName(): ?string
	{
		return 'TMm_PagesMenuItem';
	}
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
		$title = new TCv_FormItem_TextField('title', 'Menu title');
		$title->setHelpText('The title of the menu as it will appear in the navigation system.');
		$title->setIsRequired();
		//$title->setAsHalfWidth();
		$this->attachView($title);

//		$override_title = new TCv_FormItem_TextField('title_override','Page Title');
//		$override_title->setHelpText('Leave blank to use Menu Title. <br /><br />This is a title for this page that should be should be shown outside of navigation items. This will appear as the title on the page. If blank, the menu title is used as a default. ');
//		$this->attachView($override_title);
		
		
		$folder = new TCv_FormItem_TextField('folder', 'Folder');
		//$folder->setAsHalfWidth();
		$folder->setAsRequiresURLSafe();
		$folder->setIsRequired();
		$folder->disableAutoComplete();
		$folder->setHelpText('Only letters, numbers, dash, or underscore are allowed.');
		
		// Preview that gets filled in
		$target_page_link = new TCv_Link('target_page_link');
		$target_page_link->openInNewWindow();
		$folder->attachViewAfter($target_page_link);
		$this->attachView($folder);


//		$target_page = new TCv_FormItem_HTML('target_page', 'Page URL');
//		$target_page->setHTML('<a target="_blank" id="target_page_link" href=""></a>');
//		$target_page->setSaveToDatabase(false);
//		$target_page->setHelpText(' ');
//		$this->attachView($target_page);
		
		
		$field = new TCv_FormItem_Select('parent_id', 'Parent menu');
		$field->addOption('0', 'Top-Level');
		$field->setHelpText("The menu that this page belongs to. Changing this value will move this page to be a child of the selected parent menu");
		$field->useFiltering();
		
		/** @var TMm_PagesMenuItem $menu */
		$menu = $this->model();
		$menu_list = TMm_PagesMenuItemList::init();
		foreach($menu_list->items() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
			
			if($menu)
			{
				if($menu_item->id() == $menu->id() || $menu_item->menuItemIsAncestor($menu))
				{
					
					$field->addOptionAttribute($menu_item->id(), 'disabled', 'disabled');
				}
			}
		}
		$this->attachView($field);
		
		
	}
	
	
}