<?php

/**
 * Class TMv_PagesSettingsForm
 */
class TMv_PagesSettingsForm extends TSv_ModuleSettingsForm
{
	/**
	 * TMv_PagesSettingsForm constructor.
	 * @param string|TSm_Module $module
	 */
	public function __construct($module)
	{	
		parent::__construct($module);


	}

	/**
	 * THe configuration settings
	 */
	public function configureFormElements()
	{
		$website_title = new TCv_FormItem_TextField('website_title', 'Website title');
		$website_title->setHelpText('The main title of your website.');
		$website_title->setDefaultValue('Website title');
		$this->attachView($website_title);
		
		$description = new TCv_FormItem_TextBox('website_description', 'Website description');
		$description->setHelpText('The description of your website.');
		$this->attachView($description);
		
		$field = new TCv_FormItem_Select('use_301_redirects', '301 redirects');
		$field->setHelpText('Indicate if the site is replacing an existing site and 301 redirects are used.');
		$field->addOption('0', 'Disabled');
		$field->addOption('1', 'Yes - Website shows 301 redirect section');
		$field->setDefaultValue('0');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('use_bulk_import', 'Bulk import');
		$field->setHelpText('Indicate if the site enables the tools for bulk importing page information.');
		$field->addOption('0', 'Disabled');
		$field->addOption('1', 'Yes - Website allows bulk import of page information');
		$field->setDefaultValue('0');
		$this->attachView($field);
		
		
		$this->attachAppearanceFields();

		$this->attachGDRPFields();


	}

	/**
	 * Attaches the appearance fields
	 */
	public function attachAppearanceFields()
	{
		$website_title = new TCv_FormItem_Heading('appearance_heading', 'Appearance');
		$this->attachView($website_title);


		/** @var TMm_PageThemeList $theme_list */
		$theme_list = TMm_PageThemeList::init();

		$theme = new TCv_FormItem_Select('website_theme', 'Website theme');
		$theme->setHelpText('Select the name of the theme that should be loaded for the website.');
		$theme->addOption('', 'NONE – Disable public website');
		
		$theme_editor = new TCv_FormItem_Select('website_theme_editor', 'Editor theme');
		$theme_editor->setHelpText('Select the theme that will be used in the page editor.');
		$theme_editor->addOption('', 'DEFAULT – Match the value for website.');

		foreach($theme_list->availableThemes() as $class_name => $path)
		{
			$name = $class_name;
			if(property_exists($class_name, 'theme_name'))
			{
				$name = $class_name::$theme_name;
			}

			$theme->addOption($class_name, $name);
			$theme_editor->addOption($class_name, $name);
		}
		$this->attachView($theme);
		$this->attachView($theme_editor);
		
		
		if(TC_currentUserIsDeveloper())
		{
			$image_crop_width = new TCv_FormItem_TextField('image_crop_width', 'Page photo crop width');
			$image_crop_width->setHelpText('The default cropping width for page photos. If the width or height is set to -1, then the option to add images to the settings of a page are disabled. If the value is set to 0, then no cropping is used.');
			$image_crop_width->setDefaultValue('1200');
			$this->attachView($image_crop_width);
			
			$image_crop_width = new TCv_FormItem_TextField('image_crop_height', 'Page photo crop height');
			$image_crop_width->setHelpText('The default cropping height for page photos.');
			$image_crop_width->setDefaultValue('400');
			$this->attachView($image_crop_width);
			
			
			$single_page_site = new TCv_FormItem_Select('single_page', 'Single page site');
			$single_page_site->setHelpText('Indicate the page that should load when editing in the Pages module.');
			$single_page_site->addOption('', 'No - This site is not a single-page');
			$page_list = TMm_PagesMenuItemList::init();
			foreach($page_list->items() as $page_item)
			{
				$single_page_site->addOption($page_item->id(), str_repeat('--', $page_item->level() - 1) . $page_item->title());
			}
			$this->attachView($single_page_site);
			
			if(TC_getConfig('use_pages_hero_boxes'))
			{
				$image_crop_width = new TCv_FormItem_TextField('hero_box_image_crop_height', 'Hero box crop height');
				$image_crop_width->setHelpText('The default cropping height for hero boxes. The width will always match the page photo height. A value of 0 indicates that hero boxes are not used on the website.');
				$image_crop_width->setDefaultValue('0');
				$this->attachView($image_crop_width);
			}
			
		}
	}

	
	protected function attachGDRPFields()
	{
		
		
		$field = new TCv_FormItem_Heading('gdpr_heading', 'Privacy warning and GDPR');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('use_GDPR', 'Use privacy warning');
		$field->setHelpText("Indicate if this website should show a privacy warning when the website is first loaded.");
		$field->addOption(0,'No – Do not show GDPR message');
		$field->addOption(1,'Yes – Show the GDPR message');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('gdpr_learn_more','GDPR more info page');
		$field->setVisibilityToFieldValue('use_GDPR',1);
		$field->setHelpText('The page that someone is directed to if they want to learn more. ');
		$field->addOption('', 'Select a Menu');
		$field->useFiltering();
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('gdpr_message','GDPR message');
		$field->setIsRequired(false);
		$field->setVisibilityToFieldValue('use_GDPR',1);
		$this->attachView($field);
	}

}
?>