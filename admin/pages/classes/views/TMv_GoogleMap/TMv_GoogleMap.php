<?php

class TMv_GoogleMap extends TCv_View
{
	use TMt_PagesContentView;
	
	
	protected $url = '';
	protected $width = '100%';
	protected $height = '320';

	
	public function __construct()
	{
		
		parent::__construct();
		
	}

	/**
	 * Sets the embed URL for this view
	 * @param string $url
	 */
	public function setURL($url)
	{
		$this->url = $url;
	}

	/**
	 * Returns the URL for the map.
	 * @return string
	 */
	public function mapURL()
	{
		// Parse out anything that has the iframe
		if(strpos($this->url, 'iframe') > 0)
		{

			$parts = explode('"', $this->url);
			foreach($parts as $part)
			{
				if(substr(trim($part), 0, 4) == 'http')
				{
					$this->url = $part;
					$this->url = str_ireplace('&#','', $this->url);
					return $this->url;
				}
			}
		}
		
		$this->url = str_ireplace('&#','', $this->url);
		
		return $this->url;

	}


	/**
	 * Returns the HTML for the view
	 * @return string
	 */
	public function html()
	{
		$this->addClass('responsive_iframe');
		
		$map = new TCv_View();
		$map->setTag('iframe');
		$map->setAttribute('src', $this->mapURL());
		$map->setAttribute('width', $this->width);
		$map->setAttribute('height', $this->height);
		$map->setAttribute('frameborder', '0');
		$map->setAttribute('style', 'border:0');
		$map->setAttribute('marginheight', '0');
		$map->setAttribute('marginwidth', '0');
		$map->setAttribute('webkitAllowFullScreen', 'webkitAllowFullScreen');
		$map->setAttribute('mozallowfullscreen', 'mozallowfullscreen');
		$map->setAttribute('allowFullScreen', 'allowFullScreen');
		$this->attachView($map);
		
		return parent::html();
	}


	/**
	 * Returns the array of form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
	
		$field = new TCv_FormItem_TextField('url', 'Embed URL');
		$field->setDefaultValue($this->url);
		$field->setAsAllowsHTML();
		$field->setHelpText('The URL or code provided when you try to "embed" a map. Unfortunately, share links won\'t work. You can copy in the entire iframe tag.');
		$form_items[] = $field;
		
		$field = new TCv_FormItem_TextField('width','Width');
		$field->setHelpText('The width in pixels or percentage of how wide the map should be.');
		$field->setIsRequired();
		$field->setDefaultValue($this->width);
		$form_items[] = $field;

		$field = new TCv_FormItem_TextField('height','Height');
		$field->setHelpText('The height in pixels of how tall the map should be.');
		$field->setIsRequired();
		$field->setDefaultValue($this->height);
		$form_items[] = $field;

		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string  { return 'Google map'; }
	public static function pageContent_IconCode() : string  { return 'fa-map-marker'; }

	public static function pageContent_ViewDescription()  : string
	{ 
		return 'An embedded Google Map';
	}
	
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return true;
	}
	
}

?>