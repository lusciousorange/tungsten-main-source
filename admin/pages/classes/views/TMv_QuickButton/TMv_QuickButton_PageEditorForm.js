class TMv_QuickButton_PageEditorForm  extends TCv_Form {

	/**
	 *
	 * @param {HTMLFormElement} element
	 * @param params
	 */
	constructor(element, params)
	{
		super(element, params);

		this.methods = params.methods;
		this.saved_id = params.saved_id;

		element.querySelector('select#target').addEventListener('change', (e) => this.targetChanged(e));
		this.targetChanged();

	}


	/**
	 * Triggered whenever the target changes. Adjusts which fields are shown
	 */
	targetChanged()
	{
		let field = this.element.querySelector('select#target');
		let field_value = field.value;

		this.hideFieldRows('.target_option');

		// Show/hide based on values
		// uses css classes for targeting to show/hide both
		if(field_value === 'menu_item')
		{
			this.showFieldRows('redirect_to_menu');
		}
		else if(field_value === 'another_website')
		{
			this.showFieldRows('.redirect_url_row');
		}
		else if(field_value === 'file')
		{
			this.showFieldRows('.linked_file_row');
		}
		else if(field_value === 'page_template_value')
		{
			this.showFieldRows('.page_template_value_row');
		}

	}



}
