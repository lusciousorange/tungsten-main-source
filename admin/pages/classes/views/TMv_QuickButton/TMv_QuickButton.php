<?php
/**
 * Class TMv_QuickButton
 */
class TMv_QuickButton extends TCv_Link 
{
	use TMt_PagesContentView;
	
	protected $upload_folder = '/assets/pages/';
	protected $title = '';
	protected $icon = '';
	protected $target = '';
	protected $new_window = 0;
	protected $url_append = '';
	protected $redirect_to_menu, $redirect_to_url, $other_icon, $page_template_value;
	protected ?string $redirect_url;
	protected ?string $linked_file;
	
	protected $icon_only = 0;
	
	// DEPRECATED VALUE STILL EXISTS in DBs
	protected bool $track_outbound;
	
	// Detect if we're generating the quick button from the page builder. Otherwise it's being generated in code
	protected $is_page_builder = false;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_QuickButton');


	}
	
	/**
	 * A hook method that can be called to perform changes to the class prior to rendering but after initialization.
	 * This can be used to customize the view based on values provided in the editor.
	 *
	 */
	public function configureForPageView()
	{
		// Turn off the manual setting since we're using page builder
		$this->is_page_builder = true;
	}
	
	
	/**
	 * Sets the upload folder
	 * @param string $upload_folder
	 */
	public function setUploadFolder($upload_folder)
	{
		$this->upload_folder = $upload_folder;
	}

	/**
	 * Sets the target value
	 * @param string $value
	 */
	public function setTargetValue($value)
	{
		$this->target = $value;
	}

	/**
	 * Sets the redirect to menu id
	 * @param string $menu_id
	 */
	public function setRedirectToMenu($menu_id)
	{
		$this->redirect_to_menu = $menu_id;
	}

	/**
	 * Sets the redirect to menu id
	 * @param string $url
	 */
	public function setRedirectToURL($url)
	{
		$this->redirect_url = $url;
	}

	/**
	 * Override to insert into the title
	 * @param string $text
	 * @param bool $disable_template_parsing Indicates if template parsing should be disabled.
	 */
	public function addText($text, $disable_template_parsing = false)
	{
		$this->title .= $text;
	}
	
	/**
	 * Sets the icon code
	 * @param string $icon_code Usually a FontAwesome icon code
	 */
	public function setIconCode($icon_code)
	{
		$this->icon = $icon_code;
	}

	/**
	 * Sets this link to have an icon which will be prepended to the start of the link text in an <i> tag.
	 *
	 * @param string $icon
	 * @param bool|string $symbol_id The ID for this symbol
	 */
	public function setIconClassName($icon, $symbol_id = false)
	{
		$this->icon = $icon;
	}

	/**
	 * Returns the icon code
	 * @return bool|string
	 */
	public function iconCode()
	{
		if($this->icon == 'none' || $this->icon == '')
		{
			return false;
		}

		// Legacy, NO longer supported
		if($this->icon == 'other')
		{
			return 'fa-'.str_ireplace('fa-', '', $this->other_icon);
		}
		
		return $this->icon;
	}

	/**
	 * Extends the functionality
	 * @param string $url
	 */
	public function setURL($url)
	{
		if($this->url == '')
		{
			$this->url = $url;
		}
	}

	/**
	 * Returns the URL string for the button
	 * @return string
	 */
	public function buttonURL()
	{
		// override from a manual implementation
		if($this->url !== false)
		{
			// ensure processing
			return '';//$text->text();
		}
		
		$url = '#';
		if($this->target == 'menu_item')
		{
			// We must detect if there's a pageviewcode, in which case we just include that
			if(substr($this->redirect_to_menu,0,12) == '/{{pageview:')
			{
				$url = $this->redirect_to_menu; // parsed by the page renderer
			}
			else // legacy menu item, so find the ID and update it
			{
				$url = '/{{pageview:TMm_PagesMenuItem--'.$this->redirect_to_menu.'}}';
				
				// Not all buttons are associated with a content item
				if($this->contentItem())
				{
					$this->contentItem()->updateVariable('redirect_to_menu', $url);
				}
				
				
			}
			
		}
		elseif($this->target == 'another_website')
		{
			$url = $this->localizeProperty('redirect_url');
		}
		elseif($this->target == 'page_template_value')
		{
			/** @var TMm_PagesTemplateSetting $template_setting */
			$template_setting = TMm_PagesTemplateSetting::init($this->page_template_value);
			$url = $template_setting->value();
			//$url = $this->localizeProperty('redirect_url');
		}
		elseif($this->target == 'file')
		{
			$filename = $this->localizeProperty('linked_file');

			$upload_folder = $this->upload_folder;
			$server_root = false;
			$content_item = $this->contentItem();
			if($content_item instanceof TMm_PageRendererContent)
			{
				$upload_folder = $content_item::uploadFolder();
				$server_root = true;
			}
			
			// Load the file, Needed to deal with old /assets/ folder switch
			$file = new TCm_File($this->id().'file', $filename, $upload_folder,$server_root);

			$url = $file->filenameFromSiteRoot();

		}

		$url .= $this->url_append;
		return $url;
		
	}
	
	/**
	 * Returns the title span for this button
	 * @return TCv_View
	 */
	protected function titleSpan() : TCv_View
	{
		$title = new TCv_View();
		$title->addClass('title');
		$title->setTag('span');
		if($this->is_page_builder)
		{
			$title->addText($this->localizeProperty('title'));
		}
		else
		{
			$title->addText($this->title);
		}
		
		return $title;
	}
	
	
	
	public function render()
	{
		// ICON
		if($icon_code = $this->iconCode())
		{
			$icon = new TCv_View();
			$icon->addClass($icon_code);
			$icon->setTag('span');
			$this->attachView($icon);

			$this->addClass('has_icon');

		}
		if($this->title == '')
		{
			$this->addClass('no_title');
		}
		
		
		// TITLE
		// Attach the title span which should only appear if we want to show it. So icon_only isn't set in most cases
		// with legacy, but new ones must decide if it appears. If set to no, only the icon appears so we don't want
		// the title span. This still applies the title attribute
		if(!$this->icon_only)
		{
			$this->attachView($this->titleSpan());
			
			// We've attached a title, it has text, don't need the title attribute
			$this->title = '';
		}
		else
		{
			$this->addClass('icon_only');
		}
		
		
		
		$this->setURL($this->buttonURL());

		if($this->target == 'none')
		{
			$this->setTag('span');
		}


		if($this->new_window)
		{
			$this->openInNewWindow();
		}
		
		
		
	}
	
	


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	public function pageContent_EditorFormItems() : array
	{
		
		$form_items = array();
		
		$title = new TCv_FormItem_TextField('title','Title');
		$title->setHelpText('The title displayed on the button.');
		//$title->setDefaultValue('Button Title');
		$title->setPlaceholderText('Button Text');
		
		$this->addClassJSFile('TMv_QuickButton_PageEditorForm');
		$this->addClassJSInit('TMv_QuickButton_PageEditorForm');
		
		
		
		$form_items['title'] = $title;
		
		$target = new TCv_FormItem_Select('target','Target');
		$target->setHelpText('Indicate what you want the button to target when pressed.');
		$target->addOption('menu_item', 'PAGE - Link to a specific page on this website');
		$target->addOption('file', 'FILE - Link to a file on this website ');
		$target->addOption('another_website', 'SITE – Link to a website outside of this one ');
		$target->addOption('page_template_value', 'TEMPLATE VALUE – Template value in defined Pages Settings.');
		$target->addOption('none', 'NONE - No linking');
		$form_items['target'] = $target;
		
		
		$redirect = new TMv_FormItem_LinkablePageSelect('redirect_to_menu','Page');
		$redirect->setHelpText('Select the menu that this will redirect to');
		$redirect->addClass('target_option');
		$redirect->addOption('','Select a page');
		$form_items['redirect_to_menu'] = $redirect;
		
		$title = new TCv_FormItem_TextField('redirect_url','Website Address');
		$title->setHelpText('The website address or URL, which should start with http://.');
		$title->addClass('target_option');
		$title->setDefaultValue('http://');
		$form_items['redirect_url'] = $title;

		$field = new TCv_FormItem_Select('page_template_value','Page Template Value');
		$field->setHelpText('Select the page template value. ');
		$field->addClass('target_option');

		/** @var TMm_PagesTemplateSettingList $setting_list */
		$setting_list = TMm_PagesTemplateSettingList::init();
		foreach($setting_list->models() as $setting)
		{
			$field->addOption($setting->code(), $setting->code().' – '.$setting->value());
		}

		$form_items['page_template_value'] = $field;

		$link = new TCv_FormItem_FileDrop('linked_file','Linked File');
		$link->setHelpText('The file that will be linked to.');
		$link->addClass('target_option');

		$form_items['linked_file'] = $link;
		
		
		$field = new TSv_FontAwesome_FormItem_SelectIcon('icon', 'Icon');
		$field->setEmptyTitle('No Icon');
		$field->setHelpText("Search for an icon name, a preview will appear. ");
		$form_items['icon'] = $field;
		
		$field = new TCv_FormItem_Select('icon_only','Show title with icon');
		$field->setHelpText('Indicate if the title should be shown with the icon.');
		$field->addOption('0','Yes – Text and icon are both shown');
		$field->addOption('1','No – Only show the icon');
		$field->setVisibilityToFieldValueIsSet('icon');
		$form_items['icon_presentation'] = $field;
		
		
		$field = new TCv_FormItem_Heading('extra_features_heading','Additional Features');
		$form_items[] = $field;
		
		$new_window = new TCv_FormItem_Select('new_window','Open In New Window');
		$new_window->setHelpText('Turn this on to ensure the linked page opens in a new window instead of the current one.');
		$new_window->addOption('0','NO - Opens in the same window');
		$new_window->addOption('1','YES - Opens in the NEW window');
		$form_items['new_window'] = $new_window;
		
		$title = new TCv_FormItem_TextField('url_append','URL Append');
		$title->setHelpText('A custom field to allow text to be appended to the end of the URL.');
		$form_items['url_append'] = $title;
		
		
		

		


		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string  { return 'Button / icon'; }
	public static function pageContent_IconCode() : string  { return 'fas fa-hand-point-up'; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return true; }
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A photo or other file which can be uploaded and linked to.'; 
	}
	
	//////////////////////////////////////////////////////
	//
	// LOCALIZATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of values which defines the values that localized for this page content view. If there is a
	 * form field for these values, then it will be presented in the editor form as well.
	 * @return bool[] An associative array of booleans . The indices must be the variable name and the boolean
	 * indicates if it should "match" the 'required' setting for the field.
	 */
	public static function localizedPageContentSettings() : array
	{
		return [
			'title' => true,
			'linked_file' => false,
			'redirect_url' => false,
		
		];
	}

}

?>