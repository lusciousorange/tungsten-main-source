<?php

/**
 * Class TMv_PrivacyNotification
 *
 * The notification that shows up on many websites which is normally configured for GDPR-style warnings.
 */
class TMv_PrivacyNotification extends TCv_View
{
	public function html()
	{
		
		// Detect the consent cookie and if so, don't bother with anything
		if(isset($_COOKIE['TC_consent']) && $_COOKIE['TC_consent'] == 'true')
		{
			return '';
		}
		
		return parent::html();
	}
	
	public function render()
	{
		parent::render();
		
		
		$this->addClassCSSFile('TMv_PrivacyNotification');
		$this->addClassJSFile('TMv_PrivacyNotification');
		$this->addClassJSInit('TMv_PrivacyNotification');
		
		$message = new TCv_View('gdpr_message');
		$message->addText(TC_getModuleConfig('pages','gdpr_message'));
		$this->attachView($message);
		
		$button_container = new TCv_View();
		$button_container->addClass('gdpr_buttons');
		
		
		$learn_more_menu_id = TC_getModuleConfig('pages','gdpr_learn_more');
		$menu = TMm_PagesMenuItem::init($learn_more_menu_id);
		if( $learn_more_menu_id > 0 && $menu)
		{
			$learn_more_button = new TCv_Link('gdpr_learn_more');
			$learn_more_button->setURL($menu->pathToFolder());
			$learn_more_button->addText(TC_localize('gdpr_learn_more','View policy'));
			$button_container->attachView($learn_more_button);
			
		}
		
		$ok_button = new TCv_Link('gdpr_ok');
		$ok_button->addText(TC_localize('gdpr_agree','Ok'));
		$ok_button->setURL('#');
		$button_container->attachView($ok_button);
		
		$this->attachView($button_container);
		
		
	}
}