class TMv_PrivacyNotification {

	element;

	constructor(element, params) {
		this.element = element;
		this.element.querySelector('#gdpr_ok').addEventListener('click', this.okClickHandler.bind(this));
	}

	/**
	 * Click handler for the ok button
	 * @param event
	 */
	okClickHandler(event) {

		// Track consent
		this.trackConsent();

		// Remove the privacy notification
		this.element.remove();
		event.preventDefault();
	}


	// Tracks the consent value
	trackConsent() {
		document.cookie = `TC_consent=true; path=/; max-age=31536000;`;

		// Update the consent, if google tags is being used
		if(typeof gtag === 'function')
		{
			gtag('consent', 'update', {
				'ad_storage' : 'granted',
				'ad_user_data' : 'granted',
				'ad_personalization' : 'granted',
				'analytics_storage' : 'granted'
			});

		}
	}


}