<?php
class TMv_Pages301RedirectList extends TCv_ModelList
{
	/**
	 * TMv_Pages301RedirectList constructor.
	 * @param TMm_PagesMenuItem|null $menu_item
	 */
	public function __construct($menu_item = null)
	{
		parent::__construct();
		
		if($menu_item instanceof TCm_Model && method_exists($menu_item,'get301Redirects'))
		{
			$this->addModels($menu_item->get301Redirects());
		}
		$this->setModelClass('TMm_Pages301Redirect');
		$this->addClassCSSFile('TMv_Pages301RedirectList');
		
		$this->defineColumns();
		
	}
	
	/**
	 * Defines the columns for the list
	 */
	public function defineColumns()
	{
		
		$title = new TCv_ListColumn('source');
		$title->setTitle('Source');
		$title->setContentUsingListMethod('sourceColumn');
		$this->addTCListColumn($title);
		
		
		$title = new TCv_ListColumn('target');
		$title->setTitle('Target');
		$title->setContentUsingListMethod('targetColumn');
		$this->addTCListColumn($title);
		
		$title = new TCv_ListColumn('source_link');
		$title->setTitle('View');
		$title->setContentUsingListMethod('viewColumn');
		$title->setWidthAsPixels(80);
		$title->setAlignment('center');
		$this->addTCListColumn($title);
		
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
		
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
		
	}
	
	/**
	 * A column for the view
	 * @param TMm_Pages301Redirect $model
	 * @return TCv_Link|TCv_View
	 */
	public function sourceColumn($model)
	{
		$view = new TCv_View();
		
		$module_folder = $model->targetModel()->moduleForThisClass()->folder();
		
		$this->addConsoleDebugObject($model->title(), $model);
		$this->addConsoleDebug($module_folder);
		$link = $this->linkForModuleURLTargetName($model, '301-redirect-edit', $module_folder);
		$link->addText($model->source());
		
		$view->attachView($link);
		
		$cleaned = new TCv_View();
		$cleaned->addClass('cleaned_example');
		
		$cleaned->addText($model->cleanedSourceURL());
		$view->attachView($cleaned);
		return $view;
	}
	
	/**
	 * A column for the view
	 * @param TMm_Pages301Redirect $model
	 * @return TCv_Link|TCv_View
	 */
	public function targetColumn($model)
	{
		$link = new TCv_Link();
		$link->setURL($model->fullTargetUrl());
		$link->openInNewWindow();
		$link->addText($model->fullTargetUrl());
		return $link;
	}
	
	/**
	 * A column for the view
	 * @param TMm_Pages301Redirect $model
	 * @return TCv_Link|TCv_View
	 */
	public function viewColumn($model)
	{
		$link = new TCv_Link();
		$link->setIconClassName('fa-external-link-square-alt');
		$link->setURL($model->cleanedSourceURL());
		$link->openInNewWindow();
		$link->addClass('list_control_button');
		return $link;
	}
	
	/**
	 * @param TMm_Pages301Redirect $model
	 * @return bool|TCv_View
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, '301-redirect-edit', 'fa-pencil');
		
	}
	
	/**
	 * @param TMm_Pages301Redirect $model
	 * @return bool|TCv_View
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, '301-redirect-delete', 'fa-trash');
		
	}
}