<?php
/**
 * Class TMv_PageMenuItem_DisplayForm
 *
 * The form to edit a menu item's navigation.
 */
class TMv_PageMenuItem_DisplayForm extends TCv_FormWithModel
{
	
	/**
	 * TMv_PageMenuItem_DisplayForm constructor.
	 * @param bool|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setButtonText('Update display values');
		$this->setIDAttribute('TMv_PageMenuItem_DisplayForm');
		
	}
	
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
//		$heading = new TCv_FormItem_Heading('navigation_heading', 'Menus');
//		$this->attachView($heading);
		
		$navigation = new TCv_FormItem_Select('navigation', 'Navigation');
		$navigation->setAsHalfWidth();
		$navigation->setUseMultiple(TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
		                            'pages_navigation_matches','navigation_id',
		                            $this->model());
		$navigation->useFiltering();
		$navigation->setHelpText("Indicate where in the navigation, this item appears.");
		$navigation->addOption('', 'Hidden – This menu does not appear in the navigation. ');
		//$is_visible->addOption('1', 'YES – This menu will appear in the navigation.');
		$navigation_types = TMm_PagesNavigationTypeList::init()->models();
		
		foreach($navigation_types as $navigation_type)
		{
			$navigation->addOption($navigation_type->id(), $navigation_type->title());
			
		}
		
		$this->attachView($navigation);
		
		
		$this->attachView(static::contentVisibleField());
		
		$this->attachView(static::authenticationField());
		$this->attachView(null);
		
		
	}
	
	/**
	 * Returns the authentication field. Used in more than one location.
	 * @return TCv_FormItem_RadioButtons|null
	 */
	public static function contentVisibleField() : ?TCv_FormItem_RadioButtons
	{
		$content_visible = new TCv_FormItem_RadioButtons('content_visible', 'Active');
		$content_visible->setIsRequired();
		$content_visible->addHelpText("Indicate if the page and the content is active on the site");
		$content_visible->addOption('0', '<strong>NO:</strong> This page hidden from navigation, sitemaps, generates a 404 Not found error. Children also hidden.');
		$content_visible->addOption('1', '<strong>YES:</strong> This page is visible and can be loaded and will appear in the navigation if set to do so. ');
		
		return $content_visible;
	}
	
	/**
	 * Returns the authentication field. Used in more than one location.
	 * @return TCv_FormItem_RadioButtons|null
	 */
	public static function authenticationField() : ?TCv_FormItem_RadioButtons
	{
		if(TC_getModuleConfig('login', 'show_authentication'))
		{
			$requires_authentication = new TCv_FormItem_RadioButtons('requires_authentication', 'Requires authentication');
			$requires_authentication->setIsRequired();
			$requires_authentication->setHelpText('Indicate if this page will only appear if someone is logged in.');
			$requires_authentication->addOption(0, '<strong>NO:</strong> This page can be loaded without authentication.');
			$requires_authentication->addOption(1, '<strong>YES:</strong> Password-protected. Only loads if a user is signed in.');
			return $requires_authentication;
		}
		
		return null;
	}


	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);

		// Clear the cache associated with page navigation matches
		TC_Memcached::delete('pages_navigation_matches');
		
	}



}