<?php

/**
 * Class TMv_PagesTemplateSettingList
 */
class TMv_PagesTemplateSettingList extends TCv_ModelList
{
	/**
	 * TMv_PagesTemplateSettingList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_PagesTemplateSetting');
		$this->defineColumns();
	}

	/**
	 *
	 */
	public function defineColumns()
	{
		
		$title = new TCv_ListColumn('title');
		$title->setWidthAsPercentage(30);
		$title->setTitle('Title');
		$title->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($title);
		
/*
		$code = new TCv_ListColumn('code');
		$code->setTitle('Code');
		$code->setContentUsingModelMethod('code');
		$this->addTCListColumn($code);
		
*/
		
		$value = new TCv_ListColumn('value');
		$value->setTitle('Value');
		$value->setContentUsingModelMethod('value');
		$this->addTCListColumn($value);
		
		
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
				
	}
	

	public function titleColumn($model)
	{
		if($model->userCanEdit())
		{
			$url_target = TC_URLTargetFromModule( 'pages' , 'template-setting-edit');
			$url_target->setModel($model);
			$link = TSv_ModuleURLTargetLink::init($url_target);
			$link->addText('<br/><em>'.$model->code().'</em>');
			return $link;
			
		}
		
		return $model->title();

	}


	/**
	 * A list control button for the editor
	 * @param $model
	 * @return bool|TCu_Item
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'template-setting-edit', 'fa-pencil');
		
	}

	/**
	 * A list control button configured for deletion
	 * @param $model
	 * @return bool|TCu_Item
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'template-setting-delete', 'fa-trash');
	
	}

	/**
	 * The help view for this list
	 * @return TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('This is a list of settings that your web developer has setup. In most cases these values don\'t change, however this mechanism provides a convenient way to access and change these items if necessary. This provides long-term flexibility to ensure all the content on this website is manageable.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Create New Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('New Template Setting', '.url-target-template-setting-create');
				$p->attachView($link);
			$p->addText(' button to add a new setting. This will not add any content to your website, but instead enable your developer to use this content. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Edit Existing Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('Setting Title', '.TCv_ListColumn_title .url-target-template-setting-edit');
				$p->attachView($link);
			$p->addText(' or ');
				$link = new TSv_HelpLink('', '.url-target-template-setting-edit.list_control_button');
				$link->addClass('fa-pencil');
				$p->attachView($link);
			$p->addText(' to edit the values for a setting. Please note that if you change the "code", you may break existing functionally on your website.');
			$help_view->attachView($p);
			
						
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Delete Setting</strong>: Click on the ');
				$link = new TSv_HelpLink('', '.url-target-template-setting-delete');
				$link->addClass('fa-trash');
				$p->attachView($link);
				
			$p->addText(' button. This is not recommended without speaking to a devloper. If you no longer want a piece of content to appear, you can edit the setting and delete the value.');
			$help_view->attachView($p);
			
		return $help_view;
	}
		
}
?>