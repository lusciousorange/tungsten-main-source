<?php
class TMv_Video extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $alt_text = '';
	protected $video_file = ''; // The name of the image file
	protected $is_placeholder = '0';
	protected $primary_video_file = false;
	
	protected $autoplay = false;
	protected $muted = false;
	protected $loop = false;
	protected $controls = null;
	
	/**
	 * TMv_Video constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_Video');
		
		
	}
	/**
	 * Returns the image file if it exists
	 * @return bool|TCm_File
	 */
	public function imageFile()
	{
		if($this->primary_video_file === false)
		{
			if($content = $this->contentItem())
			{
				$renderer = $content->renderer();
				//$this->primary_image_file = new TCm_File($this->id().'file', $this->filename(), $this->upload_path);
				$this->primary_video_file = new TCm_File(
					$this->id().'file',
					$this->filename(),
					$renderer::uploadFolder(),
					TCm_File::PATH_IS_FROM_SERVER_ROOT);
				
			}
			
		}
		return $this->primary_video_file;
	}
	
	/**
	 * Returns the name of the file
	 * @return string
	 */
	public function filename()
	{
		return $this->video_file;
	}
	
	
	/**
	 * Returns the view of the image for this photo
	 * @return bool|TCv_Image|TCv_View
	 */
	protected function videoView()
	{
		if($this->is_placeholder)
		{
			$view = new TCv_View();
			$view->addClass('placeholder');
			
			$icon = new TCv_View();
			$icon->setTag('i');
			$icon->addClass('fas fa-image');
			$icon->addClass('placeholder_icon');
			$view->attachView($icon);
			
			$text = new TCv_View();
			$text->addClass('explanation');
			$text->addText($this->alt_text);
			$view->attachView($text);
			
			return $view;
			
			
		}
		
		if($file  = $this->imageFile())
		{
			$view = new TCv_View();
			$view->setTag('video');
			$view->setAttribute('src',$file->filenameFromSiteRoot());
			
			if($this->autoplay)
			{
				$view->setAttribute('autoplay','true');
			}
			if($this->muted)
			{
				$view->setAttribute('muted','true');
			}
			if($this->loop)
			{
				$view->setAttribute('loop','true');
			}
			
			// Controls might not be set, so we default to show if it's null
			// Or if the value is explicitly 1
			if($this->controls == null || $this->controls == 1)
			{
				$view->setAttribute('controls','true');
				
			}
			
			return $view;
		}
		
		return false;
		
	}
	
	/**
	 * Generates the HTML for this view
	 * @return string
	 */
	public function render()
	{
		
		if($video_view = $this->videoView())
		{
			$this->attachView($video_view);
		}
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$upload_folder = ($this->renderer)::uploadFolder();
		
		$form_items = array();
		
		$field = new TCv_FormItem_FileDrop('video_file','Video File');
		$field->setHelpText('The video file to be used.');
		$field->setIsRequired(true);
		$field->setPermittedExtensions(['mp4','webm']);
		$field->setUploadFolder($upload_folder);
		$form_items['video_file'] = $field;
		
		$field = new TCv_FormItem_Select('autoplay','Autoplay');
		$field->addOption(0,'No');
		$field->addOption(1,'Yes');
		$form_items['autoplay'] = $field;
		
		$field = new TCv_FormItem_Select('muted','Muted');
		$field->addOption(0,'No');
		$field->addOption(1,'Yes');
		$form_items['muted'] = $field;
		
		$field = new TCv_FormItem_Select('loop','Loop');
		$field->addOption(0,'No');
		$field->addOption(1,'Yes');
		$form_items['loop'] = $field;
		
		$field = new TCv_FormItem_Select('controls','Controls');
		$field->addOption(0,'No');
		$field->addOption(1,'Yes');
		$form_items['controls'] = $field;
		
		$field = new TCv_FormItem_TextField('alt_text','Alt Text');
		$field->setHelpText('Accessible text to describe this photo');
		$form_items['alt_text'] = $field;
		
		
		$field = new TCv_FormItem_Select('is_placeholder','Use Placeholder');
		$field->setHelpText("Show a placeholder with the alt text and no video");
		$field->addOption(0,'No');
		$field->addOption(1,'Yes – Hide the video, show a placeholder with the alt text');
		$form_items['placeholder'] = $field;
		
		return $form_items;
	}
	
	public static function pageContent_ViewTitle() : string  { return 'Video'; }
	public static function pageContent_IconCode() : string  { return 'fa-video'; }
	public static function pageContent_ViewDescription() : string
	{
		return 'A general content layout which is never actually used. All content layouts should extend this class.';
	}
	
}