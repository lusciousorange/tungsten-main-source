<?php
class TMv_PagesNavigationTypeManagerList extends TCv_ModelList
{
	/**
	 * TMv_PagesNavigationTypeManagerList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		
		$this->setModelClass('TMm_PagesNavigationType');
		
		$this->defineColumns();
		
	}
	
	/**
	 * Defines the columns for the list
	 */
	public function defineColumns()
	{
		
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('short_code');
		$column->setTitle('Short code');
		$column->setContentUsingModelMethod('shortCode');
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('is_flat');
		$column->setTitle('Display');
		$column->setContentUsingListMethod('flatColumn');
		$column->setAlignment('center');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('num_pages');
		$column->setTitle('Pages');
		$column->setContentUsingListMethod('numPagesColumn');
		$column->setAlignment('center');
		$this->addTCListColumn($column);
		
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
		
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
		
	}
	
	/**
	 * A column for the view
	 * @param TMm_PagesNavigationType $model
	 * @return
	 */
	public function titleColumn(TMm_PagesNavigationType $model)
	{
		if($model->userCanEdit())
		{
			$link = $this->linkForModuleURLTargetName($model, 'navigation-edit');
			$link->addText($model->title());
			return $link;
		}
		
		return $model->title();
	}
	
	/**
	 * A column for the view
	 * @param TMm_PagesNavigationType $model
	 * @return int
	 */
	public function numPagesColumn(TMm_PagesNavigationType $model)
	{
		
		return count($model->pages());
	}
	
	/**
	 * A column for the view
	 * @param TMm_PagesNavigationType $model
	 * @return string
	 */
	public function flatColumn(TMm_PagesNavigationType $model)
	{
		return $model->isFlat() ? 'Flat' : 'Nested';
	}
	
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'navigation-edit', 'fa-pencil');
		
	}
	
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'navigation-delete', 'fa-trash');
		
	}
	
}