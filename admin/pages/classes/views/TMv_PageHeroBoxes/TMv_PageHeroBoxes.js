class TMv_PageHeroBoxes {

	image_ids = [];
	boxes = {};
	showing = null;
	timer_id = null;
	options = {
		timer_ms		: 5000,
	};
	constructor(element, params) {
		this.element = element;

		Object.assign(this.options, params);

		// Loop through all the boxes
		let is_first = true;
		this.element.querySelectorAll('.TMv_PageHeroBox').forEach(hero_box => {

			let id = hero_box.getAttribute('data-hero-box-id');
			this.image_ids.push(id);
			this.boxes[id] = hero_box;

			if(!is_first)
			{
				hero_box.hide();
			}
			is_first = false;


		});

		this.showHeroBoxWithID(this.image_ids[0]);

		// Add event listeners to buttons
		this.element.querySelector('.hero_box_next_button')?.addEventListener('click', (e) => {
			this.showHeroBoxWithID(this.nextID(), true);
			e.preventDefault();
		});
		this.element.querySelector('.hero_box_previous_button')?.addEventListener('click', (e) => {
			this.showHeroBoxWithID(this.previousID(), true);
			e.preventDefault();
		});



	}

	/**
	 * SHows a hero box ith a specific ID
	 * @param {Number} hero_box_id
	 * @param {Boolean} is_button_click Default false
	 */
	showHeroBoxWithID(hero_box_id, is_button_click = false) {
		// Remove any faded_out classes
		if(is_button_click)
		{
			this.element.querySelectorAll('.faded_out').forEach(el => {el.classList.remove('faded_out'); });
		}


		if (this.image_ids.length > 1)
		{
			// Loop through each of the element
			for (const index in this.boxes)
			{
				let this_box = this.boxes[index];

				// Take the new one and place it behind the other ones and show it
				// The current one will fade out, revealing the new one
				if(index === hero_box_id)
				{
					this_box.style.zIndex = 0;
					this_box.show();

				}
				else if(index == this.showing) // it's the current one
				{
					this_box.style.zIndex = 1;
					this_box.show();
				}
				else // other ones, get hidden
				{
					this_box.hide();
				}
			}

			// Fade out the current one, if it's set
			if(this.showing)
			{
				let showing_box = this.boxes[this.showing];
				showing_box.classList.add('faded_out');

				// Hide them after the fade out is complete
				setTimeout(() => {
					showing_box.hide();
				},310);


			}

			// reset the timer
			this.resetTimerToID(hero_box_id);
		}
		else
		{
			this.showing = hero_box_id;
		}

	}

	/**
	 * Resets the timer on the provided ID
	 * @param {int} id
	 */
	resetTimerToID(id) {
		let old_showing = this.showing;
		this.showing = id;

		let next_id = this.nextID();

		clearTimeout(this.timer_id);
		this.timer_id = setTimeout(() => {
			if(old_showing)
			{
				this.boxes[old_showing].classList.remove('faded_out');

			}
			this.showHeroBoxWithID(next_id);
		}, this.options.timer_ms);
	}

	/**
	 * Calculates the next ID to be shown, which handles wrapping around to the start
	 * @returns int
	 */
	nextID() {
		let index = this.image_ids.indexOf(this.showing) + 1;
		if (this.image_ids[index] === undefined)
		{
			index = 0;
		}

		return this.image_ids[index];

	}

	/**
	 * Returns the previous ID which includes wrapping back to the end
	 * @returns int
	 */
	previousID() {
		let index = this.image_ids.indexOf(this.showing) - 1;
		if (this.image_ids[index] === undefined)
		{
			index = this.image_ids.length - 1;
		}

		return this.image_ids[index];

	}


}