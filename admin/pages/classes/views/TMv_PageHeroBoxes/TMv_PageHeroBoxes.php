<?php
class TMv_PageHeroBoxes extends TCv_View
{
	/** @var TMm_PagesMenuItem $menu_item */
	protected $menu_item;
	protected $timer_ms = 5000;
	

	/**
	 * TMv_PageHeroBoxes constructor.
	 * @param TMm_PagesMenuItem $menu_item
	 */
	public function __construct($menu_item)
	{
		$this->menu_item = $menu_item;
		parent::__construct();
		$this->addClassCSSFile('TMv_PageHeroBoxes');
		$this->addClassJSFile('TMv_PageHeroBoxes');


	}

	/**
	 * Sets the number of milliseconds for the switching to happen.
	 * @param int $ms The number of milliseconds for the timer to switch
	 */
	public function setTimerMilliseconds($ms)
	{
		if($ms > 0)
		{
			$this->timer_ms = $ms;
		}
	}

	/**
	 * Sets the number of milliseconds for the old one to fade out
	 * @param int $ms The number of milliseconds for the timer to switch
	 * @deprecated Fade out set in CSS instead. Extend class.
	 */
	public function setFadeOutMilliseconds($ms)
	{
	
	}

	/**
	 * Sets the number of milliseconds for the new banner to fade in
	 * @param int $ms The number of milliseconds for the timer to switch
	 * @deprecated Fade in not used
	 */
	public function setFadeInMilliseconds($ms)
	{
	
	}

	/**
	 * Returns the button that shows the previous link
	 *
	 * @return TCv_Link
	 */
	public function previousButton()
	{
		$button = new TCv_Link();
		$button->addClass('hero_box_previous_button');
		$button->setURL('#');
		$button->setIconClassName('fa-chevron-left');
		$button->setTitle(TC_localize('hero_previous','Previous'));
		return $button;
	}

	/**
	 * Returns the button that shows the next link
	 *
	 * @return TCv_Link
	 */
	public function nextButton()
	{
		$button = new TCv_Link();
		$button->addClass('hero_box_next_button');
		$button->setURL('#');
		$button->setIconClassName('fa-chevron-right');
		$button->setTitle(TC_localize('hero_next','Next'));
		return $button;
	}

	/**
	 *
	 */
	public function html()
	{
		$this->addClassJSInit('TMv_PageHeroBoxes', [
				'timer_ms' => $this->timer_ms,
			]
		);
		
		// Add CSS Height
		$css = 'height:'.TC_getModuleConfig('pages', 'hero_box_image_crop_height').'px;';
		$this->addCSSLine('hero_box_height','#'.$this->attributeID()." { ".$css."}");
		
		$num_boxes = 0;
		$boxes = new TCv_View();
		$boxes->addClass('hero_box_container');
		foreach($this->menu_item->heroBoxes() as $hero_box)
		{
			if($hero_box->isVisible())
			{
				/** @var TMv_PageHeroBox $hero_box_view */
				$hero_box_view = TMv_PageHeroBox::init($hero_box);
				$hero_box_view->addDataValue('hero-box-id', $hero_box->id());
				$boxes->attachView($hero_box_view);
				$num_boxes++;
			}
		}
		$this->attachView($boxes);
		$this->addClass('num_boxes_'.$num_boxes);
		$this->attachView($this->previousButton());
		$this->attachView($this->nextButton());



		return parent::html();
	}
	

}

?>