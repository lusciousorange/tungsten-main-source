<?php
class TMv_PageMenuContentForm extends TMv_PageRendererContentForm
{

	/**
	 * Configuration of the form
	 */
	public function configureFormElements()
	{
		parent::configureFormElements();

		

		if($this->view_class_name != 'TMv_ContentLayoutBlock' )
		{
			
			if($this->isEditor())
			{
				$field = new TCv_FormItem_HTML('duplicate', 'Duplicate');
				$field->setHelpText('Duplicates this content in the same location.');
				$button = new TCv_Link();
				$button->setURL('/admin/pages/do/duplicate-content/'.$this->model()->id());
				$button->addText('Duplicate this content item');
				$button->useDialog('Are you sure you want to duplicate all the content?');
				$field->attachView($button);
				$this->attachView($field);
			}
			
			
		}
		
		// MOVE SECTION OPTION
		// This is only true for pages in the pages module
		if($this->isEditor())
		{
			if($this->model()->isLayoutSection())
			{
				$field = new TCv_FormItem_Select('move_to_page', 'Move to page');
				$field->addHelpText("Moves this section to the bottom of a different page.");
				$field->setSaveToDatabase(false);
				$field->addOption('','Do Not Move');
				/** @var TMm_PagesMenuItem $menu_0 */
				$menu_0 = TMm_PagesMenuItem::init(0);
				foreach($menu_0->descendants() as $menu_item)
				{
					$titles = array();
					foreach($menu_item->ancestors() as $ancestor)
					{
						$titles[] = $ancestor->title();
					}
					
					$field->addOption($menu_item->id(), implode(' – ', $titles));
				}
				
				// Can't move to self
				$field->disableOption($this->model()->renderer()->id());
				$this->attachView($field);
			}
			
			
		}
	}

	
}