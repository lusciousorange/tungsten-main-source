<?php
/**
 * Class TMv_EditMenuItemForm
 *
 * The form to edit a menu item.
 */
class TMv_EditMenuItemForm extends TCv_FormWithModel
{
	protected $folder_prefix = '';
	protected $crop_width = 0;
	protected $crop_height = 0;
	
	/**
	 * TMv_EditMenuItemForm constructor.
	 * @param bool|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->addClassCSSFile('TMv_EditMenuItemForm');
		$this->addClassJSFile('TMv_EditMenuItemForm');
		
		// Add the JS values
		$this->updateJSClassInitValues(['folder' => $this->folder_prefix . $model->pathToMenu()]);
		
		// Doesn't matter if you add this because the TCv_Form has a JS class it gets triggered regardless
		//$this->addClassJSInit('TMv_EditMenuItemForm',['folder' => $this->folder_prefix . $model->pathToMenu()]);
		
		$this->setSuccessURL('/admin/pages/do/manage/');
		
		$this->crop_width = TC_getModuleConfig('pages', 'image_crop_width');
		$this->crop_height = TC_getModuleConfig('pages', 'image_crop_height');
		
		
	}
	
	
	/**
	 * Configures the form elements for this form
	 */
	public function configureFormElements()
	{
		// ----- MAIN SETTINGS -----
		$this->attachMainSettings();
		
		// ----- APPEARANCE SETTINGS -----
		$this->attachAppearanceSettings();
		
		
		// ----- NAVIGATION SETTINGS -----
		$this->attachNavigationSettings();
		
		// ----- ADVANCED SETTINGS -----
		$this->attachAdvancedSettings();
		
		$this->attachRestrictedAccessSettings();
		
		
		// ----- BUTTON -----
		$submit = new TCv_FormItem_SubmitButton('submit', 'Update menu item');
		$submit->setShowTitleColumn(false);
		$this->setSubmitButton($submit);
		
		$process_class = new TCv_FormItem_Hidden('process_class', 'Process class');
		$process_class->setValue(get_class($this));
		$process_class->setSaveToDatabase(false);
		$this->attachView($process_class);
		
		
	}
	
	protected function attachMainSettings()
	{
		$title = new TCv_FormItem_TextField('title', 'Menu title');
		$title->setHelpText('The title of the menu as it will appear in the navigation system.');
		$title->setIsRequired();
		$title->setAsHalfWidth();
		$this->attachView($title);

//		$override_title = new TCv_FormItem_TextField('title_override','Page Title');
//		$override_title->setHelpText('Leave blank to use Menu Title. <br /><br />This is a title for this page that should be should be shown outside of navigation items. This will appear as the title on the page. If blank, the menu title is used as a default. ');
//		$this->attachView($override_title);
		
		
		$folder = new TCv_FormItem_TextField('folder', 'Folder');
		$folder->setAsHalfWidth();
		$folder->setAsRequiresURLSafe();
		$folder->setIsRequired();
		$folder->disableAutoComplete();
		$folder->setHelpText('Only letters, numbers, dash, or underscore are allowed.');
		
		// Preview that gets filled in
		$target_page_link = new TCv_Link('target_page_link');
		$target_page_link->openInNewWindow();
		$folder->attachViewAfter($target_page_link);
		$this->attachView($folder);
		
		
//		$target_page = new TCv_FormItem_HTML('target_page', 'Page URL');
//		$target_page->setHTML('<a target="_blank" id="target_page_link" href=""></a>');
//		$target_page->setSaveToDatabase(false);
//		$target_page->setHelpText(' ');
//		$this->attachView($target_page);
		
		
		$field = new TCv_FormItem_Select('parent_id', 'Parent menu');
		$field->addOption('0', 'Top-Level');
		$field->useFiltering();
		
		$menu = $this->model();
		$menu_list = TMm_PagesMenuItemList::init();
		foreach($menu_list->items() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
			
			if($menu_item->id() == $menu->id() || $menu_item->menuItemIsAncestor($menu))
			{
				
				$field->addOptionAttribute($menu_item->id(), 'disabled', 'disabled');
			}
			
		}
		$this->attachView($field);
	}
	
	/**
	 * Attaches the form item views related to appearance.
	 */
	protected function attachAppearanceSettings()
	{
		$heading = new TCv_FormItem_Heading('appearance_heading', 'Appearance');
		$this->attachView($heading);
		
		
		if($this->crop_width >= 0 && $this->crop_height >= 0)
		{
			$field = new TCv_FormItem_Select('photo_type', 'Photo type');
			$field->setHelpText("Indicate the type of photo setup for this menu. ");
			$field->addOption('photo', 'Single photo – One photo that can be assigned to this page.');
			$field->addOption('none', 'None – No photo option for this page.');
			if(TC_getConfig('use_pages_hero_boxes') && TC_getModuleConfig('pages', 'hero_box_image_crop_height') > 0)
			{
				$field->addOption('hero_box', 'Hero box – Multiple photos with titles and links.');
			}
			$this->attachView($field);
			
			
			$file = new TCv_FormItem_FileDrop('photo_filename', 'Photo');
			$file->addClass('photo_setting_field');
			$file->setHelpText('A photo to be associated with this page. The use of this photo depends on the site design.');
			
			if($this->crop_width > 0 && $this->crop_height > 0)
			{
				$file->setUseCropper(array($this->crop_width, $this->crop_height));
			}
			
			
			$this->attachView($file);
			
			$field = new TCv_FormItem_TextField('photo_alt_text', 'Alt text');
			$field->addClass('photo_setting_field');
			$field->setHelpText('Accessible text to describe this photo');
			$this->attachView($field);
			
			
			$position = new TCv_FormItem_Select('photo_position', 'Photo position');
			$position->addHelpText('Identify where the header image is "pinned" ');
			$position->addOption('', 'Select an override position');
			$position->addOption('left top', 'Top left');
			$position->addOption('center top', 'Top center');
			$position->addOption('right top', 'Top right');
			$position->addOption('left center', 'Middle left');
			$position->addOption('center center', 'Centered');
			$position->addOption('right center', 'Middle right');
			$position->addOption('left bottom', 'Bottom left');
			$position->addOption('center bottom', 'Bottom center');
			$position->addOption('right bottom', 'Bottom right');
			$this->attachView($position);
			
			
		}
	
		
	}
	
	/**
	 * Attaches the form item views related to appearance.
	 */
	protected function attachNavigationSettings()
	{
		$heading = new TCv_FormItem_Heading('navigation_heading', 'Visibility and navigation ');
		$this->attachView($heading);
		
		$content_visible = new TCv_FormItem_Select('content_visible', 'Active');
		$content_visible->addHelpText("Indicate if the page and the content is active on the site");
		$content_visible->addOption('0', 'NO – Hidden from navigation, sitemaps, generates a 404 Not found error. Children also hidden.');
		$content_visible->addOption('1', 'YES – The content for this page can be loaded, even if the menu is not visible. ');
		
		$this->attachView($content_visible);
		
		
		$navigation = new TCv_FormItem_Select('navigation', 'Navigation');
		$navigation->setAsHalfWidth();
		$navigation->setUseMultiple(TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
		                       'pages_navigation_matches','navigation_id',
		                       $this->model());
		$navigation->useFiltering();
		$navigation->setHelpText("Indicate where in the navigation, this item appears.");
		$navigation->addOption('', 'Hidden – This menu does not appear in the navigation. ');
		//$is_visible->addOption('1', 'YES – This menu will appear in the navigation.');
		$navigation_types = TMm_PagesNavigationTypeList::init()->models();
		
		foreach($navigation_types as $navigation_type)
		{
			$navigation->addOption($navigation_type->id(), $navigation_type->title());
			
		}
		
		$this->attachView($navigation);


		
		$field = new TCv_FormItem_TextField('visibility_condition', 'Visibility condition');
		$field->setHelpText('A system-call string such as {{module_folder.method}} which returns a boolean 
		value that will be used to determine if this items should appear on the website.');
		$this->attachView($field);
		
		if(TC_getModuleConfig('login', 'show_authentication'))
		{
			$requires_authentication = new TCv_FormItem_Select('requires_authentication', 'Requires authentication');
			$requires_authentication->setHelpText('Indicate if this page will only appear if someone is logged in.');
			$requires_authentication->addOption(0, 'NO – This page can be loaded without authentication');
			$requires_authentication->addOption(1, 'YES – Password-protected. Only loads with a logged in user.');
			$this->attachView($requires_authentication);
		}
		
		
		
	}
	
	
	/**
	 * Attaches the form item views related to hero boxes.
	 */
	protected function attachHeroBoxSettings()
	{
		$heading = new TCv_FormItem_Heading('hero_box_heading', 'Hero boxes');
		$this->attachView($heading);
	}
	
	/**
	 * Attaches the form item views related to advanced developer settings
	 */
	protected function attachAdvancedSettings()
	{
		$heading = new TCv_FormItem_Heading('advanced_heading', 'Advanced settings');
		$this->attachView($heading);

//		$content_visible = new TCv_FormItem_Select('content_visible', 'Content Visible');
//		$content_visible->addOption('0', 'NO – The content for this cannot be loaded. ');
//		$content_visible->addOption('1', 'YES – The content for this page can be loaded, even if the menu is not visible. ');
//		$content_visible->addOption('2', 'MATCH the value of the regular navigation ');
//		$content_visible->addOption('3', 'CONDITIONAL (Advanced) – Use a system visibility condition {{folder.method}}.');
//
//		$this->attachView($content_visible);
//
//
//		$field = new TCv_FormItem_TextField('content_visibility_condition', 'Visibility Condition');
//		$field->setHelpText('A system-call string such as {{module_folder.method}} which returns a boolean
//		value that will be used to determine if the content should appear on the website.');
//		$this->attachView($field);
//
		
		
		$options = new TCv_FormItem_Select('content_entry', 'Content management');
		$options->setDefaultValue('manage');
		$options->addOption('manage', 'Use Pages Content (Recommended)');
		$options->addOption('redirect_to_menu', 'Redirects to another menu item');
		$options->addOption('redirect', 'Redirects to a URL');
		$options->addOption('class_method', 'Call a PHP class method (advanced)');
		
		$options->setHelpText('Indicate how menu deals with content. Normally managed in Pages module, sometimes you want the menu to redirect. ');
		$this->attachView($options);
		
		$redirect = new TCv_FormItem_TextField('redirect', 'Redirect URL');
		$redirect->addClass('content_entry_option_field');
		$redirect->setHelpText('Menu opens this URL instead of default');
		$this->attachView($redirect);
		
		$redirect = new TCv_FormItem_Select('redirect_to_menu', 'Redirect menu item');
		$redirect->addClass('content_entry_option_field');
		$redirect->setHelpText('Select the menu that this will redirect to');
		$redirect->useFiltering();
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$this->attachView($redirect);
		
		$class_name = new TCv_FormItem_Heading('redirect_class_name_heading', 'Class name settings');
		$class_name->addClass('content_entry_option_field');
		$class_name->setHelpText('This features requires advanced knowledge of the internal programming of the website.');
		$this->attachView($class_name);
		
		$class_name = new TCv_FormItem_TextField('redirect_class_name', 'Class name');
		$class_name->addClass('content_entry_option_field');
		$class_name->setHelpText('The name of the PHP class that should be called. It will be instantiated with the ID provided through the URL');
		$this->attachView($class_name);
		
		$method_name = new TCv_FormItem_TextField('redirect_class_method', 'Method name');
		$method_name->addClass('content_entry_option_field');
		$method_name->setHelpText('The name of the method in the class that should be trigger. Up to two additional parameters can be passed through the URL using additional slashes to separate them.');
		$this->attachView($method_name);
		
		$class_target = new TCv_FormItem_Select('redirect_class_target', 'Result target page');
		$class_target->addClass('content_entry_option_field');
		$class_target->setHelpText('Select the page that will be displayed after the processing is complete.');
		$class_target->addOption('referrer', 'Return to original page (referrer)');
		
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$class_target->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$this->attachView($class_target);
		
		$field = new TCv_FormItem_TextField('model_class_name', 'Model class name');
		$field->setHelpText('The name of the model class <em>TMm_ClassName</em> that is shown on this page');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('model_class_name_primary_view_page', 'Primary model page');
		$field->setHelpText("Indicate if this menu is the primary location where visitors can perform actions for this model.");
		$field->addOption('', 'None');
		
		$called_class = TMm_PagesMenuItem::classNameForInit();
		
		foreach($called_class::$public_page_model_modes as $code => $explanation)
		{
			$field->addOption($code, $explanation);
		}
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('model_class_name_title_display', 'Class title display');
		$field->setHelpText("Indicate how the title of this menu is displayed in headings and breadcrumbs.");
		$field->addOption(null, 'Model Title');
		$field->addOption('page_title', 'Page Title');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('is_model_list', 'Model list submenus');
		$field->setHelpText("Indicate if this menu should be replaced with a list of items from a module. ");
		$field->addOption('0', 'NO – Regular navigation ');
		$field->addOption('1', 'YES – Menu replaced with a list of menus for a model');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_TextField('model_list_class_name', 'Model list class name');
		$field->setHelpText('The name of the class that has the list items. This is usually a subclass of <em>TCm_ModelList</em>. By default the 
method <em>modelsVisible()</em> is called, but that can be overridden by using a double-colon followed by the method name to be called.');
		$this->attachView($field);
		
		if($this->isEditor())
		{
			$field = new TCv_FormItem_HTML('duplicate', 'Duplicate');
			$field->setHelpText('Duplicates this page.');
			$button = new TCv_Link();
			$button->setURL('/admin/pages/do/duplicate-page/' . $this->model()->id());
			$button->addText('Duplicate this page');
			$button->useDialog('Are you sure you want to duplicate this page and all the content on it?');
			$field->attachView($button);
			$this->attachView($field);
		}
		
		
	}
	
	/**
	 * These settings relate to allowing a specific user group to have access to a page WITHOUT having access to the
	 * entire module. This creates a "white list
	 */
	public function attachRestrictedAccessSettings()
	{
		// Get the pages module
		$pages = TMm_PagesModule::init();
		
		// Exit if they don't have full access
		if(!$pages->userHasFullAccess())
		{
			return;
		}
		
		$field = new TCv_FormItem_Heading('permissions_heading', 'User access');
		$field->setHelpText("These settings relate to allowing access to edit only certain pages for users within specific user groups");
		$this->attachView($field);
		
		$field = new TCv_FormItem_HTML('existing access', 'Existing user groups');
		$field->setHelpText("These user groups have existing full access to the Pages module");
		
		$permitted_user_groups = $pages->permittedUserGroups();
		foreach($permitted_user_groups as $user_group)
		{
			$field->addText($user_group->title().'<br />');
		}
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('user_group_access', 'Permitted user group');
		$field->setHelpText('The comma-separated list of user IDs that are considered super-users which permit them to move an item from any stage, to any stage.');
		$field->setUseMultiple(
			TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
			'pages_user_group_access',
			'group_id',
				$this->model()
		);
		
		$user_group_list = TMm_UserGroupList::init();
		
		
		foreach($user_group_list->groups() as $user_group)
		{
			if(!isset($permitted_user_groups[$user_group->id()]))
			{
				$field->addOption($user_group->id(), $user_group->title());
			}
			
		}
		$field->useFiltering();
		
		$this->attachView($field);
		
		
	}
	

	/**
	 * Validation on the form to ensure bad values aren't saved to the database
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		if($form_processor->formValue('content_entry')  == 'class_method')
		{
			$form_processor->validateIsBlank('redirect_class_name');
			$form_processor->validateIsBlank('redirect_class_method');
			$class_name = $form_processor->formValue('redirect_class_name');
			$method_name = $form_processor->formValue('redirect_class_method');
			if(!class_exists($class_name))
			{
				$form_processor->fail('There is no PHP class with the name "'.$class_name.'" ');
			}
			else // class exists
			{
				if(!method_exists($class_name, $method_name))
				{
					$form_processor->fail('There is no method with the name "'.$method_name.'" in the class "'.$class_name.'" ');
				}
			}
		}

		// Model Class List for is_visible
		if($form_processor->formValue('is_visible')  == '-1')
		{
			$model_list_class_name = $form_processor->formValue('model_list_class_name');

			$class_name = '';
			$model_method = '';
			$parts = explode('::', trim($model_list_class_name));
			if(sizeof($parts) > 1)
			{
				$class_name = $parts[0];
				$model_method = str_ireplace('()','', $parts[1]);

			}
			else
			{
				$class_name = $parts[0];
				$model_method = 'modelsVisible';
			}

			if(!class_exists($class_name))
			{
				$form_processor->failFormItemWithID('model_list_class_name','There is no PHP class with the name "<em>'.$class_name.'</em>" ');
			}
			elseif(!method_exists($class_name, $model_method)) // class exists, but method does not
			{
				$form_processor->failFormItemWithID('model_list_class_name','The class "<em>'.$class_name.'</em>" does not have a method called "<em>'.$model_method.'()</em>"');
			}
		}

		// Validate the model class name
		$model_class_name = $form_processor->formValue('model_class_name');
		if($model_class_name != '')
		{
			// The class the provided oes not exist
			if(!class_exists($model_class_name))
			{
				$form_processor->failFormItemWithID('model_class_name',
				                                    'Class with name <strong>"' . $model_class_name .'"</strong> does not exist');
			}
			
			// Test that the class uses the TMt_PageModelViewable, which is required in more recent versions of Tungsten
			elseif(!TC_classUsesTrait($model_class_name,'TMt_PageModelViewable'))
			{
				$form_processor->failFormItemWithID('model_class_name',
				                                    'Class with name <strong>"' . $model_class_name . '"</strong> requires the trait <strong>"TMt_PageModelViewable"</strong>');
				
			}
		}

		// Clear the cache associated with page navigation matches
		TC_Memcached::delete('pages_navigation_matches');


	}
	
	/**
	 * The help view that explains how this form works
	 * @return TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('This is where you edit the properties of a page.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('');
				$link = new TSv_HelpLink('Title', '#title');
				$p->attachView($link);
			$p->addText(' : The name that will appear in the website navigation. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('');
				$link = new TSv_HelpLink('Folder', '#folder');
				$p->attachView($link);
			$p->addText(' : Shown in the URL, should be short but descriptive using letters, numbers, dashes and underscores.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('');
				$link = new TSv_HelpLink('Menu Visibility', '#is_visible');
				$p->attachView($link);
			$p->addText(' : Indicates if this page appears in the navigation listing. A page can be invisible but still load if the URL is typed in. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('');
				$link = new TSv_HelpLink('Content Visibility', '#content_visible');
				$p->attachView($link);
			$p->addText(' : Indicates if the page content is shown, regardless of the menu visibility. ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('');
				$link = new TSv_HelpLink('Content Management', '#content_entry');
				$p->attachView($link);
			$p->addText(' : Most pages use the default which allows for page content to be loaded using Tungsten\'s Page Builder. Other options allow for the menu item to redirect to another menu item or another page altogether. Lastly, the is a developer option to have a navigation item trigger code. ');
			$help_view->attachView($p);
			
			return $help_view;
	}
	
}