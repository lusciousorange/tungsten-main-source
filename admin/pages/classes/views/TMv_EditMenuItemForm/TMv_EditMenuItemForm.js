class TMv_EditMenuItemForm extends TCv_Form
{
	constructor(element, settings)
	{
		super(element, settings);
		this.domain = window.location.protocol+'//'+window.location.host;
		this.folder = settings.folder;
		this.folder_error = false;

		// Add event listener from TCv_Form JS class
		this.addFieldEventListener('content_entry','change',this.contentEntryChanged,true);

		this.addFieldEventListener('is_model_list','change',this.isModelListChanged,true);

		this.addFieldEventListener('photo_type','change',this.photoTypeChanged,true);

		this.addFieldEventListener('redirect','keyup',this.updateTargetPage);

		this.addFieldEventListener('redirect_to_menu','keyup',this.updateTargetPage);

		this.addFieldEventListener('folder','input',this.updateTargetPage);

		this.addFieldEventListener('model_class_name','input',this.classNameChanged, true);


	}

	/**
	 * Triggered whenever the content entry type is changed
	 */
	contentEntryChanged() {
		this.hideFieldRows('.content_entry_option_field');

		let content_entry_field_value = this.fieldValue('content_entry');

		if(content_entry_field_value === 'manage')
		{
		}
		else if(content_entry_field_value === 'redirect')
		{
			this.showFieldRows('redirect');
		}
		else if(content_entry_field_value === 'redirect_to_menu')
		{
			this.showFieldRows('redirect_to_menu');
		}
		else if(content_entry_field_value === 'class_method')
		{
			this.showFieldRows([
				'redirect_class_name_heading',
				'redirect_class_name',
				'redirect_class_method',
				'redirect_class_result_page',
				'redirect_class_target'

				]);
		}

		// update the Target Page
		this.updateTargetPage();

	}

	/**
	 * Updates the target page value
	 */
	updateTargetPage() {
		let link_title = '';
		let link_url = '';
		this.folder_error = false;

		let target_page_link = this.element.querySelector('#target_page_link');

		// check for which is activated
		let checked_content_entry = this.fieldValue('content_entry');
		if(checked_content_entry === 'manage' || checked_content_entry === 'manual') // folder based
		{
			let folder_value = this.fieldValue('folder');

			// CLEAN BAD VALUES
			if(folder_value !== '/' && folder_value.match(/[^a-zA-Z0-9_\-]/))
			{
				folder_value = folder_value.replace(/[^a-zA-Z0-9_\-]/g, '');
				this.setFieldValue('folder', folder_value);
			}

			if(folder_value === '/') // trying to set homepage
			{
				link_title = 'Site Homepage';
				link_url = '/'
			}
			else
			{
				if(folder_value === '')
				{
					folder_value = '<em>folder_name</em>';
					this.folder_error = true;
				}
				link_title = this.domain+(this.folder)+folder_value+'/';
				link_url = link_title;

				target_page_link.classList.remove('target_page_error');
			}
		}
		else // redirect
		{
			let redirect_value = this.fieldValue('redirect');
			if(redirect_value.match(/^http:\/\//)) // starts with http://
			{
				link_title = redirect_value;
				link_url = link_title;
			}
			else // Not a new URL so let it through
			{
				link_title = this.domain+redirect_value;
				link_url = link_title;
			}
		}
		// update the link
		target_page_link.innerHTML = link_title;
		target_page_link.setAttribute('href', link_url);

	}



	/**
	 * Triggered when the photo type changed
	 */
	photoTypeChanged() {
		let photo_type_value = this.element.querySelector('#photo_type').value;
		if(photo_type_value === 'photo')
		{
			this.showFieldRows('.photo_setting_field', true);
		}
		else
		{
			this.hideFieldRows('.photo_setting_field', true);
		}
	}

	/**
	 * Triggered when the visibility changes
	 */
	isModelListChanged() {
		this.hideFieldRows('model_list_class_name');

		if(this.element.querySelector('#is_model_list').value  === '1')
		{
			this.showFieldRows('model_list_class_name', true);
		}
	}

	/**
	 * Triggered when the photo type changed
	 */
	classNameChanged() {
		if(this.fieldValue('model_class_name') === '')
		{
			this.hideFieldRows([
				'model_class_name_title_display',
				'model_class_name_primary_view_page',
			], true);
		}
		else
		{
			this.showFieldRows([
				'model_class_name_title_display',
				'model_class_name_primary_view_page',
			], true);
		}
	}
}


