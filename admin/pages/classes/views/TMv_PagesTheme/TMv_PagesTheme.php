<?php

/**
 * Class TMv_PagesTheme
 *
 * This is a view which is built on the main TCv_Website class. This view should be extended for each theme that might
 * be built for a website.
 */
class TMv_PagesTheme extends TCv_Website
{
	/**
	 * @var bool|TMm_PagesMenuItem
	 */
	protected $viewed_menu = false; // [TMm_PagesMenuItem] = The menu item that is being viewed
	protected $active_menu_items = array(); // [array]= The list of active menu items
	protected $homepage_folder_name = '/'; // [string] = The name of the folder assigned to the homepage. Shouldn't be changed, but customization may require it.
	protected $pages_content_view = false;
	protected $page_content_container = false;
	protected $menu_class = 'TMm_PagesMenuItem'; // [string] = The name of the class used for menus
	protected $menu_item_id_prefix = 'menu_item';
	protected $default_margin_percentage = 2;
	
	protected $logged_out_menu_id = null;
	
	protected bool $loading_in_page_builder = false;
	
	/** @var array|null Stores the values in the database for view styles.  */
	protected static ?array $db_view_styles = null;
	protected static ?array $all_view_styles = null;
	
	
	/**
	 * @var ?string $loaded_url The url string that is loaded for this theme rendering
	 */
	protected ?string $loaded_url = null;
	
	
	/** @var TMv_PageContentRendering $page_rendering_view */
	protected $page_rendering_view = false;
	protected $page_rendering = false;


	protected $photo_file = false;

	protected $heading_called = false;
	
	// AUTHENTICATION SETTINGS
	//protected $skip_authentication = false; // [bool] = Indicates if authentication should be skipped
	
	protected $user_model_class = 'TMm_User'; // [string] = The class used for the model
	
	/**
	 * @var bool Boolean flag to indicate if the page builder should always show the page content header. For some
	 * sites this might be preferred for consistency or layout purposes. If you want your theme to do this, set this
	 * value as true in our theme class
	 */
	public static bool $always_show_page_content_header_in_page_builder = false;
	
	// The class name that is used for header content
	// This can be overwritten to customize the theme as necessary
	public static $page_content_header_class_name = 'TMv_PageContentHeader';
	public static $skip_authentication = false;

	/**
	 * TMv_PagesTheme constructor.
	 * @param TMt_PageRenderer|TMm_PagesMenuItem $page_renderer (Default null) An optional page renderer to indicate the specific page
	 * that is being rendered. This will be null for any page rendered on the natural public site, but can be used in
	 * tools that build on top of the theme, or render it manually, such as `TMv_PageBuilder`.
	 */
	public function __construct($page_renderer = null)
	{
		$this->handleNonAdminLockout();
		
		parent::__construct(get_called_class());
		
		// Skip Link
		$this->attachView($this->skipLink(), 0);
		
		$this->processLoadedURL();
		
		if($page_renderer)
		{
			$this->processManualMenuItem($page_renderer);
			
			// Tracks how it loaded, used to avoid instantiating active models on template pages
			$this->setAsLoadingInPageBuilder();
		}
	
		$this->setMenuItemFromURL();
			
		$this->includeMenuPathCSSFiles();
		$this->pages_content_view = new TCv_View('pages_content');
		$this->pages_content_view->setTag('main'); // landmark

		$menus = TMm_PagesMenuItemList::init();

		$this->description = TC_getModuleConfig('pages', 'website_description');
		$this->setTitlePrefix(TC_getModuleConfig('pages', 'website_title'));

		// Handle Authentication
		if(TC_getModuleConfig('login', 'show_authentication'))
		{
			$this->authenticateSession(); // authenticate to ensure they can view this page
		}


		$section_name = @$this->sections[0];
		if ($section_name == '')
		{
			$section_name = 'home';
		}
		$this->addClass('section_' . $section_name);

		// Load the rendering to acquire all the active models


		// Instantiate template setting list
		$list = TMm_PagesTemplateSettingList::init();

		//$this->loadLegacyColumnGap();
		
		// Generate saved component view Styles
		// They get instantiated based on the css_class, so we need them to exist
		foreach(static::componentViewStyles() as $css_class => $view_style)
		{
			$init_values = $view_style;
			$init_values['css_class'] = $css_class;
			$init_values['required_view_classes'] = '';
			if(array_key_exists('required_view_classes', $view_style) && is_array($view_style['required_view_classes']))
			{
				$init_values['required_view_classes'] = implode(' ', $view_style['required_view_classes']);
				
			}
			TMm_PagesViewStyle::init($init_values);
		}
		
		
		
	}
	
	/**
	 * Loads the column gap CSS variable based on the legacy value from the pages module. The new system uses the CSS
	 * variable --layout-column-gap;
	 * @return void
	 */
//	public function loadLegacyColumnGap()
//	{
//		// Try to load a margin percentage as an initial --layout-column-gap, which is likely extended
//		$percentage = TC_getModuleConfig('pages', 'column_margin_percentage');
//		if($percentage >= 0)
//		{
//			$this->addMetaTag('layout-column-cap', "<style>:root { --layout-column-gap: ".$percentage."%;}</style>");
//		}
//	}
//
	
	
	/**
	 * The Screen-reader skip link that takes the reader to the main content of the site
	 * @return TCv_Link
	 */
	public function skipLink()
	{
		$link = new TCv_Link();
		$link->addClass('skip_link');
		$link->addClass('sr-only');
		$link->addText(TC_localize('skip_link','Skip to main content'));
		$link->setURL('#pages_content');
		return $link;
	}
	
	/**
	 * Acquires the URL from the get parameter
	 */
	protected function processLoadedURL() : void
	{
		if(isset($_GET['url']))
		{
			$this->loaded_url = $_GET['url'];
		}
	}
	
	/**
	 * A method that checks for locking out the preview for any non-admins in the system. This is set by the `TC_config`
	 * setting `public_requires_admin`. If set to true, the site will not load unless the person is logged in as an
	 * administrator.
	 */
	protected function handleNonAdminLockout()
	{
		// Only works when in a Tungsten view, ensures Pages don't lock out
		// If the setting is enabled and (we don't have a user OR the user is an admin)
		if(	!TC_isTungstenView() &&
			TC_getConfig('public_requires_admin') &&
			(!TC_currentUser() || !TC_currentUser()->isAdmin()))
		{
			print '<h2>Website Preview Unavailable</h2>';
			$message = TC_getConfig('public_requires_admin_message');
			if($message)
			{
				print '<p>'.$message.'</p>';
				
			}
			
			
			exit();
		}
	}
	
	/**
	 * Generates the page content.
	 */
	protected function generatePageContent()
	{
		$this->page_rendering_view = TMv_PageContentRendering::init($this->viewed_menu);
		$this->page_rendering_view->generateViews();
		
	}
	
	public function renderPageView()
	{
		if ($this->viewed_menu != false)
		{
			// Only renders if the content is visible
			if($this->viewed_menu->contentVisible())
			{
				$this->generatePageContent();
				
				// Bring in the viewed menu description if it's set
				if($this->viewed_menu->description() != '')
				{
					$this->description = $this->viewed_menu->description();
				}
				
				$this->processPageConfigSettings();

				$this->addClass('page_' . $this->viewed_menu->id());

				// Look for page specific CSS
				// These are referenced by menu ID in the theme folder in a subfolder that must be called `css_pages`
				// The file must be called `page_X.css` where X is the ID for that page
				// You can generate that file via SCSS if you prefer, doesn't matter

				$file_path = $this->classFolderFromRoot(get_called_class(), true) . '/css_pages/page_' . $this->viewedMenu()->id() . '.css';
				$full_path = $_SERVER['DOCUMENT_ROOT'] . $file_path;
				if(file_exists($full_path))
				{
					$this->addCSSFile('page_' . $this->viewedMenu()->id(), $file_path.'?v'.TCv_Website::gitVersion());
				}
			}

		}

	}

	/**
	 * A static hook method to process the content view that is being added to this theme.
	 *
	 * This method is called
	 * @param TCv_View $view
	 * @return TCv_View
	 */
	public static function hook_updateContentView($view)
	{
		return $view;
	}

	/**
	 * A  hook method to update the page builder if necessary
	 *
	 * This method is called when rendering the page builder in case elements need to be added to the builder
	 * @param TCv_View $page_builder
	 */
	public function hook_updatePageBuilder($page_builder)
	{

	}
	
	
	/**
	 * Sets a flag to indicate this theme is loading inside of a page builder
	 * @return void
	 */
	public function setAsLoadingInPageBuilder() :void
	{
		$this->loading_in_page_builder = true;
	}
	
	
	/**
	 * Returns the site header which is a view with the <header> tag.
	 * @return TCv_View
	 */
	public function siteHeader()
	{
		$header = new TCv_View('site_header');
		$header->setTag('header');	
		
		return $header;
	}

	/**
	 * Returns the page Content header view for this site
	 * @return TMv_PageContentHeader
	 */
	public function pageContentHeader()
	{
		$page_content_header = (static::$page_content_header_class_name)::init($this->viewed_menu);
		return $page_content_header;
	}

	/**
	 * Returns the footer which is a view with the <footer> tag.
	 * @return TCv_View
	 */
	public function siteFooter()
	{
		$footer = new TCv_View();
		$footer->setTag('footer');
	

		return $footer;
	}

	/**
	 * Returns the title for this page
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}

	/**
	 * Returns the photo file for this page or false if none exists.
	 *
	 * This value is calculated based on the page settings and the content being loaded on the page.
	 * @return false|TCm_File
	 */
	public function photoFile() : false|TCm_File
	{
		return $this->photo_file;
	}

	/**
	 * This method processes the viewed menu and any active models that have been loaded on the site to determine
	 * the title, description and photo for this page. These values are then setup in the website's core settings
	 * to improve Google and social media settings.
	 */
	protected function processPageConfigSettings()
	{
		
		$this->title = $this->viewedMenu()->pageTitle();
		$this->photo_file = $this->viewedMenu()->photoFile();

		$model_class_name = $this->viewedMenu()->modelClassName();

		if($model_class_name != '' && class_exists($model_class_name))
		{

		}
		
		// Find the first active model which is not the PagesTheme and load that title instead
		$active_model_found = false;
		foreach (TC_activeModels() as $model)
		{
			if (
				!$active_model_found
				&& ($model instanceof TCu_Item)
				&& $model->isUsableAsPageTitle()
			)
			{
				$active_model_found = true;
				if(method_exists($model, 'pageDisplayTitle'))
				{
					$this->title = $model->pageDisplayTitle();

					// Updates the title of the menu item
					$this->viewedMenu()->setPageTitle($this->title);

				}
				elseif(method_exists($model, 'title'))
				{
					$this->title = $model->title();

					// Updates the title of the menu item
					$this->viewedMenu()->setPageTitle($this->title);
				}
				else
				{
					$this->addConsoleWarning('Model of type <em>' . get_class($model) . '</em> does not have a method called "<em>title()</em>". Set that value in order for best SEO results');
				}

				if(method_exists($model, 'metaDescription'))
				{
					$text = new TCu_Text($model->metaDescription());
					$this->description  = $text->trimExcess(150);
				}
				else
				{
					$this->addConsoleWarning('Model of type <em>'.get_class($model).'</em> does not have a method called "<em>description()</em>". Set that value in order for best SEO results');
				}
			}
		}

		// TWITTER SETTINGS
		$this->addMetaTag('twitter:card', array('content' => 'summary_large_image'));
		$this->addMetaTag('twitter:title', array('content' => strip_tags($this->title)));
		$this->addMetaTag('twitter:description', array('content' => $this->description));


		// FACEBOOK SETTINGS
		$this->setOpenGraphType('article');
		$this->setOpenGraphURL($this->fullDomainName().$_SERVER['REQUEST_URI']);
		$this->setOpenGraphTitle(strip_tags($this->title));
		$this->setOpenGraphSiteName(TC_getModuleConfig('pages', 'website_title'));
		$this->setOpenGraphDescription($this->description);

		// Deal with PHOTO
		if($this->photo_file && $this->photo_file->isImage())
		{
			$image_view = new TCv_Image(false, $this->photo_file);
			$image_path = $this->fullDomainName().$image_view->cacheFile()->filenameFromSiteRoot();
			$this->addMetaTag('twitter:image', array('content' => $image_path));

			$this->setOpenGraphImage($image_path);
			$this->addMetaTag('og:image:width', array('property' => 'og:image:width', 'content' => $image_view->viewWidth()));
			$this->addMetaTag('og:image:height', array('property' => 'og:image:height', 'content' => $image_view->viewHeight()));
		}

		/**
		 * DEVELOPER NOTES
		 *
		 * If images aren't appearing, it's possible that the URLs are pointing to a different domain, especially if
		 * you're using a tunnelling tool such as ngrok. In those cases, you can override the value for $_SERVER['HTTP_HOST']
		 * in the TC_config_env.php file and have it indicate the URL that you want.
		 *
		 * Facebook Debugger : https://developers.facebook.com/tools/debug/og/object/
		 * Twitter Debugger :  https://cards-dev.twitter.com/validator
		 */
	}

	/**
	 * Returns the menu item for a given level.
	 *
	 * Level 1 is the top level, level 2 would be the next level down and so forth.
	 *
	 *
	 * @param int $level The level for which the menu item is wanted.
	 * @return TMm_PagesMenuItem|bool
	 */
	public function menuItemForLevel($level)
	{
		if(isset($this->active_menu_items[$level]))
		{
			return $this->active_menu_items[$level];
		}

		return false;

	}

	/**
	 * Returns if the viewed menu has submenus for the level provided.
	 *
	 * This method will return true if there are visible menus for the level provided.
	 *
	 *
	 * @param int $level The level for which the menu item is wanted. A value <= 1 will always return false. A viewed menu
	 * can't have a submenu for level 1 there is no parent to level 1.
	 * @return bool
	 */
	public function viewedMenuHasSubmenusForLevel($level)
	{
		// Level on
		if($level == 1)
		{
			return false;
		}

		if($parent_menu = $this->menuItemForLevel($level - 1))
		{
			return $parent_menu->numChildrenVisible() > 0;
		}

		return false;
	}


	/**
	 * Processes the parameter passed via the URL variable into the sections value
	 */
	public function saveURLParamsToSections() : void
	{
		$this->sections = explode("/",trim($this->loaded_url,'/'));
	}
		
	
	/**
	 * Uses the URL of the page being viewed to determine the menu item that represents it.
	 */
	public function setMenuItemFromURL()
	{
		// New method passes the values through a URL path which is processed
		if(!is_null($this->loaded_url))
		{
			$this->saveURLParamsToSections();
			
			if(count($this->sections) == 0)
			{
				// Force the blank one in there to ensure it finds a menu item
				$this->sections[0] = '';
				$this->section_name = '/'; // homepage
			}
			else
			{
				$this->section_name = $this->sections[0];
			}
			
			
		}
		
		$this->saveActiveMenuItemsFromSections();

		$this->saveActiveModelsForViewedMenuItem();
		
				
	}
	
	/**
	 * Generates the menu item with a provided menu item, which handles some internal items that ensure it operates
	 * correctly in a non-standard mode
	 * @param TMm_PagesMenuItem $menu_item
	 */
	public function processManualMenuItem($menu_item)
	{
		// Set the loaded URL to match the menu item
		if($menu_item instanceof TMm_PagesMenuItem)
		{
			$this->loaded_url = $menu_item->pathToFolder();
		}
		
		
	}
	
	/**
	 * This is an internal method that processes the section names and saves the active menus use by this theme. This
	 * method looks at all the URLs and tries to find the page that makes sense for the URL provided
	 */
	protected function saveActiveMenuItemsFromSections()
	{
		$class_name = $this->menu_class;
		
		// save level zero as a blank menu item
		$this->active_menu_items[0] = $class_name::init(0);
		
		
		// Loop through the sections to find the active menu items at each level
		$level = 0;
		$id_num = 1;
		foreach($this->sections as $section_name)
		{
			// Catch the homepage
			if($section_name == '')
			{
				$section_name = '/';
			}
		
			
			$menu = $this->activeMenuWithParentAndFolder($this->active_menu_items[$level], $section_name);
			$level++;
			$this->active_menu_items[$level] = $menu;
			if($menu)
			{
				$this->viewed_menu = $menu;
			}
			else // Menu not found, save these as IDs
			{
				if($id_num == 1)
				{
					$_GET['id'] = $section_name;
				}
				else
				{
					$_GET['id_'.$id_num] = $section_name;
				}
				$id_num++;
			}
		}
	}
	
	/**
	 * Saves the "active models" for the viewed menu
	 * @see TC_saveActiveModel()
	 */
	protected function saveActiveModelsForViewedMenuItem()
	{
		// Do not load active models in the page builder. They are template pages and shouldn't even try
		if($this->loading_in_page_builder)
		{
			return;
		}
		
		if($this->viewed_menu instanceof TMm_PagesMenuItem)
		{
			TC_saveActiveModel($this->viewed_menu);

			// Deal with model for the viewed menu
			$model_class_name = $this->viewed_menu->modelClassName();

			if($model_class_name != '' && class_exists($model_class_name))
			{
				// Check if we already have an active model, for whatever reason
				// Sometimes it's preloaded, sometimes it's set to load silently without IDs in the URL
				if($active_model = TC_activeModelWithClassName($model_class_name))
				{
					TC_setPrimaryModel($active_model);
				}
				else // Try and instantiate it
				{
					$model = ($model_class_name)::init($_GET['id']);
					TC_saveActiveModel($model);
					TC_setPrimaryModel($model);
				}
			}

		}
		
				
	}

	/**
	 * Finds teh menu item with the provided ID and returns a button pointing to that menu. Returns false if it can't
	 * find it.
	 *
	 * @param int $menu_id The ID of the menu
	 * @return TCv_Link|bool
	 */
	public function linkForMenuID($menu_id)
	{
		/** @var TMm_PagesMenuItem $menu_item */
		$menu_item = TMm_PagesMenuItem::init( $menu_id);
		if($menu_item)
		{
			$button = new TCv_Link();
			$button->setURL($menu_item->pathToFolder());
			$button->addText($menu_item->title());

			return $button;
		}

		return false;

	}


	/**
	 * Performs any actions related to the menu item that is being viewed. This should always happen after the website
	 * model is initialized to ensure all the authentication and verifications are complete.
	 */
	public function performMenuItemActions()
	{
		// Handle Redirect
		if($this->viewed_menu)
		{
			$redirect = $this->viewed_menu->redirect();
			if($redirect && $redirect != '')
			{
				header("Location: ".$redirect, true, 301);
				exit();	
			}	
			elseif($this->viewed_menu->isClassMethod())
			{
		
				$class_name = $this->viewed_menu->redirectClassName();
				$method_name = $this->viewed_menu->redirectClassMethod();
				$target_page = $this->viewed_menu->redirectClassTargetPage();
				
				$class = $class_name::init($_GET['id']);
				
				if(isset($_GET['id_2']))
				{
					if(isset($_GET['id_3']))
					{
						$class->$method_name($_GET['id_2'], $_GET['id_3']);	
					}
					else
					{
						$class->$method_name($_GET['id_2']);
					}
				}
				else
				{
					$class->$method_name();	
				}

				header("Location: ".$target_page, true, 301);
				exit();
				
			}
		}
		else // NO MENU FOUND
		{
			if(TC_getModuleConfig('pages','use_301_redirects'))
			{
				// CHECK FOR 301 Redirects
				$redirect_list = TMm_Pages301RedirectList::init();
				if($redirect_item = $redirect_list->findRedirectForCurrentURL())
				{
					header("Location: ".$redirect_item->fullTargetUrl(), true, 301);
					exit();
				}
			}
			
		
			
		}
		
		
		// Handle 404. This could happen for any number of reasons and it only performs an action if it triggers one
		// of those items.
		$this->process404();
		
	}
	
	/**
	 * Determine the active menu for a given parent that has a particular folder.
	 *
	 * @param TMm_PagesMenuItem $parent The parent menu item
	 * @param string $folder The folder that is being viewed
	 * @return bool|TMm_PagesMenuItem
	 */
	protected function activeMenuWithParentAndFolder($parent, $folder)
	{
		if(!($parent instanceof TMm_PagesMenuItem))
		{
			return false;
		}
			
		foreach($parent->children() as $child)
		{
			if($child->folder() == $folder)
			{
				$child->setIsCurrent();
				return $child;
			}
		}
		return false;
		
	}
	

	
	/**
	 * Sets the view that the pages content will be added to. This overrides the default view which is setup upon instantiation of the class.
	 * @param TCv_View $view
	 */
	public function setPagesContentContainer($view)
	{
		$this->page_content_container = $view;
	}
	

	/**
	 * Returns the page content as a single view with all the page content attached to that view.
	 * @return bool|TCv_View
	 */
	public function pagesContentView()
	{
		// Only bother if the messenger hasn't already been attached
		if(!$this->messenger_attached)
		{
			$container = new TCv_View('messenger_container');
			$container->addClass('content_width_container');
			$container->attachView($this->messenger());
			
			$this->attachViewToPagesContent($container);
		}
		if($this->viewed_menu != false)
		{
			if($this->viewed_menu->contentVisible())
			{
				$this->attachViewToPagesContent($this->page_rendering_view);
			}

		}

		$dialog = new TCv_Dialog();
		$this->pages_content_view->attachView($dialog);

		return $this->pages_content_view;
	}
	
	/**
	 * Attaches the view provided to the pages content view.
	 * @param TCv_View $view
	 */
	public function attachViewToPagesContent($view)
	{
		$this->pages_content_view->attachView($view);
	}
	
	/**
	 * The menu that is currently being viewed.
	 * @return bool|TMm_PagesMenuItem
	 */
	public function viewedMenu()
	{
		return $this->viewed_menu;
	}

	/**
	 * Sets the viewed menu item for the page
	 * @param TMm_PagesMenuItem|TMt_PageRenderer $menu_item
	 */
	public function setViewedMenu($menu_item)
	{
		$this->viewed_menu = $menu_item;
	}
		
	/**
	 * Calculates the default page title based on the menu item being viewed.
	 * @param bool|string  $site_title
	 * @deprecated Title is calculated as part of the rendering of the page and it's content separate from this method.
	 */
	public function calculateDefaultPageTitle($site_title = false)
	{
		// Do nothing

	}



	/**
	 * Includes any css files that match the path for the menu item in the root/css/ folder. So if a menu item has the
	 * path /about/services/ then a file in css/about/services.css will be loaded.
	 */
	public function includeMenuPathCSSFiles()
	{
		if($menu = $this->viewedMenu())
		{
			$css_file = '/css'.$menu->pathToFolder().'.css';
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$css_file))
			{
				$this->addCSSFile($css_file,$css_file);	
			}
		}
	}
	
	/**
	 * A heading 1 tag that containers the page title for this particular page. This function can only be called once to
	 * enforce a single <h1> on the website. Any other call will return false. Any call to this function in page view
	 * will override the main website's function to show the title
	 * @return bool|TCv_View
	 */
	public function pageTitleHeading()
	{
		if(!$this->heading_called)
		{
			$this->heading_called = true;

			$page_title = new TCv_View();
			$page_title->setTag('h1');
			$page_title->addText($this->title);
			return $page_title;
		}
		
		return false;
	}

	/**
	 * Sets the default column margin percentage.
	 *
	 * @param int $percentage The percentage of the margins for columns on the website
	 * @deprecated Margin percentage no longer used
	 */
	public function setDefaultColumnMarginPercentage($percentage)
	{
	}
	
	/**
	 * The title used in meta tags
	 * @return string
	 */
	public function metaTagTitle()
	{
		if(TC_getConfig('use_seo_advanced') && $this->viewed_menu && $this->viewed_menu->metaTitle())
		{
			return $this->viewed_menu->metaTitle();
		}
		
		return parent::metaTagTitle();
	}
	
	/**
	 * The standard page rendering which is often overridden
	 */
	public function render()
	{
		// Load the site header
		$this->attachView($this->siteHeader());
		
		// Attach the page content header
		$this->attachView($this->pageContentHeader());
		
		// Add page content
		$this->attachView($this->pagesContentView());
		
		// Load the site footer
		$this->attachView($this->siteFooter());
		
	}
	
	/**
	 * @return string
	 */
	public function html()
	{
		// Attach the Privacy notification if necessary
		if(TC_getModuleConfig('pages','use_GDPR'))
		{
			$this->attachView(TMv_PrivacyNotification::init());
		}
		
		// Parent handles the render call
		return parent::html();
	}

	//////////////////////////////////////////////////////
	//
	// TEMPLATE PAGE HEADER
	//
	// Methods related to the page-specific headers
	//
	//////////////////////////////////////////////////////

	/**
	 * The view for the header for this page. This view ties into designed to contain the header related content that
	 * is specific to this page such as the header photo, title and possibly a description. The settings for what
	 * appears here are tied into the Page Settings in the pages module Appearance.
	 */
	public function templatePageHeader()
	{
		$view = new TCv_View('page_header');
		return $view;
	}
	
	
	
	/**
	 * Returns the view used for the background image. This returns a separate view which can be styled and attached
	 * accordingly
	 *
	 * Note: this method is VERY similar to one inside of TMv_PageContentHeader. The code was added here since it
	 * does not always require being attached to the page content header
	 * @return ?TCv_View
	 */
	public function backgroundImageView() : ?TCv_View
	{
		// NO viewed menu set, just get out
		if(!$this->viewed_menu)
		{
			return null;
		}
		
		// Get the traits to confirm it uses the correct one
		// We don't run this if it's not a page renderer
		
		// Check if it's a viewed menu or it's a page renderer
		if(!($this->viewed_menu instanceof TMm_PagesMenuItem)
			&& !TC_classUsesTrait($this->viewed_menu, 'TMt_PageRenderer'))
		{
			return null;
		}
		if(TC_getConfig('use_pages_hero_boxes')
			&&  method_exists($this->viewed_menu, 'usesHeroBoxes')
			&& $this->viewed_menu->usesHeroBoxes())
		{
			/** @var TMv_PageHeroBoxes $hero_box */
			$hero_box = TMv_PageHeroBoxes::init($this->viewed_menu);
			$hero_box->setTimerMilliseconds(6000);
			
			return $hero_box;
		}
		
		$photo_file = false;
		// Get the photo file for the viewed menu if it's a page menu
		if($this->viewed_menu instanceof TMm_PagesMenuItem)
		{
			$photo_file = $this->viewed_menu->backgroundPhotoFile();
		}
		// Try and get the background for model viewable pages
		elseif(TC_classUsesTrait($this->viewed_menu, 'TMt_PageModelViewable'))
		{
			/** @var TMt_PageModelViewable $menu_model */
			$menu_model = $this->viewed_menu;
			$photo_file = $menu_model->pageDisplayPhoto();
		}
		
		
		if(!$photo_file)
		{
			$photo_file = $this->viewed_menu->photoFile();
		}
		
		if($photo_file)
		{
			if($photo_file->isImage())
			{
				return $this->headerBackgroundImageForPhotoFile($photo_file);
			}
			elseif($photo_file->extension() == 'mp4')
			{
				return $this->videoViewForPhotoFile($photo_file);
			}
		}
		
		
		return null;
	}
	
	/**
	 * Generates the video view for a photo file
	 * @param TCm_File $photo_file
	 * @return TCv_View
	 */
	public function videoViewForPhotoFile($photo_file)
	{
		$video_view = new TCv_View('background_image');
		$video_view->setTag('video');
		$video_view->setAttribute('autoplay', 'autoplay');
		$video_view->setAttribute('loop', 'loop');
		$video_view->setAttribute('muted', 'muted');
		
		$source = new TCv_View();
		$source->setTag('source');
		$source->setAttribute('src', $photo_file->filenameFromSiteRoot());
		$source->setAttribute('type', 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
		$video_view->attachView($source);
		
		return $video_view;
	}
	
	/**
	 * Returns the header background image for a photo file
	 * @param TCm_File $photo_file
	 * @return TCv_View
	 */
	public function headerBackgroundImageForPhotoFile($photo_file)
	{
		$background_container = new TCv_View('header_background_image');
		
		$photo_view = new TCv_Image(false, $photo_file);
		
		$background_url =' url("' . $photo_view->file()->filenameFromSiteRoot() . '")';
		
		$style = 'background-image: '.$background_url.';';
		
		if(method_exists($this->viewed_menu, 'photoPosition'))
		{
			$position = $this->viewed_menu->photoPosition();
			if($position != '')
			{
				$style .= ' background-position: '.$this->viewed_menu->photoPosition().';';
			}
			
		}
		
		//	$view->addClass('has_photo');
		//	$view->addClass('has position');
		$background_container->setAttribute('style', $style);
		return $background_container;
	}
	
	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION - CONFIG
	//
	// Methods related to authentication
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets the logged out menu ID
	 * @param int $menu_id
	 * @return void
	 */
	protected function setLoggedOutMenuID(int $menu_id)
	{
		$this->logged_out_menu_id = $menu_id;
	}
	
	/**
	 * Check that the current session is valid. It also will check that the user has permission for a given group. This
	 * function will obey the requires_authentication value for the TMm_PagesMenuItem value set in Tungsten. If a menu
	 * item is not found, then it will authenticate unless the $skip_authentication paramater is set to true. This
	 * defaults to authenticating unless you turn it off. It's safer and matches the Tungsten paradigm for consistency.
	 */
	public function authenticateSession()
	{
		// Invalid Session
		if(!$this->sessionIsValid())
		{
			$this->logout(false); // pass in false to do silent logout
			
			// Try and find the logged out menu id
			$menu_item = TMm_PagesMenuItem::init($this->logged_out_menu_id);
			if(($menu_item instanceof TMm_PagesMenuItem) && $menu_item->id() > 0)
			{
				header("Location: ".$menu_item->pathToFolder());
				exit();
			}
			
			// Never found one, go to homepage
			header("Location: /");
			exit;
		}
		
		// Made it here, means valid user logged in
		$this->setUserID(@$_SESSION["tungsten_user_id"]);
		
		if($this->userID())
		{
			$user = $this->user();
			$values = ['user_id' => $user->id()];
			TSv_GoogleAnalytics::addSetGTag( $values);
			
			if(method_exists($user,'googleAnalyticsUserProperties'))
			{
				$properties = $user->googleAnalyticsUserProperties();
				TSv_GoogleAnalytics::addSetGTag( ['user_properties' => $properties]);
				
			}
			
			
		}
	}

	/**
	 * Check that the current session is valid. For pages themes, this is entirely based on if the given viewed menu
	 * requires authentication. Otherwise we're loading or doing something else and that should just keep trying to
	 * load the page or whatever is loading.
	 * @return bool
	 */
	public function sessionIsValid()
	{
		// Checks for the static value
		if(static::$skip_authentication)
		{
			return true;
		}

		// Page Menu exists, we use the value for if it requires authentication.
		if($this->viewedMenu() && $this->viewedMenu()->requiresAuthentication())
		{
			// Validity based on if the user ID is set
			return isset($_SESSION["tungsten_user_id"]);
		}
		
		// If the menu item doesn't require auth OR
		// We can't find a menu item, then we assume it's valid because we're on the public side of things
		return true;
	}

	/**
	 * Handles the login attempt from the user. If successful, all the necessary values will be saved and the session variables will be appropriately set.
	 * @param string $email
	 * @param string $password
	 * @return bool
	 */
	public function processLogin($email, $password)
	{
		// delete old setting first
		unset($_SESSION['tungsten_user_id']);
		
		if($user_id = $this->authenticateUser($email, $password))
		{
			$this->user = TMm_User::init( $user_id);
			$this->user->setAsLoggedIn();
			TSv_GoogleAnalytics::trackEvent('login');
			if(class_exists('TSv_FacebookPixel'))
			{
				TSv_FacebookPixel::trackEvent('login');
			}
			
			return true;
		}
		else
		{
			return false;
		}
		

	}
	
	/**
	 * Indicates if Tungsten should skip authentication. This must be done directly after initialization before any authentication calls are made.
	 * @param bool $skip Indicate if authentication should be skipped.
	 * @deprecated Just set the static variable directly
	 */
	public function setSkipAuthentication($skip = false)
	{
		static::$skip_authentication = $skip;
	}

	//////////////////////////////////////////////////////
	//
	// PAGE TEMPLATE VALUES
	//
	// Methods related to page template values
	//
	//////////////////////////////////////////////////////

	/**
	 * Loads a template value with the given code
	 * @param string $code The code for the template value
	 * @return bool|string
	 */
	public function templateValueWithCode($code)
	{
		$template_value = TMm_PagesTemplateSetting::init( $code);
		if($template_value instanceof TMm_PagesTemplateSetting)
		{
			return $template_value->value();
		}
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// 404 Pages
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Determines if the page is a 404. This can be extended to do custom solutions on websites.
	 * @return bool
	 */
	protected function is404()
	{
		// Menu found
		if($viewed_menu = $this->viewedMenu())
		{
			// We're loading a menu item, bu it's missing the active model
			if($viewed_menu->isMissingActiveModel())
			{
				$this->addConsoleWarning('404 : Missing active model');
				return true;
			}
			
			// We are loading a menu item with an active model, however we aren't allowed to see it
			if($viewed_menu->requiresActiveModel() && !$viewed_menu->activeModelLoadedForPage()->userCanView())
			{
				$this->addConsoleWarning('404 : Required model <em>"'.$viewed_menu->modelClassName().'"</em> fails userCanView');
				return true;
			}
			
			// Deactivated pages return 404
			if(!$viewed_menu->isActive())
			{
				$this->addConsoleWarning('404 : Page is not active');
				return true;
			}
		
		}
		
		// We're rendering a page and can't find a menu
		// This avoids accidentally triggering on any other php script
		elseif(strpos($_SERVER['SCRIPT_NAME'],'pages_render.php') > 0) // menu NOT found, 404
		{
			$this->addConsoleWarning('404 : Page not found in navigation');
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * A function to handle and process 404 errors for missing pages. This function operates by calling the is404()
	 * method to determine if there's a problem, then if so it replaces the page content with that from page in the
	 * system that has a folder called 404.
	 *
	 * This functionality requires a menu item with a folder that has the value 404, otherwise it won't work.
	 */
	protected function process404()
	{
		if($this->is404())
		{
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
			
			// Find the 404 page
			$page_list = TMm_PagesMenuItemList::init();
			$page = $page_list->pageFor404();
			if($page)
			{
				// Change the viewed menu to the 404 page
				$this->viewed_menu = $page;
				$this->renderPageView(); // Must re-run the renderer
				
			}
			else // No page found
			{
				exit();
			}
			
		}
	}
	

	//////////////////////////////////////////////////////
	//
	// AUTHENTICATION - USER
	//
	// User methods for authentication
	//
	//////////////////////////////////////////////////////



	/**
	 * Manually unsets the logged in user ID. This is useful an authentication script is a multi-part system where the ID is set in the first authentication step.
	 */
	public function unsetLoggedInUserID()
	{
		unset($_SESSION['tungsten_user_id']);
	}
	

	/**
	 * Check that the username and password provided have valid access to Tungsten. It will check that the credentials
	 * provided match a user in the system.  It will also check that the user is a member of one of the groups that are
	 * permitted to vie at least one module in Tungsten.
	 * @param string $email
	 * @param string $password
	 * @return bool|int|string
	 */
	public function authenticateUser($email, $password)
	{
		// Test the username, password, and group parameters
		if (!isset($email) || !isset($password))
	    {			
			return false;
		}
		
		// NO user with email found
		$user = TMm_User::userWithEmail($email);
		if(!$user)
		{
			return false;
		}

		// Disabled Accounts can't login
		if(!$user->isActive())
		{
			TC_message(TC_localize('login_inactive','Your account is not activated. Please check your email for an activation link.'));
			return false;
		}


		$is_valid = $user->authenticatePassword($password);
		
	  	return $is_valid ? $user->id() : false;
	}


	
	/**
	 * Consistency method which returns false for complex calls for the current module
	 * @return bool
	 */
	public function currentModule()
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// COMPONENT STYLES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The array of component styles that can be applied to a component. This functionality is extended from an
	 * earlier implementation where the website might have had them stored in a database for functionality that the
	 * user could edit.
	 *
	 * The functionality is now tied to the theme, however the main function in the theme class does go find those
	 * previous values sitting in the database for backwards compatibility.
	 *
	 * To extend this functionality, the theme must implement the abstract `componentViewStyles()` method, which
	 * returns values for that specific theme.
	 * @return TMm_PagesViewStyle[]
	 */
	public static function allComponentCSSViewStyles() : array
	{
		// If we have it, skip
		if(is_array(static::$all_view_styles))
		{
			return static::$all_view_styles;
		}
		
		static::$all_view_styles = [];
		
		// Get the legacy values since they need to be part of our system as well.
		if(is_null(static::$db_view_styles))
		{
			static::$db_view_styles = [];
			foreach(TMm_PagesViewStyle::allModels() as $db_style_model)
			{
				static::$db_view_styles[$db_style_model->className()] = $db_style_model;
			}
			
		}
		$all_styles = static::$db_view_styles;
		
		// Loop through the theme styles and create view styles based on those values
		foreach(static::componentViewStyles() as $class_name => $style_values)
		{
			$model_values = [
				'css_class' => $class_name,
				'title' => $style_values['title'],
				'description' => '',
				'apply_to_container' => 0,
				'required_view_classes' => 'TMt_PagesContentView', // default string value, which is what gets passed
			];
			
			// Check for descriptions and apply to container values that need to exist
			if(isset($style_values['description']))
			{
				$model_values['description'] = $style_values['description'];
			}
			
			if(isset($style_values['apply_to_container']))
			{
				$model_values['apply_to_container'] = $style_values['apply_to_container'];
			}
			
			// We only do anything with required classes if they are an array of values
			if(isset($style_values['required_view_classes'])
				&& is_array($style_values['required_view_classes'])
				&& count($style_values['required_view_classes']) > 0
			)
			{
				$model_values['required_view_classes'] = implode(' ', $style_values['required_view_classes']);
			}
			
			
			$all_styles[$class_name] = TMm_PagesViewStyle::init($model_values);
		}
		
		static::$all_view_styles = $all_styles;
		
		return static::$all_view_styles;
	}
	
	/**
	 * This method returns all the styles for this theme. If you don't need to pull the values from the database for
	 * newer themes, then set the value of db_view_styles = [] in your constructor.
	 * The format for the return values is a nested array of values. Each entry should have the index be the CSS
	 * class name, which ensure uniqueness.
	 *
	 * The array of values are the additional properties, and the only required one is `title`.
	 * `title` => The human readable title for this view style
	 * `description` => the description shown for this view style
	 * `required_view_classes` => an array of class names. An empty array or any other value means it applies to all
	 * `apply_to_container` => A boolean that indicates if the style should be applied to the container element
	 *
	 * @return array
	 */
	public static function componentViewStyles() : array
	{
		return [
//			'another' => [
//				'title' => 'Another one',
//				'description' => 'Here is a good one that we use for different stuff',
//				'required_view_classes' => null,
//
//			],
//			'class_b' => [
//				'title' => 'Class B',
//				'required_view_classes' => ['TMv_HTMLEditor','TMv_QuickButton'],
//				'apply_to_container' => true
//
//			],
//			'simple' => [
//				'title' => 'Simple',
//			],
		];
	}

	//////////////////////////////////////////////////////
	//
	// FRONT-END ROUTING
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the menu item used for loading front-end routing. This function generates a menu item that does not
	 * actually exist, but with values that allow it to load.
	 * @param array $override_init_values An array of other values to initialize the menu item with
	 * @return TMm_PagesMenuItem
	 */
	public static function frontEndRoutingMenuItem(array $override_init_values = []) : TMm_PagesMenuItem
	{
		$current_target = TC_currentURLTarget();
		$page_title = $current_target->title();
		if(!$current_target->allowsFrontEndRouting())
		{
			$page_title = 'Error';
		}
		
		$init_values = array_merge([
			                           'menu_id'               => 0,
			                           'is_visible'            => 1,
			                           'content_visible'       => 1,
			                           'is_visible_conditional'=> true,
			                           'photo_type'            => 'photo',
			                           'title'                 => $page_title,
		                           ],
		                           $override_init_values);
		
		return new TMm_PagesMenuItem($init_values);
	}
	
	//////////////////////////////////////////////////////
	//
	// PRE-DEFINED VIEWS
	//
	// These are views that are commonly used on most websites
	// and are included here for consistency and to reduce
	// the workload necessary for building a theme
	// The can be extend or replaced entirely when needed
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the main menus which are required for most sites. This can be added to additional views as needed,
	 * specific to your website's needs. The main menus are also what are used for mobile navigation on the site.
	 * @param int $num_levels (Default 2) The number of levels that should be shown
	 * @param bool $use_hover_navigation Indicates if the navigation should use hovers to show the subnav. This might
	 * not always be the case. Note that the main menus are what's used for the larger, full navigation for the
	 * website.
	 * @return TMv_PageMenus
	 */
	protected function mainMenus($num_levels = 2, $use_hover_navigation = true)
	{
		$menus = TMv_PageMenus::init('main_menus');
		$menus->setStartingLevel(1);
		$menus->setNumLevelsToShow($num_levels);
		$menus->addNavigationType(TMm_PagesNavigationType::init(1));
		
		if($use_hover_navigation)
		{
			$menus->addClass('use_hover_nav');
		}
		return $menus;
	}
	
	/**
	 * Returns the mobile toggle button which is what is used to show the full navigation on mobile.
	 *
	 * NOTE: This requires the inclusion of the `_mobile_navigation.scss` partial in your SCSS styles in your theme.
	 * @return TCv_ToggleTargetLink
	 */
	protected function mobileMenuToggleButton($icon = 'fa-bars')
	{
		$menu_toggle_link = TCv_ToggleTargetLink::init('mobile_menu_toggle');
		$menu_toggle_link->setIconClassName($icon);
		$menu_toggle_link->addClassToggle('body', 'mobile_navigation_showing'); // toggles the class on the body tag
		$menu_toggle_link->setUseStandardToggle();
		$menu_toggle_link->addHiddenTitle(TC_localize('view_menus','View menus'));
		return $menu_toggle_link;
	}
	
	
}
?>