<?php

/**
 * A view that shows all the linkable page items available on the site.
 */
class TMv_FormItem_LinkablePageSelect extends TCv_FormItem_Select
{
	/**
	 * TMv_FormItem_LinkablePageSelect constructor.
	 * @param string $id
	 * @param string $title
	 */
	public function __construct($id, $title)
	{
		parent::__construct($id, $title);
	
		$this->useFiltering();
		
	}
	
	/**
	 * @return TCv_View
	 * Extend the field view to trigger the list configuration
	 */
	public function fieldView()
	{
		$this->configureList();
		
		return parent::fieldView();
	}
	
	/**
	 * Method to configure the actual interface to show the items.
	 * @return void
	 */
	public function configureList() : void
	{
		$page_menu_list = TMm_PagesMenuItemList::init();
		foreach($page_menu_list->linkablePagesAndItems(true) as $linkable_item)
		{
			// headings
			if(is_string($linkable_item))
			{
				if(method_exists($linkable_item, 'modelTitlePlural'))
				{
					$this->startOptionGroup($linkable_item, $linkable_item::modelTitlePlural());
				}
			}
			else // Actual linkable thing
			{
				if($linkable_item instanceof TMm_PagesMenuItem)
				{
					$title = $linkable_item->titleWithFullAncestors();
				}
				else // A TMt_PageModelViewable item, other than a menu item
				{
					$title = $linkable_item->title();
				}
				
				$this->addOption($linkable_item->pageViewCode(), $title);
			}
		}
	}
}