<?php
/**
 * Class TMv_PageMenuItem_PhotoForm
 *
 * The form to edit a menu item's photo.
 */
class TMv_PageMenuItem_PhotoForm extends TCv_FormWithModel
{
	protected int $crop_width = 0;
	protected int $crop_height = 0;
	
	/**
	 * TMv_PageMenuItem_PhotoForm constructor.
	 * @param bool|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->crop_width = TC_getModuleConfig('pages', 'image_crop_width');
		$this->crop_height = TC_getModuleConfig('pages', 'image_crop_height');
		
		$this->setButtonText('Update photo values');
		
	}
	
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
		
		if($this->crop_width >= 0 && $this->crop_height >= 0)
		{
			$field = new TCv_FormItem_Select('photo_type', 'Photo type');
			$field->setHelpText("Indicate the type of photo setup for this menu. ");
			$field->addOption('photo', 'Single Photo – One photo that can be assigned to this page.');
			$field->addOption('none', 'None – No photo option for this page.');
			if(TC_getConfig('use_pages_hero_boxes') && TC_getModuleConfig('pages', 'hero_box_image_crop_height') > 0)
			{
				$field->addOption('hero_box', 'Hero Box – Multiple Photos with titles and links.');
			}
			$this->attachView($field);
			
			
			$file = new TCv_FormItem_FileDrop('photo_filename', 'Photo');
			$file->addClass('photo_setting_field');
			$file->setHelpText('A photo to be associated with this page. The use of this photo depends on the site design.');
			
			if($this->crop_width > 0 && $this->crop_height > 0)
			{
				$file->setUseCropper(array($this->crop_width, $this->crop_height));
			}
			
			
			$this->attachView($file);
			
			$field = new TCv_FormItem_TextField('photo_alt_text', 'Alt text');
			$field->addClass('photo_setting_field');
			$field->setHelpText('Accessible text to describe this photo');
			$this->attachView($field);
			
			
			$position = new TCv_FormItem_Select('photo_position', 'Photo position');
			$position->addHelpText('Identify where the header image is "pinned" ');
			$position->addOption('', 'Select an override position');
			$position->addOption('left top', 'Top left');
			$position->addOption('center top', 'Top center');
			$position->addOption('right top', 'Top right');
			$position->addOption('left center', 'Middle left');
			$position->addOption('center center', 'Centered');
			$position->addOption('right center', 'Middle right');
			$position->addOption('left bottom', 'Bottom left');
			$position->addOption('center bottom', 'Bottom center');
			$position->addOption('right bottom', 'Bottom right');
			$this->attachView($position);
			
			
		}
		
	}
	
	
}