<?php

/**
 * Class TMv_PageHeroBoxList
 * The list of hero boxes associated with a menu item.
 */
class TMv_PageHeroBoxList extends TCv_RearrangableModelList
{
	/** @var TMm_PagesMenuItem $menu_item  */
	protected $menu_item;

	/**
	 * TMv_PageHeroBoxList constructor.
	 * @param TMm_PagesMenuItem $menu_item The menu item that we're creating a list for
	 */
	public function __construct($menu_item)
	{	
		parent::__construct();


		$this->setModelClass('TMm_PageHeroBox');
		$this->defineColumns();
		//$this->addClassCSSFile();

		if($menu_item)
		{
			$this->addModels($menu_item->heroBoxes());

		}



		//$this->addClassCSSFile('TMv_PageHeroBoxList');


	}

	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);

		$title = new TCv_ListColumn('visibility');
		$title->setWidthAsPixels(100);
		$title->setTitle('Visible');
		$title->setAlignment('center');
		$title->setContentUsingListMethod('visibleColumn');
		$this->addTCListColumn($title);


		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);

		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);

	}

	/**
	 * The column for the title
	 * @param TMm_PageHeroBox $model The item being provided
	 * @return TCv_View|string
	 */
	public function titleColumn ($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'edit-page-hero-box');
		$link->addText($model->title());
		return $link;
	}



	/**
	 * The column for the title
	 * @param TMm_PageHeroBox $model The item being provided
	 * @return TCv_View|string
	 */
	public function visibleColumn($model)
	{
		if($model->isVisible())
		{
			$check = new TCv_View();
			$check->setTag('i');
			$check->addClass('fa-check');
			$check->addClass('list_control_button');
			return $check;
		}
		else
		{
			return '';
		}
	}


	/**
	 * @param TMm_PageHeroBox $model The program
	 * @return TCv_Link|TCv_View|string
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'edit-page-hero-box', 'fa-pencil');

	}

	/**
	 * @param TMm_Torchlight $model The program
	 * @return TCv_Link|TCv_View|string
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'delete-page-hero-box', 'fa-trash');

	}

}
?>