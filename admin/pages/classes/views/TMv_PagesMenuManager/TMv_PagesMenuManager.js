class TMv_PagesMenuManager {
	element;
	first_folder = '';

	constructor(element, params) {
		this.element = element;

		this.first_folder = params['first_folder'];
		// Menu hover listeners
		this.element.addEventListener('mouseover', (e) => { this.menuItemHovered(e)});
		this.element.addEventListener('mouseout', (e) => { this.clearHovers()});

		// Create new page button in the top right, might be null so ?.
		document.querySelector("a.url-target-create-page")?.addEventListener('click',
			(e) =>{ this.createSubmenu(e);} );

		// Show/hide expansion links
		this.element.addEventListener('click', (e) => { this.showHideClicked(e)});

		// Track rearrange toggle, might be null so ?.
		document.querySelector("a.url-target-rearrange-menus")?.addEventListener('click',
			(e) =>{ this.toggleRearrange(e);} );

		this.configureRearrange();

		// Run the process and cleanup
		this.processCleanup();
	}

	/**
	 * Creates a submenu for the clicked item
	 * @param {Event} event
	 */
	createSubmenu(event) {
		event.preventDefault();

		this.showLoading();
		let parent_id = event.target.getAttribute('data-parent_id');

		let url = '/admin/pages/do/create-page/?return_type=json';

		fetch(url, {
			method : 'POST',
		})	.then(response => response.json())
			.then(response =>{

				if(response.page_id)
				{
					// Wrap in a shell to get the menu item
					let new_menu = document.createElement('div');
					new_menu.innerHTML =response.menu_manager_html;
					new_menu = new_menu.firstElementChild;

					// Append to the list
					let target_parent = this.element.querySelector('#menu_list_0');
					target_parent.append(new_menu);

					// Add the animation
					new_menu.classList.add('new_menu_item');
					setTimeout(() => { new_menu.classList.remove('new_menu_item');},4000);

					// Scroll to the bottom
					window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'});

					this.processCleanup();

				}
				// Replace the values entirely
			}).finally(() =>{
			this.hideLoading();
		});

	}



	/**
	 * Cleans up the child status of elements
	 */
	processCleanup() {
		this.element.querySelectorAll('li').forEach(el => {

			let li_children = el.querySelectorAll('ul > li');

			if(li_children.length > 0)
			{
				el.classList.add('has_children');
				el.classList.remove('no_children');

				// Has children, which means we need to configure the hidden status
				let saved_value = localStorage.getItem('page_menu_hidden_'+el.getAttribute('data-menu-id'));
				if(saved_value === '1')
				{
					el.classList.add('submenu_hidden');
				}
				else // not set or 0. So remove the hidden setting
				{
					el.classList.remove('submenu_hidden');
				}
			}
			else
			{
				el.classList.remove('has_children');
				el.classList.add('no_children');
			}

			// Correct any folders
			this.recalculateFolderForLI(el);
		});


	}

	/**
	 * Recalculates the
	 * @param li
	 */
	recalculateFolderForLI(li_element) {
		let folders = [];

		// Save to ensure we have access to the original value provided
		let li = li_element;
		// Find the parent LI
		//li = li.parentElement.closest('li');

		// Add the hover class to the element and all of its parent li elements
		while(li instanceof HTMLLIElement)
		{
			folders.unshift(li.getAttribute('data-folder'));

			let parent = li.parentElement;
			if(parent)
			{
				li = li.parentElement.closest('li');
			}
			else // can't find a parent also stop looking
			{
				li = null;
			}

		}

		// Push the first folder onto the end
		if(this.first_folder != ''){
			folders.unshift(this.first_folder);
		}


		let path = '/'+folders.join('/') + '/';

		// Replace any possible double-slashes
		path = path.replace(/\/\//g, "/");
		path = path.replace(/\/\//g, "/");
		path = path.replace(/\/\//g, "/");

		// // Catch the homepage special case
		// if(path === '///')
		// {
		// 	path = '/';
		// }

		let folder_text_link = li_element.querySelector('.folder_text');
		if(folder_text_link)
		{
			let class_name = folder_text_link.getAttribute('data-class_name');
			if(class_name != null)
			{
				path += '<em>&lt;' +class_name+ '&gt;</em>/';

				// Disable the link
			}
			else // No class name, update href
			{
				folder_text_link.setAttribute('href', path);
			}

			folder_text_link.innerHTML = path;

		}
	}


	//////////////////////////////////////////////////////
	//
	// REARRANGING
	//
	//////////////////////////////////////////////////////

	/**
	 * Handles the toggling of the rearrange buttons
	 * @param event
	 */
	toggleRearrange(event) {
		event.preventDefault();

		this.element.classList.toggle('rearranging');
	}

	/**
	 * Configures a sortable
	 */
	configureRearrange() {

		// Loop through each <ul>, configuring a sortable with the group
		this.element.querySelectorAll('ul').forEach(el => {
			new Sortable(el, {
				handle : '.rearrange_handle',
				group: 'nested',
				animation: 150,
				fallbackOnBody: true,
				swapThreshold: 0.65,
				onSort		: (e) => {this.rearranged(e); },
			});
		});
	}

	/**
	 * Handles the processing of when something is rearranged
	 * @param {Event} event
	 */
	rearranged(event) {
		this.showLoading();
		let params = new FormData();

		let item_count=0;
		this.element.querySelectorAll('li').forEach(li => {
			item_count++;

			let menu_id = li.getAttribute('data-menu-id');
			params.append('item_'+item_count, menu_id);

			// Find the parent LI
			let parent_id = 0;
			let parent_li = li.parentElement.closest('li');
			if(parent_li)
			{
				parent_id = parent_li.getAttribute('data-menu-id');
			}
			params.append('item_'+item_count+'_parent', parent_id);

		});


		fetch( "/admin/pages/classes/views/TMv_PagesMenuManager/ajax_rearrange_menus.php", {
			method : 'POST',
			body : params,
		})	.finally(() =>{
			this.processCleanup();
			this.hideLoading();
		});



	}

	//////////////////////////////////////////////////////
	//
	// EXPANSION LINKS
	//
	//////////////////////////////////////////////////////

	/**
	 * Adds the 'hover' class to a hovered LI as well as any parent LI elements
	 * @param event
	 */
	showHideClicked(event) {
		let button = event.target.closest('a.show_hide_link');
		if(button)
		{
			let parent_li = button.closest('li');
			let menu_id = parent_li.getAttribute('data-menu-id');

			parent_li.classList.toggle('submenu_hidden');
			event.preventDefault();

			let is_hidden = parent_li.classList.contains('submenu_hidden');
			localStorage.setItem('page_menu_hidden_'+menu_id, is_hidden ? '1' : '0');


		}
	}

	//////////////////////////////////////////////////////
	//
	// LOADING
	//
	//////////////////////////////////////////////////////

	/**
	 * Shows the loading icon
	 */
	showLoading() {
		this.element.classList.add('tungsten_loading');
	}

	/**
	 * Hides the loading icon
	 */
	hideLoading() {
		this.element.classList.remove('tungsten_loading');
	}


	//////////////////////////////////////////////////////
	//
	// MENU HOVERING
	//
	//////////////////////////////////////////////////////

	/**
	 * Clears all the hover flags from the menu manager
	 */
	clearHovers() {
		this.element.querySelectorAll('li.hover').forEach(el => {
			el.classList.remove('hover');
		});
	}

	/**
	 * Adds the 'hover' class to a hovered LI as well as any parent LI elements
	 * @param event
	 */
	menuItemHovered(event) {
		let li = event.target.closest('li');
		if(li)
		{
			// Remove hover class from li
			this.clearHovers();

			// Add LI class to main LI target
			li.classList.add('hover');

			// Find the parent LI
			li = li.parentElement.closest('li');

			// Add the hover class to the element and all of its parent li elements
			while(li instanceof HTMLLIElement)
			{
				li.classList.add('hover');
				let parent = li.parentElement;
				if(parent)
				{
					li = li.parentElement.closest('li');
				}
				else // can't find a parent also stop looking
				{
					li = null;
				}

			}

		}
	}
}