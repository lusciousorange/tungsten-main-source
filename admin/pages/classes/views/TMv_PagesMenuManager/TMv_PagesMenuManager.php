<?php
/**
 * Class TMv_PagesMenuManager
 *
 * A class that shows all the pages on the site and provides means to add/delete and sort those pages.
 */
class TMv_PagesMenuManager extends TCv_View 
{
	protected $items = array(); // [array] = The list of items to be displayed on the dashboard. The first index is the level (starting at 1), then each of those indices has an array with the indices being the id of the item and the value being the object itself.
	protected $possible_items = false; // [array]= The list of dashboard items
	protected $user_id = false; // [int] = the id of the user

	/** @var array|null The array of pages that someone with partial access can view. We only show them these. */
	protected ?array $partial_access_page_ids = null;
	/**
	 * TMv_PagesMenuManager constructor.
	 */
	public function __construct()
	{	
		parent::__construct('TMv_PagesMenuManager');
		
		// Handle workflow, preloading
		if(TC_getConfig('use_workflow'))
		{
			$module = TSm_Module::init('workflow');
			if($module && $module->installed())
			{
				// Preload the matches for pages
				TMm_PagesMenuItem::allWorkflowModelMatches();
				
			}
		}
		
		// Load SortableJS
		// @see https://sortablejs.github.io/Sortable/
		
		$this->addJSFile('SortableJS','https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js');
		
		// USER GROUP PARTIAL ACCESS
		// Only bother if the user doesn't have full access
		$pages_module = TMm_PagesModule::init();
		if(!$pages_module->userHasFullAccess())
		{
			// Instantiate the list to an array, which is our check for this being a thing
			$this->partial_access_page_ids = [];
			
			// Loop through all the pages first
			$pages = TMm_PagesMenuItemList::init();
			foreach($pages->items() as $page)
			{
				// If the user can edit it, then save the ID, plus the IDs of all the ancestors
				if($page->userCanEdit())
				{
					$this->partial_access_page_ids[$page->id()] = $page->id();
					foreach($page->ancestors() as $ancestor)
					{
						$this->partial_access_page_ids[$ancestor->id()] = $ancestor->id();
					}
				}
			}
		}
		
		$first_folder = '';
		if(TC_getConfig('use_localization') && class_exists('TMm_LocalizationModule'))
		{
			$localization = TMm_LocalizationModule::init();
			$first_folder = $localization->loadedLanguage();
		}
		$this->addJSClassInitValue('first_folder',$first_folder);
		
		
	}
	
	/**
	 * Creates a submenu for the provided menu item
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function submenuListForMenuItem($menu_item)
	{
		$menu_list = new TCv_View('menu_list_'.$menu_item->id());
		$menu_list->setTag('ul');
		//$this->addClass('level_0');
		foreach($menu_item->children() as $menu_item)
		{
			// Detect partial access
			if(is_array($this->partial_access_page_ids))
			{
				if(isset($this->partial_access_page_ids[$menu_item->id()]))
				{
					$menu_list->attachView($this->listItemForMenu($menu_item));
				}
			}
			else // full access
			{
				$menu_list->attachView($this->listItemForMenu($menu_item));
			}
			
		}
		
		//$menu_list->attachView($this->addNewListItemForMenu($menu_item));
		
		
		$list_item = new TCv_View('pages_menu_item_'.$menu_item->id());
		$list_item->setTag('li');
		
		
		return $menu_list;
	}
	
	/**
	 * Creats a new view for the menu item and return the <li> item
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */

	public function addNewListItemForMenu($menu_item)
	{
		$list_item = new TCv_View('add_new_'.$menu_item->id());
		$list_item->setTag('li');
		$list_item->addClass('child_'.$menu_item->parentID());
		$list_item->addClass('add_new');
		
			$add_new_button = new TCv_Link();
			$add_new_button->addText('Add menu item');
			$add_new_button->setURL('#');
			$list_item->attachView($add_new_button);
		return $list_item;
		
	}


	/**
	 * Creates a heading row
	 * @return TCv_View
	 */
//	public function headingRow()
//	{
//		$row = new TCv_View();
//		$row->setTag('ul');
//
//		return $row;
//	}


	/**
	 * Creates a page menu item for the provided menu item
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function listItemForMenu($menu_item)
	{
		$list_item = new TCv_View('pages_menu_item_'.$menu_item->id());
		$list_item->setTag('li');
		$list_item->addClass('child_'.$menu_item->parentID());
		$list_item->addClass('menu_item');
		$list_item->addDataValue('menu-id', $menu_item->id());
		$list_item->addDataValue('folder', $menu_item->folder());
		$list_item->addClass('content_entry_'.$menu_item->contentEntry());
		
		if(!$menu_item->isActive())
		{
			$list_item->addClass('deactivated');
		}
		
		$row_box = new TCv_View();
		$row_box->addClass('row_box');
		
		
			// ---- SHOW / HIDE LINK -----
		if($menu_item->numChildren() > 0)
		{
			$show_hide_link = new TCv_Link();
			$show_hide_link->setURL('#');
			$show_hide_link->setIconClassName('fa-chevron-down');
//			// Deal with hidden
//			// TODO: Stop storing this in sessions. Jeez
//			$is_hidden = isset($_SESSION['TMv_PagesMenuManager_hide_'.$menu_item->id()]);
//
//			if($is_hidden)
//			{
//				$list_item->addClass('submenu_hidden');
//				$show_hide_link->setIconClassName('fa-chevron-right');
//
//			}
//			else
//			{
//				$show_hide_link->setIconClassName('fa-chevron-down');
//
//			}
		}
		else
		{
			$show_hide_link = new TCv_View();
			$show_hide_link->setTag('span');
			
		}
		
		
		
		$show_hide_link->addClass('right_icon');
		$show_hide_link->addClass('show_hide_link');
		//$show_hide_link->addDataValue('menu-id', $menu_item->id());
		//$show_hide_link->addClass('list_control_button');
		$row_box->attachView($show_hide_link);
		
		
		// ---- REARRANGE HANDLE -----
		if($menu_item->userCanEdit())
		{
			$sort_link = new TCv_Link();
			$sort_link->setURL('#');
			$sort_link->setIconClassName('fa-sort','fa-sort');
			$sort_link->addClass('rearrange_handle');
			
			$sort_link->addClass('list_control_button');
			$row_box->attachView($sort_link);
			
		}
		
		// ---- MAIN TITLE LINK -----
		$row_box->attachView($this->titleLink($menu_item));

		// ---- FOLDER TEXT -----
		$row_box->attachView($this->folderText($menu_item));

		// ---- VISIBILITY -----
		$row_box->attachView($this->visibilityColumn($menu_item));

		// ---- FLAGGING -----
		$row_box->attachView($this->addFlags($menu_item));
		
		// ---- MANAGE CONTENT -----
		$row_box->attachView($this->manageContentButton($menu_item));
		
		// ---- EDIT SETTINGS -----
		$row_box->attachView($this->editSettingsButton($menu_item));
		
		// ---- DELETE BUTTON -----
		$row_box->attachView($this->deleteButton($menu_item));
		
		// ---- WORKFLOW -----
		if($workflow_view = $this->workflowView($menu_item))
		{
			$row_box->attachView($workflow_view);
		}
		
		
		$list_item->attachView($row_box);

		$list_item->attachView($this->submenuListForMenuItem($menu_item));
		
		
		// Main button
		
		return $list_item;
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View|TCv_Link
	 */
	protected function titleLink($menu_item)
	{
		if($menu_item->userCanEdit() &&  $menu_item->isContentManaged())
		{
			$edit_link = new TCv_Link();
			$edit_link->setURL('/admin/pages/do/page-builder/'.$menu_item->id());
			$edit_link->setTitle($menu_item->title());
		}
		else
		{
			$edit_link = new TCv_View();
			$edit_link->setTag('span');
			$edit_link->addClass('no_access');
		}
		$edit_link->addClass('title_link');
		$edit_link->addText($menu_item->title());
		if($menu_item->contentEntry() != 'manage')
		{
			$content_entry_details = new TCv_View();
			$content_entry_details->setTag('span');
			$content_entry_details->addClass('content_entry_details');
			$content_entry_details->addText('<i class="fa fa-share"></i> ');
			if($redirect = $menu_item->redirect())
			{
				$content_entry_details->addText('Redirects to '.$redirect);
			}
			elseif($menu_item->contentEntry() == 'class_method')
			{
				$content_entry_details->addText('<em>'.$menu_item->redirectClassNameFriendly().
				                                ' <i class="fa fa-angle-right"></i> '.$menu_item->redirectClassMethod().'</em>');
			}
			$edit_link->attachView($content_entry_details);
		}
		
		elseif($menu_item->isModelList())
		{
			$content_entry_details = new TCv_View();
			$content_entry_details->setTag('span');
			$content_entry_details->addClass('model_list_navigation_note');
			$content_entry_details->addText('<i class="fa fa-list"></i> ');
			
			/** @var TCm_ModelList $model_list */
			$model_list = ($menu_item->modelListClassName())::init();
			$model_name_plural = call_user_func(array($model_list->modelClassName(), 'modelTitlePlural'));
			
			$content_entry_details->addText('Navigation shows list of '.$model_name_plural);
		
			$edit_link->attachView($content_entry_details);
		}
		
		
		return $edit_link;
	}
	
	/**
	 * 
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	protected function folderText($menu_item)
	{
		if($menu_item->isModelList())
		{
			$folder_text = new TCv_View();
			$folder_text->setTag('span');
			$folder_text->addClass('model_list_folder_block');
			
			
		}
		else
		{
			$folder_text = new TCv_Link();
			$folder_text->setURL($menu_item->pathToFolder());
			$folder_text->openInNewWindow();
		}
		
		$folder_text->addClass('folder_text');
		$folder_text->addText($menu_item->pathToFolder());
		$menu_class_name =$menu_item->modelClassName();
		if($menu_class_name != '')
		{
			$folder_text->setTag('span');
			$folder_text->addClass('model_list_folder_block');
			
			if(class_exists($menu_class_name))
			{
				$primary_id_column = $menu_class_name::$table_id_column;
				$folder_text->addText('<em>&lt;'.$primary_id_column.'&gt;</em>/');
				$folder_text->addDataValue('class_name', $primary_id_column);
			}
			
		}
		if($menu_item->isModelList())
		{
			// Add the <model_id> to the end of the URL
			// Only bother if a model isn't set, otherwise already indicated
			if($menu_class_name == '')
			{
				$folder_text->addText('<em>&lt;model_id&gt;</em>/');
			}
			
			/** @var TCm_ModelList $model_list */
			$model_list = ($menu_item->modelListClassName())::init();
			$model_name_plural = call_user_func(array($model_list->modelClassName(), 'modelTitlePlural'));
			
			$folder_text->addText('<br />Auto-generated list of '.$model_name_plural);
		}
		
		return $folder_text;
	}
	
	/**
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	protected function visibilityColumn($menu_item)
	{
		$visibility_block = new TCv_View();
		$visibility_block->addClass('visibility_block');
		$visibility_block->setTag('span');
		
		// DEACTIVATED
		if(!$menu_item->isActive())
		{
			$icon_view = new TSv_FontAwesomeSymbol('eye-slash', 'fa-times fa-fw');
			$visibility_block->attachView($icon_view);
			$visibility_block->addText('<em>Deactivated</em>');
			return $visibility_block;
		}
		
		
		if($menu_item->isModelList())
		{
			//	$icon_view = new TSv_FontAwesomeSymbol('model_list_icon', 'fa-list fa-fw');
			
			
			$visibility_block->addText('<i class="fa-list fa-fw"></i>Model list');
			//	$list_item->addClass('invisible');
			
		}
//		elseif($menu_item->isVisibleValue() == 0)
//		{
//			$icon_view = new TSv_FontAwesomeSymbol('eye-slash', 'fa-eye-slash fa-fw');
//
//			$visibility_block->attachView($icon_view);
//			//	$list_item->addClass('invisible');
//		}
//		else
//		{
//			$icon_view = new TSv_FontAwesomeSymbol('eye', 'fa-eye fa-fw');
//			$visibility_block->attachView($icon_view);
//
//		}
		else // Find navigation types
		{
			$navigation_types = $menu_item->navigationTypes();
			
			if(count($navigation_types) == 0)
			{
				$item = new TCv_View();
				$item->addClass('nav_type empty');
				$item->addText('––');
				$visibility_block->attachView($item);
			}
			else
			{
				foreach($navigation_types as $type)
				{
					$item = new TCv_View();
					$item->addClass('nav_type');
					$item->addText($type->title());
					$visibility_block->attachView($item);
				}
			}
			
		}
		//$visibility_block->addText($menu_item->visibilityName());
		
		
		$requirements  = array();
		
		if($menu_item->requiresAuthentication())
		{
			$requirements [] = "Login";
		}
		
		$menu_class_name =$menu_item->modelClassName();
		if($menu_class_name != '')
		{
			if(class_exists($menu_class_name))
			{
				$requirements [] = $menu_class_name::$model_title;
			}
			
		}
		
		if(count($requirements) > 0)
		{
			//$visibility_block->addText(' <br />');
			$icon_view = new TSv_FontAwesomeSymbol('angle-right', 'fa-angle-right fa-fw');
			$visibility_block->attachView($icon_view);
			$visibility_block->addText('<em>'.implode( ' & ',$requirements).' Required</em>');
			
		}
		
		
		$condition = trim($menu_item->visibilityCondition());
		if($condition != '')
		{
			$condition = str_replace(array('{{','}}',''), '', $condition);
			$condition = str_replace(array('TMm_','TSm_','TCm_'), '', $condition);
			
			$visibility_block->addText(' <br />');
			$icon_view = new TSv_FontAwesomeSymbol('angle-right', 'fa-angle-right fa-fw');
			$visibility_block->attachView($icon_view);
			$visibility_block->addText('<em>'.$condition.'</em>');
		}
		
		
		return $visibility_block;
	}
	
	/**
	 * Generates the view for the workflow, only bothering if it's set
	 * @param TMm_PagesMenuItem $menu_item
	 * @return string|TCv_View
	 *
	 */
	public function workflowView($menu_item ) : ?TMv_WorkflowItemSummaryListColumn
	{
		// ---- WORKFLOW -----
		if(TC_getConfig('use_workflow'))
		{
			$this->addClass('uses_workflow');
			return TMv_WorkflowItemSummaryListColumn::init($menu_item);
		

		}

		return null;
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	public function addFlags(TMm_PagesMenuItem $menu_item)
	{
		if(TC_getConfig('use_content_flagging'))
		{
			$button = new TMv_ContentFlagButton($menu_item);
			$button->addClass('list_control_button');
			return $button;
		}
		return null;
	}
	
	
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	public function manageContentButton($menu_item)
	{
		if($menu_item->userCanEdit())
		{
			$content_link = new TCv_Link();
			$content_link->setURL('/admin/pages/do/page-builder/' . $menu_item->id());
			$content_link->setTitle('Manage content');
			//$content_link->addClass('fa-fw');
			if($menu_item->isContentManaged())
			{
				$content_link->setIconClassName('fa-pencil', 'pencil');
				
			}
			
		}
		else
		{
			$content_link = new TCv_View();
			$content_link->setTag('span');
		}
		$content_link->addClass('list_control_button');
		
		return $content_link;
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	public function editSettingsButton($menu_item)
	{
		if($menu_item->userCanEdit())
		{
			$edit_link = new TCv_Link();
			$edit_link->setURL('/admin/pages/do/edit/' . $menu_item->id());
			$edit_link->setTitle('Edit settings');
			$edit_link->setIconClassName('fa-cog', 'cog');
		}
		else
		{
			$edit_link = new TCv_View();
			$edit_link->setTag('span');
		}
	
		$edit_link->addClass('list_control_button');
		return $edit_link;
		
	}
	
	/**
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link|TCv_View
	 */
	public function deleteButton($menu_item)
	{
		if($menu_item->userCanEdit())
		{
			$delete_link = new TCv_Link();
			$delete_link->setURL('/admin/pages/do/delete-menu-item/' . $menu_item->id());
			$delete_link->addClass('right_icon');
			$delete_link->setIconClassName('fa-trash', 'trash');
			$delete_link->addClass('list_control_button');
			$warning = 'Are you sure you want to delete this menu item?';
			if($menu_item->numChildren() > 0)
			{
				$warning .= 'All submenus will ALSO be deleted.';
			}
			
			$delete_link->useDialog($warning);
			
		}
		else
		{
			$delete_link = new TCv_View();
			$delete_link->setTag('span');
		}
		
		$delete_link->addClass('delete_content_button');
		
		return $delete_link;
	}
	
	/**
	 * A method that adds the control buttons
	 * @param TCv_View $row_box the row box that is being added to
	 * @return string
	 */
	public function addControlButtons($row_box)
	{

	}

	/**
	 * Generates the HTML for thie view
	 * @return string
	 */
	public function html()
	{
		$this->addClassCSSFile('TMv_PagesMenuManager');
		$this->addClassJSFile('TMv_PagesMenuManager');
		$this->addClassJSInit('TMv_PagesMenuManager');
		
		TMm_PagesMenuItemList::init();
		
		/** @var TMm_PagesMenuItem $menu_0 */
		$menu_0 = TMm_PagesMenuItem::init(0);
		$this->attachView($this->submenuListForMenuItem($menu_0));
	
		return parent::html();
		
	}
	
	/**
	 * Help html
	 * @return string|null
	 */
	public static function defaultHelpHTML(): ?string
	{
		return
'<p>This is where you manage the pages/menus that will appear on your website, along with the content shown on them.</p>
<ul>
	<li>Adding a page will create a new one at the end of the list.</li>
	<li>To rearrange the page, click on the Rearrange button then drag-and-drop them into the correct location. Make sure to pay attention to indentation since that indicates hierarchy. </li>
	<li>To edit the page content, click on the name of the page. </li>
</ul>
';
	}
	
	/**
	 * The help view to explain the manager
	 * @return TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('This is where you manage the pages/menus that will appear on your website, along with the content shown on them.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Create New Page</strong>: Click on the ');
				$link = new TSv_HelpLink('New Page', '.url-target-add-menu-item');
				$p->attachView($link);
			$p->addText(' button.');
			
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Rearrange Pages</strong>: Drag and drop them using the ');
				$link = new TSv_HelpLink('', '.rearrange_handle');
				$link->addClass('fa-sort');
				$p->attachView($link);
			$p->addText(' handle.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Toggle Navigation Visibility</strong>: Click on the ');
				$link = new TSv_HelpLink('', '.toggle_visibility_button');
				$link->addClass('fa-eye');
				$p->attachView($link);
			$p->addText(' button to toggle the visibility of a page. Invisible pages won\'t appear in the site\'s navigation, but can still be loaded in the browser.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Add/Edit Page Content</strong>: Click on the ');
				$link = new TSv_HelpLink('Page Title', '.title_link');
				$p->attachView($link);
			$p->addText(' or the ');
				$link = new TSv_HelpLink('', '.fa-pencil.list_control_button');
				$link->addClass('fa-pencil');
				$p->attachView($link);
			$p->addText('  button.');
			$help_view->attachView($p);
		
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Edit Page Settings</strong>: Click on the ');
				$link = new TSv_HelpLink('', '.fa-cog.list_control_button');
				$link->addClass('fa-cog');
				$p->attachView($link);
			$p->addText(' button to change the menu title, folder or other settings for the page.');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>View Live Page</strong>: Click on the ');
				$link = new TSv_HelpLink('Page Link', '.folder_text');
				$p->attachView($link);
			$p->addText('.');
			
			$help_view->attachView($p);
			
			
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('<strong>Delete Page</strong>: Click on the ');
				$link = new TSv_HelpLink('', '.fa-trash.list_control_button');
				$link->addClass('fa-trash');
				$p->attachView($link);
				
			$p->addText(' button to delete a page. The system will also delete any sub-pages as well as the content on any deleted page.');
			$help_view->attachView($p);
			
		return $help_view;
	}


	



}
?>