<?php
/**
 * Class TMv_VimeoEmbedded
 */
class TMv_VimeoEmbedded extends TCv_View 
{
	use TMt_PagesContentView;
	
	protected $url = '';
	protected $title = '';


	/**
	 * TMv_VimeoEmbedded constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Returns the HTML for this view. Each subclass must define what html is returned when it to be displayed.
	 * @return bool
	 */
	public function vimeoCode()
	{
		// Eventually use the API
		//https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/76979871
		
		$parts = explode('/', $this->url);
		foreach($parts as $section)
		{
			if($section != '')
			{
				if(is_numeric($section))
				{

					return $section;
				}
					
			}	
		}
		
		return false;
	}

	/**
	 * @return string
	 */
	public function html()
	{
		$this->addClass('responsive_iframe');
		
		$code = $this->vimeoCode();
		$video_tag = new TCv_View();
		$video_tag->setTag('iframe');
		
		$video_tag->setAttribute('title', $this->title);
		$video_tag->setAttribute('width', 600);
		$video_tag->setAttribute('height', 340);
		$video_tag->setAttribute('frameborder', '0');
		$video_tag->setAttribute('webkitAllowFullScreen', 'webkitAllowFullScreen');
		$video_tag->setAttribute('mozallowfullscreen', 'mozallowfullscreen');
		$video_tag->setAttribute('allowFullScreen', 'allowFullScreen');
		
		$video_tag->setAttribute('src', 'https://player.vimeo.com/video/'.htmlspecialchars($code));
		
		$this->attachView($video_tag);
		
		return parent::html();
	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
	
		$field = new TCv_FormItem_TextField('url', 'Vimeo URL');
		$field->setDefaultValue($this->url);
		$field->setHelpText('The code from the Vimeo URL. ');
		$form_items[] = $field;
		
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setDefaultValue($this->title);
		$field->setHelpText('The title for this video');
		$form_items[] = $field;
		
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string  { return 'Vimeo embedded video'; }
	public static function pageContent_IconCode() : string  { return 'fab fa-vimeo'; }

	public static function pageContent_ViewDescription() : string
	{ 
		return 'An embedded Vimeo video.';
	}
	
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return true;
	}
	
	
}

?>