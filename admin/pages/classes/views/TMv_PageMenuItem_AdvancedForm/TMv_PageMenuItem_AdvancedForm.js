class TMv_PageMenuItem_AdvancedForm extends TCv_Form {

	constructor(element, params) {
		super(element, params);

		// Add event listener from TCv_Form JS class
		this.addFieldEventListener('content_entry','change',this.contentEntryChanged,true);
		this.addFieldEventListener('is_model_list','change',this.isModelListChanged,true);
		this.addFieldEventListener('model_class_name','input',this.classNameChanged, true);


	}

	/**
	 * Triggered whenever the content entry type is changed
	 */
	contentEntryChanged() {
		this.hideFieldRows('.table_row.content_entry_option_field');

		let content_entry_field_value = this.fieldValue('content_entry');

		if(content_entry_field_value === 'manage')
		{
		}
		else if(content_entry_field_value === 'redirect')
		{
			this.showFieldRows('redirect');
		}
		else if(content_entry_field_value === 'redirect_to_menu')
		{
			this.showFieldRows('redirect_to_menu');
		}
		else if(content_entry_field_value === 'class_method')
		{
			this.showFieldRows([
				'redirect_class_name_heading',
				'redirect_class_name',
				'redirect_class_method',
				'redirect_class_result_page',
				'redirect_class_target'

			]);
		}

		// update the Target Page
		//this.updateTargetPage();

	}

	/**
	 * Triggered when the visibility changes
	 */
	isModelListChanged() {
		this.hideFieldRows('model_list_class_name');

		if(this.element.querySelector('#is_model_list').value  === '1')
		{
			this.showFieldRows('model_list_class_name', true);
		}
	}

	/**
	 * Triggered when the photo type changed
	 */
	classNameChanged() {
		if(this.fieldValue('model_class_name') === '')
		{
			this.hideFieldRows([
				'model_class_name_title_display',
				'model_class_name_primary_view_page',
			], true);
		}
		else
		{
			this.showFieldRows([
				'model_class_name_title_display',
				'model_class_name_primary_view_page',
			], true);
		}
	}
}