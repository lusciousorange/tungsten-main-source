<?php

/**
 * Class TMv_PageMenuItem_AdvancedForm
 *
 * The form to edit a menu item's photo.
 */
class TMv_PageMenuItem_AdvancedForm extends TCv_FormWithModel
{
	protected int $crop_width = 0;
	protected int $crop_height = 0;
	
	/**
	 * TMv_PageMenuItem_PhotoForm constructor.
	 * @param bool|TMm_PagesMenuItem $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		
		$this->setButtonText('Update advanced settings');
		
		$this->addClassJSFile('TMv_PageMenuItem_AdvancedForm');
		$this->setIDAttribute('TMv_PageMenuItem_AdvancedForm');
	}
	
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
		
		$options = new TCv_FormItem_Select('content_entry', 'Content management');
		$options->setDefaultValue('manage');
		$options->addOption('manage', 'Use Pages Content (Recommended)');
		$options->addOption('redirect_to_menu', 'Redirects to Another Menu Item');
		$options->addOption('redirect', 'Redirects to a URL');
		$options->addOption('class_method', 'Call a PHP Class Method (Advanced)');
		
		$options->setHelpText('Indicate how menu deals with content. Normally managed in Pages module, sometimes you want the menu to redirect. ');
		$this->attachView($options);
		
		$redirect = new TCv_FormItem_TextField('redirect', 'Redirect URL');
		$redirect->addClass('content_entry_option_field');
		$redirect->setHelpText('Menu opens this URL instead of default');
		$this->attachView($redirect);
		
		$redirect = new TCv_FormItem_Select('redirect_to_menu', 'Redirect menu item');
		$redirect->addClass('content_entry_option_field');
		$redirect->setHelpText('Select the menu that this will redirect to');
		$redirect->useFiltering();
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$this->attachView($redirect);
		
		$field = new TCv_FormItem_TextField('visibility_condition', 'Visibility condition');
		$field->setHelpText('A system-call string such as {{module_folder.method}} which returns a boolean
		value that will be used to determine if this items should appear on the website.');
		$field->setPlaceholderText('eg: {{module_folder.method}}');
		$this->attachView($field);
		
		
		
		$class_name = new TCv_FormItem_Heading('redirect_class_name_heading', 'PHP class method settings');
		$class_name->addClass('content_entry_option_field');
		$class_name->setHelpText('This features requires advanced knowledge of the internal programming of the website.');
		$this->attachView($class_name);
		
		$class_name = new TCv_FormItem_TextField('redirect_class_name', 'Class name');
		$class_name->addClass('content_entry_option_field');
		$class_name->setHelpText('The name of the PHP class that should be called. It will be instantiated with the ID provided through the URL');
		$this->attachView($class_name);
		
		$method_name = new TCv_FormItem_TextField('redirect_class_method', 'Method name');
		$method_name->addClass('content_entry_option_field');
		$method_name->setHelpText('The name of the method in the class that should be trigger. Up to two additional parameters can be passed through the URL using additional slashes to separate them.');
		$this->attachView($method_name);
		
		$class_target = new TCv_FormItem_Select('redirect_class_target', 'Result target page');
		$class_target->addClass('content_entry_option_field');
		$class_target->setHelpText('Select the page that will be displayed after the processing is complete.');
		$class_target->addOption('referrer', 'Return to original page (referrer)');
		
		$parent_id = 0;
		$parent_title = '';
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$class_target->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$this->attachView($class_target);
		
		$field = new TCv_FormItem_Select('is_model_list', 'Model list submenus');
		$field->setHelpText("Indicate if this menu should be replaced with a list of items from a module. ");
		$field->addOption('0', 'NO – Regular navigation ');
		$field->addOption('1', 'YES – Menu replaced with a list of menus for a model');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_TextField('model_list_class_name', 'Model list class name');
		$field->setHelpText('The name of the class that has the list items. This is usually a subclass of <em>TCm_ModelList</em>. By default the
method <em>modelsVisible()</em> is called, but that can be overridden by using a double-colon followed by the method name to be called.');
		$this->attachView($field);
		
		
		
		
		if($this->isEditor())
		{
			$field = new TCv_FormItem_HTML('duplicate', 'Duplicate');
			$field->setHelpText('Duplicates this page.');
			$field->setShowTitleColumn(false);
			$button = new TCv_Link();
			$button->setURL('/admin/pages/do/duplicate-page/' . $this->model()->id());
			$button->addText('Duplicate this page');
			$button->useDialog('Are you sure you want to duplicate this page and all the content on it?');
			$field->attachView($button);
			$this->attachView($field);
		}
		
		$this->modelTemplateFields();
		
	}
	
	public function modelTemplateFields()
	{
		$class_name = new TCv_FormItem_Heading('model_class_heading', 'Model template');
		$class_name->setHelpText('Template pages require a specific model in order to load. The ID is part of URL and the page loads content based on that model.');
		$this->attachView($class_name);
		
		//$field = new TCv_FormItem_TextField('model_class_name', 'Input model');
		$field = new TCv_FormItem_Select('model_class_name', 'Input model');
		$field->addOption('', 'None');
		$model_names = TC_modelsWithTrait('TMt_PageModelViewable');
		foreach($model_names as $model_name)
		{
			$field->addOption($model_name, $model_name::modelTitlePlural());
		}
		$field->setHelpText('Select the model that must be found to load this page. ');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('model_class_name_primary_view_page', 'Model mode');
		$field->setHelpText("Models can have multiple modes. Indicating a mode tells the system that when looking for a particular mode for a model, this is the page to find. Most commonly used for 'View' to indicate that viewing the item should take them to this page. ");
		$field->addOption('', 'None');
		
		$called_class = TMm_PagesMenuItem::classNameForInit();
		
		foreach($called_class::$public_page_model_modes as $code => $explanation)
		{
			$field->addOption($code, $explanation);
		}
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_RadioButtons('model_class_name_title_display', 'Title display');
		$field->setHelpText("Indicate how the title of this menu is displayed in headings and breadcrumbs.");
		$field->addOption(null, '<strong>Model title:</strong> The page title will show the name of the specific model being loaded.');
		$field->addOption('page_title', '<strong>Template page title:</strong> The page title will always show the title for this template page, regardless of which model is being loaded.');
		$this->attachView($field);
		
		
	}
	
}