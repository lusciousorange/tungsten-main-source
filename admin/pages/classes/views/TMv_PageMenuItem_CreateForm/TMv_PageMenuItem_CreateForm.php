<?php
class TMv_PageMenuItem_CreateForm extends TMv_PageMenuItem_AttributesForm
{
	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct('');
		
		$this->setButtonText('Create new page');
		$this->setIDAttribute('TMv_PageMenuItem_CreateForm');
		
	//	$this->setSuccessURL('page-builder');
		
	}
	
	
	/**
	 * Extended view only shows the one section
	 */
	public function configureFormElements()
	{
		parent::configureFormElements();


		$heading = new TCv_FormItem_Heading('visibility_heading', 'Visibility ');
		$this->attachView($heading);
		
		$field = TMv_PageMenuItem_DisplayForm::contentVisibleField();
		$field->setDefaultValue('1');
		$this->attachView($field);
		
		$field = TMv_PageMenuItem_DisplayForm::authenticationField();
		if($field)
		{
			$field->setDefaultValue('0');
			$this->attachView($field);
			
		}
		
		
		
		
	}
	
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// Add some values that need to be included
		$form_processor->addDatabaseValue('content_entry','manage');
		
		// Calculate the number of children
		$parent = TMm_PagesMenuItem::init($form_processor->formValue('parent_id'));
		$form_processor->addDatabaseValue('display_order',$parent->numChildren()+1);
		
		
		parent::customFormProcessor_performPrimaryDatabaseAction($form_processor);
	}
	
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);
		
		$new_page = $form_processor->model();
		
		$form_processor->setSuccessURL('/admin/pages/do/page-builder/'.$new_page->id());
		
	}

//	/**
//	 * Set the required model for the form
//	 * @return string|null
//	 */
//	public static function formRequiredModelName(): ?string
//	{
//		return 'TMm_IMaginaryClass';
//	}
}