<?php

/**
 * A form to show a navigation type
 */
class TMv_PagesNavigationTypeForm extends TCv_FormWithModel
{
	/**
	 * TMv_Pages301RedirectForm constructor.
	 * @param string|TMm_PagesNavigationType $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setSuccessURL('navigation-list');
		
	}
	
	/**
	 * Set the required model for the form
	 * @return class-string<TCm_Model>|null
	 */
	public static function formRequiredModelName(): ?string
	{
		return 'TMm_PagesNavigationType';
	}
	
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setPlaceholderText('Utilities, Footer, etc');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('short_code', 'Short code');
		$field->setPlaceholderText('utilities, sub_nav, etc');
		$field->setIsRequired();
		$field->setAsRequiresURLSafe();
		$field->setMaxLength(16);
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('is_flat','Render as flat');
		$field->setHelpText('Indicates if this navigation item renders as a flat list, regardless of where they exist in the structure');
		if($this->isEditor() && $this->model()->id() == 1)
		{
			$field->disable();
		}
		
		$field->addOption(0, 'No - Menus rendered based on their hierarchy');
		$field->addOption(1, 'Yes - Show all menus as one level, regardless of hierarchy');
		
		$this->attachView($field);
	
	}
	
	
}