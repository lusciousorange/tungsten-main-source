<?php
class TMv_PagesViewStyleForm extends TCv_FormWithModel
{

	/**
	 * TMv_PagesViewStyleForm constructor.
	 * @param string|TCm_Model $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		$this->setSuccessURL('css-style-list');
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$field->setHelpText('A title for this css style. ');
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('css_class', 'Class name');
		$field->setHelpText('The CSS class name which code for this setting. This is how a developer can reference this value for later use.');
		$field->setNoSpaces();
		$field->setIsRequired();
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('apply_to_container', 'Apply to content container');
		$field->setHelpText('For styles applied to content items, indicate if this should be applied to the content container, instead of the element itself. Used for controlling styles in CSS.');
		$field->setIsRequired();
		$field->addOption('0','No');
		$field->addOption('1','Yes');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('required_view_classes', 'Usage restriction');
		$field->setHelpText('Indicate if there is restriction for using this class in the page editor.');
		$field->setUseMultiple(' ');
		$field->useFiltering();
		$field->setIsRequired();
		//$field->addOption('','No Restrictions');
		
		$field->addOption('TMv_ContentLayout_Section','Sections');
		$field->addOption('TMv_ContentLayout','Rows (inside sections)');
		$field->addOption('TMv_ContentLayoutBlock','Columns');
		$field->addOption('TMt_PagesContentView','Any content item');
		$field->startOptionGroup('content_items_group','Specific content items');
		$content_list  = TMm_PageContentList::init();
		$content_options = $content_list->pageContentOptions();
		foreach($content_options as $class_name)
		{
			if ($class_name::pageContent_IsAddable())
			{
				$field->addOption($class_name, $class_name::pageContent_ViewTitle().' ['.$class_name.']');
			}
		}
		$field->startOptionGroup('content_layouts_group','Content Layouts');

		/** @var TMm_PageLayoutList $layout_list */
		$layout_list  = TMm_PageLayoutList::init();

		$layouts = $layout_list->pageLayouts();
		ksort($layouts);
		foreach($layouts as $class_name => $path)
		{
			if( $class_name != 'TMv_ContentLayout_Empty' // deprecated, should not show
				&& $class_name != 'TMv_ContentLayout_Section'
				&& !is_subclass_of($class_name, 'TMv_ContentLayout_Section'))
			{
				$field->addOption($class_name, $class_name::pageContent_ViewTitle() );
			}
		}

		$this->attachView($field);

		$field = new TCv_FormItem_TextField('description', 'Description');
		$this->attachView($field);



	}
	
}