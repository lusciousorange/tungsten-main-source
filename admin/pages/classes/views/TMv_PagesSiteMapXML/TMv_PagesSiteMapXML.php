<?php

/**
 * A view that generates an XML list of all the pages for the sake of a sitemap for a website.
 * @author Paulo Fernandes
 */
class TMv_PagesSiteMapXML extends TCv_View
{
	protected $url_set;
	
	protected $sitemap_model_names = null;
	
	public function render()
	{
		// Deal with the head
		header ("Content-Type:application/xml");
		print '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
		print '<?xml-stylesheet type="text/xsl" href="/admin/pages/classes/views/TMv_PagesSiteMapXML/sitemap_xsl_stylesheet.xml" ?>';
	
		// Grab the class name. If it exists, we're processing only one class
		$class_name = null;
		if(isset($_GET['c']))
		{
			if($_GET['c'] == 'Page')
			{
				$class_name = 'TMm_PagesMenuItem';
			}
			else
			{
				$class_name = 'TMm_'.$_GET['c'];
			}
			
			
			
		}
		
		// Shortcut if there are no other model names
		if(count($this->sitemapModelNames()) == 0)
		{
			$class_name = 'TMm_PagesMenuItem';
		}
		
		
		// Load a url set for a class
		if($class_name)
		{
			$this->setTag('urlset');
			
			// Generate the appropriate list
			if($class_name == 'TMm_PagesMenuItem')
			{
				$this->generatePageListing();
			}
			else
			{
				$this->generateModelListing($class_name);
			}
		}
		else
		{
			$this->setTag('sitemapindex');
			$this->generateSitemaps();
		}
		$this->setAttribute('xmlns',"http://www.sitemaps.org/schemas/sitemap/0.9");
		$this->setAttribute('xmlns:xhtml',"http://www.w3.org/1999/xhtml");
		// ADD PAGES
		
	}
	
	//////////////////////////////////////////////////////
	//
	// SITEMAP INDEX
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the <sitemap> view for a single classname
	 * @param string $class_name
	 * @return TCv_View
	 */
	protected function sitemapViewForClassName($class_name)
	{
		$code =str_replace('TMm_','',$class_name);
		
		// Vanity change
		if($class_name == 'TMm_PagesMenuItem')
		{
			$code = 'Page';
		}
		
		$url = new TCv_View();
		$url->setTag('sitemap');
		
		$loc = new TCv_View();
		$loc->setTag('loc');
		$loc->addText($this->fullDomainName().'/sitemap.xml?c='. $code);
		
		$url->attachView($loc);
		
		return $url;
	}
	
	/**
	 * Returns the array of model names that are page model viewable and should appear on the sitemap
	 * @return array
	 */
	protected function sitemapModelNames()
	{
		if($this->sitemap_model_names === null)
		{
			$this->sitemap_model_names = [];
			
			$model_names = TC_modelsWithTrait('TMt_PageModelViewable');
			foreach($model_names as $model_name)
			{
				if($model_name::showOnSitemap())
				{
					$model_list_class = $model_name . 'List';
					if(class_exists($model_list_class))
					{
						$this->sitemap_model_names[$model_name] = $model_name;
					}
				}
				
			}
		}
		
		return $this->sitemap_model_names;
	}
	
	
	/**
	 * Adds all sitemaps which are for all the public viewable content
	 */
	protected function generateSitemaps()
	{
		// Pages
		$this->attachView($this->sitemapViewForClassName('TMm_PagesMenuItem'));
		
		foreach($this->sitemapModelNames() as $model_name)
		{
			$this->attachView($this->sitemapViewForClassName($model_name));
		}
		
	}
	//////////////////////////////////////////////////////
	//
	// PAGES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Generates a single URL tag for a model
	 * @param TCm_Model $model
	 * @return TCv_View
	 */
	protected function urlTagForModel($model)
	{
		$url = new TCv_View();
		$url->setTag('url');
		
		$loc = new TCv_View();
		$loc->setTag('loc');
		$loc->addText($this->fullDomainName(). $model->pageViewURLPath());
		
		$url->attachView($loc);
		
		// Last Modified
		$loc = new TCv_View();
		$loc->setTag('lastmod');
		$loc->addText($model->dateModifiedFormatted('c'));
		$url->attachView($loc);
		
		return $url;
	}
	
	/**
	 * Returns the <url> view for a single model that is provided
	 * @param TCm_Model $model
	 * @return array
	 */
	protected function listingViewsForModel($model)
	{
		$views = [];
		$views[] = $this->urlTagForModel($model);
		
		return $views;
	}
	
	/**
	 * Adds all the public pages that should appear in the XML.
	 */
	protected function generatePageListing()
	{
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			if($menu_item->appearsOnSitemap())
			{
				$this->addText("\n");
				foreach($this->listingViewsForModel($menu_item) as $url_view)
				{
					$this->attachView($url_view);
				}
				
				
			}
		}
	}
	
	/**
	 * Adds all the public pages that should appear in the XML.
	 */
	protected function generateModelListing($model_name)
	{
		$model_list_class = $model_name . 'List';
		if(class_exists($model_list_class))
		{
			//$values[]
			$model_list = $model_list_class::init();
			foreach($model_list->models() as $model)
			{
				if($model->appearsOnSitemap())
				{
					$this->addText("\n");
					foreach($this->listingViewsForModel($model) as $url_view)
					{
						$this->attachView($url_view);
					}
					
				}
			}
		}
	}
	
}