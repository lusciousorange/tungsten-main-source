<?php
class TMv_PagesMenuItemManagerList extends TCv_SearchableModelList
{
	protected bool $user_can_rearrange = true;
	
	/**
	 * TMv_PageMenuItemManagerList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	
		$this->setAsHorizontalScrolling();
		
		$this->setModelClass('TMm_PagesMenuItem');
		$this->defineColumns();
		
		$this->addClassCSSFile('TMv_PagesMenuItemManagerList');
		$this->addClassJSFile('TMv_PagesMenuItemManagerList');
		$this->addClassJSInit('TMv_PagesMenuItemManagerList');
		
		$this->addFontAwesomeClassAsSymbol('fa-angle-right', 'fa-angle-right fa-fw');
		$this->addFontAwesomeClassAsSymbol('sort-up', 'fa-arrow-up fa-fw');
		$this->addFontAwesomeClassAsSymbol('sort-down', 'fa-arrow-down fa-fw');
		
		
		$pages_module = TMm_PagesModule::init();
		$this->user_can_rearrange = $pages_module->userHasFullAccess();
		
		$this->addText('<div class="rearrange_warning">Rearranging is disabled when filters are applied</div>');
		
	}
	
	/**
	 * @param TMm_PagesMenuItem $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		$row->addClass('level_'.$model->level());
		$row->addDataValue('level', $model->level());
		$row->addDataValue('order', $model->displayOrder());
		$row->addDataValue('parent', $model->parentID());
		//$row->addClass('order_'.$model->displayOrder());
		
		
		if(!$model->isActive())
		{
			$row->addClass('deactivated');
		}
		return $row;
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns() : void
	{
		
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$column->setWidthAsPixels(300);
		$this->addTCListColumn($column);
		
//		$column = new TCv_ListColumn('path');
//		$column->setTitle('Path');
//		$column->setContentUsingListMethod('pathColumn');
//		$column->setWidthAsPixels(200);
//		$column->setRightPadding(8);
//		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('navigation');
		$column->setTitle('Navigation');
		$column->setContentUsingListMethod('visibilityColumn');
		$column->setWidthAsPixels(160);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('status');
		$column->setTitle('Status');
		$column->setContentUsingListMethod('statusColumn');
		$column->setWidthAsPixels(160);
	//	$column->setAlignment('center');
		$column->setRightPadding(8);
		$this->addTCListColumn($column);
		
	
		
		$column = new TCv_ListColumn('requirements');
		$column->setTitle('Requirements');
		$column->setContentUsingListMethod('requirementsColumn');
		$column->setWidthAsPixels(160);
		//$column->setAlignment('center');
		$column->setRightPadding(8);
		$this->addTCListColumn($column);
		
		$edit_button = $this->controlButtonColumnWithListMethod('openIconColumn');
		$this->addTCListColumn($edit_button);
		
		
		$this->addDefaultEditIconColumn();
		$this->addDefaultDeleteIconColumn();
	}
	
	/**
	 * Creates and returns the link for the menu item that is found in the title column. This is sometimes extended
	 * or tweaked and needs to be accessible to programmers outside of just overwriting the entire title column.
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_Link
	 */
	protected function titleLink(TMm_PagesMenuItem $menu_item) : TCv_Link
	{
		$edit_link = new TCv_Link();
		$edit_link->addClass('title_link');
		if($menu_item->userCanEdit() &&  $menu_item->isContentManaged())
		{
			$edit_link->setURL('/admin/pages/do/page-builder/'.$menu_item->id());
			$edit_link->setTitle($menu_item->title());
		}
		else
		{
			$edit_link->setTag('span');
			$edit_link->addClass('no_access');
		}
		$edit_link->addClass('title_link');
		$edit_link->addText($menu_item->title());
		
		return $edit_link;
		
	}
		
		/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function titleColumn(TMm_PagesMenuItem $menu_item) : TCv_View
	{
		$container = new TCv_View();
		$container->addClass('title_container');
		
		if($this->user_can_rearrange)
		{
			
			$container->attachView($this->rearrangeControlsForMenuItem($menu_item));
		}
		
		$inside = new TCv_View();
		$inside->addClass('inside');
		
		// Main title link
		$inside->attachView($this->titleLink($menu_item));
		
		
		if($menu_item->contentEntry() != 'manage')
		{
			$content_entry_details = new TCv_View();
			$content_entry_details->setTag('span');
			$content_entry_details->addClass('content_entry_details');
			$content_entry_details->addText('<i class="fa fa-share"></i> ');
			if($redirect = $menu_item->redirect())
			{
				$content_entry_details->addText('Redirects to '.$redirect);
			}
			elseif($menu_item->contentEntry() == 'class_method')
			{
				$content_entry_details->addText('<em>'.$menu_item->redirectClassNameFriendly().
				                                ' <i class="fa fa-angle-right"></i> '.$menu_item->redirectClassMethod().'</em>');
			}
			$inside->attachView($content_entry_details);
		}
		
		elseif($menu_item->isModelList())
		{
			$content_entry_details = new TCv_View();
			$content_entry_details->setTag('span');
			$content_entry_details->addClass('model_list_navigation_note');
			$content_entry_details->addText('<i class="fa fa-list"></i> ');
			
			/** @var TCm_ModelList $model_list */
			$model_list = ($menu_item->modelListClassName())::init();
			$model_name_plural = call_user_func(array($model_list->modelClassName(), 'modelTitlePlural'));
			
			$content_entry_details->addText('Navigation shows list of '.$model_name_plural);
			
			$inside->attachView($content_entry_details);
		}
		
		$inside->attachView($this->pathComponent($menu_item));
		
		$container->attachView($inside);
		
		
		return $container;
		
	}
	
	protected function rearrangeControlsForMenuItem(TMm_PagesMenuItem $menu_item) : TCv_View
	{
		$rearrange_controls = new TCv_View();
		$rearrange_controls->addClass('rearrange');
		
		// MOVE UP BUTTON
		
		$move_up = new TCv_Link();
		$move_up->setURL('#');
		$move_up->addClass('move_up');
		$move_up->addClass('rearrange_button');

		$icon_view = new TSv_FontAwesomeSymbol('sort-up', '');
		$move_up->attachView($icon_view);
		
		$rearrange_controls->attachView($move_up);
		
		$move_down = new TCv_Link();
		$move_down->setURL('#');
		$move_down->addClass('move_down');
		$move_down->addClass('rearrange_button');
		//}
		$icon_view = new TSv_FontAwesomeSymbol('sort-down', '');
		$move_down->attachView($icon_view);
		
		$rearrange_controls->attachView($move_down);
		
		
		return $rearrange_controls;
		
	}
	
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function pathComponent(TMm_PagesMenuItem $menu_item) : TCv_View
	{
//		if($menu_item->isModelList())
//		{
			$folder_text = new TCv_View();
			$folder_text->setTag('span');
			$folder_text->addClass('model_list_folder_block');
		//}
//		else
//		{
//			$folder_text = new TCv_Link();
//			$folder_text->setURL($menu_item->pathToFolder());
//			$folder_text->openInNewWindow();
//		}
		
		$folder_text->addClass('folder_text');
		$folder_text->addText($menu_item->pathToFolder());
		$menu_class_name =$menu_item->modelClassName();
		if($menu_class_name != '')
		{
			$folder_text->setTag('span');
			$folder_text->addClass('model_list_folder_block');
			
			if(class_exists($menu_class_name))
			{
				$primary_id_column = $menu_class_name::$table_id_column;
				$primary_id_column = str_replace('_',' ', $primary_id_column);
				$folder_text->addText('<em class="model_name">'.$primary_id_column.'</em>/');
				//$folder_text->addDataValue('class_name', $primary_id_column);
			}
			
		}
		if($menu_item->isModelList())
		{
			// Add the <model_id> to the end of the URL
			// Only bother if a model isn't set, otherwise already indicated
			if($menu_class_name == '')
			{
				$folder_text->addText('<em>&lt;model_id&gt;</em>/');
			}
			
			/** @var TCm_ModelList $model_list */
			$model_list = ($menu_item->modelListClassName())::init();
			$model_name_plural = call_user_func(array($model_list->modelClassName(), 'modelTitlePlural'));
			
			$folder_text->addText('<br />Auto-generated list of '.$model_name_plural);
		}
		
		return $folder_text;
	}
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function visibilityColumn(TMm_PagesMenuItem $menu_item) : TCv_View
	{
		$visibility_block = new TCv_View();
		$visibility_block->addClass('visibility_block');
		$visibility_block->setTag('span');
		
		
		if($menu_item->isModelList())
		{
			$visibility_block->addText('<i class="fa-list fa-fw"></i>Model list');
		}
		else // Find navigation types
		{
			$navigation_types = $menu_item->navigationTypes();
			
			if(count($navigation_types) == 0)
			{
				$item = new TCv_View();
				$item->addClass('nav_type empty');
				$item->addText('––');
				$visibility_block->attachView($item);
			}
			else
			{
				foreach($navigation_types as $type)
				{
					$item = new TCv_View();
					$item->addClass('nav_type');
					$item->addText($type->title());
					$visibility_block->attachView($item);
				}
			}
			
		}
		
		
	
		
		return $visibility_block;
	}
	
	
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function statusColumn(TMm_PagesMenuItem $menu_item)
	{
		$container = new TCv_View();
		$container->addClass('status_container');
		
	//	$view->addClass('list_control_button');
		$status_box = new TCv_View();
		$status_box->addClass('status_indicator');
		if($menu_item->isActive())
		{
			$status_box->addClass('active');
			$status_box->addText('<i class="fas fa-check"></i>');
			$status_box->addText('<span>Active</span>');
		}
		else
		{
			$status_box->addClass('deactivated');
			$status_box->addText('<i class="fas fa-times"></i>');
			$status_box->addText('<span>Deactivated</span>');
		}
		$container->attachView($status_box);
		
		if(TC_getConfig('use_pages_version_publishing'))
		{
			if($menu_item->hasUnpublishedContent())
			{
				$unpublished = new TCv_View();
				$unpublished->addClass('status_indicator');
				$unpublished->addClass('unpublished_changes');
				$unpublished->addText('<i class="fas fa-exclamation-triangle"></i>');
				$unpublished->addText('<span>Unpublished changes</span>');
				$container->attachView($unpublished);
				
			}
			
		}
		
		return $container;
		
	}
	
	
	/**
	 * Returns the column value for the provided model
	 *
	 * @param TMm_PagesMenuItem $menu_item
	 * @return TCv_View
	 */
	public function requirementsColumn(TMm_PagesMenuItem $menu_item)
	{
		$view = new TCv_View();
		$view->addClass('requirements_list');
		
		// LOGIN
		if(TC_getModuleConfig('login', 'show_authentication'))
		{
			if($menu_item->requiresAuthentication())
			{
				$requirement = new TCv_View();
				$requirement->addClass('required_class');
				
				$requirement->addText('<i class="fas fa-lock"></i>');
				$requirement->addText('User login');
//				$requirements [] = $menu_class_name::$model_title;
				$view->attachView($requirement);
			}
			
		}
		
		// MODEL CLASS
		$menu_class_name =$menu_item->modelClassName();
		
		if($menu_class_name != '')
		{
			if(class_exists($menu_class_name))
			{
				$requirement = new TCv_View();
				$requirement->addClass('required_class');
				$requirement->addClass('class_cond');
				$icon = $menu_class_name::moduleForClassName()->iconCode();
				$requirement->addText('<i class="fas '.$icon.'"></i>');
				$requirement->addText('<span><strong>'.$menu_class_name::$model_title.'</strong> required</span>');
//				$requirements [] = $menu_class_name::$model_title;
				$view->attachView($requirement);
			}
			
		}
		
		// VISIBILITY CONDITIONS
		$condition = trim($menu_item->visibilityCondition());
		if($condition != '')
		{
			$condition = str_replace(['{{','}}',''], '', $condition);
			$condition = str_replace(['TMm_','TSm_','TCm_'], '', $condition);
			$condition = str_replace(['.',':'],' ', $condition);
			$requirement = new TCv_View();
			$requirement->addClass('required_class');
			$requirement->addClass('vis_cond');
			$requirement->addText('<i class="fas fa-question"></i>');
			$requirement->addText('<span>'.$condition.'</span>');
//
			$view->attachView($requirement);
		}
		
		
		return $view;
	}
	
	public function openIconColumn(TMm_PagesMenuItem $menu_item)
	{
		// Can't open deactivated menus
		if(!$menu_item->isActive())
		{
			return;
		}
		
		$menu_class_name =$menu_item->modelClassName();
		if($menu_class_name != '')
		{
			return;
		}
		
		
		$link = new TCv_Link();
		$link->setURL($menu_item->pageViewURLPath());
		$link->setIconClassName('fa-external-link');
		$link->addClass('list_control_button');
		
		return $link;
		
	}
	
	
	public function defineFilters()
	{
		parent::defineFilters();
		
		
		$field = new TCv_FormItem_Select('navigation_type','Navigation type');
		$field->addOption('','All navigation types');
		
		$navigation_types = TMm_PagesNavigationTypeList::init()->models();
		foreach($navigation_types as $type)
		{
			$field->addOption($type->id(), $type->title());
		}
		$field->addOption('none','No navigation type');
		
		$this->addFilterFormItem($field);
		
		$field = new TCv_FormItem_Select('status','Status');
		$field->addOption('','All pages');
		
		$field->addOption('1', 'Active pages only');
		$field->addOption('0', 'Deactivated pages only');
		if(TC_getConfig('use_pages_version_publishing'))
		{
			$field->addOption('unpublished', 'Unpublished changes only');
			
		}
		
		$this->addFilterFormItem($field);
		
	}
}