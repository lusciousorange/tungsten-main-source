class TMv_PagesMenuItemManagerList extends TCv_SearchableModelList {

	constructor(element, values = {}) {
		super(element, values);

		// LISTEN FOR REARRANGE
		element.addEventListener('click', this.rearrangeHandler.bind(this));

		// REARRANGE BUTTON Track rearrange toggle, might be null so ?.
		document.querySelector("a.url-target-rearrange-menus")?.addEventListener('click',
			this.toggleRearrange.bind(this) );

	}

	/**
	 * Handles the toggling of the rearrange buttons
	 * @param event
	 */
	toggleRearrange(event) {
		event.preventDefault();

		this.element.classList.toggle('rearranging');
	}


	rearrangeHandler(event) {

		let button = event.target.closest('.rearrange_button');
		if(button)
		{
			let row = button.closest('tr');
			let menu_id = row.getAttribute('data-row-id');
			let this_level = row.getAttribute('data-level');
			let direction = button.classList.contains('move_up') ? 'up' : 'down';


			if(direction == 'up')
			{
				this.moveRowUp(row);
			}
			else // DOWN
			{
				this.moveRowDown(row);
			}

		}


	}

	/**
	 * Moves a row up
	 * @param row
	 */
	async moveRowUp(row) {

		// Add the loading indicator
		this.addTungstenLoading();

		let row_id = parseInt(row.getAttribute('data-row-id'));
		let previous_row = this.findPreviousSiblingForRow(row);

	//	console.log('previous_row', previous_row);
	//	return;
		// We found the next row to move down
		if(previous_row)
		{
			// Send the request first
			let response = await this.sendRequest('POST', "/admin/pages/do/menu-move-up/" + row_id, {});
		//	console.log(response);

			// If we have a valid response, do some updates
			if(('updated' in response) && response.updated === true)
			{
				// Perform the swap
				this.swapRows(previous_row, row);

				// Update the display orders
				for(const menu_id in response.display_orders)
				{
					this.updateDisplayOrderValueForMenuId(menu_id, response.display_orders[menu_id]);
				}


			}
		}

		// Remove the loading no matter what
		this.removeTungstenLoading();
	}

	/**
	 * Moves a row down
	 * @param row
	 */
	async moveRowDown(row) {

		// Add the loading indicator
		this.addTungstenLoading();

		let row_id = row.getAttribute('data-row-id');
		let next_row = this.findNextSiblingForRow(row);

		// We found the next row to move down
		if(next_row)
		{
			// Send the request first
			let response = await this.sendRequest('POST', "/admin/pages/do/menu-move-down/" + row_id, {});
			//console.log(response);

			// If we have a valid response, do some updates
			if(('updated' in response) && response.updated === true)
			{
				// Perform the swap
				this.swapRows(row, next_row);

				// Update the display orders
				for(const menu_id in response.display_orders)
				{
					this.updateDisplayOrderValueForMenuId(menu_id, response.display_orders[menu_id]);
				}


			}
		}

		// Remove the loading no matter what
		this.removeTungstenLoading();

	}

	/**
	 * Loops through, trying to find the next sibling for this row which is of the same parent, but the order is greater than this one.
	 * @param row
	 */
	findNextSiblingForRow(row) {
		let parent_id = parseInt(row.getAttribute('data-parent'));
		let order = parseInt(row.getAttribute('data-order' ));
		let next_row = null;

		let siblings = this.element.querySelectorAll('tr[data-parent="'+parent_id+'"]');
		siblings.forEach((el) => {
			const el_order = parseInt(el.getAttribute('data-order'));

			if(next_row == null && el_order > order)
			{
				next_row = el;
			}
		});
		return next_row;
	}

	/**
	 * Loops through, trying to find the next sibling for this row which is of the same parent, but the order is greater than this one.
	 * @param row
	 */
	findPreviousSiblingForRow(row) {
		let parent_id = parseInt(row.getAttribute('data-parent'));
		let order = parseInt(row.getAttribute('data-order'));
		let previous_row = null;

		// Go through all the siblings and so long as their order is less than the row, keep tracking as the current one
		let siblings = this.element.querySelectorAll('tr[data-parent="'+parent_id+'"]');
		siblings.forEach((el) => {
			const el_order = parseInt(el.getAttribute('data-order'));

			if(el_order < order)
			{
				previous_row = el;
			}
		});
		return previous_row;
	}

	/**
	 * Takes a row, finds ALL of its children and puts them in a DIV for the purpose of swapping them for rearranging.
	 * The animation assumes they are all one unit, so we need to box them up for transport, then after it's done, unbox them.
	 * @param row
	 */
	groupRowForSwap(row) {
		// Create the container
		let container = document.createElement('div');
		container.classList.add('animation_group');

		container.setAttribute('data-row-id', row.getAttribute('data-row-id'));
		//let parent_id = parseInt(row.getAttribute('data-parent'));
		//let order = parseInt(row.getAttribute('data-order' ));
		let level = parseInt(row.getAttribute('data-level' ));

		// Find EVERY item that comes after the row, and move them into the container
		// If the level is greater than the row, it's a child somewhere
		let next_row = row.nextElementSibling;
		while(next_row && next_row.getAttribute('data-level') > level)
		{
			// Grab the next one before it moves
			let temp_next_row = next_row.nextElementSibling;
			// Move it inside
			container.append(next_row);

			// Assign the next row to continue the loop
			next_row = temp_next_row;
		}

		// Everything except the main row in now in the box
		// Attach the box, before the row itself
		row.before(container);

		// move the row inside the container, at the start
		container.prepend(row);

		return container;

	}

	/**
	 * Reverses the grouping of rows into a container, putting them all back to where they were
	 * @param container
	 */
	ungroupRowForSwap(container) {

		// Loop through all the rows, and attach them BEFORE the container.
		// it will re-add them in the order they were found
		container.querySelectorAll('tr').forEach(child_row => {
			container.before(child_row);
		});

		// Delete the container
		container.remove();
	}


	/**
	 * Takes two rows, groups them with ALL their descendants, swaps them, then puts them back.
	 * @param row
	 * @param next_row
	 */
	swapRows(row, next_row) {
		let container_1 = this.groupRowForSwap(row);
		let container_2 = this.groupRowForSwap(next_row);

		// Insert the 1st container AFTER the 2nd container
		container_2.after(container_1);

		// Undo the containers
		this.ungroupRowForSwap(container_1);
		this.ungroupRowForSwap(container_2);

		this.cleanUpOrderClasses();
	}


	/**
	 * Updates the tracked display order for a row, based on a menu ID
	 * @param menu_id
	 * @param new_display_order
	 */
	updateDisplayOrderValueForMenuId(menu_id, new_display_order) {
		let menu_row = this.element.querySelector('tr[data-row-id="'+menu_id+'"]');
		if(menu_row)
		{
			menu_row.setAttribute('data-order', new_display_order);
		}
	}

	/**
	 * Function to loop through the rows and clean up any class markings on them
	 */
	cleanUpOrderClasses() {
		let previous_row = null;
		let previous_level = 0;
		this.element.querySelectorAll('tr.TCv_ListRow').forEach(row => {

			// Remove the class
			row.classList.remove('last_in_order');
			let this_level = parseInt(row.getAttribute('data-level'));
			// This row is a step back, need to mark the previous one
			if(this_level < previous_level)
			{
				previous_row.classList.add('last_in_order');
			}
			previous_level = this_level;
			previous_row = row;


		});

		// last one gets it too
		previous_row?.classList.add('last_in_order');
	}


	/**
	 * A hook method to allow programmers to deal with the response coming back if necessary
	 * @param {Object} response
	 */
	hook_processUpdateResponse(response) {
		// Extend and override this function if you need to process data from the response in an extended class.
		this.cleanUpOrderClasses();
	}


	////////////////////////////////////////////////
	//
	// REQUEST
	//
	////////////////////////////////////////////////



	/**
	 * Sends a request to the server and returns a promise. Used in async methods to send requests
	 * @param {string} method
	 * @param {string} url
	 * @param {object} data
	 * @return {Promise<unknown>}
	 */
	sendRequest(method, url, data) {

		method = method.toUpperCase();

		let config = {
			method : method,
			headers : {}
		};

		if(method === 'POST')
		{

			config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			config.body = new URLSearchParams(data);
		}

		return fetch(url, config)
			.then(response =>
			{
				if(!response.ok)
				{
					throw new Error(`HTTP error: ${response.status}`);
				}

				return response.json();

			}).then(response => {

				// Pass it along the chain
				return response;


			})
			.catch(exception =>
			{
				console.error(exception);
			})

			.finally(() =>
			{


			});


	}
}