<?php
/**
 * Class TMv_PagesTemplateSettingForm
 * The form to create/edit a pages template setting form
 */
class TMv_PagesTemplateSettingForm extends TCv_FormWithModel
{
	/**
	 * TMv_PagesTemplateSettingForm constructor.
	 * @param string|TCm_Model $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		$this->setSuccessURL('template-setting-list');
	}

	/**
	 *
	 */
	public function configureFormElements()
	{
		$title = new TCv_FormItem_TextField('title', 'Title');
		$title->setIsRequired();
		$title->setHelpText('A title for this setting that helps you identify what it means. ');
		$this->attachView($title);
		
		$title = new TCv_FormItem_TextField('code', 'Code');
		$title->setHelpText('The code for this setting. This is how a developer can reference this value for later use.');
		$title->setIsAlphaNumericUnderscore();
		$title->setIsRequired();
		$this->attachView($title);


		if($this->isEditor() && $this->model()->useHTMLEditor())
		{
			$value = new TCv_FormItem_HTMLEditor('value', 'Value');
			$value->useBasicEditor();
			$this->attachView($value);

		}
		else
		{
			$value = new TCv_FormItem_TextBox('value', 'Value');
			$this->attachView($value);

		}
		$field = new TCv_FormItem_Select('use_html_editor', 'Use HTML editor');
		$field->setHelpText('Indicate if this value should use an HTML editor instead of a regular simple text editor. Changing this setting will only take affect after you update the form, then reload it.');
		$field->addOption('0','NO - Use simple text');
		$field->addOption('1','YES - Use the HTML editor');
		$this->attachView($field);



	}
	
}