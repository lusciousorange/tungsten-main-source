<?php
/**
 * Class TMv_Theme_Plain
 *
 * An example class for a plain theme. This class can easily be duplicated and changed for each website you build.
 */
class TMv_Theme_Plain extends TMv_PagesTheme
{
	/** @var string $theme_name The name of the theme that appears in the Pages setting selector */
	public static $theme_name = 'Plain';

	/** @var string $page_content_header_class_name The name of the class that is used for the content header. The
	 * content header is the view that shows the title, photo, breadcrumbs and description of the page. You can
	 * extend that class and change the setting here to control the layout and content shown.  */
	public static $page_content_header_class_name = 'TMv_PageContentHeader';

	/**
	 * TMv_Theme_Plain constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_Theme_Plain');

		// Include a Google Font
		// You can also use the TCv_Website::addMetaTag() method for a more general usage
		$this->addCSSFile('Overpass Font', "https://fonts.googleapis.com/css?family=Overpass:300,400,600");
		
		//$this->enablePerformanceTracking();
	}

	/**
	 * The site's header
	 *
	 * @return TCv_View
	 */
	public function siteHeader()
	{
		// Call the parent's siteHeader() view to get the starting point TCv_View
		$header = parent::siteHeader();
		
		// Insert a content width container which is styled to enforce a maximum 1000 pixel wide container
		// that scales down for responsive layouts
		$container = new TCv_View();
		$container->addClass('content_width_container');

		// ---- TOP BAR -----

		$top_bar = new TCv_View('header_top_bar');

		// Mobile Menu toggle button
		$menu_toggle_link = TCv_ToggleTargetLink::init('mobile_menu_toggle');
		$menu_toggle_link->setIconClassName('fa-bars'); // bars/hamburger icon
		$menu_toggle_link->addClassToggle('body', 'mobile_navigation_showing'); // toggles the class on the body tag
		$menu_toggle_link->setUseStandardToggle();
		//$menu_toggle_link->addText('Menus'); // Text can be added as well
		$container->attachView($menu_toggle_link);


		$page_title = TC_getModuleConfig('pages', 'website_title');

		$title = new TCv_View('page_title');
		$title->setTag('h2');
		$title->addText( $page_title );
		$top_bar ->attachView($title);


		// Add the top bar to the container
		$container->attachView($top_bar);

		// ---- NAVIGATION -----

		// Add the main level of navigation
		// We include all the levels so that we can show a nice list for
		// responsive layouts. usually we hide anything that isn't the first level in CSS
		$menus = TMv_PageMenus::init('main_menus');
		$menus->setStartingLevel(1);
		$menus->addNavigationType(new TMm_PagesNavigationType(1));
		$container->attachView($menus);


		$header->attachView($container);
			
		return $header;
	}
	
	/**
	 * The site's footer view.
	 * @return TCv_View
	 */
	public function siteFooter()
	{
		// Call the parent's siteFooter() view to get the starting point TCv_View
		$footer = parent::siteFooter();
		
			$inside = new TCv_View();
			$inside->addClass('content_width_container');
			
				// a paragraph added to the footer
				// hard-coded an anchor, it works but it's not preferred
				// try using TCv_Link instead
				$copyright_notice = new TCv_View();
				$copyright_notice->setTag('p');
				$copyright_notice->addText('&copy; '.date('Y').' – Tungsten is created by <a href="http://lusciousorange.com">Luscious Orange</a> – ');
				$copyright_notice->attachView($this->showConsoleLink());
				$inside->attachView($copyright_notice);
				
				
			
				
			$footer->attachView($inside);
		return $footer;
	}
	
	/**
	 * Renders the website with the header, page content and footer.
	 */
	public function render()
	{
		$this->attachView($this->siteHeader());

		/** @var TMv_PageContentHeader $page_content_header */
		$page_content_header = (static::$page_content_header_class_name)::init($this->viewed_menu);
		$this->attachView($page_content_header);


		$this->attachView($this->pagesContentView());
		
		$this->attachView($this->siteFooter());
		
	}
	
}
?>