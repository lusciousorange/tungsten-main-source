<?php
/**
 * Class TMv_ContentLayout_3Column_To_2_1
 *
 * A 2 column layout with the same spacing as the 3-column layout.
 * The first and second columns are combined into a single column.
 */
class TMv_ContentLayout_3Column_To_2_1 extends TMv_ContentLayout 
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 66% spans one gap
	 * Column 2 : 33% spans no gaps
	 */
	protected static array $column_percentages = [[66.66,1], [33.33,0]];
	protected static ?array $column_widths = null;
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		$this->setPreviewGroupingColor('25, 112, 158');

		$this->addClassCSSFile('TMv_ContentLayout_3Column_To_2_1');
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 2;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '2 Columns - 66% | 33% (3-column spacing) '; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{
		return 'Two column layout where the first column is 66% wide and the second column is 33% wide.';
	}

}

?>