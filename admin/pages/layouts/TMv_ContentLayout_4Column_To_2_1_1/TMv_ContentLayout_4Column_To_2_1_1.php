<?php
/**
 * Class TMv_ContentLayout_4Column_To_2_1_1
 *
 * A 3 column layout with the same spacing as the 4-column layout.
 * The first and second columns are combined into a single column.
 */
class TMv_ContentLayout_4Column_To_2_1_1 extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	
	
	/**
	 * Column 1 : 50% spans 1 gap
	 * Column 2 : 25% spans no gaps
	 * Column 3 : 25% spans no gaps
	 */
	protected static array $column_percentages = [[50,1], [25,0], [25,0]];
	protected static ?array $column_widths = null;
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		$column_3 = new TMv_ContentLayoutBlock();
		$column_3->addClass('column_3');
		$this->defineContentBlock($column_3);
		
		$this->setPreviewGroupingColor('13, 199, 128');
		
		$this->addClassCSSFile('TMv_ContentLayout_4Column_To_2_1_1');
		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 3;
	}
	
	
	
	/**
	 * Generates the CSS values for the margins for this column and adds them to the auto-generated CSS for this layout
	 * @param string|bool $css_prefix A prefix for classes that are used to identify if this is a subtype for css
	 * classes
	 * @return array
	 */
	public function generateMarginCSS($css_prefix = false)
	{
		$css_lines = parent::generateMarginCSS($css_prefix);
		// Deal with columns 2 and 3
		if($this->responsive_collapse == 2)
		{
			$identifier = "#" . $this->attributeID();

			$css = "@media only screen and (max-width : 750px) {";
			$css .= "\n\t".$identifier." .content_width_container .column_1 {".$this->singleColumnCSS()."}";

			$css .= "\n\t".$identifier." .content_width_container .column_2 {";
			$css .= "\n\t".$this->calculateCSSSnippetForMarginValues(50, false, true, $this->margin_percentage);
			$css .= "\n\t".'}';

			$css .= "\n\t".$identifier." .content_width_container .column_3 {";
			$css .= "\n\t".$this->calculateCSSSnippetForMarginValues(50, true, false, $this->margin_percentage);
			$css .= "\n\t".'}';
			$css .= '}';
			$css_lines[$identifier] = $css;

		}
		return $css_lines;
	}


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();

		$form_items['responsive_collapse']->addOption('2','PARTIAL - Column 1 collapses to single column. Columns 2 and 3 stay together');
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '3 Columns - 50% | 25% | 25% (4-column spacing) '; }
	public static function pageContent_IsAContainer() : bool { return true; }

	public static function pageContent_ViewDescription() : string
	{
		return 'A three column layout where the first column is 50% wide, the second column is 25% wide and the third column is 25% wide.';
	}
}

?>