<?php
/**
 * Class TMv_ContentLayout_1Column
 *
 * A single column content layout
 */
class TMv_ContentLayout_1Column extends TMv_ContentLayout 
{
	use TMt_PagesContentView;
	
	protected static array $column_percentages = [[100,0]];
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		$this->setPreviewGroupingColor('60,60,60');

		$this->addClassCSSFile('TMv_ContentLayout_1Column');

	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 1;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '1 Column'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'One column layout.';
	}
	
}

?>