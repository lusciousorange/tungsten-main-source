<?php
/**
 * Class TMv_ContentLayout_2Column
 *
 * A content layout with 2 columns of the same width
 */
class TMv_ContentLayout_2Column extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 50% spans no gaps
	 * Column 2 : 50% spans no gaps
	 */
	protected static array $column_percentages = [[50,0], [50,0]];
	protected static ?array $column_widths = null;
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);

		$this->setPreviewGroupingColor('0, 0, 90');

		$this->addClassCSSFile('TMv_ContentLayout_2Column');

	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 2;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '2 Columns - 50% | 50%'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'Two column layout with each column is 50% wide.';
	}

}

?>