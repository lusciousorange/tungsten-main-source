<?php
/**
 * Class TMv_ContentLayout_Section
 *
 * A layout section that contains multiple rows.
 */
class TMv_ContentLayout_Section extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	protected static $num_rows = 5;
	protected $current_row = 1;
	protected $maximum_columns = 5;
	
	protected $override_padding_top = '';
	protected $override_padding_bottom = '';
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->setPreviewGroupingColor('255,25,75');
		
		$this->setTag('section');
		
		$this->content_width_container = false;

		// Apply "basic" style to any that weren't extended
		if(get_called_class() == 'TMv_ContentLayout_Section')
		{
			$this->addClass('basic_style');
		}
		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 0;
	}
	
	/**
	 * Sets the maximum number of columns permitted in this layout
	 * @param int $max
	 */
	public function setMaxColumns($max)
	{
		$this->maximum_columns = $max;
		
	}
	
	
	
	/**
	 * Returns the maximum number of rows
	 * @return int
	 */
	public static function maxRows()
	{
		return static::$num_rows;
	}
	
	/**
	 * Returns the HTML for this view
	 * @return string
	 */
	public function html()
	{
		$this->addDataValue('max-cols',$this->maximum_columns);
		
		// Deal with override padding that can be set for sections
		$css = '';
		if($this->override_padding_bottom != '')
		{
			$css .= '#'.$this->attributeID().' { padding-bottom:'.$this->override_padding_bottom.'px;}';
			
		}
		if($this->override_padding_top != '')
		{
			$css .= '#'.$this->attributeID().' { padding-top:'.$this->override_padding_top.'px;}';
			
		}
		$this->addCSSLine($this->attributeID().'override_padding', $css);
		
		return parent::html();
	}
	
	//////////////////////////////////////////////////////
	//
	// BACKGROUND IMAGES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if this section allows background settings to be shown
	 * @return bool
	 */
	public static function allowsBackgroundSettings() : bool
	{
		return TC_getConfig('page_builder','section_background_images');
	}
	
	/**
	 * Extends the function related to background images to disable them if the layout style doesn't permit them.
	 * @return ?TCv_Image
	 */
	public function backgroundImageView() : ?TCv_Image
	{
		if(static::allowsBackgroundSettings())
		{
			return parent::backgroundImageView();
		}
		return null;
	}
	
	//////////////////////////////////////////////////////
	//
	// OUTPUT
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Sets this style to be a "dark" version which provides a common formatting css style that can be used to style
	 * all "dark" styles on the website. This method should be called from the constructor of any layout style that
	 * needs to invert text etc.
	 */
	protected function setAsDarkStyle()
	{
		$this->addClass('dark');
	}
	
	/**
	 * Override attach view method to attach to the inside content width container
	 *
	 * @param bool|TCv_View $view The view that is being attached
	 * @param bool $prepend (Optional) Default false. Indicates if this view should be prepended before the other views.
	 */
	public function attachView($view, $prepend = false)
	{
		if($view instanceof TMv_ContentLayout)
		{
			$view->addClass('row_'.$this->current_row);
			
			// If we're not in a manual layout scenario, then we need to pass that onto the views we're attaching
			if(!$this->is_manual_layout)
			{
				$view->setAsPagesLayout();
			}
			
			$this->current_row++;
		}
		
		$this->attachViewToRoot($view, $prepend);
	
		
	}



	/**
	 * Sets the page content item for this view. Extends the functionality to process the content item
	 * @param TMm_PagesContent $content_item
	 */
	public function setPagesContentItem($content_item) : void
	{
		parent::setPagesContentItem($content_item);
		// Deal with any processing of the content item being passed in
	}


	/**
	 * Overrides the preview method to deal with not including the columns
	 *
	 */
	public function preview()
	{
		$preview = parent::preview();
		$preview->detachAllViews();
		$preview->addText('<div class="full_width_title numbering">'.static::pageContent_ViewTitle().'</div>');
		return $preview;
	}


	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		
		// REMOVE A FEW THAT DON'T BELONG ANYMORE
		unset($form_items['column_format']);
		unset($form_items['responsive_collapse']);
		unset($form_items['responsive_flip_order']);
		unset($form_items['equal_height_columns']);
		unset($form_items['extend_full_width']);
		
		$content_model = $this->contentItem();
		
		if($content_model && $content_model->isLayoutSection())
		{
			//$layout_list  = TMm_PageLayoutList::init();
			//$section_styles = $layout_list->pageSections();
			
//			// Only bother if we actually have one
//			if(count($section_styles) > 1)
//			{
//				$field = new TCv_FormItem_Select('section_style', 'Section Style');
//				$field->setSaveToDatabase(false);
//				$field->setDefaultValue($content_model->viewClass());
//
//				foreach($section_styles as $class_name => $path)
//				{
//					/** @var string|TMv_ContentLayout_Section $class_name */
//
//					$field->addOption($class_name, $class_name::pageContent_ViewTitle());
//
//					// Track if the layout uses backgrounds to show/hide those fields
//					$field->addOptionAttribute($class_name,
//					                           'data-use-backgrounds',
//					                           $class_name::allowsBackgroundSettings() ? '1' : '0');
//
//				}
//
//				// Put the style field first
//				array_unshift($form_items, $field);
//			}
			
			if(TC_getConfig('page_builder','allow_custom_spacing'))
			{
				
				$heading = new TCv_FormItem_Heading('advanced_', 'Custom spacing');
				$heading->addHelpText("These settings should only be used in specific cases to manually adjust the padding for this section. The default values should be blank which means it will use the normal styling spaces. ");
				$form_items['advanced_heading'] = $heading;
				
				
				$field = new TCv_FormItem_TextField('override_padding_top', 'Padding top override');
				$field->setHelpText('Enter an integer value to override the padding top.');
				$form_items['override_padding_top'] = $field;
				
				$field = new TCv_FormItem_TextField('override_padding_bottom', 'Padding bottom override');
				$field->setHelpText('Enter an integer value to override the padding bottom.');
				$form_items['override_padding_bottom'] = $field;
			}
			
			if($filter_options = TC_getConfig('page_builder','background_image_filtering'))
			{
				if(is_array($filter_options) && count($filter_options) > 0)
				{
					$field = new TCv_FormItem_Select('filters', 'Background Filters');
					$field->addCSSClassForRow('background_field');
					$field->setUseMultiple(',');
					$field->useFiltering();
					foreach($filter_options as $name => $settings)
					{
						$field->addOption($name, $settings['name']);
					}
					$form_items['background_filters'] = $field;
					
				}
				
			}
			
		}
		
		

		return $form_items;
	}
	
	/**
	 * Hook method for the form processing that handles the move to a new page
	 
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return string[] Returns an array of values that should be stored for the page content
	 */
	public static function pageContent_FormProcessing($form_processor) : array
	{
		$variable_values = array();
		
		
		
		// Detect if being moved
		if($form_processor->fieldIsSet('move_to_page'))
		{
			$new_menu_id = $form_processor->formValue('move_to_page');
			
			// Only bother if we selected a menu
			if($new_menu_id != '')
			{
				/** @var TMm_PagesContent $page_content */
				$page_content = $form_processor->model();
				
				// $current_menu = $page_content->renderer();
				
				// Change to menu ID
				$page_content->changeMenuID($new_menu_id);
				
			}
			
			
		}
		
		// Returns empty array
		return $variable_values;
		
	}

	public static function pageContent_ViewTitle() : string { return 'Basic theme style'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	public static function pageContent_IsAddable() : bool { return false; } // Deprecated since it's a setting on
	// Regular

	public static function pageContent_ViewDescription() : string
	{
		return "A simple section with no particular style.";
	}

}

?>