<?php
/**
 * Class TMv_ContentLayout_5Column
 *
 * A 5 column layout
 */
class TMv_ContentLayout_5Column extends TMv_ContentLayout 
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 20% spans no gaps
	 * Column 2 : 20% spans no gaps
	 * Column 3 : 20% spans no gaps
	 * Column 4 : 20% spans no gaps
	 * Column 5 : 20% spans no gaps
	 */
	protected static array $column_percentages = [[20,0], [20,0], [20,0], [20,0], [20,0] ];
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		$column_3 = new TMv_ContentLayoutBlock();
		$column_3->addClass('column_3');
		$this->defineContentBlock($column_3);
		
		$column_4 = new TMv_ContentLayoutBlock();
		$column_4->addClass('column_4');
		$this->defineContentBlock($column_4);
		
		$column_5 = new TMv_ContentLayoutBlock();
		$column_5->addClass('column_5');
		$this->defineContentBlock($column_5);
		
		$this->setPreviewGroupingColor('256, 128, 0');
		
		$this->addClassCSSFile('TMv_ContentLayout_5Column');
		
		
		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 5;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		$form_items['responsive_collapse']->addOption('2','PARTIAL 3-2 - Columns 1, 2, 3 in one row. Columns 4 and 5 in another. ');
		$form_items['responsive_collapse']->addOption('3','PARTIAL 2-3 - Columns 1 and 2 in one row. Columns 3, 4, 5 in another. ');
		$form_items['responsive_collapse']->addOption('4','PARTIAL 2-2-1 - Columns 1 and 2 in one row. Columns 3, 4 in a row. 5 in a third row. ');
		$form_items['responsive_collapse']->addOption('5','PARTIAL 1-2-2 - Columns 1 in a row. Columns 2 and 3 in one row. Columns 4 and 5 in a third row. ');
		
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '5 Columns - 20% | 20% | 20% | 20% | 20%'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{
		return 'Five column layout with each column is 20% of the width.';
	}

}

?>