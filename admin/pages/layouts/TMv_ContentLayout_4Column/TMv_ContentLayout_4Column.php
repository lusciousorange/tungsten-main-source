<?php
/**
 * Class TMv_ContentLayout_4Column
 *
 * A 4 column layout
 */
class TMv_ContentLayout_4Column extends TMv_ContentLayout 
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 25% spans no gaps
	 * Column 2 : 25% spans no gaps
	 * Column 3 : 25% spans no gaps
	 * Column 4 : 25% spans no gaps
	 */
	protected static array $column_percentages = [[25,0], [25,0], [25,0], [25,0]];
	protected static ?array $column_widths = null;
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		$column_3 = new TMv_ContentLayoutBlock();
		$column_3->addClass('column_3');
		$this->defineContentBlock($column_3);
		
		$column_4 = new TMv_ContentLayoutBlock();
		$column_4->addClass('column_4');
		$this->defineContentBlock($column_4);
		
		$this->setPreviewGroupingColor('13, 199, 128');

		$this->setMarginPatternForColumn(1, 25);
		$this->setMarginPatternForColumn(2, 25);
		$this->setMarginPatternForColumn(3, 25);
		$this->setMarginPatternForColumn(4, 25);
		
		$this->addClassCSSFile('TMv_ContentLayout_4Column');

		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 4;
	}
	
	

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		$form_items['responsive_collapse']->addOption('2','2 and 2 - Columns 1 and 2 stay together, as do columns 3 and 4');
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '4 Columns - 25% | 25% | 25% | 25%'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{
		return 'A four column layout where each column is 25% wide.';
	}

}

?>