<?php
/**
 * Class TMv_ContentLayout_3Column
 *
 * A 3-column column content layout
 */
class TMv_ContentLayout_3Column extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 33.33% spans no gaps
	 * Column 2 : 33.33% spans no gaps
	 * Column 3 : 33.33% spans no gaps
	 */
	protected static array $column_percentages = [[33.33,0], [33.33,0], [33.33,0]];
	protected static ?array $column_widths = null;
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		$column_3 = new TMv_ContentLayoutBlock();
		$column_3->addClass('column_3');
		$this->defineContentBlock($column_3);
		
		$this->setPreviewGroupingColor('25, 112, 158');
		
		$this->addClassCSSFile('TMv_ContentLayout_3Column');
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 3;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '3 Columns - 33% | 33% | 33%'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'Three column layout where each column is 33.3% wide.';
	}


}

?>