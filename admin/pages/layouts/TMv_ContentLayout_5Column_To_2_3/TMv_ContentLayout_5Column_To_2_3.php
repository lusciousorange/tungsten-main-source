<?php
/**
 * Class TMv_ContentLayout_5Column_To_2_3
 *
 * A 5 column layout
 */
class TMv_ContentLayout_5Column_To_2_3 extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 40% spans 1 gap
	 * Column 2 : 60% spans 2 gaps
	 */
	protected static array $column_percentages = [[40,1], [60,2]];
	protected static ?array $column_widths = null;
	
	
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		

		$this->setPreviewGroupingColor('256, 128, 0');
		
		$this->addClassCSSFile('TMv_ContentLayout_5Column_To_2_3');
		
		
		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 2;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '5 Columns - 40% | 60% (5-column spacing)'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A two column layout with column one is 40% and the second column is 60%.';
	}

}

?>