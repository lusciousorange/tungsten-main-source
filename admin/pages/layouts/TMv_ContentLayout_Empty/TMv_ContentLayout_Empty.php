<?php
/**
 * Class TMv_ContentLayout_Empty
 *
 * A content layout with no columns and no content width container.
 * This layout will extend to the edges of the page for content.
 */
class TMv_ContentLayout_Empty extends TMv_ContentLayout
{
	use TMt_PagesContentView;

	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$inside = new TMv_ContentLayoutBlock();
		$inside->addClass('inside');
		$this->defineContentBlock($inside);
		$this->setPreviewGroupingColor('60,60,60');
		
		$this->addClassCSSFile('TMv_ContentLayout_Empty');
	}

	/**
	 * Overrides the preview method to deal with not including the columns
	 *
	 */
	public function preview()
	{
		$preview = parent::preview();
		$preview->detachAllViews();
		$preview->addText('<div class="full_width_title numbering">Full Width</div>');
		return $preview;
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 1;
	}
	
	
	
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}

	public static function pageContent_ViewTitle() : string { return 'Full Width – 1 Column'; }
	public static function pageContent_IsAContainer() : bool { return true; }
	public static function pageContent_IsAddable() : bool { return false; } // Deprecated since it's a setting on Regular
	// layouts

	public static function pageContent_ViewDescription() : string
	{
		return "A special layout that doesn't restrict content to the normal container of the website. This is a one column layout that spans the width of the browser window.";
	}

}

?>