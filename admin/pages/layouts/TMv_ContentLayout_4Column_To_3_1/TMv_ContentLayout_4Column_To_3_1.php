<?php
/**
 * Class TMv_ContentLayout_4Column_To_3_1
 *
 * A 2 column layout with the same spacing as the 4-column layout.
 * The first, second, and third columns are combined into a single column.
 */
class TMv_ContentLayout_4Column_To_3_1 extends TMv_ContentLayout
{
	use TMt_PagesContentView;
	
	/**
	 * Column 1 : 75% spans 2 gaps
	 * Column 2 : 25% spans no gaps
	 */
	protected static array $column_percentages = [[75,2], [25,0] ];
	protected static ?array $column_widths = null;
	
	/**
	 * TMv_ContentLayout_1Column constructor.
	 * @param bool|int $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$column_1 = new TMv_ContentLayoutBlock();
		$column_1->addClass('column_1');
		$this->defineContentBlock($column_1);
		
		$column_2 = new TMv_ContentLayoutBlock();
		$column_2->addClass('column_2');
		$this->defineContentBlock($column_2);
		
		
		$this->setPreviewGroupingColor('13, 199, 128');
		
		
		$this->addClassCSSFile('TMv_ContentLayout_4Column_To_3_1');
		
	}
	
	/**
	 * @return int
	 */
	public static function numColumns()
	{
		return 2;
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();
		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return '2 Columns - 75% | 25%  (4-column spacing)'; }
	public static function pageContent_IsAContainer() : bool { return true; }

	public static function pageContent_ViewDescription() : string
	{
		return 'A two column layout where the first column is 75% wide and the second column is 25% wide.';
	}

}

?>