<?php

/**
 * Class TSc_ProfileController
 */
class TSc_ProfileController extends TSc_ModuleController
{
	/**
	 * TSc_ProfileController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

		// Note: Current user is always loaded in the main module controller	
		
		// Always have a user available, if one every doesn't exist, load this. \
		// Any other loading of a TMm_User will overwrite this one

		if(!TC_activeModelWithClassName('TMm_User'))
		{
			TC_saveActiveModel(TC_currentUser());
		}
	}

	/**
	 * Defining URL Targets
	 */
	public function defineURLTargets()
	{

		/** @var TSm_ModuleURLTarget $target */
		$target = TSm_ModuleURLTarget::init( 'general');
		$target->setViewName('TMv_ProfilePersonalForm');
		$target->setTitle('General');
		$this->addModuleURLTarget($target);
	
		$target = TSm_ModuleURLTarget::init( '');
		$target->setNextURLTarget('general');
		$target->setParentURLTargetWithName('general');
		$this->addModuleURLTarget($target);
	
		
	
		$target = TSm_ModuleURLTarget::init( 'password');
		$target->setViewName('TMv_ProfileChangePasswordForm');
		$target->setTitle('Password');
		$this->addModuleURLTarget($target);

		$url_targets = array('general', 'password');
		$external_profile_forms = $this->findProfileForms();

		$this->defineSubmenuGroupingWithURLTargets('general', 'password', $external_profile_forms );
	
	}

	/**
	 * Finds all the profile forms in installed modules
	 */
	protected function findProfileForms()
	{

		$targets_to_combine = array();

		/** @var TSm_ModuleList $module_list */
		$module_list = TSm_ModuleList::init();
		foreach($module_list->installedModulesForUser(TC_currentUser()) as $module)
		{
			if($module->folder() != 'profile')
			{
				// Find all the views that extend
				// loop through the classes folder
				$folder_path = $_SERVER['DOCUMENT_ROOT'].'/admin/'.$module->folder().'/classes/views/';
				if(is_dir($folder_path))
				{
					$view_folders = scandir($folder_path);
					foreach($view_folders as $view_folder_name)
					{
						if($view_folder_name != '.' && $view_folder_name != '..')
						{
							if(is_subclass_of($view_folder_name, 'TMv_ProfileForm'))
							{

								$title = str_ireplace('TMv_', '', $view_folder_name);
								$title = str_ireplace('ProfileForm', '', $title);

								if($title == '')
								{
									$title = $view_folder_name;
								}
								// Found a user form in a module that this user can see
								// Create the URL Target
								/** @var TSm_ModuleURLTarget $target */
								$target_name = 'edit-' . strtolower($title);
								$target = TSm_ModuleURLTarget::init( $target_name);
								$target->setViewName($view_folder_name);
								$target->setModelName('TMm_User');
								//$target->setTitleUsingModelMethod('title');
								$target->setTitle($title);
								//$target->setModelInstanceRequired();
								$target->setParentURLTargetWithName('');
								$this->addModuleURLTarget($target);

								$targets_to_combine[] = $target_name;


							}

						}

					}
				}
			}
		}

		return $targets_to_combine;


	}

}
?>