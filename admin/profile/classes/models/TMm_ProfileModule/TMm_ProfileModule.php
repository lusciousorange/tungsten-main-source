<?php

/**
 * Class TMm_ProfileModule
 *
 * An extension of the normal module to add functionality specific to this module.
 */
class TMm_ProfileModule extends TSm_Module
{
	
	/**
	 * TSm_Module constructor.
	 * @param array|int $module_id
	 */
	public function __construct($module_id = 'pages')
	{
		parent::__construct($module_id);
		
		// ensure this module is always auto-updated
		// since it's core to the operation of the system
		$this->setAsAutoUpdate();
	}
	
	
	/**
	 * Extend user functionality to include users with page-specific access
	 * @param TMm_User $user
	 * @return bool
	 */
	public function checkUserPermission(TMm_User $user) :bool
	{
		if($user->hasTungstenAccess())
		{
			return true;
		}
		
		return false;
	}
	
	
}





