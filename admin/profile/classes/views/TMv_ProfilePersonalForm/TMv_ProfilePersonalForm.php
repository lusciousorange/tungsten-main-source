<?php

/**
 * Class TMv_ProfilePersonalForm
 *
 * The form that shows personal information
 */
class TMv_ProfilePersonalForm extends TMv_ProfileForm
{
	use TMt_PagesContentView;

	protected $use_phone_number = false;
	protected $show_address = false;

	/**
	 * TMv_ProfilePersonalForm constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
	}
	
	/**
	 * A method that sets the success URL to be the referrer. This can be useful for forms that validate user
	 * information and this form is shown as an intermediary step.
	 */
	public function setSuccessAsReferrer()
	{
		// Ensure redirected pages go back to where they came from
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != $this->pageURL())
		{
			$this->setSuccessURL($_SERVER['HTTP_REFERER']);
		}
		
	}

	/**
	 * Turns on the feature that requires the phone number
	 * @param int $required (Optional) Default 1. 1 means shown and required, 0 means not shown, 2 means shown but
	 * not required.
	 */
	public function setUsePhoneNumber($required = 1)
	{
		$this->use_phone_number = $required;
	}


	/**
	 * Attaches the views related to personal form items
	 */
	public function personalFormItems()
	{
		if(TC_getConfig('use_single_name_field'))
		{
			$field = new TCv_FormItem_TextField('first_name', 'Name');
			$field->setIsRequired();
			$this->attachView($field);
		}
		else
		{
			$field = new TCv_FormItem_TextField('first_name', 'First name');
			$field->setIsRequired();
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('last_name', 'Last name');
			$field->setIsRequired();
			$this->attachView($field);
			
		}
		
		$email = new TCv_FormItem_TextField('email', 'Email');
		$email->setIsRequired();
		$email->setIsEmail();
		$this->attachView($email);

		if($this->use_phone_number > 0 )
		{
			$phone = new TCv_FormItem_TextField('cell_phone', 'Mobile phone');
			if($this->use_phone_number == 1)
			{
				$phone->setIsRequired();
			}

			$phone->disableAutoComplete();
			$this->attachView($phone);

		}
	}

	/**
	 * Attaches the views related to address form items
	 */
	public function addressFormItems()
	{
		if($this->show_address)
		{
			$field = new TCv_FormItem_Heading('address_heading', 'Address');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('address_1', 'Address');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('address_2', '');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('city', 'City');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('province', 'Province');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('country', 'Country');
			$this->attachView($field);

			$field = new TCv_FormItem_TextField('postal_code', 'Postal code');
			$this->attachView($field);
		}
	}

	/**
	 * Configure to ask for name and email
	 */
	public function configureFormElements()
	{
		$this->personalFormItems();
		$this->addressFormItems();



		$this->setButtonText('Update your profile');
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		
		// Phone validation
		if($form_processor->fieldIsSet('cell_phone'))
		{
			$cell_phone = $form_processor->formValue('cell_phone');
			if(!TCu_Text::validatePhoneNumber($cell_phone))
			{
				$form_processor->failFormItemWithID('cell_phone', 'It appears your phone number is incorrectly formatted. Please include an area code plus 7 digits. eg: (234) 567-8901. International number must begin with +.');
			}
		}
		
	}


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Page content form editor items
	 * @return array
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		$use_phone = new TCv_FormItem_Select('use_phone_number','Phone number');
		$use_phone->addOption(0, 'Phone number is not asked for');
		$use_phone->addOption(2,"Phone number is asked for BUT NOT required");
		$use_phone->addOption(1, 'Phone number is asked for AND required');
		$form_items[] = $use_phone;

		$field = new TCv_FormItem_Select('show_address', 'Show address');
		$field->addOption(0,'NO - Do NOT show address fields');
		$field->addOption(1,'YES - Show address fields');
		$form_items[] = $field;


		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string { return 'Profile – personal info form'; }
	public static function pageContent_IconCode() : string  { return 'fa-user'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'The form to update the current user profile';
	}

	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false;
	}

}