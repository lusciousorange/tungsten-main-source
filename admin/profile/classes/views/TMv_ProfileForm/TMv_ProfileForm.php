<?php

/**
 * Class TMv_ProfileForm
 */
class TMv_ProfileForm extends TCv_FormWithModel
{
	/**
	 * TMv_ProfileForm constructor.
	 */
	public function __construct()
	{	
		parent::__construct(TC_website()->user());
		
	}

	/**
	 * Configuration for the form items.
	 */
	public function configureFormElements()
	{
	}


}