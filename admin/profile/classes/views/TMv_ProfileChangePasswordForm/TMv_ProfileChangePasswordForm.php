<?php

/**
 * Class TMv_ProfileChangePasswordForm
 */
class TMv_ProfileChangePasswordForm extends TMv_ProfileForm
{
	use TMt_PagesContentView;

	/**
	 * TMv_ProfileChangePasswordForm constructor.
	 */
	public function __construct()
	{	
		// always load the user that is currently viewing the system
		parent::__construct();
		
	}

	/**
	 * Form Elements
	 */
	public function configureFormElements()
	{
		$current = new TCv_FormItem_Password('current_password','Current password');
		$current->setIsRequired();
		$current->setValidateUserPassword();
		$current->setSaveToDatabase(false);
		$this->attachView($current);
		
		$password = new TCv_FormItem_Password('password', 'New password');
		$password->setIsRequired();
		$password->setValidatePasswordFormat();
		$password->setValidateUniqueHistory();


		$this->attachView($password);
		
		$password_verify = new TCv_FormItem_Password('password_verify', 'Verify password');
		$password_verify->setIsRequired();
		$password_verify->setSaveToDatabase(false);
		$password_verify->setValidateMatchField($password->id());
		$this->attachView($password_verify);
				
		$this->setButtonText('Update your password');
	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Page content form editor items
	 * @return array
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string  { return 'Change profile password form'; }
	public static function pageContent_IconCode() : string  { return 'fa-user-secret'; }
	public static function pageContent_IsAddable() : bool { return TC_getModuleConfig('login', 'show_authentication'); }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A form that allows the currently logged-in user to change their password. It requires entering their current password as a verification since it only works once they are already logged in.  ';
	}
	
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false;
	}
	
}