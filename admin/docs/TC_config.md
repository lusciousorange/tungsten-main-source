# TC_config.php
The main configuration file for Tungsten is `TC_config.php` which must reside in the root of the website. It contains
top-level settings for how the site operates, primarly items that must be known before the PHP `session_start()` call or
anything related to databases or class loading.

## Configuration Values
Outlined below are the values that can be configured in `TC_config.php`. Additional values can be set as necessary for use in the
website as needed.

- `site_tungsten_title` : The name of the site as seen in various locations
- `DB_timezone` : The timezone as GMT difference (eg: `'-05:00'`).
- `show_errors` : Indicate if this website shows errors, which will enable/disable error reporting in PHP. Should be disabled for production.
- `use_local_libraries` : Indicates if libraries such as JQuery should load locally instead of from a remote host 
- `is_localhost` : Indicates if this server is a localhost
- `TCore_path` : The location of the `TCore` folder. 
- `saved_file_path` : The server path to where uploaded files are stored. 
- `DB_hostname` : The hostname for the database connection
- `DB_database` : The database name
- `DB_username` : The database username
- `DB_password` : The database password
- `DB_port` : The database port, false uses the default
- `DB_type` : The database type, currently only mysql is supported
- `console_saving` : Indicate if the system should save to the console. Should be disabled for production.
- `class_loaders` : Array of php scripts that are used with PHP's autoload functionality. 
- `skin` : (Deprecated) Name of the folder where a skin exists. Extend the `TSv_Tungsten` class instead.
- `class_overrides` : Array of classes that have overrides. Tungsten will load the override class instead of the original
when initializing using the `::init` factory method.  
- `public_requires_admin` : Indicates if the public website will only load to those who are logged in and are admins. 
- `public_requires_admin_message` : The message shown to those who don't have access
 
## Local Config File
Ever server can have a second configuration file which is local to that server. It must be called `TC_config_env.php` and
must also be located in the site root. Any settings that are defined in the local config will overwrite those set in the 
main config.

The existence of a local config also indicates to the system that we are on a development server. Since these files are
commonly ignored from repos and are server specific, the assumption is that the production server does not require a config
since all those settings are in the main `TC_config.php`.

The most common settings to adjust in the local are related to database settings as well as the `show_errors` and `console_saving`
flags. This allows a development server to use a different database and show errors, while ensuring the production site runs
silently. 

## Accessing Settings
Settings can be accessed anywhere in Tungsten via the `TC_getConfig('setting_name')` function. Similarly, a setting can 
temporarily be overwritten by the `TC_setConfig('setting_name', 'new_temp_value')`.  
