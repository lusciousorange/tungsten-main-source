# Pages - Active Models
When loading content in pages, there are many times when we must know the model(s) that are being worked on. In most cases
these are handled automatically using the properties within `TMt_PagesContentView:pageContent_View_InputModelName()` 
and the `Model Class Name` in the settings for that page. 

There are times when we need to know what models are being viewed, outside of the Page Content View scope. In those moments,
we can manually access the "Active Models".

## Background
The active model is the particular thing being viewed on a given page. It is a single instance of a model, just just a 
model class name. For example, if we have a website that lists events throughout the year, then the page with the list
of events wouldn't have an active model. However when we view a single event page, then that page would likely have a 
`TMm_Event` as the active model.  

To reiterate, this happens automatically in most instances and it's why the particular `TMm_Event` is passed into the view
that shows the event on the page. The website knows that that there is an active model for `TMm_Event` and the Page Content View
indicates that it needs a `TMm_Event`, so it passes it in. 

There can never be more than one active model for any given class name. 

## Setting an Active Model
The active model is set for each page load, so any code that sets the active model should almost always happen during the 
setup or rendering of the theme for the site. In most cases, there is no need to manually add an active model, but it is
possible using the global `TC_saveActiveModel($model)` method, where `$model` is the actual instance.

## Accessing the Active Model
It is more likely that you may need to access the active model, mostly likely in the theme setup of the site for something
that is bigger than one content view on a single page. 

_You should rarely need to access the active models from a Page Content View. The model should be passed into the 
view as a parameter and then any additional models that might be needed should be accessed view class methods. If you find
yourself accessing active models from within a Page Content View, reconsider your class structure._

To access the active model, use `TC_activeModelWithClassName($class_name)`. It will either return the model object if it
is set, or return `false`.


### Example 1 : Events
Let's assume our website that shows events. We have multiple pages for events and the theme for the website is used 
to show the event photo in a prominent location on in the header for all those pages. 

_Remember that if this is assuming it's theme or template related. Otherwise we'd just use the normal methods inside
of our actual Page Content Views._


```
// Inside of our theme that extends TMv_PagesTheme 
if($event = TC_activeModelWithClassName('TMm_Event'))
{
    // Attach event logo to header
}
```

The code above will work on any page in which:
 1. The "Model Class Name" has been set to `"TMm_Event"` in the Page Settings
 2. Any `TMt_PagesContentView` on that page returns `"TMm_Event"` as the `pageContent_View_InputModelName()`. 


## Multiple Active Models
A given page can have multiple active models, which usually correlates to models in which one is the owner or part
of another. This arises when viewing a page which is about a specific item but we need to show elements of the owner of
that item. 

In that case, we can override the method `modelsToLoadWhenActive()` for the class to indicate that it has additional 
models to load when it is the active one. That method returns an array of models which will also be set as active models
when that model is loaded.


### Example 2 : Event Teams
Let's assume that our events also have "teams", which can be viewed on their own page. That view is probably something like
`TMm_EventTeamView` and it would accept a `TMm_EventTeam`. Since every team belongs to an event, we want the header to show
 the event logo in the header as well.
 
By default, our current code would fail since there is no active `TMm_Event`, just a `TMm_EventTeam`. We could certainly
add more if statements separated by `OR` but there is a cleaner way which ensures we're just looking for events.

Within the `TMm_EventTeam` class we override the `modelsToLoadWhenActive()` method to indicate that there are additional 
 models that should be active.
 
 ```
 /**
  * Returns the additional active models when loading this model. 
  * @return TCm_Model[]
  */
 public function modelsToLoadWhenActive()
 {
    return array($this->event());
 }
 ```
 
 This method is called whenever a model is set to be active and if there is an array of models, then those are also set to active.
 In this case, we would have two active models 
 
 1. `TMm_EventTeam` - The main model being loaded
 2. `TMm_Event` - The event model for that team
 
 Using this approach, our original `if` statement will succeed since it finds an active model of `TMm_Event` and then shows
 the logo when viewing the team. 