# Public User Accounts
This document provides an overview of how public user accounts are setup within Tungsten. The general goal with this approach 
is to provide a mechanism for users in the system to "log in" on the public facing website.

## Enabling Website Authentication
In order to allow for user logins, you must first enable `Website Uses Authentication` in the `Pages > Setting`. This 
opens up a few features for later use.
1. Pages can now be "locked" via the `Requires Authentication` setting in their settings so that they only load if someone is logged in.
1. Various `Page Content Plugins` become visible in the page builder for common things such as login forms, forgot passowrd, etc.
1. The Site Template will instantiate that user and set the `current user`, which is accessible via the global `TC_currentUser()` method.

## User Account Basics
The users that can login are stored in Tungsten's `Users` module which uses the same emails and passwords that admins use
to log into the backend for managing the website. A user an exist in the system with no user-group privileges and still 
log into the public website. 

If you are an administrator for the website and are logged into Tungsten, note that the site will also detect that you are 
logged into the public website. It's the same set of credentials. 

A user who is logged into the public website but not an admin, still won't be able to access Tungsten's admin panel.

*Note:* It's recommended to create a separate non-admin account for each type of user that might use the website. Some 
permissions and settings might be associated with admin access, so a more accurate test case can be made by using a separate account.

## Creating Authenticated Pages
Any page on the website can be set to only load if a user is logged in. This top-level setting is done per-page and can be 
found in the Page's settings under `Requires Authentication`. That setting only appears if the Pages module has authentication enabled.

If the setting is set to `Yes` for a given page, then it will only load if someone is logged in.

In the Pages listing, any page that requires authentication will indicate "Login Required" in the permissions column.

*Note:* Any page that has a login form cannot be marked as requiring authentication. 

## Build-In Page Content Views
Several `Page Content Views` exist in the `login` and `pages` modules which handle some of the more common fuctionality needed
for authenticating users. 

### `TMv_LoginForm`
A form that shows a username and password which authenticates them against accounts in the `users` module. Processing of the
form will log the person in (if successful) and direct them to the page set in the content settings.

### `TMv_CreateAccountForm`
A form that allows for new accounts to be created asking for basic information needed such as name, email, etc. It includes
options for phone numbers and addresses and will also reject any duplicate email addresses. 

### `TMv_ForgotPasswordForm`
A form that allows someone to enter an email address, which will trigger will generate a `reset_code` for that account and
email the person with a link to reset their password. The content settings for this view includes an option to customize
the explanation as well as setting the "Reset Password Page" which should point to a page that loads a `TMv_EmailResetPasswordForm`.

Each user can only have one `reset-code`, so submitting the form twice will overwrite the first code. 

### `TMv_ResetPasswordEmail`
This view is the email sent to anyone who requests a password change. This is triggered by the processing of the `TMv_ForgotPasswordForm`
and provides a link similar to `/path/to/reset/<user_id>/<reset_code>/`. 

### `TMv_EmailResetPasswordForm`
A form that is loaded as a result of the email link for resetting a password. This page should be hidden since it only loads
via an email process. The form requires the URL to have the format matching the `TMv_ResetPasswordEmail` which includes
the `user_id` and `reset_code`. 

If the `user_id` and `reset_code` match, then it presents a form to enter a new password. Once completed, the user's password
is updated and the `reset_code` is cleared. 

### `TMv_ProfilePersonalForm`
This form extends the `TMv_ProfileForm` and provides the ability to edit basic information about a user such as
name, email and phone number. This particular 
form does not accept a model as an input because the `TMv_ProfileForm` always loads the `current_user` as the model. This 
is a security measure to ensure only the current user's profile is edited.

### `TMv_ProfileChangePasswordForm`
A form that allows a logged in user to change their password. It requires the re-entry of the existing password as well
as the double-entry of the new password. 

### `TMv_UserAccountRedirect`
This view is a utility view that allows pages to redirect to other pages depending on if the user is logged in and/or logged out.
This view is not necessary on pages that are marked as requiring authentication, since that process will handle it.

The most common usage for this view is on the page that shows the login or create account views. If someone is logged in and
attempts to load that page, it makes sense to redirect them to the account dashboard or some other page. This view would
then be configured to have the `Logged In Redirect Page` point to the page they should see if they are already logged in.


## Recommended Basic Page Structure
Developers have complete control over how they structure their pages, however it makes sense to provide some basic guidance 
on the pages that are likely needed for accounts. Pages with `(X)` indicate that they should be password protected. Some pages
indicate the recommended view to be loaded on that page.

- Account / Login _(redirect to Login page)_
    - Dashboard `(X)` : Customizable, no default layout
    - Edit Profile `(X)` : `TMv_ProfilePersonalForm` 
    - Change Password `(X)` : `TMv_ProfileChangePasswordForm`
    - Logout `(X)` : Often configured in Page settings to Call PHP Class Method `TCv_Website::logout()`.
    - Login : `TMv_LoginForm`, `TMv_CreateAccountForm` if necessary, possibly `TMv_UserAccountRedirect` to avoid showing the 
    login form if someone is already logged in. Just send them to Dashboard or another page. 
        - Forgot Password : `TMv_ForgotPasswordForm` Shows a form for those who forgot their password. 
        - Reset Password : `TMv_EmailResetPasswordForm` The page password resets are sent to via email links.
    
    
This format provides the basics necessary for logging in, creating an account and managing the basics of that account. The
rest of the pages that are authenticated are usually defined by the developer. 

## Showing login status in a `TMv_PagesTheme`
Most websites that require a login, also include the option to login or see if they are logged in, somewhere in the overall
template of the website. If the `TMv_LoginForm` and `TMv_CreateAccountForm` are on the same page, you can create a `TCv_Link`
that directs to the relevant page.

This code often exists in the `TMv_PagesTheme` which has a method called `user()` that returns the current logged in user
if it is set, and returns `false` otherwise. The basic code is provided below. We recommend only showing the first name 
in order to avoid situations with awkward long last names.  

    $user_link = new TCv_Link('header_user_link');
    $user_link->setURL('/account/');
    $user_link->setIconClassName('fas fa-user-circle');
    
    if($this->user())
    {
        $user_link->addClass('logged_in');
        $user_link->addText($this->user()->firstName());
    }
    else
    {
        $user_link->addText('Login');
    }
    $parent_view->attachView($user_link);



## Restricting Access using Visibility Conditions
It's likely that a user may not have permission to see _every_ page that requires authentication, so there is a mechanism
to restrict access one step further. This is done per-page in the Page Settings. Each page has a `Visibility Condition` 
which is a specifically formatted string that is processed as part of other validation for the page. 

The general format is `{{TMm_ClassName.methodName}}` or `{{TMm_ClassName.methodName AND TMm_ClassName.otherMethodName}}`. The condition
must begin with `{{` and end with `}}`. Inside of that condition are any number of class names combined with a method name 
which are each separated by `AND`. 

Additionally, `NOT` can be included before a condition to negate it such as `{{NOT TMm_ClassName.methodName}}`

NOTE: Visibility Conditions do not accept `OR`. If that is required, we recommend writing a new method in that class that
performs that functionality and returns a boolean, then calling that method. 

#### Common `TCm_Model` methods `userCanEdit()`, `userCanDelete` and `userCanView()`
The `TCm_Model` class has common methods that can be overridden by each subclass to return a boolean if the current user
can view, edit or delete that model. These are commonly used as conditions for pages which are viewing that particular model.

#### Setting Model Class Names
In order for the Visibility Conditions to work, they require a model to call the methods against. This means that in most 
cases the page must be aware of the model and load it via the URL using a provided ID. This is commonly done in data-driven
websites in Tungsten, however it's important to note that without setting the `Model Class Name` in the page settings, 
the visibility conditions will likely generate errors.

## Example : Team Management Page
Suppose we have a page that lets a user manage a team in the system. In most cases we only want a person to edit a team if 
they have the correct permissions. This means the page must exist and we must set the permissions correctly to avoid users
editing pages they shouldn't be able to.

**Page URL** : `/teams/edit-team/<team_id>`

**Requires Authentication** : `Yes` to ensure it fails if someone isn't logged in

**Model Class Name** : `TMm_Team` the name of the model that must be provided. This ensures we instantiate a new `TMm_Team` 
with the `team_id` provided in the URL.

**Visibility Condition** : `{{TMm_Team.userCanEdit}}` This ensures that when loading, that method must also return `true` 
called on the same model that was instantiated with the `team_id`.

As a result, if someone is logged in and attempts to change the ID in the URL, and they dont' have permission for that team,
the website will avoid loading the page and exposing information it shouldn't. 