# Cron Jobs
Running a cron job involves having the web server run a regularly scheduled script which perofrms a 
series of tasks without the need for a web browser to trigger anything. This is commonly used for
behind the scenes tasks such as syncing or back-end cleanup. 

## Relevant Classes / Files
Tungsten has a few built in classes and files that help reduce the amount of overhead required to run a cron job.

####cron.php
A basic PHP script found in the site root and is the script that is called by the cron job. It can also be manually 
loaded by called `http://yourdomain.com/cron.php`. 

#### TSi_Cronable
An interface that must be applied to any class that wishes to add something to the cron job. Every class
that uses the interface will define a method called `runCron()` which will perform whatever task is needed for that class.

#### TSm_CronManager
A class that looks through all the classes in the project and finds any that implement the `TSi_Cronable` interface, then
it calls the `runCron()` method on each one of them.

## Configuration
There is plenty of online documentation on how to setup cron jobs on a server and the specifics of those
can vary depending on your hosting platform and their possible restrictions. 

#### Cron Command
The command must be something "runnable" on 
the web server and we normally recommend the following format:

`wget --no-check-certificate --delete-after -U firefox "https://yourdomain.com/cron.php"`

This command uses the `wget` command to essentially load a webpage and the other commands reduce the likelihood of an error occuring.

#### Hosting May Vary
Many hosting platforms provide a mechanism to setup a cron job without the command line, however the link below provides a great
resource for understanding the details.

http://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/ 

#### Tungsten Settings
There are two settings in `System > Settings` which let you control cron calls which `TSm_CronManager` will respect.

* `Cron Domain` indicates the domain that will be called. This is sometimes different than the actual domain that is being loaded and
is automatically filled in by the site the first time it's loaded.
* `Cron Tracking` a flag to indicate if the system will track whenever cron.php is called. This helps debug if the calls are even
happening on the server. Enabling this flag will create a new entry in the `cron_calls` table for each cron call.


**NOTE:** If you enable Cron Tracking, the script will restrict it to one call every 60 seconds.
#### Calling a Single Class runCron()
It's possible to call only a single class' runCron() if necessary. In that case, you pass the class name via the URL
to the script.

`http://yourdomain.com/cron.php?class_name=TMm_YourClass`

## Testing the Cron Job
There are a few methods to test and confirm cron jobs. 

#### Manual Script Call
The easiest way to test the cron job would be to run `http://yourdomain.com/cron.php` in a separate browser tab, 
then using the Tungsten Console confirm the results as needed. 

1. Open a new tab and load the cron script
2. Go back to the first tab (must be same session), and load the console

You'll see the last call was to `/cron.php` and if you performed any tasks that would appear in the console, you'd see them there.

#### Using the `cron_calls` table
If you enable cron tracking, then you'll see each of your asyncronous cron calls on the server.