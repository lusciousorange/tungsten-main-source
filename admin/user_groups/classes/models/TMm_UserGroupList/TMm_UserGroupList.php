<?php

/**
 * Class TMm_UserGroupList
 */
class TMm_UserGroupList extends TCm_ModelList
{
	/**
	 * TMm_UserGroupList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_UserGroup',$init_model_list);
	}
	
	/**
	 * Returns all the user groups
	 * @return TMm_UserGroup[]
	 */

	public function groups()
	{
		return $this->models();
	}
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if all when we try and get all the items for this list, if we should cache them.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cacheAllItemsFromTable() : bool
	{
		return true;
	}
	
	
	
}

?>