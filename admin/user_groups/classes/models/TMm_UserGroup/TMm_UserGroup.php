<?php
/**
 * Class TMm_UserGroup
 */
class TMm_UserGroup extends TCm_Model
{
	//use TMt_Searchable; // Disabled on most sites. Unless you have a lot of user groups. Then extend and enable.
	
	// DATABASE COLUMNS
	protected $group_id, $title, $code, $description, $tungsten_access, $home_url_target, $api_access;
	
	// CLASS-DEFINED PROPERTIES
	protected ?array $users = null;
	
	protected string $home_module = '';
	protected ?array $permitted_modules = null;
	
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'group_id'; // [string] = The id column for this class
	public static $table_name = 'user_groups'; // [string] = The table for this class
	public static $model_title = 'User group';
	public static $primary_table_sort = 'title ASC';
	
	/**
	 * TMm_UserGroup constructor.
	 *
	 * @param array|int $group_id
	 */
	public function __construct($group_id)
	{
		parent::__construct($group_id);
		
		// Not a digit or an array, try to find group with code
		if(is_string($group_id))
		{
			$query = "SELECT * FROM `user_groups` WHERE code = :code LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('code'=>$group_id));
			if($row = $result->fetch())
			{
				$this->id = $row['group_id'];
			}
		}
		
		// instantiate group from table
		$this->setPropertiesFromTable();
		
	}
	
	/**
	 * Returns the title for the group
	 * @return string
	 */
	public function title() : string
	{
		return $this->title;
	}
	
	/**
	 * Returns the code for the group
	 * @return string
	 */
	public function code() : string
	{
		return $this->code;
	}
	
	/**
	 * Returns the description for this group
	 *
	 * @return string
	 */
	public function description() : string
	{
		return $this->description;
	}
	
	/**
	 * Returns if this group has Tungsten admin access
	 * @return bool
	 */
	public function tungstenAccess() : bool
	{
		return $this->tungsten_access;
	}
	
	/**
	 * Returns if this group has api access
	 * @return bool
	 */
	public function apiAccess() : bool
	{
		if(TC_getConfig('use_api'))
		{
			return $this->api_access;
		}
		
		return false;
		
	}
	
	
	
	/**
	 * Returns a "Yes" or "no" string indicating if the user group has tungsten access
	 * @return string
	 */
	public function tungstenAccessString() : string
	{
		return $this->tungstenAccess() ? 'Yes' : 'No';
	}
	
	/**
	 * Returns a "Yes" or "no" string indicating if the user group has tungsten access
	 * @return string
	 */
	public function apiAccessString() : string
	{
		return $this->apiAccess() ? 'Yes' : 'No';
	}
	
	/**
	 * Returns the array of users in this group
	 * @return TMm_User[]
	 */
	public function users() : array
	{
		if(is_null($this->users))
		{
			$this->users = [];
			$query = "SELECT u.* FROM users u INNER JOIN user_group_matches m USING(user_id) WHERE m.group_id = :group_id ORDER BY u.last_name, u.first_name";
			$result = $this->DB_Prep_Exec($query, array('group_id' => $this->id()));
			while($row = $result->fetch())
			{
				$user = TMm_User::init( $row);
				$this->users[$user->id()] = $user;
			}
		}
		
		return $this->users;
	}
	
	/**
	 * Returns the number of users in this group
	 * @return int
	 */
	public function numUsers() : int
	{
		if(is_array($this->users))
		{
			return sizeof($this->users());
		}
		else // avoid a possible large query
		{
			$query = "SELECT u.user_id FROM users u INNER JOIN user_group_matches m USING(user_id) WHERE m.group_id = :group_id ";
			$result = $this->DB_Prep_Exec($query, array('group_id' => $this->id()));
			return sizeof($result->fetchAll());
		}
	}
	
	/**
	 * @return string
	 */
	public function homeModuleTitle() : string
	{
		if($module = $this->homeModule())
		{
			$title = $module->title();
			if($this->homeURLTargetName() != '')
			{
				$title .= " > ".$this->homeURLTargetName();
			}
			
			return $title;
		}
		
		return 'Default';
	}
	
	/**
	 * Returns the module that is home for this user group, otherwise false
	 * @return null|TSm_Module
	 */
	public function homeModule() : ?TSm_Module
	{
		if($this->home_module !== '')
		{
			$module = TSm_Module::init($this->home_module);
			if($module && $module->installed())
			{
				return $module;
			}
		}
		
		return null;
	}
	
	public function homeURLTargetName() : string
	{
		return trim($this->home_url_target);
	}
	
	/**
	 * The list of modules that this user group can access
	 * @return TSm_Module[]
	 */
	public function permittedModules() : array
	{
		if(is_null($this->permitted_modules))
		{
			$this->permitted_modules = [];

			$query = "SELECT m.* FROM module_access_groups ag INNER JOIN modules m USING (module_id) WHERE ag.group_id = :group_id";
			$result = $this->DB_Prep_Exec($query, array('group_id' => $this->id()));
			while($row = $result->fetch())
			{
				$module = TSm_Module::init($row);
				$this->permitted_modules[$module->id()] = $module;
				
			}
		}
		return $this->permitted_modules;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Indicates if the provided user can edit this group
	 * @param TMm_User|bool $user (Optional) Default false, which uses the current user
	 * @return bool
	 */
	public function userCanEdit($user = false)
	{
		if($user == false)
		{
			$user = TC_currentUser();
		}
		return $user->isAdmin();
	}
	
	/**
	 * Indicates if the provided user can delete this model
	 * @param bool|TMm_User $user (Optional) Default false, which uses the current user
	 * @return bool
	 */
	public function userCanDelete($user = false)
	{
		if($this->id() == 1) // can't delete system administrator group
		{
			return false;
		}
		
		return parent::userCanDelete($user);
	}
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_Searchable
	//
	//////////////////////////////////////////////////////
	
	
	//////////////////////////////////////////////////////
	//
	// MEMCACHED
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Plans should be cached when found from properties in tables.
	 * @return bool
	 * @see TC_Memcached
	 */
	public static function cachePropertiesFromTable() : bool
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema() + [
				
				'title' => [
					'comment'       => 'The user group title',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
				'code' => [
					'comment'       => 'The code',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				
				'tungsten_access' => [
					'comment'       => 'Indicates if users in this group have access to the Tungsten admin',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'api_access' => [
					'comment'       => 'Indicates if users in this group have access to make API calls',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				'description' => [
					'comment'       => 'The description of the user group',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'home_module' => [
					'comment'       => 'The folder name for the homepage module for this user group ',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				'home_url_target' => [
					'comment'       => 'The url target name that is loaded within hte home module if set',
					'type'          => 'varchar(32)',
					'nullable'      => false,
				],
				
				// Old value that ends up in DBs
				'user_id' => [
					'delete' => true,
				],
			
			
			];
	}
	
}
