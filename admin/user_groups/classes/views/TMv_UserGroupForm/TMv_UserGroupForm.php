<?php
/**
 * Class TMv_UserGroupForm
 */
class TMv_UserGroupForm extends TCv_FormWithModel
{
	/**
	 * TMv_UserGroupForm constructor.
	 *
	 * @param string|TMm_UserGroup $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
	}
	
	/**
	 * Set the required model for the form
	 * @return class-string<TCm_Model>|null
	 */
	public static function formRequiredModelName(): ?string
	{
		return 'TMm_UserGroup';
	}
	
	
	/**
	 * The configuration of the form
	 */
	public function configureFormElements()
	{
		$title = new TCv_FormItem_TextField('title', 'Title');
		$title->setIsRequired();
		$this->attachView($title);
		
		$show_codes = true;
		if($this->isEditor())
		{
			if($this->model()->id() == 1)
			{
				// never allow the administrator group to have the code or tungsten access to be altered
				$show_codes = false;
			}
		}

//
//		$description = new TCv_FormItem_TextBox('description', 'Description');
//		$this->attachView($description);
		
		if($show_codes)
		{
			// DO away with this in Tungsten 9
			if(!TC_getConfig('use_tungsten_9'))
			{
				$title = new TCv_FormItem_TextField('code', 'Code');
				$title->setHelpText('An option code that can be useful for identifying groups by a shorthand name, rather than an id.');
				$title->setIsAlphaNumericUnderscore();
				$this->attachView($title);
			}
			$tungsten_access = new TCv_FormItem_Select('tungsten_access', 'Tungsten Access');
			$tungsten_access->setHelpText('If turned on, any user that is a part of this group will have access to log into Tungsten');
			$tungsten_access->addOption('0', 'No');
			$tungsten_access->addOption('1', 'Yes');
			$this->attachView($tungsten_access);
			
			// API ACCESS, IF ENABLED
			if(TC_getConfig('use_api'))
			{
				$field = new TCv_FormItem_Select('api_access', 'API Access');
				$field->setHelpText('Indicate if this user group can have access to the API.');
				$field->addOption('0', 'No');
				$field->addOption('1', 'Yes');
				$this->attachView($field);
				
			}
		}
		
		
		$field = new TCv_FormItem_Select('home_module', 'Home Module');
		$field->setHelpText('Indicate if this user group has a special module that it should load when logging into Tungsten');
		$field->addOption('', 'Default');
		
		$module_list = TSm_ModuleList::init();
		foreach($module_list->modules() as $module)
		{
			
			$field->addOption($module->folder(), $module->title());
			
		}
		$this->attachView($field);
		
		$title = new TCv_FormItem_TextField('home_url_target', 'Home URL Target Name');
		$title->setHelpText('If the home module is set, you can also specify a particular URL Target name instead of the default.');
		$title->setIsAlphaNumericUnderscore();
		$this->attachView($title);
		
		
		// Show the list of modules that this user can access
		if(TC_getConfig('use_tungsten_9'))
		{
			// System admins don't show this
			if($this->isEditor() && $this->model()->id() == 1)
			{
				return;
			}
			
			$module_list = new TSm_ModuleList();
			$modules = new TCv_FormItem_CheckboxList('modules', 'Modules');
			$modules->setSaveToDatabase(false);
			$modules->setHelpText('Select the modules that this user group can access.');
			
			$modules->setUseMultiple(
				TCv_FormItem::USE_MULTIPLE_PAIRING_TABLE,
				'module_access_groups',
				'module_id',
				$this->model()
			
			);
			
			
			$selected_modules = array();
			if($this->isEditor())
			{
				$selected_modules = $this->model->permittedModules();
			}
			
			
			
			$possible_modules = $module_list->modules();
			//unset($possible_modules[1]);// remove admin from options
			
			// Don't list any of the public ones
			foreach($possible_modules as $index => $module)
			{
				// Modules that are all access (Public), don't require these
				// User groups are part of system and also system can't be exposed to other groups
				if($module->isAllAccess() || $module->folder()== 'user_groups' || $module->folder()== 'system')
				{
					unset($possible_modules[$index]);
				}
			}
			
			$modules->setValuesFromObjectArrays($possible_modules, $selected_modules);
			$this->attachView($modules);
		}
		
	}
	
}