<?php
class TMv_UserGroupFormTest extends TC_FormWithModelTestCase
{
	
	
	public function testCreateUserGroup()
	{
		// Pass in a string for the class name, which sets the mode to "creator"
		$form = $this->resetFormWithModel('TMm_UserGroup');
		
		$values = array(
			'title' => 'New Group',
			'code' => 'new_code'
			
		);
		$form_processor = $this->processFormWithValues($form, $values);
		$this->assertTrue($form_processor->isValid());
		
	}
}