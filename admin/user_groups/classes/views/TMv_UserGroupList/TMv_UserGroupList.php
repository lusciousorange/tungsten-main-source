<?php
/**
 * Class TMv_UserGroupList
 *
 * A list of all the user groups in the system
 */
class TMv_UserGroupList extends TCv_ModelList
{
	/**
	 * TMv_UserGroupList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_UserGroup');
		$this->defineColumns();
	}
	
	/**
	 * Defines the columns for this list
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setWidthAsPercentage(30);
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		if(!TC_getConfig('use_tungsten_9'))
		{
			$column = new TCv_ListColumn('code');
			$column->setTitle('Code');
			$column->setContentUsingModelMethod('code');
			$this->addTCListColumn($column);
		}
		$column = new TCv_ListColumn('tungsten_access');
		$column->setTitle('Tungsten');
		$column->setContentUsingListMethod('tungstenAccessColumn');
		$column->setAlignment('center');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('modules');
		$column->setTitle('Modules');
		$column->setContentUsingListMethod('modulesColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('home_module');
		$column->setTitle('Home module');
		$column->setContentUsingModelMethod('homeModuleTitle');
		$this->addTCListColumn($column);
		
		if(TC_getConfig('use_api'))
		{
			$column = new TCv_ListColumn('api_access');
			$column->setTitle('API access');
			$column->setContentUsingListMethod('apiAccessColumn');
			$column->setAlignment('center');
			$this->addTCListColumn($column);
		}
		
//		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
//		$this->addTCListColumn($edit_button);
//
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
				
	}
	
	
	/**
	 * The title column
	 * @param TMm_UserGroup $model
	 * @return bool
	 */
	public function titleColumn($model)
	{
		if($model->userCanEdit())
		{
			if(TC_getConfig('use_tungsten_9'))
			{
				$url_target = TC_URLTargetFromModule('system', 'user-group-edit');
			}
			else
			{
				$url_target = TC_URLTargetFromModule( 'user_groups' , 'edit');
			}
			
			$url_target->setModel($model);
			$url_target->validateAccess(true);
			$link = TSv_ModuleURLTargetLink::init($url_target);
			return $link;
			
		}
		
		return $model->title();

	}
	
	/**
	 * The access column
	 * @param TMm_UserGroup $model
	 * @return bool
	 */
	public function tungstenAccessColumn($model)
	{
		return $model->tungstenAccess() ? 'Yes' : 'No';
//
//		if($model->id() == 1 || $model->tungstenAccess())
//		{
//
//		}
//
//		if(TC_Website()->user()->isAdmin())
//		{
//			if($model->id() == 1) // admin
//			{
//				return 'Yes - All Access';
//			}
//			elseif($model->tungstenAccess())
//			{
//				$manage_link = new TCv_Link();
//				$manage_link->setURL('/admin/system/do/list/');
//				$manage_link->addText('Yes – Manage');
//				return $manage_link;
//			}
//			else
//			{
//				return 'No';
//			}
//
//
//		}
//		else // not an admin, return yes or no
//		{
//			return $model->tungstenAccessString();
//		}
	
	}
	
	/**
	 * The access column
	 * @param TMm_UserGroup $model
	 * @return mixed
	 */
	public function modulesColumn($model)
	{
		if(TC_Website()->user()->isAdmin())
		{
			if($model->id() == 1) // admin
			{
				return 'All';
			}
			elseif($model->tungstenAccess())
			{
				$view = new TCv_View();
				$view->setTag('p');
				foreach($model->permittedModules() as $module)
				{
					$view->addText('<br/>'.$module->title());
				}
				return $view;
//				$manage_link = new TCv_Link();
//				$manage_link->setURL('/admin/system/do/list/');
//				$manage_link->addText('Yes – Manage');
//				return $manage_link;
			}
			else
			{
				return '–';
			}
			
			
		}
		else // not an admin, return yes or no
		{
			return $model->tungstenAccessString();
		}
		
	}
	
	/**
	 * The access column
	 * @param TMm_UserGroup $model
	 * @return bool
	 */
	public function apiAccessColumn($model)
	{
		return $model->apiAccessString();
		
		
	}
	
	
	
	
	/**
	 * The delete column
	 * @param TMm_UserGroup $model
	 * @return bool
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'delete', 'fa-trash');
	
	}
	
	
		
}
?>