<?php
	$module_title = 'User groups'; // The name displayed for the Module
	$version = "1.0.7"; // the current version of this module
	$min_system_version = '6.0';// the earliest version that this module will work with
	$show_in_menu = false;

	// Tungsten 9 value to indicate if the module appears in the left side menus
	// A value of true or false is immutable and it always appears
	// A value of null indicates that it is editable in the System configuration
	$show_in_left_menu = false;
	
	// ICON FONT LIBRARY CODE
	// Every module can have a single icon, which is limited to using a code from a font library. The available list of font libraries is outlined below. In each case, you must provide a valid icon value and the system will handle any processing to ensure it is presetned properly.
	// Font Awesome - fortawesome.github.io/Font-Awesome/icons/
	$icon_library_code = 'fa-users-rectangle';
	
	// MODEL NAME
	// The class name for the primary model for this module. Most modules have on primary class that is represented. False otherwise. 
	$model_name = 'TMm_UserGroup';
	
	// MODULE CONTROLLER CLASS
	// The controller class that defines the url targets for this module. Many modules will require a custom module controller, however the most basic module will default to using the TSc_ModuleController.php class. If a custom controller is not needed, comment out the line below.
	$controller_class = 'TMc_UserGroupsController';
?>